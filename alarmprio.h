/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        alarmprio.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for alarmprio.c module
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

#ifndef	ALARMPRIO_H
#define	ALARMPRIO_H


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/

    /* Alarm state constants */
    enum
    {
        ALM_STATE_ALARM_INIT = 0,
        ALM_STATE_ALARM_NONE,	
        ALM_STATE_MASTER_SMOKE,	
        ALM_ALARM_MASTER_CO,
        ALM_ALARM_SLAVE_SMOKE,
        ALM_ALARM_SLAVE_CO,	
        ALM_ALARM_REMOTE_SMOKE,
        ALM_ALARM_REMOTE_CO,
        ALM_ALARM_REMOTE_SMOKE_HW,
        ALM_ALARM_REMOTE_CO_HW,
        ALM_STATE_CO_CLR,
        ALM_STATE_SMK_CLR

    };

/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
    // Global Routines
    uint ALM_Priority_Task(void);
    
    #ifdef CONFIGURE_SERIAL_ALM_SOURCE_OUT
        uchar ALM_Get_State(void);
    #endif

    #ifdef CONFIGURE_WIRELESS_ALARM_LOCATE
        void ALM_Init_Locate_Timer(void);
    #endif

#ifdef	CONFIGURE_WIRELESS_MODULE
    #ifdef CONFIGURE_INTERCONNECT
        void ALM_Init_Master_Slave_Timer(void);        
        void ALM_Check_Master_Slave_Timer(void);
    #endif
#endif

#endif  /* ALARMPRIO_H */
