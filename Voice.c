/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        voice.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Support routines for MOAL 1.5 Voice Chip
|               
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/



/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include "common.h"
#include "voice.h"
#include "photosmoke.h"
#include "cocalibrate.h"
#include "systemdata.h"


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

#define VOICE_TIME_61_SECONDS       (uint)61000

#ifdef CONFIGURE_2018_VOICE_CHIP
    #define VCE_NUM_MESSAGES                125
#else
    #define VCE_NUM_MESSAGES                120
#endif


#ifdef	CONFIGURE_VOICE

//***********************************************************
// If voice and data lines need to be inverted, uncomment.
// #define  INVERT_VOICE_CLOCK_DATA
//
//***********************************************************

// Define Voice Clock and Data I/O actions at the Voice Chip Pin
#ifdef	INVERT_VOICE_CLOCK_DATA

	#define	VOICE_CLOCK_HIGH    LAT_VOICE_CLK_PIN=0;
	#define	VOICE_CLOCK_LOW     LAT_VOICE_CLK_PIN=1;
	#define	VOICE_CLOCK_ON      TRISCbits.TRISC4 = 0;
	#define	VOICE_CLOCK_OFF     TRISCbits.TRISC4 = 1;

    #define	VOICE_DATA_HIGH     LAT_VOICE_DATA_PIN=0;
	#define	VOICE_DATA_LOW      LAT_VOICE_DATA_PIN=1;
	#define	VOICE_DATA_ON       TRISCbits.TRISC5 = 0;
	#define	VOICE_DATA_OFF      TRISCbits.TRISC5 = 1;

#else

	#define	VOICE_CLOCK_HIGH    LAT_VOICE_CLK_PIN=1;
	#define	VOICE_CLOCK_LOW     LAT_VOICE_CLK_PIN=0;
	#define	VOICE_CLOCK_ON      TRISCbits.TRISC4 = 0;
	#define	VOICE_CLOCK_OFF     TRISCbits.TRISC4 = 1;

	#define	VOICE_DATA_HIGH     LAT_VOICE_DATA_PIN=1;
	#define	VOICE_DATA_LOW      LAT_VOICE_DATA_PIN=0;
	#define	VOICE_DATA_ON       TRISCbits.TRISC5 = 0;
	#define	VOICE_DATA_OFF      TRISCbits.TRISC5 = 1;


#endif

//  Active timing is in 10 ms Units

#define POWERUP_VOICE_DELAY_130_MS      (uint)130     // in ms
#define	POWERUP_VOICE_DELAY_200_MS      (uint)200     // in ms
#define POWERUP_VOICE_DELAY_230_MS      (uint)230     // in ms

// Voice chip requires 10 ms tic times

#define VOICE_CLOCK_TIME_10_MS           (uint)10     // in MS  
#define VOICE_CLOCK_TIME_200_MS          (uint)200
#define VOICE_CLOCK_TIME_500_MS          (uint)500

#define VOICE_TIME_50_MS                 (uint)50     // in MS  
#define VOICE_TIME_100_MS                (uint)100    // in MS  

#define VCE_PAUSE_TIME_200_MS       (uchar)2       // 200 ms
#define VCE_PAUSE_TIME_500_MS       (uchar)5       // 500 ms


#define	VCE_STATE_IDLE              0
#define	VCE_STATE_CLOCK_ACTIVE		1
#define	VCE_STATE_CLOCK_INACTIVE	2
#define	VCE_STATE_TX_END            3
#define VCE_STATE_DWELL             4
#define VCE_STATE_MESSAGE_COMPLETE  5

#define VCE_QSTART          (uchar)0
#define VCE_QLENGTH         (uchar)10       // locations 0 - 9
#define VCE_QEND            (uchar)(VCE_QLENGTH-1)
    
#define VCE_QUE_FULL        (uchar)VCE_QLENGTH-1
#define VCE_QUE_NEAR_FULL   (uchar)VCE_QLENGTH-2
#define VCE_QUE_EMPTY       (uchar)0
    


/*
|-----------------------------------------------------------------------------
| Typedef and Enums and Constants
|-----------------------------------------------------------------------------
*/
#ifndef CONFIGURE_VOICE_SERIAL

    // Times are for US / French Voice Chip
    // Voice Index Table Holding Voice message and tone play lengths
    const uchar VCE_PLAY_LENGTH_TABLE[VCE_NUM_MESSAGES] =
    {
        // Voice message play time table (100 ms units, 30 = 3 secs)
        19,         // VCE_ENGLISH_SELECTED
        29,         // VCE_FR_FRENCH_SELECTED
        34,         // VCE_READY_TO_CONNECT
        43,         // VCE_FR_READY_TO_CONNECT
        14,         // VCE_SETUP_COMPLETE
        18,         // VCE_FR_SETUP_COMPLETE
        14,         // VCE_DEVICES_CONNECTED
        18,         // VCE_FR_DEVICES_CONNECTED
        9,          // VCE_SUCCESS
        9,          // VCE_FR_SUCCESS
        14,         // VCE_HUSH_ACTIVATED
        19,         // VCE_FR_HUSH_ACTIVATED
        16,         // VCE_NO_DEVICES_FOUND
        22,         // VCE_FR_NO_DEVICES_FOUND
        13,         // VCE_HUSH_CANCELLED
        18,         // VCE_FR_HUSH_CANCELLED
        16,         // VCE_TEMP_SILENCED
        23,         // VCE_FR_TEMP_SILENCED
        6,          // VCE_FIRE_ENGLISH_1
        5,          // VCE_FIRE_FRENCH_1
        11,         // VCE_TEST_CANCELLED
        13,         // VCE_FR_TEST_CANCELLED
        5,          // VCE_ONE
        4,          // VCE_FR_ONE
        5,          // VCE_TWO
        5,          // VCE_FR_TWO
        5,          // VCE_THREE
        5,          // VCE_FR_THREE
        6,          // VCE_FOUR
        6,          // VCE_FR_FOUR
        6,          // VCE_FIVE
        8,          // VCE_FR_FIVE
        6,          // VCE_SIX
        7,          // VCE_FR_SIX
        6,          // VCE_SEVEN
        5,          // VCE_FR_SEVEN
        6,          // VCE_EIGHT
        6,          // VCE_FR_EIGHT
        7,          // VCE_NINE
        5,          // VCE_FR_NINE
        5,          // VCE_TEN
        6,          // VCE_FR_TEN
        7,          // VCE_ELEVEN
        7,          // VCE_FR_ELEVEN
        6,          // VCE_TWELVE
        7,          // VCE_FR_TWELVE
        8,          // VCE_THIRTEEN
        7,          // VCE_FR_THIRTEEN
        8,          // VCE_FOURTEEN
        9,          // VCE_FR_FOURTEEN
        9,          // VCE_FIFTEEN
        7,          // VCE_FR_FIFTEEN
        10,         // VCE_SIXTEEN
        8,          // VCE_FR_SIXTEEN
        10,         // VCE_SEVENTEEN
        7,          // VCE_FR_SEVENTEEN
        8,          // VCE_EIGHTEEN
        6,          // VCE_FR_EIGHTEEN
        9,          // VCE_NINETEEN
        8,          // VCE_FR_NINETEEN
        6,          // VCE_TWENTY
        4,          // VCE_FR_TWENTY
        9,          // VCE_TWENTY_ONE
        8,          // VCE_FR_TWENTY_ONE
        9,          // VCE_TWENTY_TWO
        8,          // VCE_FR_TWENTY_TWO
        9,          // VCE_TWENTY_THREE
        9,          // VCE_FR_TWENTY_THREE
        10,         // VCE_TWENTY_FOUR
        10,         // VCE_FR_TWENTY_FOUR
        20,         // VCE_NETWORK_SEARCH
        27,         // VCE_FR_NETWORK_SEARCH
        27,         // VCE_TESTING_AND_LOUD
        25,         // VCE_FR_TESTING_AND_LOUD
        12,         // VCE_TEST_COMPLETE
        12,         // VCE_FR_TEST_COMPLETE
        20,         // VCE_RESET_WIRELESS_SETTINGS
        32,         // VCE_FR_RESET_WIRELESS_SETTINGS
        12,         // VCE_NOW_CONNECTED
        14,         // VCE_FR_NOW_CONNECTED
        28,         // VCE_TOO_MUCH_SMOKE
        33,         // VCE_FR_TOO_MUCH_SMOKE
        12,         // VCE_NOT_CONNECTED
        11,         // VCE_FR_NOT_CONNECTED
        20,         // VCE_PRESS_TO_CANCEL_TEST
        25,         // VCE_FR_PRESS_TO_CANCEL_TEST
        22,         // VCE_CO_ENGLISH
        29,         // VCE_CO_FRENCH
        18,         // VCE_SMK_PREVIOUSLY
        20,         // VCE_FR_SMK_PREVIOUSLY
        24,         // VCE_CO_PREVIOUSLY
        30,         // VCE_FR_CO_PREVIOUSLY
        25,         // VCE_ERROR_SEE_GUIDE
        29,         // VCE_FR_ERROR_SEE_GUIDE
        11,         // VCE_REPLACE_ALARM
        17,         // VCE_FR_REPLACE_ALARM
        23,         // VCE_BUTTON_TO_SILENCE
        43,         // VCE_FR_BUTTON_TO_SILENCE
        12,         // VCE_NETWORK_CONN_LOST
        13,         // VCE_FR_NETWORK_CONN_LOST
        9,          // VCE_EMERGENCY
        8,          // VCE_FR_EMERGENCY
        21,         // VCE_WEATHER_ALERT
        22,         // VCE_FR_WEATHER_ALERT
        14,         // VCE_WATER_LEAK
        15,         // VCE_FR_WATER_LEAK
        18,         // VCE_EXPLOSIVE_GAS
        19,         // VCE_FR_EXPLOSIVE_GAS
        15,         // VCE_TEMP_DROP
        21,         // VCE_FR_TEMP_DROP
        13,         // VCE_INTRUDER_ALERT
        16,         // VCE_FR_INTRUDER_ALERT
        7,          // VCE_SND_SONAR_PING
        4,          // VCE_SND_BTN_PRESS
        6,          // VCE_SND_NETWORK_CLOSE
        15,         // VCE_SND_NETWORK_ERROR
        9,          // VCE_SND_POWER_ON
        6,          // VCE_SND_PUSH_TO_TEST
        11,         // VCE_SND_RETURN_TO_OOB

    #ifdef CONFIGURE_2018_VOICE_CHIP
        // Recent Voice messages added (7/24/208)
        10,         // Not Used
        20,         // VCE_ACTIVATE_BATTERY         120,Ox78
        30,         // VCE_FR_ACTIVATE_BATTERY      121,Ox79
        32,         // VCE_FR_FOR_FRENCH_PUSH_BTN   122,Ox7A
        32          // VCE_FOR_ENGLISH_PUSH_BTN     123,Ox7B
    #else
        32          // VCE_FR_FOR_FRENCH_PUSH_BTN
    #endif
                
    };

#endif



/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
static uchar	voice_state = 0;
static uchar 	voice_current_code = 0;
static uchar 	voice_bit_counter = 0;

static uchar    voice_msg_play_timer = 0;

#ifndef CONFIGURE_VOICE_SERIAL 
    static uchar   voice_current_length = 10;        // 1 Sec in 100 ms units
#endif
    
uchar VCE_Msg_Queue[VCE_QLENGTH];
static uchar vce_msg_qin;
static uchar vce_msg_qout;


/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/
#ifndef CONFIGURE_UNDEFINED
    // Test only, undefine later
    void vce_send_serial(uchar v);
#endif

void vce_msg_qinit (void);
uchar vce_msg_qpop(void);
void vce_msg_qpush(uchar c);
uchar vce_msg_exque(void);
    
    
    
/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/

/*
+------------------------------------------------------------------------------
| Function:      VCE_Process_Task
+------------------------------------------------------------------------------
| Purpose:       Test voice message queue for voice to process and handles
|                Serial voice chip driver control
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Voice Task Next Execution Time (in Task TICs)
+------------------------------------------------------------------------------
*/

uint VCE_Process_Task(void)
{

    switch (voice_state)
    {

        case VCE_STATE_IDLE:
        {
            // See if anything to announce        
            uchar c = vce_msg_exque();
            if(VCE_QUE_EMPTY == c)
            {
                // No Voice message in progress
                // We want the clock and data low when in Idle
                // since the Voice Power may be off in Idle
                VOICE_CLOCK_LOW
                VOICE_DATA_LOW
                        
                // If a Button Press is pending leave Power Pin active        
                if(!FLAG_VCE_BUTTON_PRESS)
                {
                    // OK to Remove Power from Chip
                    PORT_VOICE_EN_PIN = 0;  // Voice Chip Power Off
                }
                
                FLAG_VCE_PWR_ON_DELAY_COMPLETE = 0;
                
                // Nothing to Send to Voice Chip and buffer empty
                FLAG_VOICE_ACTIVE = 0;

                // Voice Buffer free to accept a message
                FLAG_VOICE_BUSY = 0;
                break;      // Exit voice task immediately

            }
            
            // We get here only if there is a Voice to Send
            // Remove voice message from buffer, and send it now.
            voice_current_code = vce_msg_qpop();
            
        #ifdef CONFIGURE_VOICE_SERIAL    
            // For Test Only
            vce_send_serial(voice_current_code); 
            
            FLAG_VOICE_ACTIVE = 0;
        #else
            
            // TODO: Undefine Later  BC: Test Only
            #ifndef CONFIGURE_UNDEFINED
                // (sends voice to Serial Port)
                vce_send_serial(voice_current_code); 
            #endif
            
            // Voice Buffer free to accept another message
            FLAG_VOICE_BUSY = 0;

            if(VCE_PAUSE_200_MS == voice_current_code)
            {
                
                return MAIN_Make_Return_Value(VOICE_CLOCK_TIME_200_MS);
            }
            else if(VCE_PAUSE_500_MS == voice_current_code)
            {
                return MAIN_Make_Return_Value(VOICE_CLOCK_TIME_500_MS);
            }
            else
            {
                // Load voice length time for the current voice
                voice_current_length =
                        VCE_PLAY_LENGTH_TABLE[voice_current_code];
            }
                    
            FLAG_VOICE_SEND = 1;
            FLAG_VOICE_TX_ACTIVE = 1;

            // Voice Send is Active (message ready to transmit)
            // Insure Voice is Powered ON at this point
            PORT_VOICE_EN_PIN = 1;      // Voice Chip Power On

            // Indicate Voice is Active at start of Transmission
            FLAG_VOICE_ACTIVE = 1;

            //
            // Initialize the 8 bit counter and go to next state.
            //
            // Proceed to Clock states
            voice_state++;
            voice_bit_counter = 8;


            // There is a Power ON Setup Time specification
            // Delay Setup Time if voice chip just powered on
            // for this message
            if(FLAG_VCE_BUTTON_PRESS)
            {
                FLAG_VCE_BUTTON_PRESS = 0;
                FLAG_VCE_PWR_ON_DELAY_COMPLETE = 1;
                // Return Delay needed for Power Up Setup Time
                // Shorter if Button Pressed
                return MAIN_Make_Return_Value(POWERUP_VOICE_DELAY_130_MS);
            }
            else if(!FLAG_VCE_PWR_ON_DELAY_COMPLETE)
            {
                FLAG_VCE_PWR_ON_DELAY_COMPLETE = 1;

                // Return Full Delay needed for Power Up Setup Time
                return MAIN_Make_Return_Value(POWERUP_VOICE_DELAY_230_MS);

            }
            else
            {
                // Already been powered, No Setup Time required
            }
            
        #endif

        }
        break;
        

        case VCE_STATE_CLOCK_ACTIVE:
        {
            // Take Clock Signal High
            VOICE_CLOCK_HIGH

            //
            // Set next data bit.  Voice module expects the data
            // LSB first.
            //
            if(voice_current_code & (0x80>>--voice_bit_counter))
            {
                // Bit to send is a one.
                VOICE_DATA_HIGH
            }
            else
            {
                // Bit to send is a zero.
                VOICE_DATA_LOW
            }

            // Proceed to Clock inactive state
            voice_state++;
        }
        break;

        case VCE_STATE_CLOCK_INACTIVE:
        {
            if( 0 == voice_bit_counter)
            {
                // Transmission complete
                voice_state = VCE_STATE_TX_END;
                VOICE_CLOCK_LOW
            }
            else
            {
                // Clock signal Low (data clocked in on low edge)
                VOICE_CLOCK_LOW

                // Proceed to Clock Active state
                voice_state--;
            }
        }
        break;

        case VCE_STATE_TX_END:
        {
            FLAG_VOICE_TX_ACTIVE = 0;

            // Set Voice Play Timer (Timer to allow voice to
            // complete returning to Idle State
            #ifndef CONFIGURE_VOICE_SERIAL 
                voice_msg_play_timer = voice_current_length;
            #else
                // Set minimal Length time for Serial Port Output
                voice_msg_play_timer = (uchar)4;
            #endif

            voice_state = VCE_STATE_DWELL;
        }
        break;

        case VCE_STATE_DWELL:
        {
            // Dwell here to allow voice to complete
            if(0 == --voice_msg_play_timer)
            {
                voice_state = VCE_STATE_MESSAGE_COMPLETE;
                // Voice Chip has a 50 ms minimum requirement before
                //  next voice can be received
                return MAIN_Make_Return_Value(VOICE_TIME_50_MS);
            }
            else
            {
                // Voice Timer clock is in 100 ms units
                return MAIN_Make_Return_Value(VOICE_TIME_100_MS);
            }
        }

        case VCE_STATE_MESSAGE_COMPLETE:
        {
            FLAG_VOICE_SEND = 0;

            voice_state = VCE_STATE_IDLE;

            // Deactivate Input pins to voice chip
            VOICE_CLOCK_LOW
            VOICE_DATA_LOW
                    
            // When a message is completed, remain in Active Mode
            //  long enough to see if another voice msg is in the Buffer
            FLAG_ACTIVE_TIMER = 1;
            if(MAIN_ActiveTimer < TIMER_ACTIVE_MINIMUM_1_SEC)
            {
                // Extend time to remain in Active Mode
                MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_1_SEC;
            }
            
        }
        break;

    }
    return MAIN_Make_Return_Value(VOICE_CLOCK_TIME_10_MS);
}


/*
+------------------------------------------------------------------------------
| Function:      VCE_Play
+------------------------------------------------------------------------------
| Purpose:       Initiates a voice message annunciation
|                Examines voice message queue in case voice queue is full
|
+------------------------------------------------------------------------------
| Parameters:    voice - defined voice number to Play
+------------------------------------------------------------------------------
| Return Value:  TRUE if voice message accepted and pushed into queue
|                FALSE - if voice message cannot be played
+------------------------------------------------------------------------------
*/

uchar VCE_Play(uchar voice)
{
    // Disable Voice during 0/150 CO Calibration
    #ifdef CONFIGURE_CO
        uchar calstate = CO_Cal_Get_State();
        if( !( (CAL_STATE_VERIFY == calstate) ||
                (CAL_STATE_CALIBRATED == calstate) ) )
        {
            return FALSE;
        }
    #else
        if(FLAG_NO_SMOKE_CAL)
        {
            return FALSE;
        }
    #endif

    // If Voice Busy, and buffer full return False
    // 1st Examine the Voice Queue

    uchar c = vce_msg_exque();
    
    #ifdef CONFIGURE_UNDEFINED
        // Test Only
        SER_Send_String("voice que -");
        SER_Send_Int(' ',(uint)c,10);
        SER_Send_Prompt();
    #endif

    if(VCE_QUE_FULL == c)
    {
        // Buffer is Full
        // A Voice Controller is Still Busy (and no room for a new message)
        FLAG_VOICE_BUSY = 1;

        return FALSE;
    }
    else
    {
        
        //If Canadian Voice is enabled we need to monitor language select bit
        #ifdef CONFIGURE_CANADA
            if(FLAG_VOICE_FRENCH)
            {
                #ifdef CONFIGURE_2018_VOICE_CHIP                
                    // Sounds are for Both English and French
                    if( voice < VCE_SND_SONAR_PING  || 
                        voice > VCE_UNDEFINED_119)
                    {
                        // Buffer the French Select Voice message 
                        vce_msg_qpush(voice+1);
                    }
                    else
                    {
                        // Buffer the English Select Sound message
                        // VCE_FR_FOR_FRENCH_PUSH_BTN is a French Only message
                        vce_msg_qpush(voice);
                    }
                #else
                    // Sounds are for Both English and French
                    if( voice < VCE_SND_SONAR_PING)
                    {
                        // Buffer the French Select Voice message 
                        vce_msg_qpush(voice+1);
                    }
                    else
                    {
                        // Buffer the English Select Sound message
                        // VCE_FR_FOR_FRENCH_PUSH_BTN is a French Only message
                        vce_msg_qpush(voice);
                    }
                #endif
            }
            else
            {
                // Buffer the US Voice message
                vce_msg_qpush(voice);
            }

        #else
            // Buffer the US Voice message
            vce_msg_qpush(voice);
        #endif

        // Enable Clock and Data signals
        VOICE_CLOCK_ON
        VOICE_DATA_ON
                
        PORT_VOICE_EN_PIN = 1;      // Insure Voice Chip Power On
        
        if(!FLAG_MODE_ACTIVE)
        {
            // This will force 2 second timer in Active Mode 
            FLAG_ACTIVE_TIMER = 1;
        }
        
        
        if(VCE_QUE_NEAR_FULL == c)
        {
            // It's full now after Pushing another voice in queue
            // Set Voice Controller Busy (no buffer room for a new message)
            FLAG_VOICE_BUSY = 1;
        }
        return TRUE;
    }
}


/*
+------------------------------------------------------------------------------
| Function:      VCE_Announce_Number
+------------------------------------------------------------------------------
| Purpose:       Announce number passed
|
+------------------------------------------------------------------------------
| Parameters:    number - value to announce
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void VCE_Announce_Number (uchar number)
{

    switch(number)
    {
        case 0:
            number = VCE_NO_DEVICES_FOUND;
        break;

        case 1:
            number = VCE_ONE;
        break;

        case 2:
            number = VCE_TWO;                
        break;

        case 3:
            number = VCE_THREE;                
        break;

        case 4:
            number = VCE_FOUR;                
        break;

        case 5:
            number = VCE_FIVE;                
        break;

        case 6:
            number = VCE_SIX;                
        break;

        case 7:
            number = VCE_SEVEN;                
        break;

        case 8:
            number = VCE_EIGHT;                
        break;

        case 9:
            number = VCE_NINE;                
        break;

        case 10:
            number = VCE_TEN;              
        break;

        case 11:
            number = VCE_ELEVEN;                
        break;

        case 12:
            number = VCE_TWELVE;                
        break;

        case 13:
            number = VCE_THIRTEEN;                
        break;

        case 14:
            number = VCE_FOURTEEN;                
        break;

        case 15:
            number = VCE_FIFTEEN;                
        break;

        case 16:
            number = VCE_SIXTEEN;                
        break;

        case 17:
            number = VCE_SEVENTEEN;                
        break;

        case 18:
            number = VCE_EIGHTEEN;                
        break;

        case 19:
            number = VCE_NINETEEN;                
        break;

        case 20:
            number = VCE_TWENTY;                
        break;

        case 21:
            number = VCE_TWENTY_ONE;                
        break;

        case 22:
            number = VCE_TWENTY_TWO;
        break;

        case 23:
            number = VCE_TWENTY_THREE;                
        break;

        case 24:
            number = VCE_TWENTY_FOUR;                
        break;

        default:
            number = VCE_NO_DEVICES_FOUND;
        break;

    }

    if(!FLAG_VOICE_BUSY)
    {
        VCE_Play(number);
    }
}


/*
+------------------------------------------------------------------------------
| Function:      vce_msg_qinit
+------------------------------------------------------------------------------
| Purpose:       Initialize the Voice message queue
|
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void vce_msg_qinit (void)
{
    for(uchar i = 0; i < VCE_QLENGTH; i++)
    {
        VCE_Msg_Queue[i] = 0;   // Clear the Queue
        vce_msg_qin = VCE_QSTART;
        vce_msg_qout = VCE_QSTART;
    }
}


/*
+------------------------------------------------------------------------------
| Function:      vce_msg_qpop
+------------------------------------------------------------------------------
| Purpose:       Pop a byte from the voice message queue
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  return byte popped from the queue
+------------------------------------------------------------------------------
*/

uchar vce_msg_qpop(void)
{
    uchar c = VCE_Msg_Queue[vce_msg_qout];

    if(VCE_QEND == vce_msg_qout)
    {
        // Wrap to Start of Queue
        vce_msg_qout = VCE_QSTART;
    }
    else
    {
        // Next Queue location
        vce_msg_qout++;
    }

    return c;
}


/*
+------------------------------------------------------------------------------
| Function:      vce_msg_qpush
+------------------------------------------------------------------------------
| Purpose:       Push byte into the voice message queue
|
+------------------------------------------------------------------------------
| Parameters:    c = byte to push into queue
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void vce_msg_qpush(uchar c)
{
    VCE_Msg_Queue[vce_msg_qin] = c;
    if(VCE_QEND == vce_msg_qin)
    {
        // Wrap to Start of Queue
        vce_msg_qin = VCE_QSTART;
    }
    else
    {
        // Next Queue location
        vce_msg_qin++;
    }
}


/*
+------------------------------------------------------------------------------
| Function:      vce_msg_exque
+------------------------------------------------------------------------------
| Purpose:       Return Status of voice Message Queue
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  return # of chars in Queue, 0 = empty
+------------------------------------------------------------------------------
*/

uchar vce_msg_exque(void)
{
    uchar s;

    if(vce_msg_qin == vce_msg_qout)
    {
        s = 0;
    }
    else if(vce_msg_qin > vce_msg_qout)
    {
        s = (vce_msg_qin - vce_msg_qout);
    }
    else
    {
        // QIN must have wrapped so add in VCE_QLENGTH to calc. # of chars
        //   in Que
        s = ((vce_msg_qin + VCE_QLENGTH) - vce_msg_qout);

    }

    return s;

}



#ifndef CONFIGURE_UNDEFINED
        
/*
+------------------------------------------------------------------------------
| Function:      vce_send_serial
+------------------------------------------------------------------------------
| Purpose:       Outputs Voice Message via Serial Port for testing
|
+------------------------------------------------------------------------------
| Parameters:    voice - voice ID to output 
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
        
void vce_send_serial(uchar voice)     
{
    if(FLAG_VOICE_FRENCH)
    {
        
        #ifdef CONFIGURE_2018_VOICE_CHIP                
            // Sounds are for Both English and French
            if( voice < VCE_SND_SONAR_PING  || 
                voice > VCE_UNDEFINED_119)
            {
                // French Voice but output in English 
                voice--;
            }
        #else
            // Sounds are for Both English and French
            if( voice < VCE_SND_SONAR_PING )
            {
                // French Voice but output in English 
                voice--;
            }
        #endif
    }
    
    switch(voice)
    {

        case VCE_FIRE_ENGLISH_1:
            SER_Send_String("Fire");
            break;
        case VCE_CO_ENGLISH:
            SER_Send_String("Warning, Carbon Monoxide");
            break;
        case VCE_HUSH_ACTIVATED:
            SER_Send_String("Hush Mode Activated");
            break;
        case VCE_HUSH_CANCELLED:
            SER_Send_String("Hush Mode Canceled");
            break;
        case VCE_CO_PREVIOUSLY:
            SER_Send_String("Carbon Monoxide Previously Detected");
            break;
        case VCE_NETWORK_SEARCH:
            SER_Send_String("Searching for Other Devices");
            break;
        case VCE_SMK_PREVIOUSLY:
            SER_Send_String("Smoke Previously Detected");
            break;
        case VCE_ERROR_SEE_GUIDE:
            SER_Send_String("Error, See Troubleshooting Guide");
            break;
        case VCE_REPLACE_ALARM:
            SER_Send_String("Replace Alarm");
            break;
        case VCE_BUTTON_TO_SILENCE:
            SER_Send_String("Press Button to Silence");
            break;
        case VCE_WEATHER_ALERT:
            SER_Send_String("Weather Emergency in vicinity");
            break;
        case VCE_NETWORK_CONN_LOST:
            SER_Send_String("Connection Lost");
            break;
        case VCE_TOO_MUCH_SMOKE:
            SER_Send_String("Too Much Smoke, alarm cannot be hushed");
            break;
        case VCE_EMERGENCY:
            SER_Send_String("Emergency");
            break;
        case VCE_WATER_LEAK:
            SER_Send_String("Water Leak Detected");
            break;
        case VCE_EXPLOSIVE_GAS:
            SER_Send_String("Explosive Gas Detected");
            break;
        case VCE_TEMP_DROP:
            SER_Send_String("Temperature Drop Detected");
            break;
        case VCE_INTRUDER_ALERT:
            SER_Send_String("Intruder Detected");
            break;
        case VCE_SND_BTN_PRESS:
            SER_Send_String("SND Button Pressed");
            break;
        case VCE_SND_NETWORK_CLOSE:
            SER_Send_String("SND Network Closed");
            break;
        case VCE_SND_NETWORK_ERROR:
            SER_Send_String("SND Network Error");
            break;
        case VCE_SND_POWER_ON:
            SER_Send_String("SND Power On");
            break;
        case VCE_SND_PUSH_TO_TEST:
            SER_Send_String("SND PWR_On, Soft_RST or Bat_ACT");
            break;
        case VCE_SND_RETURN_TO_OOB:
            SER_Send_String("SND Return to OOB");
            break;

        // New Voices
        case VCE_ONE:
            SER_Send_String("One");
        break;

        case VCE_TWO:
            SER_Send_String("Two");
        break;

        case VCE_THREE:
            SER_Send_String("Three");
        break;

        case VCE_FOUR:
            SER_Send_String("Four");
        break;

        case VCE_FIVE:
            SER_Send_String("Five");
        break;

        case VCE_SIX:
            SER_Send_String("Six");
        break;

        case VCE_SEVEN:
            SER_Send_String("Seven");
        break;

        case VCE_EIGHT:
            SER_Send_String("Eight");
        break;

        case VCE_NINE:
            SER_Send_String("Nine");
        break;

        case VCE_TEN:
            SER_Send_String("Ten");
        break;

        case VCE_ELEVEN:
            SER_Send_String("11");
        break;

        case VCE_TWELVE:
            SER_Send_String("12");
        break;

        case VCE_THIRTEEN:
            SER_Send_String("13");
        break;

        case VCE_FOURTEEN:
            SER_Send_String("14");
        break;

        case VCE_FIFTEEN:
            SER_Send_String("15");
        break;

        case VCE_SIXTEEN:
            SER_Send_String("16");
        break;

        case VCE_SEVENTEEN:
            SER_Send_String("17");
        break;

        case VCE_EIGHTEEN:
            SER_Send_String("18");
        break;

        case VCE_NINETEEN:
            SER_Send_String("19");
        break;

        case VCE_TWENTY:
            SER_Send_String("20");
        break;

        case VCE_TWENTY_ONE:
            SER_Send_String("21");
        break;

        case VCE_TWENTY_TWO:
            SER_Send_String("22");
        break;

        case VCE_TWENTY_THREE:
            SER_Send_String("23");
        break;

        case VCE_TWENTY_FOUR:
            SER_Send_String("24");
        break;

        case VCE_SND_SONAR_PING:
            SER_Send_String("SND Sonar Ping");
        break;
        case VCE_RESET_WIRELESS_SETTINGS:
            SER_Send_String("Resetting Wireless Settings");
        break;
        case VCE_TEST_CANCELLED:
            SER_Send_String("Test Canceled");
        break;
        case VCE_TEMP_SILENCED:
            SER_Send_String("Temporarily Silenced");
        break;
        case VCE_TESTING_AND_LOUD:
            SER_Send_String("Testing - this is very loud");
        break;
        case VCE_PRESS_TO_CANCEL_TEST:
            SER_Send_String("Press Now to Cancel Test");
        break;
        case VCE_READY_TO_CONNECT:
            SER_Send_String
                    ("Ready to Connect, Follow Quick Start Instructions");
        break;
        case VCE_SETUP_COMPLETE:
            SER_Send_String("Setup Complete");
        break;
        case VCE_SUCCESS:
            SER_Send_String("Success");
        break;

        case VCE_DEVICES_CONNECTED:
            SER_Send_String(" Devices Connected");
        break;
        case VCE_NOW_CONNECTED:
            SER_Send_String("Now Connected");
        break;
        case VCE_NOT_CONNECTED:
            SER_Send_String("Not Connected");
        break;
        case VCE_NO_DEVICES_FOUND:
            SER_Send_String("No Devices Found");
        break;
        case VCE_ENGLISH_SELECTED:
            if(!FLAG_VOICE_FRENCH)
            {
                SER_Send_String("English Selected");
            }
            else
            {
                SER_Send_String("French Selected ");
            }
        break;
        case VCE_TEST_COMPLETE:
            SER_Send_String("Test Complete");
        break;

#ifdef CONFIGURE_2018_VOICE_CHIP
        case VCE_ACTIVATE_BATTERY:
            SER_Send_String("Activate Battery");       
        break;
        
        case VCE_FR_FOR_FRENCH_PUSH_BTN:
        {
            if(!FLAG_VOICE_FRENCH)
            {
                SER_Send_String("For French, Press Button Twice");
            }
            else
            {
                SER_Send_String("For English, Press Button Twice");
            }
        }
        break;
        
#else
        case VCE_FR_FOR_FRENCH_PUSH_BTN:
            SER_Send_String("For French, Press Button Twice");       
        break;
        
#endif        

        case VCE_PAUSE_500_MS:
            break;
        case VCE_PAUSE_200_MS:
            break;

        default:
            SER_Send_Int('?',voice,16);
            SER_Send_Prompt();
            SER_Send_String("Voice Message Undefined");
            break;
    } 

    if( (voice >= VCE_ONE) &&
        (voice <= VCE_TWENTY_FOUR) )
    {
        // No Prompt
    }
    else
    {
        SER_Send_Prompt();
    }
}    

#endif
        

#else   // Voice not Configured

    // No Voice option function placeholders
    uint VCE_Process_Task(void)
    {
        return MAIN_Make_Return_Value(VOICE_TIME_61_SECONDS);
    }

    uchar VCE_Play(uchar)
    {
        return (FALSE);
    }
    
    void VCE_Announce_Number (uchar)
    {
        
    }

#endif




