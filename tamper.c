/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// tamper.c
// Functions for anti-tamper switch feature
#define	_BATTERY_C

#include    "common.h"
#include    "tamper.h"
#include    "diaghist.h"


#include    "sound.h"


#ifdef _CONFIGURE_TAMPER_SWITCH


#define	TAMPER_STATE_IDLE	0
#define	TAMPER_STATE_DEBOUNCE   1
#define	TAMPER_STATE_ACTIVE	2
#define	TAMPER_STATE_ALERT	3

#define TAMPER_ALERT_TIME       120     // 2 minutes, in seconds
#define TAMPER_INTERVAL_1_SEC   1000    // 1 second, in ms


// Local variables
static UCHAR bTamperState = TAMPER_STATE_IDLE;
UCHAR bTamperTimer;

void Tamper_Init(void)
{
    // If Serial Port was detected, disable Tamper feature
    // as Serial Tx and Tamper share same I/O pin.
    if(FLAG_SERIAL_PORT_ACTIVE == 1)
    {
        FLAG_TAMPER_DISABLED = 1;
    }
    else
    {
        // I/O is used for Tamper Sw Input
        FLAG_TAMPER_DISABLED = 0;
        bTamperTimer = TAMPER_ALERT_TIME ;
    }
}


UINT Tamper_Process(void)
{

     switch (bTamperState)
    {
        case	TAMPER_STATE_IDLE:
            //SER_Send_String("T0") ;

            FLAG_TAMPER_ACTIVE = 0;

            // Two minute Tamper Timer must be expired to enable Tamper Switch
            if(--bTamperTimer == 0)
            {
                // Prevent Timer from wrapping below Zero
                bTamperTimer = 1;

                if(FLAG_LOW_BATTERY == 0)
                {

                    if( (FLAG_CALIBRATION_COMPLETE == TRUE) && (FLAG_CALIBRATION_UNTESTED == FALSE) )
                    {
                        if(PORT_TAMPER_SW == 1)
                        {
                            bTamperState = TAMPER_STATE_DEBOUNCE ;
                        }
                     }
                }
            }
            break ;

        case    TAMPER_STATE_DEBOUNCE:
                //SER_Send_String("T1") ;
                // See if button is still depressed
                if (PORT_TAMPER_SW == 1)
                {
                    // Switch is debounced.  Start timer and transition to wait
                    // for button to be released.
                    //bTamperTimer = TAMPER_ALERT_TIME ;
                    bTamperState = TAMPER_STATE_ACTIVE ;

                    // Here we add a Tamper Switch Activate Beep
                    SND_Do_Chirp();

                }
                else
                {
                    // Button is up, go back and wait for another button press.
                    bTamperState = TAMPER_STATE_IDLE ;
                }
                break ;

         case    TAMPER_STATE_ACTIVE:
             //SER_Send_String("T2") ;

             FLAG_TAMPER_ACTIVE = 1;

             // Timeout, proceed to Tamper Alert
             bTamperState = TAMPER_STATE_ALERT ;

             #ifdef _CONFIGURE_DIAG_HIST
                 // Only Record if entering LB
                 Diag_Hist_Que_Push(HIST_TAMPER_EVENT);
             #endif

             if (PORT_TAMPER_SW == 0)
             {
                // Switch is inactive, go back and wait for another
                //  Tamper Switch activation
                    bTamperState = TAMPER_STATE_IDLE ;
             }
             break;

         case    TAMPER_STATE_ALERT:
            //SER_Send_String("T3") ;

            if (PORT_TAMPER_SW == 0)
             {
                // Switch is inactive, go back and wait for another
                //  Tamper Switch activation
                bTamperState = TAMPER_STATE_IDLE ;
                HORN_OFF
             }
             else
             {
                // Limit when the Tamper Alert is On
                if((FLAG_CO_ALARM_ACTIVE) || ((FLAG_LOW_BATTERY) && (FLAG_LB_HUSH_ACTIVE == 0))
                        || (FLAG_FAULT_FATAL ) || (FLAG_EOL_MODE && FLAG_EOL_HUSH_ACTIVE == 0)
                        || (FLAG_BUTTON_NOT_IDLE || FLAG_PTT_ACTIVE))
                {
                    // No Horn
                }
                else
                {
                    HORN_ON       // Sound Horn continuously for Tamper Alert
                }
             }
             break;

     }
     return Make_Return_Value(TAMPER_INTERVAL_1_SEC) ;
}






#endif

