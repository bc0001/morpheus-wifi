#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-2581_WiFi_Production.mk)" "nbproject/Makefile-local-2581_WiFi_Production.mk"
include nbproject/Makefile-local-2581_WiFi_Production.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=2581_WiFi_Production
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/morpheus-wifi.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/morpheus-wifi.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=main.c coalarm.c systemdata.c serial.c diaghist.c cocalibrate.c fault.c sound.c comeasure.c button.c ptt.c a2d.c alarmprio.c cocompute.c life.c command_proc.c battery.c esclight.c led.c photosmoke.c algorithm.c interconnect.c light.c strobe.c voice.c vboost.c cci_phy.c app.c alarmmp.c memory_18877.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/main.p1 ${OBJECTDIR}/coalarm.p1 ${OBJECTDIR}/systemdata.p1 ${OBJECTDIR}/serial.p1 ${OBJECTDIR}/diaghist.p1 ${OBJECTDIR}/cocalibrate.p1 ${OBJECTDIR}/fault.p1 ${OBJECTDIR}/sound.p1 ${OBJECTDIR}/comeasure.p1 ${OBJECTDIR}/button.p1 ${OBJECTDIR}/ptt.p1 ${OBJECTDIR}/a2d.p1 ${OBJECTDIR}/alarmprio.p1 ${OBJECTDIR}/cocompute.p1 ${OBJECTDIR}/life.p1 ${OBJECTDIR}/command_proc.p1 ${OBJECTDIR}/battery.p1 ${OBJECTDIR}/esclight.p1 ${OBJECTDIR}/led.p1 ${OBJECTDIR}/photosmoke.p1 ${OBJECTDIR}/algorithm.p1 ${OBJECTDIR}/interconnect.p1 ${OBJECTDIR}/light.p1 ${OBJECTDIR}/strobe.p1 ${OBJECTDIR}/voice.p1 ${OBJECTDIR}/vboost.p1 ${OBJECTDIR}/cci_phy.p1 ${OBJECTDIR}/app.p1 ${OBJECTDIR}/alarmmp.p1 ${OBJECTDIR}/memory_18877.p1
POSSIBLE_DEPFILES=${OBJECTDIR}/main.p1.d ${OBJECTDIR}/coalarm.p1.d ${OBJECTDIR}/systemdata.p1.d ${OBJECTDIR}/serial.p1.d ${OBJECTDIR}/diaghist.p1.d ${OBJECTDIR}/cocalibrate.p1.d ${OBJECTDIR}/fault.p1.d ${OBJECTDIR}/sound.p1.d ${OBJECTDIR}/comeasure.p1.d ${OBJECTDIR}/button.p1.d ${OBJECTDIR}/ptt.p1.d ${OBJECTDIR}/a2d.p1.d ${OBJECTDIR}/alarmprio.p1.d ${OBJECTDIR}/cocompute.p1.d ${OBJECTDIR}/life.p1.d ${OBJECTDIR}/command_proc.p1.d ${OBJECTDIR}/battery.p1.d ${OBJECTDIR}/esclight.p1.d ${OBJECTDIR}/led.p1.d ${OBJECTDIR}/photosmoke.p1.d ${OBJECTDIR}/algorithm.p1.d ${OBJECTDIR}/interconnect.p1.d ${OBJECTDIR}/light.p1.d ${OBJECTDIR}/strobe.p1.d ${OBJECTDIR}/voice.p1.d ${OBJECTDIR}/vboost.p1.d ${OBJECTDIR}/cci_phy.p1.d ${OBJECTDIR}/app.p1.d ${OBJECTDIR}/alarmmp.p1.d ${OBJECTDIR}/memory_18877.p1.d

# Object Files
OBJECTFILES=${OBJECTDIR}/main.p1 ${OBJECTDIR}/coalarm.p1 ${OBJECTDIR}/systemdata.p1 ${OBJECTDIR}/serial.p1 ${OBJECTDIR}/diaghist.p1 ${OBJECTDIR}/cocalibrate.p1 ${OBJECTDIR}/fault.p1 ${OBJECTDIR}/sound.p1 ${OBJECTDIR}/comeasure.p1 ${OBJECTDIR}/button.p1 ${OBJECTDIR}/ptt.p1 ${OBJECTDIR}/a2d.p1 ${OBJECTDIR}/alarmprio.p1 ${OBJECTDIR}/cocompute.p1 ${OBJECTDIR}/life.p1 ${OBJECTDIR}/command_proc.p1 ${OBJECTDIR}/battery.p1 ${OBJECTDIR}/esclight.p1 ${OBJECTDIR}/led.p1 ${OBJECTDIR}/photosmoke.p1 ${OBJECTDIR}/algorithm.p1 ${OBJECTDIR}/interconnect.p1 ${OBJECTDIR}/light.p1 ${OBJECTDIR}/strobe.p1 ${OBJECTDIR}/voice.p1 ${OBJECTDIR}/vboost.p1 ${OBJECTDIR}/cci_phy.p1 ${OBJECTDIR}/app.p1 ${OBJECTDIR}/alarmmp.p1 ${OBJECTDIR}/memory_18877.p1

# Source Files
SOURCEFILES=main.c coalarm.c systemdata.c serial.c diaghist.c cocalibrate.c fault.c sound.c comeasure.c button.c ptt.c a2d.c alarmprio.c cocompute.c life.c command_proc.c battery.c esclight.c led.c photosmoke.c algorithm.c interconnect.c light.c strobe.c voice.c vboost.c cci_phy.c app.c alarmmp.c memory_18877.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-2581_WiFi_Production.mk dist/${CND_CONF}/${IMAGE_TYPE}/morpheus-wifi.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=16LF18877
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.p1.d 
	@${RM} ${OBJECTDIR}/main.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/main.p1 main.c 
	@-${MV} ${OBJECTDIR}/main.d ${OBJECTDIR}/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/coalarm.p1: coalarm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/coalarm.p1.d 
	@${RM} ${OBJECTDIR}/coalarm.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/coalarm.p1 coalarm.c 
	@-${MV} ${OBJECTDIR}/coalarm.d ${OBJECTDIR}/coalarm.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/coalarm.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/systemdata.p1: systemdata.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/systemdata.p1.d 
	@${RM} ${OBJECTDIR}/systemdata.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/systemdata.p1 systemdata.c 
	@-${MV} ${OBJECTDIR}/systemdata.d ${OBJECTDIR}/systemdata.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/systemdata.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/serial.p1: serial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/serial.p1.d 
	@${RM} ${OBJECTDIR}/serial.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/serial.p1 serial.c 
	@-${MV} ${OBJECTDIR}/serial.d ${OBJECTDIR}/serial.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/serial.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/diaghist.p1: diaghist.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/diaghist.p1.d 
	@${RM} ${OBJECTDIR}/diaghist.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/diaghist.p1 diaghist.c 
	@-${MV} ${OBJECTDIR}/diaghist.d ${OBJECTDIR}/diaghist.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/diaghist.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/cocalibrate.p1: cocalibrate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/cocalibrate.p1.d 
	@${RM} ${OBJECTDIR}/cocalibrate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/cocalibrate.p1 cocalibrate.c 
	@-${MV} ${OBJECTDIR}/cocalibrate.d ${OBJECTDIR}/cocalibrate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/cocalibrate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/fault.p1: fault.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/fault.p1.d 
	@${RM} ${OBJECTDIR}/fault.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/fault.p1 fault.c 
	@-${MV} ${OBJECTDIR}/fault.d ${OBJECTDIR}/fault.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/fault.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sound.p1: sound.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/sound.p1.d 
	@${RM} ${OBJECTDIR}/sound.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/sound.p1 sound.c 
	@-${MV} ${OBJECTDIR}/sound.d ${OBJECTDIR}/sound.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sound.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/comeasure.p1: comeasure.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/comeasure.p1.d 
	@${RM} ${OBJECTDIR}/comeasure.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/comeasure.p1 comeasure.c 
	@-${MV} ${OBJECTDIR}/comeasure.d ${OBJECTDIR}/comeasure.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/comeasure.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/button.p1: button.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/button.p1.d 
	@${RM} ${OBJECTDIR}/button.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/button.p1 button.c 
	@-${MV} ${OBJECTDIR}/button.d ${OBJECTDIR}/button.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/button.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ptt.p1: ptt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/ptt.p1.d 
	@${RM} ${OBJECTDIR}/ptt.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/ptt.p1 ptt.c 
	@-${MV} ${OBJECTDIR}/ptt.d ${OBJECTDIR}/ptt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ptt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/a2d.p1: a2d.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/a2d.p1.d 
	@${RM} ${OBJECTDIR}/a2d.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/a2d.p1 a2d.c 
	@-${MV} ${OBJECTDIR}/a2d.d ${OBJECTDIR}/a2d.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/a2d.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/alarmprio.p1: alarmprio.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/alarmprio.p1.d 
	@${RM} ${OBJECTDIR}/alarmprio.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/alarmprio.p1 alarmprio.c 
	@-${MV} ${OBJECTDIR}/alarmprio.d ${OBJECTDIR}/alarmprio.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/alarmprio.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/cocompute.p1: cocompute.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/cocompute.p1.d 
	@${RM} ${OBJECTDIR}/cocompute.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/cocompute.p1 cocompute.c 
	@-${MV} ${OBJECTDIR}/cocompute.d ${OBJECTDIR}/cocompute.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/cocompute.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/life.p1: life.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/life.p1.d 
	@${RM} ${OBJECTDIR}/life.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/life.p1 life.c 
	@-${MV} ${OBJECTDIR}/life.d ${OBJECTDIR}/life.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/life.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/command_proc.p1: command_proc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/command_proc.p1.d 
	@${RM} ${OBJECTDIR}/command_proc.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/command_proc.p1 command_proc.c 
	@-${MV} ${OBJECTDIR}/command_proc.d ${OBJECTDIR}/command_proc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/command_proc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/battery.p1: battery.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/battery.p1.d 
	@${RM} ${OBJECTDIR}/battery.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/battery.p1 battery.c 
	@-${MV} ${OBJECTDIR}/battery.d ${OBJECTDIR}/battery.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/battery.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/esclight.p1: esclight.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/esclight.p1.d 
	@${RM} ${OBJECTDIR}/esclight.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/esclight.p1 esclight.c 
	@-${MV} ${OBJECTDIR}/esclight.d ${OBJECTDIR}/esclight.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/esclight.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/led.p1: led.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/led.p1.d 
	@${RM} ${OBJECTDIR}/led.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/led.p1 led.c 
	@-${MV} ${OBJECTDIR}/led.d ${OBJECTDIR}/led.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/led.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/photosmoke.p1: photosmoke.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/photosmoke.p1.d 
	@${RM} ${OBJECTDIR}/photosmoke.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/photosmoke.p1 photosmoke.c 
	@-${MV} ${OBJECTDIR}/photosmoke.d ${OBJECTDIR}/photosmoke.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/photosmoke.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/algorithm.p1: algorithm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/algorithm.p1.d 
	@${RM} ${OBJECTDIR}/algorithm.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/algorithm.p1 algorithm.c 
	@-${MV} ${OBJECTDIR}/algorithm.d ${OBJECTDIR}/algorithm.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/algorithm.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/interconnect.p1: interconnect.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/interconnect.p1.d 
	@${RM} ${OBJECTDIR}/interconnect.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/interconnect.p1 interconnect.c 
	@-${MV} ${OBJECTDIR}/interconnect.d ${OBJECTDIR}/interconnect.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/interconnect.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/light.p1: light.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/light.p1.d 
	@${RM} ${OBJECTDIR}/light.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/light.p1 light.c 
	@-${MV} ${OBJECTDIR}/light.d ${OBJECTDIR}/light.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/light.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/strobe.p1: strobe.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/strobe.p1.d 
	@${RM} ${OBJECTDIR}/strobe.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/strobe.p1 strobe.c 
	@-${MV} ${OBJECTDIR}/strobe.d ${OBJECTDIR}/strobe.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/strobe.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/voice.p1: voice.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/voice.p1.d 
	@${RM} ${OBJECTDIR}/voice.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/voice.p1 voice.c 
	@-${MV} ${OBJECTDIR}/voice.d ${OBJECTDIR}/voice.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/voice.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/vboost.p1: vboost.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/vboost.p1.d 
	@${RM} ${OBJECTDIR}/vboost.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/vboost.p1 vboost.c 
	@-${MV} ${OBJECTDIR}/vboost.d ${OBJECTDIR}/vboost.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/vboost.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/cci_phy.p1: cci_phy.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/cci_phy.p1.d 
	@${RM} ${OBJECTDIR}/cci_phy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/cci_phy.p1 cci_phy.c 
	@-${MV} ${OBJECTDIR}/cci_phy.d ${OBJECTDIR}/cci_phy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/cci_phy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/app.p1: app.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/app.p1.d 
	@${RM} ${OBJECTDIR}/app.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/app.p1 app.c 
	@-${MV} ${OBJECTDIR}/app.d ${OBJECTDIR}/app.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/app.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/alarmmp.p1: alarmmp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/alarmmp.p1.d 
	@${RM} ${OBJECTDIR}/alarmmp.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/alarmmp.p1 alarmmp.c 
	@-${MV} ${OBJECTDIR}/alarmmp.d ${OBJECTDIR}/alarmmp.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/alarmmp.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/memory_18877.p1: memory_18877.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/memory_18877.p1.d 
	@${RM} ${OBJECTDIR}/memory_18877.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/memory_18877.p1 memory_18877.c 
	@-${MV} ${OBJECTDIR}/memory_18877.d ${OBJECTDIR}/memory_18877.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/memory_18877.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.p1.d 
	@${RM} ${OBJECTDIR}/main.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/main.p1 main.c 
	@-${MV} ${OBJECTDIR}/main.d ${OBJECTDIR}/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/coalarm.p1: coalarm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/coalarm.p1.d 
	@${RM} ${OBJECTDIR}/coalarm.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/coalarm.p1 coalarm.c 
	@-${MV} ${OBJECTDIR}/coalarm.d ${OBJECTDIR}/coalarm.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/coalarm.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/systemdata.p1: systemdata.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/systemdata.p1.d 
	@${RM} ${OBJECTDIR}/systemdata.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/systemdata.p1 systemdata.c 
	@-${MV} ${OBJECTDIR}/systemdata.d ${OBJECTDIR}/systemdata.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/systemdata.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/serial.p1: serial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/serial.p1.d 
	@${RM} ${OBJECTDIR}/serial.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/serial.p1 serial.c 
	@-${MV} ${OBJECTDIR}/serial.d ${OBJECTDIR}/serial.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/serial.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/diaghist.p1: diaghist.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/diaghist.p1.d 
	@${RM} ${OBJECTDIR}/diaghist.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/diaghist.p1 diaghist.c 
	@-${MV} ${OBJECTDIR}/diaghist.d ${OBJECTDIR}/diaghist.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/diaghist.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/cocalibrate.p1: cocalibrate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/cocalibrate.p1.d 
	@${RM} ${OBJECTDIR}/cocalibrate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/cocalibrate.p1 cocalibrate.c 
	@-${MV} ${OBJECTDIR}/cocalibrate.d ${OBJECTDIR}/cocalibrate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/cocalibrate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/fault.p1: fault.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/fault.p1.d 
	@${RM} ${OBJECTDIR}/fault.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/fault.p1 fault.c 
	@-${MV} ${OBJECTDIR}/fault.d ${OBJECTDIR}/fault.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/fault.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sound.p1: sound.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/sound.p1.d 
	@${RM} ${OBJECTDIR}/sound.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/sound.p1 sound.c 
	@-${MV} ${OBJECTDIR}/sound.d ${OBJECTDIR}/sound.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sound.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/comeasure.p1: comeasure.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/comeasure.p1.d 
	@${RM} ${OBJECTDIR}/comeasure.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/comeasure.p1 comeasure.c 
	@-${MV} ${OBJECTDIR}/comeasure.d ${OBJECTDIR}/comeasure.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/comeasure.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/button.p1: button.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/button.p1.d 
	@${RM} ${OBJECTDIR}/button.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/button.p1 button.c 
	@-${MV} ${OBJECTDIR}/button.d ${OBJECTDIR}/button.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/button.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ptt.p1: ptt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/ptt.p1.d 
	@${RM} ${OBJECTDIR}/ptt.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/ptt.p1 ptt.c 
	@-${MV} ${OBJECTDIR}/ptt.d ${OBJECTDIR}/ptt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ptt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/a2d.p1: a2d.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/a2d.p1.d 
	@${RM} ${OBJECTDIR}/a2d.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/a2d.p1 a2d.c 
	@-${MV} ${OBJECTDIR}/a2d.d ${OBJECTDIR}/a2d.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/a2d.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/alarmprio.p1: alarmprio.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/alarmprio.p1.d 
	@${RM} ${OBJECTDIR}/alarmprio.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/alarmprio.p1 alarmprio.c 
	@-${MV} ${OBJECTDIR}/alarmprio.d ${OBJECTDIR}/alarmprio.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/alarmprio.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/cocompute.p1: cocompute.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/cocompute.p1.d 
	@${RM} ${OBJECTDIR}/cocompute.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/cocompute.p1 cocompute.c 
	@-${MV} ${OBJECTDIR}/cocompute.d ${OBJECTDIR}/cocompute.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/cocompute.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/life.p1: life.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/life.p1.d 
	@${RM} ${OBJECTDIR}/life.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/life.p1 life.c 
	@-${MV} ${OBJECTDIR}/life.d ${OBJECTDIR}/life.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/life.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/command_proc.p1: command_proc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/command_proc.p1.d 
	@${RM} ${OBJECTDIR}/command_proc.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/command_proc.p1 command_proc.c 
	@-${MV} ${OBJECTDIR}/command_proc.d ${OBJECTDIR}/command_proc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/command_proc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/battery.p1: battery.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/battery.p1.d 
	@${RM} ${OBJECTDIR}/battery.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/battery.p1 battery.c 
	@-${MV} ${OBJECTDIR}/battery.d ${OBJECTDIR}/battery.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/battery.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/esclight.p1: esclight.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/esclight.p1.d 
	@${RM} ${OBJECTDIR}/esclight.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/esclight.p1 esclight.c 
	@-${MV} ${OBJECTDIR}/esclight.d ${OBJECTDIR}/esclight.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/esclight.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/led.p1: led.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/led.p1.d 
	@${RM} ${OBJECTDIR}/led.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/led.p1 led.c 
	@-${MV} ${OBJECTDIR}/led.d ${OBJECTDIR}/led.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/led.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/photosmoke.p1: photosmoke.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/photosmoke.p1.d 
	@${RM} ${OBJECTDIR}/photosmoke.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/photosmoke.p1 photosmoke.c 
	@-${MV} ${OBJECTDIR}/photosmoke.d ${OBJECTDIR}/photosmoke.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/photosmoke.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/algorithm.p1: algorithm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/algorithm.p1.d 
	@${RM} ${OBJECTDIR}/algorithm.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/algorithm.p1 algorithm.c 
	@-${MV} ${OBJECTDIR}/algorithm.d ${OBJECTDIR}/algorithm.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/algorithm.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/interconnect.p1: interconnect.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/interconnect.p1.d 
	@${RM} ${OBJECTDIR}/interconnect.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/interconnect.p1 interconnect.c 
	@-${MV} ${OBJECTDIR}/interconnect.d ${OBJECTDIR}/interconnect.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/interconnect.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/light.p1: light.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/light.p1.d 
	@${RM} ${OBJECTDIR}/light.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/light.p1 light.c 
	@-${MV} ${OBJECTDIR}/light.d ${OBJECTDIR}/light.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/light.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/strobe.p1: strobe.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/strobe.p1.d 
	@${RM} ${OBJECTDIR}/strobe.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/strobe.p1 strobe.c 
	@-${MV} ${OBJECTDIR}/strobe.d ${OBJECTDIR}/strobe.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/strobe.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/voice.p1: voice.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/voice.p1.d 
	@${RM} ${OBJECTDIR}/voice.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/voice.p1 voice.c 
	@-${MV} ${OBJECTDIR}/voice.d ${OBJECTDIR}/voice.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/voice.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/vboost.p1: vboost.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/vboost.p1.d 
	@${RM} ${OBJECTDIR}/vboost.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/vboost.p1 vboost.c 
	@-${MV} ${OBJECTDIR}/vboost.d ${OBJECTDIR}/vboost.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/vboost.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/cci_phy.p1: cci_phy.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/cci_phy.p1.d 
	@${RM} ${OBJECTDIR}/cci_phy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/cci_phy.p1 cci_phy.c 
	@-${MV} ${OBJECTDIR}/cci_phy.d ${OBJECTDIR}/cci_phy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/cci_phy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/app.p1: app.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/app.p1.d 
	@${RM} ${OBJECTDIR}/app.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/app.p1 app.c 
	@-${MV} ${OBJECTDIR}/app.d ${OBJECTDIR}/app.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/app.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/alarmmp.p1: alarmmp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/alarmmp.p1.d 
	@${RM} ${OBJECTDIR}/alarmmp.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/alarmmp.p1 alarmmp.c 
	@-${MV} ${OBJECTDIR}/alarmmp.d ${OBJECTDIR}/alarmmp.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/alarmmp.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/memory_18877.p1: memory_18877.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/memory_18877.p1.d 
	@${RM} ${OBJECTDIR}/memory_18877.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/memory_18877.p1 memory_18877.c 
	@-${MV} ${OBJECTDIR}/memory_18877.d ${OBJECTDIR}/memory_18877.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/memory_18877.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/morpheus-wifi.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -Wl,-Map=dist/${CND_CONF}/${IMAGE_TYPE}/morpheus-wifi.${IMAGE_TYPE}.map  -D__DEBUG=1  -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -Wl,--defsym=__MPLAB_BUILD=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib -std=c90 -gdwarf-3 -mstack=compiled:auto:auto  --MSGDISABLE=1510        $(COMPARISON_BUILD) -Wl,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -o dist/${CND_CONF}/${IMAGE_TYPE}/morpheus-wifi.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/morpheus-wifi.${IMAGE_TYPE}.hex 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/morpheus-wifi.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -Wl,-Map=dist/${CND_CONF}/${IMAGE_TYPE}/morpheus-wifi.${IMAGE_TYPE}.map  -DXPRJ_2581_WiFi_Production=$(CND_CONF)  -Wl,--defsym=__MPLAB_BUILD=1  -fno-short-double -fno-short-float -mram=default -Os -fasmfile -Og -maddrqual=require -DCONFIGURE_PRODUCTION_UNIT -DCONFIGURE_2581_US -xassembler-with-cpp -I"../../../Program Files (x86)/Microchip/xc8/v2.05/pic/include" -mwarn=0 -Wa,-a -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib -std=c90 -gdwarf-3 -mstack=compiled:auto:auto  --MSGDISABLE=1510     $(COMPARISON_BUILD) -Wl,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -o dist/${CND_CONF}/${IMAGE_TYPE}/morpheus-wifi.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/2581_WiFi_Production
	${RM} -r dist/2581_WiFi_Production

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
