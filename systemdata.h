/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        systemdata.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for systemdata.c module
|               Contains the system data structure
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


#ifndef	SYSTEMDATA_H
#define SYSTEMDATA_H


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
    #include "diaghist.h"
    #include "battery.h"


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/
    // Offsets within Non-Volatile Data
    #define PRIMARY_START  0x00
    #define BACKUP_START   0x10

    #ifdef	CONFIGURE_MANUAL_CO_CAL

        #define CO_STATUS_INIT_VAL          0x03
        #define CO_CHECKSUM_INIT_VAL		0xfC
        #define CO_OFFSET_LSB_INIT_VAL		0x78
        #define CO_OFFSET_MSB_INIT_VAL		0x00
        #define CO_SCALE_LSB_INIT_VAL		0x02
        #define CO_SCALE_MSB_INIT_VAL		0x01

    #else
        #define CO_STATUS_INIT_VAL              0x00
        #define CO_CHECKSUM_INIT_VAL            0xFF
        #define CO_OFFSET_LSB_INIT_VAL          0x00
        #define CO_OFFSET_MSB_INIT_VAL          0x00
        #define CO_SCALE_LSB_INIT_VAL           0x00
        #define CO_SCALE_MSB_INIT_VAL           0x01
    #endif

    // Set some manual cal values
    #ifdef	CONFIGURE_MANUAL_SMOKE_CAL
        #define SMK_CAL_CAV_INIT_VAL            0x48
        #define SMK_CAL_IRLED_PTT_INIT_VAL      0x3D
        #define SMK_CAL_ALM_THOLD_INIT_VAL      0x71
        #define SMK_CAL_LED_INIT_VAL            0x30
        #define SMK_CAL_COR_THOLD_INIT_VAL      SMK_CAL_ALM_THOLD_INIT_VAL
    #else
        #define SMK_CAL_CAV_INIT_VAL            0x00
        #define SMK_CAL_IRLED_PTT_INIT_VAL      0x00
        #define SMK_CAL_ALM_THOLD_INIT_VAL      0x00
        #define SMK_CAL_LED_INIT_VAL            0x00
        #define SMK_CAL_COR_THOLD_INIT_VAL      0x00
    #endif



    // Determine best manual cal values
    #ifdef  CONFIGURE_MANUAL_BAT_CAL
        #define BAT_LB_THOLD_INIT_VAL   BAT_LOW_BAT_THRESHOLD_DEFAULT
    #else
        #define BAT_LB_THOLD_INIT_VAL   0x00
    #endif


    #ifdef	CONFIGURE_EOL_TEST
        #define	LIFE_LSB_INIT_VAL		((LIFE_EXPIRATION_DAY & 0xff) - 1)
        #define	LIFE_MSB_INIT_VAL		(LIFE_EXPIRATION_DAY >> 8)
        #define	LIFE_CHECKSUM_INIT_VAL  (LIFE_LSB_INIT_VAL + LIFE_MSB_INIT_VAL)
    #else
        #define	LIFE_LSB_INIT_VAL           0
        #define	LIFE_MSB_INIT_VAL           0
        #define	LIFE_CHECKSUM_INIT_VAL  	0
    #endif

    #define PAD_INIT_VAL                    0
    #define UNUSED_INIT_VAL                 0


    // Add up the values to compute Checksums
    #define	CHECKSUM_CO	(CO_STATUS_INIT_VAL + CO_CHECKSUM_INIT_VAL + \
                         CO_OFFSET_LSB_INIT_VAL + CO_OFFSET_MSB_INIT_VAL + \
                         CO_SCALE_LSB_INIT_VAL + CO_SCALE_MSB_INIT_VAL)

    #define	CHECKSUM_LIFE (LIFE_LSB_INIT_VAL + LIFE_MSB_INIT_VAL + \
                           LIFE_CHECKSUM_INIT_VAL)

    #define CHECKSUM_SMK (SMK_CAL_ALM_THOLD_INIT_VAL + SMK_CAL_LED_INIT_VAL + \
                          SMK_CAL_IRLED_PTT_INIT_VAL + \
                          SMK_CAL_COR_THOLD_INIT_VAL +SMK_CAL_CAV_INIT_VAL)

    #define CHEKSUM_BAT  (BAT_LB_THOLD_INIT_VAL)

    #define	CHECKSUM	(uchar)(CHECKSUM_CO + CHECKSUM_LIFE + CHECKSUM_SMK + \
                                CHEKSUM_BAT)



/*
|-----------------------------------------------------------------------------
| Typedef, Enums and Structures
|-----------------------------------------------------------------------------
*/
    struct CO_Calibration
    {
        uchar	status;
        uchar	checksum;
        uchar	offset_LSB;
        uchar	offset_MSB;
        uchar	scale_LSB;
        uchar	scale_MSB;
    };
	
    /*
     * If the number of bytes in this structure is odd then sizeof() will not
     * work properly.  In that case the Pad will have to be added.  If even 
     * number of bytes, remove Pad and adjust initialization accordingly.
     */
    struct SystemData
    {
        struct  CO_Calibration CO_Cal;

        uchar Smk_Cav_At_Cal;
        uchar Smk_IRled_PTT_Drive;

        uchar Life_LSB;
        uchar Life_MSB;
        uchar Life_Checksum;

        uchar Smk_Alarm_TH_At_Cal;
        uchar Smk_IRled_Drive_Level;
        uchar Smk_Corrected_Alm_TH;
        uchar LB_Cal_Status;

        uchar Checksum;    // Sum of previous 15 structure bytes
    };




/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/
    extern struct SystemData  volatile SYS_RamData;
    
    
/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
    uchar SYS_Init(void);
    void  SYS_Save(void);
    uchar SYS_Validate_Data_Structure(void);



#endif  // SYSTEMDATA_H
