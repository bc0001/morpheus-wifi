/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        cci_phy.c
|  Author:      Stan Burnette
|               (updated by Bill Chandler, for 8 bit timers)
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Common Communication Interface Physical Layer
|
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include "common.h"
#include "cci_phy.h"

#ifdef CONFIGURE_WIRELESS_MODULE
    #include "app.h"
#endif


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

// USEC Timer defines
#define CCI_TIMEOUT_TRXACK  (uchar)100     // microseconds (use usec timer)
#define CCI_TIMEOUT_TRXRDY  (uchar)100     // microseconds (use usec timer)

//#define CCI_CLOCK_WIDTH_HI  (uchar)10    // microseconds (use usec timer)
//#define CCI_CLOCK_WIDTH_LO  (uchar)7    // microseconds (use usec timer)

// Slow Down Clock
#define CCI_CLOCK_WIDTH_HI  (uchar)20      // microseconds (use usec timer)
#define CCI_CLOCK_WIDTH_LO  (uchar)17      // microseconds (use usec timer)


// MS Timer Defines (timeouts)
#define CCI_TIMEOUT_TTXDAT  (uchar)(1200/64)    // 1.0MS, 64 usec clock
#define CCI_TIMEOUT_BITTIME (uchar)(1700/64)    // 1.5MS, 64 usec clock
#define CCI_TIMEOUT_TTXACK  (uchar)(1200/64)    // 1.0MS, 64 usec clock

// We are using Timer 2 as a mSec Timer and Timer 0 as uSec Timer
#define CCI_USEC_TIMER0          TMR0
#define CCI_MS_TIMER2            TMR2


// Use Timer 2 for a ms Timer
#define CCI_MS_TIMER2_CONTROL    T2CON

// Select SOSC (31.25Khz) = 32 uSec clock)
#define CCI_MS_TIMER2_CLOCK      0b00000110

#ifdef CONFIGURE_T2_64_USEC_CLK
// 64 usec clock x 255 = 16.3 ms max.
#define CCI_MS_TIMER2_INIT       0b10010000 //Clk = 31.25 KHz, prescaler 1:2
#endif

#ifdef CONFIGURE_T2_128_USEC_CLK
// 128 usec clock x 255 = 32.6 ms max.
#define CCI_MS_TIMER2_INIT       0b10100000 //Clk = 31.25 KHz, prescaler 1:4
#endif

#ifdef CONFIGURE_T2_256_USEC_CLK
// 256 usec clock x 255 = 65.2 ms max.
#define CCI_MS_TIMER2_INIT       0b10110000 //Clk = 31.25 KHz, prescaler 1:8
#endif

// Use the 64 uSec Clock
#define CCI_MS_TIMER2_INIT       0b10010000 //Clk = 31.25 KHz, prescaler 1:2

// Weak Pull Up Definitions (used in CCI I/O)
#define CCI_DATA_WPU_OFF  WPUBbits.WPUB1 = 0;
#define CCI_CLK_WPU_OFF   WPUBbits.WPUB2 = 0;
#define WPUB_ALL_PULLUPS_OFF    0x00;


#define CCI_PKT_16_BITS     (uchar)16

#define CCI_PACKET_RECEIVED      0
#define CCI_PACKET_NOT_AVAILABLE 1



/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/
// State constants
enum
{
    CCI_SEND_IDLE =     0,
    CCI_SEND_RTS =      1,
    CCI_SEND_RXACK =    2,
    CCI_SEND_RXRDY =    3,
    CCI_SEND_XMIT =     4
};

#ifdef CONFIGURE_WIRELESS_MODULE

/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/
volatile CCI_Flags_t CCI_Flags;

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
static volatile uchar cci_send_state = CCI_SEND_IDLE;
static union tag_CCI_Packet cci_rx_packet;

/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/
void cci_phy_reset (void);
void cci_enable_rx_int(void);
void cci_disable_rx_int(void);


/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/

/*
+------------------------------------------------------------------------------
| Function:     CCI_Phy_Get_Microsecond_Timer
+------------------------------------------------------------------------------
| Purpose:      Timer 0 is used as a usec Timer
|               This routine returns the current Timer0 value
|
|               returns current usec Timer value
|               
|               Only times up to 255 usecs with 1 usec clocks
|               Need to use MS timer to time longer times
|
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  current value of Microsecond Timer
+------------------------------------------------------------------------------
*/

uchar CCI_Phy_Get_Microsecond_Timer(void)
{
    // This only reads as an 8 bit timer value
    return CCI_USEC_TIMER0;
}

/*
+------------------------------------------------------------------------------
| Function:      CCI_Phy_RST_Microsecond_Timer
+------------------------------------------------------------------------------
| Purpose:       Reset the USec Timer (Timer2)
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void CCI_Phy_RST_Microsecond_Timer(void)
{
    CCI_USEC_TIMER0 = 0;
}



/*
+------------------------------------------------------------------------------
| Function:      CCI_Phy_Get_MS_Timer
+------------------------------------------------------------------------------
| Purpose:       Timer 2 is used as a ms Timer
|                This routine returns the current Timer2 value
|
|               
|               Only times up to 16 milliseconds with 64usec clocks
|               
|               
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

uchar CCI_Phy_Get_MS_Timer(void)
{
    // This only reads an 8 bit timer value
    return CCI_MS_TIMER2;
}

/*
+------------------------------------------------------------------------------
| Function:      CCI_Phy_RST_MS_Timer
+------------------------------------------------------------------------------
| Purpose:       Reset the ms Timer (Timer0)
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void CCI_Phy_RST_MS_Timer(void)
{
    CCI_MS_TIMER2 = 0;
}


/*
+------------------------------------------------------------------------------
| Function:      CCI_Phy_Init
+------------------------------------------------------------------------------
| Purpose:      Initialize Timers used by CCI module
|               Enable Internal Weak Pull-ups on the Clock and Data Pins
|               Enable CCI interupt to receive a packet
|
|               No 16 bit timers available so using two 8 bit timers to handle
|               CCI Timing
|
|               Note: Timers Available 16lf1938/39 PIC
|               TO - 8 bit timer  uSec Timer (1 uSec Clock)
|               T1 - 16 bit timer - Main Sleep Timer
|               T2, 8 bit timer MS Timer (64 uSec clock,times up to 16ms max)
|               T4, 8 bit timer  Main Interrupt Timer
|               T6, 8 bit timer (currently unused)
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void CCI_Phy_Init(void)
{
    // Timer O will be the uSec Timer
    // Timer0 should already be initialized from Main Init
    
    // Timer 2 will be used to measure mSecs
    
    // Timer 2 setup. (set for ms timer)
    T2CON = CCI_MS_TIMER2_INIT; // Timer On, pre-scaler 1:2  (= 64uSec Clk)
    T2CLKCON = CCI_MS_TIMER2_CLOCK; // Clock Source = 31.25 KHz MFINTOSC
    

    // Weak Pull up is enabled for CCI data pin, in case RF module
    //  is not installed. (prevents repeated CCI data interrupts)

    // When we enable Weak pull-ups for CCI, we only want 1 pin
    // enabled, clear all bits 1st
    WPUB = WPUB_ALL_PULLUPS_OFF;

    // For CCI Data Pin Only, enable Weak pull-up
    #ifdef CONFIGURE_UNDEFINED
        // Disable this weak pull-up, as it was creating current leakage
        //  between the Host PCB and the RF module since they run
        //  at different voltage levels
        CCI_DATA_WPU_ENABLE
    #endif

    // Clock and DATA pins start off as inputs.
    mCCI_DATA_PIN_DIR = 1;
    mCCI_CLOCK_PIN_DIR = 1;

    /*
     * 1. Interrupt on Change for CCI_Data input
     *    This should wake up from sleep during Low Power Mode
     *      and set the CCI_Edge Detect Flag
     *
     * 2. Receives packet from Interrupt Routine
     *
     */

    CCI_PIN_INTERRUPT_FLAG = 0;     // CCI Pin Int Flag Cleared
    CCI_PIN_INTERRUPT_EDGE = 1;     // Falling edge
    
    #ifdef CONFIGURE_WIFI
        // Insure CCI is reset to Idle State
        cci_phy_reset();
    #endif

}


/*
+------------------------------------------------------------------------------
| Function:      cci_disable_rx_int
+------------------------------------------------------------------------------
| Purpose:       Disable CCI Receive Interupts
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void cci_disable_rx_int(void)
{
    // Prevent all IOC ints during packet receive/transmit timing
    // For receive we still need 1MS Timer Ints enabled so don't use Global IE
    IOC_INTERRUPT_ENABLE = 0;
}


/*
+------------------------------------------------------------------------------
| Function:      cci_enable_rx_int
+------------------------------------------------------------------------------
| Purpose:       Enable CCI Receive Interupts
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void cci_enable_rx_int(void)
{
    // We need to clear the interrupt before re-enabling the interrupt flag.
    CCI_PIN_INTERRUPT_FLAG = 0;
    IOC_INTERRUPT_ENABLE = 1;
}


/*
+------------------------------------------------------------------------------
| Function:      cci_phy_reset
+------------------------------------------------------------------------------
| Purpose:       Reset CCI hardware Interface back to IDLE state
|                and ready for any new transactions
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void cci_phy_reset (void)
{
    cci_send_state = CCI_SEND_IDLE;
    mCCI_DATA_PIN_DIR = INPUT_PIN;
    mCCI_CLOCK_PIN_DIR = INPUT_PIN;
    cci_enable_rx_int();
    
    // Reset all CCI Flags
    CCI_Flags.ALL = 0;

    FLAG_CCI_EDGE_DETECT = 0;

}


/*
+------------------------------------------------------------------------------
| Function:      CCI_Phy_Xmit
+------------------------------------------------------------------------------
| Purpose:       Currently this is only called from within ISR processing,
|                so Global Interrupt disable/enable should not be needed
|                as this PIC only has a single INT routine.
|
|                Transmits a 16 bit packet over CCI interface
|
+------------------------------------------------------------------------------
| Parameters:    pkt (packet to transmit)
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void CCI_Phy_Xmit(union tag_CCI_Packet pkt)
{
    uchar bitcount;

    // We want no interruptions during the 16 bit transmit time
    // GLOBAL_INT_ENABLE is purposely commented out for this PIC, not needed
    //  since this routine is only called from within a SINGLE Interrupt 
    //  Routine, therefore this routine is not Interruptable.
    //GLOBAL_INT_ENABLE = 0;

    // This function assumes that the clock and data pins have been
    // configured for output.
    for(bitcount = 0; bitcount < 16; bitcount++)
    {
        if( pkt.word & 0x8000)
        {
            // Bit is a one, assert data pin
            mCCI_DATA_PIN = 1;
        }
        else
        {
            // Bit is a zero, reset data pin
            mCCI_DATA_PIN = 0;
        }

        pkt.word = pkt.word << 1;
        
        // Clock data into receiver.
        mCCI_CLOCK_PIN = 1;        // Set clock high
        PIC_delay_us(CCI_CLOCK_WIDTH_HI);
        mCCI_CLOCK_PIN = 0;        // Set clock low.
        PIC_delay_us(CCI_CLOCK_WIDTH_LO);

    }

 
    // GLOBAL_INT_ENABLE is purposely commented out for this PIC, not needed
    //  since this routine is only called from within a SINGLE Interrupt 
    //  Routine, therefore this routine is not Interruptable.
    //GLOBAL_INT_ENABLE = 1;

}


/*
+------------------------------------------------------------------------------
| Function:      CCI_Phy_Send
+------------------------------------------------------------------------------
| Purpose:       Handles Packet Transaction Handshaking and Data Transmission
|                of a packet to be sent to the receiver
|                
|
+------------------------------------------------------------------------------
| Parameters:    pkt (packet to be sent)
+------------------------------------------------------------------------------
| Return Value:  Returns non-zero if error occurred or zero if successful.
+------------------------------------------------------------------------------
*/

uchar CCI_Phy_Send(union tag_CCI_Packet pkt)
{
    uchar return_status;

    #ifdef CONFIGURE_WDT_RST_FLAGS
        // Test Only, Remove later
        FLAG_WDT_1_5 = 1;
    #endif
    
    do
    {
        switch (cci_send_state)
        {
            case CCI_SEND_IDLE:
                if(mCCI_DATA_IN_PIN)
                {
                    // Disable receive interrupt.
                    cci_disable_rx_int();

                    // Turn data pin into output, low to start comm.
                    mCCI_DATA_PIN = 0;
                    mCCI_DATA_PIN_DIR = OUTPUT_PIN;
                    cci_send_state = CCI_SEND_RTS;

                    // Initialize USEC Timer
                    CCI_Phy_RST_Microsecond_Timer();    //Starts from zero
                }
                else
                {
                    // Data Pin must be Busy, don't send back CCI_SEND_SUCCESS
                    //  so we can retry later
                    return_status = CCI_SEND_DATA_PIN_BSY;
                }
            break;

            case CCI_SEND_RTS:
                // This simply waits in this state for CCI_TIMEOUT_TRXACK Time

                // See if timer has expired.
                if(CCI_USEC_TIMER0 >= CCI_TIMEOUT_TRXACK)
                {
                    // Tristate data pin.
                    mCCI_DATA_PIN_DIR = INPUT_PIN;
                    cci_send_state = CCI_SEND_RXACK;
                }
            break;

            case CCI_SEND_RXACK:
                if(!mCCI_DATA_IN_PIN)
                {
                    // Data pin is still low, that means that the receiver has
                    // acknowledged.  Set the clock pin low.
                    mCCI_CLOCK_PIN = 0;
                    mCCI_CLOCK_PIN_DIR = OUTPUT_PIN;
                    cci_send_state = CCI_SEND_RXRDY;

                    // Initialize USEC Timer
                    CCI_Phy_RST_Microsecond_Timer();    //Starts from zero
                }
                else
                {
                    // Data pin is high, receiver has not acknowledged.  
                    // Reset the interface and return error.
                    cci_phy_reset();

                    return_status = CCI_SEND_ERROR_RXACK;
                    cci_send_state = CCI_SEND_IDLE; // Exit State machine

                }
            break;

            case CCI_SEND_RXRDY:
                if(mCCI_DATA_IN_PIN)
                {
                    // Data pin is high => receiver is ready for data.
                    mCCI_DATA_PIN_DIR = OUTPUT_PIN;
                    cci_send_state = CCI_SEND_XMIT;
                }
                else
                {
                    // See if timer has expired.
                    if(CCI_USEC_TIMER0 >= CCI_TIMEOUT_TRXRDY)
                    {
                        // Timer expired => timeout waiting for receiver 
                        // to go ready, Return error
                        cci_phy_reset();

                        return_status = CCI_SEND_ERROR_RXRDY;
                        cci_send_state = CCI_SEND_IDLE; // Exit State machine

                    }
                }
            break;

            case CCI_SEND_XMIT:
                
                CCI_Phy_Xmit(pkt);
                cci_phy_reset();

                return_status = CCI_SEND_SUCCESS;
                
                #ifdef CONFIGURE_WIRELESS_CCI_DEBUG
                    if(FLAG_SERIAL_PORT_ACTIVE)
                    {
                        // Buffer TX Activity in CCI Debug Queue
                        //  For now assume we will never fill the queue so don't 
                        //  examine queue
                        APP_CCI_QPush(APP_MSG_TRANSMIT);
                        APP_CCI_QPush(pkt.tag);
                        APP_CCI_QPush(pkt.data);
                    }
                #endif
                
                cci_send_state = CCI_SEND_IDLE; // Exit State machine
            break;
        }
    
    } while(cci_send_state);

    #ifdef CONFIGURE_WDT_RST_FLAGS
        // Test Only, Remove later
        FLAG_WDT_1_5 = 0;
    #endif
    
    return return_status;
}



/*
+------------------------------------------------------------------------------
| Function:      CCI_Phy_Receive_Packet
+------------------------------------------------------------------------------
| Purpose:       Handles CCI Packet Reception and places packet into
|                cci_rx_packet
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  CC_Rx_Result (result of packet reception)
|                Zero if packet sucessfully received (CCI_RECEIVE_SUCCESS)
|                Non-Zero value if packet Receive Error Detected
+------------------------------------------------------------------------------
*/

enum CC_Rx_Result CCI_Phy_Receive_Packet(void)
{
    uchar bitcounter = 0;
    uchar receiving_packet = TRUE;
    uchar return_status;
    
    
    #ifdef CONFIGURE_WDT_RST_FLAGS
        // Test Only, Remove later
        FLAG_WDT_1_0 = 1;
    #endif
    
    cci_rx_packet.word = 0;

    // Initialialize Millisecond Timer
    CCI_Phy_RST_MS_Timer();     // Timer starts at zero
 
    do
    {
        // Wait for clock to go high
        while(!mCCI_CLOCK_IN_PIN)
        {
            
            #ifdef CONFIGURE_WDT_RST_FLAGS
                // Test Only, Remove later
                FLAG_WDT_1_1 = 1;
            #endif
            
            // See if timer has expired.
            if( CCI_MS_TIMER2  >= CCI_TIMEOUT_TTXDAT)   // 1 millisecond
            {
                cci_phy_reset();
                return_status = CCI_RECEIVE_TO_CLOCK_HIGH;
                

                #ifdef CONFIGURE_WDT_RST_FLAGS
                    // Test Only, Remove later
                    FLAG_WDT_1_0 = 0;
                    FLAG_WDT_1_1 = 0;
                    FLAG_WDT_1_2 = 0;
                    FLAG_WDT_1_3 = 0;
                #endif

                return return_status;
            }
        };
        
        #ifdef CONFIGURE_WDT_RST_FLAGS
            // Test Only, Remove later
            FLAG_WDT_1_1 = 0;
        #endif
        
        // Increment bit counter
        bitcounter++;

        // Clock pin is high, shift state of data pin into packet structure.
        if(mCCI_DATA_IN_PIN)
        {
            // Or in LSB and shift
            cci_rx_packet.word |= 1;
        }    
 
        // If this is the last bit, just return success.
        if(bitcounter >= CCI_PKT_16_BITS)
        {
            // Wait for data pin to go high before continuing.  This ensures
            // that the response will not start until the line goes high.
            while(0 == mCCI_DATA_IN_PIN)
            {
                #ifdef CONFIGURE_WDT_RST_FLAGS
                    // Test Only, Remove later
                    FLAG_WDT_1_3 = 1;
                #endif
            };
            
            receiving_packet = FALSE;
            
            #ifdef CONFIGURE_WDT_RST_FLAGS
                // Test Only, Remove later
                FLAG_WDT_1_0 = 0;
                FLAG_WDT_1_1 = 0;
                FLAG_WDT_1_2 = 0;
                FLAG_WDT_1_3 = 0;
            #endif
            
            // Return directly from here since mCCI_DATA_IN_PIN is back High
            //  and all 16 bits of packet is received
            return CCI_RECEIVE_SUCCESS;
        }
        else
        {
            cci_rx_packet.word = cci_rx_packet.word << 1;
        }

        // Wait for clock to go low
        while(mCCI_CLOCK_IN_PIN)
        {

            #ifdef CONFIGURE_WDT_RST_FLAGS
                // Test Only, Remove later
                FLAG_WDT_1_2 = 1;
            #endif

            // If timer times out waiting for clock to go low, timeout with 
            // error.  This prevents hangup during clocking.
            if(CCI_MS_TIMER2 >= CCI_TIMEOUT_BITTIME)
            {
                cci_phy_reset();

                return_status = CCI_RECEIVE_TO_CLOCK_LOW;
                
                #ifdef CONFIGURE_WDT_RST_FLAGS
                    // Test Only, Remove later
                    FLAG_WDT_1_0 = 0;
                    FLAG_WDT_1_1 = 0;
                    FLAG_WDT_1_2 = 0;
                    FLAG_WDT_1_3 = 0;
                #endif

                return return_status;
            }
        };
        
        #ifdef CONFIGURE_WDT_RST_FLAGS
            // Test Only, Remove later
            FLAG_WDT_1_2 = 0;
        #endif
        
    }  while(receiving_packet);

    #ifdef CONFIGURE_WDT_RST_FLAGS
        // Test Only, Remove later
        FLAG_WDT_1_0 = 0;
        FLAG_WDT_1_1 = 0;
        FLAG_WDT_1_2 = 0;
        FLAG_WDT_1_3 = 0;
    #endif
    
   return return_status;
}


/*
+------------------------------------------------------------------------------
| Function:      CCI_Phy_Receive
+------------------------------------------------------------------------------
| Purpose:       Handles Packet Transaction Handshaking and Data Reception
|                of a packet being received
|
|               CCI_Phy_Receive is called from Interrupt Routine, therefore,
|               Interrupts do not need to be disabled during packet reception
|               since Global IE is automatically cleared during the ISR.
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  CC_Rx_Result (result of packet receive)
|                Zero if packet sucessfully received (CCI_RECEIVE_SUCCESS)
|                Non-Zero value if packet Receive Error Detected
+------------------------------------------------------------------------------
*/

enum CC_Rx_Result CCI_Phy_Receive(void)
{

    // If the data line is low, respond by turning data pin into output low.
    if(0 == mCCI_DATA_IN_PIN)
    {  
        // Acknowledge with Data Output/Low
        mCCI_DATA_PIN = 0;
        mCCI_DATA_PIN_DIR = OUTPUT_PIN;

        // Initialialize Millisecond Timer
        CCI_Phy_RST_MS_Timer();                // Timer starts at zero

        do
        {
            // Sender will lower clock pin as second handshake
            if(!mCCI_CLOCK_IN_PIN)
            {
                // Initialize Receive Status
                uchar retval = CCI_RECEIVE_SUCCESS;
                
                mCCI_DATA_PIN_DIR = INPUT_PIN;
                
                retval = CCI_Phy_Receive_Packet();
                
                // Test for Receive packet errors, continue on Success
                if(CCI_RECEIVE_SUCCESS == retval)
                {
                
                    CCI_Phy_RST_MS_Timer(); // Reset Timer starts at zero

                    // Wait here while clock is still low from sender
                    //  but continue to monitor Timeout Timer
                    do
                    {
                        if(CCI_MS_TIMER2 > CCI_TIMEOUT_TTXACK)
                        {
                            // A timeout occurred waiting for the clk pin to go 
                            //  return high
                            cci_phy_reset();

                            return CCI_RECEIVE_TO_CLK_HANDSHAKE;
                        }

                    }
                    while(!mCCI_CLOCK_IN_PIN);

                    // Clock has been released by Sender

                    // Reset will re-enable CCI and IOC Interrupts
                    cci_phy_reset();

                    #ifdef CONFIGURE_DEBUG_LP_CCI_RESPONSE
                        if(CCI_RECEIVE_SUCCESS == retval)
                        {
                            MAIN_TP2_Off();      // TP2 Off when Complete
                        }
                    #endif

                    #ifdef CONFIGURE_WIRELESS_CCI_DEBUG
                        #ifndef CONFIGURE_DIAG_ACTIVE_FLAGS
                            if(FLAG_SERIAL_PORT_ACTIVE)
                            {
                                // Buffer Response packet in CCI Queue
                                // For now assume we will never fill the queue 
                                //  so don't examine queue
                                APP_CCI_QPush(APP_MSG_RECEIVE);
                                APP_CCI_QPush(cci_rx_packet.tag);
                                APP_CCI_QPush(cci_rx_packet.data);
                            }
                        #else
                                // Buffer Response packet in CCI Queue
                                // For now assume we will never fill the queue 
                                //  so don't examine queue
                                APP_CCI_QPush(APP_MSG_RECEIVE);
                                APP_CCI_QPush(cci_rx_packet.tag);
                                APP_CCI_QPush(cci_rx_packet.data);
                        #endif
                    #endif
                }
                
                return retval;

            }

        } while( CCI_MS_TIMER2 <= CCI_TIMEOUT_TTXACK);

        #ifdef CONFIGURE_DEBUG_LP_CCI_RESPONSE
            MAIN_TP2_Off();      // TP2 Off if Timeout
        #endif

        // A timeout occurred waiting for the clk pin to go low 
        cci_phy_reset();

        return CCI_RECEIVE_TO_CLK_HANDSHAKE;
    }
    
    #ifdef CONFIGURE_DEBUG_LP_CCI_RESPONSE
        // Pulse TP2
        MAIN_TP2_Off();      
        PIC_delay_us(10);
        MAIN_TP2_On();      
        PIC_delay_us(10);
        MAIN_TP2_Off();      
    #endif

   // Return instr. to prevent implicit return warning
   return CCI_RECEIVE_END;
}


/*
+------------------------------------------------------------------------------
| Function:      CCI_Get_Rx_Packet
+------------------------------------------------------------------------------
| Purpose:       Gets a received packet and places it in packet structure
|                pointed to by *packetptr
|
|                This function will only return zero once per message.
|
+------------------------------------------------------------------------------
| Parameters:    *packetptr    (where to place received packet)
+------------------------------------------------------------------------------
| Return Value:  Returns 0 if CCI_PACKET_RECEIVED
|                Returns non-zero if CCI_PACKET_NOT_AVAILABLE
|
+------------------------------------------------------------------------------
*/

uchar CCI_Get_Rx_Packet(union tag_CCI_Packet *packetptr)
{
    if(CCI_Flags.FLAG_CCI_PACKET_RECEIVED)
    {
        CCI_Flags.FLAG_CCI_PACKET_RECEIVED = 0;
        packetptr->word = cci_rx_packet.word;
        return CCI_PACKET_RECEIVED;
    }
    else
    {
        return CCI_PACKET_NOT_AVAILABLE;
    }
 }



#else   /* No Wireless Module */
    

#endif  /* CONFIGURE_WIRELESS_MODULE */

