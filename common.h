/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        common.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  common definitions available for entire project
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


#ifndef	COMMON_H
#define	COMMON_H


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/


#define COUNT_ZERO  (uint)0
#define FLAGS_ALL_CLEARED (uchar)0

#ifndef NULL
 #define  NULL   (void *)0
#endif


#define LSB_TO_UINT(lsb) (*((uint*)&(lsb)))


// Defines used for Scope TP debugging
#define DBUG_DLY_8_USec     (uint)8
#define DBUG_DLY_10_USec    (uint)10
#define DBUG_DLY_20_USec    (uint)20
#define DBUG_DLY_25_USec    (uint)25
#define DBUG_DLY_50_USec    (uint)50

/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/
// Shorthand Data Type definitions
typedef unsigned int uint;
typedef unsigned char uchar;
typedef unsigned int UINT;
typedef unsigned char UCHAR;


// Fixed Integer Data Types for MPLABX and PIC16/18 controllers
typedef unsigned long uint32_t;
typedef unsigned int uint16_t;
typedef unsigned char uint8_t;

typedef long int32_t;
typedef int int16_t;
typedef char int8_t;

enum
{
    FALSE = 0,
    TRUE
};

enum
{
    BIT_CLR = 0,
    BIT_SET
};

enum
{
    LOGIC_ZERO = 0,
    LOGIC_ONE
};


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include  <xc.h>

#ifdef CONFIGURE_UNDEFINED
    // included in the xc.h file
    #include    "pic16lf18877.h"
    #include    "pic16lf1939.h"

    #include    <pic.h>
#endif

#include    "config.h"
#include    "main.h"
#include    "pic_helper.h"

#include    "io.h"
#include    "flags.h"

#include    "serial.h"




/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/
// Not applicable

/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
// Not applicable


#endif  // COMMON_H
