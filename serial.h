/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        serial.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for serial.c module
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


#ifndef	SERIAL_H
#define	SERIAL_H


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/
	#define SERIAL_CONV_BASE_DECIMAL	10
	#define	SERIAL_CONV_BASE_HEX		16

    #define SERIAL_ENABLE_SMOKE     	SER_out_flags._0
    #define SERIAL_ENABLE_CO            SER_out_flags._1
    #define SERIAL_ENABLE_BATTERY   	SER_out_flags._2
    #define SERIAL_ENABLE_HEALTH		SER_out_flags._3
    #define SERIAL_ENABLE_AFT_OUT       SER_out_flags._4
    #define SERIAL_ENABLE_TEMPERATURE	SER_out_flags._5
    #define SERIAL_ENABLE_ALG           SER_out_flags._6

    #define SERIAL_ENABLE_GAS           SER_out_flags._7
    #define SERIAL_ENABLE_LIGHT         SER_out_flags._7

/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/
    extern volatile Flags_t SER_out_flags;
    
    #ifdef CONFIGURE_WIRELESS_CCI_DEBUG
        #ifdef CONFIGURE_SER_QUE_MAX
            // Test Only
            extern uint SER_quemax;
            extern uint SER_cci_quemax;
        #endif
    #endif


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
    void	SER_Send_Int(char tag, uint val, char base);
    void 	SER_Send_Byte(uchar byte);
    void	SER_Send_String(char const *stringtosend);
    void 	SER_Send_Prompt(void);
    void	SER_Send_Char(char);
    uint 	SER_Strx_To_Int(char const *str);
    void 	SER_Init(void);
    void    SER_Process_Rx(void);

    #ifdef CONFIGURE_SERIAL_QUEUE
        void SER_Xmit_Queue(void);
    #endif

    #ifdef CONFIGURE_DIAG_ACTIVE_FLAGS
        void SER_Off(void);
        void SER_On(void);
        uint8_t SER_Queue_Empty(void);
    #endif

    
#endif  // SERIAL_H

