/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        config.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Configuration header file for project model and option
|               configurations
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

#ifndef	CONFIG_H
#define	CONFIG_H

/*
|-----------------------------------------------------------------------------
| Define PIC Configuration Memory Here
|-----------------------------------------------------------------------------
*/

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

// CONFIG1
// External Oscillator mode selection bits (Oscillator not enabled)
#pragma config FEXTOSC = OFF     

// Power-up default value for COSC bits (HFINTOSC with OSCFRQ= 32 MHz and 
//  CDIV = 1:1)                                
#pragma config RSTOSC = HFINT32  
//#pragma config RSTOSC = HFINT1  //  (HFINTOSC (1MHz))

// Clock Out Enable bit (CLKOUT function is disabled; i/o or oscillator 
//  function on OSC2)      
#pragma config CLKOUTEN = OFF   

// Clock Switch Enable bit (Writing to NOSC and NDIV is allowed)                                
//#pragma config CSWEN = ON 
#pragma config CSWEN = OFF

// Fail-Safe Clock Monitor Enable bit (FSCM timer disabled)
#pragma config FCMEN = OFF      


// CONFIG2
// Master Clear Enable bit (MCLR pin is Master Clear function)
#pragma config MCLRE = ON 
//#pragma config MCLRE = OFF

// Power-up Timer Enable bit (PWRT enabled)

// Disable during debugging.
#ifdef __DEBUG
    #pragma config PWRTE = OFF
#else 
    #pragma config PWRTE = ON      // Power-up Timer Enable bit (PWRT enabled)
#endif

//
// Low-Power BOR enable bit (ULPBOR disabled)
//  typically 2.1 - This has a wide range of tolerance- we should not use LPBOR
//  1.8 to 2.7 volts
//
#pragma config LPBOREN = OFF
//#pragma config LPBOREN = ON     // Low-Power BOR enable bit (ULPBOR enabled)

// Brown-out reset enable bits (Brown-out reset enabled according to SBOREN bit)
#pragma config BOREN = SBOREN
//#pragma config BOREN = OFF      // (Brown-out reset disabled)

// Brown-out Reset Voltage Selection 
// (Brown-out Reset Voltage (VBOR) set to 1.9V on LF, and 2.45V on F Devices)
#pragma config BORV = LO

// Zero-cross detect disable (Zero-cross detect circuit is disabled at POR.)
//#pragma config ZCD = ON   // use this for 1.36 to disable
#pragma config ZCD = OFF    // use this for 1.42 (and later) to disable

// Peripheral Pin Select one-way control (The PPSLOCK bit can be cleared 
//  and set only once in software)
//#pragma config PPS1WAY = ON
#pragma config PPS1WAY = OFF 

// Stack Overflow/Underflow Reset Enable bit 
// (Stack Overflow or Underflow will cause a reset)
#pragma config STVREN = ON 


// CONFIG3
// WDT Period Select bits (Divider ratio 1:65536)
#pragma config WDTCPS = WDTCPS_11

// WDT operating mode (WDT enabled/disabled by SWDTEN bit in WDTCON0)
#pragma config WDTE = SWDTEN 
//#pragma config WDTE = OFF       // (WDT Disabled, SWDTEN is ignored)

// WDT Window Select bits (window always open (100%); software control; 
//  keyed access not required)
#pragma config WDTCWS = WDTCWS_7

// WDT input clock selector (WDT reference clock is the 31.0kHz LFINTOSC output)
#pragma config WDTCCS = LFINTOSC


// CONFIG4
// UserNVM self-write protection bits (0x0000 to 0x1FFF write protected)
//#pragma config WRT = ON
#pragma config WRT = OFF      // UserNVM (Write protection off)

// Scanner Enable bit (Scanner module is not available for use)
#pragma config SCANE = not_available

// Low Voltage Programming Enable bit (Low Voltage programming enabled. 
// MCLR/Vpp pin function is MCLR.)
//#pragma config LVP = ON         
#pragma config LVP = OFF      // Low Voltage Programming (High Voltage )


// CONFIG5
// UserNVM Program memory code protection bit (UserNVM code protection disabled)
#pragma config CP = OFF

// DataNVM code protection bit (DataNVM code protection disabled)
#pragma config CPD = OFF        


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

/******************************************************************/
// For WiFi restarted at version at 1.00.00
#define	REVISION_MAJOR	1
#define	REVISION_MINOR	9

#define	REV_STRING	"FW Ver 1.09.00"

// Note: Remember to also update compiler message in defmsgs.c file
/******************************************************************/


// ****************************************************
// This section maps project names to firmware P/N's
//
//****************************************************
//              Select Model to Configure
//****************************************************
//
// ***************************************************
// *********************  US Models  *****************
// ***************************************************

//****************************************************
// Production Configuration? <select or deselect>
//****************************************************
//#define CONFIGURE_PRODUCTION_UNIT

// ************************************************
// ********** Photo/CO Combo AC/DC Models **********
// ************************************************

// P/N - 2581-PG01 (Voice, HW Interconnect, WiFi)
//#define CONFIGURE_2581_US   // PhotoCO ACDC with Wireless WiFi,US


// ***************************************************
// ********** Photo Smoke Only AC/DC Models **********
// ***************************************************

// P/N - 1381-PG03 (Voice, HW Interconnect, WiFi)
//#define CONFIGURE_1381_US   // Photo SMK ACDC with Wireless WiFi,US



// ************************************************
// ********** Photo Smoke Only DC Models **********
// ************************************************



// ***************************************************
// *********************  CA Models  *****************
// ***************************************************






//****************************************************
// Model Specific Configurations
//****************************************************
#ifdef CONFIGURE_2581_US
    #define CONFIGURE_ACDC             // AC with battery backup
    #define CONFIGURE_PHOTO_SMOKE      // Photo Smoke Sensor
    #define CONFIGURE_SMK_CC_TEST      // Conductive Smoke Chamber 

    #define CONFIGURE_CO               // CO Sensor
    #define CONFIGURE_UL_ALARM_CURVE
    #define CONFIGURE_CO_ALM_BAT_CONSERVE
    #define CONFIGURE_10_YEAR_LIFE

    #define CONFIGURE_SMK_ALARM_MEMORY
    #define CONFIGURE_CO_ALARM_MEMORY

    #define CONFIGURE_TEMPERATURE      //Temperature Sensor
    #define CONFIGURE_TEMP_DEG_F       //Temperature Conversion

    #define CONFIGURE_1000MS_TIC       // Low Power Mode Timing
    #define CONFIGURE_EOL_HUSH
    #define CONFIGURE_DIAG_HIST
    #define CONFIGURE_SERIAL_PORT_ENABLED
    #define CONFIGURE_LBAT_CAL_ENABLE

    // Wireless option
    #define CONFIGURE_WIRELESS_MODULE
    // WiFi RF Module
    #define CONFIGURE_WIFI

    // To Sync Battery Test with RF Module RX/TX OFF
    // This does not apply to WiFi Module since it is AC powered only.
    //#define CONFIGURE_WIRELESS_BAT_TXRX_SYNC   // for AC/DC Wireless Only


    #define CONFIGURE_WIRELESS_ALARM_LOCATE

    #define CONFIGURE_WIRELESS_BAT_STATUS

    #define CONFIGURE_DRIFT_COMP	//Enable Smoke Drift Compensation

//	******** Button Feature Configurations ***********
    #define CONFIGURE_SMK_HUSH_ACTIVATE
    //#define CONFIGURE_SMK_HUSH_CANCEL
    //#define CONFIGURE_LB_HUSH
    #define CONFIGURE_EOL_HUSH
    #define CONFIGURE_BUTTON_ALG_ENABLE

    #define CONFIGURE_DIAG_HIST
    #define CONFIGURE_VOICE

    #define CONFIGURE_2018_VOICE_CHIP
    #define CONFIGURE_VCE_OPEN_SWITCH  // Activate Battery Message enabled

    #define CONFIGURE_INTERCONNECT
    #define CONFIGURE_LIGHT_SENSOR

    // See AMP Spec for Bit definitions
    // CO, Smoke, Battery, Smoke Comp
    // For Wireless CCI requirement

    // For now only support Smoke Measure and CO
    //  (until bug in RF Module is fixed)
    #define APP_MY_CAPABILITY1  0x1F
    #define APP_MY_CAPABILITY2  0x00
    #define APP_MODEL_DATA    P4010ACSCO_W_2581

    // This Interconnect Model Supports Strobe Sync Pulses
    #define CONFIGURE_STROBE_SYNC
    #define CONFIGURE_STROBE_1_MS_TASK

    // Test Only - remove 
    #define CONFIGURE_TEST_CODE


#endif

// Photo Smoke AC/DC with Wireless WiFi
#ifdef CONFIGURE_1381_US
    #define CONFIGURE_ACDC             // AC with battery backup
    #define CONFIGURE_PHOTO_SMOKE      // Photo Smoke Sensor
    #define CONFIGURE_SMK_CC_TEST      // Conductive Smoke Chamber 

    #define CONFIGURE_WIRELESS_MODULE
    #define CONFIGURE_WIFI

    // To Sync Battery Test with RF Module RX/TX OFF
    //#define CONFIGURE_WIRELESS_BAT_TXRX_SYNC   // for AC/DC Wireless Only

    #define CONFIGURE_WIRELESS_BAT_STATUS

    #define CONFIGURE_WIRELESS_ALARM_LOCATE

//    #define CONFIGURE_CO_ALM_BAT_CONSERVE
    #define CONFIGURE_10_YEAR_LIFE

    #define CONFIGURE_SMK_ALARM_MEMORY

    #define CONFIGURE_TEMPERATURE      //Temperature Sensor
    #define CONFIGURE_TEMP_DEG_F       //Temperature Conversion

    #define CONFIGURE_1000MS_TIC       // Low Power Mode Timing
    #define CONFIGURE_EOL_HUSH
    #define CONFIGURE_DIAG_HIST
    #define CONFIGURE_SERIAL_PORT_ENABLED
    #define CONFIGURE_LBAT_CAL_ENABLE

    #define CONFIGURE_DRIFT_COMP	//Enable Smoke Drift Compensation

//	******** Button Feature Configurations ***********
    #define CONFIGURE_SMK_HUSH_ACTIVATE
    //#define CONFIGURE_SMK_HUSH_CANCEL
    //#define CONFIGURE_LB_HUSH
    #define CONFIGURE_EOL_HUSH
    #define CONFIGURE_BUTTON_ALG_ENABLE

    #define CONFIGURE_VOICE
    #define CONFIGURE_2018_VOICE_CHIP
    #define CONFIGURE_VCE_OPEN_SWITCH  // Activate Battery Message enabled

    #define CONFIGURE_INTERCONNECT

    #define CONFIGURE_LIGHT_SENSOR

    // See AMP Spec for Bit definitions
    // Smoke, Battery, Smoke Comp
    // For Wireless CCI requirement
    #define APP_MY_CAPABILITY1  0x1E
    #define APP_MY_CAPABILITY2  0x00
    #define APP_MODEL_DATA    P4010ACS_W_1381

    // This Interconnect Model Supports Strobe Sync Pulses
    #define CONFIGURE_STROBE_SYNC
    #define CONFIGURE_STROBE_1_MS_TASK


#endif


//#define CONFIGURE_SIMULATOR_DEBUG

// Test Configuration to Output WDT flags after reset via Serial Port
// TODO:  Undefine after Test Protocol / Development testing
// #define CONFIGURE_WDT_RST_FLAGS

//********************************************************
// DEMO Test firmware Enables Smoke/CO Alarm Clear feature
//
    //#define CONFIGURE_DEMO_TEST_FW
//
//********************************************************


//****************************************************
//****************************************************
//****************************************************
// Global Configurations Applying to All MODELS
//****************************************************
//****************************************************
//****************************************************

//****************************************************
//         *** Smoke Sensitivity Targets ***
#define CONFIGURE_SMK_TARGET_89_UA


// This is for development and agency testing only
// It allows setting Smoke Sensitivity without altering alarm threshold
//#define CONFIGURE_SMK_SLOPE_SIMULATION
//
//****************************************************


// Select Baud rate for Serial Port
//#define CONFIGURE_BAUD_9600
#define CONFIGURE_BAUD_19200

// Enable for Serial Port Hot Reconnect
#define CONFIGURE_SERIAL_CONNECT_BY_BUTTON

#ifdef CONFIGURE_VOICE
    // Button Chirp instead of Button Press Voice-Chip Sound
    //#define CONFIGURE_BTN_CHIRP

    //#define CONFIGURE_CHIRP_ENABLE

    // Need to use Serial Port Voice only
    //#define CONFIGURE_VOICE_SERIAL
#endif


#ifdef CONFIGURE_DRIFT_COMP
    // Uncomment if we want to prevent Trouble Chirps on Dust Faults
    //#define CONFIGURE_DUST_FAULT_BEEPS_OFF

    // Uncomment if we want to prevent Button Resets on Drift Comp Faults
    //#define CONFIGURE_DRIFT_FAULT_RST_OFF

    #define CONFIGURE_USE_CALCULATED_COMP_SLOPE

#endif


//*****************************************************************************
// Configurations to enable Active Mode Timeout Timer
// This is now a standard feature - 2 Hour Timeout
//  
//#define CONFIGURE_ACTIVE_TIMEOUT_TIMER      // Enable Active Mode Timer 
//#define CONFIGURE_ACCEL_ACTIVE_TIMEOUT    // Enable for Accel. 2 Min Timeout
//*****************************************************************************


// Use Light Sensor LED as a Power Indicator
#define CONFIGURE_LGT_SENSOR_PWR_IND
#define CONFIGURE_LGT_FADE_OFF_TO_MEASURE

// Use Green Led as RFD Join Indicator
#define CONFIGURE_GLED_RFD_JOIN_MODE

#ifdef CONFIGURE_CO
    // Peak CO Detect > 100 PPM
    //#define CONFIGURE_CO_PEAK_MEMORY
#endif

#ifdef CONFIGURE_ACDC
    #define CONFIGURE_AC_TRANSIENT_FILTERING_SMK
    //#define CONFIGURE_AC_TRANSIENT_FILTERING_TEMP
#endif

#ifdef CONFIGURE_TEMPERATURE
    #define CONFIGURE_THERMISTOR_TEST
#endif

#define CONFIGURE_NO_REMOTE_LB


// Selected when no VREG regulator or when A/D values are converted
//   to millivolts (uses internal 1 Volt FVR
//#define CONFIGURE_NO_REGULATOR

#ifdef CONFIGURE_INTERCONNECT
    // TEST VBoost Supervises INT Boost Voltage (for INT shorts)
    // Comment out for now, as Matt has implemented a Current Limit circuit.
    //#define CONFIGURE_TEST_VBOOST

    // Configuration to use analog input for INT In
    //#define CONFIGURE_INT_ANALOG_INPUT

#endif

#ifdef CONFIGURE_WIRELESS_MODULE
    //#define CONFIGURE_HUSH_LOCATE_REFRESH
#endif

#ifdef CONFIGURE_SERIAL_PORT_ENABLED
    // Comment Out if not using a Queue
    #define CONFIGURE_SERIAL_QUEUE
    // Outputs Alarm Source (Master, Slave or Remote)
    #define CONFIGURE_SERIAL_ALM_SOURCE_OUT

    // Outputs a user programmable ID value via serial port.
    //#define CONFIGURE_ID_MANAGEMENT

    // Output AC Detect Status via serial port
    #define CONFIGURE_OUTPUT_AC_DETECT

    #define CONFIGURE_OUTPUT_REVISION

    #ifdef CONFIGURE_LIGHT_SENSOR
    // ********* Light Configurations *****************
        #define CONFIGURE_OUTPUT_LIGHT_STRUCT
        #define CONFIGURE_OUTPUT_RAW_LIGHT
    #endif

#endif


//***********************************************************
//    Configurations needed for Cenelec Standard
//    #define CONFIGURE_CENELEC_ALARM



#ifdef	CONFIGURE_PRODUCTION_UNIT
    //********************************************************************
    //********************************************************************
    //  Configurations to Add to production Builds (Development/Test Only)
    //  Normally These will be commented out for final production firmware

    // Enables command to manually Set Smoke Output level (Alg testing)
    #define CONFIGURE_MANUAL_SMK_OUT

    #ifdef CONFIGURE_CO
        // Enables command to manually Set PPM level (Alg testing)
        #define CONFIGURE_MANUAL_PPM_COMMAND

        // Allows F command to toggle CO Alarm ON/OFF
        #define CONFIGURE_SERIAL_CO_ALARM      

    #endif


// TODO:  Review these production CONFIGS after development phase
#ifdef CONFIGURE_SERIAL_PORT_ENABLED
    // For NOW - use Serial Port Voice only for all CONFIGS
    //#define CONFIGURE_VOICE_SERIAL

    // To Monitor Timers via serial Port
    #define CONFIGURE_TIMER_SERIAL

    // Monitor CHIRPs on Serial Port (when testing with no sounder)
    //#define CONFIGURE_CHIRP_SERIAL

    // Monitor Light Transitions to Serial Port
    #define CONFIGURE_LIGHT_CHANGE_SERIAL

    // Monitor Interconnect Signaling
    #define CONFIGURE_INT_ALM_SERIAL

    // Monitor Alarm Priority Alarm Timeout
    //#define CONFIGURE_SER_ALM_TIMER

    // Other Test configurations

    #ifdef CONFIGURE_WIRELESS_MODULE
        // Enables CCI Message Command via serial Port keyboard
        #define CONFIGURE_WIRELESS_MESSAGE_TEST

        // Enables Monitoring of CCI messages via serial Port
        // Creates a Queue to buffer message activity and display on Serial Port
        #define CONFIGURE_WIRELESS_CCI_DEBUG
    #endif
#endif

    //********************************************************************
    //********************************************************************
#endif  /* CONFIGURE_PRODUCTION_UNIT */



#ifndef	CONFIGURE_PRODUCTION_UNIT
//****************************************************
// Available Non-Production Configurations
//****************************************************

//*****************************************************************************
// Test Configurations to run serial port in Low Power/Active Mode
// Test Feature Only
#define CONFIGURE_DIAG_ACTIVE_FLAGS

//#define CONFIGURE_DIAG_GRN_LED_STATES
//#define CONFIGURE_DIAG_RED_LED_STATES
//#define CONFIGURE_DIAG_AMB_LED_STATES

// Note: LED Configurations must be configured together with 
//       CONFIGURE_DIAG_ACTIVE_FLAGS
//*****************************************************************************

//
//	Only Select One Non-Production Build Configuration
//
#define CONFIGURE_NON_PRODUCTION_BENCH

//#define CONFIGURE_EOL_TEST_FW
//#define CONFIGURE_DRIFT_TEST_FW
//#define CONFIGURE_DIAG_HIST_TEST_FW

//#define CONFIGURE_DEPASS_TEST_FW
//#define CONFIGURE_60_HR_TEST_FW
//#define CONFIGURE_DATA_COLLECTION_FW
//#define CONFIGURE_LB_HUSH_TEST_FW
//#define CONFIGURE_DISABLE_ALGORITHM
//#define CONFIGURE_SMOKE_CORRELATION

//#define CONFIGURE_LIGHT_SENSOR_TEST_FW


//****************************************************
//	Selected if UL test units
//	 This disables the ALG ON/OFF 4 Hour Timer
//****************************************************
//#define CONFIGURE_UL_TESTING

//*******************************************************************
//	Select a Sensitivity Target to adjust Calibration Sensitivity
//*******************************************************************
//#define		CONFIGURE_SMK_SENS_83_0	//* Used for Correlation
//#define		CONFIGURE_SMK_SENS_83_5
//#define		CONFIGURE_SMK_SENS_84_0
//#define		CONFIGURE_SMK_SENS_84_5
//#define		CONFIGURE_SMK_SENS_85_0	//* Used for Correlation
//#define		CONFIGURE_SMK_SENS_85_5
//#define		CONFIGURE_SMK_SENS_86_0
//#define		CONFIGURE_SMK_SENS_86_5
//#define		CONFIGURE_SMK_SENS_87_0	//* Used for Correlation
//#define		CONFIGURE_SMK_SENS_89_0	//* Used for Correlation
//#define		CONFIGURE_SMK_SENS_89_5
//#define		CONFIGURE_SMK_SENS_91_0	//* Used for Correlation
//#define		CONFIGURE_SMK_SENS_92_0
//#define		CONFIGURE_SMK_SENS_92_5
//#define		CONFIGURE_SMK_SENS_93_0	//* Used for Correlation
//#define		CONFIGURE_SMK_SENS_93_5
//#define		CONFIGURE_SMK_SENS_94_0
//#define		CONFIGURE_SMK_SENS_94_5
//#define		CONFIGURE_SMK_SENS_95_0	//* Used for Correlation
//#define		CONFIGURE_SMK_SENS_95_5
//#define		CONFIGURE_SMK_SENS_96_0



//	***********************************************************************
//	***********************************************************************
//  **********************  Special Test Configurations *******************
//	***********************************************************************
//	***********************************************************************

//	***************************************************************************
#ifdef CONFIGURE_DISABLE_ALGORITHM

    #define CONFIGURE_ALG_OFF

    #ifdef CONFIGURE_CO
        // Use manually calibrated Co values
        #define	CONFIGURE_MANUAL_CO_CAL 

        // Enables command to manually Set PPM level
        #define CONFIGURE_MANUAL_PPM_COMMAND   
    #endif

    // To manually calibrate SMK values
	//#define CONFIGURE_MANUAL_SMOKE_CAL 

    // Enables command to manually Set Smoke Output level
	#define	CONFIGURE_MANUAL_SMK_OUT

	#define	CONFIGURE_MANUAL_BAT_CAL

#endif

//	***************************************************************************
#ifdef CONFIGURE_NON_PRODUCTION_BENCH 
// Basic Development Bench Test Version (Manual CO Cal)

    // To manually Calibrate Low Battery Threshold
    #define	CONFIGURE_MANUAL_BAT_CAL

    #ifdef CONFIGURE_CO
        // To manually calibrate CO offset/slope values
        #define CONFIGURE_MANUAL_CO_CAL
        //#define CONFIGURE_FAST_CO_CAL

        // Enables command to manually Set PPM level
        #define CONFIGURE_MANUAL_PPM_COMMAND

        // Allows F command to toggle CO Alarm ON/OFF
        #define CONFIGURE_SERIAL_CO_ALARM      

        // For some data collection and for early AFT firmware
        // #define CONFIGURE_DISABLE_CO_HEALTH_TEST

        // Disable for some wireless development testing
        #ifdef CONFIGURE_CO_ALM_BAT_CONSERVE
//            #undef CONFIGURE_CO_ALM_BAT_CONSERVE
        #endif
    #endif

    // To manually calibrate SMK values
    //#define CONFIGURE_MANUAL_SMOKE_CAL

    // Allows Alarm Threshold to be changed via Serial Port
    //#define CONFIGURE_MANUAL_SMK_TH   

    // Enables command to manually Set Smoke Output level
    #define CONFIGURE_MANUAL_SMK_OUT

    // To Select Voice messages via serial Port
    #define CONFIGURE_VOICE_COMMAND

    // Other Serial Port Commands
    //#define CONFIGURE_OUTPUT_RAM_DATA
    //#define CONFIGURE_OUTPUT_BACKUP_STRUCT

    // Need to use Serial Port Voice only
//    #define CONFIGURE_VOICE_SERIAL

    //#define CONFIGURE_LIGHT_COMMAND

    // Outputs Serial Que MAX Used values for CCI and Serial Queues
    //#define CONFIGURE_SER_QUE_MAX

    // To Monitor Timers via serial Port
    #define CONFIGURE_TIMER_SERIAL

    // Monitor CHIRPs on Serial Port (when testing with no sounder)
    //#define CONFIGURE_CHIRP_SERIAL

    // Monitor Light Transitions to Serial Port
    //#define CONFIGURE_LIGHT_CHANGE_SERIAL

    // Monitor Interconnect Signaling
    #define CONFIGURE_INT_ALM_SERIAL

    // Monitor Alarm Priority Alarm Timeout
    //#define CONFIGURE_SER_ALM_TIMER

    // Other Test configurations
    //#define CONFIGURE_ACTIVE_MODE_ONLY
    //#define CONFIGURE_WATCHDOG_TIMER_OFF
    //#define CONFIGURE_ACCEL_TIMERS

    // To test language select only
    //#define CONFIGURE_CANADA

    // Special Light Test FW to measure LIght Sensor in different Modes
//    #define CONFIGURE_SPECIAL_LIGHT_TEST_FW

    // Enable Manual Life Commands
    // To test Life Expiration
    #define CONFIGURE_MANUAL_LIFE_COMMAND


#endif


#ifdef CONFIGURE_LIGHT_SENSOR_TEST_FW                 
// Accelerated Light Test Version (Manual CO Cal)

	#define	CONFIGURE_MANUAL_BAT_CAL

    #ifdef CONFIGURE_CO
        // To manually calibrate CO offset/slope values
        #define CONFIGURE_MANUAL_CO_CAL

        // Enables command to manually Set PPM level
        #define CONFIGURE_MANUAL_PPM_COMMAND
    #endif

    // To manually calibrate SMK values
	//#define CONFIGURE_MANUAL_SMOKE_CAL

    // Enables command to manually Set Smoke Output level
	#define	CONFIGURE_MANUAL_SMK_OUT

    #define CONFIGURE_ACCEL_LIGHT_TEST
    #define CONFIGURE_LIGHT_TEST_TABLE

    // Use Day 1st Pre-Loaded Table
    #define CONFIGURE_LGT_TABLE_DAY_FIRST
    #define CONFIGURE_LIGHT_COMMAND

    #define CONFIGURE_LIGHT_CHANGE_SERIAL
    #define CONFIGURE_TIMER_SERIAL

#endif


//	***************************************************************************
#ifdef CONFIGURE_EOL_TEST_FW				
// End of Life Test Firmware

    // CONFIGS to allow testing of 7 day timers and 24 hour hush times
    //  Test 7 day hush limits (EOL, LB, Network Error)
    //  Hush 1 Day (24 hours) = 3 minutes
    //  7 Day Timer = 10 minutes
    #define CONFIGURE_ACCEL_TIMERS

    // for EOL test - 1 minute = 1 day, therfore for testing:
    //    - 1 Day = 1 minute to EOL Mode
    //    - 7 Days = 7 minutes to EOL fatal
    //    - EOL Hush = 3 minutes (for 24 hours))
    //    - EOL Voice Timer = remains at 5 minutes
    #define CONFIGURE_EOL_TEST

    #ifdef CONFIGURE_CO
        // To manually calibrate CO offset/slope values
        #define CONFIGURE_MANUAL_CO_CAL
        #define CONFIGURE_MANUAL_PPM_COMMAND

        // Allows F command to toggle CO Alarm ON/OFF
        #define CONFIGURE_SERIAL_CO_ALARM      

    #endif

    // To manually Calibrate SMK values
	//#define CONFIGURE_MANUAL_SMOKE_CAL 

    // Enables command to manually Set Smoke Output level
    #define CONFIGURE_MANUAL_SMK_OUT

    // Pre-Set Low Battery CAL 
	#define CONFIGURE_MANUAL_BAT_CAL


    // Add Accelerated Light testing to EOL Test firmware to allow testing
    //  of EOL Night Time Hush

    #ifndef CONFIGURE_ACCEL_LIGHT_TEST
        #define CONFIGURE_ACCEL_LIGHT_TEST

        #ifndef CONFIGURE_LIGHT_TEST_TABLE
            // Pre-Load Light Table
            #define CONFIGURE_LIGHT_TEST_TABLE
            #define CONFIGURE_LGT_TABLE_DAY_FIRST
        #endif

    #endif

    #define CONFIGURE_LIGHT_COMMAND

#endif

//	***************************************************************************
#ifdef CONFIGURE_DRIFT_TEST_FW
    // Drift Comp Test Firmware

    // UL Test Configuration for SMK compensation
    #define CONFIGURE_TEST_SMK_COMP
    #ifdef CONFIGURE_TEST_SMK_COMP
        #define DBUG_SERIAL_OUTPUT_COMP_CAV
    #endif

    #ifdef CONFIGURE_CO
        // To manually calibrate CO offset/slope values
        #define CONFIGURE_MANUAL_CO_CAL  
        //#define CONFIGURE_MANUAL_PPM_COMMAND
    #endif

	#ifdef  CONFIGURE_DIAG_HIST
		#undef CONFIGURE_DIAG_HIST	// Free up memory for Drift Test Code
	#endif

    // Enables command to manually Set Smoke Output level
	#define CONFIGURE_MANUAL_SMK_OUT		

    // Force ALG OFF for this configuration
	#define	CONFIGURE_ALG_OFF 

	#define	CONFIGURE_MANUAL_BAT_CAL

#endif


//	***************************************************************************
#ifdef CONFIGURE_DIAG_HIST_TEST_FW                    
// Diagnostic Hist Test Firmware

    // Accelerates day timing to 1 min for testing
	#define CONFIGURE_DIAG_HIST_TEST		
    #ifdef CONFIGURE_CO
        // To manually calibrate CO offset/slope values
        #define CONFIGURE_MANUAL_CO_CAL  
        #define CONFIGURE_MANUAL_PPM_COMMAND
    #endif

    // To manually calibrate SMK values
	//#define CONFIGURE_MANUAL_SMOKE_CAL         

    // Enables command to manually Set Smoke Output level
	#define CONFIGURE_MANUAL_SMK_OUT		
	#define CONFIGURE_MANUAL_BAT_CAL

#endif


//	***************************************************************************
#ifdef CONFIGURE_60_HR_TEST_FW			
// 60 Hour CO Sensitivity Test Firmware (should be run without Manual Co Cal)

	#define CONFIGURE_60_HOUR_TEST

    // Enables command to manually Set Smoke Output level
	#define CONFIGURE_MANUAL_SMK_OUT
    #define CONFIGURE_MANUAL_PPM_COMMAND

    #define CONFIGURE_DBUG_SERIAL_PPM_OUTPUT_CORRECTED

	#define	CONFIGURE_MANUAL_BAT_CAL

#endif

//	***************************************************************************
#ifdef CONFIGURE_LB_HUSH_TEST_FW			
// LB Hush Test Firmware

    // CONFIGS to allow testing of 7 day timers and 24 hour hush times
    //  Test 7 day hush limits (EOL, LB, Network Error)
    //  Hush 1 Day (24 hours) = 3 minutes
    //  7 Day Timer = 10 minutes
    #define CONFIGURE_ACCEL_TIMERS

    #ifdef    CONFIGURE_CO
        // To manually calibrate CO offset/slope values
        #define CONFIGURE_MANUAL_CO_CAL                
    #endif
    // To manually calibrate SMK values
	//#define CONFIGURE_MANUAL_SMOKE_CAL 

	#define	CONFIGURE_MANUAL_BAT_CAL

#endif





//	***********************************************************************
//	***********************************************************************
//  ******************** Available Configurations ************************
//	***********************************************************************
//	***********************************************************************

//	******** Button Feature Configurations ***********
//	#define CONFIGURE_SMK_HUSH_ACTIVATE
//	#define CONFIGURE_SMK_HUSH_CANCEL
//	#define CONFIGURE_LB_HUSH
//	#define CONFIGURE_EOL_HUSH
//  #define CONFIGURE_BUTTON_ALG_ENABLE

//  Outputs Button Press detection modes via serial Port
//  #define CONFIGURE_SERIAL_BTN_TEST

//	********** CO Model Configurations ***************
//	#define CONFIGURE_CO_FUNC_TEST
//  #define CONFIGURE_CO_H2_FAULTS

    // Increases from CO 7 year life to 10 years when enabled
//	#define CONFIGURE_10_YEAR_LIFE

//  #define CONFIGURE_60_HOUR_TEST
//	#define CONFIGURE_24_HOUR_TEST

    // Enables 1 minute updated of CO Ambient Averaging
//	#define CONFIGURE_TEST_CO_AMBIENT	

//	#define CONFIGURE_MANUAL_CO_CAL
//  #define CONFIGURE_FAST_CO_CAL
//	#define CONFIGURE_MANUAL_PPM_COMMAND


//	********** Smoke Configurations ******************
//  #define CONFIGURE_FORCE_SMK_CAL

    // To manually calibrate smoke value 
//  #define CONFIGURE_MANUAL_SMOKE_CAL 

//  #define CONFIGURE_OFFSET_ONLY_CAL
//  #define CONFIGURE_FAST_SMOKE_CAL


//	********** Interconnect Configurations ***********
//	#define CONFIGURE_INTERCONNECT


//	********** Power Configurations ******************
//	#define CONFIGURE_OUTPUT_AC_DETECT
//	#define CONFIGURE_CO_ALM_BAT_CONSERVE            
//	#define CONFIGURE_ALARM_BAT_CONSERVE_OFF
//  #define CONFIGURE_MANUAL_BAT_CAL
//	#define CONFIGURE_LBAT_CAL_ENABLE
//	#define CONFIGURE_BAT_INITIAL_TEST



    // *** Allows Low Power Clock switching if we want to use for long delays
//  #define CONFIGURE_LED_LP_CLOCK_SWITCH

	// *** Configuration for Short Low Battery Hush Test (3 minutes) ***
//	#define CONFIGURE_SHORT_HUSH_TEST


    // Selected when no VREG regulator or when A/D values are converted
    //   to millivolts (uses internal 1 Volt FVR
//  #define CONFIGURE_NO_REGULATOR


//  ********* Feature Configurations *****************
//  #define CONFIGURE_VOICE



//  *************************************************************************
//                  Misc Test/debug Configurations
//  *************************************************************************

#ifdef CONFIGURE_SERIAL_PORT_ENABLED
    #ifdef CONFIGURE_WIRELESS_MODULE
        // Enables CCI Message Command via serial Port keyboard
        #define CONFIGURE_WIRELESS_MESSAGE_TEST

        // Enables Monitoring of CCI messages via serial Port
        // Creates a Queue to buffer message activity and display on Serial Port
        #define CONFIGURE_WIRELESS_CCI_DEBUG
    #endif

    // Enable This if we need to monitor a value on Serial Port
    //#define CONFIGURE_MONITOR_INT
#endif

    //**********************************************************************
    // Debug Test Configurations
    // #define CONFIGURE_DBUG_STACK_TEST
    //**********************************************************************

#ifdef CONFIGURE_2557_US
    // *** For 60 Second Battery Test Only
    //#define CONFIGURE_DEBUG_60_SEC_BAT_TEST
#endif

	// *** Scope Test Point for Debugging ***
    // Enable the CONFIGURE_DEBUG_PIN_ENABLED when any of the Debug
    //  Configurations are enabled
	#define CONFIGURE_DEBUG_PIN_ENABLED

    //**********************************************************************
    // TP1 Test Configurations
    // *** Select Only One of the following Debug Configurations ***
    //#define CONFIGURE_DEBUG_TASK_TIMING        // Task Engine Timing Debug

    #define CONFIGURE_DEBUG_WAKE_SLEEP_TIMING  // Wake/Sleep Timing Debug
    //#define CONFIGURE_DEBUG_INTS               // Main Interrupt Debug
    //#define CONFIGURE_DEBUG_INT_1MS            // 1MS Interrupt Timing Debug

    //#define CONFIGURE_DEBUG_INTERCONNECT_IN    // Monitors INT IN Sampling
    //#define CONFIGURE_DEBUG_INT_STATES
    //#define CONFIGURE_DEBUG_INT_BIT_DETECTED

    //**********************************************************************

    //**********************************************************************
    // TP2 Test Configurations
    // *** Select Only One of the following Debug Configurations ***
    //#define CONFIGURE_DEBUG_1_MICROSEC_TIMER
    //#define CONFIGURE_DEBUG_CCI_MS_TIMER
    //#define CONFIGURE_DEBUG_ALM_PRIORITY_STATES
    //#define CONFIGURE_DEBUG_STROBE_STATES
    //#define CONFIGURE_DEBUG_BTN_STATES
    //#define CONFIGURE_DEBUG_PTT_STATES
    //#define CONFIGURE_DEBUG_APP_STATES
    //#define CONFIGURE_DEBUG_SMOKE_STATES
    //#define CONFIGURE_DEBUG_CO_MEAS_STATES
    //#define CONFIGURE_DEBUG_TEMP_SAMPLES
    //#define CONFIGURE_DEBUG_A2D_SAMPLES

    //#define CONFIGURE_DEBUG_INT_IN_DETECT

    //#define CONFIGURE_DEBUG_INT_IN_TEST

    //#define CONFIGURE_DEBUG_PWM

    //#define CONFIGURE_DEBUG_LP_CCI_RESPONSE

    // This blinks # of devices with Red Led every # of devices request
    //#define CONFIGURE_DEBUG_NET_DEVICES_TEST
    

    //**********************************************************************


    //#define CONFIGURE_MIN_MAX_OUT

    //#define CONFIGURE_DISABLE_FAULTS
    //#define CONFIGURE_NO_SLEEP
    //#define CONFIGURE_RST_TEST

    // For debugging turn off WDT
    //#define CONFIGURE_WATCHDOG_TIMER_OFF

	// *** Configure to Output Active Flags status ***
	// (to see what flag may be preventing Low Power mode)
    //#define CONFIGURE_ACTIVE_TEST

	// *** Config for Short EOL Test (1 minute) ***
	// 1 minute = 1 day, Hush = 3 minutes
    //#define  CONFIGURE_EOL_TEST

    // Accelerates Life - 24 Hours = 1 Minute
    //#define CONFIGURE_DIAG_HIST_TEST

    // *** Light Test Firmware ***
    //#define CONFIGURE_ACCEL_LIGHT_TEST
    //#define CONFIGURE_LIGHT_TEST_TABLE

    //#define CONFIGURE_MAN_NIGHT_DAY_TOGGLE



//#define CONFIGURE_ACCEL_TIMERS
//
// CONFIGS to allow testing of 7 day timers and 24 hour hush times
//  Timers (EOL, LB, Network Error, Alarm memory)
//  Hush 1 Day (24 hours) = 3 minutes
//  7 Day Timer = 10 minutes
//  Alarm Memory (24 Hours) = 3 minutes
//  Voice Timers remain at 5 minutes


//#define CONFIGURE_EOL_TEST
//
// for EOL test - 1 minute = 1 day, therfore for testing:
//    - 1 Day = 1 minute to EOL Mode
//    - 7 Days = 7 minutes to EOL fatal
//    - EOL Hush = 3 minutes (for 24 hours))
//    - EOL Voice Timer = remains at 5 minutes

#endif	//#ifndef	CONFIGURE_PRODUCTION



/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/



#endif	// CONFIG_H
