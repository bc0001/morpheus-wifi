/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        interconnect.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for interconnect.c module
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

#ifndef INTERCONNECT_H
#define	INTERCONNECT_H

#ifdef	__cplusplus
extern "C" {
#endif
    
    

/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/
    // Interconnect Drive Macros
    #define PORT_INT_LO_DRV_ENA       TRISDbits.TRISD5 = 0;
    #define PORT_INT_HI_DRV_ENA       TRISDbits.TRISD6 = 0;
    #define PORT_INT_OUT_ENA          TRISEbits.TRISE2 = 0;
    #define PORT_INT_OUT_DIGITAL      ANSELEbits.ANSE2 = 0;

    #define PORT_INT_LO_DRV_DIS       TRISDbits.TRISD5 = 1;
    #define PORT_INT_HI_DRV_DIS       TRISDbits.TRISD6 = 1;
    #define PORT_INT_OUT_DIS          TRISEbits.TRISE2 = 1;
    #define PORT_INT_OUT_ANALOG       ANSELEbits.ANSE2 = 1;

    // Interconnect Inverted Logic
    #define INT_OUT_LOW               LAT_INT_OUT_PIN = 1;
    #define INT_OUT_HIGH              LAT_INT_OUT_PIN = 0;

    //
    // States returned by INT_get_state function
    //
    #define INTCON_STATE_INIT           0
    #define	INTCON_STATE_IDLE           1
    #define	INTCON_STATE_CO_TRANSMIT	2
    #define	INTCON_STATE_MSTR_SMOKE		3
    #define	INCON_STATE_WAIT            4
    #define	INCON_STATE_RECEIVE_1ST		5
    #define	INTCON_STATE_RECEIVE_NEXT	6
    #define INTCON_STATE_REMOTE_SMOKE   7



/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/

    uint INT_Process_Task(void);
    void INT_Intercon_Init(void);
    uchar INT_Is_Int_In_Active(void);

    void INT_set_output(void);
    void INT_set_input(void);
    
    uchar INT_get_state(void);


#ifdef	__cplusplus
}
#endif

#endif	/* INTERCONNECT_H */

