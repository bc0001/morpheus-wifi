/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        photosmoke.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for photosmoke.c module
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


#ifndef PHOTOSMOKE_H
#define	PHOTOSMOKE_H

#ifdef	__cplusplus
extern "C" {
#endif


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/
    // IRED Drive I/O
    #define IRED_DRIVE_PIN_OUTPUT     TRISAbits.TRISA6 = 0;
    #define IRED_DRIVE_PIN_INPUT      TRISAbits.TRISA6 = 1;

    // Increased amount of Init Comp Time to allow full compensation to occur.
    #define    INIT_CAV_AVG_TIME   58      //58 samples 
    
// Smoke measurement rails at 255, but limit Alarm Threshold to 214
//  to leave some room for Slump and Smoke Alarm Hush
// We could allow it to go higher, but then Smoke Nuisance would be affected.
//  (i.e no Slump or Smoke Hush))
#define SMK_ALARM_MAX_LIMIT (uchar)214

// Equiv to about 30 CAV counts (30 x 1.6 = 48)
#define SMK_ALARM_MIN_LIMIT (uchar)50
    

/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/
    // Union used for Averaging variables
    typedef union
    {
        struct {
            unsigned char lsb;
            unsigned char msb;
        } byte;
        unsigned int word;
    } avg_t;

    //
    // Photo Smoke States
    //
    enum
    {
        SMK_PHOTO_STARTUP_DELAY = 0,
        SMK_PHOTO_IDLE,
        SMK_SET_HIGH_DRIVE,
        SMK_SET_LOW_DRIVE,
        SMK_CALIBRATE,
        SMK_FIND_PTT_DRIVE,
        SMK_STANDBY,
        SMK_ALARM_NORMAL,
        SMK_ALARM_HUSH,
        SMK_JUMP_MODE,
        SMK_SLUMP_MODE_1,
        SMK_SLUMP_MODE_2,
        SMK_DWELL,
        SMK_STATE_NO_ACTION = 0xFF
    };


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/
    extern uchar PHOTO_Alarm_Thold;
    extern uchar PHOTO_Reading;
    extern uchar PHOTO_Man_Reading;
    extern uchar PHOTO_Cav_Timer;

    extern persistent avg_t PHOTO_Cav_Average;
    
    extern uchar PHOTO_Comp_Max_Cav;
    extern uchar PHOTO_Comp_Min_Cav;


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
    uint PHOTO_Process_Task(void);
    void PHOTO_Set_Drive_Pulse_Width(uchar);
    void PHOTO_Reset(void);
    void PHOTO_Photo_Init(void);
    void PHOTO_Init_CAV_Avg(void);
    void PHOTO_Compensate_Alm_Thold(void);
    void PHOTO_Compute_CAV_Avg(void);
    uchar PHOTO_Get_State(void);
    uchar PHOTO_Hush_Too_High(void);


#ifdef	__cplusplus
}
#endif
 

#endif	/* PHOTOSMOKE_H */

