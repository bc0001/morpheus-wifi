/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        esclight.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Functions for escape light
|                
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/



/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include    "common.h"
#include    "esclight.h"
#include    "main.h"



/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

#ifdef	CONFIGURE_ESCAPE_LIGHT
    #define ESC_LIGHT_INTERVAL_100_MS       (uint)100
    #define ESC_LIGHT_INTERVAL_1000_MS      (uint)1000

    #define	ESC_STATE_INIT      0
    #define	ESC_STATE_IDLE      1
    #define ESC_STATE_ACTIVE    2

    // Set Light Timer to 4 minutes, to save preserve ESC Light Battery Life
    #define ESC_LGT_TIMEOUT     (uint)240

    // Escale Light I/O definitions
    #define ESCAPE_LIGHT_ON        LAT_ESC_LIGHT_PIN = 1;
    #define ESCAPE_LIGHT_OFF       LAT_ESC_LIGHT_PIN = 0;

#endif

#define ESC_LIGHT_INTERVAL_61_SEC      61000


/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
#ifdef	CONFIGURE_ESCAPE_LIGHT
    static unsigned char esc_light_state = ESC_STATE_INIT;
    static unsigned int  esc_light_timer;
#endif
    
/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/

#ifdef	CONFIGURE_ESCAPE_LIGHT

/*
 * The escape Light option uses high intensity CREE LED
 * When Alarm is active the Escape Light is enabled by
 * setting the ESCAPE_LIGHT FLAG Active (High)
 *
 * A Maximum On Time is controlled by Timer
 * Currently set at 4 minutes
 * 
 */

/*
+------------------------------------------------------------------------------
| Function:      Esc_Light_Process_Task
+------------------------------------------------------------------------------
| Purpose:       The escape Light option uses high intensity CREE LED
|                When Alarm is active the Escape Light is enabled by
|                setting the ESCAPE_LIGHT FLAG Active (High)
|
|                A Maximum On Time is controlled by Timer
|                Currently set at 4 minutes
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Escape Light Task Next Execution Time (in Task TICs)
+------------------------------------------------------------------------------
*/

unsigned int Esc_Light_Process_Task(void)
{
    switch	(esc_light_state)
    {
        case ESC_STATE_INIT:
            ESCAPE_LIGHT_OFF
            esc_light_state = ESC_STATE_IDLE;
        break;

        case ESC_STATE_IDLE:
            // Monitor Escape Light Status Flag
            if( FLAG_ESC_LIGHT_ON )
            {
                ESCAPE_LIGHT_ON

                // Init Maximum On Timer
                esc_light_timer = ESC_LGT_TIMEOUT;

                esc_light_state = ESC_STATE_ACTIVE;
            }
            else
            {
                ESCAPE_LIGHT_OFF
            }
        break;

        case ESC_STATE_ACTIVE:
            // See if Escape Light is still Active
            if( FLAG_ESC_LIGHT_ON )
            {
                if(0 == --esc_light_timer)
                {
                    // Timer has expired, turn Light Off and remain
                    //  in this state while ESC Light condition remains
                    //  active
                    esc_light_timer++;      // Remain in this Off state
                    ESCAPE_LIGHT_OFF
                }
                else
                {
                    // Hang Here while Escape Light is still Active
                }

            }
            else
            {
                // Escape Light OFF, return to Idle
                esc_light_state = ESC_STATE_IDLE;
            }

        break;

        default:
            esc_light_state = ESC_STATE_IDLE;
        break;
    }
    return  MAIN_Make_Return_Value(ESC_LIGHT_INTERVAL_1000_MS);
}


/*
+------------------------------------------------------------------------------
| Function:      Esc_Light_On
+------------------------------------------------------------------------------
| Purpose:       Turn Escape Light On
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void Esc_Light_On(void)
{
    ESCAPE_LIGHT_ON
}

/*
+------------------------------------------------------------------------------
| Function:      Esc_Light_Off
+------------------------------------------------------------------------------
| Purpose:       Turn Escape Light Off
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void Esc_Light_Off(void)
{
    ESCAPE_LIGHT_OFF
}

#else

// No Escape Light option
// Dummy placeholder function when no Escape Light 

unsigned int Esc_Light_Process_Task(void)
{
    return  MAIN_Make_Return_Value(ESC_LIGHT_INTERVAL_61_SEC);
}

#endif