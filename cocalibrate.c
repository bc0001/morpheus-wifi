/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        cocalibrate.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Contains functionality for CO alarm 2 point calibration at
|               zero and 150 PPM
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include	"common.h"
#include	"cocalibrate.h"
#include	"systemdata.h"
#include	"comeasure.h"
#include	"fault.h"
#include	"sound.h"
#include        "led.h"



/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

#define CAL_RETURN_61_SEC               (uint)61000   //61 Seconds

#ifdef CONFIGURE_CO


#define	CAL_INITIAL_DELAY_1000_MS       (uint)1000
#define	CAL_UPDATE_RATE_30_SEC          (uint)30000	// 30 Seconds
#define CAL_ACQ_RATE_10_SEC             (uint)10000	// 10 Seconds
#define	CAL_BEEP_TIME_10_SEC            (uint)10000	// 10 seconds
#define	CAL_WAIT_TESTED_TIME_100_MS     (uint)100   // .1 second
#define CAL_RETURN_1_SEC                (uint)1000  //  1 second

#ifdef	CONFIGURE_FAST_CO_CAL
    #define CAL_ZERO_STABIL_PERIOD      (uchar)4   // 2 Minute
    #define CAL_150_STABIL_PERIOD       (uchar)4   // 2 Minute
    #define CAL_ZERO_CALIBRATION_PERIOD	(uchar)4   // 2 Minute
    #define CAL_150_CALIBRATION_PERIOD	(uchar)4   // 2 Minute

    #define	CAL_ERR_DELAY_PERIOD    (uchar)4 // 2 minutes (in 30 second units)

#else
    #define CAL_ZERO_STABIL_PERIOD	(uchar)12       // 6 minutes (30S Rate)
    #define CAL_150_STABIL_PERIOD	(uchar)12       // 6 minutes (30S Rate)
    #define CAL_ZERO_CALIBRATION_PERIOD	(uchar)21   // 3.5 minutes (10S rate)
    #define CAL_150_CALIBRATION_PERIOD	(uchar)21   // 3.5 minutes (10S rate)

    #define	CAL_ERR_DELAY_PERIOD	(uchar)40 // 20 minutes (in 30 second units)

#endif

#define CAL_STATE_DELAY             0
#define CAL_STATE_BEGIN             1
#define CAL_STATE_STABILIZATION		2
#define CAL_STATE_ACQUISITION		3
#define CAL_STATE_WAIT              4
#define CAL_STATE_WAIT_TESTED		5
#define	CAL_STATE_DELAY_TIMER		6
#define CAL_STATE_400_COMPLETE      7



/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
// Local static variables
static uchar cal_mgr_state = CAL_STATE_DELAY;
static uchar cal_timer;
static uint cal_average  = 0;


/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/





/*
+------------------------------------------------------------------------------
| Function:      CO_Cal_Init
+------------------------------------------------------------------------------
| Purpose:       Called from main at initialization to check current
|                CO calibration status that has already been loaded into
|                SYS_RamData Data Structure memory
|
|                Calibration Status Flags are set accordingly
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void CO_Cal_Init(void)
{
    // Check cal status for validity
    if(SYS_RamData.CO_Cal.status + SYS_RamData.CO_Cal.checksum != 
            STATUS_CSUM_VALID)
    {

        // Perform another Sys Init to attempt to restore Calibration information
        // Then repeat CO Cal Init.
        if(FALSE == SYS_Init() )
        {
            if(SYS_RamData.CO_Cal.status + SYS_RamData.CO_Cal.checksum != 
                    STATUS_CSUM_VALID)
            {
                // There is still an error in the calibration status.
                // Force a memory fault.
                FLT_Fault_Manager(FAULT_MEMORY);
            }

        }
        else
        {
             FLT_Fault_Manager(FAULT_MEMORY);
        }
    }

    switch (SYS_RamData.CO_Cal.status)
    {
        case 3:
            FLAG_CALIBRATION_COMPLETE = 1;
            break;

        case 1:
            FLAG_CALIBRATION_PARTIAL = 1;
            break;

        case 7:
            FLAG_CALIBRATION_UNTESTED = 1;
            break;

        default:
            // Zero Cal not Complete
            FLAG_CALIBRATION_NONE = 1;
            break;
    }

}



/*
+------------------------------------------------------------------------------
| Function:      CO_CAL_Manager_Task
+------------------------------------------------------------------------------
| Purpose:       CO Calibration Task State machine
|                During CO Calibration this is executed to perform CO 2 point
|                 calibration.
|
|                Calibration executed, tested for validity and saved in
|                non-volatile memory
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Next execution return time (in active mode timing tics)
+------------------------------------------------------------------------------
*/

uint CO_Cal_Manager_Task(void)
{

    switch (cal_mgr_state)
    {
        case CAL_STATE_DELAY:
            // LB Cal must be completed first
            if(!FLAG_LOW_BATTERY_CAL_MODE && !FLAG_NO_SMOKE_CAL)
            {
                cal_mgr_state++;
            }
            return  MAIN_Make_Return_Value(CAL_INITIAL_DELAY_1000_MS);

        case CAL_STATE_BEGIN:
            // Check to see if 400 PPM test is in progress.  
            if(STATUS_CAL_FINAL_TEST == SYS_RamData.CO_Cal.status)
            {
                // Proceed to Cal Wait Tested
                // Green Led manager will handle Green LED control
                cal_mgr_state = CAL_STATE_WAIT_TESTED;
                return	MAIN_Make_Return_Value(CAL_WAIT_TESTED_TIME_100_MS);
            }
            else
            {
                if(STATUS_CAL_150 == SYS_RamData.CO_Cal.status)
                {
                    cal_timer = CAL_150_STABIL_PERIOD;
                }
                else if(STATUS_CAL_ZERO == SYS_RamData.CO_Cal.status)
                {
                    cal_timer = CAL_ZERO_STABIL_PERIOD;
                }
                else
                {
                    // This alarm is already calibrated.  Just stay in this
                    // state.
                    break;
                }

                cal_mgr_state++;
                return	MAIN_Make_Return_Value(CAL_RETURN_1_SEC);
            }

        case CAL_STATE_STABILIZATION:
            if(0 == --cal_timer)
            {
                if(STATUS_CAL_150 == SYS_RamData.CO_Cal.status)
                {
                    cal_timer = CAL_150_CALIBRATION_PERIOD;
                }
                else
                {
                    cal_timer = CAL_ZERO_CALIBRATION_PERIOD;
                }

                cal_mgr_state++;	// Next state.
            }

            return MAIN_Make_Return_Value(CAL_UPDATE_RATE_30_SEC);

        case CAL_STATE_ACQUISITION:
            // Get the measured CO Average
            cal_average = CO_raw_avg_counts;

            if(0 == --cal_timer)
            {
                // Timer has timed out.  If this is a zero cal the current 
                // value of wCalibrationAverage is the offset.  If this is 
                // the 150 cal, calculate the calibration scale factor.

                // Check for errors in calibration based on the current 
                //  calibration mode.
                if(STATUS_CAL_ZERO == SYS_RamData.CO_Cal.status)
                {
                    // Check offset against limits.
                    if( (cal_average < CALIBRATION_OFFSET_MAXIMUM) &&
                         (cal_average > CALIBRATION_OFFSET_MINIMUM) )
                    {
                        // calibration status byte and checksum.
                        SYS_RamData.CO_Cal.offset_LSB =
                                (uchar)(cal_average & 0x00ff);
                        SYS_RamData.CO_Cal.offset_MSB =
                                (uchar)(cal_average / 256);
                        SYS_RamData.CO_Cal.status = STATUS_CAL_150;
                        SYS_RamData.CO_Cal.checksum = STATUS_CAL_150_CSUM;

                        // Save Data Structure in Flash
                        SYS_Save();

                        //CAL_MgrState = CAL_STATE_WAIT;
                        cal_timer = CAL_ERR_DELAY_PERIOD;
                        cal_mgr_state = CAL_STATE_DELAY_TIMER;
                    }
                    else
                    {
                        cal_timer = CAL_ERR_DELAY_PERIOD;
                        cal_mgr_state = CAL_STATE_DELAY_TIMER;
                        RLED_On();
                        FLAG_CAL_ERROR = 1;
                        return  MAIN_Make_Return_Value(CAL_RETURN_1_SEC);
                    }
                }
                else
                {
                    // Check scal against limits.
                    uint calval = LSB_TO_UINT(SYS_RamData.CO_Cal.offset_LSB);

                    uint scale_cts = (cal_average - calval);

                    if( (scale_cts < CALIBRATION_SLOPE_MAXIMUM) && 
                         (scale_cts > CALIBRATION_SLOPE_MINIMUM) )
                    {
                        #ifdef CONFIGURE_UNDEFINED
                            // Valid 150 cal. Calculate the scale factor 
                            //  (PPM/count)
                        
                            uint scale = 
                                    (((unsigned long)CAL_PPM_LEVEL_150 << 16) / 
                                        scale_cts) >> 8;
                        #endif
                        
                        // Peform more math result casting since compiler has
                        //  been treating some math results as signed values
                        unsigned long temp1 = 
                            ((unsigned long)CAL_PPM_LEVEL_150 << 16);
                        unsigned long temp2 = 
                            (unsigned long)((temp1 / scale_cts) >> 8);
                        
                        uint scale = (uint)temp2;
                        
                        
                        // If any rounding errors, error on High Side
                        scale++;

                        LSB_TO_UINT(SYS_RamData.CO_Cal.scale_LSB) = scale;
                        SYS_RamData.CO_Cal.status = STATUS_CAL_FINAL_TEST;
                        SYS_RamData.CO_Cal.checksum = 
                                            STATUS_CAL_FINAL_TEST_CSUM;
                        SYS_Save();

                        cal_timer = CAL_ERR_DELAY_PERIOD;
                        cal_mgr_state = CAL_STATE_DELAY_TIMER;
                    }
                    else
                    {
                        cal_timer = CAL_ERR_DELAY_PERIOD;
                        cal_mgr_state = CAL_STATE_DELAY_TIMER;
                        RLED_On();
                        FLAG_CAL_ERROR = 1;
                        return  MAIN_Make_Return_Value(CAL_RETURN_1_SEC);
                    }
                }

            }
            return  MAIN_Make_Return_Value(CAL_ACQ_RATE_10_SEC);

        case CAL_STATE_WAIT:

            break;

        case CAL_STATE_WAIT_TESTED:
            // If the alarm flag has been set, the cal. final test is over.
            if(FLAG_CO_ALARM_CONDITION)
            {
                // Update system data to reflect calibration status.
                SYS_RamData.CO_Cal.status = STATUS_CAL_COMPLETE;
                SYS_RamData.CO_Cal.checksum = STATUS_CAL_COMPLETE_CSUM;
                SYS_Save();
                cal_mgr_state = CAL_STATE_400_COMPLETE;
            }
            return  MAIN_Make_Return_Value(CAL_WAIT_TESTED_TIME_100_MS);
            

        case CAL_STATE_DELAY_TIMER:
            if(0 == --cal_timer)
            {
                if(FLAG_CAL_ERROR)
                {
                    // Set Fatal Fault and proceed to Wait State
                    RLED_Off();
                    FLT_Fault_Manager(FAULT_CALIBRATION);
                    cal_mgr_state = CAL_STATE_WAIT;
                }
                else
                {
                    // Chirp and remain in this state
                    SND_Do_Chirp();
                    cal_timer++;
                }
            }
            break;

        case CAL_STATE_400_COMPLETE:
            // Entering This State allows the Green LED to remain on
            //  since the Green LED monitors CAL_STATE_WAIT to turn it Off

            break;

        default:
            cal_mgr_state = CAL_STATE_WAIT;
            break;

    }
	
    return  MAIN_Make_Return_Value(CAL_UPDATE_RATE_30_SEC);
}


/*
+------------------------------------------------------------------------------
| Function:      CO_Cal_Get_State
+------------------------------------------------------------------------------
| Purpose:       returns the current calibration state to calling routine
|
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  current CO calibration state
+------------------------------------------------------------------------------
*/
uchar CO_Cal_Get_State(void)
{
    if(cal_mgr_state == CAL_STATE_400_COMPLETE)
    {
        return	CAL_STATE_FINAL_COMPLETE;
    }
    if(cal_mgr_state == CAL_STATE_WAIT)
    {
        return	CAL_STATE_WAITING;
    }
    else if(cal_mgr_state == CAL_STATE_DELAY_TIMER)
    {
        return	CAL_STATE_WAIT_DELAY;
    }
    else if(SYS_RamData.CO_Cal.status == STATUS_CAL_ZERO)
    {
        return	CAL_STATE_ZERO;
    }
    else if(SYS_RamData.CO_Cal.status == STATUS_CAL_150)
    {
        return	CAL_STATE_150;
    }
    else if(SYS_RamData.CO_Cal.status == STATUS_CAL_FINAL_TEST)
    {
        return	CAL_STATE_VERIFY;
    }

    return CAL_STATE_CALIBRATED;
}


#else   // #ifdef CONFIGURE_CO
// No CO Sensor, placeholders for CO Cal routines

void CO_Cal_Init(void)
{

}

uchar CO_Cal_Get_State(void)
{
    return FALSE;
}

uint CO_Cal_Manager_Task(void)
{
    return MAIN_Make_Return_Value(CAL_RETURN_61_SEC);
}

#endif  // #ifdef CONFIGURE_CO