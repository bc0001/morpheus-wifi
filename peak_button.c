/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// PEAK_BUTTON.C
// Debounces peak button and controls peak button flag
#define	PEAK_BUTTON_C


#include	"common.h"
#include	"peak_button.h"
#include	"sound.h"
#include	"serial.h"
#include        "esclight.h"
#include        "diaghist.h"


#ifdef	_CONFIGURE_PEAK_BUTTON

#define PEAK_BTN_CHECK_INTERVAL_MS  100     // Button Sample rate
#define PEAK_BTN_TIMEOUT            50      // 5 seconds in 100 ms units
#define PEAK_HOLD_INTERVAL_MS       2000    // 2 seconds

#define PEAK_BTN_NL_RATE            300     // ms Nightlight Intensity change rate
#define PEAK_BTN_NL_OFF_RATE        1000    // ms Nightlight Off Time

static unsigned char Peak_BTN_State = 0;
UCHAR Peak_BTN_Cnt;

//******************************************************************************
unsigned int Peak_BTN_Task(void)
{

    switch (Peak_BTN_State)
    {
        case	PEAK_BTN_STATE_IDLE:

                FLAG_PEAK_BUTTON_NOT_IDLE = 0 ;

                if( (FLAG_CALIBRATION_COMPLETE == 1) && (FLAG_CALIBRATION_UNTESTED == 0) && ( FLAG_VOICE_SEND == 0 ) )
                {
                    #ifdef _CONFIGURE_VOICE
                        // Enable Peak Button Input
                        TRISBbits.TRISB6 = 1;
                    #endif

                    if(PORT_PEAK_BTN == 1)
                    {
                        FLAG_PEAK_BUTTON_NOT_IDLE = 1 ;	// Leaving Idle State
                        Peak_BTN_State = PEAK_BTN_STATE_DEBOUNCE ;
                    }

                    FLAG_PEAK_BUTTON_EDGE_DETECT = 0 ;

                }

                break ;

        case	PEAK_BTN_STATE_DEBOUNCE:

                // See if button is still depressed
                if (PORT_PEAK_BTN == 1)
                {
                        // Audible Button Detected Feedback Beep
                        SND_Set_Chirp_Cnt(1) ;
                        // Reset sound task so that chirp happens right away.
                        MAIN_Reset_Task(TASKID_SOUND_TASK) ;

                        // Button is debounced.
                        Peak_BTN_State = PEAK_BTN_STATE_FUNCTION ;
                        Peak_BTN_Cnt = PEAK_BTN_TIMEOUT;

                        #ifdef _CONFIGURE_DIGITAL_DISPLAY
                            MAIN_Reset_Task(TASKID_DISP_UPDATE) ;
                        #endif

                        #ifdef _CONFIGURE_DIAG_HIST
                            Diag_Hist_Que_Push(HIST_PEAK_BTN_EVENT);
                        #endif


                }
                else
                {
                        // Button is up, go back and wait for another button press.
                        Peak_BTN_State = PEAK_BTN_STATE_IDLE ;
                        FLAG_PEAK_BUTTON_EDGE_DETECT = 0 ;
                }
                break ;



        case	PEAK_BTN_STATE_FUNCTION:
                {

                    // This is the Display Peak CO state
                    if (PORT_PEAK_BTN == 1)
                    {
                       #ifdef	_CONFIGURE_ESCAPE_LIGHT
                        #ifdef _CONFIGURE_ESC_LIGHT_TEST
                        // Test only (With AC Off only test the Escape Light)
                        if(FLAG_AC_DETECT == 0)
                        {
                            FLAG_ESC_LIGHT_ON = 1;
                            return Make_Return_Value(PEAK_BTN_CHECK_INTERVAL_MS);
                        }
                        #endif
                       #endif

                        // Display Peak for a Maximum of 5 Seconds
                        // If > 5 seconds, enter Night Light Intensity state
                        if(--Peak_BTN_Cnt == 0)
                        {
                           #ifdef   _CONFIGURE_ESCAPE_LIGHT

                            Peak_BTN_State = PEAK_BTN_STATE_NIGHTLIGHT ;
                            FLAG_ESC_LIGHT_ON = 0;

                            // Proceed to Nightlight State
                            Esc_Next_NL_Intensity(INIT_NL_STATE);

                           #else

                            // Button Timeout, return to Idle
                            Peak_BTN_State = PEAK_BTN_STATE_IDLE ;
                            FLAG_PEAK_BUTTON_EDGE_DETECT = 0 ;

                           #endif

                            return Make_Return_Value(PEAK_BTN_CHECK_INTERVAL_MS);
                        }

                        // Else, Remain in this state until Button RELEASED or Timeout
 
                    }
                    else
                    {
                       #ifdef	_CONFIGURE_ESCAPE_LIGHT
                            #ifdef _CONFIGURE_ESC_LIGHT_TEST
                                // Test only
                                FLAG_ESC_LIGHT_ON = 0;
                            #endif
                       #endif

                        // Button is released, go back and wait for another button press.
                        Peak_BTN_State = PEAK_BTN_STATE_IDLE ;
                        FLAG_PEAK_BUTTON_EDGE_DETECT = 0 ;

                        return Make_Return_Value(PEAK_HOLD_INTERVAL_MS);
                    }

                }
                break ;


        case    PEAK_BTN_STATE_NIGHTLIGHT:
                {
                     
                   #ifdef _CONFIGURE_ESCAPE_LIGHT
                    // This is the Display Peak CO state
                    if (PORT_PEAK_BTN == 1)
                    {
                        FLAG_NIGHT_LIGHT_ON = 1;
                       
                        // Each pass we cycle thru NL intensities
                        //  until button is released (to select it)
                        Esc_Next_NL_Intensity(NEXT_NL_STATE);

                        if(FLAG_NIGHT_LIGHT_DISABLED )
                            return Make_Return_Value(PEAK_BTN_NL_OFF_RATE);
                        else
                            return Make_Return_Value(PEAK_BTN_NL_RATE);
                    }
                    else
                    {
                        #ifdef _CONFIGURE_ESC_LIGHT_TEST
                            // Test only
                            FLAG_ESC_LIGHT_ON = 0;
                        #endif

                        // Use Current Intensity and turn off Light
                        FLAG_NIGHT_LIGHT_ON = 0;

                        // Button is released, go back and wait for another button press.
                        Peak_BTN_State = PEAK_BTN_STATE_IDLE ;
                        FLAG_PEAK_BUTTON_EDGE_DETECT = 0 ;
                    }
                   #endif

                }
                break;

    }

    return Make_Return_Value(PEAK_BTN_CHECK_INTERVAL_MS) ;

}

//*************************************************************************
unsigned char Peak_BTN_Get_State(void)
{
    return  Peak_BTN_State ;
}




//*************************************************************************
// else no peak Button Configured
//*************************************************************************

#else

unsigned int Peak_BTN_Task(void)
{
	return Make_Return_Value(32000) ;

} 

void Peak_Button_Init(void) 
{
	
}

unsigned char Peak_BTN_Get_State(void) 
{
	return(PEAK_BTN_STATE_IDLE)	;
}
#endif

