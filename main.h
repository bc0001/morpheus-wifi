/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  Workfile:   main.h
|  Author:     Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  header File for main.c module
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Date           	Name               Description
| ========      ===============    ==========================================
| 12/29/2014	 Bill Chandler     Initial draft
*/


#ifndef	MAIN_H
#define	MAIN_H

/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/
#define	TASKID_SOUND_TASK       0
#define TASKID_VOICE_TASK       1
#define	TASKID_BUTTON_TASK      2
#define	TASKID_PTT_TASK         3
#define	TASKID_BATTERY_TEST     4
#define	TASKID_DEBUG_TASK       5
#define	TASKID_ALARM_PRIORITY   6
#define	TASKID_COALARM          7
#define	TASKID_CALIBRATE        8
#define	TASKID_COMEASURE        9
#define	TASKID_TROUBLE_MANAGER  10
#define	TASKID_RLED_MANAGER     11
#define	TASKID_GLED_MANAGER     12
#define	TASKID_ALED_MANAGER     13
#define	TASKID_LED_BLINK_TASK   14
#define TASKID_ESCAPE_LIGHT     15

#define TASKID_PHOTO_SMOKE      16
#define TASKID_INTERCONNECT     17
#define TASKID_LIGHT_TASK       18
#define TASKID_APP_STAT_MONITOR 19
#define TASKID_STROBE_TASK      20


//************************************************************************
// Interrupt Defines

// Global Interrupt Enable
#define GLOBAL_INT_ENABLE       INTCONbits.GIE
#define PERIPERAL_INT_ENABLE    INTCONbits.PEIE

// Global Int on Change Enable (for all IOC bits)
#define IOC_INTERRUPT_ENABLE    PIE0bits.IOCIE

// Global Int on Change Flag (for any IOC bit)
#define IOC_INTERRUPT_FLAG      PIR0bits.IOCIF


// Timer 0 Interrupts (CCI ms Timer)
#define TMR0_INT_FLAG           INTCONbits.TMR0IF
#define TMR0_INT_ENABLE         INTCONbits.TMR0IE


// System Clock - 16 MHZ, OSCFRQ, Internal Osc (0x0000 0101 = 16MHZ)
#define OSC_16_MHZ      0b00000101

// OSCCON1 settings (for OSC switching))
#define OSC_HF_INT_OSC  0b01100000
#define OSC_32KHZ_XTAL  0b01000000

// OSCCON3 settings (for OSC power)
#define OSC_SOSC_PWR_HI 0b01000000

// Enable the Secondary Oscillator select (0x0000 1000)
#define OSC_ENABLE_SOSC_SELECT        0b00001000

// Enable the 31 KHz Internal Oscillator (0x0001 0000)
#define OSC_LO_FREQ_31_KHZ_SELECT   0b00010000


// TIMER1 Definitions
/*
 * Notes:
 * Setting the RD16 bit configures Timer1 to read and write all 16 bits 
 * of data to and from TMR1L and TMR1H simultaneously using a buffer on 
 * the TMR1H values.  When in 16-bit mode, when a read of TMR1L is performed, 
 * the current value of TMR1H is loaded into the TMR1H buffer and any reads 
 * of TMR1H will read from the TMR1H buffer.  When in 16-bit mode, when a 
 * write of TMR1H is performed, the value is written to the TMR1H buffer 
 * and is only loaded into the actual TMR1H register when TMR1L is written to.
 * This allows all 16-bits to be read or written at the same time.  
 * In addition, when in 16-bit mode, writes to the TMR1H register do not 
 * reset the Timer1 pre-scaler (as they do in 8-bit mode), as the TMR1H buffer 
 * is not actually written until TMR1L is written to (at which point the 
 * Timer1 pre-scaler would indeed reset).  
 */
// Timer1, Pre-scale 1:1,SYNC OFF, Timer ON, RD16=0 (16 bit read/write not used)
#define T1_CON_INIT     0b00000101

// Timer1 Clock = T1CHIPPS  (External XTAL In, RC0 Pin)
// Selects Peripheral Pin Select as Clock Input to T1
// PPS defaults to RCO pin Secondary OSC

// For xtal circuit we need to use SOSC clock selection
// T1CHIPPS is for digital external clock inputs only (won't drive a XTAL))
#define T1_CLK_INIT_SOSC     0b00000110
#define T1_CLK_INIT_LFOSC    0b00000100

#define TIMER_ACTIVE_MINIMUM_18_SEC    (uint)1800
#define TIMER_ACTIVE_MINIMUM_10_SEC    (uint)1000
#define TIMER_ACTIVE_MINIMUM_6_SEC     (uint)600
#define TIMER_ACTIVE_MINIMUM_3_SEC     (uint)300
#define TIMER_ACTIVE_MINIMUM_2_SEC     (uint)200
#define TIMER_ACTIVE_MINIMUM_1500_MS   (uint)150
#define TIMER_ACTIVE_MINIMUM_1_SEC     (uint)100
#define TIMER_ACTIVE_MINIMUM_200_MS    (uint)20
#define TIMER_ACTIVE_MINIMUM_120_MS    (uint)12
#define TIMER_ACTIVE_MINIMUM_100_MS    (uint)10
#define TIMER_ACTIVE_MINIMUM_50_MS     (uint)5
#define TIMER_ACTIVE_MINIMUM_40_MS     (uint)4
#define TIMER_ACTIVE_MINIMUM_30_MS     (uint)3
#define TIMER_ACTIVE_MINIMUM_20_MS     (uint)2


#define TIMER_WHILE_MS_10               (uint)10
#define TIMER_WHILE_MS_25               (uint)25
#define TIMER_WHILE_MS_50               (uint)50
#define TIMER_WHILE_MS_100              (uint)100
#define TIMER_WHILE_MS_250              (uint)250
#define TIMER_WHILE_MS_500              (uint)500
#define TIMER_WHILE_MS_1000             (uint)1000



//#define CONFIGURE_DIAG_TIMER



/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/
extern uchar MAIN_OneHourTic;
extern uchar MAIN_OneMinuteTic;
extern uchar MAIN_OneSecTimer;
extern uint MAIN_ActiveTimer;


#ifdef	CONFIGURE_INTERCONNECT
    #ifdef CONFIGURE_WIRELESS_MODULE
        extern uint MAIN_100_MS_Timer;
    #endif
#endif

#ifdef CONFIGURE_MONITOR_INT
    extern uint MAIN_Monitor_Int1;
    extern uint MAIN_Monitor_Int2;
#endif

#ifdef CONFIGURE_DBUG_STACK_TEST
    // Test Only
    extern uchar MAIN_Stack_Ptr_Peak;
    extern uchar MAIN_Stack_Ptr_Now;
#endif



/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
void MAIN_Reset_Task(uchar taskID);
void MAIN_Reschedule_Int_Active(uchar Task_ID, uint time);
uint MAIN_Make_Return_Value(uint milliseconds);

#ifdef CONFIGURE_ID_MANAGEMENT
    void MAIN_Output_Unit_ID(void);
#endif

// TODO: Undefine Later, BC: Test Only
#ifdef CONFIGURE_UNDEFINED
     void MAIN_Init_While_Timer(uint timer_ms);
    uchar MAIN_While_Timer(void);
#endif

void MAIN_TP_On(void);
void MAIN_TP_Off(void);
void MAIN_TP_Toggle(void);
void MAIN_TP2_On(void);
void MAIN_TP2_Off(void);
void MAIN_TP2_Toggle(void);

#ifdef CONFIGURE_ACTIVE_TIMEOUT_TIMER
    void MAIN_Reset_Active_Mode_Timer(void);
    void MAIN_Active_Mode_Timer_Timeout(void);
#endif


#ifdef CONFIGURE_DIAG_TIMER
    void MAIN_Init_Diag_Timer(void);
#endif

#ifdef CONFIGURE_TEST_CODE
    void log_seq(uchar seqid);
    void log_out(void);
#endif


#endif  // MAIN_H
