/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        app.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for app.c module
|
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

#ifndef APP_H
#define	APP_H


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

#ifdef CONFIGURE_WIRELESS_MODULE
    // Configurations for Test
    #define CONFIGURE_APP_COMM_UNTESTED
    //#define CONFIGURE_APP_COMM_VERSION
    #define CONFIGURE_APP_COMM_FINAL_TEST
    #define CONFIGURE_APP_WIRELESS_SUPERVISION
#endif


//******************************************************************************
//******************************************************************************
//******************************************************************************
/* Define the list of Supported AMP CCI Messages
 * If Message Define is Commented out, an Unsupported Message response
 *  is returned via CCI for that Message
 */

// Define Supported Messages

// Status Messages
#define AMP_STATUS_SUPPORTED
#define AMP_STATUS1_SUPPORTED
#define AMP_STATUS2_SUPPORTED
#define AMP_STATUS3_SUPPORTED
#define AMP_COMM_STATUS_SUPPORTED
#define AMP_COMM_STATUS_ACK_SUPPORTED

// Request Messages
#define AMP_PING_SUPPORTED
#define AMP_COMM_STATUS_REQ_SUPPORTED
#define AMP_GET_LOGICAL_ADDRESS_SUPPORTED
#define AMP_REQUEST_AMP_VER_SUPPORTED

#define AMP_VERSION_REQ_SUPPORTED

#define AMP_CAPABILITY1_SUPPORTED
#define AMP_CAPABILITY2_SUPPORTED
#define AMP_REQ_BAT_VOLT_SUPPORTED
#define AMP_MODEL_NUMBER_SUPPORTED
#define AMP_SMOKE_READING_SUPPORTED
#define AMP_STATUS_REQUEST_SUPPORTED

#define AMP_SMOKE_SUPPORTED

#define AMP_ERROR_CODE_SUPPORTED
#define AMP_LIFE_SUPPORTED
#define AMP_ID_SUPPORTED


#ifdef CONFIGURE_CO
    #define AMP_C0_PPM_SUPPORTED
#endif

#ifdef  CONFIGURE_WIRELESS_ALARM_LOCATE
    #define AMP_LOCATE_SUPPORTED
#endif

// Response Only Messages
#define AMP_NOT_SUPPORTED_SUPPORTED
#define AMP_PING_ACK_SUPPORTED
#define AMP_COMM_STATUS_ACK_SUPPORTED

// Reset type definitions
#define APP_INIT_ACTION_PWR_RST         0
#define APP_INIT_ACTION_SOFT_RST        1
#define APP_INIT_ACTION_NOT_PWR_RST     2

#ifdef CONFIGURE_TEMPERATURE
    #define AMP_TEMPERATURE_SUPPORTED
#endif

#define APP_ALARM_STATUS_CLEAR          (uchar)0


#define AMP_FUNCTION_SUPPORTED


//#define AMP_PTT_SUPPORTED

//#define AMP_COMM_INIT_SUPPORTED
//#define AMP_COMM_LEARN_SUPPORTED
//#define AMP_ACOUSTIC_STATUS_SUPPORTED
//#define AMP_ACOUSTIC_STATUS_REQ_SUPPORTED
//#define AMP_SET_ID_SUPPORTED
//#define AMP_RF_TEST_STATUS_SUPPORTED
//#define AMP_RF_PACKET_SUPPORTED
//#define AMP_SERIAL_DATA_SUPPORTED
//#define AMP_FUNCTION_SUPPORTED
//#define AMP_HUMIDITY_SUPPORTED
//#define AMP_SOUND_SUPPORTED

//******************************************************************************
//******************************************************************************
//******************************************************************************

#define APP_FUNCTION_ESC_LIGHT_ON   (uchar)0x01

#define APP_MSG_RECEIVE (uchar)0x01
#define APP_MSG_TRANSMIT (uchar)0xFF

#ifdef CONFIGURE_WIRELESS_CCI_DEBUG
    #define CQLENGTH (uint)150
#endif


//  CCI COMM_STATUS Flags (inputs)
#define FLAG_COMM_JOIN_COMPLETE         APP_Remote_COMM_Status._0
#define FLAG_COMM_JOIN_IN_PROGRESS      APP_Remote_COMM_Status._1
#define FLAG_COMM_ONLINE                APP_Remote_COMM_Status._2
#define FLAG_COMM_UNTESTED             	APP_Remote_COMM_Status._5
#define FLAG_COMM_COORDINATOR           APP_Remote_COMM_Status._6
#define FLAG_COMM_RFD                  	APP_Remote_COMM_Status._7

// Alarm COMM_STATUS Flags (outputs)
#define FLAG_COMM_ENABLE_JOIN           APP_Current_COMM_Status._3
#define FLAG_COMM_RESET_OOB             APP_Current_COMM_Status._4
#define FLAG_COMM_COORDINATOR_OUT       APP_Current_COMM_Status._6

//******************************************************************************
// Alarm Status sent to the RF Module
//  CCI ALARM_STATUS Flags (outputs)
#define FLAG_STATUS_ALM_LB_ACTIVE       APP_Current_ALM_Status._0   //Bi-Dir
#define FLAG_STATUS_ALM_SMK_ACTIVE      APP_Current_ALM_Status._1   // Output
#define FLAG_STATUS_ALM_CO_ACTIVE       APP_Current_ALM_Status._2   // Output
#define FLAG_STATUS_ALM_HW_INT_ACTIVE   APP_Current_ALM_Status._5   //Bi-Dir     
#define FLAG_STATUS_ALM_FAULT_ACTIVE    APP_Current_ALM_Status._6   //Bi-Dir

// Alarm Status received from the RF Module
//  CCI REMOTE_STATUS Flags (inputs)
#define FLAG_STATUS_RMT_LB_ACTIVE       APP_Remote_ALM_Status._0    //Bi-Dir
#define FLAG_STATUS_RMT_SMK_RCVD      	APP_Remote_ALM_Status._3    // Input
#define FLAG_STATUS_RMT_CO_RCVD         APP_Remote_ALM_Status._4    // Input
#define FLAG_STATUS_RMT_HW_INT_ACTIVE   APP_Remote_ALM_Status._5    //Bi-Dir
#define FLAG_STATUS_RMT_FAULT_ACTIVE    APP_Remote_ALM_Status._6    //Bi-Dir
#define FLAG_STATUS_RMT_PTT_RCVD        APP_Remote_ALM_Status._7

// Special Remote Flags to indicate received HW Alarm status messages
#define FLAG_STATUS_RMT_SMK_HW_RCVD     APP_Remote_ALM_Status2._0
#define FLAG_STATUS_RMT_CO_HW_RCVD      APP_Remote_ALM_Status2._1



//******************************************************************************
// Status 1 Flags

//  CCI ALARM_STATUS_1 Flags
#define FLAG_STATUS_1_REMOTE_HUSH       APP_ALM_Status_1._0     // to Host
#define FLAG_STATUS_1_ALARM_CANCEL      APP_ALM_Status_1._1
#define FLAG_STATUS_1_HUSH_ACTIVE       APP_ALM_Status_1._2     // not used
#define FLAG_STATUS_1_SELFTEST_ACTIVE   APP_ALM_Status_1._3
#define FLAG_STATUS_1_TXRX_DISABLE_REQ  APP_ALM_Status_1._4
#define FLAG_STATUS_1_TXRX_DISABLED     APP_ALM_Status_1._5
#define FLAG_STATUS_1_FINAL_TESTED      APP_ALM_Status_1._6
#define FLAG_STATUS_1_7                 APP_ALM_Status_1._7


//******************************************************************************
// Status 2 Flags

//  CCI ALARM_STATUS_2 Flags
#define FLAG_STATUS_2_TEMP_INCR         APP_ALM_Status_2._0
#define FLAG_STATUS_2_TEMP_DROP         APP_ALM_Status_2._1
#define FLAG_STATUS_2_WATER             APP_ALM_Status_2._2
#define FLAG_STATUS_2_TAMPER            APP_ALM_Status_2._3
#define FLAG_STATUS_2_SOUND_DETECT      APP_ALM_Status_2._4
#define FLAG_STATUS_2_REMOTE_LB         APP_ALM_Status_2._5 // unused
#define FLAG_STATUS_2_FAULT_RESET       APP_ALM_Status_2._6 // unused
#define FLAG_STATUS_2_PANIC             APP_ALM_Status_2._7


//******************************************************************************
// Status 3 Flags

//  CCI ALARM_STATUS_3 Flags
#define FLAG_STATUS_3_GAS               APP_ALM_Status_3._0
#define FLAG_STATUS_3_INTRUDER          APP_ALM_Status_3._1
#define FLAG_STATUS_3_WEATHER           APP_ALM_Status_3._2
#define FLAG_STATUS_3_HEAT_ALARM        APP_ALM_Status_3._3
#define FLAG_STATUS_3_FREEZE            APP_ALM_Status_3._4
#define FLAG_STATUS_3_GEN_ALERT         APP_ALM_Status_3._5
#define FLAG_STATUS_3_6                 APP_ALM_Status_3._6
#define FLAG_STATUS_3_7                 APP_ALM_Status_3._7


#define APP_CCI_HS_ATTEMPTS_3   (uchar)3


/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/

// Alarm Status sent to the RF Module
extern volatile Flags_t APP_Current_ALM_Status;
extern volatile Flags_t APP_Previous_ALM_Status;

// Alarm Status received from the RF Module
extern volatile Flags_t APP_Remote_ALM_Status;
extern volatile Flags_t APP_Remote_ALM_Status2;


// Status 1 Flags
extern persistent volatile Flags_t APP_ALM_Status_1;
// Status 2 Flags
extern volatile Flags_t APP_ALM_Status_2;
// Status 3 Flags
extern volatile Flags_t APP_ALM_Status_3;


// COMM Status Outputs sent to the RF Module
extern volatile Flags_t APP_Current_COMM_Status;
extern volatile Flags_t APP_Previous_COMM_Status;

// COMM Status Inputs received from the RF Module
extern persistent volatile Flags_t APP_Remote_COMM_Status;

extern persistent volatile uchar APP_Rf_Network_Size;
extern persistent volatile uchar APP_RF_Module_FW_Version;

extern volatile uchar APP_CCI_Retry_Count;
extern union tag_CCI_Packet AMP_RetryPacket;

/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/

#ifdef CONFIGURE_WIRELESS_MODULE
    uchar APP_Process_Response(union tag_CCI_Packet *SentPacket,
                               union tag_CCI_Packet *RxPacket);

    union tag_CCI_Packet APP_Process_Request(union tag_CCI_Packet *);

    void APP_PTT_Arm( void );
    void APP_PTT_Local( void );
    
    uchar APP_Send_Clr_Alarm_Locate( void );
    
    void APP_Request_Comm_Status( void );    
    void APP_Request_Status1(void);

    void APP_Reset_Fault(void);

    void APP_LP_CCI_Timer(void);

    #ifdef CONFIGURE_WIRELESS_CCI_DEBUG
        void APP_CCI_QInit (void);
        uint APP_CCI_Exque(void);
        uchar APP_CCI_QPop(void);
        void APP_CCI_QPush(uchar c);
    #endif

    uint APP_Status_Monitor(void);
    
    #ifdef CONFIGURE_WIFI
        void APP_Status_Monitor_Reset(void);
        void APP_Enable_WiFi_Port(void);
    #endif
    
    void APP_Init(uchar action);
    void APP_Request_Net_Size(void);
    void APP_Request_RF_Version( void );

    uchar APP_Update_Status(void);
    void APP_Request_Local_Id(void);
    uchar APP_Get_Local_Id( void );
    uchar APP_Send_Alarm_Locate(void);
    void APP_Request_Status(void);
    
#ifdef	CONFIGURE_INTERCONNECT    
    void APP_Init_Slave_To_Remote_Alarm_Timer(uchar time);
    void APP_Slave_To_Remote_Alarm_Timer(void);
    void APP_Init_Rmt_PTT_Dly_Timer(void);
    void APP_Rmt_PTT_Timer(void);
#endif
    
    void APP_Init_Min_Alarm_Timer(void);
    void APP_Min_Alarm_Timer(void);
    
    
#ifdef CONFIGURE_UNDEFINED    
    void APP_PTT_Clear( void );
#endif
    

#else
    uint APP_Status_Monitor(void);
#endif    
    
#endif	/* APP_H */

