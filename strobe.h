/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        strobe.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for strobe.c module
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


#ifndef STROBE_H
#define	STROBE_H

#ifdef	__cplusplus
extern "C" {
#endif


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/
    
#ifdef CONFIGURE_STROBE_SYNC
    
    #ifdef CONFIGURE_STROBE
        #define STROBE_PULSE_ON         LAT_ESC_LIGHT_PIN = 1;
        #define STROBE_PULSE_OFF        LAT_ESC_LIGHT_PIN = 0;
        #define STROBE_PULSE_OUTPUT     TRISCbits.TRISC2 = 0;

    #else
        // No Strobe I/O, so nop these defines
        #define STROBE_PULSE_ON         _nop();
        #define STROBE_PULSE_OFF        _nop();
        #define STROBE_PULSE_OUTPUT     _nop();
    #endif

    

/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
    
    unsigned int STROBE_Task(void);





#ifdef	__cplusplus
}
#endif

#endif //#define CONFIGURE_STROBE_SYNC

#endif  // STROBE_H 

