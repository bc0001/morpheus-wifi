/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        io.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for Pic I/O definitions
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

#ifndef IO_H
#define	IO_H

/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

        // Define IO bit assignments
        // Port A
#define PORT_CO_VOLT_PIN            PORTAbits.RA0
#define PORT_BAT_VOLT_PIN           PORTAbits.RA1
#define PORT_AC_DETECT_PIN          PORTAbits.RA2
#define PORT_PHOTO_OUT_PIN          PORTAbits.RA3
#define PORT_ESC_LIGHT_PIN          PORTAbits.RA4
#define PORT_TEMP_VOLT_PIN          PORTAbits.RA5
#define PORT_IRLED_DRIVE_PIN        PORTAbits.RA6
#define PORT_VBOOST_PIN             PORTAbits.RA7



        //Latch A
#define LAT_ESC_LIGHT_PIN           LATAbits.LATA4
#define LAT_IRLED_DRIVE_PIN         LATAbits.LATA6
#define LAT_VBOOST_PIN              LATAbits.LATA7

        // Port B
#define PORT_INT_IN_PIN             PORTBbits.RB0
#define PORT_CCI_DATA_PIN           PORTBbits.RB1
#define PORT_CCI_CLK_PIN            PORTBbits.RB2
#define PORT_TEST_BTN_PIN           PORTBbits.RB3
#define PORT_LIGHT_SENSE_PIN        PORTBbits.RB4
#define PORT_PHOTO_PWR_PIN          PORTBbits.RB5
#define PORT_ICSP_CLK_PIN           PORTBbits.RB6
#define PORT_ICSP_DATA_PIN          PORTBbits.RB7
#define PORT_LB_CAL_PIN             PORTBbits.RB7

        // Latch B
#define LAT_CCI_DATA_PIN            LATBbits.LATB1
#define LAT_CCI_CLK_PIN             LATBbits.LATB2
#define LAT_LIGHT_SENSE_PIN         LATBbits.LATB4
#define LAT_PHOTO_PWR_PIN           LATBbits.LATB5
#define LAT_ICSP_CLK_PIN            LATBbits.LATB6
#define LAT_ICSP_DATA_PIN           LATBbits.LATB7


        // Port C
#define PORT_T1OSC_O_PIN            PORTCbits.RC0
#define PORT_T1OSC_I_PIN            PORTCbits.RC1
#define PORT_GREEN_LED_PIN          PORTCbits.RC2
#define PORT_VOICE_EN_PIN           PORTCbits.RC3
#define PORT_VOICE_CLK_PIN          PORTCbits.RC4
#define PORT_VOICE_DATA_PIN         PORTCbits.RC5
#define PORT_TX_PIN                 PORTCbits.RC6
#define PORT_RX_PIN                 PORTCbits.RC7


        // Latch C
#define LAT_GREEN_LED_PIN           LATCbits.LATC2
#define LAT_RC3_PIN                 LATCbits.LATC3
#define LAT_VOICE_CLK_PIN           LATCbits.LATC4
#define LAT_VOICE_DATA_PIN          LATCbits.LATC5

#define TRIS_GREEN_LED              TRISCbits.TRISC2

        // Port D
#define PORT_DISABLE_IN_PIN         PORTDbits.RD0
#define PORT_RED_LED_PIN            PORTDbits.RD1
#define PORT_HORN_DISABLE_PIN       PORTDbits.RD2
#define PORT_CO_TEST_PIN            PORTDbits.RD3
#define PORT_RD4_PIN                PORTDbits.RD4
#define PORT_INT_LO_DRV_PIN         PORTDbits.RD5
#define PORT_INT_HI_DRV_PIN         PORTDbits.RD6
#define PORT_BAT_TEST_PIN           PORTDbits.RD7

        // Latch D

#define LAT_RED_LED_PIN             LATDbits.LATD1
#define LAT_HORN_DISABLE_PIN        LATDbits.LATD2
#define LAT_CO_TEST_PIN             LATDbits.LATD3
#define LAT_RD4_PIN                 LATDbits.LATD4
#define LAT_INT_LO_DRV_PIN          LATDbits.LATD5
#define LAT_INT_HI_DRV_PIN          LATDbits.LATD6
#define LAT_BAT_TEST_PIN            LATDbits.LATD7

#define TRIS_RED_LED                TRISDbits.TRISD1

        // Port E
#define PORT_VBOOST_FB_PIN          PORTEbits.RE0
#define PORT_IR_LED_SENSE_PIN       PORTEbits.RE1
#define PORT_INT_OUT_PIN            PORTEbits.RE2

        // Latch E
#define LAT_INT_OUT_PIN             LATEbits.LATE2


// Define Debug Pin based upon Model ( Pins 1 and 2 of programming header)
#define LAT_DEBUG_TP1_PIN           LATBbits.LATB6
#define LAT_DEBUG_TP2_PIN           LATBbits.LATB7



// *****************************************************************
// IO defines for CCI INTERFACE

#define mCCI_DATA_PIN       LAT_CCI_DATA_PIN
#define mCCI_CLOCK_PIN      LAT_CCI_CLK_PIN
#define mCCI_DATA_PIN_DIR   TRISBbits.TRISB1
#define mCCI_CLOCK_PIN_DIR  TRISBbits.TRISB2
#define mCCI_DATA_IN_PIN    PORT_CCI_DATA_PIN
#define mCCI_CLOCK_IN_PIN   PORT_CCI_CLK_PIN

// Direction
#define OUTPUT_PIN          0
#define INPUT_PIN           1


// *****************************************************************
// IO DEFINES FOR HW Interconnect
#define INT_IN_DISABLE_IOC      IOCBPbits.IOCBP0 = 0;
#define INT_IN_ENABLE_IOC       IOCBPbits.IOCBP0 = 1;


/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/
// Not applicable

/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
// Not applicable





#endif	/* IO_H */

