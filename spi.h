/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// SPI.H


#ifndef	_SPI_H
#define	_SPI_H

	
	void SPI_Write(unsigned char address, unsigned char data) ;	
	void SPI_Init(void) ;
	unsigned char SPI_Read(unsigned char address) ;
	UCHAR SPI_Update_Reg(UCHAR address, UCHAR val, UCHAR mask) ;
		
	#ifdef	_SPI_C
		void SPI_Send(unsigned char data) ;
		
	#else
		#ifdef	_CONFIGURE_FAST_SPI
			extern struct _tag_Analog_Regs Analog_Shadow_Regs ;
		#endif
	#endif


#endif


