/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        a2d.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Routines for Analog to digital support
|
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include    "common.h"
#include    "a2d.h"
#include    "diaghist.h"
#include    "app.h"
#include    "fault.h"
#include    "alarmmp.h"


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

#define	AC_OFF_DETECT_THRESHOLD     (uint16_t)300
#define	AC_ON_DETECT_THRESHOLD      (uint16_t)305


// Define AD Register Settings
#define AD_CON0_OFF_LEFT_JUST       0b00000000       //AD OFF, Left
#define AD_CON0_OFF_RIGHT_JUST      0b00000100       //AD OFF, Right
#define AD_CON0_ON_LEFT_JUST        0b10000000       //AD ON, Left
#define AD_CON0_ON_RIGHT_JUST       0b10000100       //AD ON, Right

#define AD_CLK_FOSC_DIV_16          0b00000111       // Fosc/16           
#define AD_REF_SELECT_FVR           0b00000011       // select FVR
#define AD_REF_SELECT_VDD           0b00000000       // select VDD

// Define FVR settings  (FVRCON)
#define AD_FVR_OFF                  0b00000000       //FVR OFF
#define AD_FVR_1024                 0b10000001       //FVR AD 1.024
#define AD_FVR_2048                 0b10000010       //FVR AD 2.048




#ifdef CONFIGURE_TEMPERATURE
    #ifdef CONFIGURE_TEMP_DEG_F
        // 80 Degrees C
        #define TEMP_AD_MAX         (uint16_t)254

        // -11 Degrees C
        #define TEMP_AD_MIN         (uint16_t)980

        #define A2D_MULT_BY_1000        (uint16_t)1000
        #define A2D_TEMP_EQU_DIVISOR    (uint16_t)5086 
        #define A2D_TEMP_EQU_OSET       (uint16_t) 215     

        //
        // Define an adjustment value in DegF if needed
        //  (for any data collection error)
        #define TEMP_TWEEK  (uchar)0
    #endif
#endif

#define A2D_ACQ_TIME_25US    (uint16_t)25
#define A2D_ON_TIME_50US     (uint16_t)50

/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/
#ifdef CONFIGURE_TEMPERATURE
    #ifdef CONFIGURE_TEMP_DEG_F
        static uchar a2d_temp_deg_f = 0;
    #endif
#endif

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
#ifdef	CONFIGURE_ACDC
    static uchar ac_detect_count = 2;
#endif


/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/
void a2d_convert(void);


/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/


/*
+------------------------------------------------------------------------------
| Function:      A2D_On
+------------------------------------------------------------------------------
| Purpose:       Enable AD Converter
|                Some measurements we want to pre-enable A/D to shorten
|                conversion time
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void A2D_On(void)
{
    ADCON0bits.ADON = 1;
}

/*
+------------------------------------------------------------------------------
| Function:      A2D_Off
+------------------------------------------------------------------------------
| Purpose:       Turn Off AD Converter
|                Saves Power by turning Off A/D converter when not needed
|                
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void A2D_Off(void)
{
    ADCON0bits.ADON = 0;
}


/*
+------------------------------------------------------------------------------
| Function:      A2D_Convert_8Bit
+------------------------------------------------------------------------------
| Purpose:       An 8 bit conversion is done with a right justified result.
|                This supports DC Worry-free algorithm which only uses 8 bit
|                smoke measurements.
|                Requires register declarations found in pic16lf18877.h
|
+------------------------------------------------------------------------------
| Parameters:    Channel to convert is passed in channel (0x00 - 0x3F).
|                channel constants ADC_CHAN defined in a2d.h
+------------------------------------------------------------------------------
| Return Value:  8 Bit Result is returned (most significant 8 Bits of Result)
+------------------------------------------------------------------------------
*/
unsigned char A2D_Convert_8Bit(uchar channel)
{
    #ifdef CONFIGURE_DEBUG_A2D_SAMPLES
           MAIN_TP2_On();
    #endif

    // Set AD CLock Speed       
    ADCLK = AD_CLK_FOSC_DIV_16;            

    if(A2D_FVR_CHAN == channel)
    {
        // Set FVR for 1.024 Voltage in preparation of 
        //  measuring the FVR 1.024 volts
        FVRCON = AD_FVR_1024;
        
        do
        {
            // Wait for FVR stabilization

        }while(0 == FVRCONbits.FVRRDY);

        // Wait the required acquisition time, at least 20 microseconds.
        PIC_delay_us(A2D_ACQ_TIME_25US);
    }

    if(FLAG_AD_USE_2048_FVR)
    {
        FLAG_AD_USE_2048_FVR = 0;
        // Enable 2.028 FVR
        FVRCON = AD_FVR_2048;

        ADREF = AD_REF_SELECT_FVR;  // Select AD Ref for measurement

        // Select the Channel and set left justification
        ADPCH = channel;    //Set Input Channel
        ADCON0 = AD_CON0_OFF_LEFT_JUST;      //ADOFF, Left Justified
        
        
        do
        {
            // Wait for FVR stabilization

        }while(0 == FVRCONbits.FVRRDY);

    }
    else
    {
        ADREF = AD_REF_SELECT_VDD;  // Select AD Ref for measurement
        
        // Setup Channel and A/D ON
        if( (ADCON0bits.ADON) &&
             (ADPCH == channel) )
        {
            // AD already ON with same channel as previous sample 
            // No Acquisition Time is required
            ADCON0 = AD_CON0_ON_LEFT_JUST;
        }
        else
        {
            // Select the Channel and justification with A/D left off
            
            //  This will add required acquisition time in A2D_Convert
            // Select the Channel and set left justification
            ADPCH = channel;    //Set Input Channel
            ADCON0 = AD_CON0_OFF_LEFT_JUST;      //ADOFF, Left Justified
        }

    }
           
    // Perform A/D conversion       
    a2d_convert();

    // Return Ref to Standard reference so it will be pre-stabilized
    //   for next upcoming Non-FVR measurements
    ADREF = AD_REF_SELECT_VDD;  // Select AD Ref for measurement

    #ifdef CONFIGURE_DEBUG_A2D_SAMPLES
           MAIN_TP2_Off();
    #endif

    // Return the 8 bit result
    return (ADRESH);
}


/*
+------------------------------------------------------------------------------
| Function:      A2D_Convert_10Bit
+------------------------------------------------------------------------------
| Purpose:       A 10 bit conversion is done with a right justified result.
|                Requires register declarations found in pic16lf18877.h
|
+------------------------------------------------------------------------------
| Parameters:    Channel to convert is passed in channel (0x00 - 0x3F).
|                channel constants ADC_CHAN defined in a2d.h
+------------------------------------------------------------------------------
| Return Value:  10 Bit Result is returned
+------------------------------------------------------------------------------
*/
uint A2D_Convert_10Bit(uchar channel)
{
    #ifdef CONFIGURE_DEBUG_A2D_SAMPLES
           MAIN_TP2_On();
    #endif

    // Set AD CLock Speed       
    ADCLK = AD_CLK_FOSC_DIV_16;            

    if(A2D_FVR_CHAN == channel)
    {
        // Set FVR for 1.024 Voltage in preparation of 
        //  measuring the FVR 1.024 volts
        FVRCON = AD_FVR_1024;
        
        do
        {
            // Wait for FVR stabilization

        }while(0 == FVRCONbits.FVRRDY);

        // Wait the required acquisition time, at least 20 microseconds.
        PIC_delay_us(A2D_ACQ_TIME_25US);
    }

    ADREF = AD_REF_SELECT_VDD;  // Select VDD AD Ref for measurement

    // Setup Channel and A/D ON
    if( (ADCON0bits.ADON) &&
         (ADPCH == channel) )
    {
        // AD already ON with channel already set 
        // No Acquisition Time is required
        ADCON0 = AD_CON0_ON_RIGHT_JUST;
    }
    else
    {
        // Select the Channel and justification with A/D off

        //  This will add required acquisition time in A2D_Convert
        // Select the Channel and set left justification
        ADPCH = channel;    //Set Input Channel
        ADCON0 = AD_CON0_OFF_RIGHT_JUST;      //ADOFF, Right Justified
    }

    // Perform A/D conversion       
    a2d_convert();

    #ifdef CONFIGURE_DEBUG_A2D_SAMPLES
           MAIN_TP2_Off();
    #endif

    // Return full 10 bit result
    return (ADRES);
}


/*
+------------------------------------------------------------------------------
| Function:      a2d_convert
+------------------------------------------------------------------------------
| Purpose:       Complete the Conversion initialized for 8 bit or 10 
|                bit conversion
|                Requires register declarations found in pic16lf18877.h
+------------------------------------------------------------------------------
| Parameters:    none
|                
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void a2d_convert(void)
{
    // Pre-enabled if ADON is already set and we can
    //   skip the AD ON Time
    if(!ADCON0bits.ADON)
    {
        // Enable A/D Power if it is not already On
        ADCON0bits.ADON = 1;

        /*
         * Wait the required Acquisition time according to PIC specification:
         * A/D Specification requirement:
         * After the analog input channel is selected (or changed),
         * an A/D acquisition must be done before the conversion can be started
         *
         * Need about 50 usecs for lower VDD voltages
         */
        PIC_delay_us(A2D_ON_TIME_50US);
    }
    else
    {
         // No Acquisition Time required (speeds up conversion)
    }

    /*
     * Start the conversion and wait for the Go/Done bit
     *  to be cleared, which signals the end of the conversion.
     */
    ADCON0bits.ADGO = 1;
    do
    {
        // Wait for conversion complete

    }while(ADCON0bits.ADGO);    
    
    // Normally we turn Off A/D after measurement to save current
    // but leave ADON here until we enter LP Sleep ... ADCON0bits.ADON

    // Turn FVR Off
    FVRCON = AD_FVR_OFF;

}


#ifdef	CONFIGURE_ACDC

#ifdef CONFIGURE_WIFI
/*
+------------------------------------------------------------------------------
| Function:      A2D_Test_AC_Sample
+------------------------------------------------------------------------------
| Purpose:       Monitor AC Detect voltage and return Power Status
|                Some functions may need to detect AC power loss faster
|                than normal AC Power Sampling
|                 
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  TRUE if AC active, else FALSE
+------------------------------------------------------------------------------
*/

uchar A2D_Test_AC_Sample(void)
{
    uint a2d_ac_volt = 0;
    
    a2d_ac_volt = A2D_Convert_10Bit(A2D_AC_DETECT_CHAN);
    
    if(AC_OFF_DETECT_THRESHOLD > a2d_ac_volt)
    {
        // TODO: Undefine Later  BC: Test Only
        #ifdef CONFIGURE_UNDEFINED
            SER_Send_String("A2D WiFi FALSE");
            SER_Send_Prompt();
        #endif
        return FALSE; 
    }
    else
    {
        // TODO: Undefine Later  BC: Test Only
        #ifdef CONFIGURE_UNDEFINED
            SER_Send_String("A2D WiFi TRUE");
            SER_Send_Prompt();
        #endif
        
        return TRUE; 
    }
}
#endif

/*
+------------------------------------------------------------------------------
| Function:      A2D_Test_AC
+------------------------------------------------------------------------------
| Purpose:       Monitor AC Detect voltage and update AC Power status
|                FLAG_AC_DETECT set or reset depending upon measured
|                 AC detect voltage. 
|                FLAG_GREEN_FADE_OFF and FLAG_GREEN_FADE_ON set on detected
|                 AC Transitions
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void A2D_Test_AC(void)
{
    uint a2d_ac_volt;

    a2d_ac_volt = A2D_Convert_10Bit(A2D_AC_DETECT_CHAN);

    if(FLAG_AC_DETECT)
    {
        // AC Previously ON
        if(AC_OFF_DETECT_THRESHOLD > a2d_ac_volt)
        {
            if(0 == --ac_detect_count)
            {
                // AC Off Detected
                FLAG_AC_DETECT = 0;
                
                #ifdef CONFIGURE_WIFI
                    //
                    // Reset APP Monitor Task to Power Reset State
                    //  for next AC Power On
                    //
                
                    // Turn OFF CCI Data Pin Interrupt ASAP
                    // CCI Data Negative edge bit-1 OFF
                    IOCBN = 0b00000000;     

                    // Notify Wireless Task to turn Off WiFi Port
                    FLAG_DISABLE_WIFI = 1;
                    FLAG_WIRELESS_DISABLED = 1;
                    
                    //
                    // note: A CCI Interrupt without AC power will also
                    //  reset APP Monitor task
                    //
                    APP_Status_Monitor_Reset();
                    
                    // Begin Wireless Monitor Task ASAP
                    MAIN_Reset_Task(TASKID_APP_STAT_MONITOR);
                    
                    // Clear any CCI faults
                    FLT_Fault_Manager(AMP_SUCCESS_CCI);

                    // TODO: Undefine Later  BC: Test Only
                    #ifndef CONFIGURE_UNDEFINED
                        SER_Send_String("A2D WiFi Disable");
                        SER_Send_Prompt();
                    #endif
                    
                    FLAG_ACTIVE_TIMER = 1;
                    if(MAIN_ActiveTimer < TIMER_ACTIVE_MINIMUM_2_SEC)
                    {
                        // Extend time to remain in Active Mode
                        MAIN_ActiveTimer = 
                                TIMER_ACTIVE_MINIMUM_2_SEC;
                    }
                    
                #endif
                
                
                if(!FLAG_LED_DIM)
                {
                    FLAG_GREEN_FADE_OFF = 1;
                    
                    FLAG_ACTIVE_TIMER = 1;
                    if(MAIN_ActiveTimer < TIMER_ACTIVE_MINIMUM_2_SEC)
                    {
                        // Extend time to remain in Active Mode
                        MAIN_ActiveTimer = 
                                TIMER_ACTIVE_MINIMUM_2_SEC;
                    }
                
                }
                
                // Change Green LED State immediately
                MAIN_Reset_Task(TASKID_GLED_MANAGER);

                // Re-Init AC Counter
                ac_detect_count = 2;

                // TODO: Undefine Later  BC: Test Only
                #ifdef CONFIGURE_OUTPUT_AC_DETECT
                    SER_Send_String("AC Off");
                    SER_Send_Prompt();
                #endif

                #ifdef CONFIGURE_DIAG_HIST
                    DIAG_Hist_Que_Push(HIST_AC_POWER_OFF);
                #endif
            }
        }
        else
        {
            // Reset AC OFF Detect Counter
            ac_detect_count = 2;
        }

    }
    else
    {
        // AC Previously OFF
        if(AC_ON_DETECT_THRESHOLD < a2d_ac_volt)
        {
            if(0 == --ac_detect_count)
            {
                // AC Power On Detected
                FLAG_AC_DETECT = 1;
                
                #ifdef CONFIGURE_WIFI
                    //
                    // If Wireless is disabled, we need to re initialize  
                    //  WiFi on AC Power Reset
                    //
                    // This should run the wireless monitor task from Power 
                    //  Reset and restart new Wireless Connection
                    //
                    if(FLAG_WIRELESS_DISABLED)
                    {
                        #ifdef CONFIGURE_TEST_CODE
                            log_seq(6);
                        #endif
                        
                        //*********************************************
                        // Test Only - See if we need to Init ASAP
                        
                        APP_Enable_WiFi_Port();        
                        
                        //*********************************************
                        
                        FLAG_AC_WIFI_RST = 1;   // Sends a Ping on AC Power
                        
                        // Power reset Wireless Monitor
                        APP_Init(APP_INIT_ACTION_PWR_RST);
                        
                        // Reset APP State
                        APP_Status_Monitor_Reset();
                        
                        // Reset any CCI faults
                        FLT_Fault_Manager(AMP_SUCCESS_CCI);
                        
                        // Begin Wireless Monitor Task ASAP
                        MAIN_Reset_Task(TASKID_APP_STAT_MONITOR);
                        
                        // TODO: Undefine Later  BC: Test Only
                        #ifndef CONFIGURE_UNDEFINED
                            SER_Send_String("A2D WiFI Enable");
                            SER_Send_Prompt();
                        #endif
                       
                    }
                #endif
                
                if(!FLAG_LED_DIM)
                {
                    FLAG_GREEN_FADE_ON = 1;
                }
                
                // Change Green LED State immediately
                MAIN_Reset_Task(TASKID_GLED_MANAGER);
                
                // Re-Init AC Counter
                ac_detect_count = 2;

                // TODO: Undefine Later  BC: Test Only
                #ifdef CONFIGURE_OUTPUT_AC_DETECT
                    SER_Send_String("AC On");
                    SER_Send_Prompt();
                #endif

                #ifdef CONFIGURE_DIAG_HIST
                    DIAG_Hist_Que_Push(HIST_AC_POWER_ON);
                #endif

            }
        }
        else
        {
            // Reset AC On Detect Count
            ac_detect_count = 2;
        }
    }

}
#endif


#ifdef CONFIGURE_TEMPERATURE

/*
+------------------------------------------------------------------------------
| Function:      A2D_Temp
+------------------------------------------------------------------------------
| Purpose:       Read the Temperature Voltage and return in ADRES
|                Temp circuit op-amp must be powered on and stabilized
|                before measurement
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  result (16 bit)
+------------------------------------------------------------------------------
*/
unsigned int A2D_Temp(void)
{
    uint result = A2D_Convert_10Bit(A2D_TEMP_CHAN);

    return (result);
}


#ifdef CONFIGURE_TEMP_DEG_F

/*
+------------------------------------------------------------------------------
| Function:      A2D_Temp_Degrees_F
+------------------------------------------------------------------------------
| Purpose:       Convert A/D value to Degrees F
|                and store result in a2d_temp_deg_f
|
+------------------------------------------------------------------------------
| Parameters:    a2d_temp = temperature reading A2D value
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void A2D_Set_Temp_Degress_F(uint a2d_temp)
{
    
    uint32_t a2d_temp_work;

#ifdef CONFIGURE_UNDEFINED
    uint16_t temp1;
    uint16_t temp2;
#endif
    
    // 
    // Keep a2d_temp within supported min/max limits
    //  which is set at -11 Degress C to 80 Degrees C
    //
    if(a2d_temp > TEMP_AD_MIN)
    {
        a2d_temp = TEMP_AD_MIN;
    }
    else if(a2d_temp < TEMP_AD_MAX)
    {
        a2d_temp = TEMP_AD_MAX;
    }
    
    //
    // Equation based upon Data Collection by Lab
    // Excel Equation - y = -0.1966x + 215.46
    // DegF = (215 - .1966(Temp A2D Value) )
    //
    // 1/.1966 = 5.086
    // Equation Equiv to: 
    // DegF = (215 - (Temp A2D value x 1000 / 5086) )
    //
    
    // Integer Math Precision Shift
    a2d_temp_work = ((uint32_t)a2d_temp * A2D_MULT_BY_1000);
    
    // Calculate Temp in Deg F
    a2d_temp_work = (a2d_temp_work/A2D_TEMP_EQU_DIVISOR);
    
    // Store result in static deg_f variable  
    a2d_temp_deg_f = (A2D_TEMP_EQU_OSET - (uint8_t)a2d_temp_work) ;

}

/*
+------------------------------------------------------------------------------
| Function:      A2D_Get_Degrees_F
+------------------------------------------------------------------------------
| Purpose:       Return current Temp in Degrees F 
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  (8-bit value in Degrees F)
+------------------------------------------------------------------------------
*/

unsigned char A2D_Get_Degrees_F(void)
{
    return a2d_temp_deg_f;
}

#endif

#endif  //CONFIGURE_TEMPERATURE
