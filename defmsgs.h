/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        defmsgs.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Compiler output message header file
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

#ifndef DEFMSG_H
#define DEFMSG_H

#warning *******************************************
#warning

#ifdef	CONFIGURE_2581_US
    #warning *** ACDC PhotoCO US, WiFi p/n 2581-PG01
#endif
#ifdef	CONFIGURE_1381_US
    #warning *** ACDC Photo, US, WiFi p/n 1381-PG01
#endif


#warning ***      Morph_1_5_WiFi Project
#warning ***      Firmware Version 1.09.00

#ifdef CONFIGURE_PRODUCTION_UNIT
	#warning *** Production Unit
#endif

#ifdef CONFIGURE_DEBUG_60_SEC_BAT_TEST
    #warning *** Battery Discharge FW
#endif

#ifdef	CONFIGURE_AUSTRALIA
    #warning *** For Australia 
#endif

#ifdef CONFIGURE_DEMO_TEST_FW
    #warning *** DEMO TEST FW!
#endif

#ifdef  CONFIGURE_WIRELESS_MODULE
    #ifdef CONFIGURE_WIFI
        #warning *** Wireless WiFi
    #else
        #warning *** Wireless Proprietary 
    #endif
#endif

#ifdef CONFIGURE_SMK_TARGET_89_UA
    #warning *** SMK Sens. = 89uA
#endif

#ifdef	CONFIGURE_PHOTO_SMOKE
       #warning *** Photo Smoke 
#endif

#ifdef CONFIGURE_SMK_CC_TEST
    #warning *** Conductive Chamber
#endif

#ifdef	CONFIGURE_CO
	#warning *** CO Sensor
#endif

#ifdef	CONFIGURE_STROBE
	#warning *** Strobe Option Enabled
#endif

#ifdef CONFIGURE_ESCAPE_LIGHT
    #warning *** Escape Light
#endif

#ifdef CONFIGURE_TEMPERATURE
    #warning *** Temperature Sensor
#endif

#ifdef CONFIGURE_THERMISTOR_TEST
    #warning *** Thermistor Test Enabled
#endif

#ifdef  CONFIGURE_GAS
	#warning *** Gas Detector
#endif

#ifdef CONFIGURE_NO_REGULATOR
    #warning *** No Voltage Regulator option (A/D converted to mv)
#endif

#ifdef CONFIGURE_10_YEAR_LIFE
	#warning *** 10 Year Life
#endif

#ifdef CONFIGURE_INTERCONNECT
	#warning *** Hardware Interconnect
#endif

#ifdef CONFIGURE_UL_ALARM_CURVE
	#warning *** Curve UL
#endif

#ifdef CONFIGURE_ALARM_CENELEC
	#warning *** CENELEC Alarm
#endif

#ifdef CONFIGURE_ACTIVE_TIMEOUT_TIMER
    #warning *** Active Mode Timeout Enabled
#endif

#ifdef  CONFIGURE_DIAG_HIST
    #warning *** Diag History Enable
#endif

#ifdef CONFIGURE_LBAT_CAL_ENABLE
        #warning *** LB Cal. Enable
#endif

#ifdef CONFIGURE_WATCHDOG_TIMER_OFF
	#warning *** Watchdog Timer Off
#else
       #warning *** Watchdog Timer On
#endif

#ifdef	CONFIGURE_EOL_HUSH
	#warning *** EOL Hush Enabled
#endif

#ifdef	CONFIGURE_VOICE
	#warning *** Voice Enabled
#endif

#ifdef	CONFIGURE_LB_HUSH
	#warning *** LB Hush ON
#else
    #warning *** LB Hush OFF
#endif

#ifdef CONFIGURE_NO_REMOTE_LB
    #warning *** Remote LB OFF
#else
    #warning *** Remote LB ON
#endif

#ifdef	CONFIGURE_BUTTON_ALG_ENABLE
	#warning *** BTN ALG ON/OFF Enabled
#endif

#ifdef CONFIGURE_DRIFT_COMP
	#warning *** Smoke Drift Comp. Enabled
#else
	#warning *** Smoke Drift Comp. OFF
#endif

#ifdef CONFIGURE_LIGHT_SENSOR
    #warning *** Light Sensor Enabled
#endif

#ifdef CONFIGURE_BTN_CHIRP
    #warning *** Button Feedback Chirp
#else
    #warning *** Button Feedback Sound
#endif

#ifdef CONFIGURE_CO_ALM_BAT_CONSERVE
    #warning *** CO ALM Bat Conserve ON
#else
    #warning *** CO ALM Bat Conserve OFF
#endif

#ifdef	CONFIGURE_CANADA
    #warning *** Canadian Language options
#endif

#warning
#warning *******************************************
#warning *******************************************
#warning

// Place Test/Debug Cofig messages in here

#ifdef CONFIGURE_DIAG_ACTIVE_FLAGS
    #warning *** Serial Active Flag Test FW!
#endif

#ifdef CONFIGURE_DUCT_TEST
    #warning *** Duct Test FW
#endif

#ifdef CONFIGURE_SPECIAL_LIGHT_TEST_FW
    #warning *** Special Light Test FW
#endif

#ifdef CONFIGURE_SEND_RAW_DATA
    #warning *** AMP Smoke Send Raw Data 
    #warning
#endif

#ifdef CONFIGURE_ACTIVE_MODE_ONLY
    #warning *** Active Mode Only!
#endif

#ifdef CONFIGURE_MANUAL_BAT_CAL
    #warning *** Manual Battery Cal.
#endif

#ifdef CONFIGURE_MANUAL_CO_CAL
    #warning *** Manual CO Cal.
#endif

#ifdef CONFIGURE_FAST_CO_CAL
    #warning *** Fast CO Calibration for Test
#endif

#ifdef CONFIGURE_MANUAL_SMOKE_CAL
	#warning *** Manual Smoke Cal.
#endif

#ifdef CONFIGURE_DISABLE_FAULTS
	#warning *** Faults are Disabled
#endif

#ifdef CONFIGURE_ID_MANAGEMENT
    #warning *** ID Management Enabled
#endif

#ifdef	CONFIGURE_ALG_OFF
	#warning *** Smoke ALG Disabled
#endif

#ifdef CONFIGURE_ACCEL_LIGHT_TEST
    #warning *** Acclerated Light Test Firmware
#endif

#ifdef CONFIGURE_MAN_NIGHT_DAY_TOGGLE
    #warning *** Day_Night Toggle Test FW
#endif

#ifdef CONFIGURE_DIAG_HIST_TEST
    #warning *** Diag History Test Firmware
#endif

#ifdef CONFIGURE_24_HOUR_TEST
	#warning *** 24 Hour Test Timing
#endif

#ifdef CONFIGURE_EOL_TEST
	#warning *** End of Life Testing Enabled

    #ifdef CONFIGURE_LGT_TABLE_DAY_FIRST
        #warning *** Day 1st
    #else
        #warning *** Night 1st
    #endif

#endif

#ifdef CONFIGURE_DEBUG_NET_DEVICES_TEST
    #warning *** Blink # of Devices Test Code
#endif

#ifdef CONFIGURE_60_HOUR_TEST
	#warning *** 60 Hour Test Timing
#endif

#ifdef CONFIGURE_DISABLE_CO_HEALTH_TEST
    #warning *** CO Sensor Test DISABLED
#endif

#ifdef CONFIGURE_MANUAL_SMK_TH
	#warning *** Alarm TH CMD Enabled
#endif

#ifdef CONFIGURE_SMK_SLOPE_SIMULATION
        #warning *** Smoke Slope Simulation On
#endif

#ifdef CONFIGURE_MANUAL_SMK_OUT
	#warning *** Serial Smoke CMD Enabled
#endif

#ifdef CONFIGURE_SERIAL_CO_ALARM
    #warning *** Serial CO Alarm CMD Enabled
#endif

#ifdef CONFIGURE_MANUAL_PPM_COMMAND
	#warning *** Serial CO CMD Enabled
#endif

#ifdef	CONFIGURE_DRIFT_TEST_FW
	#warning *** Smoke Drift Comp Test Firmware
#endif

#ifdef CONFIGURE_WDT_RST_FLAGS
    #warning *** WDT Test Flags Enabled
#endif

#ifdef	CONFIGURE_TEST_SMK_COMP
	#warning *** Smoke Comp Test Enabled
#endif

#ifdef	CONFIGURE_UL_TESTING
	#warning *** UL Test Firmware Build
#endif

#ifdef	CONFIGURE_SMOKE_CORRELATION
	#warning *** Smoke Box Corr Test Firmware
#endif

#ifdef CONFIGURE_ACCEL_TIMERS
	#warning *** Accelerated Timers for Test
#endif

#ifdef	CONFIGURE_VOICE_SERIAL
	#warning *** Output Voice to Serial
#endif

#ifdef	CONFIGURE_TIMER_SERIAL
	#warning *** Output Timers to Serial
#endif

#ifdef CONFIGURE_LGT_SENSOR_PWR_IND
    #warning *** LGT SENS LED as Pwr Ind   
#endif

#ifdef	CONFIGURE_LIGHT_CHANGE_SERIAL
	#warning *** Light Change to Serial
#endif

#ifdef	CONFIGURE_CHIRP_SERIAL
	#warning *** Chirp to Serial
#endif

#ifdef CONFIGURE_DIAG_ACTIVE_FLAGS
    #warning *** Diag Active Flags Enabled
#endif

#ifdef CONFIGURE_BAUD_19200
    #warning *** Serial 19200 BAUD
#else
    #warning *** 9600 BAUD
#endif


#warning
#warning *******************************************



#endif // DEFMSG_H