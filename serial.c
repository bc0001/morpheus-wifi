

/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        serial.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Basic routines for serial output.
|               
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/



/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include    <string.h>
#include    "common.h"
#include    "command_proc.h"
#include    "serial.h"
#include    "a2d.h"
#include    "systemdata.h"

#ifdef CONFIGURE_WIRELESS_MODULE
    #include    "app.h"
#endif


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

// BAUD Rate Constants
// BRGH = 1 values
#define	SER_9600BAUD_4MHZ	(uchar)25
#define	SER_9600BAUD_8MHZ	(uchar)51
#define	SER_9600BAUD_16MHZ	(uchar)103
#define	SER_9600BAUD_20MHZ	(uchar)129

#define SER_19200BAUD_16MHZ (uchar)51

#ifdef CONFIGURE_SERIAL_QUEUE

    #ifdef CONFIGURE_BAUD_19200
        #define SER_QLENGTH (uint)500
    #else
        #define SER_QLENGTH (uint)600
    #endif

#endif

#define SER_MAX_CHAR_TX_4   (uchar)4
#define SER_MAX_CHAR_TX_2   (uchar)2
#define SER_NUM_CCI_CHARS_3 (uchar)3

/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/
// Serial Flag register
volatile Flags_t SER_out_flags;

#ifdef CONFIGURE_WIRELESS_CCI_DEBUG
    #ifdef CONFIGURE_SER_QUE_MAX
        // Test only
        uint SER_quemax = 0;
        uint SER_cci_quemax = 0;
    #endif
#endif

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/

#ifdef CONFIGURE_SERIAL_QUEUE

    uchar ser_queue[SER_QLENGTH];
    static uint ser_qin;
    static uint ser_qout;

#endif

/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/
    void ser_enable_init(void);
    char* itoa( unsigned int value, char* result, char base);
    void ser_send_error(void);

    #ifdef CONFIGURE_SERIAL_QUEUE
        void ser_qinit (void);
        uint ser_exque(void);
        uchar ser_qpop(void);
        void ser_qpush(uchar c);
    #endif
    

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/

/*
+------------------------------------------------------------------------------
| Function:      SER_Init
+------------------------------------------------------------------------------
| Purpose:       Serial Port Initialization
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void SER_Init(void)
{
    SER_out_flags.ALL = 0;
    
    // Insure Serial Port Pins are initialized to test
    TRISCbits.TRISC7 = 1;
    TRISCbits.TRISC6 = 1;
    
    // Route Serial Port TX to Pin
    RC6PPS = 0x0010;   //RC6 -> TX
    
    ser_enable_init();

}

#ifdef CONFIGURE_DIAG_ACTIVE_FLAGS

/*
+------------------------------------------------------------------------------
| Function:      SER_Off
+------------------------------------------------------------------------------
| Purpose:       Serial Port De-Activated
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void SER_Off(void)
{
    // All Serial Messages OFF
    SER_out_flags.ALL = 0x00;

    // No Serial Port connection, make RX Output Low and
    // Port C , bit 7 = RX Input
    TRISCbits.TRISC7 = 1;   // Leave as Input since a 1M pulldown attached
    TRISCbits.TRISC6 = 0;
    LATCbits.LATC7 = 0;
    LATCbits.LATC6 = 0;

    // Make Sure Serial Port is not attached to the Pins
    RC6PPS = 0x00; 
    TXPPS = 0x00;

    #ifndef CONFIGURE_SERIAL_CONNECT_BY_BUTTON
        // Not Required (leave default settings in case of Hot Connection)  
        RC7PPS = 0x00;
        RXPPS = 0x00;
    #endif

    // Allow Low Power Mode    
    FLAG_SERIAL_PORT_ACTIVE = 0;    
    
}

/*
+------------------------------------------------------------------------------
| Function:      SER_On
+------------------------------------------------------------------------------
| Purpose:       Serial Port Tested and Initialized
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void SER_On(void)
{
    SER_Init();
}

#endif

/*
+------------------------------------------------------------------------------
| Function:      SER_Queue_Empty
+------------------------------------------------------------------------------
| Purpose:       Test Serial Queue if Empty or Not
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  TRUE if Empty
+------------------------------------------------------------------------------
*/

uint8_t SER_Queue_Empty(void)
{
    if( 0 == ser_exque())
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/*
+------------------------------------------------------------------------------
| Function:      ser_enable_init
+------------------------------------------------------------------------------
| Purpose:       Test for Serial Port connection and enable serial port
|                 if detected
|                Call before using the serial port.
|
|                FLAG_SERIAL_PORT_ACTIVE set if serial port enabled
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void ser_enable_init(void)
{
    #ifdef CONFIGURE_SERIAL_QUEUE
        // Initialize Que to Empty if Configured
        ser_qinit();
    #endif

    //Test for Serial Port Connection
    if(PORT_RX_PIN == 1)
    {
        FLAG_SERIAL_PORT_ACTIVE = 1;
    }
    else
    {
        FLAG_SERIAL_PORT_ACTIVE = 0;
    }

    if(FLAG_SERIAL_PORT_ACTIVE)
    {
        // 8 Bit (this should also be Power on default setting)
        BAUD1CONbits.BRG16 = 0;

        // Initialize Serial Port for Diagnostics
        TXSTAbits.SYNC = 0;   // Asynchronous operation
        TXSTAbits.TXEN = 1;
        TXSTAbits.BRGH = 1;   // Set High Speed  (fosc/16 rate)
      
        #ifdef CONFIGURE_BAUD_19200      
              //SPBRGH not used in 8-bit mode
              SPBRGL = SER_19200BAUD_16MHZ;
        #else
              //SPBRGH not used in 8-bit mode
              SPBRGL = SER_9600BAUD_16MHZ;
        #endif
      
        // Clear any Errors in case of a Hot Connection
        RCSTAbits.FERR = 0;
        RCSTAbits.OERR = 0;
        RCSTAbits.RX9 = 0;
        RCSTAbits.ADDEN = 0;

        RCSTAbits.SPEN = 1;
        RCSTAbits.CREN = 1;

        /*
         * Any serial output to be turned on at power-up can be put here.
         * 
         * SERIAL_ENABLE_CO = 1;
         * SERIAL_ENABLE_BATTERY = 1;
         * SERIAL_ENABLE_HEALTH = 1;
         * SERIAL_ENABLE_SMOKE = 1;
         * SERIAL_ENABLE_TEMPERATURE = 1;
         * SERIAL_ENABLE_LIGHT = 1;
        */

        #ifdef CONFIGURE_DIAG_ACTIVE_FLAGS
            // Serial messages enabled for this test
            SER_out_flags.ALL = 0x27;
        #else
            // Default with All Serial Messages ON
            SER_out_flags.ALL = 0xFF;
        #endif
    }
    else
    {
        // All Messages OFF
        SER_out_flags.ALL = 0x00;

        // No Serial Port connection, make RX Output Low and
        // Port C , bit 7 = RX
        TRISCbits.TRISC7 = 0;
        TRISCbits.TRISC6 = 0;
        LATCbits.LATC7 = 0;
        LATCbits.LATC6 = 0;
        
        // Make Sure Serial Port is not attached to the Pins
        RC6PPS = 0x00; 
        TXPPS = 0x00;
        
        #ifndef CONFIGURE_SERIAL_CONNECT_BY_BUTTON
            // Not Required (leave default settings in case of Hot Connection)  
            RC7PPS = 0x00;
            RXPPS = 0x00;
        #endif

    }
}


/*
+------------------------------------------------------------------------------
| Function:      SER_Process_Rx
+------------------------------------------------------------------------------
| Purpose:       Check for any Received Characters to process
|                and Push into Command Buffer
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void SER_Process_Rx(void)
{
    static unsigned char buffer = 0;

    if(RCSTAbits.OERR)
    {
        RCSTAbits.CREN=0;
        _nop();
        RCSTAbits.CREN=1;
        
    }

    else if(RCSTAbits.FERR)
    {
        // Read to clear framing error
        buffer = RCREG;
        buffer = 0;
        
    }
    
    else if(PIR3bits.RCIF)
    {
        
        PIR3bits.RCIF = 0;
        
        CMD_Buffer_Push(RCREG);
        
    }
}

/*
+------------------------------------------------------------------------------
| Function:      itoa
+------------------------------------------------------------------------------
| Purpose:       Convert Interger to Ascii value
|
|
+------------------------------------------------------------------------------
| Parameters:    value:  Value to be converted
|                base:  10 or 16 (dec or hex)
|
+------------------------------------------------------------------------------
| Return Value:  result: string representation of integer
+------------------------------------------------------------------------------
*/

char* itoa( unsigned int value, char* presult, char base) 
{ 
	char* out = presult; 
	unsigned int quotient = value; 
	unsigned int absQModB; 
	char *start;
	unsigned char temp; 
		
	do 
    { 
        absQModB=quotient % base; 
        *out = "0123456789ABCDEF"[ absQModB ]; 
        ++out; 
        quotient /= base;
        
	} while( quotient ); 
	
	*out = 0; 
	
	start = presult; 

	out--; 
	while(start < out) 
	{ 
        temp=*start;
        *start=*out;
        *out=temp;
        start++;
        out--;
	} 
	
	return presult; 
} 


/*
+------------------------------------------------------------------------------
| Function:      SER_Send_Int
+------------------------------------------------------------------------------
| Purpose:       Send Integer value to Serial Port
|                
|
+------------------------------------------------------------------------------
| Parameters:    tag:  A character to send before sending the value
|                val:  value to be sent
|                      Call with ' ' in tag if no tag is desired
|                base: 10 for output in decimal, 16 for output in hex
|
|
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void SER_Send_Int(char tag, unsigned int val, char base)
{
    if(FLAG_SERIAL_PORT_ACTIVE)
    {
        char stringtosend[9];	// 2 char for tag, 6 for the int and a NULL

        if( ' ' == tag)
        {
            // If there is no tag, just send the string.
            itoa(val, stringtosend, base);
        }
        else
        {
            // Send the tag and a colon, followed by the number
            *stringtosend = tag;
            *(stringtosend + 1) = ':';
            itoa(val, stringtosend + 2, base);
        }

        SER_Send_String(stringtosend);
    }
}


/*
+------------------------------------------------------------------------------
| Function:      SER_Send_Byte
+------------------------------------------------------------------------------
| Purpose:       Transmit byte in hex ascii format
|                Tx order - upper nibble, lower nibble
|
+------------------------------------------------------------------------------
| Parameters:    byte to be send
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void SER_Send_Byte(uchar byte)
{
    if(FLAG_SERIAL_PORT_ACTIVE)
    {
        uchar nibble;

        nibble = ((byte & 0xF0) >> 4);
        if( nibble < 0x0A)
        {
            nibble += 0x30;
        }
        else
        {
            nibble += 0x37;
        }

        SER_Send_Char((char) nibble);

        nibble = byte & 0x0F;
        if( nibble < 0x0A)
        {
            nibble += 0x30;
        }
        else
        {
            nibble += 0x37;
        }

        SER_Send_Char((char) nibble);
	}
}


/*
+------------------------------------------------------------------------------
| Function:      SER_Send_String
+------------------------------------------------------------------------------
| Purpose:       Sends a NULL terminated string out the serial port.
|
|
+------------------------------------------------------------------------------
| Parameters:    stringtosend - string to send
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void SER_Send_String(char const *pstringtosend)
{
    if(FLAG_SERIAL_PORT_ACTIVE)
    {
        CLRWDT();
        
        #ifdef CONFIGURE_SERIAL_QUEUE
            char  *charpointer = (char*) pstringtosend;

            // Some Serial Outputs are too large and will overflow the buffer
            // i.e The "G" Command
            if(FLAG_SER_SEND_DIRECT)
            {
                if(!FLAG_VOICE_TX_ACTIVE)
                {
                    char  *charpointer = (char*) pstringtosend;

                    do
                    {
                        // Wait for TX Register ready for Char
                        while(!TXSTAbits.TRMT)
                        {
                            // Wait for TX Register ready
                        }

                        // Send the next character
                        TXREG = *charpointer++;

                    } while(*charpointer != 0);
                }
            }
            else
            {
                // Push Serial Data into Serial Queue
                do
                {
                    // Push the next character into Queue
                    ser_qpush(*charpointer);
                    charpointer++;

                } while(*charpointer != 0);
            }

        #else
            if(!FLAG_VOICE_TX_ACTIVE)
            {
                char  *charpointer = (char*) pstringtosend;

                do
                {
                    // Wait for TX Register ready for Char
                    while(!TXSTAbits.TRMT)
                    {
                        // Wait for TX Register ready
                    }

                    // Send the next character
                    TXREG = *charpointer++;

                } while(*charpointer != 0);
            }
        #endif
    }
}


/*
+------------------------------------------------------------------------------
| Function:      SER_Send_Prompt
+------------------------------------------------------------------------------
| Purpose:       Send prompt character out the serial port
|                (after the carriage return character)
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void SER_Send_Prompt(void)
{
    if(FLAG_SERIAL_PORT_ACTIVE)
    {
        SER_Send_String("\r>");
    }
}

/*
+------------------------------------------------------------------------------
| Function:      SER_Send_Error
+------------------------------------------------------------------------------
| Purpose:       Send error character out the serial port
|                (after the carriage return character)
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void ser_send_error(void)
{
    if(FLAG_SERIAL_PORT_ACTIVE)
    {
        SER_Send_Char('\r');
        SER_Send_Char('?');
        SER_Send_Prompt();
    }
}


/*
+------------------------------------------------------------------------------
| Function:      SER_Send_Char
+------------------------------------------------------------------------------
| Purpose:       Send character out the serial port
|                
|
|
+------------------------------------------------------------------------------
| Parameters:    c - character to send
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void SER_Send_Char(char c)
{
    if(FLAG_SERIAL_PORT_ACTIVE)
    {
        char string[2];
        *string = c;
        *(string + 1) = 0;
        SER_Send_String(string);
    }
}

/*
+------------------------------------------------------------------------------
| Function:      SER_Strx_To_Int
+------------------------------------------------------------------------------
| Purpose:       Convert String to Integer value 
|
|
+------------------------------------------------------------------------------
| Parameters:    str - location of string to convert
+------------------------------------------------------------------------------
| Return Value:  integer value
+------------------------------------------------------------------------------
*/

unsigned int SER_Strx_To_Int(char const *pstr)
{
	uint value = 0x0000;
	uchar multiplier = 0;
	char i = strlen(pstr) - 1;  //Index to current char of string to evaluate


#ifdef CONFIGURE_UNDEFINED        
        SER_Send_Int( ' ', i, 16 );
        SER_Send_Char(':');
        SER_Send_Char(' ');
#endif
    
	
	do 
	{
		if(*(pstr+i) >= 'A')
		{
            //
            // For some reason this statement no longer worked...
            // (char) cast may be truncating value
            // So I split it out into separate statements and it works
            //
			//value += (char)((*(pstr + i) + 0xC9) << multiplier);
            //
            
            uint temp1 = ((*(pstr + i) + 0xC9); // Get next previous char
            
            temp1 = temp1 & 0x0f;   // mask 4 bits

            #ifdef CONFIGURE_UNDEFINED              
                SER_Send_Int( 't', temp1, 16 );
                SER_Send_Char(',');
            #endif

            temp1 = temp1 << multiplier;    // Should shift into proper nibble
            
            #ifdef CONFIGURE_UNDEFINED              
                SER_Send_Int( 's', temp1, 16 );
                SER_Send_Char(',');
            #endif  
                
            value = value | temp1;  // Merge into value position
            
            #ifdef CONFIGURE_UNDEFINED              
                SER_Send_Int( 'v', value, 16 );
                SER_Send_Prompt();             
            #endif    
            
		}
		else
		{
            //
            // For some reason this statement no longer worked...
            // (char) cast may be truncating value
            // So I split it out into separate statements and it works
            //
			//value += (char)((*(pstr + i) + 0xD0) << multiplier);
            
            uint temp1 = ((*(pstr + i) + 0xD0); // Get next previous char
            
            temp1 = temp1 & 0x0f;   // mask to 4 bits

            #ifdef CONFIGURE_UNDEFINED              
                SER_Send_Int( 't', temp1, 16 );
                SER_Send_Char(',');
            #endif

            temp1 = temp1 << multiplier;    // Should shift into proper nibble
            
            #ifdef CONFIGURE_UNDEFINED              
                SER_Send_Int( 's', temp1, 16 );
                SER_Send_Char(',');
            #endif     
                
            value = value | temp1;  // Merge into value position
            
            #ifdef CONFIGURE_UNDEFINED              
                SER_Send_Int( 'v', value, 16 );
                SER_Send_Prompt();             
            #endif    
                
		}
		
		multiplier += 4;
        		
	} while( i-- != 0);  // this compares to zero before decrementing
	
	return value;
}


#ifdef CONFIGURE_SERIAL_QUEUE

/*
+------------------------------------------------------------------------------
| Function:      ser_qinit
+------------------------------------------------------------------------------
| Purpose:       Initialize empty serial port Queue
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void ser_qinit (void)
{
    // Init to Queue Empty
    ser_qin = 0;
    ser_qout = 0;
}


/*
+------------------------------------------------------------------------------
| Function:      ser_qpop
+------------------------------------------------------------------------------
| Purpose:       Return Char popped from Serial Queue
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  character popped from queue
+------------------------------------------------------------------------------
*/

uchar ser_qpop(void)
{
    uchar c = ser_queue[ser_qout];

    if((SER_QLENGTH-1) == ser_qout)
    {
        // Wrap to Start of Queue
        ser_qout = 0;
    }
    else
    {
        // Next Queue location
        ser_qout++;
    }

    return c;
}


/*
+------------------------------------------------------------------------------
| Function:      ser_qpush
+------------------------------------------------------------------------------
| Purpose:       Push Char in Serial Queue
|
|
+------------------------------------------------------------------------------
| Parameters:    character to push in queue
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void ser_qpush(uchar c)
{
    ser_queue[ser_qin] = c;
    if((SER_QLENGTH-1) == ser_qin)
    {
        // Wrap to Start of Queue
        ser_qin = 0;
    }
    else
    {
        // Next Queue location
        ser_qin++;
    }
}


/*
+------------------------------------------------------------------------------
| Function:      ser_exque
+------------------------------------------------------------------------------
| Purpose:       Return Status of Queue
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  return # of chars in Queue, 0 = empty
+------------------------------------------------------------------------------
*/

uint ser_exque(void)
{
    uint s;
    
    GLOBAL_INT_ENABLE = 0;
    
    if(ser_qin == ser_qout)
    {
        s = 0;
    }
    else if(ser_qin > ser_qout)
    {
        s = (ser_qin - ser_qout);
    }
    else
    {
        // QIN must have wrapped to add in QLENGTH to calc. # of chars in Que
        s = ((ser_qin + SER_QLENGTH) - ser_qout);

    }
    
    GLOBAL_INT_ENABLE = 1;
    
    return s;

}


/*
+------------------------------------------------------------------------------
| Function:      SER_Xmit_Queue
+------------------------------------------------------------------------------
| Purpose:       Examine Queue and Transmit if anything to send
|                We limit number of characters transmitted each call to
|                prevent overflowing Active Mode TIC timing from serial output 
|                transmissions
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void SER_Xmit_Queue(void)
{
        
   // Serial Port should be Off during Voice TX so don't
   //  TX buffer yet if Voice Transmission is Active
   if(!FLAG_VOICE_TX_ACTIVE)
   {
       
        #ifdef CONFIGURE_WIRELESS_CCI_DEBUG       
            // Check the CCI debug Queue for any Serial Port
            //   data to place in Serial Buffer
            
            uint c = APP_CCI_Exque();
            
            #ifdef CONFIGURE_SER_QUE_MAX
                // Test Only
                if(c > SER_cci_quemax)
                {
                    SER_cci_quemax = c;
                }
            #endif
            
            if(c != 0)
            {
                // Queued data to transmit ( 1 message at a time)
                // Data is only Pushed in Queue 2 bytes at a time
                // plus a direction byte
                if(c >= SER_NUM_CCI_CHARS_3)
                {
                    // Get MSG Direction
                    uchar i = APP_CCI_QPop();
                    if(APP_MSG_RECEIVE == i)
                    {
                        SER_Send_String("CR:");
                    }
                    else
                    {
                        SER_Send_String("CT:");
                    }

                    // Get Tag
                    // If 0 then this is from an Error
                    i = APP_CCI_QPop();
                    if(0 == i)
                    {
                        SER_Send_String("Err");
                        
                        // Remove Data Byte 
                        i = APP_CCI_QPop();
                         
                    }
                    else
                    {

                        SER_Send_Byte(i);
                        SER_Send_Char(' ');

                        // Get Data
                        i = APP_CCI_QPop();
                        SER_Send_Byte(i);
                    }

                    SER_Send_Prompt();
                }
            }
            else
            {
                // If Queue is not empty, start Transmission
                uint c = ser_exque();
                
                #ifdef CONFIGURE_SER_QUE_MAX
                    // Test Only
                    if(c > SER_quemax)
                    {
                        SER_quemax = c;
                    }
                #endif
                
                if(c!= 0)
                {

                    if(c > SER_MAX_CHAR_TX_4)
                    {
                        // Limit to 4 chars each pass at a time
                        c = SER_MAX_CHAR_TX_4;
                    }

                    for(uchar i = c; i > 0; i--)
                    {
                        // Wait for TX Register ready for Char
                        while(TXSTAbits.TRMT == 0)
                        {
                            //Wait for TX Register ready
                        }

                        // Send the next character
                        TXREG = ser_qpop();

                    }
                }
            }

        #else

            // If Queue is not empty, start Transmission
            uint c = ser_exque();
            if(c!= 0)
            {

                if(c > SER_MAX_CHAR_TX_2)
                {
                    // Limit to 2 chars each pass at a time
                    c = SER_MAX_CHAR_TX_2;
                }

                for(uchar i = c; i > 0; i--)
                {
                    // Wait for TX Register ready for Char
                    while(TXSTAbits.TRMT == 0)
                    {
                        //Wait for TX Register ready
                    }

                    // Send the next character
                    TXREG = ser_qpop();

                }
            }
            
        #endif
        
   }
}

#endif








