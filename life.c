/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        life.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Routines to manage life expiration day counter
|               
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include    "common.h"
#include    "life.h"
#include    "systemdata.h"
#include    "fault.h"
#include    "battery.h"
#include    "diaghist.h"
#include    "photosmoke.h"
#include    "app.h"
#include    "ptt.h"


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/
   #ifdef CONFIGURE_24_HOUR_TEST
        #define	LFE_MINUTES_PER_DAY	(uint)1440
    #endif

    #ifdef CONFIGURE_EOL_TEST
        #define	LFE_MINUTES_PER_DAY	(uint)1
    #endif

    #ifdef CONFIGURE_DIAG_HIST_TEST
        #define	LFE_MINUTES_PER_DAY	(uint)1
    #endif

    #ifdef CONFIGURE_DEPASSIVATION_TEST
        #define	LFE_MINUTES_PER_DAY	(uint)1
    #endif

    #ifdef CONFIGURE_60_HOUR_TEST
        #define	LFE_MINUTES_PER_DAY	(uint)1
    #endif

    #ifndef	LFE_MINUTES_PER_DAY
        #define	LFE_MINUTES_PER_DAY	(uint)1440
    #endif

    // Day 2 is actually the 1st full day of operation for these models
    //  since customer should receive unit pre-set to Day 1
    #define LFE_DAY_ONE         (uint)2
    #define LFE_YEAR_ONE        (uint)366
    #define LFE_DAYS_PER_YEAR   (uint)365

    #ifdef CONFIGURE_ACCEL_TIMERS
        #define LFE_EOL_HUSH_3_MIN      (uchar)3   
    #else
        // 24 Hours in Hours
        #define LFE_EOL_HUSH_24_HOURS   (uchar)24      
    #endif


    #define	LIFE_LSB_INIT_EOL_VAL	((LIFE_EXPIRATION_DAY & 0xff) - 1)
    #define	LIFE_MSB_INIT_EOL_VAL	(LIFE_EXPIRATION_DAY >> 8)



/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
persistent static uint life_timer_count;


static uint life_min_per_day = LFE_MINUTES_PER_DAY;


#ifdef	CONFIGURE_EOL_HUSH
    static uchar life_eol_hush_timer = 0;
#endif

/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/
void life_check_eol(void);

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/


/*
+------------------------------------------------------------------------------
| Function:      LIFE_Init_Life_Count
+------------------------------------------------------------------------------
| Purpose:       Initialize Life Minute Counter
|                This is not initialized on Soft Resets 
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void LIFE_Init_Life_Count(void)
{
    // Clear Life Count
    life_timer_count = 0;
}


/*
+------------------------------------------------------------------------------
| Function:      LIFE_Init
+------------------------------------------------------------------------------
| Purpose:       Verify Valid Life Checksum and Life Expiration
|                Sets memory Fault if life data corrupted 
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void LIFE_Init(void)
{
    unsigned char sum;
    sum = SYS_RamData.Life_LSB;
    sum+= SYS_RamData.Life_MSB;
    if(sum != SYS_RamData.Life_Checksum)
    {
        // Perform another Sys Init to attempt to restore
        // Then repeat Init.
        if(FALSE == SYS_Init() )
        {
            sum = SYS_RamData.Life_LSB;
            sum+= SYS_RamData.Life_MSB;
            if(sum != SYS_RamData.Life_Checksum)
            {
                // There is still an error in the checksum byte.
                // Force a memory fault.
                FLT_Fault_Manager(FAULT_MEMORY);
            }
        }
        else
        {
             FLT_Fault_Manager(FAULT_MEMORY);
        }

    }

    // Verify Life is not expired
    life_check_eol();

}


/*
+------------------------------------------------------------------------------
| Function:      LIFE_Get_Current_Day
+------------------------------------------------------------------------------
| Purpose:       returns Current Day of Life
|                 
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  returns 16 bit current life day
+------------------------------------------------------------------------------
*/
uint LIFE_Get_Current_Day(void)
{
    // Return the current day as an integer
    return *((uint*)&(SYS_RamData.Life_LSB));
}


/*
+------------------------------------------------------------------------------
| Function:      LIFE_Check
+------------------------------------------------------------------------------
| Purpose:       This function must be called once per minute to correctly
|                keep track of time.
|                Tracks minutes per day and every 24 hours updates the Day 
|                counter, and saves in Non-Volatile memory
| 
|                Also checks for Life Expiration every 24 Hours
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void LIFE_Check(void)
{
    // Increment and check the life timer
    if(++life_timer_count >= life_min_per_day)
    {
       
        // This writes data structure back into Non-volatile every 60 seconds
        // We have already verified in Main() that the current data structure
        // is not corrupted, so proceed.

        // A day has expired.  Increment the day counter and check for
        // expiration.
        // Cast the LSB to an integer so that 16 bit math will be done.
        *((uint*)&(SYS_RamData.Life_LSB)) += 1;
        
        #ifdef CONFIGURE_DIAG_HIST_TEST
            // Output for Testing only
            SER_Send_String("\r          Life Count = ");
            SER_Send_Int(' ',*((uint*)&(SYS_RamData.Life_LSB)),10);
            SER_Send_String("\r");
        #endif

        SYS_RamData.Life_Checksum = SYS_RamData.Life_LSB + SYS_RamData.Life_MSB;
        SYS_Save();
        life_check_eol();

        // Update the compensation for the Photo Smoke IRLED.
        PHOTO_Compensate_Alm_Thold();

        // Reset minute counter for next day.
        life_timer_count = 0;
        
        uchar life_bat_volt = BAT_Get_Last_Voltage();
        
        // Call anything that executes every day here.
        #ifdef CONFIGURE_DIAG_HIST
            // Update Current battery Voltage once a day.
            DIAG_Hist_Bat_Record(BAT_CURRENT, life_bat_volt);

            if(LFE_DAY_ONE == LIFE_Get_Current_Day() )
            {
                // Record battery Voltage after 1st 24 hours of operation
                DIAG_Hist_Bat_Record(BAT_DAY_ONE, life_bat_volt);
            }
            if(LFE_YEAR_ONE == LIFE_Get_Current_Day() )
            {
                // Record battery Voltage after 1st year of operation
                DIAG_Hist_Bat_Record(BAT_YEAR_ONE, life_bat_volt);
            }
        #endif

        #ifdef CONFIGURE_WIRELESS_MODULE
            // Every 24 Hours, if in Network Error, Reset the Error
            //  to trigger RF Module re-join attempt
            if(FLAG_NETWORK_ERROR_MODE)
            {
                // Send Reset Fault Message to RF Module
                APP_Reset_Fault();

                 // Reset task for Faster Response to Error Reset
                MAIN_Reset_Task(TASKID_TROUBLE_MANAGER);

            }
        #endif
    }
    
    // Test Code Only
    #ifdef CONFIGURE_UNDEFINED
        SER_Send_Prompt();
        SER_Send_String("Life Minutes - ");
        SER_Send_Int(' ',life_timer_count,10);
        SER_Send_Prompt();
    #endif

    #ifdef	CONFIGURE_EOL_HUSH
        // EOL Hush was reduced from 3 days to 24 Hours
        if(FLAG_EOL_HUSH_ACTIVE)
        {

            #ifdef CONFIGURE_ACCEL_TIMERS
                // Check here to see if EOL hush mode should be cancelled.  
                // This is a good spot to do it since it is called every minute.  
                // Subtract the stored timer from the current minute to see if 
                // enough time has expired.
                if( (uchar)(MAIN_OneMinuteTic - life_eol_hush_timer) >= 
                     (uchar)LFE_EOL_HUSH_3_MIN)
                {
                    FLAG_EOL_HUSH_ACTIVE = 0;

                    #ifdef CONFIGURE_TIMER_SERIAL
                        SER_Send_String("EOL Hush Timer Expired- ");
                        SER_Send_Prompt();
                    #endif        

                }

            #else
                // Check here to see if EOL hush mode should be cancelled.  
                // This is a good spot to do it since it is called every minute.  
                // Subtract the stored timer from the current minute to see if 
                // enough time has expired.
                if( (uchar)(MAIN_OneHourTic - life_eol_hush_timer) >= 
                     (uchar)LFE_EOL_HUSH_24_HOURS)
                {
                    FLAG_EOL_HUSH_ACTIVE = 0;
                }
            #endif
        }
    #endif

}


/*
+------------------------------------------------------------------------------
| Function:      life_check_eol
+------------------------------------------------------------------------------
| Purpose:       Test Currrent Day against Life Expiration count
|                Sets EOL Mode is entering End of Life
|                Sets EOL Fatal Fault if past hushable life extension (7 days)   
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void life_check_eol(void)
{
#ifdef	CONFIGURE_EOL_HUSH
    if(LIFE_Get_Current_Day() >= LIFE_UL_EXTENSION)
    {
        #ifdef CONFIGURE_TIMER_SERIAL
            if(!FLAG_EOL_FATAL)
            {
                SER_Send_String("EOL 7 Day Expired- ");
                SER_Send_Prompt();
            }
        #endif        

            // EOL fatal fault, no more EOL Hush Extension
        FLT_Fault_Manager(FAULT_EXPIRATION);
        
    }
    else if(LIFE_Get_Current_Day() >= LIFE_EXPIRATION_DAY)
    {

        #ifdef CONFIGURE_TIMER_SERIAL
            if(!FLAG_EOL_MODE)
            {
                // Only send this the 1st time
                SER_Send_String("Start EOL 7 Day Timer- ");
                SER_Send_Prompt();
            }
        #endif  
        
        // This begins EOL chirping
        FLAG_EOL_MODE = 1;
        
    }
#else
    if(LIFE_Get_Current_Day() >= LIFE_EXPIRATION_DAY)
    {
        // EOL fatal fault
        FLT_Fault_Manager(FAULT_EXPIRATION);
    }
#endif


}


#ifdef	CONFIGURE_EOL_HUSH
/*
+------------------------------------------------------------------------------
| Function:      LIFE_Init_EOL_Hush
+------------------------------------------------------------------------------
| Purpose:       Initialize the End of Life Hush Timer
|                Sets EOL hush timer to the current hour. (24 hours of hush)  
|                   
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void LIFE_Init_EOL_Hush(void)
{
    
    #ifdef CONFIGURE_ACCEL_TIMERS
        // Dummy One Hour Tic instruction to avoid unused warning...
        life_eol_hush_timer = MAIN_OneHourTic;
        
        life_eol_hush_timer = MAIN_OneMinuteTic;
        FLAG_EOL_HUSH_ACTIVE = 1;
        
        #ifdef CONFIGURE_TIMER_SERIAL
            SER_Send_String("Start EOL Hush 24- ");
            SER_Send_Prompt();
        #endif        
        
    #else
        // Init 24 Hour Timer
        life_eol_hush_timer = MAIN_OneHourTic;
        FLAG_EOL_HUSH_ACTIVE = 1;
    #endif
}

#endif


#ifdef CONFIGURE_MANUAL_LIFE_COMMAND
/*
+------------------------------------------------------------------------------
| Function:      LIFE_Set_Accel_Life
|                LIFE_Clr_Accel_Life
+------------------------------------------------------------------------------
| Purpose:       For Life Test command
|                Sets / Clears Accelerated Life
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void LIFE_Set_Accel_Life(void)
{
    // Set 1 minute per Day
    life_min_per_day = 1;
}

void LIFE_Clr_Accel_Life(void)
{
    // Clr 1 minute per Day
    life_min_per_day = LFE_MINUTES_PER_DAY;
    
    // Reset Life Expiration Counter
    SYS_RamData.Life_LSB = 0;
    SYS_RamData.Life_MSB = 0;
    
    SYS_RamData.Life_Checksum = 0;
    SYS_Save();
    
}

/*
+------------------------------------------------------------------------------
| Function:      LIFE_Set_Eol_Test
+------------------------------------------------------------------------------
| Purpose:       Set Life Expiartion to a day before EOL
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void LIFE_Set_Eol_Test(void)
{
    // Pre-load Life Expiration
    SYS_RamData.Life_LSB = LIFE_LSB_INIT_EOL_VAL;
    SYS_RamData.Life_MSB = LIFE_MSB_INIT_EOL_VAL;
    
    SYS_RamData.Life_Checksum = SYS_RamData.Life_LSB + SYS_RamData.Life_MSB;
    SYS_Save();

    LIFE_Set_Accel_Life();
}

/*
+------------------------------------------------------------------------------
| Function:      LIFE_Set_Life_Days
+------------------------------------------------------------------------------
| Purpose:       Set Life Counter
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void LIFE_Set_Life_Days(uint life_days)
{
    // Set Life Counter
    SYS_RamData.Life_LSB = (uchar)(life_days & 0xFF);
    SYS_RamData.Life_MSB = (uchar)(life_days >> 8);
    
    SYS_RamData.Life_Checksum = SYS_RamData.Life_LSB + SYS_RamData.Life_MSB;
    SYS_Save();
    
    // Accelerated Life Mode
    LIFE_Set_Accel_Life();
}

#endif
