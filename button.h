/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        button.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for button.c module
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

#ifndef	BUTTON_H
#define	BUTTON_H


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/

    /* Button States */
    enum 
    {
        BTN_STATE_IDLE = 0,
        BTN_STATE_DEBOUNCE,
        BTN_STATE_ACTIVE1,
        BTN_STATE_RELEASED1,
        BTN_STATE_ACTIVE2,
        BTN_STATE_RELEASED2,
        BTN_STATE_TIMER,
        BTN_STATE_FUNCTION,
        BTN_STATE_WAIT_PTT,
        BTN_STATE_FUNC_TEST,
        BTN_STATE_WAIT_RELEASE,
        BTN_STATE_ISSUE_A_TEST,
        BTN_STATE_WAIT_FT_RELEASE,
        BTN_STATE_FUNCTION_2,
        BTN_STATE_FUNCTION_3,
        BTN_STATE_FUNCTION_4,
        BTN_STATE_TIMER_2,
        BTN_STATE_BTN_STUCK
    };


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
    uint BTN_Process_Task(void);
    uchar BTN_Get_State(void);
    void BTN_Chk_ALG_Off_Timer(void);




 

#endif  // BUTTON_H

