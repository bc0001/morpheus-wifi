/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        memory_18877.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for memory_18877.c module
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


#ifndef MEMORY_18877_H
#define	MEMORY_18877_H

#ifdef	__cplusplus
extern "C" {
#endif


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
    void MEMORY_Read_Block(uchar num_bytes, uchar source, uchar *dest);
    uchar MEMORY_Write_Block(uchar num_bytes, uchar *source, uchar dest);
    uchar MEMORY_Byte_Write(uchar byte, uchar dest);
    uchar MEMORY_Byte_Write_Verify(uchar byte, uchar dest);
    uchar MEMORY_Byte_Read(uchar addr);


#ifdef	__cplusplus
}
#endif

#endif	/* MEMORY_18877_H */

