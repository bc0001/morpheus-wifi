/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        systemdata.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Routines to support data structure control
|               
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/



/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include	"common.h"
#include 	"systemdata.h"
#include    "memory_18877.h"
#include    "diaghist.h"
#include    "fault.h"
#include	"life.h"


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/


#ifdef CONFIGURE_SMK_SLOPE_SIMULATION
    #define INIT_SLOPE_1_00     (uint)0x0100
#endif

/*
|-----------------------------------------------------------------------------
| Typedef and Enums and Structures
|-----------------------------------------------------------------------------
*/

// Set up a Data Structure
//  to organize Non-Volatile Memory data address locations
struct SYS_NV_Memory
{
    struct SystemData Primary;
    struct SystemData Backup;
    struct DIAG_HistData DiagHist;
};



/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/
// Define and locate Working Data Structure
struct SystemData volatile SYS_RamData @ 0x00A0;

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/

// Non-Volatile Data contains Primary, Backup data structures
// and Diagnostic History Information.
eeprom struct SYS_NV_Memory sys_nv_data =
{
     // Primary Copy of data Structure (0x00 - 0x0F)
     {
        CO_STATUS_INIT_VAL,
        CO_CHECKSUM_INIT_VAL,
        CO_OFFSET_LSB_INIT_VAL,
        CO_OFFSET_MSB_INIT_VAL,
        CO_SCALE_LSB_INIT_VAL,
        CO_SCALE_MSB_INIT_VAL,
        SMK_CAL_CAV_INIT_VAL,
        SMK_CAL_IRLED_PTT_INIT_VAL,
        LIFE_LSB_INIT_VAL,
        LIFE_MSB_INIT_VAL,
        LIFE_CHECKSUM_INIT_VAL,
        SMK_CAL_ALM_THOLD_INIT_VAL,
        SMK_CAL_LED_INIT_VAL,
        SMK_CAL_COR_THOLD_INIT_VAL,
        BAT_LB_THOLD_INIT_VAL,
        CHECKSUM

     },

     // backup Copy of data Structure (0x10 - 0x1F)
     {
        CO_STATUS_INIT_VAL,
        CO_CHECKSUM_INIT_VAL,
        CO_OFFSET_LSB_INIT_VAL,
        CO_OFFSET_MSB_INIT_VAL,
        CO_SCALE_LSB_INIT_VAL,
        CO_SCALE_MSB_INIT_VAL,
        SMK_CAL_CAV_INIT_VAL,
        SMK_CAL_IRLED_PTT_INIT_VAL,
        LIFE_LSB_INIT_VAL,
        LIFE_MSB_INIT_VAL,
        LIFE_CHECKSUM_INIT_VAL,
        SMK_CAL_ALM_THOLD_INIT_VAL,
        SMK_CAL_LED_INIT_VAL,
        SMK_CAL_COR_THOLD_INIT_VAL,
        BAT_LB_THOLD_INIT_VAL,
        CHECKSUM

     },

     // Diagnostic History Data Structure
     {
        // 0x20 - 0x2F
    #ifdef CONFIGURE_SMK_SLOPE_SIMULATION         
        0,0,0,0,0,0,0,INIT_SLOPE_1_00,0,0,0,00,REVISION_MAJOR,REVISION_MINOR,
    #else
         0,0,0,0,0,0,0,00,0,0,0,00,REVISION_MAJOR,REVISION_MINOR,
    #endif
        // 0x30 - 0x3F
        0,0,0,0,00,00,0,0,0,0,0,QPTR_CKSUM_INIT,MAJOR_QPTR_INIT,MINOR_QPTR_INIT,

        // 0x40 - 0x9F
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,

        // 0xA0 - 0xFF
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,

     }
};


/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/
uchar   sys_calc_checksum(uchar addr);
void    sys_load(uchar addr);


/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/



/*
+------------------------------------------------------------------------------
| Function:      SYS_Init
+------------------------------------------------------------------------------
| Purpose:       Copies the flash version of the system data structure into 
|                data memory
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  TRUE or FALSE (TRUE if Memory Checksum Fault)
+------------------------------------------------------------------------------
*/

uchar SYS_Init(void)
{
    /*
	 * Test that the data saved in flash is valid by verifying the checksum.
	 * The primary copy is checked first.
     */

    if( sys_calc_checksum(PRIMARY_START) != 
            MEMORY_Byte_Read((uchar)&sys_nv_data.Primary.Checksum) )
	{
		// Checksum of primary block is corrupted.  See if backup 
        //  block is correct.
		if( sys_calc_checksum(BACKUP_START) != 
                MEMORY_Byte_Read((uchar)&sys_nv_data.Backup.Checksum) )
		{
			// Both copies are corrupt.  That's a fatal fault.
			FLT_Fault_Manager(FAULT_MEMORY);

			return (TRUE);
		}
		else
		{
			// Copy Backup Data into System Ram Data Structure
            // and restore Primary Data Structure
            sys_load(BACKUP_START);
            SYS_Save();
		}
	}
	else
    {
        // Copy Primary Data into System Ram Data Structure
        sys_load(PRIMARY_START);
    }

	return (FALSE);
}


/*
+------------------------------------------------------------------------------
| Function:      sys_calc_checksum
+------------------------------------------------------------------------------
| Purpose:       Calculates checksum of system data structure in NV memory
|                Data Structure is 16 bytes long, CSUM is sum of 1st 15 bytes
|
|
|
+------------------------------------------------------------------------------
| Parameters:    addr - points to start of Data Structure to calculate
+------------------------------------------------------------------------------
| Return Value:  calculated checksum value
+------------------------------------------------------------------------------
*/

uchar sys_calc_checksum(uchar addr)
{
	uchar sum = 0;
    uint i;

	for(i = 0; i < ( sizeof(SYS_RamData) -1 ); i++)
	{
        uchar byte = MEMORY_Byte_Read(addr + i);
        sum += byte;
	}

	return sum;
}


/*
+------------------------------------------------------------------------------
| Function:      SYS_Calc_Ram_Checksum
+------------------------------------------------------------------------------
| Purpose:       Calculates checksum of system data structure in data memory
|                Data Structure is 16 bytes long, CSUM is sum of 1st 15 bytes
|
|
+------------------------------------------------------------------------------
| Parameters:    addr - points to start of Data Structure to calculate
+------------------------------------------------------------------------------
| Return Value:  calculated checksum value
+------------------------------------------------------------------------------
*/

uchar SYS_Calc_Ram_Checksum(uchar *addr)
{
	uchar sum = 0;
    uint i;

	for(i = 0; i < ( sizeof(SYS_RamData) - 1 ); i++)
	{
        uchar byte = *addr++;
        sum += byte;
	}

	return sum;
}


/*
+------------------------------------------------------------------------------
| Function:      sys_load
+------------------------------------------------------------------------------
| Purpose:       Load SYS Ram Data structure from NonVolatile memory copy
|
|
+------------------------------------------------------------------------------
| Parameters:    address of NonVolatile memory Copy
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void sys_load( uchar addr)
{
    uchar * ptr;
    ptr = (uchar*) &SYS_RamData;

	for(uchar i = 0; i != (sizeof(SYS_RamData)); i++)
	{
        *ptr = MEMORY_Byte_Read(addr+i);
        ptr++;
	}

}


/*
+------------------------------------------------------------------------------
| Function:      SYS_Save
+------------------------------------------------------------------------------
| Purpose:       Saves contents of SYS_RamData to Primary and Backup 
|                 copies located in NV memory.
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void SYS_Save(void)
{
    // 1st Calculate the new RamData checksum
    SYS_RamData.Checksum = SYS_Calc_Ram_Checksum((uchar*)&SYS_RamData);

    // Save Ram Data Structure to Non-Volatile Flash memory structures
    MEMORY_Write_Block((uchar) 
            sizeof(SYS_RamData),(uchar*)&SYS_RamData,PRIMARY_START);

    // Update backup Copy 
    MEMORY_Write_Block((uchar) 
            sizeof(SYS_RamData),(uchar*)&SYS_RamData,BACKUP_START);

}


/*
+------------------------------------------------------------------------------
| Function:      SYS_Validate_Data_Structure
+------------------------------------------------------------------------------
| Purpose:       Verify Data Structure Integrity
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Return FALSE = if Checksum Error detected
+------------------------------------------------------------------------------
*/

uchar SYS_Validate_Data_Structure(void)
{
    // Validate Data Structure Integrity every minute
    if( SYS_RamData.Checksum != SYS_Calc_Ram_Checksum((uchar*)&SYS_RamData) )
    {
        // Data Structure has checksum error!
        return FALSE;
    }

    // Checksum OK
    return TRUE;

}