/****************************************************************
 * The contents of this file are Kidde proprietary  and confidential.
 * *************************************************************/
// led_display.c

#include	"common.h"
#include	"led_display.h"
#include	"serial.h"
#include	"comeasure.h"
#include	"cocompute.h"
#include	"main.h"
#include        "fault.h"

#ifdef	_CONFIGURE_PEAK_BUTTON
    #include	"peak_button.h"
#endif

// LED display will only run in Active Mode
// as digital I/O is off during sleep and
// sleep times are greater than required refresh rate.

#define MAX_CO_DISPLAYED 		999	// in PPM

#define	SEGMENT_ON_TIME			3000	// in uSec

#define DISPLAY_UPDATE_PPM_RATE		4000	// in ms
#define DISPLAY_UPDATE_PPM_RATE_LP	45000	// in ms
#define DISPLAY_UPDATE_TEST_RATE	500	// in ms
#define DISPLAY_UPDATE_FAULT_RATE	4000	// in ms
#define DISPLAY_UPDATE_DEFAULT_RATE	100	// in ms
#define	DISPLAY_INIT_TIME		8000	// in ms
#define DISPLAY_UPDATE_ALM_RATE		1000	// in ms
#define DISPLAY_UPDATE_LB_RATE		2000	// in ms
#define DISPLAY_UPDATE_PCO_RATE         1000    // in ms
#define DISPLAY_UPDATE_CO_RATE          3000    // in ms
#define DISPLAY_UPDATE_OFF_RATE         1000    // in ms
#define DISPLAY_UPDATE_EOL_RATE		2000	// in ms


void Update_PPM(UINT) ;

#define	DISP_DIGIT_1	0
#define	DISP_DIGIT_2	1
#define	DISP_DIGIT_3	2


#define	DISP_UPDATE_INIT        0
#define	DISP_UPDATE_IDLE        1
#define	DISP_UPDATE_TEST        2
#define	DISP_UPDATE_LB          3
#define	DISP_UPDATE_ERR         4
#define	DISP_UPDATE_EOL         5
#define	DISP_UPDATE_EOL_HUSH    6
#define DISP_UPDATE_ALARM       7
#define DISP_UPDATE_PEAK_CO     8


// Used in nested display states
#define	TEST1 0
#define	TEST2 1
#define	TEST3 2


//TODO:  re-define LED Display Port mask bits
#define	PORTA_MASK		0x94
#define	PORTC_MASK		0x3C

#define	DIG_SELECT_MASK		0x34	// bits 4,5,2 of PortB
#define	DIGIT_1			0x10
#define	DIGIT_2			0x20
#define	DIGIT_3			0x04

// Decimal point not used on this model
//#define	DP_MASK			0x10	// decimal point, PortC


// Define Characters supported in Character Table 
// ( CharTable[] array indexes)
#define 	CHAR_0	0
#define 	CHAR_1	1
#define 	CHAR_2	2
#define 	CHAR_3	3
#define 	CHAR_4	4
#define 	CHAR_5	5
#define 	CHAR_6	6
#define 	CHAR_7	7
#define 	CHAR_8	8
#define 	CHAR_9	9
#define 	CHAR_A	10
#define 	CHAR_b	11
#define 	CHAR_C	12
#define 	CHAR_d	13
#define 	CHAR_E	14
#define 	CHAR_F	15
#define 	CHAR_L	16
#define 	CHAR_n	17
#define 	CHAR_r	18
#define 	CHAR_BLANK	19
#define		CHAR_DASH	20
#define         CHAR_H  21
#define         CHAR_P  22
#define         CHAR_LC_C   23
#define         CHAR_LC_O   24

// Basic Segment data structure
struct	Segment 
	{
            UCHAR   PortA ;
            UCHAR   PortC ;
 	} ;
	
// Current digit Digit Data Buffers

struct Segment	Digit_1_Data ;
struct Segment	Digit_2_Data ;
struct Segment	Digit_3_Data ;

//TODO:
// Re-Define each Characters Segment Bit/Mask here
// Low bits drive LED segments,
// High bits are unchanged (ANDed with current port value)
const struct Segment CharTable[25] =
	{ 
            // Ports
            // A    C
            {0x6B,0xE3},		// 0
            {0x7F,0xEF},		// 1
            {0xEB,0xCB},		// 2
            {0x7B,0xCB},		// 3
            {0x7F,0xC7},		// 4
            {0x7B,0xD3},		// 5
            {0x6B,0xD3},		// 6
            {0x7B,0xEF},		// 7
            {0x6B,0xC3},		// 8
            {0x7B,0xC3},		// 9
            {0x6B,0xC7},		// A
            {0x6F,0xD3},		// b
            {0xEB,0xF3},		// C
            {0x6F,0xCB},		// d
            {0xEB,0xD3},		// E
            {0xEB,0xD7},		// F
            {0xEF,0xF3},		// L
            {0x6F,0xDF},		// n
            {0xEF,0xDF},		// r
            {0xFF,0xFF},		// BLANK
            {0xFF,0xDF},                // -
            {0x6F,0xC7},                // H
            {0xEB,0xC7},                // P
            {0xEF,0xDB},                // LC c
            {0x6F,0xDB}                 // LC o
	} ;


UCHAR	Disp_State = DISP_DIGIT_1 ;
UCHAR	Disp_Update_State = DISP_UPDATE_INIT ;
UCHAR	Test_Disp_State ;                       // nested sub-state
UCHAR	Disp_Refresh_Count = LED_DISP_BRIGHT;	// Used to control brightness
UCHAR	Disp_Intensity = LED_DISP_BRIGHT;


// Global / Static Initialization
/*
void Led_Display_Init(void)
{
	Disp_State = DISP_DIGIT_1 ;	
	Disp_Update_State = DISP_UPDATE_INIT ;
	Disp_Intensity = LED_DISP_BRIGHT ;
	Disp_Refresh_Count = Disp_Intensity ;
}
*/

void Display_Set_Intensity(UCHAR inten)
{
	Disp_Intensity = inten ;
}

UCHAR Display_Get_Intensity(void)
{
	return Disp_Intensity ;
}

// Turns the LED Display OFF
void Display_Off(void)
{
    Disp_Intensity = LED_DISP_OFF;

    Digit_1_Data = CharTable[CHAR_BLANK] ;
    Digit_2_Data = CharTable[CHAR_BLANK] ;
    Digit_3_Data = CharTable[CHAR_BLANK] ;

    // All Digit Selects Set LOW
    LATB &= ~DIG_SELECT_MASK ;			// without Drive Transistors

    // Segments all Disabled (segment high = off)
    LATA |= PORTA_MASK ;
    LATC |= PORTC_MASK ;


}

// Refresh the Display by scanning thru the 3 digits
void Display_Refresh_Task (void)
{
	
	// First, turn OFF previous Digit position and Data segments
	// Digit Selects Off (select low = off)
	LATB &= ~DIG_SELECT_MASK ;			// without Drive Transistors
	
	// Digit Selects Off (select high = off)
	// LATB |= DIG_SELECT_MASK ;			// with drive transistors

	// Segments all disabled (segment high = off)
	LATA |= PORTA_MASK ;
	LATC |= PORTC_MASK ;

	
		
	
  if( (Disp_Intensity != LED_DISP_OFF) && (FLAG_DISPLAY_OFF == 0) )
  {
		
	switch (Disp_State)
	{
		case DISP_DIGIT_1:
		
			// Load Segment Data for this Digit position
			LATA &= Digit_1_Data.PortA ;
			LATC &= Digit_1_Data.PortC ;

		
			if( --Disp_Refresh_Count != 0)
			{
				break ;
			}
		
			// Enable Digit select and exit
			LATB |= DIGIT_1 ;			// select digit 1 without drive transistors

			// Enable Digit select and exit
			//P1OUT &= ~DIGIT_1 ;			// select digit 1 with drive Transistors

			Disp_State = DISP_DIGIT_2 ;
			Disp_Refresh_Count = Disp_Intensity ;
		break;

		case DISP_DIGIT_2:
		
			// Load Segment Data for this Digit position
			LATA &= Digit_2_Data.PortA ;
			LATC &= Digit_2_Data.PortC ;


			if( --Disp_Refresh_Count != 0)
			{
				break ;
			}
		
			// Enable Digit select and exit
			LATB |= DIGIT_2 ;			// select digit 2 without drive transistors

			// Enable Digit select and exit
			// P1OUT &= ~DIGIT_2 ;			// select digit 2 with drive Transistors
	
	
			Disp_State = DISP_DIGIT_3 ;
			Disp_Refresh_Count = Disp_Intensity ;
		break;

		case DISP_DIGIT_3:
		
			// Load Segment Data for this Digit position
			LATA &= Digit_3_Data.PortA ;
			LATC &= Digit_3_Data.PortC ;

		
			if( --Disp_Refresh_Count != 0)
			{
				break ;
			}
			// Enable Digit select and exit
			LATB |= DIGIT_3 ;			// select digit 3 without drive transistors

			// Enable Digit select and exit
			// P1OUT &= ~DIGIT_3 ;			// select digit 3 with drive Transistors
		
			Disp_State = DISP_DIGIT_1 ;
			Disp_Refresh_Count = Disp_Intensity ;
		break;
	
	}
  }

 }





// Update Displayed Data buffer depending upon
// current mode of operation
// ( PPM, Low Battery, Error, End of Life... etc)
UINT Display_Update_Task (void) 
{
	//To measure task time
	//Main_TP_On();
	
	// Test Code Only
	/*
	if(Disp_Intensity == LED_DISP_OFF)
		Disp_Intensity = LED_DISP_BRIGHT ;
	else if(Disp_Intensity == LED_DISP_BRIGHT)
		Disp_Intensity = LED_DISP_MEDIUM ;
	else if(Disp_Intensity == LED_DISP_MEDIUM)
		Disp_Intensity = LED_DISP_LOW ;
	else if(Disp_Intensity == LED_DISP_LOW)
		Disp_Intensity = LED_DISP_OFF ;
	*/
      if( Disp_Intensity != LED_DISP_OFF)
      {
	
	switch (Disp_Update_State)
	{
		
		case DISP_UPDATE_INIT:
			// Display '888'
			Digit_1_Data = CharTable[CHAR_8] ;
			Digit_2_Data = CharTable[CHAR_8] ;
			Digit_3_Data = CharTable[CHAR_8] ;
			
			Disp_Update_State = DISP_UPDATE_IDLE ;
		
			return	Make_Return_Value(DISPLAY_INIT_TIME) ;
		
		case DISP_UPDATE_IDLE:
		
			// Test for Display State change
                        if((FLAG_CO_ALARM_ACTIVE) && (!FLAG_PTT_ACTIVE))
                        {
                                Test_Disp_State = TEST1 ;
                                Disp_Update_State = DISP_UPDATE_ALARM ;
                        }
                        else if(FLAG_EOL_FATAL)
			{
				Disp_Update_State = DISP_UPDATE_EOL ;
				
			}
			else if( (FLAG_EOL_MODE)&&(!FLAG_EOL_HUSH_ACTIVE) )
			{
				Disp_Update_State = DISP_UPDATE_EOL ;
				
			}
			else if(FLAG_PTT_ACTIVE)
			{
				Test_Disp_State = TEST1 ;
				Disp_Update_State = DISP_UPDATE_TEST ;
				
			}
			else if(FLAG_FAULT_FATAL || FLAG_CAL_ERROR)
			{
				Disp_Update_State = DISP_UPDATE_ERR ;
			
			}
			else if(FLAG_LOW_BATTERY)
			{
				Test_Disp_State = TEST1 ;
				Disp_Update_State = DISP_UPDATE_LB ;
				
			}
			else if(FLAG_EOL_HUSH_ACTIVE )
			{
				Test_Disp_State = TEST1 ;
				Disp_Update_State = DISP_UPDATE_EOL_HUSH ;
				
			}
                        else if (!FLAG_PTT_ACTIVE && (FLAG_CO_PEAK_MEMORY)  )
                        {
				Test_Disp_State = TEST1 ;
				Disp_Update_State = DISP_UPDATE_PEAK_CO ;
                        }
			else
			{
				if(FLAG_DISP_UPDATE_ENABLED)
				{
					Update_PPM (wPPM) ;
                                        /*
					if(FLAG_LED_DISP_DP)
					{
						Digit_3_Data.PortC &= ~DP_MASK ;
						FLAG_LED_DISP_DP = 0 ;
					}
					else
					{
						Digit_3_Data.PortC |= DP_MASK ;
						FLAG_LED_DISP_DP = 1 ;
					}
                                        */
						
				}
				else
				{
					Update_PPM (0) ;
				}
				
				if(FLAG_MODE_ACTIVE)
				{
					return	Make_Return_Value(DISPLAY_UPDATE_PPM_RATE) ;
				}
				else 
				{
					// Don't need to update as often in Low Power Mode
					return	Make_Return_Value(DISPLAY_UPDATE_PPM_RATE_LP) ;
				}
				
			}
		break;	

		
		case DISP_UPDATE_TEST:
		
			switch(Test_Disp_State)
			{
				case TEST1:
					Digit_1_Data = CharTable[CHAR_DASH] ;
					Digit_2_Data = CharTable[CHAR_BLANK] ;
					Digit_3_Data = CharTable[CHAR_BLANK] ;
					Test_Disp_State = TEST2 ;
				break;

				case TEST2:
					Digit_1_Data = CharTable[CHAR_BLANK] ;
					Digit_2_Data = CharTable[CHAR_DASH] ;
					Test_Disp_State = TEST3 ;
				break;

				case TEST3:
					Digit_2_Data = CharTable[CHAR_BLANK] ;
					Digit_3_Data = CharTable[CHAR_DASH] ;
					Test_Disp_State = TEST1 ;
				break;

			}
			return	Make_Return_Value(DISPLAY_UPDATE_TEST_RATE) ;


		case DISP_UPDATE_LB:

		if((FLAG_LOW_BATTERY == 1)
                    && (FLAG_PTT_ACTIVE == 0) 
                    && (FLAG_CO_ALARM_ACTIVE == 0)
                    && (FLAG_FAULT_FATAL == 0) )
		{
			switch(Test_Disp_State)
			{
                            case TEST1:
                                Digit_1_Data = CharTable[CHAR_L] ;
                                Digit_2_Data = CharTable[CHAR_b] ;
                                Digit_3_Data = CharTable[CHAR_BLANK] ;


                                // Set the dec point bit during LB msg
                                //Digit_3_Data.PortC &= ~DP_MASK ;
                                if(FLAG_LB_HUSH_ACTIVE)
                                {
                                    Test_Disp_State = TEST2 ;
                                    return Make_Return_Value(DISPLAY_UPDATE_OFF_RATE) ;
                                }
                                else
                                {
                                    Test_Disp_State = TEST3 ;
                                }
                            break;

                            case TEST2:
                                Digit_1_Data = CharTable[CHAR_0] ;
                                Digit_2_Data = CharTable[CHAR_F] ;
                                Digit_3_Data = CharTable[CHAR_F] ;
                                Test_Disp_State = TEST3 ;

                                return Make_Return_Value(DISPLAY_UPDATE_OFF_RATE) ;
                            //break;

                            case TEST3:
                                if(FLAG_DISP_UPDATE_ENABLED)
                                {
                                    Update_PPM (wPPM) ;
                                    // Clr the dec point bit during PPM msg
                                    //Digit_3_Data.PortC |= DP_MASK ;
                                }
                                else
                                {                                
                                    Update_PPM (0) ;
                                    // Clr the dec point bit during PPM msg
                                    //Digit_3_Data.PortC |= DP_MASK ;
                                }

                                Test_Disp_State = TEST1 ;
                            break;

			}
		
			return	Make_Return_Value(DISPLAY_UPDATE_LB_RATE) ;
		}
		else
		{
			Disp_Update_State = DISP_UPDATE_IDLE ;
			break ;	
		}

		case DISP_UPDATE_ERR:
                    Digit_1_Data = CharTable[CHAR_E] ;
                    Digit_2_Data = CharTable[CHAR_r] ;
                    Digit_3_Data = CharTable[CHAR_r] ;

                    if (PORT_PEAK_BTN == 1)
                    {
                        // Display error Code
                        Digit_2_Data = CharTable[CHAR_0] ;

                        switch(FatalFaultCode)
                        {
                            case FAULT_CONVERSION_SETUP:
                                Digit_3_Data = CharTable[FAULT_CONVERSION_SETUP] ;
                                break;

                            case FAULT_SENSOR_SHORT:
                                Digit_3_Data = CharTable[FAULT_SENSOR_SHORT] ;
                                break;

                            case FAULT_CALIBRATION:
                                Digit_3_Data = CharTable[FAULT_CALIBRATION] ;
                                break;

                            case FAULT_PTT:
                                Digit_3_Data = CharTable[FAULT_PTT] ;
                                break;

                            case FAULT_MEMORY:
                                Digit_3_Data = CharTable[FAULT_MEMORY] ;
                                break;

                            case FAULT_EXPIRATION:
                                Digit_3_Data = CharTable[FAULT_EXPIRATION] ;
                                break;

                        }

                    }

                    Disp_Update_State = DISP_UPDATE_IDLE ;
                    return	Make_Return_Value(DISPLAY_UPDATE_FAULT_RATE) ;
			
		
	
		case DISP_UPDATE_EOL:

			Digit_1_Data = CharTable[CHAR_E] ;
			Digit_2_Data = CharTable[CHAR_n] ;
			Digit_3_Data = CharTable[CHAR_d] ;

			Disp_Update_State = DISP_UPDATE_IDLE ;
			return	Make_Return_Value(DISPLAY_UPDATE_FAULT_RATE) ;
 

		case DISP_UPDATE_EOL_HUSH:		

                    if((FLAG_EOL_HUSH_ACTIVE)
                    && (FLAG_PTT_ACTIVE == 0)
                    && (FLAG_CO_ALARM_ACTIVE == 0) )
                    {

			  switch(Test_Disp_State)
			  {
                            case TEST1:
                                if(FLAG_DISP_UPDATE_ENABLED)
                                {
                                        Update_PPM (wPPM) ;
                                        // Clr the dec point bit during PPM msg
                                        //Digit_3_Data.PortC |= DP_MASK ;
                                }
                                Test_Disp_State = TEST2 ;
                            break;

                            case TEST2:
                                Digit_1_Data = CharTable[CHAR_E] ;
                                Digit_2_Data = CharTable[CHAR_n] ;
                                Digit_3_Data = CharTable[CHAR_d] ;

                                // Set the dec point bit during End msg
                                //Digit_3_Data.PortC &= ~DP_MASK ;

                                Test_Disp_State = TEST3 ;
                                return Make_Return_Value(DISPLAY_UPDATE_OFF_RATE) ;

                            case TEST3:
                                Digit_1_Data = CharTable[CHAR_0] ;
                                Digit_2_Data = CharTable[CHAR_F] ;
                                Digit_3_Data = CharTable[CHAR_F] ;
                                Test_Disp_State = TEST1 ;

                                return Make_Return_Value(DISPLAY_UPDATE_OFF_RATE) ;

			  }

			  return Make_Return_Value(DISPLAY_UPDATE_EOL_RATE) ;
                    }
                    else
                    {
                            Disp_Update_State = DISP_UPDATE_IDLE ;
                            break ;
                    }

		case DISP_UPDATE_ALARM:

                    if(FLAG_CO_ALARM_ACTIVE)
                    {
                        switch(Test_Disp_State)
                        {
                                case TEST1:
                                        Digit_1_Data = CharTable[CHAR_C] ;
                                        Digit_2_Data = CharTable[CHAR_0] ;
                                        Digit_3_Data = CharTable[CHAR_BLANK] ;

                                        // Set the dec point bit during LB msg
                                        //Digit_3_Data.PortC &= ~DP_MASK ;

                                        Test_Disp_State = TEST2 ;
                                break;

                                case TEST2:
                                        if(FLAG_DISP_UPDATE_ENABLED)
                                        {
                                                Update_PPM (wPPM) ;
                                                // Clr the dec point bit during PPM msg
                                                //Digit_3_Data.PortC |= DP_MASK ;
                                        }
                                        Test_Disp_State = TEST1 ;
                                break;
                        }

                        return	Make_Return_Value(DISPLAY_UPDATE_ALM_RATE) ;
                    }
                    else
                    {
                        Disp_Update_State = DISP_UPDATE_IDLE ;
                        break ;
                    }

		case DISP_UPDATE_PEAK_CO:

		if((FLAG_CO_PEAK_MEMORY == 1)
                    && (FLAG_PTT_ACTIVE == 0)
                    && (FLAG_CO_ALARM_ACTIVE == 0)
                    && (FLAG_FAULT_FATAL == 0)
                    && (FLAG_LOW_BATTERY == 0))
		{
			switch(Test_Disp_State)
			{
                            case TEST1:
                                if(PORT_PEAK_BTN == 0)
                                {
                                    Digit_1_Data = CharTable[CHAR_P] ;
                                    Digit_2_Data = CharTable[CHAR_C] ;
                                    Digit_3_Data = CharTable[CHAR_0] ;
                                }

                                Test_Disp_State = TEST2 ;
                                return	Make_Return_Value(DISPLAY_UPDATE_PCO_RATE) ;

                            //break;

                            case TEST2:
                                if(FLAG_DISP_UPDATE_ENABLED)
                                {
                                    Update_PPM (wPPM) ;
                                    // Clr the dec point bit during PPM msg
                                    //Digit_3_Data.PortC |= DP_MASK ;
                                }
                                else
                                {
                                    Update_PPM (0) ;
                                    // Clr the dec point bit during PPM msg
                                    //Digit_3_Data.PortC |= DP_MASK ;
                                }

                                Test_Disp_State = TEST1 ;
                                return	Make_Return_Value(DISPLAY_UPDATE_CO_RATE) ;

                            //break;

			}
			
		}
		else
		{
			Disp_Update_State = DISP_UPDATE_IDLE ;
			break ;
		}
        }

      }
      return	Make_Return_Value(DISPLAY_UPDATE_DEFAULT_RATE) ;
}


// Convert passed PPM value to LED display format
//  and update Digit Buffers
void Update_PPM (UINT ppm)
{
	char 	digits[4];		// 3 digits and a terminating null
	if (ppm > MAX_CO_DISPLAYED)
	  	ppm = MAX_CO_DISPLAYED ;
	 
	 // If Peak Button is configured, test for display Peak value instead of PPM	
	 #ifdef	_CONFIGURE_PEAK_BUTTON
		if ( PEAK_BTN_STATE_FUNCTION == Peak_BTN_Get_State() )
		{
                    // Override ppm with peak ppm value
                    ppm = wPeakPPM;
		}
	 #endif
	
	digits[0] = 0;
	digits[1] = 0;
	digits[2] = 0;
	digits[3] = 0;
	   
	itoa(ppm, digits, 10) ;
	
	// Need to arrange digit array depending upon 
	//  number of digits of result 
	// (from left justifuy to right justify)
	if( (digits[2] == 0) && (digits[1] == 0) )
	{
		digits[2] = digits[0] ;
		digits[0] = 0;
	}
	else if (digits[2] == 0)
	{
		digits[2] = digits[1] ;
		digits[1] = digits[0] ;
		digits[0] = 0;
	}
	
	if(digits[2] == 0)
	{
		Digit_3_Data = CharTable[CHAR_BLANK] ;
	}
	else
	{
		UCHAR d3 = (digits[2] & 0xF) ;
		Digit_3_Data = CharTable[d3] ;
	}
	
	if(digits[1] == 0)
	{
		Digit_2_Data = CharTable[CHAR_BLANK] ;
	}
	else
	{
		UCHAR d2 = (digits[1] & 0xF) ;
		Digit_2_Data = CharTable[d2] ;
	}

	if(digits[0] == 0)
	{
		Digit_1_Data = CharTable[CHAR_BLANK] ;
	}
	else
	{
		UCHAR d1 = (digits[0] & 0xF) ;
		Digit_1_Data = CharTable[d1] ;
	}

}




