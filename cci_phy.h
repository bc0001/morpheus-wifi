/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        cci_phy.h
|  Author:      stan Burnette (updated by Bill Chandler for Morpheus 1_5)
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for cci_phy.c module
|               CCI Interface, physical layer
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

#ifndef CCI_PHY_H
#define	CCI_PHY_H


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/
// Error returns for CCI_Phy_Send function
#define CCI_SEND_SUCCESS        (uchar)0
#define CCI_SEND_ERROR_RXACK	(uchar)21
#define CCI_SEND_ERROR_RXRDY	(uchar)22
#define CCI_SEND_DATA_PIN_BSY   (uchar)23




// Define assignments for CCI Hardware Interrupts

// CCI Data Signal Int Flag
#define CCI_PIN_INTERRUPT_FLAG      IOCBFbits.IOCBF1

// Falling Edge, Bit 1 = Data Pin
// Turning off the Falling edge bit should disable CCI Ints
#define CCI_PIN_INTERRUPT_EDGE      IOCBNbits.IOCBN1

// For testing without a RF module we should pull up the Data Pin
//  to prevent endless interrupts using Weak pull-ups internal to PIC
#define CCI_DATA_WPU_ENABLE  WPUBbits.WPUB1 = 1;
#define CCI_CLK_WPU_ENABLE   WPUBbits.WPUB2 = 1;



/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/
typedef union {
    struct {
        unsigned FLAG_CCI_PACKET_RECEIVED   :1;
        unsigned _1   :1;
        unsigned _2   :1;
        unsigned _3   :1;
        unsigned _4   :1;
        unsigned _5   :1;
        unsigned _6   :1;
        unsigned _7   :1;
    };

    unsigned char ALL;

} CCI_Flags_t;

enum CC_Rx_Result
{
  CCI_RECEIVE_SUCCESS = 0,

  // Timeout waiting for the clock to go high during packet receive.
  CCI_RECEIVE_TO_CLOCK_HIGH = 1,

  // Timeout wating for the clock to go low during packet receive.
  CCI_RECEIVE_TO_CLOCK_LOW = 2,

  // Timeout wating for the clock to go high during handshake sequence.
  CCI_RECEIVE_TO_CLK_HANDSHAKE = 3,

  // Receive at End of Function
  CCI_RECEIVE_END = 5

};

// Define union for CCI packet Structure
union  tag_CCI_Packet
{
    struct
    {
        uchar data;
        uchar tag;
    };

    unsigned int word;

};


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/
extern volatile CCI_Flags_t CCI_Flags;


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
#ifdef CONFIGURE_WIRELESS_MODULE

    // CCI Module functions
    void CCI_Phy_Init(void);
    
    uchar CCI_Phy_Send(union tag_CCI_Packet p);
    enum CC_Rx_Result CCI_Phy_Receive(void);
    uchar CCI_Get_Rx_Packet(union tag_CCI_Packet *);
    uchar CCI_Phy_Get_MS_Timer(void);
    void CCI_Phy_RST_MS_Timer(void);

    uchar CCI_Phy_Get_Microsecond_Timer(void);
    void CCI_Phy_RST_Microsecond_Timer(void);
    
 
#endif






#endif	/* CCI_PHY_H */

