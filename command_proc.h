/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        command_proc.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for command_proc.c module
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


#ifndef	COMMAND_PROC_H
#define	COMMAND_PROC_H


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/
 

/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
    unsigned char CMD_Processor( char const *string);

    void CMD_Buffer_Push(char ch);
    void CMD_Process_Command(void);
   
    #ifdef CONFIGURE_SMK_SLOPE_SIMULATION    
        uint CMD_Get_Slope(void);
        CMD_Set_Slope(uint slope);
    #endif

#endif  // COMMAND_PROC_H
