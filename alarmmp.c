/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        alarmmp.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Routines for CCI Alarm Messaging Support
|
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include "common.h"

#ifdef CONFIGURE_WIRELESS_MODULE

#include "alarmmp.h"
#include "app.h"


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

// *****************************************************************************
// Debug, Test Only Defines (re-define as needed)

#ifdef CONFIGURE_UNDEFINED
    #define DBUG_OUTPUT_PULSE
#endif

// *****************************************************************************

// These are 8 bit Timer values set for 64 uSec Clock
#define TIME_MS_15              (uchar)(15000/64)  // 15  ms with 64 usec clk
#define TIME_MS_45              (uchar)(45000/64)  // 45  ms with 64 usec clk

#ifdef CONFIGURE_WIFI
    #define AMP_RESPONSE_TIMEOUT_MS_15     TIME_MS_15
#else
    #define AMP_RESPONSE_TIMEOUT_MS     TIME_MS_15
#endif


// Increased retries to test how long response was taking from the RF module
// Sometimes it took up to 6 retries
#define AMP_NUM_RETRIES                 7

#define AMP_TRANSACTION_COMPLETE        0
#define AMP_TRANSACTION_INCOMPLETE      1

#define AMP_CCI_MAX_3_RETRIES           3



/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/

// Transport Manager states.
enum
{
    AMP_TM_STATE_IDLE = 0,
    AMP_TM_STATE_WAIT_RESPONSE = 1
};


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/
uchar AMP_Callback_Status = 0;

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/

static union tag_CCI_Packet AMP_TxPacket;
static union tag_CCI_Packet AMP_RxPacket;
static union tag_CCI_Packet AMP_TxAckPacket;

static uchar amp_tm_state = AMP_TM_STATE_IDLE;
static volatile uchar amp_status = 0;
static volatile uchar amp_cci_err_retries = 0;




/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/
void AMP_Status_Callback(uchar s);

/*
 * Declare a function pointer AMP_Callback and initialize it
 * to the AMP_Status_Callback function
 */
void (*pAMP_Callback)(uchar data) = AMP_Status_Callback;



/*
+------------------------------------------------------------------------------
| Function:      AMP_Status_Callback
+------------------------------------------------------------------------------
| Purpose:       Declare a Callback Function that accepts unsigned int
|
|
+------------------------------------------------------------------------------
| Parameters:    s - callback status
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void AMP_Status_Callback(uchar s)
{
    // This function may be called during Transactions to Record Status
    AMP_Callback_Status = s;
    
    switch (AMP_Callback_Status)
    {
        case CCI_SEND_ERROR_RXACK:
            // This is now pushed into Serial Port Buffer for output later
            //  so it doesn't corrupt Interrupt Timing
            SER_Send_String("*CB RX-ACK*");
            SER_Send_Prompt();

         break;
         
        case CCI_SEND_ERROR_RXRDY:
            
            #ifndef CONFIGURE_UNDEFINED
                if(FLAG_WIRELESS_DISABLED)
                {
                    SER_Send_String("*CB CCI-OFF*");
                    SER_Send_Prompt();
                }
            #endif
    
            // This is now pushed into Serial Port Buffer for output later
            //  so it doesn't corrupt Interrupt Timing
            SER_Send_String("*CB RX-RDY*");
            SER_Send_Prompt();

         break;

        case FLAG_AMP_STATUS_TX_ERROR:
            // This is now pushed into Serial Port Buffer for output later
            //  so it doesn't corrupt Interrupt Timing
            SER_Send_String("*CB TX*");
            SER_Send_Prompt();

        break;
        
        case CCI_SEND_DATA_PIN_BSY:
            
            #ifndef CONFIGURE_UNDEFINED
                if(FLAG_WIRELESS_DISABLED)
                {
                    SER_Send_String("*CB CCI-OFF*");
                    SER_Send_Prompt();
                }
            #endif

           // This is now pushed into Serial Port Buffer for output later
            //  so it doesn't corrupt Interrupt Timing
            SER_Send_String("*CB BSY*");
            SER_Send_Prompt();
            
        break;
        
        default:
            
        break;    
        
    }

}

#ifdef DBUG_OUTPUT_PULSE
/*
+------------------------------------------------------------------------------
| Function:      amp_send_debug_pulse
+------------------------------------------------------------------------------
| Purpose:       Output Debug Pulses to Test Point
|                Used for Test Only, to monitor code progress with a scope
|
+------------------------------------------------------------------------------
| Parameters:    val = number of pulses
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

    void amp_send_debug_pulse(uchar val)
    {
        uchar i;

        for(i = 0; i < val; i++)
        {
            LAT_DEBUG_TP2_PIN = 1;
            //  add some short delay here
            PIC_delay_us(DBUG_DLY_8_USec);

            LAT_DEBUG_TP2_PIN = 0;
            //  add some short delay here
            PIC_delay_us(DBUG_DLY_8_USec);
        }
    }
#endif






/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/


#ifdef CONFIGURE_UNDEFINED
/*
+------------------------------------------------------------------------------
| Function:      AMP_Register_Callback
+------------------------------------------------------------------------------
| Purpose:       Register a Callback function
|
|
+------------------------------------------------------------------------------
| Parameters:    funcptr - pointer to the callback function
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void AMP_Register_Callback(void (*funcptr)(uchar data))
{
    pAMP_Callback = funcptr;
}
#endif


/*
+------------------------------------------------------------------------------
| Function:      AMP_Start_Transaction
+------------------------------------------------------------------------------
| Purpose:       This function will copy the packet data into module level
|                variables and set the start flag for the transport manager.
|
+------------------------------------------------------------------------------
| Parameters:    Txpacket  - Packet to Transmit
+------------------------------------------------------------------------------
| Return Value:  Returns non-zero if a transation is already in progress.
+------------------------------------------------------------------------------
*/

uchar AMP_Start_Transaction(union tag_CCI_Packet TxPacket)
{	
    if(!FLAG_WIRELESS_DISABLED)
    {
        AMP_TxPacket.word = TxPacket.word;
        amp_status = FLAG_AMP_STATUS_START_TRANSACTION;
        
        CLRWDT();
        
        /*
         * Wait until the start transaction is cleared indicating that
         * the packet has been sent or error.  This must be used only with an 
         * interrupt driven AMP transport or else it will hang here.
         */
        while(FLAG_AMP_STATUS_START_TRANSACTION == amp_status)
        {
            // Wait until the start transaction flag is cleared
            // Within 1 ms this should be transmitted
            
            #ifdef CONFIGURE_WDT_RST_FLAGS
                // Test Only, Remove later
                FLAG_WDT_1_7 = 1;
            #endif
        }

        #ifdef CONFIGURE_UNDEFINED
            // Test Only to see if Transaction Errors Occur here
            if(AMP_LOCATE == TxPacket.tag)
            {
                SER_Send_Int('O', amp_status, 10);
                SER_Send_Prompt();
            }
        #endif
 
        
        #ifdef CONFIGURE_WDT_RST_FLAGS
            // Test Only, Remove later
            FLAG_WDT_1_7 = 0;
        #endif
    }
    return amp_status;
}


/*
+------------------------------------------------------------------------------
| Function:      AMP_Transport_Manager
+------------------------------------------------------------------------------
| Purpose:       Handles CCI reception/transmission of packets with response
|                Called from ISR every 1ms
|                Looks for any received packets or AMP_status requests to
|                transmit a packet
|
|                Transport Status handles with AMP_Callback Function
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Returns 0
|
+------------------------------------------------------------------------------
*/

uchar AMP_Transport_Manager(void)
{
    static uchar retry_count = 0;
    static uchar num_retries;

    #ifdef CONFIGURE_DBUG_STACK_TEST
        //Stack, Test Only
        if(MAIN_Stack_Ptr_Peak < STKPTR)
        {
            MAIN_Stack_Ptr_Peak = STKPTR;
        }
    #endif

    /*
     * The transport manager will send the packet to the receiver and wait
     * for a valid reply.  There is a timeout on the reply to each packet sent.
     * The manager will also re-try number of times, returning a timeout error
     *  if too many retries have been done.
     */
    switch (amp_tm_state)
    {

        case AMP_TM_STATE_IDLE:
            
            // Test Only
            #ifdef CONFIGURE_DEBUG_1_MICROSEC_TIMER
                #define TEST_TIME_50_USEC   (50-1)

                // Test the CCI uSec Timer (every ms, 50 usecs)
                // Initialize the USEC Timer
                CCI_Phy_RST_Microsecond_Timer();
               
                LAT_DEBUG_TP2_PIN = 1;
                do
                {
                    // Just monitor timer since we always start at 0
                    if(TEST_TIME_50_USEC <= CCI_Phy_Get_Microsecond_Timer())
                    {
                        LAT_DEBUG_TP2_PIN = 0;
                        break;
                    }

                } while(1);
            #endif

            // Test Only
            #ifdef CONFIGURE_DEBUG_CCI_MS_TIMER
                
                #define TEST_TIME_256_USEC   (256/64)   //.25 ms

                // Test the CCI MS Timer
                // Initialize the USEC Timer (every ms, 250 usecs)
                CCI_Phy_RST_MS_Timer();
               
                LAT_DEBUG_TP2_PIN = 1;
                do
                {
                    // Just monitor timer since we always start at 0
                    if(TEST_TIME_256_USEC <= CCI_Phy_Get_MS_Timer())
                    {
                        LAT_DEBUG_TP2_PIN = 0;
                        break;
                    }

                } while(1);
            #endif

            /*
             * ****************************************************************
             *   Note:
             *   FLAG_CCI_PACKET_RECEIVED is tested every 1 MS and
             *   launches into Process Request if one has been received.
             * ****************************************************************

             *
             * See if a packet has been received.  This will get a packet
             * that is sent to a receiver.  The packet must be processed and
             * acknowledged.
             *
             */
             
             // Notify INT routine if CCI activity occurred  
             FLAG_TM_NO_ACTIONS = 1;
             
            if(!CCI_Get_Rx_Packet(&AMP_RxPacket))
            {
                
                // Packet received.  
                if(FLAG_AMP_TIMEOUT)
                {
                    // There was a timeout on the last transaction.  In this 
                    // event, the receiver might send a response packet that
                    // would be interpreted as the first part of a transaction.
                    // We'll ignore it here, just in case.  If it's a valid
                    // start to a transaction, the sender will resend.
                    FLAG_AMP_TIMEOUT = 0 ;
                    break ;
                }
                
                FLAG_TM_NO_ACTIONS = 0;
                
                // TODO: 
                // BC_2021 I think we should declare and create 
                // AMP_TxAckPacket to hold Process Request Packets instead of 
                // using AMP_TxPacket in case it might have a valid pending 
                // packet to transmit.
                
                //
                // Packet received.  Call function to figure out what to do
                // with it.
                //AMP_TxPacket = APP_Process_Request(&AMP_RxPacket);
                AMP_TxAckPacket = APP_Process_Request(&AMP_RxPacket);
				
                // If the tag is zero, that means that something is out
                //   of sync.
                //if(0 == AMP_TxPacket.tag)
                if(0 == AMP_TxAckPacket.tag)
                {
                    break;
                }

                // This is the response to the received packet
                //CCI_Phy_Send(AMP_TxPacket);
                CCI_Phy_Send(AMP_TxAckPacket);
                
                amp_tm_state = AMP_TM_STATE_IDLE;
            }

         #ifndef CONFIGURE_UNDEFINED 
            // Tested OK - Single attempt within same Interrupt   
            if(FLAG_AMP_STATUS_START_TRANSACTION & amp_status)
            {
                FLAG_TM_NO_ACTIONS = 0;
                
                // A transaction has been requested, send the packet.
                uchar app_send_status = CCI_Phy_Send(AMP_TxPacket);
                
                if(CCI_SEND_SUCCESS != app_send_status)
                {
                    // Error has occurred during transmit portion of CCI
                    // Callback with specific Send error.
                    pAMP_Callback(app_send_status);

                    // Update Status as TX Error
                    amp_status = FLAG_AMP_STATUS_TX_ERROR;

                    amp_tm_state = AMP_TM_STATE_IDLE;
                    
                    #ifdef CONFIGURE_WIRELESS_CCI_DEBUG
                        // Output error indication
                        if(FLAG_SERIAL_PORT_ACTIVE)
                        {
                            // On CCI error Output Tag/Data Zeros
                            // Buffer TX Activity in CCI Debug Queue
                            //  For now assume we will never fill the 
                            //  queue so don't examine queue
                            APP_CCI_QPush(APP_MSG_TRANSMIT);
                            APP_CCI_QPush(0x00);
                            APP_CCI_QPush(0x00);
                            APP_CCI_QPush(APP_MSG_TRANSMIT);
                            APP_CCI_QPush(AMP_TxPacket.tag);
                            APP_CCI_QPush(AMP_TxPacket.data);
                        }
                    #endif

                    // Try To Re-transmit Packet from Handshake Error
                    FLAG_CCI_HS_RETRY = 1;
                    // Save entire packet for re-transmission
                    AMP_RetryPacket.word = AMP_TxPacket.word;
                    break;
                    
                }
                
                // Successful Packet Transmitted
                FLAG_CCI_HS_RETRY = 0;
                
                // Clear the time out flag for the next state.
                FLAG_AMP_TIMEOUT = 0 ;
                
                // Transition to wait for response
                amp_tm_state = AMP_TM_STATE_WAIT_RESPONSE;
                
                // Init response Timeout Retry Counter
                retry_count = 0;
                
                // Initialize the timeout timer for Response
                CCI_Phy_RST_MS_Timer();         // Timer starts at zero
           }
           break;  
        #endif
             

        case AMP_TM_STATE_WAIT_RESPONSE:
        {
            // We will Hang in this state a few microseconds out of every MS
            // Waiting for response packet, or until AMP_RESPONSE_TIMEOUT_MS

            #ifdef DBUG_OUTPUT_PULSE
                amp_send_debug_pulse(1);
            #endif

            if(AMP_RESPONSE_TIMEOUT_MS_15 > CCI_Phy_Get_MS_Timer())
            {
                // Monitor the Packet received Flag here to reduce INT Time
                if(CCI_Flags.FLAG_CCI_PACKET_RECEIVED)
                {
                    #ifdef DBUG_OUTPUT_PULSE
                        amp_send_debug_pulse(2);
                    #endif

                    // Go Get the Packet
                    CCI_Get_Rx_Packet(&AMP_RxPacket);

                    #ifdef DBUG_OUTPUT_PULSE
                        amp_send_debug_pulse(3);
                    #endif

                    // Packet received.  Check to see if it's valid.
                    if(1 == APP_Process_Response(&AMP_TxPacket, &AMP_RxPacket ))
                    {
                        #ifdef DBUG_OUTPUT_PULSE
                            amp_send_debug_pulse(6);
                        #endif

                        // Problem with packet.
                        if(AMP_NUM_RETRIES <= ++retry_count)
                        {
                            // Too many retries, abort.
                            amp_status = FLAG_AMP_STATUS_RESPONSE_TIMEOUT;
                            amp_tm_state = AMP_TM_STATE_IDLE;

                            retry_count = 0;
                            pAMP_Callback(amp_status);
                            
                            #ifdef CONFIGURE_RETRY_TEST
                                if( AMP_STATUS == AMP_TxPacket.tag)
                                {
                                    // Test only, output current RF ID
                                    SER_Send_String("AS TO");
                                    SER_Send_Prompt();
                                }
                            #endif
                        }
                        else
                        {
                            // Try again.
                            CCI_Phy_Send(AMP_TxPacket);

                            // Re-Init the timeout timer.
                            CCI_Phy_RST_MS_Timer();    // Timer starts at zero
                            
                            #ifdef CONFIGURE_RETRY_TEST
                                if( AMP_STATUS == AMP_TxPacket.tag)
                                {
                                    // Test only, output current RF ID
                                    SER_Send_String("RT");
                                    SER_Send_Int('A',retry_count,10);
                                    SER_Send_Prompt();
                                }
                            #endif
                            
                        }
                    }
                    else
                    {
                        #ifdef DBUG_OUTPUT_PULSE
                            amp_send_debug_pulse(5);
                        #endif

                        #ifdef CONFIGURE_RETRY_TEST
                            if( AMP_STATUS == AMP_TxPacket.tag)
                            {
                                // Test only, output current RF ID
                                SER_Send_String("ALM ST OK");
                                SER_Send_Prompt();
                            }
                        #endif

                        //    
                        // TODO: BC_2021 should we clear  FLAG_AMP_TIMEOUT
                        //  in case this might acknowledged on a retry?
                        // i.e FLAG_AMP_TIMEOUT = 0;
                        //
                        FLAG_AMP_TIMEOUT = 0;
                        
                        // Valid response was received.
                        amp_tm_state = AMP_TM_STATE_IDLE;
                        amp_status = FLAG_AMP_STATUS_RESPONSE_RECEIVED;
                        pAMP_Callback(amp_status);
                    }

                }   // if(CCI_Flags.FLAG_CCI_PACKET_RECEIVED)

            }   // if(AMP_RESPONSE_TIMEOUT_MS_15 > CCI_Phy_Get_MS_Timer())
            else
            {
                if(AMP_TxPacket.tag == AMP_STATUS)
                {
                    // Increased for Amp Status since I saw Some CCI  
                    // messages not being acknowledged after 3 retries. 
                    // RF sometimes does not respond with ACK fast enough???
                    num_retries = 7;
                }
                else
                {
                    // CCI protocol should be able to respond within 3 retries
                    num_retries = AMP_CCI_MAX_3_RETRIES;
                }
                
                // Timeout has occurred.  Increment retry count and check for
                // transaction failure.
                if( num_retries <= ++retry_count)
                {
                    // Too many retries, abort.
                    amp_status = FLAG_AMP_STATUS_RESPONSE_TIMEOUT;
                    amp_tm_state = AMP_TM_STATE_IDLE;
                    retry_count = 0;
                    pAMP_Callback(amp_status);

                    #ifdef CONFIGURE_RETRY_TEST
                        if( AMP_STATUS == AMP_TxPacket.tag)
                        {
                            // Test only, output current RF ID
                            SER_Send_String("AS TO");
                            SER_Send_Prompt();
                        }
                    #endif
                }
                else
                {
                    // Try again.
                    CCI_Phy_Send(AMP_TxPacket);
                    
                    FLAG_AMP_TIMEOUT = 1 ;
                    
                    // Initialize the timeout timer.
                    CCI_Phy_RST_MS_Timer();     // Timer starts at zero
                    
                    #ifdef CONFIGURE_RETRY_TEST
                        if( AMP_STATUS == AMP_TxPacket.tag)
                        {
                            // Test only, output current RF ID
                            SER_Send_String("RT");
                            SER_Send_Int('B',retry_count,10);
                            SER_Send_Prompt();
                        }
                    #endif
                    
                }
            }
        }

        break;
    }
    
    return 0;
}


#ifdef CONFIGURE_UNDEFINED
/*
+------------------------------------------------------------------------------
| Function:      AMP_Get_Packet
+------------------------------------------------------------------------------
| Purpose:       Returns 0 if Response has been received,
|                or non-zero if no Response available
|
|
+------------------------------------------------------------------------------
| Parameters:    pointer to copy packet into
+------------------------------------------------------------------------------
| Return Value:  AMP_TM_SUCCESS  if packet received  = 0
|                AMP_TM_TIMEOUT  if packet response timed out
|                FLAG_AMP_STATUS_TX_ERROR if error
|                AMP_TM_NONE     if none of the above
+------------------------------------------------------------------------------
*/


uchar AMP_Get_Packet(union tag_CCI_Packet *packet)
{
    // Init the packet first.
    packet->word = 0;
    if(amp_status & FLAG_AMP_STATUS_RESPONSE_RECEIVED)
    {
        packet->word = AMP_RxPacket.word;
        return  AMP_TM_SUCCESS;
    }
    else if(amp_status & FLAG_AMP_STATUS_RESPONSE_TIMEOUT)
    {
        return  AMP_TM_TIMEOUT;
    }
    else if(amp_status & FLAG_AMP_STATUS_TX_ERROR)
    {
        return FLAG_AMP_STATUS_TX_ERROR;
    }
    else
    {
        return AMP_TM_NONE;
    }
}
#endif


#ifdef CONFIGURE_UNDEFINED
/*
+------------------------------------------------------------------------------
| Function:      AMP_Start_Enroll
+------------------------------------------------------------------------------
| Purpose:       Begins Start Enrollment packet transaction
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void AMP_Start_Enroll(void)
{
    union tag_CCI_Packet pkt;
    pkt.data = 0x55;
    pkt.tag = AMP_COMM_LEARN;

    AMP_Start_Transaction(pkt);
}

/*
+------------------------------------------------------------------------------
| Function:      AMP_Get_Comm_Status
+------------------------------------------------------------------------------
| Purpose:       Begins request Comm Status packet transaction
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void AMP_Get_Comm_Status(void)
{
    union tag_CCI_Packet pkt;
    pkt.data = 0x55;
    pkt.tag = AMP_COMM_STATUS_REQ;

    AMP_Start_Transaction(pkt);
}

/*
+------------------------------------------------------------------------------
| Function:      AMP_Set_Comm_Status
+------------------------------------------------------------------------------
| Purpose:       Begins Set Comm Status packet transaction
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void AMP_Set_Comm_Status(uchar status_byte)
{
    union tag_CCI_Packet pkt;
    pkt.data = status_byte;
    pkt.tag = AMP_COMM_STATUS;

    AMP_Start_Transaction(pkt);
}

#endif  // CONFIGURE_UNDEFINED


#else   

#endif    // #ifdef CONFIGURE_WIRELESS_MODULE



 
 