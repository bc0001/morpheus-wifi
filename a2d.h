/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        a2d.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for a2d.c module
|               Routines for analog to digital support
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

#ifndef A2D_H
#define A2D_H

/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/
 
    // Define PIC16LF18877 Analog Channels Used
    #define A2D_CO_CHAN                 0x00            //RA0,19
    #define A2D_BATTERY_CHAN            0x01            //RA1,20
    #define A2D_AC_DETECT_CHAN          0x02            //RA2,21
    #define A2D_PHOTO_OUT_CHAN          0x03            //RA3,22
    #define A2D_TEMP_CHAN               0x05            //RA5,24

    #define A2D_VBST_FB_CHAN            0x20            //RE0,25
    #define A2D_IRED_SENSE_CHAN         0x21            //RE1,26
    #define A2D_LIGHT_CHAN              0x0C            //RB4,14
    #define A2D_INT_IN                  0x08            //RB0,08
    #define A2D_FVR_CHAN                0x3F            // Internal





/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/

    unsigned int A2D_Convert_10Bit(uchar channel);
    unsigned char A2D_Convert_8Bit(uchar channel);
    void A2D_On(void);
    void A2D_Off(void);

    #ifdef CONFIGURE_TEMPERATURE
        unsigned int A2D_Temp(void);

        #ifdef CONFIGURE_TEMP_DEG_F
            void A2D_Set_Temp_Degress_F(uint t);
            unsigned char A2D_Get_Degrees_F(void);
        #endif
    #endif

    #ifdef	CONFIGURE_ACDC
        void A2D_Test_AC(void);
        
        #ifdef CONFIGURE_WIFI
            uchar A2D_Test_AC_Sample(void);
        #endif
        
    #endif


#endif  // A2D_H
