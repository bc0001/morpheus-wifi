/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        battery.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Functions for Battery Test under battery Test Load
|               
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include    "common.h"
#include    "battery.h"
#include    "a2d.h"
#include    "sound.h"
#include    "diaghist.h"
#include    "systemdata.h"
#include    "life.h"

#ifdef CONFIGURE_VOICE
    #include "voice.h"
#endif

#ifdef CONFIGURE_WIRELESS_MODULE
    #include    "app.h"
#endif


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

#define	BAT_STATE_INIT		0
#define	BAT_STATE_CAL		1
#define	BAT_STATE_IDLE		2
#define BAT_STATE_TEST_START    3
#define BAT_STATE_TEST_END      4


#define	BAT_SAMPLE_INTERVAL_1_MIN       61000 	// one minute + 1 sec
#define BAT_SAMPLE_INTERVAL_20_SEC      20000   // 20 seconds
#define BAT_SAMPLE_INTERVAL_10_SEC      10000   // 20 seconds
#define BAT_INIT_TIME_1_SEC             1000    // milliseconds
#define BAT_INIT_TIME_5_SEC             5000    // milliseconds
#define BAT_INIT_TIME_12_SEC            12000   // milliseconds
#define BAT_INIT_TIME_10_SEC            10000   // milliseconds
#define BAT_INIT_TIME_30_SEC            30000   // milliseconds
#define	BAT_DELAY_TIME_4_SEC            4000 	// milliseconds
#define BAT_DELAY_TIME_1_SEC            1000    // milliseconds
#define BAT_CAL_TIME_1_SEC              1000    // 1 second
#define BAT_DELAY_TIME_100_MS           100     // milliseconds
#define BAT_DELAY_TIME_50_MS             50     // milliseconds
#define BAT_DELAY_TIME_10_MS             10     // milliseconds

#define TIME_5_SEC                       50      // in 100ms units

#define BAT_CAL_COUNT_10_SEC             10     

#define	BAT_RETRY_INTERVAL_30_SEC       30000 	// in ms
#define BAT_RETRY_COUNT                 3


#define BAT_LB_HYSTERISIS_10_CNTS       (uchar)10

// Abnormally low Threshold to detect open Battery Connection
#define BAT_LB_ABNORMAL_TH              (uchar)50

// ~50 mv is about 2 A/D counts (at 2.0 volt vref)
#define BAT_AD_50_MV                    (uchar)2

// For now use a larger value to detect an open battery connection
// in AC battery backed up Models (Detect ~500mv below LB)
#define BAT_AD_500_MV                   (uchar)20

/*
 * Define manufacturing Low Battery Calibration Limits
 * (Approx. +/- 75 mv from default LB threshold, based upon 2.550 VBAT)
 * NOTE: 3 A/D counts is about 75mv at the battery for this model
 */
#define	BAT_CAL_MIN	(uchar)(BAT_LOW_BAT_THRESHOLD_DEFAULT - 3)
#define	BAT_CAL_MAX	(uchar)(BAT_LOW_BAT_THRESHOLD_DEFAULT + 3)

// Battery Test I/O
#define BAT_TEST_ON     LAT_BAT_TEST_PIN = 1;
#define BAT_TEST_OFF    LAT_BAT_TEST_PIN = 0;




// A/D Reference for PIC = VDD
// FVR to calculate VDD = 1024 mv
#define FVR_MVOLTS			(uint)1024
// FVR_MVOLTS * 1024

// Default to a healthy battery after reset until it is measured
#define BAT_DEFAULT_HEALTHY             122

#define BAT_TST_SHORT_60_SEC            1	// 1 Minute Test Interval
#define BAT_TST_LONG_12_HOUR            720	// 12 Hour interval
#define BAT_TST_ALG_1_HOUR              60  // 1 Hour interval
#define BAT_TEST_RETRY                  1   // in 1 Minute 
#define BAT_TEST_TIMER_IMMED            1      

#define BAT_TST_STARTUP_TIME_12_MIN     (uchar)12      // 12 minutes
#define BAT_TST_STARTUP_TIME_10_MIN     (uchar)10      // 10 minutes

#define BAT_TEST_1MS_PULSE_AC           1       // ms
#define BAT_TEST_50MS_PULSE_DC          50      // ms


#ifdef	CONFIGURE_ACCEL_TIMERS
	#define	TIME_LB_HUSH_MINUTES	3	// 3 minutes
#else
        // This was increased to 24 Hours
	#define	TIME_LB_HUSH_MINUTES	1440	// 24 Hour
#endif

#ifdef  CONFIGURE_WIRELESS_BAT_STATUS

    // Define A/D Count Battery Status Thresholds
    #define BAT_STATUS_LOW      2   //~50mv above LB
    #define BAT_STATUS_HIGH     5   //~130mv above LB

    #define BAT_STATUS_RED      0x81
    #define BAT_STATUS_YELLOW   0x82
    #define BAT_STATUS_GREEN    0x83

#endif

// Used for Initialization before Bat Test is 1st measured (in A/D units))
#define BAT_HEALTHY_DEFAULT     (uchar)120


// Enable this to output Battery Test Interval Timing to Serial Port
//#define CONFIGURE_BAT_SWITCH_TEST


/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
static unsigned char bat_battery_state = BAT_STATE_INIT;
static unsigned int  bat_test_timer = BAT_TST_SHORT_60_SEC;
static unsigned char bat_startup_timer;

static unsigned char bat_cal_count;

// Initialize to healthy battery voltage until 1st measurement is taken
persistent volatile unsigned char bat_last_volt;


#ifdef CONFIGURE_WIRELESS_MODULE
  #ifdef CONFIGURE_WIRELESS_BAT_TXRX_SYNC
    static unsigned char bat_transceiver_timer = 0;
    static unsigned char bat_retry_count;
  #endif
#endif

#ifdef CONFIGURE_LB_HUSH
    static unsigned int	bat_hush_timer;
#endif

/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/
uchar bat_do_test(void);
void bat_init_startup_timer(void);
void bat_process_startup_timer(void);
void bat_set_test_interval(void);


/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/

#ifdef  CONFIGURE_WIRELESS_BAT_STATUS


/*
+------------------------------------------------------------------------------
| Function:      BAT_Get_Status
+------------------------------------------------------------------------------
| Purpose:       Return Battery Status per Wireless CCI request
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  BAT_STATUS_RED (Battery nearing or at Low battery)
|                BAT_STATUS_YELLOW
|                BAT_STATUS_GREEN  (Battery Healthy)
|
+------------------------------------------------------------------------------
*/

uchar BAT_Get_Status (void)
{
    uchar status;

    if((bat_last_volt <= SYS_RamData.LB_Cal_Status))
    {
        status = BAT_STATUS_RED;
    }
    else
    {
        // Compare Battery Voltage measured against the Calibrated LB Threshold
        uchar delta = (bat_last_volt - SYS_RamData.LB_Cal_Status);

        if(BAT_STATUS_LOW > delta)
        {
            status = BAT_STATUS_RED;
        }
        else if(BAT_STATUS_HIGH > delta)
        {
            status = BAT_STATUS_YELLOW;
        }
        else
        {
            status = BAT_STATUS_GREEN;
        }
        
    }

    return  status;
    
}
#endif

/*
+------------------------------------------------------------------------------
| Function:      BAT_Init_Last_Voltage
+------------------------------------------------------------------------------
| Purpose:       Init Last Battery Voltage value
|                Called to initialize to a healthy value before battery 
|                 test is run.
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void BAT_Init_Last_Voltage(void)
{
    bat_last_volt = BAT_HEALTHY_DEFAULT;
}

/*
+------------------------------------------------------------------------------
| Function:      BAT_Get_Last_Voltage
+------------------------------------------------------------------------------
| Purpose:       Return Last measured Battery Voltage
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Battery Voltage in A/D counts (8 bit)
|
+------------------------------------------------------------------------------
*/

uchar BAT_Get_Last_Voltage(void)
{
    return bat_last_volt;
}

/*
+------------------------------------------------------------------------------
| Function:      BAT_Test_Task
+------------------------------------------------------------------------------
| Purpose:       Battery Test Monitor State machine
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Battery Test Next Execution Time (in Task TICs)
|
+------------------------------------------------------------------------------
*/

unsigned int BAT_Test_Task(void)
{
    switch (bat_battery_state)
    {

        case BAT_STATE_INIT:
        {
            // Init Battery Test I/O
            BAT_TEST_OFF
            #ifdef CONFIGURE_WIRELESS_BAT_TXRX_SYNC
                bat_retry_count = BAT_RETRY_COUNT;
            #endif

            if(FLAG_LOW_BATTERY_CAL_MODE)
            {
                // Proceed to LB Cal state
                bat_battery_state = BAT_STATE_CAL;
                
                // Init bat Cal chirp counter
                bat_cal_count =  BAT_CAL_COUNT_10_SEC;
                
                FLAG_BATTERY_NOT_IDLE = 1;

                if(FLAG_LB_CAL_SIGNAL)
                {
                    // Clear Battery Cal Status if signaled by the test point.
                    FLAG_LB_CAL_STATUS = 0;
                }
                return MAIN_Make_Return_Value(BAT_CAL_TIME_1_SEC);
            }

            #ifdef CONFIGURE_CO
                if( !FLAG_CALIBRATION_COMPLETE || FLAG_CALIBRATION_UNTESTED)
                {
                    // CO Cal must be completed before proceeding
                    return  MAIN_Make_Return_Value(BAT_SAMPLE_INTERVAL_1_MIN);
                }
            #else
                if(FLAG_NO_SMOKE_CAL)
                {
                    // Smoke Cal must be completed before proceeding
                    return  MAIN_Make_Return_Value(BAT_SAMPLE_INTERVAL_1_MIN);
                }
            #endif

            #ifdef CONFIGURE_WIRELESS_MODULE
                if(!FLAG_WIRELESS_DISABLED)
                {
                    // No Battery Test if in Enrollment Modes 
                    if( FLAG_APP_JOIN_MODE || FLAG_APP_SEARCH_MODE )
                    {
                         return  MAIN_Make_Return_Value
                                 (BAT_SAMPLE_INTERVAL_1_MIN);
                    }
                }
            #endif

            //  start with 1 minute Battery Test
            bat_test_timer = BAT_TST_SHORT_60_SEC;
            
            // Begin 10-12 Minute Startup Timer
            bat_init_startup_timer();

            bat_battery_state = BAT_STATE_IDLE;
            
            // Allow Low Power Entry at this time
            FLAG_BATTERY_NOT_IDLE = 0; 

            #ifdef CONFIGURE_WIRELESS_MODULE
                if(FLAG_RST_SOFT)
                {
                    return MAIN_Make_Return_Value(BAT_INIT_TIME_5_SEC);
                }
                else
                {
                    return MAIN_Make_Return_Value(BAT_INIT_TIME_30_SEC);
                }
            #else
                if(FLAG_RST_SOFT)
                {
                    return MAIN_Make_Return_Value(BAT_INIT_TIME_5_SEC);
                }
                else
                {
                    return MAIN_Make_Return_Value(BAT_INIT_TIME_10_SEC);
                }
            #endif

        }

        case BAT_STATE_CAL:
            /*
             * Low battery Threshold is calibrated with a 2.550
             * Precision VBAT Power Source
             * To Cal LB Thrteshold, measure the Battery Test Voltage and
             * record in Non-Volatile memory
             * If out of limits, then do not complete LB Cal.
             */

            if(!FLAG_LB_CAL_STATUS && FLAG_LB_CAL_SIGNAL)
            {
                // Sample the battery Test Voltage
                // Test Battery under Load
                uchar batvolt = bat_do_test();
                
                SER_Send_Int('B',batvolt,10);
                SER_Send_Prompt();

                // Test result against min/max limits
                if( (batvolt < BAT_CAL_MIN) || (batvolt > BAT_CAL_MAX) )
                {
                    // Exit without completing LB Cal
                    // Red LED will remain ON (FAIL) (controlled in LED Module)
                    
                    // Add LB Cal chirping while in Low battery Cal Mode
                    // This provides supervision if somehow LB Cal mode is 
                    // unintentionally entered (memory corruption or I/O pin
                    // floating)
                }
                else
                {
                    // Save LB Threshold in Non-Volatile Memory
                    SYS_RamData.LB_Cal_Status = batvolt;
                    SYS_Save();

                    // LB Cal sucessfully completed (RED LED will turn OFF)
                    FLAG_LB_CAL_STATUS = 1;
                    SER_Send_String("LB Cal Complete ");
                    SER_Send_Prompt();

                }
            }
            
            // Add LB Cal chirping while in Low Battery Cal Mode
            // This provides supervision if somehow LB Cal mode is 
            // unintentionally entered (memory corruption or I/O pin
            // floating)
            if(0 == --bat_cal_count)
            {
                SND_Do_Chirp();
                bat_cal_count = BAT_CAL_COUNT_10_SEC;
            }


            // Remain in this state (requires a power cycle after CAL)
            return MAIN_Make_Return_Value(BAT_CAL_TIME_1_SEC);

        case BAT_STATE_IDLE:

            FLAG_BATTERY_NOT_IDLE = 0;  // LP Mode allowed if Idle

            /*
             * First check to see if any conditions exist that would delay
             * the battery test.
             * 1. If in alarm, no reason to do a battery test since alarm
             *     overrides low battery.  Also Horn, Voice and Leds
             *     would overrun Battery Test Current Pulse
             * 
             *    If EOL or Fatal Fault is Active no reason to do a battery 
             *     test since fault modes override low battery indicators.
             * 
             * 2. Delay if Voice is active
             *
             * 3. For Wireless AC Models in Low Power Mode (no AC)
             *     the RF Module receiver/transmitter should be OFF
             */

            if( FLAG_CO_ALARM_ACTIVE || FLAG_SMOKE_ALARM_ACTIVE )
            {
                 return	MAIN_Make_Return_Value(BAT_DELAY_TIME_4_SEC);
            }
            
            // Horn Active?
            if(PORT_HORN_DISABLE_PIN == 0)
            {
                return	MAIN_Make_Return_Value(BAT_DELAY_TIME_4_SEC);
            }
            
            if( FLAG_EOL_MODE || FLAG_FAULT_FATAL || 
                FLAG_EOL_FATAL || FLAG_EOL_HUSH_ACTIVE )
            {
                // Insure Low power Mode is allowed while in Fault
                FLAG_LP_BATTERY_TEST = 0;
                return MAIN_Make_Return_Value(BAT_DELAY_TIME_4_SEC);
            }
            
            #ifdef CONFIGURE_WIRELESS_MODULE
                #ifdef CONFIGURE_UNDEFINED
                    // Remove this condition in case unit never exits 
                    //  Search or Join if RF module quits communicating

                    // No Battery Test if in Enrollment Modes
                    if( FLAG_APP_JOIN_MODE || FLAG_APP_SEARCH_MODE )
                    {
                         return	MAIN_Make_Return_Value(BAT_DELAY_TIME_4_SEC);
                    }
                #endif

                #ifdef CONFIGURE_WIFI
                    if(!FLAG_WIRELESS_DISABLED)
                    {
                        // No Battery Test if in Enrollment Modes 
                        if( FLAG_APP_JOIN_MODE || FLAG_APP_SEARCH_MODE )
                        {
                             return  MAIN_Make_Return_Value
                                     (BAT_SAMPLE_INTERVAL_1_MIN);
                        }
                    }
                #endif
            
            #endif
            
            #ifdef CONFIGURE_VOICE
                if( FLAG_VOICE_ACTIVE )
                {
                   return  MAIN_Make_Return_Value(BAT_DELAY_TIME_4_SEC);
                }
            #endif

            else
            {
                #ifdef CONFIGURE_BAT_SWITCH_TEST  

                  #ifdef CONFIGURE_WIRELESS_BAT_TXRX_SYNC    
                    if(0== bat_transceiver_timer)
                    {
                  #endif
                        if(FLAG_LP_BATTERY_TEST && FLAG_BAT_TST_SHORT)
                        {
                            // LP Mode do a blink to test timing
                            FLAG_RED_LED_BLINK = 1;
                        }


                        if(FLAG_BAT_TST_SHORT)
                        {
                            if(FLAG_BAT_ABNORMALLY_LOW)
                            {
                                SER_Send_String("BAT Switch OPEN");
                                SER_Send_Prompt();
                                SER_Send_String("20 Sec Battery Interval");
                            }
                            else
                            {
                                SER_Send_String("60 Sec Battery Interval");
                            }
                        }
                        else
                        {
                            SER_Send_String("12 Hour Battery Interval");
                        }
                        SER_Send_Prompt();  
                        
                  #ifdef CONFIGURE_WIRELESS_BAT_TXRX_SYNC        
                    }
                  #endif

                #endif

                // This Timer is based upon 1 Minute task time
                if(0 == --bat_test_timer)
                {
                    
                    #ifdef CONFIGURE_WIRELESS_MODULE
                        // To remain in Wait for TX Off State if needed   
                        bat_test_timer++;
                        
                      #ifdef CONFIGURE_WIRELESS_BAT_TXRX_SYNC
                        if(FLAG_WIRELESS_DISABLED)
                        {
                            // Always perform Battery Test when Wireless 
                            //  is disabled
                            
                            // Default to 60 second Tests    
                            bat_test_timer = BAT_TST_SHORT_60_SEC;
                            
                            // No Sync Required for DC Wireless Battery Test
                            bat_battery_state = BAT_STATE_TEST_START;

                            // Prevent LP Mode while Pulse is Active
                            FLAG_BATTERY_NOT_IDLE = 1;
                            
                            if(FLAG_LP_BATTERY_TEST)
                            {
                                //If This test is from LP Mode, clear Flag
                                FLAG_LP_BATTERY_TEST = 0;
                                
                                // and Shorten the Active Mode Timer to
                                //  return to LP Mode soon after battery Test
                                MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_50_MS;
                            }

                            // We should be in active mode at this time
                            return MAIN_Make_Return_Value
                                    (BAT_DELAY_TIME_10_MS);
                            
                        }
                        else if(!FLAG_REQUEST_TRANSCEIVER_STATUS)
                        {

                            // Set the Request TXRX Notification Flag
                            //  The App module will send status1 changes
                            FLAG_STATUS_1_TXRX_DISABLE_REQ = 1;
                            
                            // Init TX Disabled Reply Flag
                            FLAG_STATUS_1_TXRX_DISABLED = 0;

                            // Init TXRX Battery Test Timeout Timer
                            // We expect a notification before this time
                            bat_transceiver_timer = TIME_5_SEC;
                            
                            // Indicate that we have requested CCI permission
                            FLAG_REQUEST_TRANSCEIVER_STATUS = 1;

                            // We are in active mode at this point so
                            //  faster return times are valid.
                            return MAIN_Make_Return_Value
                                        (BAT_DELAY_TIME_10_MS);

                        }
                        else if(!FLAG_STATUS_1_TXRX_DISABLED)
                        {
                            //  Put a timeout here to limit time
                            //  in this condition, in case RF module does not 
                            //  send Transceiver Disabled message.
                            if(0 == --bat_transceiver_timer)
                            {
                                // After 5 Seconds waiting for 
                                //      FLAG_TRANSCEIVER_DISABLED
                                
                                // Check retry counter to see if
                                //   we need to force a Battery Test
                                if(0 == --bat_retry_count)
                                {
                                    // Force a Battery Test now if Timeout
                                    FLAG_STATUS_1_TXRX_DISABLED = 1;
                                    
                                    // For next Test reset retries
                                    bat_retry_count = BAT_RETRY_COUNT;
                                    
                                    return MAIN_Make_Return_Value
                                                (BAT_DELAY_TIME_10_MS);
                                }
                                else
                                {
                                    // skip this battery test period
                                    //  and retry in 60 seconds
                                    FLAG_STATUS_1_TXRX_DISABLE_REQ = 0;
                                    FLAG_STATUS_1_TXRX_DISABLED = 0;
                                    FLAG_REQUEST_TRANSCEIVER_STATUS = 0;
                                    
                                    // Reset LP Battery Test Indicator
                                    //  so Alarm can return to Sleep Mode
                                    FLAG_LP_BATTERY_TEST = 0;
                                    
                                    // Update Battery Startup Timer if needed
                                    bat_set_test_interval();
                                    
                                    // Then force to 60 second retry time
                                    bat_test_timer = BAT_TST_SHORT_60_SEC;
                                }
                                
                            }
                            else
                            {
                                return MAIN_Make_Return_Value
                                            (BAT_DELAY_TIME_100_MS);
                            }
                        }
                        else
                        {
                            // Notification Received, Proceed to Battery Test
                            FLAG_STATUS_1_TXRX_DISABLE_REQ = 0;
                            FLAG_STATUS_1_TXRX_DISABLED = 0;
                            FLAG_REQUEST_TRANSCEIVER_STATUS = 0;
                            
                            bat_transceiver_timer = 0;
                            
                            bat_battery_state = BAT_STATE_TEST_START;

                            // Prevent LP Mode while Pulse is Active
                            FLAG_BATTERY_NOT_IDLE = 1;
                            
                            if(FLAG_LP_BATTERY_TEST)
                            {
                                //If This test is from LP Mode, clear Flag
                                FLAG_LP_BATTERY_TEST = 0;
                                
                                // and Shorten the Active Mode Timer to
                                //  return to LP Mode soon after battery Test
                                MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_50_MS;
                            }

                            return MAIN_Make_Return_Value
                                    (BAT_DELAY_TIME_10_MS);
                        }

                      #else
                            // Default to 60 second Tests    
                            bat_test_timer = BAT_TST_SHORT_60_SEC;
                            
                            // No Sync Required for DC Wireless Battery Test
                            bat_battery_state = BAT_STATE_TEST_START;

                            // Prevent LP Mode while Pulse is Active
                            FLAG_BATTERY_NOT_IDLE = 1;
                            
                            if(FLAG_LP_BATTERY_TEST)
                            {
                                //If This test is from LP Mode, clear Flag
                                FLAG_LP_BATTERY_TEST = 0;
                                
                                // and Shorten the Active Mode Timer to
                                //  return to LP Mode soon after battery Test
                                MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_50_MS;
                            }

                            // We should be in active mode at this time
                            return MAIN_Make_Return_Value
                                    (BAT_DELAY_TIME_10_MS);
                      #endif

                    #else

                        // Default to 60 second Tests   
                        bat_test_timer = BAT_TST_SHORT_60_SEC;
                        
                        // Non Wireless Battery Test
                        bat_battery_state = BAT_STATE_TEST_START;

                        // Prevent LP Mode while Pulse is Active
                        FLAG_BATTERY_NOT_IDLE = 1;
                        
                        if(FLAG_LP_BATTERY_TEST)
                        {
                            //If This test is from LP Mode, clear Flag
                            FLAG_LP_BATTERY_TEST = 0;

                            // and Shorten the Active Mode Timer to
                            //  return to LP Mode soon after battery Test
                            MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_50_MS;
                        }

                        return MAIN_Make_Return_Value
                                (BAT_DELAY_TIME_100_MS);
                    #endif
                }
            }
        break;


        case BAT_STATE_TEST_START:

            /*
             * This delays all measurements until a second after Battery Test.
             * This is here only because Battery Test pulse was
             *  affecting CO Measurement on this design.
             */
            FLAG_BAT_TEST_DLY  = 1;

            /*
             * To make the battery test, raise the battery test pin
             * and proceed to BAT_STATE_TEST_END for A/D conversion.
             */
            BAT_TEST_ON

            bat_battery_state = BAT_STATE_TEST_END;

            return MAIN_Make_Return_Value(BAT_DELAY_TIME_50_MS);


        case BAT_STATE_TEST_END:
            /*
             * For battery Test measurement, we need to use FVR 
             *  as A/D Refrenece Voltage instead of VDD
             */
            FLAG_AD_USE_2048_FVR = 1;
            uchar batvolt = A2D_Convert_8Bit(A2D_BATTERY_CHAN);

            // Remove Battery Test Load
            BAT_TEST_OFF

            // Fixed Vref Off after testing
            FVRCONbits.FVREN = 0;

            // Save A/D counts for battery Recording
            bat_last_volt = batvolt;

            #ifdef CONFIGURE_LB_HUSH
                // LB Hush inhibit should not be required at 50mv below
                //  LB threshold, because we have added LB Hush Disable
                //  after 7 days of Low Battery.  This should meet the UL 
                //  requirement.
            
                // Update Low battery Hush Inhibit Flag
                // Use it to detect a disconnected or open battery
                // in which case we do not allow LB HUSH
                if(batvolt < (SYS_RamData.LB_Cal_Status -
                               BAT_AD_500_MV) )
                {
                    FLAG_LB_HUSH_INHIBIT = 1;
                }
                else
                {
                    FLAG_LB_HUSH_INHIBIT = 0;
                }
            #endif

            // Test 1st to see if low voltage was abnormally Low 
            // i.e an open activation switch
            if(batvolt < BAT_LB_ABNORMAL_TH)
            {
                FLAG_BAT_ABNORMALLY_LOW = 1;
            }
            else
            {
                if(FLAG_BAT_ABNORMALLY_LOW)
                {
                    // If Switch Closed detected, reset the 10 minute
                    //  startup timer
                    bat_init_startup_timer();
                    
                    #ifdef CONFIGURE_BAT_SWITCH_TEST
                        SER_Send_String("Switch Closed Detected!");
                        SER_Send_Prompt();
                    #endif

                    #ifdef CONFIGURE_VCE_OPEN_SWITCH 
                        // Switch Activated
                        if(FLAG_BAT_ABNORMALLY_LOW)
                        {
                            VCE_Play(VCE_SND_DEV_ENROLLED);
                        }
                    #endif
                        
                }
                
                FLAG_BAT_ABNORMALLY_LOW = 0;
            }

            // The low battery flag is only set if the battery voltage
            //  is below the threshold and low battery pending is set.
            if(batvolt < SYS_RamData.LB_Cal_Status)
            {
                
                if(FLAG_LB_PENDING)
                {
                    // Only Record if entering LB
                    if(!FLAG_LOW_BATTERY)
                    {
                        // Enter Low Battery
                        FLAG_LOW_BATTERY = 1;

                        #ifdef CONFIGURE_DIAG_HIST
                            // Test for the LB event we want to record
                            if(batvolt < BAT_LB_ABNORMAL_TH)
                            {
                                // Possibly an Open Battery Connection
                                //  or Open Switch
                                DIAG_Hist_Que_Push(HIST_ABN_LOW_BATT_EVENT);
                            }
                            else
                            {
                                // Typical Low Battery Condition detected
                                DIAG_Hist_Que_Push(HIST_LOW_BATT_MODE);
                            }
                        #endif

                    }

                }
                else
                {
                    FLAG_LB_PENDING = 1;
                }
            }
            else if(batvolt > (SYS_RamData.LB_Cal_Status +
                                    BAT_LB_HYSTERISIS_10_CNTS))
            {
                FLAG_LB_PENDING = 0;
                FLAG_LOW_BATTERY = 0;
                
                #ifdef CONFIGURE_WIRELESS_MODULE
                    #ifndef CONFIGURE_NO_REMOTE_LB
                        if(!FLAG_STATUS_RMT_LB_ACTIVE)
                        {
                            // Insure LB Hush is Off unless we are in 
                            //  Remote Low Battery
                            FLAG_LB_HUSH_ACTIVE = 0;
                        }
                    #endif
                #else
                        // Insure LB Hush is Off unless we are in 
                        //  Remote Low Battery
                        FLAG_LB_HUSH_ACTIVE = 0;
                #endif
            }

            if(SERIAL_ENABLE_BATTERY)
            {
                SER_Send_Int('B', batvolt, 10);
                if(FLAG_LOW_BATTERY)
                {
                    if(FLAG_LB_HUSH_ACTIVE)
                    {
                        // Add LB Hush Character
                        SER_Send_Char('"');
                    }
                    else
                    {
                        // Add LB Trouble Character
                        SER_Send_Char('!');
                    }

                    if(FLAG_LB_HUSH_INHIBIT || FLAG_LB_HUSH_DISABLED)
                    {
                        // Add another ! if very low battery
                        SER_Send_Char('!');
                    }

                }
                // Send LB Threshold
                SER_Send_Char(',');
                SER_Send_Char('L');
                SER_Send_Int('B', SYS_RamData.LB_Cal_Status, 10);

                SER_Send_Prompt();


            }

            bat_battery_state = BAT_STATE_IDLE;

            // Set the next Battery Test Interval
            bat_set_test_interval();

            // Always Reset LP Battery Test Indicator
            FLAG_LP_BATTERY_TEST = 0;
            
            // Allow Low Power Mode transitions
            FLAG_BATTERY_NOT_IDLE = 0;

        break;

        default:
            bat_battery_state = BAT_STATE_IDLE;
        break;
    }
    
    if(FLAG_BAT_ABNORMALLY_LOW)
    {
        return  MAIN_Make_Return_Value(BAT_SAMPLE_INTERVAL_10_SEC);
    }
    else
    {
        return  MAIN_Make_Return_Value(BAT_SAMPLE_INTERVAL_1_MIN);
    }
}


// ***********************************************************************
#ifdef CONFIGURE_LB_HUSH

/*
+------------------------------------------------------------------------------
| Function:      BAT_Init_LB_Hush
+------------------------------------------------------------------------------
| Purpose:       Initialize Low battery Hush Timer/Mode
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

    void BAT_Init_LB_Hush(void)
    {
        FLAG_LB_HUSH_ACTIVE = 1;
        bat_hush_timer = TIME_LB_HUSH_MINUTES;

        // LB Hush indication to start immediately
        MAIN_Reset_Task(TASKID_RLED_MANAGER);
        
        #ifdef CONFIGURE_TIMER_SERIAL
            SER_Send_String("Start LB Hush 24Hr- ");
            SER_Send_Prompt();
        #endif        
        

    }


/*
+------------------------------------------------------------------------------
| Function:      BAT_Check_LB_Hush_Timer
+------------------------------------------------------------------------------
| Purpose:       Update Low battery Hush Timer if Active
|                Must be called once a minute
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

    void BAT_Check_LB_Hush_Timer(void)
    {
        if(FLAG_LB_HUSH_ACTIVE )
        {
            if(--bat_hush_timer == 0)
            {
                FLAG_LB_HUSH_ACTIVE = 0;
                
                #ifdef CONFIGURE_TIMER_SERIAL
                    SER_Send_String("LB Hush 24 hr Expired- ");
                    SER_Send_Prompt();
                #endif  
            }
        }

    }

#endif

#ifdef  CONFIGURE_LBAT_CAL_ENABLE


/*
+------------------------------------------------------------------------------
| Function:      BAT_LB_Cal_Init
+------------------------------------------------------------------------------
| Purpose:       Test current Low Battery Calibration status
|                FLAG_LB_CAL_STATUS is set if LB Cal is Complete
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

    void BAT_LB_Cal_Init(void)
    {
        if(SYS_RamData.LB_Cal_Status != 0)
        {
            // Signal Low Battery Cal Completed
            FLAG_LB_CAL_STATUS = 1;
        }
    }

#endif


/*
+------------------------------------------------------------------------------
| Function:      bat_set_test_interval
+------------------------------------------------------------------------------
| Purpose:       Reset battery test interval based upon time after reset
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void bat_set_test_interval(void)
{
    
    #ifdef CONFIGURE_DEBUG_60_SEC_BAT_TEST
         // For testing only use 1 minute battery test times
         bat_test_timer = BAT_TST_SHORT_60_SEC;
         return;
    #else

        #ifdef CONFIGURE_ACCEL_LIGHT_TEST
             // For testing only use 1 minute battery test times
             bat_test_timer = BAT_TST_SHORT_60_SEC;
             return;
        #else


        // Reset The Battery Test Timer to proper interval
        if(FLAG_BAT_TST_SHORT)
        {
            bat_test_timer = BAT_TST_SHORT_60_SEC;

            // Update and Test the Bat Test Interval Startup Timer
            bat_process_startup_timer();
            
            if(!FLAG_BAT_TST_SHORT && FLAG_BAT_ABNORMALLY_LOW)
            {
                //If Timer expired and Activation switch still detected Open
                // remain if Short Battery test Interval
                bat_init_startup_timer();
            }
        }
        else
        {
            if(FLAG_BAT_ABNORMALLY_LOW)
            {
                // If the Activation switch becomes Detected Open
                //  transition to the short Battery test sample Rate
                bat_test_timer = BAT_TST_SHORT_60_SEC;

                /*
                 * If Abnormally Low - execute startup timer reset
                 * This is normally caused by an open activation switch
                 * Return to short battery test sampling
                 */
                bat_init_startup_timer();
            }
            else
            {
                // maintain the Longer Battery test Interval (12 hours)
                bat_test_timer = BAT_TST_LONG_12_HOUR;
                
            }

        }

        #endif
    #endif
}

/*
+------------------------------------------------------------------------------
| Function:      BAT_Init_Startup_Timer
+------------------------------------------------------------------------------
| Purpose:       Initialize Battery Test Interval Startup Timer
|                After 10 minutes battery test Interval changes from 
|                Short to Long Test inteval
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/
void bat_init_startup_timer(void)
{
    bat_startup_timer = MAIN_OneMinuteTic;
    FLAG_BAT_TST_SHORT = 1;

}

/*
+------------------------------------------------------------------------------
| Function:      BAT_Process_Startup_Timer
+------------------------------------------------------------------------------
| Purpose:       Update 10 minute Startup Timer
|                and set Bat Test Interval Flag accordingly
|                FLAG_BAT_TST_SHORT is cleared at Timer Expiration   
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void bat_process_startup_timer(void)
{
    // Monitor Timer for expiration
    if( (uchar)(MAIN_OneMinuteTic - bat_startup_timer) >= 
         (uchar)BAT_TST_STARTUP_TIME_10_MIN)
    {
        // Switch to Long Battery Test Interval
        FLAG_BAT_TST_SHORT = 0;
        
    }
}


/*
+------------------------------------------------------------------------------
| Function:      BAT_Check_LP_Mode_Timer
+------------------------------------------------------------------------------
| Purpose:       Update Battery Test Timer and return TRUE if it expires.
|                This routine called from Low Power Mode only
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  TRUE - if expired,  otherwise FALSE
|
+------------------------------------------------------------------------------
*/

uchar BAT_Check_LP_Mode_Timer(void)
{
    // Battery Test Timer is based upon 1 Minute Calls
    if(0 == --bat_test_timer) 
    {
        // This will force an immediate battery test after Waking Up
        bat_test_timer = BAT_TEST_TIMER_IMMED;          
        
        return TRUE;
    }
    else
    {
        // Not time for a Battery Test
        return FALSE;
    }
}



/*
+------------------------------------------------------------------------------
| Function:      bat_do_test
+------------------------------------------------------------------------------
| Purpose:       To make the battery test, raise the battery test pin
|                and do an A/D conversion.
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  measured battery test voltage (in A/D counts)
|
+------------------------------------------------------------------------------
*/

uchar bat_do_test(void)
{
    /*
     * To make the battery test, raise the battery test pin
     * and do an A/D conversion.
     */

    BAT_TEST_ON

    /*
     * Add enough delay to discharge Capacitance on VBat
     */

    PIC_delay_ms(BAT_TEST_50MS_PULSE_DC);

    /*
     * For battery Test measurement, we need to use FVR (1.024)
     *  as A/D Refrenece Voltage instead of VDD
     */

    FLAG_AD_USE_2048_FVR = 1;
    uchar retval = A2D_Convert_8Bit(A2D_BATTERY_CHAN);

    BAT_TEST_OFF

    // Fixed Vref Off after testing
    FVRCONbits.FVREN = 0;

    return retval;
}












