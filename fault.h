/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        fault.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for fault.c module
|               This file contains fault status definitions.
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

#ifndef	FAULT_H_
#define FAULT_H_

/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

    // Fault Codes
    #define FAULT_1                     1   // unused
    #define FAULT_CO_HEALTH             2   // CO sensor test fault
    #define FAULT_CO_COMP               3   // unused
    #define FAULT_SENSOR_SHORT          4   // CO sensor fault
    #define FAULT_GAS_SENSOR            5   // Gas sensor fault
    #define FAULT_CALIBRATION           6   // CO calibration fault
    #define FAULT_PTT                   7
    #define FAULT_MEMORY                8
    #define FAULT_EXPIRATION            9
    #define FAULT_PHOTOCHAMBER          10  // Reserved for Photo Smoke models
    #define FAULT_MEMORY_FLASH          11
    #define FAULT_TEMPERATURE           12
    #define FAULT_INT_SUPERVISION       13  // Interconnect Supervision Fault
    #define FAULT_SMK_DRIFT_COMP        14  // Smoke Drift Comp Fault

    #define FAULT_NO_FAULT              0x80

#ifdef CONFIGURE_WIRELESS_MODULE

    #define AMP_SUCCESS_COORDINATOR       0x80 | AMP_FAULT_COORDINATOR
    #define AMP_SUCCESS_RFD_JOIN          0x80 | AMP_FAULT_RFD_JOIN
    #define AMP_SUCCESS_CCI               0x80 | AMP_FAULT_CCI
    #define AMP_SUCCESS_RFD_CHECK_IN      0x80 | AMP_FAULT_RFD_CHECK_IN
    #define AMP_SUCCESS_COORDINATOR_LOST  0x80 | AMP_FAULT_TIME_SYNC
    #define AMP_SUCCESS_WM_BATTERY_EOL    0x80 | AMP_FAULT_WM_BATTERY_EOL



    #ifdef CONFIGURE_UNDEFINED_HERE
        /*
         * Defined in alarmmp.h
         * Reference Only
         */
        #define AMP_FAULT_COORDINATOR             34      //0x22
        #define AMP_FAULT_RFD_JOIN                35      //0x23
        #define AMP_FAULT_CCI                     36      //0x24
        #define AMP_FAULT_RFD_CHECK_IN            37      //0x25
        #define AMP_FAULT_TIME_SYNC               38      //0x26
        #define AMP_FAULT_WM_BATTERY_EOL          39      //0x27
    #endif

#endif


    #define SUCCESS_FAULT_1             0x80 | FAULT_1
    #define SUCCESS_CONVERSION_SETUP	0x80 | FAULT_CO_HEALTH
    #define SUCCESS_SMK_DRIFT_COMP	0x80 | FAULT_SMK_DRIFT_COMP
    #define SUCCESS_SENSOR_SHORT	0x80 | FAULT_SENSOR_SHORT
    #define SUCCESS_GAS_SENSOR		0x80 | FAULT_GAS_SENSOR
    #define SUCCESS_CALIBRATION		0x80 | FAULT_CALIBRATION
    #define SUCCESS_PTT             0x80 | FAULT_PTT
    #define SUCCESS_MEMORY          0x80 | FAULT_MEMORY
    #define SUCCESS_EXPIRATION		0x80 | FAULT_EXPIRATION
    #define SUCCESS_PHOTOCHAMBER	0x80 | FAULT_PHOTOCHAMBER
    #define SUCCESS_MEMORY_DIAG		0x80 | FAULT_MEMORY_FLASH
    #define SUCCESS_TEMPERATURE		0x80 | FAULT_TEMPERATURE
    #define SUCCESS_INT_SUPERVISION	0x80 | FAULT_INT_SUPERVISION




/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/

    // Fault Manager Return Values
    enum
    {
        FAULT_NO_ACTION_TAKEN = 0,  // No Current Fault 
        FAULT_SUCCESS_ACTION,       // Success logged
        FAULT_PENDING_ACTION,       // Fault logged but not in Fatal Fault 
        FAULT_FATAL_ACTION,         // Fault logged and Fatal Fault Set
        FAULT_RECOVERY_ACTION,      // Fatal Fault was cleared
        FAULT_NETWORK_ACTION,       // Network Fault Logged in Network Error
        FAULT_UNRECOGNIZED          // Invalid Code
    };

/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/
    // Global variables
    extern uchar FLT_Fatal_Code;

    #ifdef CONFIGURE_WIRELESS_MODULE
        extern uchar FLT_NetworkFaultCode;
    #endif

/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/

    uchar FLT_Fault_Manager(uchar faultcode);
    uchar FLT_Status(void);
    
    #ifdef CONFIGURE_WIRELESS_MODULE
        void FLT_Init_Error_Hush(void);
        void FLT_Check_Error_Hush(void);
        uchar FLT_Net_Error_Status(void);
    #endif


        
    
	
#endif  // FAULT_H_

