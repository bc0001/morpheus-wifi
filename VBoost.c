/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        vboost.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Routines to Support Voltage Boost used in Interconnect and 
|               some horn circuits
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/



/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/

#include	"common.h"
#include	"vboost.h"
#include	"a2d.h"



/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

// We want a minimum VINT voltage of about 7 volts
// With 3.3 M over 330K ohm
//#define VBOOST_LOW_LIMIT  (uint)260

// With 3.0 M over 330K ohm
#define VBOOST_LOW_LIMIT    (uint)284



/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/


/*
+------------------------------------------------------------------------------
| Function:      VBOOST_On
+------------------------------------------------------------------------------
| Purpose:       Turn on the VBoost Chip
|                Vboost is enabled when in Active Mode, and can be turned 
|                off in Low Power
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/


void VBOOST_On(void)
{
    // VBoost circuit is used only in HW Interconnected Models
    #ifdef CONFIGURE_INTERCONNECT
        VBOOST_ON
    #else
        VBOOST_OFF
    #endif
}


/*
+------------------------------------------------------------------------------
| Function:      VBOOST_Off
+------------------------------------------------------------------------------
| Purpose:       Turn off the VBoost Chip
|                
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void VBOOST_Off(void)
{
    VBOOST_OFF
}


/*
+------------------------------------------------------------------------------
| Function:      VBOOST_Test
+------------------------------------------------------------------------------
| Purpose:       Test VBoost feedback voltage to see if it is 
|                above or below VBOOST low limit (about 7 volts)
|                Return FALSE if below LIMIT
|
|          Note: The existing Interconnect Circuit has Current Limit            
|                protection so an Interconnect Line Short to Ground does not
|                pull down VBoost Voltage below 7 volts. Alarm will operate as
|                Stand Alone Alarm if no Interconnect Short supervision.
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  TRUE or FALSE (FALSE if below LIMIT)
+------------------------------------------------------------------------------
*/

uchar VBOOST_Test(void)
{
    // read the VINT feedback voltage
    uint vbst = A2D_Convert_10Bit(A2D_VBST_FB_CHAN);


    if(VBOOST_LOW_LIMIT > vbst)
    {
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}
