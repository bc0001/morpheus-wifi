
#include "systemdata.h"
#include "diaghist.h"
#include "eeprom.h"

eeprom struct EE_Memory EE_Data =
{
    struct SYS_FlashData_Primary,
    struct SYS_FlashData_Backup,
    struct DiagHist

};