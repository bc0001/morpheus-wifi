/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        sound.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Contains functionality for sounder timing
|               Alarm sound control and Trouble sound control actions
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/



/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include "common.h"
#include "sound.h"
#include "led.h"
#include "alarmmp.h"
#include "fault.h"
#include "cocalibrate.h"

#ifdef CONFIGURE_VOICE
    #include "voice.h"
#endif

#ifdef CONFIGURE_WIRELESS_MODULE
    #include "app.h"
#endif


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

// Sound Module Debug Configs
//#define CONFIGURE_DEBUG_TM_STATES

#define SND_DEFAULT_INTERVAL	(uint)100   // 100 ms
#define SND_TASK_TIME_100MS     (uint)100	// 100 ms
#define SND_TASK_TIME_500MS     (uint)500	// 500 ms
#define SND_TASK_TIME_1_SEC 	(uint)1000	// 1 sec
#define SND_TASK_TIME_2_SEC 	(uint)2000	// 2 sec
#define SND_TASK_TIME_5_SEC 	(uint)5000	// 5 sec
#define SND_TASK_TIME_10_SEC 	(uint)10000	// 1 sec

#define SND_TASK_TIME_30_SEC	(uint)30000	// 30 sec
#define SND_TASK_TIME_60_SEC	(uint)60000	// 60 sec


#define	SND_SMOKE_TMPORAL_MS    (uint)1500
#define	SND_SMOKE_ON_TIME_MS	(uint)500
#define	SND_SMOKE_OFF_TIME_MS	(uint)500
#define	SND_NUM_BEEPS_SMOKE     (uchar)3

#define	SND_CO_TMPORAL_MS       (uint)5000
#define	SND_CO_ON_TIME_MS       (uint)100
#define	SND_CO_OFF_TIME_MS      (uint)100
#define	SND_NUM_BEEPS_CO        (uchar)4
    
#ifdef CONFIGURE_WIFI
    // No network size is applicable
    #define SND_NET_SIZE_OF_2   (uchar)0
#else
    #define SND_NET_SIZE_OF_2   (uchar)2
#endif

// Set chirp time in ms
#define SND_CHIRP_TIME              (uint)30
#define SND_DBL_CHIRP_PAUSE_TIME    (uint)100


// Define some accelerated timer values for test
#define FAULT_TIMER_7_DAYS      (uchar)168  // normal time 168 hours = 7 Days
#define FAULT_TIMER_21_MINS     (uchar)21   // accelerated Timer - 21 mins.
#define FAULT_TIMER_5_MINS      (uchar)5    // 5 minutes used for voice timer



/*
|-----------------------------------------------------------------------------
| Typedef, Enums and Structures
|-----------------------------------------------------------------------------
*/
struct	SND_Alarm_Params
    {
        uint    on_time_ms;
        uint    off_time_ms;
        uint    temprl_time_ms;
        uint    num_beeps;
        uchar   voice;
    };

const struct SND_Alarm_Params snd_smoke_params = 
    {
         SND_SMOKE_ON_TIME_MS ,
         SND_SMOKE_OFF_TIME_MS ,
         SND_SMOKE_TMPORAL_MS ,
         SND_NUM_BEEPS_SMOKE ,
         1
    };

const struct SND_Alarm_Params snd_co_params =
    {
         SND_CO_ON_TIME_MS ,
         SND_CO_OFF_TIME_MS ,
         SND_CO_TMPORAL_MS ,
         SND_NUM_BEEPS_CO ,
         2
    };


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
static unsigned char snd_beep_count = 0;;
static unsigned char snd_trouble_chirp_count = 0;
static unsigned int  sounder_state = SND_STATE_IDLE;
static unsigned int  snd_play_voice_count = 0;
static volatile uchar snd_trouble_state = SND_TROUBLE_IDLE;

#ifdef CONFIGURE_WIRELESS_MODULE
    #ifdef CONFIGURED_UNDEFINED
        // Removed from Requirement
        persistent static unsigned char  snd_fault_timer;
    #endif
#endif

persistent static unsigned char  snd_voice_timer;

#ifdef CONFIGURE_LB_HUSH
    persistent static unsigned char  snd_lb_timer;
#endif

const struct SND_Alarm_Params *ptr_sound_params;

/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/
#ifdef CONFIGURE_WIRELESS_MODULE
  #ifdef CONFIGURE_UNDEFINED
    // Removed from Requirement
    void snd_start_fault_timer(void);
    void snd_process_fault_timer(void);
  #endif
#endif

#ifdef CONFIGURE_LB_HUSH    
    void snd_start_lb_timer(void);
    void snd_process_lb_timer(void);
#endif

void snd_start_voice_timer(void);
void snd_process_voice_timer(void);
void snd_do_double_chirp(void);

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/


/*
+------------------------------------------------------------------------------
| Function:      SND_Process_Task
+------------------------------------------------------------------------------
| Purpose:       Task to control alarm sound timing and trouble chirps
|                SND_Process_Task is only run in Active Mode
|
|                Trouble Chirps no longer blink the Red LED as to not disturb
|                 trouble LED blink rates/ error codes and Fade Profiles
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Sound Process State Next Execution Time (in Task TICs)
+------------------------------------------------------------------------------
*/

unsigned int SND_Process_Task(void)
{
    switch (sounder_state)
    {
        case SND_STATE_IDLE:
        {
            FLAG_SOUND_NOT_IDLE = 0;
            
            if(FLAG_LOW_BATTERY_CAL_MODE)
            {
                // Hang here until LB Threshold Calibrated
                return MAIN_Make_Return_Value(SND_DEFAULT_INTERVAL);
            }

            if(FLAG_SMOKE_ALARM_ACTIVE)
            {
                // Start Alarm Pattern Here
                HORN_ON
                
                #ifndef CONFIGURE_AUSTRALIA
                    RLED_On();
                #else
                    //
                    // Australian Models only drive RED LED if initiating
                    //
                    if(FLAG_MASTER_SMOKE_ACTIVE )
                    {
                        RLED_On();
                    }
                #endif

                ptr_sound_params = &snd_smoke_params;
                snd_beep_count = (*ptr_sound_params).num_beeps;
                sounder_state = SND_STATE_OFF;
                FLAG_SOUND_NOT_IDLE = 1;
                return MAIN_Make_Return_Value((*ptr_sound_params).on_time_ms);
            }

            if(FLAG_CO_ALARM_ACTIVE)
            {
                #ifdef CONFIGURE_CO
                    /*
                     * If the CO Calibration state is in verify state, go to
                     * Sound Verify State which turns on the sounder
                     * continuously.
                     */
                    uchar COState = CO_Cal_Get_State();

                    if( (CAL_STATE_FINAL_COMPLETE == COState) ||
                         (CAL_STATE_VERIFY == COState) )
                    {
                        sounder_state = SND_STATE_VERIFY;
                        FLAG_SOUND_NOT_IDLE = 1;

                        return MAIN_Make_Return_Value(SND_DEFAULT_INTERVAL);
                    }
                #endif

                // Start Alarm Pattern Here
                HORN_ON
                            
                #ifndef CONFIGURE_AUSTRALIA
                    RLED_On();
                #else
                    //
                    // Australian Models only drive RED LED if initiating
                    //
                    if(FLAG_CO_ALARM_CONDITION)
                    {
                        RLED_On();
                    }
                #endif

                ptr_sound_params = &snd_co_params;
                snd_beep_count = (*ptr_sound_params).num_beeps;
                sounder_state = SND_STATE_OFF;
                FLAG_SOUND_NOT_IDLE = 1;
                return MAIN_Make_Return_Value((*ptr_sound_params).on_time_ms);
            }
            
            #ifdef CONFIGURE_ESCAPE_LIGHT
                // If ESC Light remotely set by CCI then do not turn it off
                if(FLAG_ESC_LIGHT_ON && !FLAG_REMOTE_ESC_LIGHT_ON)
                {
                    // No alarm Condition, Turn OFF Escape Light
                    // and allow Active Time for Light to be turned off
                    FLAG_ESC_LIGHT_ON = 0;

                    // Refresh the Active Timer to insure Escape Light
                    //  gets turned OFF before transition to LP Mode

                    FLAG_ACTIVE_TIMER = 1;
                    if(MAIN_ActiveTimer < TIMER_ACTIVE_MINIMUM_2_SEC)
                    {
                        // Extend time to remain in Active Mode
                        MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_2_SEC;
                    }
                }
            #endif

            if(snd_trouble_chirp_count != 0)
            {
                #ifdef CONFIGURE_CHIRP_SERIAL
                    SER_Send_String("Chirp!");
                    SER_Send_Prompt();
                #endif

                snd_trouble_chirp_count--;
                HORN_ON
                sounder_state = SND_STATE_CHIRP;
                FLAG_SOUND_NOT_IDLE = 1;
                return MAIN_Make_Return_Value(SND_CHIRP_TIME);
            }
            else
            {
                // Clear Active Chirp indicator if no more chirps
                FLAG_CHIRPS_ACTIVE = 0;
            }
        }
        break;

        case SND_STATE_OFF:
        {
            #ifdef CONFIGURE_ESCAPE_LIGHT
                // During Alarm, Escape Light On
                FLAG_ESC_LIGHT_ON = 1;
            #endif

            HORN_OFF
            RLED_Off();

            if(0 == --snd_beep_count)
            {
                // Last Alarm Pattern Beep Completed
                if(FLAG_CO_ALARM_ACTIVE)
                {
                    // CO Alarm pattern Control
                    #ifdef CONFIGURE_VOICE
                        if(FLAG_PLAY_VOICE)
                        {
                            FLAG_PLAY_VOICE = 0;
                            if(FLAG_CO_ALARM_ACTIVE)
                            {
                                VCE_Play(VCE_CO_ENGLISH);
                            }
                        }
                        else
                        {
                            FLAG_PLAY_VOICE = 1;
                        }
                    #endif

                    #ifdef CONFIGURE_INTERCONNECT
                       /*
                        * If Master CO Alarm, signal Interconnect to
                        * transmit CO Alarm pattern over interconnect line
                        * during the temporal interval unless also in Slave CO
                        */
                        if(FLAG_CO_ALARM_CONDITION && 
                            !FLAG_SLAVE_CO_ACTIVE)
                        {
                            FLAG_INTCON_TX_CO_SIGNAL = 1;
                        }

                        #ifdef CONFIGURE_WIRELESS_MODULE
                            /*
                             * Remote CO can also drive the Interconnect
                             *  if its RF Gateway Timer has expired
                             *  and Gateway Remote to Slave is Enabled
                             */
                            else if(FLAG_REMOTE_ALARM_MODE && 
                                    FLAG_WL_GATEWAY_ENABLE &&
                                    FLAG_GATEWAY_REMOTE_TO_SLAVE)
                            {
                                FLAG_INTCON_TX_CO_SIGNAL = 1;
                            }
                        #endif
                    #endif
                }
                else
                {
                    // Smoke Alarm Pattern Control
                    if(FLAG_SMOKE_ALARM_ACTIVE)
                    {
                        // Unit still in Smoke Alarm
                        // Alarm pattern Control
                        #ifdef CONFIGURE_VOICE
                            if(FLAG_PLAY_VOICE)
                            {
                                FLAG_PLAY_VOICE = 0;
                                #ifdef CONFIGURE_VOICE
                                    VCE_Play(VCE_FIRE_ENGLISH_1);
                                #endif
                            }
                            else
                            {
                                FLAG_PLAY_VOICE = 1;
                            }
                        #endif
                    }
                }

                /*
                 * While in Smoke or CO Alarm, keep the trouble beep counter
                 *  cleared, for when we exit alarm.
                 * Clear any trouble chirps that may have accumulate
                 *   from a fault condition while in Alarm
                 */
                snd_trouble_chirp_count = 0;

                // This is the last beep of the series.  Wait for the
                // temporal interval to expire.
                sounder_state = SND_STATE_IDLE;

                return MAIN_Make_Return_Value
                            ((*ptr_sound_params).temprl_time_ms);
            }
            else
            {
                sounder_state = SND_STATE_ON;
                return MAIN_Make_Return_Value
                            ((*ptr_sound_params).off_time_ms);
            }
        }
        
        case SND_STATE_ON:
        {
            HORN_ON
            
            #ifndef CONFIGURE_AUSTRALIA
                RLED_On();
            #else
                //
                // Australian Models only drive RED LED if initiating
                //  alarm condition
                //
                if(FLAG_SMOKE_ALARM_ACTIVE && FLAG_MASTER_SMOKE_ACTIVE)
                {
                    RLED_On();
                }
                else if(FLAG_CO_ALARM_ACTIVE && FLAG_CO_ALARM_CONDITION)
                {
                    RLED_On();
                }
            #endif

            sounder_state = SND_STATE_OFF;
            return MAIN_Make_Return_Value((*ptr_sound_params).on_time_ms);
        }

        case SND_STATE_CHIRP:
        {
            HORN_OFF
            sounder_state = SND_STATE_IDLE;
        }
        break;

        case SND_STATE_VERIFY:
        {
            HORN_ON
            RLED_On();
        }
        break;

        default:
        {
            sounder_state = SND_STATE_IDLE;
        }
        break;
    }

    return MAIN_Make_Return_Value(SND_DEFAULT_INTERVAL);
}


/*
+------------------------------------------------------------------------------
| Function:      SND_Get_State
+------------------------------------------------------------------------------
| Purpose:       Return current sound state
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  current sounder task state
+------------------------------------------------------------------------------
*/

unsigned char SND_Get_State(void)
{
    return  sounder_state;
}


/*
+------------------------------------------------------------------------------
| Function:      SND_Do_Chirp
+------------------------------------------------------------------------------
| Purpose:       execute a sounder chirp
|                primarily used for Low Power Mode chirps
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void SND_Do_Chirp(void)
{
    // Turn on the sounder, delay and turn off the sounder
    HORN_ON

    // Delay here
    PIC_delay_ms(SND_CHIRP_TIME);

    HORN_OFF
    
}


/*
+------------------------------------------------------------------------------
| Function:      snd_do_double_chirp
+------------------------------------------------------------------------------
| Purpose:       execute a sounder double chirp
|                primarily used for Low Power Mode chirps
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void snd_do_double_chirp(void)
{
    SND_Do_Chirp();
    PIC_delay_ms(SND_DBL_CHIRP_PAUSE_TIME);
    SND_Do_Chirp();
    
}

/*
+------------------------------------------------------------------------------
| Function:      SND_Reset_Task
+------------------------------------------------------------------------------
| Purpose:       reset sounder process task to execute on next tic
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void SND_Reset_Task(void)
{
    snd_trouble_chirp_count = 0;
    MAIN_Reset_Task(TASKID_SOUND_TASK);
	
}

/*
+------------------------------------------------------------------------------
| Function:      SND_Set_Chirp_Cnt
+------------------------------------------------------------------------------
| Purpose:       Load chirp counter with number of chirps to execute
|
|
+------------------------------------------------------------------------------
| Parameters:    count - number of sounder chirps
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void SND_Set_Chirp_Cnt(uchar count)
{
    FLAG_CHIRPS_ACTIVE = 1;    // Stay in active mode during chirp counts
    snd_trouble_chirp_count = count;
	
}

/*
+------------------------------------------------------------------------------
| Function:      SND_Get_Chirp_Cnt
+------------------------------------------------------------------------------
| Purpose:       Return number of chirps in chirp counter
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  count - number of sounder chirps
+------------------------------------------------------------------------------
*/

uchar SND_Get_Chirp_Cnt(void)
{
    return snd_trouble_chirp_count;
}


/*
+------------------------------------------------------------------------------
| Function:      SND_Get_Trouble_State
+------------------------------------------------------------------------------
| Purpose:       Return current trouble manager state
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  current trouble manager state
+------------------------------------------------------------------------------
*/

unsigned char SND_Get_Trouble_State(void)
{
    return  snd_trouble_state;
}

/*
+------------------------------------------------------------------------------
| Function:      SND_Trouble_Manager_Task
+------------------------------------------------------------------------------
| Purpose:       Task to control trouble mode timing and audible actions
|                
|                 
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Trouble Manager State Next Execution Time (in Task TICs)
+------------------------------------------------------------------------------
*/

unsigned int SND_Trouble_Manager_Task(void)
{
    #ifdef CONFIGURE_DEBUG_TM_STATES
        // Test Only
        for(uchar i = (snd_trouble_state + 1); i > 0; i-- )
        {
            // Test Only
            MAIN_TP2_On();
            PIC_delay_us(DBUG_DLY_25_USec);
            MAIN_TP2_Off();
            PIC_delay_us(DBUG_DLY_25_USec);
        }
        PIC_delay_us(DBUG_DLY_25_USec);
    #endif

    switch	(snd_trouble_state)
    {
        case SND_TROUBLE_IDLE:
        {
            if( FLAG_PTT_ACTIVE || FLAG_LOW_BATTERY_CAL_MODE || 
                FLAG_SMOKE_HUSH_ACTIVE )
            {

                // Hang here during PTT or LB Cal or Smoke Hush
                break;
            }

            if(FLAG_CO_ALARM_ACTIVE || FLAG_BAT_CONSRV_MODE_ACTIVE ||
                    FLAG_SMOKE_ALARM_ACTIVE)
            {
                /*
                 *  CO and Smoke Alarm has priority over many trouble
                 *  conditions in regard to Sounder and Voice control
                 */ 
                #ifdef CONFIGURE_DEBUG_TM_STATES
                    // Test Only
                    for(uchar i = 2; i > 0; i-- )
                    {
                        // Test Only
                        MAIN_TP2_On();
                        PIC_delay_us(DBUG_DLY_25_USec);
                        MAIN_TP2_Off();
                        PIC_delay_us(DBUG_DLY_10_USec);
                    }
                #endif
                
              break;

            }
            
            else if(FLAG_EOL_FATAL)
            {
                // This must be tested before FLAG_FATAL_FAULT
                //  as both are set in EOL Fatal

                // Start the 5 Minute Voice Timer
                snd_start_voice_timer();

                snd_trouble_state = SND_TROUBLE_EOL_FATAL;
            }
            
            else if(FLAG_FAULT_FATAL)
            {   
                // Start the 5 Minute Voice Timer
                snd_start_voice_timer();

                snd_trouble_state = SND_TROUBLE_FATAL_FAULT;
            }
            
            else if(FLAG_APP_JOIN_MODE || FLAG_LED_OOB_PROFILE_ENABLE || 
                    FLAG_APP_SEARCH_MODE)
            {
                #ifdef CONFIGURE_DEBUG_TM_STATES
                    // Test Only
                    for(uchar i = 3; i > 0; i-- )
                    {
                        // Test Only
                        MAIN_TP2_On();
                        PIC_delay_us(DBUG_DLY_25_USec);
                        MAIN_TP2_Off();
                        PIC_delay_us(DBUG_DLY_10_USec);
                    }
                #endif

                // If in Join / Search Mode interrupt Trouble States
                break;
            }
            
            else if(FLAG_ALARM_IDENTIFY)
            {
                // No trouble Action during Alarm ID
                break;
            }
            
            else if(FLAG_EOL_MODE)
            {
                /*
                 * Do not enter EOL Mode if Night Detected in progress
                 *  Delays start of sounding (FLAG_SOUND_INHIBIT = 1) until Day
                 */
                if(!FLAG_SOUND_INHIBIT)
                {
                    snd_trouble_state = SND_TROUBLE_EOL_MODE;

                    // Start the 5 Minute Voice Timer
                    snd_start_voice_timer();

                    /*
                     * EOL has it's own 7 day Timer extension Timer
                     *  so don't use the 7 Day Fault Timer for this state
                     */
                }
            }
            
            else if(FLAG_LOW_BATTERY)
            {
                /*
                 * Do not enter Low Battery if Night Detected in progress
                 *  Delays start of sounding (FLAG_SOUND_INHIBIT) until Day
                 */
                if(!FLAG_SOUND_INHIBIT)
                {
                    snd_trouble_state = SND_TROUBLE_LB;

                    // Start with Chirp upon entry
                    FLAG_LB_CHIRP_NOW = 1;
                    
                    // Start the 5 Minute Voice Timer
                    snd_start_voice_timer();

                    #ifdef CONFIGURE_LB_HUSH
                        // Start the 7 Day Timer
                        snd_start_lb_timer();
                    #endif
                }
            }
            
            #ifndef CONFIGURE_NO_REMOTE_LB
                #ifdef CONFIGURE_WIRELESS_MODULE
                    else if(FLAG_STATUS_RMT_LB_ACTIVE)
                    {
                        /*
                         * This should apply to DC Models only
                         *  when RF Module sends a Low battery status...
                         * 
                         * Do not enter Low Battery if Night Detected which
                         *  Delays start of sounding (FLAG_SOUND_INHIBIT = 1) 
                         *  until Day
                         */
                        if(!FLAG_SOUND_INHIBIT)
                        {
                            snd_trouble_state = SND_TROUBLE_REMOTE_LB;

                            // Start the 5 Minute Voice Timer
                            snd_start_voice_timer();

                            #ifdef CONFIGURE_LB_HUSH
                                // Start the 7 Day Timer
                                snd_start_lb_timer();
                            #endif
                        }
                    }
                #endif
            #endif
            
            #ifdef CONFIGURE_WIRELESS_MODULE
                else if(FLAG_NETWORK_ERROR_MODE)
                {
                    /*
                     * Do not enter Network ERR Mode if Night Detected in 
                     *  progress. Delays start of sounding 
                     *  (FLAG_SOUND_INHIBIT = 1) until Day
                     */
                    if(!FLAG_SOUND_INHIBIT)
                    {
                        
                        snd_trouble_state = SND_TROUBLE_NETWORK_ERROR_MODE;

                        // Start the 5 Minute Voice Timer
                        snd_start_voice_timer();

                        #ifdef CONFIGURE_UNDEFINED
                            // Removed from Requirement

                            // Start the 7 Day Fault Timer
                            snd_start_fault_timer();
                        #endif
                    }
                }
            #endif
        }
        break;

        case SND_TROUBLE_FATAL_FAULT:
        {
            /*
             * This State must be run every 30 Seconds
             */
            
            /*
             * Allow Smoke Hush to take Priority, since there is Smoke
             *  detected in order to be in Smoke Hush
             * After Hush Times out or Smoke Condition ends, then
             *  Fatal Fault state can be re-entered.
             */
            if(FLAG_SMOKE_HUSH_ACTIVE)
            {
                // Voice Timer Off 
                SND_Init_Voice_Timer_Off();
                
                // Return to IDLE in this condition
                snd_trouble_state = SND_TROUBLE_IDLE;
            }
            
            /*
             * Alarm Modes take priority, so End Trouble Fault State
             */
            if(FLAG_CO_ALARM_ACTIVE || FLAG_BAT_CONSRV_MODE_ACTIVE ||
                    FLAG_SMOKE_ALARM_ACTIVE)
            {
                // Voice Timer Off 
                SND_Init_Voice_Timer_Off();
                
                // Return to IDLE in this condition
                snd_trouble_state = SND_TROUBLE_IDLE;
                break;
            }
            
            /*
             * Some fatal faults are recoverable so monitor FLAG_FAULT_FATAL
             * EOL fatal is the highest priority Fatal Fault, so exit on it.
             */
            else if(FLAG_FAULT_FATAL && !FLAG_EOL_FATAL )
            {
                snd_process_voice_timer();  // Update Voice Timer

                if(FLAG_MODE_ACTIVE)
                {
                    
                    #ifdef CONFIGURE_DUST_FAULT_BEEPS_OFF
                        if(!FLAG_FAULT_BEEP_INHIBIT)
                        {
                            // Trouble chirp in Fatal Fault Mode
                            snd_trouble_chirp_count ++;
                        }
                        else
                        {
                            // Do we want to inhibit Voice Message Also?
                        }
                    #else
                        // Trouble chirp in Fatal Fault Mode
                        snd_trouble_chirp_count ++;
                    #endif
                    
                }
                else
                {
                    #ifdef CONFIGURE_DUST_FAULT_BEEPS_OFF
                        if(!FLAG_FAULT_BEEP_INHIBIT)
                        {
                            // Low Power Mode Chirp
                            SND_Do_Chirp();
                        }
                        else
                        {
                            // Do we want to inhibit Voice Message Also?
                        }
                    #else
                        // Low Power Mode Chirp
                        SND_Do_Chirp();
                    #endif
                }

                if(FLAG_VOICE_TIMER_EXP && (0 == snd_play_voice_count))
                {
                     // 5 Minutes Expired (no Voice)

                     // Test Light/Dark Transition Detected
                     if(FLAG_DAY_NIGHT_SWITCHED)
                     {
                        FLAG_DAY_NIGHT_SWITCHED = 0 ;
                        // Announce Error Message on Light Transitions
                        #ifdef CONFIGURE_VOICE
                            VCE_Play(VCE_ERROR_SEE_GUIDE);
                        #endif

                        // Play Voice only one more time after this
                        snd_play_voice_count = 1;
                     }
                }
                else
                {
                    // Announce Error Message
                    #ifdef CONFIGURE_VOICE
                        VCE_Play(VCE_ERROR_SEE_GUIDE);
                    #endif

                    // Update play counter if needed
                    if(snd_play_voice_count != 0)
                    {
                        snd_play_voice_count--;
                    }
                }
                
                // Send Fault Code via Serial if Serial Port is enabled
                SER_Send_String("Fatal Err Code - ");
                SER_Send_Int(' ',FLT_Fatal_Code,10);
                SER_Send_Prompt();
                
                return MAIN_Make_Return_Value(SND_TASK_TIME_30_SEC);
            }
            else
            {
                // Voice Timer Off if Fault Cleared
                SND_Init_Voice_Timer_Off();
                
                // Return to IDLE if not in Fault
                snd_trouble_state = SND_TROUBLE_IDLE;
            }
        }
        break;

        case SND_TROUBLE_EOL_MODE:
        {
            /*
             * This State must be run every 30 Seconds
             */
            
             snd_process_voice_timer();  // Update Voice Timer
             
            // Some fatal faults are recoverable so monitor FLAG_FAULT_FATAL
            if(FLAG_EOL_MODE && !FLAG_EOL_FATAL && !FLAG_FAULT_FATAL &&
                !FLAG_CO_ALARM_ACTIVE && !FLAG_SMOKE_ALARM_ACTIVE &&
                !FLAG_SMOKE_HUSH_ACTIVE &&
                !FLAG_APP_JOIN_MODE && !FLAG_APP_SEARCH_MODE &&
                !FLAG_LED_OOB_PROFILE_ENABLE && !FLAG_ALARM_IDENTIFY)
            {

                if(FLAG_EOL_HUSH_ACTIVE)
                {
                    // EOL Hush silences all Chirps and Voices
                }
                else
                {
                    if(FLAG_MODE_ACTIVE)
                    {
                        // Double chirp in EOL Fatal Mode
                        snd_trouble_chirp_count ++;
                        snd_trouble_chirp_count ++;
                    }
                    else
                    {
                        // Low Power Mode Double Chirp
                        snd_do_double_chirp();
                    }

                    if(FLAG_VOICE_TIMER_EXP && (0 == snd_play_voice_count))
                    {
                         // 5 Minutes Expired (no Voice)

                         // Test Light/Dark Transition Detected
                         if(FLAG_DAY_NIGHT_SWITCHED)
                         {
                            FLAG_DAY_NIGHT_SWITCHED = 0 ;
                            
                            // Announce EOL Message on Light Transitions
                            #ifdef CONFIGURE_VOICE
                                VCE_Play(VCE_REPLACE_ALARM);
                            #endif

                            #ifdef CONFIGURE_VOICE
                                if(!FLAG_VOICE_BUSY)
                                {
                                    // And Button Message (should be buffered 
                                    //  in voice)
                                    VCE_Play(VCE_BUTTON_TO_SILENCE);
                                }
                            #endif   
                                
                            // Play Voice only one more time after this
                            snd_play_voice_count = 1;
                         }
                    }
                    else
                    {
                         // Announce EOL Message
                        #ifdef CONFIGURE_VOICE
                            VCE_Play(VCE_REPLACE_ALARM);
                            
                            if(!FLAG_VOICE_BUSY)
                            {
                                // And Button Message (should be buffered 
                                //  in voice)
                                VCE_Play(VCE_BUTTON_TO_SILENCE);
                            }
                        #endif

                         // Update play counter if needed
                         if(snd_play_voice_count != 0)
                         {
                             snd_play_voice_count--;
                         }
                    }
                }
                return MAIN_Make_Return_Value(SND_TASK_TIME_30_SEC);
            }
            else
            {
                // Voice Timer Off if Fault Cleared or we need to exit state.
                SND_Init_Voice_Timer_Off();

                // Return to IDLE if not in Fault
                snd_trouble_state = SND_TROUBLE_IDLE;
            }
        }
        break;

        case SND_TROUBLE_EOL_FATAL:
        {
            /*
             * This State must be run every 30 Seconds
             */
            
            /*
             * Alarm Modes take priority, so End Trouble EOL Fatal State
             */
            if(FLAG_CO_ALARM_ACTIVE || FLAG_BAT_CONSRV_MODE_ACTIVE ||
                    FLAG_SMOKE_ALARM_ACTIVE)
            {
                // Voice Timer Off 
                SND_Init_Voice_Timer_Off();
                
                // Return to IDLE in this condition
                snd_trouble_state = SND_TROUBLE_IDLE;
                break;
            }
            
            if(FLAG_EOL_FATAL  && !FLAG_SMOKE_HUSH_ACTIVE)
            {
                snd_process_voice_timer();  // Update Voice Timer

                if(FLAG_MODE_ACTIVE)
                {
                    // Double chirp in EOL Fatal Mode
                    snd_trouble_chirp_count ++;
                    snd_trouble_chirp_count ++;
                }
                else
                {
                    // Low Power Mode Double Chirp
                    snd_do_double_chirp();
                }

                if(FLAG_VOICE_TIMER_EXP && (0 == snd_play_voice_count))
                {
                     // 5 Minutes Expired (no Voice)

                     // Test Light/Dark Transition Detected
                     if(FLAG_DAY_NIGHT_SWITCHED)
                     {
                        FLAG_DAY_NIGHT_SWITCHED = 0 ;
                        
                        // Announce EOL Message on Light Transitions
                        #ifdef CONFIGURE_VOICE
                            VCE_Play(VCE_REPLACE_ALARM);
                        #endif

                        // Play Voice only one more time after this
                        snd_play_voice_count = 1;
                     }
                }
                else
                {
                     // Announce EOL Message
                    #ifdef CONFIGURE_VOICE
                        VCE_Play(VCE_REPLACE_ALARM);
                    #endif

                     // Update play counter if needed
                     if(snd_play_voice_count != 0)
                     {
                         snd_play_voice_count--;
                     }
                }
                return MAIN_Make_Return_Value(SND_TASK_TIME_30_SEC);
            }
            else
            {
                // Voice Timer Off 
                SND_Init_Voice_Timer_Off();

                // Return to IDLE if not in Fault
                snd_trouble_state = SND_TROUBLE_IDLE;
            }
        }
        break;

        case SND_TROUBLE_LB:
        {
            /*
             * This State must be run every 30 Seconds
             * Note: Once in Low Battery, we no longer inhibit for Night 
             *  Detect so don't test for that anymore.
             */
            
            // Abort LB State if in Alarm or Fault
            if(FLAG_CO_ALARM_ACTIVE    || FLAG_BAT_CONSRV_MODE_ACTIVE ||
                FLAG_SMOKE_ALARM_ACTIVE ||
                FLAG_FAULT_FATAL        ||
                FLAG_EOL_MODE  || FLAG_ALARM_IDENTIFY ||
                FLAG_ALERT_PANIC  || FLAG_ALERT_DETECT || 
                FLAG_APP_JOIN_MODE || FLAG_LED_OOB_PROFILE_ENABLE || 
                FLAG_APP_SEARCH_MODE || FLAG_SMOKE_HUSH_ACTIVE)
            {
                // Return to IDLE immediately if in Alarm or Fault
                snd_trouble_state = SND_TROUBLE_IDLE;

                // Voice Timer Off
                SND_Init_Voice_Timer_Off();

                return MAIN_Make_Return_Value(SND_TASK_TIME_1_SEC);
            }

            // Update LB Trouble Timers
            snd_process_voice_timer();  // Update Voice Timer
             
            #ifdef CONFIGURE_LB_HUSH
                snd_process_lb_timer();     // Update 7 Day Timer
            #endif

            // See if still in Low Battery
            if(FLAG_LOW_BATTERY)
            {
                // AC Powered or Active Mode?
                if(FLAG_MODE_ACTIVE)
                {
                    /*
                     * There is a low voltage limit to Hushing the LB Chirp
                     * Test for limit being reached.(FLAG_LB_HUSH_INHIBIT set)
                     */
                    if(FLAG_LB_HUSH_ACTIVE && !FLAG_LB_HUSH_INHIBIT)
                    {
                        // No LB Chirp (Hushed!)
                    }
                    else
                    {
                        // Only Chirp Every Other Cycle (60 seconds)
                        if(FLAG_LB_CHIRP_NOW)
                        {
                            FLAG_LB_CHIRP_NOW = 0;
                            // LB chirp in AC Power Operation
                            snd_trouble_chirp_count ++;
                        }
                        else
                        {
                            // Chirp Next Cycle
                            FLAG_LB_CHIRP_NOW = 1;
                        }
                    }
                }
                else
                {
                    /*
                     * There is a low voltage limit to Hushing the LB Chirp
                     * Test for limit being reached.(FLAG_LB_HUSH_INHIBIT set)
                     */
                    if(FLAG_LB_HUSH_ACTIVE && !FLAG_LB_HUSH_INHIBIT)
                    {
                        // No LB Chirp (Hushed!)
                    }
                    else
                    {
                        // Only Chirp Every Other Cycle (60 seconds)
                        if(FLAG_LB_CHIRP_NOW)
                        {
                            FLAG_LB_CHIRP_NOW = 0;
                            // Low Power Mode LB chirp
                            SND_Do_Chirp();
                        }
                        else
                        {
                            // Chirp Next Cycle
                            FLAG_LB_CHIRP_NOW = 1;
                        }
                    }
                    
                }

                /*
                 * Test for Voice Announcement
                 * There is a low voltage limit to Hushing Low Battery
                 * Test for limit being reached.(FLAG_LB_HUSH_INHIBIT set)
                 */
                if(FLAG_LB_HUSH_ACTIVE && !FLAG_LB_HUSH_INHIBIT)
                {
                    // No LB Voice Announced (when Hushed!)
                }
                else if(FLAG_VOICE_TIMER_EXP && (0 == snd_play_voice_count))
                {
                    // 5 Minutes Expired (no Voice)

                    // Test Light/Dark Transition Detected
                    if(FLAG_DAY_NIGHT_SWITCHED)
                    {
                        FLAG_DAY_NIGHT_SWITCHED = 0 ;
                        
                        // only Announce LB Message on Light Transitions
                        #ifdef CONFIGURE_VOICE
                            if(FLAG_LB_HUSH_DISABLED)
                            {
                                // FLAG_LB_HUSH_DISABLED should never be enabled
                                // as LB Hush was removed from requirement
                                
                                // After 7 days has expired
                                VCE_Play(VCE_REPLACE_ALARM);
                            }
                            else
                            {
                                #ifdef CONFIGURE_VCE_OPEN_SWITCH 
                                    // Test for Open Switch
                                    if(FLAG_BAT_ABNORMALLY_LOW)
                                    {
                                        VCE_Play(VCE_ACTIVATE_BATTERY);
                                    }
                                    else
                                    {
                                        // We replaced Low Battery message
                                        VCE_Play(VCE_REPLACE_ALARM);
                                        
                                        #ifdef CONFIGURE_LB_HUSH
                                            if(!FLAG_VOICE_BUSY)
                                            {
                                                // And Button Message (should be  
                                                //  buffered in voice)
                                                VCE_Play(VCE_BUTTON_TO_SILENCE);
                                            }
                                        #endif
                                        
                                    }
                                #else
                                    // We replaced Low Battery message
                                    VCE_Play(VCE_REPLACE_ALARM);

                                    #ifdef CONFIGURE_LB_HUSH
                                        if(!FLAG_VOICE_BUSY)
                                        {
                                            // And Button Message (should be  
                                            //  buffered in voice)
                                            VCE_Play(VCE_BUTTON_TO_SILENCE);
                                        }
                                    #endif
                                #endif
                            }
                            
                        #endif

                        // Play Voice only one more time after this
                        snd_play_voice_count = 1;
                    }
                }
                else
                {
                    // Announce LB Message for 1st 5 minutes
                    #ifdef CONFIGURE_VOICE
                        if( FLAG_LB_HUSH_DISABLED ||
                            FLAG_LB_HUSH_INHIBIT )
                        {
                            // FLAG_LB_HUSH_DISABLED should never be enabled
                            // as LB Hush was removed from requirement
                            
                            // After 7 days has expired or LB Inhibit is active
                             VCE_Play(VCE_REPLACE_ALARM);
                        }
                        else
                        {
                            #ifdef CONFIGURE_VCE_OPEN_SWITCH 
                                // Test for Open Switch
                                if(FLAG_BAT_ABNORMALLY_LOW)
                                {
                                    VCE_Play(VCE_ACTIVATE_BATTERY);
                                }
                                else
                                {
                                    // We replaced Low Battery message
                                    VCE_Play(VCE_REPLACE_ALARM);
                                }
                                
                                #ifdef CONFIGURE_LB_HUSH
                                    if(!FLAG_VOICE_BUSY)
                                    {
                                        // And Button Message (should be  
                                        //  buffered in voice)
                                        VCE_Play(VCE_BUTTON_TO_SILENCE);
                                    }
                                #endif
                            #else
                                // We replaced Low Battery message
                                VCE_Play(VCE_REPLACE_ALARM);

                                #ifdef CONFIGURE_LB_HUSH
                                    if(!FLAG_VOICE_BUSY)
                                    {
                                        // And Button Message (should be  
                                        //  buffered in voice)
                                        VCE_Play(VCE_BUTTON_TO_SILENCE);
                                    }
                                #endif
                            #endif
                        }
                    #endif

                    // Update play counter if needed
                    if(snd_play_voice_count != 0)
                    {
                        snd_play_voice_count--;
                    }
                }
                
                return MAIN_Make_Return_Value(SND_TASK_TIME_30_SEC);

            }
            else
            {
                // Return to Idle State Immediately if Low Battery was cleared
                snd_trouble_state = SND_TROUBLE_IDLE;
                
                // No need for LB Hush
                FLAG_LB_HUSH_ACTIVE = 0;
                
                #ifdef CONFIGURE_LB_HUSH  
                    // Turn Off LB Trouble Timer if LB Trouble ends
                    SND_Init_LB_Timer_Off();
                #endif
                
                // and Voice Timer Off
                SND_Init_Voice_Timer_Off();
            }
        }
        break;
        
        case SND_TROUBLE_NETWORK_ERROR_MODE:
        {

            #ifdef CONFIGURE_WIRELESS_MODULE
                /*
                 * This State must be run every 30 Seconds
                 * Note: Once in Network Error, no longer inhibit for Night 
                 *  Detect so don't test for that anymore.
                 * 
                 * If PTT Resume wait here until resume flag is cleared
                 */
                if(FLAG_PTT_RESUME)
                {
                    return MAIN_Make_Return_Value(SND_TASK_TIME_1_SEC);
                }

                /*
                 * Abort Network Error State if in Alarm LB Trouble, Certain 
                 *  Faults or Enrollment state changes
                 */
                if(FLAG_CO_ALARM_ACTIVE    ||
                    FLAG_SMOKE_ALARM_ACTIVE || FLAG_SMOKE_HUSH_ACTIVE ||
                    FLAG_FAULT_FATAL || FLAG_ALARM_IDENTIFY ||
                    FLAG_LOW_BATTERY || FLAG_STATUS_RMT_LB_ACTIVE ||
                    FLAG_EOL_MODE  ||
                    FLAG_APP_JOIN_MODE || FLAG_APP_SEARCH_MODE ||
                    FLAG_LED_OOB_PROFILE_ENABLE )
                {
                    // Return to IDLE immediately if in Alarm or Fault
                    snd_trouble_state = SND_TROUBLE_IDLE;

                    // Voice and Fault Timer Off
                    SND_Init_Voice_Timer_Off();
                    SND_Init_Fault_Timer_Off();

                    return MAIN_Make_Return_Value(SND_TASK_TIME_1_SEC);
                }

                // Update Network Error Timers
                snd_process_voice_timer();  // Update Voice Timer

                #ifdef CONFIGURE_UNDEFINED
                    // Removed from Requirement   
                    snd_process_fault_timer();  // Update 7 Day Timer
                #endif

                // See if still in Network Error 
                if(FLAG_NETWORK_ERROR_MODE)
                {
                    // No Audible Indications for Network of 2 or less devices 
                    // Unless a CCI Fault or Coordinator Fault is active
                    if( (APP_Rf_Network_Size > SND_NET_SIZE_OF_2) ||
                        (AMP_FAULT_CCI == FLT_NetworkFaultCode ) ||
                        (AMP_FAULT_COORDINATOR == FLT_NetworkFaultCode) )
                    {
                         // Active Mode
                        if(FLAG_MODE_ACTIVE)
                        {
                            // Test for Error Silenced
                            if(FLAG_NETWORK_ERROR_HUSH_ACTIVE)
                            {
                                // No Error Chirp (Hushed!)
                            }
                            else
                            {
                                // Error chirp in AC Power Operation
                                #ifdef CONFIGURE_CHIRP_ENABLE
                                    snd_trouble_chirp_count ++;
                                #else
                                    // This is now an error "Chime"
                                    #ifdef CONFIGURE_VOICE
                                        VCE_Play(VCE_SND_NETWORK_ERROR);
                                    #endif   
                                #endif

                            }
                        }
                        else
                        {
                            /*
                             * Test for Error Silenced
                             */
                            if(FLAG_NETWORK_ERROR_HUSH_ACTIVE)
                            {
                                // No Error Chirp (Hushed!)
                            }
                            else
                            {
                                // Low Power Mode Error chirp
                                SND_Do_Chirp();
                            }
                        }

                        // Test for Voice Announcement

                        // Test for Error Silenced Condition
                        if(FLAG_NETWORK_ERROR_HUSH_ACTIVE)
                        {
                            // No Error Voice Announced (when Hushed!)
                        }
                        else if( FLAG_VOICE_TIMER_EXP && 
                                 (0 == snd_play_voice_count) )
                        {
                             // 5 Minutes Expired (no Voice)

                             // Unless a Light Transition is detected
                             //  Test Light/Dark Transition Detected
                             if(FLAG_DAY_NIGHT_SWITCHED)
                             {
                                 FLAG_DAY_NIGHT_SWITCHED = 0 ;

                                 // announce network error message
                                #ifdef CONFIGURE_VOICE
                                    if(!FLAG_VOICE_BUSY)
                                    {
                                        if(FLAG_WIRELESS_DISABLED)
                                        {
                                            #ifdef CONFIGURE_WIFI
                                                VCE_Play(VCE_NETWORK_CONN_LOST);
                                            #else
                                                VCE_Play(VCE_REPLACE_ALARM);
                                            #endif
                                        }
                                        else
                                        {
                                            VCE_Play(VCE_NETWORK_CONN_LOST);
                                        }
                                    }
                                #endif

                                #ifdef CONFIGURE_VOICE
                                    if(!FLAG_VOICE_BUSY)
                                    {
                                        // And Button Message (should be 
                                        //  buffered in voice)
                                        VCE_Play(VCE_BUTTON_TO_SILENCE);
                                    }
                                #endif
                                // Play Voice only one more time after this
                                snd_play_voice_count = 1;
                             }
                        }
                        else
                        {
                            // Announce Network Error Message
                            #ifdef CONFIGURE_VOICE
                                if(FLAG_WIRELESS_DISABLED)
                                {
                                    #ifdef CONFIGURE_WIFI
                                        VCE_Play(VCE_NETWORK_CONN_LOST);
                                    #else
                                        VCE_Play(VCE_REPLACE_ALARM);
                                    #endif
                                }
                                else
                                {
                                    VCE_Play(VCE_NETWORK_CONN_LOST);
                                }
                            #endif

                            #ifdef CONFIGURE_VOICE
                                // No need to Silence if less than 3 devices 
                                //  enrolled
                                if( (APP_Rf_Network_Size > SND_NET_SIZE_OF_2) ||
                                    (AMP_FAULT_CCI == FLT_NetworkFaultCode) ||
                                    (AMP_FAULT_COORDINATOR == 
                                                FLT_NetworkFaultCode) )
                                {
                                    if(!FLAG_VOICE_BUSY)
                                    {
                                        // And Button Message (should be 
                                        //  buffered in voice)
                                        VCE_Play(VCE_BUTTON_TO_SILENCE);
                                    }
                                }
                            #endif

                             // Update play counter if needed
                             if(snd_play_voice_count != 0)
                             {
                                 snd_play_voice_count--;
                             }
                        }
                    }
                    
                    // Send Net Err Code via Serial if Serial Port is enabled
                    SER_Send_String("Network Err Code - ");
                    SER_Send_Int(' ',FLT_NetworkFaultCode,10);
                    SER_Send_Prompt();
                    
                    return MAIN_Make_Return_Value(SND_TASK_TIME_30_SEC);
                }
                else
                {
                    // Return to Idle State Immediately if Error was cleared
                    snd_trouble_state = SND_TROUBLE_IDLE;

                    // No longer need Network Error Hush
                    FLAG_NETWORK_ERROR_HUSH_ACTIVE = 0;

                    // Turn Off Fault Trouble Timer if Fault ends
                    SND_Init_Fault_Timer_Off();
                    // and Voice Timer Off
                    SND_Init_Voice_Timer_Off();
                }
            #endif

        }
        break;

        case SND_TROUBLE_REMOTE_LB:
        {
            #ifndef CONFIGURE_NO_REMOTE_LB
                #ifdef CONFIGURE_WIRELESS_MODULE
                    /*
                     * This state runs much like the Low Battery state, but I
                     *  separated it into this SND_TROUBLE_REMOTE_LB since we 
                     *  may want to operate differently.
                     * 
                     * This State must be run every 30 Seconds
                     * Note: Once in Remote Low Battery, we no longer inhibit 
                     *  for Night Detect so don't test for that anymore.
                     */

                    // Abort Remote LB State if in Alarm or Fault or 
                    //  Main Board LB
                    if(FLAG_CO_ALARM_ACTIVE    ||
                        FLAG_SMOKE_ALARM_ACTIVE ||
                        FLAG_FAULT_FATAL        ||
                        FLAG_LOW_BATTERY ||
                        FLAG_EOL_MODE  || 
                        FLAG_ALERT_PANIC  || 
                        FLAG_ALERT_DETECT || FLAG_ALARM_IDENTIFY ||
                        FLAG_APP_JOIN_MODE || FLAG_APP_SEARCH_MODE || 
                        FLAG_LED_OOB_PROFILE_ENABLE || FLAG_SMOKE_HUSH_ACTIVE)
                    {
                        // Return to IDLE immediately if in Alarm or Fault
                        snd_trouble_state = SND_TROUBLE_IDLE;

                        // Voice Timer Off
                        SND_Init_Voice_Timer_Off();

                        return MAIN_Make_Return_Value(SND_TASK_TIME_1_SEC);
                    }

                    // Update LB Trouble Timers
                    snd_process_voice_timer(); // Update Voice Timer

                    #ifdef CONFIGURE_LB_HUSH
                        snd_process_lb_timer(); // Update 7 Day Timer
                    #endif

                    // See if still in Remote Low Battery
                    if(FLAG_STATUS_RMT_LB_ACTIVE)
                    {
                        // Active Mode?
                        if(FLAG_MODE_ACTIVE)
                        {
                            // LB Hush?
                            if(FLAG_LB_HUSH_ACTIVE)
                            {
                                // No LB Chirp (Hushed!)
                            }
                            else
                            {
                                // Only Chirp Every Other Cycle (60 seconds)
                                if(FLAG_LB_CHIRP_NOW)
                                {
                                    FLAG_LB_CHIRP_NOW = 0;
                                    // LB chirp in AC Power Operation
                                    snd_trouble_chirp_count ++;
                                }
                                else
                                {
                                    // Chirp Next Cycle
                                    FLAG_LB_CHIRP_NOW = 1;
                                }
                            }
                        }
                        else
                        {
                            // LB Hush?
                            if(FLAG_LB_HUSH_ACTIVE) 
                            {
                                // No LB Chirp (Hushed!)
                            }
                            else
                            {
                                // Only Chirp Every Other Cycle (60 seconds)
                                if(FLAG_LB_CHIRP_NOW)
                                {
                                    FLAG_LB_CHIRP_NOW = 0;
                                    // Low Power Mode LB chirp
                                    SND_Do_Chirp();
                                }
                                else
                                {
                                    // Chirp Next Cycle
                                    FLAG_LB_CHIRP_NOW = 1;
                                }
                            }
                        }

                        /*
                         * Test for Voice Announcement
                         * There is a no low voltage limit to Hushing Remote 
                         *  Low Battery
                         */
                        if(FLAG_LB_HUSH_ACTIVE)
                        {
                            // No LB Voice Announced (when Hushed!)
                        }
                        else if( FLAG_VOICE_TIMER_EXP && 
                                 (0 == snd_play_voice_count) )
                        {
                            // 5 Minutes Expired (no Voice)

                            // Test Light/Dark Transition Detected
                            if(FLAG_DAY_NIGHT_SWITCHED)
                            {

                                FLAG_DAY_NIGHT_SWITCHED = 0 ;
                                // Announce LB Message on Light Transitions
                                #ifdef CONFIGURE_VOICE
                                    VCE_Play(VCE_REPLACE_ALARM);
                                #endif

                                // Play Voice only one more time after this
                                snd_play_voice_count = 1;
                            }

                        }
                        else
                        {
                            // Announce LB Message
                            #ifdef CONFIGURE_VOICE
                                VCE_Play(VCE_REPLACE_ALARM);

                                if(!FLAG_LB_HUSH_DISABLED)
                                {
                                    #ifdef CONFIGURE_LB_HUSH
                                        if(!FLAG_VOICE_BUSY)
                                        {
                                            // And Button Message (should be  
                                            //  buffered in voice)
                                            VCE_Play(VCE_BUTTON_TO_SILENCE);
                                        }
                                    #endif
                                }

                            #endif

                            // Update play counter if needed
                            if(snd_play_voice_count != 0)
                            {
                                snd_play_voice_count--;
                            }
                        }
                        return MAIN_Make_Return_Value(SND_TASK_TIME_30_SEC);

                    }
                    else
                    {
                        // Return to Idle State Immediately if Low Battery 
                        //  was cleared
                        snd_trouble_state = SND_TROUBLE_IDLE;

                        // No need for LB Hush
                        FLAG_LB_HUSH_ACTIVE = 0;

                        #ifdef CONFIGURE_LB_HUSH  
                            // Turn Off LB Trouble Timer if LB Trouble ends
                            SND_Init_LB_Timer_Off();
                        #endif

                        // and Voice Timer Off
                        SND_Init_Voice_Timer_Off();
                    }
                #endif
            #endif
        }
        break;


        default:
            snd_trouble_state = SND_TROUBLE_IDLE;
        break;
    }

    // Determine Return Time if no trouble
    if(FLAG_MODE_ACTIVE)
    {
        return MAIN_Make_Return_Value(SND_TASK_TIME_100MS);
    }
    else
    {
        // For Low Power Mode operation and no trouble
        return	MAIN_Make_Return_Value(SND_TASK_TIME_5_SEC);
    }
}


#ifdef CONFIGURE_WIRELESS_MODULE

/*
+------------------------------------------------------------------------------
| Function:      SND_Init_Fault_Timer_Off
+------------------------------------------------------------------------------
| Purpose:       Init fault Timer - Timer Off  and LB/ERR Flags Cleared
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void SND_Init_Fault_Timer_Off(void)
{
    FLAG_FAULT_7_DAY_TIMER_ACTIVE = 0;
    FLAG_NET_ERR_7_DAY_EXP = 0;
}

#ifdef CONFIGURE_UNDEFINED
// Removed from Requirement

/*
+------------------------------------------------------------------------------
| Function:      snd_start_fault_timer
+------------------------------------------------------------------------------
| Purpose:       Start the 7 Day Fault/Trouble Timer unless it already is 
|                running or Expired
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void snd_start_fault_timer(void)
{
    #ifdef CONFIGURE_ACCEL_TIMERS
        if(!FLAG_FAULT_7_DAY_TIMER_ACTIVE && !FLAG_NET_ERR_7_DAY_EXP)
        {
            snd_fault_timer = MAIN_OneMinuteTic;
            FLAG_FAULT_7_DAY_TIMER_ACTIVE = 1;

            #ifdef CONFIGURE_TIMER_SERIAL
                SER_Send_String("Start Err 7 Day- ");
                SER_Send_Prompt();
            #endif
        }
    #else
        if(!FLAG_FAULT_7_DAY_TIMER_ACTIVE && !FLAG_NET_ERR_7_DAY_EXP)
        {
            snd_fault_timer = MAIN_OneHourTic;
            FLAG_FAULT_7_DAY_TIMER_ACTIVE = 1;
        }
    #endif
}
#endif

#ifdef CONFIGURE_UNDEFINED
// Removed from Requirement   

/*
+------------------------------------------------------------------------------
| Function:      snd_process_fault_timer
+------------------------------------------------------------------------------
| Purpose:       Update 7 Day Fault/Trouble Timer and set Flags accordingly 
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void snd_process_fault_timer(void)
{
    if(FLAG_FAULT_7_DAY_TIMER_ACTIVE)
    {

        #ifdef CONFIGURE_ACCEL_TIMERS            
            // Monitor Timer for expiration
            if( (uchar)(MAIN_OneMinuteTic - snd_fault_timer) >= 
                 (uchar)FAULT_TIMER_21_MINS)
            {
                FLAG_NET_ERR_7_DAY_EXP = 1;
                FLAG_FAULT_7_DAY_TIMER_ACTIVE = 0;

                #ifdef CONFIGURE_TIMER_SERIAL
                    SER_Send_String("Err 7 Day Expired- ");
                    SER_Send_Prompt();
                #endif
            }
        #else
            // Monitor Timer for expiration
            if( (uchar)(MAIN_OneHourTic - snd_fault_timer) >= 
                 (uchar)FAULT_TIMER_7_DAYS)
            {
                FLAG_NET_ERR_7_DAY_EXP = 1;
                FLAG_FAULT_7_DAY_TIMER_ACTIVE = 0;
            }
        #endif
    }
}
#endif
    
#endif
    

/*
+------------------------------------------------------------------------------
| Function:      SND_Init_Voice_Timer_Off
+------------------------------------------------------------------------------
| Purpose:       Init Voice Timer - Timer Off and cleared
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void SND_Init_Voice_Timer_Off(void)
{
    FLAG_VOICE_TIMER_ACTIVE = 0;
    FLAG_VOICE_TIMER_EXP = 0;
}


/*
+------------------------------------------------------------------------------
| Function:      snd_start_voice_timer
+------------------------------------------------------------------------------
| Purpose:       Start /Reset the 5 minute Voice Timer 
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void snd_start_voice_timer(void)
{
    snd_voice_timer = MAIN_OneMinuteTic;
    FLAG_VOICE_TIMER_ACTIVE = 1;
    FLAG_VOICE_TIMER_EXP =0;
    
    #ifdef CONFIGURE_TIMER_SERIAL
        SER_Send_String("Start Voice Timer 5 Min-  ");
        SER_Send_Prompt();
    #endif    

}


/*
+------------------------------------------------------------------------------
| Function:      snd_process_voice_timer
+------------------------------------------------------------------------------
| Purpose:       Update 5 minute Voice Timer and set Flags accordingly
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void snd_process_voice_timer(void)
{
    if(FLAG_VOICE_TIMER_ACTIVE)
    {
        // Monitor Timer for expiration
        if( (uchar)(MAIN_OneMinuteTic - snd_voice_timer) >= 
             (uchar)FAULT_TIMER_5_MINS)
        {
            FLAG_VOICE_TIMER_EXP = 1;
            FLAG_VOICE_TIMER_ACTIVE = 0;
            
            /*
             * Initialize light switch detect, for voice annunciations
             * Not monitored during voice active, clear any previous 
             *  Light Switch detects, 
             */
            FLAG_DAY_NIGHT_SWITCHED = 0 ;

            #ifdef CONFIGURE_TIMER_SERIAL
                SER_Send_String("Voice Timer Expired- ");
                SER_Send_Prompt();
            #endif           
        }
    }
}


#ifdef CONFIGURE_LB_HUSH

/*
+------------------------------------------------------------------------------
| Function:      snd_start_lb_timer
+------------------------------------------------------------------------------
| Purpose:       Start the 7 Day LB Trouble Timer unless it already is running
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void snd_start_lb_timer(void)
{
    #ifdef CONFIGURE_ACCEL_TIMERS
        // If it was already running or expired, do not reset, resume
        //  from where it left off.
        if(!FLAG_LB_TIMER_ACTIVE && !FLAG_LB_HUSH_DISABLED)
        {
            snd_lb_timer = MAIN_OneMinuteTic;
            FLAG_LB_TIMER_ACTIVE = 1;

            #ifdef CONFIGURE_TIMER_SERIAL
                SER_Send_String("Start LB 7 Day Timer- ");
                SER_Send_Prompt();
            #endif            
        }
    #else    
        // If it was already running or expired, do not reset, resume
        //  from where it left off.
        if(!FLAG_LB_TIMER_ACTIVE && !FLAG_LB_HUSH_DISABLED)
        {
            snd_lb_timer = MAIN_OneHourTic;
            FLAG_LB_TIMER_ACTIVE = 1;
        }
    #endif
}


/*
+------------------------------------------------------------------------------
| Function:      snd_process_lb_timer
+------------------------------------------------------------------------------
| Purpose:       Update LB Trouble Timer and set Flags accordingly
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void snd_process_lb_timer(void)
{
    if(FLAG_LB_TIMER_ACTIVE)
    {

        #ifdef CONFIGURE_ACCEL_TIMERS
            // Monitor Timer for expiration
            if( (uchar)(MAIN_OneMinuteTic - snd_lb_timer) >= 
                 (uchar)FAULT_TIMER_21_MINS )
            {
                // LB Timer has expired
                FLAG_LB_HUSH_DISABLED = 1;
                FLAG_LB_TIMER_ACTIVE = 0;

                // When the 7 day timer expires, reset the 5 minute Voice 
                //  Timer so Replace Alarm is announced again.
                SND_Init_Voice_Timer_Off();
                snd_start_voice_timer();

                #ifdef CONFIGURE_TIMER_SERIAL
                    SER_Send_String("LB 7 Day Expired- ");
                    SER_Send_Prompt();
               #endif                
            }
        #else        
            // Monitor Timer for expiration
            if( (MAIN_OneHourTic - snd_lb_timer) >= FAULT_TIMER_7_DAYS)
            {
                // LB Timer has expired
                FLAG_LB_HUSH_DISABLED = 1;
                FLAG_LB_TIMER_ACTIVE = 0;

                // When the 7 day timer expires, reset the 5 minute Voice
                //   Timer so Replace Alarm is announced again.
                SND_Init_Voice_Timer_Off();
                snd_start_voice_timer();
            }
        #endif
    }
}


/*
+------------------------------------------------------------------------------
| Function:      SND_Init_LB_Timer_Off
+------------------------------------------------------------------------------
| Purpose:       Init LB Timer - Timer Off and cleared
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void SND_Init_LB_Timer_Off(void)
{
    FLAG_LB_TIMER_ACTIVE = 0;
    FLAG_LB_HUSH_DISABLED = 0;
}
    
#endif






