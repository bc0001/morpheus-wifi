/****************************************************************
 * The contents of this file are Kidde proprietary  and confidential.
 * *************************************************************/
// SPI.C
#define	_SPI_C


#include "common.h"
#include "spi.h"
#include "serial.h"
#include "systemdata.h"

#ifdef	_CONFIGURE_FAST_SPI
	struct _tag_Analog_Regs Analog_Shadow_Regs ;
#endif

void SPI_Init(void)
{
  	UCB0CTL1 |= UCSWRST;                      // USCI logic held in reset state.
  	
  	/*	UCB0CTL0 = 0xA9
  	 *	parity Enable, parity Odd
  	 *  MSB first, 8 Data Bits
  	 *  2 stop Bits
  	 *  UART mode
  	 *  Synchronous mode
  	 */
  	UCB0CTL0 = 0xA9; 
  	                                                                             
	UCB0CTL1 |= UCSSEL_1;                     // Clock source = ACLK 
  	UCB0BR0 = 0x00;                           // Clear Baudrate control regs
  	UCB0BR1 = 0x00;                           //
  	//UCB0MCTL = 0;                           // No modulation 
  	 
  	UCB0CTL1 &= ~UCSWRST;                     // USCI reset released for operation 
    //UCB0IE |= UCRXIE;                       // Enable USCI_A0 RX interrupt
}


/*****************************************************************************/
void SPI_Send(unsigned char data)
{
	//unsigned int i; 
	while (!(UCB0IFG & UCTXIFG));           // USCI_A0 TX buffer ready?
  	UCB0TXBUF = data;                     // Transmit address     
	while (!(UCB0IFG & UCTXIFG));           // USCI_A0 TX buffer ready?
    //for(i=0;i<50;i=i+1); 				  // small delay	. RX interrupt should happen
}


/*****************************************************************************/
void SPI_Write(unsigned char address,unsigned char data)
{

#ifdef	_CONFIGURE_FAST_SPI
	// First update shadow registers.
	*( ((UCHAR *)&Analog_Shadow_Regs) + address) = data ;
#endif	
	
	// Disable P1 interrupt because PORT1 interrupt handler reads SPI port.
	//P1IE &= ~PORT_INTERRUPT_PIN ;
	
    P3OUT |= PORT_SRESET_PIN; // CS high
	SPI_Send((address << 1) | 1 ) ;	// send address
	//SPI_Send(address);	// send address
	SPI_Send(data);     // send data
    P3OUT &= ~PORT_SRESET_PIN; // CS low

	// Enable P1 interrupt.
	//P1IE |= PORT_INTERRUPT_PIN ;
}


/*****************************************************************************/
unsigned char SPI_Read(unsigned char address)
{
	// Disable P1 interrupt because PORT1 interrupt handler reads SPI port.
	//P1IE &= ~PORT_INTERRUPT_PIN ;

    P3OUT |= PORT_SRESET_PIN;			// CS high
	SPI_Send((address << 1)); 	// send address
	SPI_Send(0x00);  			// send zeros
    P3OUT &= ~PORT_SRESET_PIN; 			// CS low
    
	// Enable P1 interrupt.
	//P1IE |= PORT_INTERRUPT_PIN ;
	//
	// Value read should be in receive buffer.
	//
	return	UCB0RXBUF ; 
}

//************************************************************************
// address: Address of the register to update
// val: 	Value to update register with.  val must have the bits to update
//			in the proper bit position for the contents of address.
// mask:	The bitmask to use for combining bits into register.
//			
UCHAR SPI_Update_Reg(UCHAR address, UCHAR val, UCHAR mask)
{
	UCHAR regval;
	
#ifdef	_CONFIGURE_FAST_SPI
	// For AGPIO 1,2,3 Timer application we may need to
	// Read the address from the Analog Die before writing
	if(FLAG_DISABLE_SHADOW)
	{
		// Get current value of register from Analog SS
		regval = SPI_Read(address) ;
	}
	else
	{
		regval = *( ((UCHAR *)&Analog_Shadow_Regs) + address) ;
	}
#else
	// Get current value of register from Analog SS
	regval = SPI_Read(address) ;
#endif

	// Mask off bits
	regval &= ~mask ;
	regval |= val ;
	
#ifdef	_CONFIGURE_FAST_SPI
	// Disable P1 interrupt because PORT1 interrupt handler reads SPI port.
	//P1IE &= ~PORT_INTERRUPT_PIN ;

	// Update Shadow registers.
	*( ((UCHAR *)&Analog_Shadow_Regs) + address) = regval ;
	
	// Enable Analog Die SPI
    P3OUT |= PORT_SRESET_PIN; 				// CS high
    
	// Send the address
  	UCB0TXBUF = (address << 1) | 1 ;         // Transmit address     
	while (!(UCB0IFG & UCTXIFG));           // USCI_A0 TX buffer ready?
	
  	UCB0TXBUF = regval ;                     // Transmit Data     
	while (!(UCB0IFG & UCTXIFG));			// USCI_A0 TX buffer ready?
	
    P3OUT &= ~PORT_SRESET_PIN; 				// CS low
	
	// Enable P1 interrupt.
	//P1IE |= PORT_INTERRUPT_PIN ;

#else
	SPI_Write(address,	regval) ;
#endif	
	
	return TRUE ;	
}



