/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        pic_helper.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  common definitions and macros available for entire project 
|               related to the MicroChip PIC device designed in this project
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


#ifndef PIC_HELPER_H
#define	PIC_HELPER_H

#ifdef	__cplusplus
extern "C" {
#endif
    
/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
// Not applicable

/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

    #define BANK0   0
    #define BANK1   1
    #define BANK2   2
    #define BANK3   3
    #define BANK4   4
    #define BANK5   5
    #define BANK6   6



    #define CLEAR_WDT      asm("clrwdt");
    #define GOTOSLEEP      asm("sleep");
    #define SOFT_RESET     asm("reset");

    #define XTAL_FREQ  16000000    // 16 Mhz

    // To use the macros below, YOU must have previously defined _XTAL_FREQ
    // Convert time (us or ms) into number of instruction cycles used by 
    //  _delay().
    #define PIC_delay_us(x) _delay((unsigned long)((x)*(XTAL_FREQ/4000000.0)))
    #define PIC_delay_ms(x) _delay((unsigned long)((x)*(XTAL_FREQ/4000.0)))

/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/

#ifndef TMR1
    // PIC16LF18877 header file only declared TMR1L and TMR1H
    // Added to match other Timer 16 bit declarations
    extern volatile unsigned short          TMR1   @ 0x20C;
#endif


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
// Not applicable

#ifdef	__cplusplus
}
#endif

#endif	/* PIC_HELPER_H */

