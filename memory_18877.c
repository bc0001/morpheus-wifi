/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        memory_18877.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Routines for Non-Volatile Memory support for PIC 18877
|               
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/



/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include    "common.h"
#include    "memory_18877.h"
#include    "fault.h"
#include    "diaghist.h"
#include    "systemdata.h"


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

//NV Memory Located starting at address 0x7000
#define MEMORY_ADDR_NV_HIGH (uchar)0x70


/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/


/*
+------------------------------------------------------------------------------
| Function:      MEMORY_Byte_Read
+------------------------------------------------------------------------------
| Purpose:       Read byte from EE memory
|
|
+------------------------------------------------------------------------------
| Parameters:    addr, holds address to read
+------------------------------------------------------------------------------
| Return Value:  returns read data byte
+------------------------------------------------------------------------------
*/
uchar MEMORY_Byte_Read(uchar addr)
{
    
    // Load EE Address to read
    NVMADRH = MEMORY_ADDR_NV_HIGH;
    NVMADRL = addr;
    // select Non-Volatile memory Access (NVMREGS))
    NVMCON1bits.NVMREGS = 1;
    
    NVMCON1bits.RD = 1;

    return (NVMDATL);
    
}


/*
+------------------------------------------------------------------------------
| Function:      MEMORY_Byte_Write
+------------------------------------------------------------------------------
| Purpose:       Writes 1 byte at EEProm address
|                
|
+------------------------------------------------------------------------------
| Parameters:    uchar byte, Data to be written into EEProm
|                uchar dest, Address of EE byte (0-255)
|
+------------------------------------------------------------------------------
| Return Value:  TRUE if Successful,  FALSE if write error
+------------------------------------------------------------------------------
*/
uchar MEMORY_Byte_Write(uchar byte, uchar dest)
{
    // Set EE address of write (0-255)
    NVMADRH = MEMORY_ADDR_NV_HIGH;
    NVMADRL = dest;

    // Load Data to be written
    NVMDATL = byte;

    // Disable Interrupts
    GLOBAL_INT_ENABLE = 0;

    // select Non-Volatile memory Access (NVMREGS)
    NVMCON1bits.NVMREGS = 1;
    NVMCON1bits.WREN = 1;

    // Unlock Code for Non Volatile Write Access
    NVMCON2 = 0x55;
    NVMCON2 = 0xAA;
    
    // Begin EE Write
    NVMCON1bits.WR = 1;
    NVMCON1bits.WREN = 0;
    
    //  Global Ints Enabled for Both Low Power CCI Ints and Active mode
    GLOBAL_INT_ENABLE = 1;
    
    // Wait for the write complete
    // WR flag is cleared by hardware after completion
    while(NVMCON1bits.WR == 1)
    {
        // Wait for the write complete
        
        #ifdef CONFIGURE_WDT_RST_FLAGS
            // Test Only, Remove later
            FLAG_WDT_1_6 = 1;
        #endif
        
    }

    #ifdef CONFIGURE_WDT_RST_FLAGS
        // Test Only, Remove later
        FLAG_WDT_1_6 = 0;
    #endif

    // Verify that the byte was written correctly
    if(byte == MEMORY_Byte_Read(dest))
    {
        // Verify confirmed
        return TRUE;
    }
    else
    {
        // Write Verify error
        return FALSE;
    }

}


/*
+------------------------------------------------------------------------------
| Function:      MEMORY_Read_Block
+------------------------------------------------------------------------------
| Purpose:       Reads n bytes starting at an address
|                
+------------------------------------------------------------------------------
| Parameters:    num_bytes,  Number of sequential bytes to read
|                source,     Starting address in EE Memory (0 - 255)
|                dest:       Pointer to address of first byte in RAM
    
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void MEMORY_Read_Block(uchar num_bytes, uchar source, uchar *pdest)
{
    for(uchar i = 0; i < num_bytes; i++, source++, pdest++)
    {
        *pdest = MEMORY_Byte_Read(source);
    }
}


/*
+------------------------------------------------------------------------------
| Function:      MEMORY_Write_Block
+------------------------------------------------------------------------------
| Purpose:       Writes n bytes starting at an address
|                
+------------------------------------------------------------------------------
| Parameters:    num_bytes,  Number of sequential bytes to write
|                source,     Pointer to starting address in Ram
|                dest:       Address of first EE byte (0-255)
    
+------------------------------------------------------------------------------
| Return Value:  TRUE if Successful,  FALSE if write error
+------------------------------------------------------------------------------
*/
uchar MEMORY_Write_Block(uchar num_bytes, uchar *psource, uchar dest)
{
    // Init Default Write Block Result
    uchar result = TRUE;

    for(uchar i = 0; i < num_bytes; i++)
    {
        if(FALSE == MEMORY_Byte_Write(*psource, dest))
        {
            // Memory Write Error
            DIAG_Hist_EE_Error();   // Count this error in DIAG_CNT_EE_ERR

            // Try again if Write Verify was not confirmed
            if(FALSE == MEMORY_Byte_Write(*psource, dest))
            {
                // Memory Write Error
                DIAG_Hist_EE_Error();   // Count this error in DIAG_CNT_EE_ERR
                // Indicate a Write Error was detected
                result = FALSE;
            }
        }
        psource++;
        dest++;
    }

    return result;

}


/*
+------------------------------------------------------------------------------
| Function:      MEMORY_Byte_Write_Verify
+------------------------------------------------------------------------------
| Purpose:       Writes 1 byte at EEProm address
|                Verifies the write and RETRIES 1 time if needed
|
+------------------------------------------------------------------------------
| Parameters:    uchar byte, Data to be written into EEProm
|                uchar dest, Address of EE byte (0-255)
|
+------------------------------------------------------------------------------
| Return Value:  TRUE - Write verified,  FALSE - Write error
+------------------------------------------------------------------------------
*/
uchar MEMORY_Byte_Write_Verify(uchar byte, uchar dest)
{
    if(FALSE == MEMORY_Byte_Write(byte, dest) )
    {
        // Memory Write Error
        DIAG_Hist_EE_Error();   // Count this error in DIAG_CNT_EE_ERR
        if(FALSE == MEMORY_Byte_Write(byte, dest) )
        {
            // Memory Write Error
            DIAG_Hist_EE_Error();   // Count this error in DIAG_CNT_EE_ERR
            return FALSE;
        }
    }
    return TRUE;
}


