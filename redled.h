/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// REDLED.H


#ifndef	_REDLED_H
#define	_REDLED_H

    // Prototypes
    UINT RLED_Manager(void) ;
    UINT RLED_Blink(void);
    void RLED_Reset_Task(void) ;
    void RLED_On (void) ;
    void RLED_Off (void) ;
	
#endif


