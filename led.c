/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        led.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Supports Green,Red and Amber Led control
|               and PWM LED Profile Interrupt Control
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/



/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/

#include    "common.h"
#include    "led.h"

#include    "systemdata.h"
#include    "cocalibrate.h"
#include    "a2d.h"
#include    "fault.h"

#include    "photosmoke.h"
#include    "sound.h"
#include    "voice.h"

#ifdef CONFIGURE_WIRELESS_MODULE
    #include    "app.h"
#endif


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

// Debug  Section, Test Only Defines (un-comment as needed)

//#define DBUG_SERIAL_OUTPUT_RLED_STATE
//#define DBUG_SERIAL_OUTPUT_GLED_STATE
//#define DBUG_SERIAL_OUTPUT_ALED_STATE
//#define CONFIGURE_DEBUG_ALED_STATES



// 10th Step in the profile
#define LED_PROFILE_IDX_10      (uchar)10

#define LED_PROF_INT_10_SEC     (uint)100
#define LED_PROF_INT_3_SEC      (uint)30
#define LED_PROF_INT_2_SEC      (uint)20
#define LED_PROF_INT_1_SEC      (uint)10
#define LED_PROF_INT_500_MS     (uint)5
#define LED_PROF_INT_200_MS     (uint)2
#define LED_PROF_INT_100_MS     (uint)1

#define SEARCH_TIMER_3_MIN      (uint)1800    //in 100 ms units
#define SEARCH_TIMER_100_MS     (uint)1       //in 100 ms units

// Define Blink rates
#define LED_BLINK_10_MS     (uint)10        // 10 ms
#define LED_BLINK_30_MS     (uint)30        // 30 ms
#define LED_BLINK_40_MS     (uint)40        // 40 ms
#define LED_BLINK_50_MS     (uint)50        // 50 ms
#define LED_BLINK_100_MS    (uint)100       // 100 ms
#define LED_BLINK_200_MS    (uint)200       // 0 Cal
#define LED_BLINK_400_MS    (uint)400       // 300 ms
#define LED_BLINK_300_MS    (uint)300       // 300 ms
#define LED_BLINK_500_MS    (uint)500       // 500 ms
#define LED_BLINK_1_SEC     (uint)1000      // Functional CO Mode
#define LED_BLINK_2_SEC     (uint)2000      // Low Battery Hush and Smoke Hush
#define LED_BLINK_3_SEC     (uint)3000      // 3 Seconds
#define LED_BLINK_4_SEC     (uint)4000      // 4 Seconds
#define LED_BLINK_5_SEC     (uint)5000      // 5 Seconds
#define LED_BLINK_10_SEC    (uint)10000     // CO Peak Memory
#define LED_BLINK_16_SEC    (uint)16000     // 16 seconds
#define LED_BLINK_30_SEC    (uint)30000     // 30 seconds
#define LED_BLINK_60_SEC    (uint)61000     // Low Power Standby Mode

// Set Standby LED Blink Time in ms
#define LED_BLINK_TIME_30MS         (uint)30
#define LED_BLINK_TIME_50MS         (uint)50
#define LED_BLINK_TIME_100MS        (uint)100

// 1 minute Timer Units
#define LED_ALM_MEMORY_60_MIN       (uchar)60
#define LED_ALM_MEMORY_3_MIN        (uchar)3
#define LED_ALM_MEMORY_5_MIN        (uchar)5

// 1 Second Timer Units
#define LED_ALERT_TIMER_40_SEC      (uchar)40
#define LED_ALERT_TIMER_30_SEC      (uchar)30
#define LED_ALERT_TIMER_10_SEC      (uchar)10
#define LED_ALERT_TIMER_5_SEC       (uchar)5

#define ALERT_HORN_DELAY_2_SECS     (uchar)2
#define ALERT_HORN_DELAY_3_SECS     (uchar)3
#define ALERT_HORN_DELAY_4_SECS     (uchar)4

// 1/2 second Timer Units
#define LED_PANIC_ALERT_TIMER_30_SEC      (uchar)60    //1/2 second units
#define LED_PANIC_ALERT_TIMER_5_SEC       (uchar)10    //1/2 second units

// 100 ms Timer Units
#define LED_ID_TIMER_2_SEC          (uint)20
#define LED_ID_TIMER_1_MIN          (uint)600
#define LED_ID_TIMER_3_MIN          (uint)1800

#define LED_PING_TIMER_1_SEC        (uint)10
#define LED_PING_TIMER_1700_MS      (uint)17
#define LED_PING_TIMER_2_SEC        (uint)20
#define LED_PING_TIMER_3_SEC        (uint)30


#define LED_TIMER_5_MINS            (uchar)5
#define LED_TIMER_10_MINS           (uchar)10
#define LED_TIMER_20_MINS           (uchar)20
#define LED_TIMER_40_MINS           (uchar)40

// A/D Reference for PIC = VDD
// FVR to calculate VDD = 1024 mv
#define FVR_MVOLTS                  (uint)1024
// FVR_MVOLTS * 1024
#define FVR_FACTOR                  1048576

#define LED_FLICKER_700_MS          7

// Dim Duty Cycle (x/250)
#define LED_GREEN_DIM_PWM           15

// For Red PWM if Amber Pulse selected to reduce amount of Red
//  compared to the Green PWM on time
#define LED_AMBER_DIM_PWM           10       


// Message Sequence States
#define MSG_SEQ_1                   1
#define MSG_SEQ_2                   2
#define MSG_SEQ_3                   3

#define LED_PROFILE_LENGTH  (uchar)65
#define LED_PROFILE_FIRST   (uchar)1
#define LED_PROFILE_LAST    (uchar)63



/*
|-----------------------------------------------------------------------------
| Typedef and Enums and Constants
|-----------------------------------------------------------------------------
*/

#ifdef CONFIGURE_UNDEFINED
    // For Reference only, defined in led.h
    enum
    {
        STATE_LED_INIT = 0,
        STATE_LED_LB_CAL = 1,
        STATE_LED_CAL_BLINKING = 2,
        STATE_LED_RESET_PROFILE = 3,
        STATE_LED_NORM_BLINKING = 4,
        STATE_LED_SMK_CAL_INIT = 5,
        STATE_LED_SMK_CAL = 6,
        STATE_LED_AMB_5_SEC_BLINK = 7,
        STATE_LED_SMK_HUSH_MODE = 8,
        STATE_LED_ALM_MEMORY = 9,
        STATE_LED_WL_JOIN = 10,
        STATE_LED_WL_JOIN_RFD = 11,
        STATE_LED_WL_SEARCH = 12,
        STATE_LED_WL_OOB = 13,
        STATE_LED_WL_DETECTION_ALERT = 14,
        STATE_LED_WL_SHORT_ALERT = 15,
        STATE_LED_WL_PANIC_ALERT = 16,
        STATE_LED_WL_IDENTIFY = 17,
        STATE_LED_BLINK_SIZE = 18,
        STATE_LED_BLINK_ERR_CODE = 19,
        STATE_LED_NETWORK_CLOSED = 20,
        STATE_LED_NETWORK_NOT_FOUND = 21,
        STATE_LED_LOW_BATTERY = 22,
        STATE_LED_GREEN_FADE_ON = 23,
        STATE_LED_GREEN_FADE_OFF = 24

    };
#endif

// Defined Blink States
enum
{
    STATE_LED_BLINK_ON = 0,
    STATE_LED_BLINK_OFF
};

// Defined Alert States
enum
{
    STATE_ALERT_HORN = 0,
    STATE_ALERT_DELAY,
    STATE_ALERT_VOICE,
    STATE_ALERT_END
};

// Define LED PWM Profile States
enum
{
    STATE_PROFILE_FADE_ON = 0x00,
    STATE_PROFILE_FADE_OFF = 0x01,
    STATE_PROFILE_INTERVAL = 0x02,
    STATE_PROFILE_FADE_ON_2 = 0x03,
    STATE_PROFILE_FADE_OFF_2 = 0x04,
    STATE_PROFILE_INTERVAL_2 = 0x05

};

//
// PWM Profile Duty cycle Pulses can range from 0 - 250
// 16uSec resolution  (16 x 250 = 4.000ms)
// So LED Full On = 250, LED Full Off = 0
// Each Interval count represents 1 ms of time

const LED_PWM_Profile_t PRO_FADE_LED1[LED_PROFILE_LENGTH]=
{
    // Array of PWM structures
    //          (UCHAR ,              UCHAR,             UINT)
    // Red PWM On Pulse, Green PWM On Pulse, UINT PWM Interval (in ms)
    LED_PROFILE_END,LED_PROFILE_END,0,
    0, 0, 20,
    1, 1, 50,
    2, 2, 50,
    3, 3, 50,
    4, 4, 40,
    6, 6, 40,
    8, 8, 40,
    10, 10, 40,
    12, 12, 40,
    15, 15, 40,
    18, 18, 40,
    21, 21, 40,
    25, 25, 40,
    30, 30, 40,
    35, 35, 40,
    40, 40, 30,
    45, 45, 30,
    50, 50, 30,
    55, 55, 30,
    60, 60, 30,
    65, 65, 30,
    70, 70, 30,
    75, 75, 30,
    82, 82, 30,
    90, 90, 30,
    98, 98, 30,
    105, 105, 30,
    115, 115, 30,
    125, 125, 30,
    132, 132, 20,
    140, 140, 20,
    145, 145, 20,
    150, 150, 15,
    155, 155, 15,
    160, 160, 15,
    165, 165, 15,
    170, 170, 15,
    175, 175, 15,
    180, 180, 15,
    185, 185, 15,
    190, 190, 15,
    195, 195, 15,
    200, 200, 15,
    200, 200, 15,
    205, 205, 15,
    205, 205, 15,
    207, 207, 15,
    210, 210, 10,
    215, 215, 10,
    220, 220, 10,
    225, 225, 10,
    230, 230, 10,
    235, 235, 10,
    240, 240, 10,
    245, 245, 10,
    250, 250, 5,
    250, 250, 5,
    250, 250, 5,
    250, 250, 5,
    250, 250, 5,
    250, 250, 5,
    250, 250, 5,
    250, 250, 5,
    LED_PROFILE_END,LED_PROFILE_END,0
};


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
static uchar profile_state;
static uchar profile_int_count;
static uchar cycle_count;

static uchar led_alm_memory_timer;

volatile uint led_pwm_dwell_count_red = 0;
volatile uint led_pwm_dwell_count_grn = 0;

// Local Variables
static volatile uchar rled_state = STATE_LED_INIT;
static volatile uchar gled_state = STATE_LED_INIT;
static volatile uchar aled_state = STATE_LED_INIT;

#ifdef CONFIGURE_DIAG_GRN_LED_STATES    
    // Test Only
    static volatile uchar gled_prev_state = 0xFF;
#endif
    
#ifdef CONFIGURE_DIAG_RED_LED_STATES
    static volatile uchar rled_prev_state = 0xFF;
#endif
    
#ifdef CONFIGURE_DIAG_AMB_LED_STATES    
    // Test Only
    static volatile uchar aled_prev_state = 0xFF;
#endif
    

static uchar led_blink_state = STATE_LED_BLINK_ON;
static uchar led_blink_count;

#ifdef CONFIGURE_WIRELESS_MODULE
    static uchar Local_Count;
    
    #ifdef CONFIGURE_VOICE
        static uchar voice_seq_state;
    #endif

    static uint  id_mode_timer;
    static uchar led_alert_timer;
    static uchar led_alert_count;
    static uchar alert_state;

#ifndef CONFIGURE_WIFI    
    static uchar voice_ping_timer;
#endif
    
    static uchar led_timeout_timer;
    
#endif
    


/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/

void led_pwm_on(void);
void led_pwm_off(void);
void led_select_pps(void);
void led_select_latch(void);
void led_set_profile(uchar id);

#ifdef CONFIGURE_WIRELESS_MODULE
    void led_process_timeout_timer(uchar timeout);
    void led_start_timeout_timer(void);
#endif

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/
    
#ifdef CONFIGURE_DIAG_GRN_LED_STATES 
    // Test Only
    uchar GLED_Get_State(void)
    {
        return gled_state;
    }
#endif    

#ifdef CONFIGURE_DIAG_RED_LED_STATES 
    // Test Only
    uchar RLED_Get_State(void)
    {
        return rled_state;
    }
#endif   
    
#ifdef CONFIGURE_DIAG_AMB_LED_STATES 
    // Test Only
    uchar ALED_Get_State(void)
    {
        return aled_state;
    }
#endif    


/*
+------------------------------------------------------------------------------
| Function:      GLED_Manager_Task
+------------------------------------------------------------------------------
| Purpose:       Handles Green LED State Control and Priorities
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Green LED Task Next Execution Time (in Task TICs)
+------------------------------------------------------------------------------
*/
uint GLED_Manager_Task(void)
{
    #ifdef DBUG_SERIAL_OUTPUT_GLED_STATE
        // Test Only
        SER_Send_String("GLED Stat");
        SER_Send_Int('e', gled_state, 10);
    #endif

    #ifdef CONFIGURE_DIAG_GRN_LED_STATES
        if(gled_prev_state != gled_state)
        {
            // Test Only
            SER_Send_String("\rGLED Stat");
            SER_Send_Int('e', gled_state, 10);
            SER_Send_String("\r");
            gled_prev_state = gled_state;
        }
    #endif

    switch (gled_state)
    {
        case STATE_LED_INIT:
        {
            
            // Hang in this state until Smoke Cal is completed
            if(!FLAG_NO_SMOKE_CAL && !FLAG_LOW_BATTERY_CAL_MODE)
            {
                // Check CO Cal Status State
                gled_state = STATE_LED_CAL_BLINKING;
                
                // Hold Off Power Indications until Green LED Power profile
                FLAG_PWR_LED_OFF = 1;

                return  MAIN_Make_Return_Value(LED_BLINK_500_MS);

            }
        }
        break;

        case STATE_LED_CAL_BLINKING:
            
            #ifdef CONFIGURE_CO
                // This applies only to CO Models
                if(CAL_STATE_CALIBRATED == CO_Cal_Get_State() )
                {

                    // Voice message for Power On Chime
                    #ifdef CONFIGURE_VOICE
                        // Announce Power On Chime  changed to PTT Sound
                        VCE_Play(VCE_SND_PUSH_TO_TEST);
                    #endif

                    #ifdef CONFIGURE_CANADA
                        // If English Voice is selected, announce
                        //  Press Button for French message on Power Reset
                        if(!FLAG_VOICE_FRENCH && !FLAG_RST_SOFT)
                        {
                            VCE_Play(VCE_ENGLISH_SELECTED);
                            VCE_Play(VCE_FR_FOR_FRENCH_PUSH_BTN);
                        }
                    #endif
                       
                    // Init Green LED Profile at Power Reset

                    // Begin Profile in 500 ms
                    profile_state = STATE_PROFILE_FADE_ON;
                    cycle_count = 1;
                    gled_state = STATE_LED_RESET_PROFILE;

                    // This is cleared when profile is disabled
                    FLAG_LED_PROFILE_BUSY = 1;

                }
                else if( (CAL_STATE_WAITING == CO_Cal_Get_State() ) ||
                          (CAL_STATE_WAIT_DELAY == CO_Cal_Get_State() )  )
                {
                    GLED_Off();

                    SER_Send_String("Wait");

                }
                // We're in a Calibration State, Determine action
                else if( CAL_STATE_FINAL_COMPLETE == CO_Cal_Get_State() )
                {
                    // If in 400 PPM verification, Green LED is solid ON
                    GLED_On();
                }
                // We're in a Calibration State, Determine action
                else if( CAL_STATE_VERIFY == CO_Cal_Get_State() )
                {
                    // If in 400 PPM verification, Green LED is solid ON
                    GLED_On();
                }
                else if( CAL_STATE_ZERO == CO_Cal_Get_State())
                {

                    // Fast Green LED Blink
                    if(FLAG_GREEN_LED_ON)
                    {
                        FLAG_GREEN_LED_ON = 0;
                    }
                    else
                    {
                        FLAG_GREEN_LED_ON = 1;
                    }
                    return  MAIN_Make_Return_Value(LED_BLINK_50_MS);

                }
                else if(CAL_STATE_150 == CO_Cal_Get_State())
                {
                    // 150 Cal Blinking
                    FLAG_GREEN_LED_BLINK = 1;
                    return  MAIN_Make_Return_Value(LED_BLINK_500_MS);
                }
            #else

                // Voice message for Power On Chime
                #ifdef CONFIGURE_VOICE
                    // Announce Power On Chime  changed to PTT Sound
                    VCE_Play(VCE_SND_PUSH_TO_TEST);
                #endif

                #ifdef CONFIGURE_CANADA
                    // If English Voice is selected, announce
                    //  Press Button for French message on Power Reset
                    if(!FLAG_VOICE_FRENCH && !FLAG_RST_SOFT)
                    {
                        VCE_Play(VCE_ENGLISH_SELECTED);
                        VCE_Play(VCE_FR_FOR_FRENCH_PUSH_BTN);
                    }
                #endif

                // Init Green LED Profile at Power Reset

                // Begin Profile in 500 ms
                profile_state = STATE_PROFILE_FADE_ON;
                cycle_count = 1;
                gled_state = STATE_LED_RESET_PROFILE;

                // This is cleared when profile is disabled
                FLAG_LED_PROFILE_BUSY = 1;

            #endif
        break;

        case STATE_LED_RESET_PROFILE:
            {

                // We are in Power Reset LED Profile
                if(!FLAG_LED_PRO_N_PROGRESS)
                {
                    switch(profile_state)
                    {
                        case STATE_PROFILE_FADE_ON:
                        {
                            led_set_profile(LED_PRO_FADE_ON_GRN_1);
                            profile_state = STATE_PROFILE_FADE_OFF;
                        }
                        break;

                        case STATE_PROFILE_FADE_OFF:
                        {
                            led_set_profile(LED_PRO_FADE_OFF_GRN_1);
                            profile_state = STATE_PROFILE_INTERVAL;
                            profile_int_count = LED_PROF_INT_500_MS;
                        }
                        break;

                        case STATE_PROFILE_INTERVAL:
                        {
                            if(0 == --profile_int_count)
                            {
                                if(0 == --cycle_count)
                                {
                                    // Set LED Profiles Off, Profile Busy Flag
                                    // will be cleared
                                    led_set_profile(LED_PRO_NONE);

                                    // Search Mode is inactive or we entered 
                                    // Alarm Abort this State
                                    GLED_Off();
                                    gled_state = STATE_LED_NORM_BLINKING;

                                    FLAG_PWR_LED_OFF = 0;
                                    return MAIN_Make_Return_Value
                                            (LED_BLINK_2_SEC);                                
                                }
                                else
                                {
                                    profile_state = STATE_PROFILE_FADE_ON;
                                }
                            }
                        }
                        break;

                    }
                }
            }
            break;
        
        case STATE_LED_NORM_BLINKING:
            FLAG_GLED_NOT_IDLE = 0;
            
            // Monitor Operational Modes to determine Green LED actions if any
            if(FLAG_PTT_ACTIVE)
            {
                // No LED Blink Control during PTT
                break;
            }

            if( (FLAG_LED_PROFILE_BUSY || FLAG_APP_DISPLAY_NETWORK_SIZE ) &&
                 !FLAG_APP_SEARCH_MODE )
            {
                // No LED Blink Control if other LED Activity that
                //  we do not want to interrupt
                
                // Turn off Green LED and activate Power Indicator delay
                //  for when Profile completes
                FLAG_LED_GRN_PWR_DELAY = 1;
                
                GLED_Off();
                return MAIN_Make_Return_Value(LED_BLINK_1_SEC);
            }

           
            #ifdef CONFIGURE_WIRELESS_MODULE
                if( FLAG_FAULT_FATAL || FLAG_NETWORK_ERROR_MODE ||
                    FLAG_CO_ALARM_ACTIVE || FLAG_SMOKE_ALARM_ACTIVE )
                {
                    // Bypass Join/Search profiles if Fault Mode or Alarm
                }
                else if(FLAG_APP_SEARCH_MODE || FLAG_FORCE_INTO_SEARCH)
                {
                    #ifdef CONFIGURE_WIFI
                        if(FLAG_AC_DETECT)
                        {
                            GLED_Off();

                            // Init Search Mode End blink flicker count
                            Local_Count = LED_FLICKER_700_MS;

                            // Notify other LED modules to wait for flicker 
                            //  to complete

                            // Not Used
                            //FLAG_GLED_SEARCH_MONITOR = 1;

                            gled_state = STATE_LED_WL_SEARCH;
                            FLAG_GLED_NOT_IDLE = 1;
                            break;
                        }
                    #else
                        GLED_Off();

                        // Init Search Mode End blink flicker count
                        Local_Count = LED_FLICKER_700_MS;

                        // Notify other LED modules to wait for flicker 
                        //  to complete

                        // Not Used
                        //FLAG_GLED_SEARCH_MONITOR = 1;

                        gled_state = STATE_LED_WL_SEARCH;
                        FLAG_GLED_NOT_IDLE = 1;
                        break;
                    #endif
                }
                else if(!FLAG_APP_JOIN_MODE)
                {
                    // If not still in Join Mode at this point after join was
                    //  interrupted, clear Join Initiating flag
                    FLAG_JOIN_OPEN_INITIATING = 0;
                }
            
            #ifndef CONFIGURE_WIFI
                // No Coordinators in WiFi
            
                else if( FLAG_APP_JOIN_MODE && FLAG_COMM_COORDINATOR && 
                         !FLAG_APP_DISPLAY_NETWORK_SIZE )
                {
                    // Begin any LED Profiles in 500 ms
                    profile_state = STATE_PROFILE_INTERVAL;
                    profile_int_count = LED_PROF_INT_500_MS;
                    FLAG_LED_PROFILE_BUSY = 1;

                    #ifdef CONFIGURE_VOICE
                        if(FLAG_JOIN_OPEN_INITIATING)
                        {
                            // JOIN Initiating still set from previous
                            //  interruption, Do not announce entry voice
                            //  again
                            
                            // Continue Sonar Pings in 1 second
                            voice_ping_timer = LED_PING_TIMER_1_SEC;   
                        }
                        // Only the initiating unit will announce
                        //  network open messages
                        else if(FLAG_REQ_VOICE_JOIN_COORD)
                        {
                            FLAG_REQ_VOICE_JOIN_COORD = 0;
                            
                            // Enable Sonar Pings while in Join
                            FLAG_JOIN_OPEN_INITIATING = 1;
                            
                            // Begin in 3 seconds
                            voice_ping_timer = LED_PING_TIMER_3_SEC;   
                            
                            // Announce Voice Messages upon State Entry
                            FLAG_ANNOUNCE_GRN_VOICE =1;
                            voice_seq_state = MSG_SEQ_1;    // starting msg
                        }
                    
                    #endif

                    // This is a Coordinator Join State
                    gled_state = STATE_LED_WL_JOIN;
                    FLAG_GLED_NOT_IDLE = 1;
                    break;
                }
            #endif
            
                #ifdef CONFIGURE_GLED_RFD_JOIN_MODE
                    else if( (FLAG_APP_JOIN_MODE) && (FLAG_COMM_RFD) && 
                              (!FLAG_APP_DISPLAY_NETWORK_SIZE) )
                    {
                       // Begin any LED Profiles in 500 ms
                        profile_state = STATE_PROFILE_INTERVAL;
                        profile_int_count = LED_PROF_INT_500_MS;
                        FLAG_LED_PROFILE_BUSY = 1;

                    #ifdef CONFIGURE_VOICE

                        // Do not need Join Initiating for WiFi
                        #ifndef CONFIGURE_WIFI
                            if(FLAG_JOIN_OPEN_INITIATING)
                            {
                                // JOIN Initiating still set from previous
                                //  interruption, Do not announce entry voice
                                //  again

                                // Continue Sonar Pings in 1 second
                                voice_ping_timer = LED_PING_TIMER_1_SEC;   
                            }
                            // Only the initiating unit will announce
                            //  network open messages
                            else if(FLAG_REQ_VOICE_JOIN_RFD) 
                            {
                                FLAG_REQ_VOICE_JOIN_RFD = 0;

                                // Enable Sonar Pings while in Join
                                FLAG_JOIN_OPEN_INITIATING = 1;

                                // Begin in 3 seconds
                                voice_ping_timer = LED_PING_TIMER_3_SEC;   

                                // Announce Voice Messages upon State Entry
                                FLAG_ANNOUNCE_GRN_VOICE =1;
                                voice_seq_state = MSG_SEQ_1;  
                            }
                            else
                            {
                                FLAG_JOIN_OPEN_INITIATING = 0;
                            }
                        #endif

                    #endif

                        // Init Join Mode End blink flicker count
                        Local_Count = LED_FLICKER_700_MS;

                        // This is a RFD Join State
                        gled_state = STATE_LED_WL_JOIN_RFD;
                        FLAG_GLED_NOT_IDLE = 1;
                        
                        // Reset Stand Alone indicator when entering Join
                        // This will get set again if Join not successful
                        FLAG_STAND_ALONE_MODE = 0;
                        
                        break;
                    }
                #endif
            
            #endif

            // Functional CO Test is currently disabled in this Model
            #ifdef CONFIGURE_CO_FUNC_TEST
                if(FLAG_FUNC_CO_TEST)
                {
                    // Blink every second
                    FLAG_GREEN_LED_BLINK = 1;
                    return MAIN_Make_Return_Value(LED_BLINK_1_SEC);

                }
            #endif

            #ifdef CONFIGURE_INTERCONNECT
                // Indicate Initiating LED Blinks if alarm Condition
                //   detected locally
                if(FLAG_BAT_CONSRV_MODE_ACTIVE && !FLAG_MASTER_SMOKE_ACTIVE)
                {
                    // Initiating Alarm Indication for Detected Smoke
                    //   or CO but Off during CO Bat Conserve
                }
                else
                {
                    if(FLAG_CO_ALARM_CONDITION || FLAG_MASTER_SMOKE_ACTIVE )
                    {
                        // This is the initiating alarm indication
                        // Set Green LED state OFF and Blink every second
                        GLED_Off();
                        FLAG_GREEN_LED_BLINK = 1;
                        return MAIN_Make_Return_Value(LED_BLINK_1_SEC);
                    }
                }
            
            #else

                // Non-Interconnect but with Wireless Option
            
                #ifdef CONFIGURE_WIRELESS_MODULE
                    // Indicate Initiating LED Blinks if alarm Condition
                    //   detected locally
                    if(FLAG_BAT_CONSRV_MODE_ACTIVE && !FLAG_MASTER_SMOKE_ACTIVE)
                    {
                        // Initiating Alarm Indication for Detected Smoke
                        //   or CO but Off during CO Bat Conserve
                        
                    }
                    else
                    {
                        // Indicate a Detected Source of Smoke or CO
                        if(FLAG_CO_ALARM_CONDITION || FLAG_MASTER_SMOKE_ACTIVE)
                        {
                            // This is the initiating alarm indication
                            // Set Green LED state OFF and Blink every second
                            GLED_Off();
                            FLAG_GREEN_LED_BLINK = 1;
                            return MAIN_Make_Return_Value(LED_BLINK_1_SEC);
                        }
                    }
                #endif
            #endif

            if(FLAG_SMOKE_HUSH_ACTIVE && !FLAG_PTT_ACTIVE)
            {
                // This is Smoke Hush Active
                // Keep Green LED OFF until Smoke Hush ends
                GLED_Off();
                return MAIN_Make_Return_Value(LED_BLINK_1_SEC);
            }

            if(FLAG_LED_GRN_PWR_DELAY)
            {
                // Delay Start of Power indication after Profile Ends
                FLAG_LED_GRN_PWR_DELAY = 0;
                GLED_Off();
                
                return MAIN_Make_Return_Value(LED_BLINK_3_SEC);
            }
            
            // Monitor Conditions that should inhibit Green LED Power
            // indications (i.e. fault and error blinks)
            if( FLAG_NETWORK_ERROR_MODE || FLAG_FAULT_FATAL || 
                FLAG_REQ_ERROR_CODE_BLINK || FLAG_ERROR_CODE_BLINKING ||
                FLAG_EOL_FATAL || FLAG_EOL_MODE || FLAG_EOL_HUSH_ACTIVE ||
                FLAG_NETWORK_ERROR_HUSH_ACTIVE || 
                FLAG_CO_ALARM_ACTIVE || FLAG_SMOKE_ALARM_ACTIVE ||
                FLAG_LOW_BATTERY || FLAG_LB_HUSH_ACTIVE ||
                FLAG_ALERT_PANIC || FLAG_ALERT_WEATHER || FLAG_ALERT_DETECT ||   
                FLAG_ALARM_IDENTIFY || FLAG_BAT_CONSRV_MODE_ACTIVE )
            {
                // No Green Power indications allowed while above condition
                //  is active
                GLED_Off();
                return  MAIN_Make_Return_Value(LED_BLINK_5_SEC);
            }
            #ifdef CONFIGURE_SPECIAL_LIGHT_TEST_FW
                else if (FLAG_LED_RING_OFF)
                {
                    // No Green Power indications allowed while above condition
                    //  is active
                    GLED_Off();
                    return  MAIN_Make_Return_Value(LED_BLINK_5_SEC);
                }
                #endif
            else
            {
                // Anytime AC is Detected and Green LED is OFF
                //  provide a Fade ON State request for Green Power Indication
                //  unless certain conditions are active
                if( FLAG_AC_DETECT && !FLAG_GREEN_FADE_ON && 
                    !FLAG_GREEN_LED_ON && !FLAG_LED_DIM &&
                    !FLAG_LGT_MEAS_CYCLE )
                {
                    // Fade Back On instead of immediately ON
                    FLAG_GREEN_FADE_ON = 1;
                }
                
                if( FLAG_AC_DETECT && !FLAG_GREEN_FADE_OFF && 
                    FLAG_GREEN_LED_ON && FLAG_LED_DIM )
                {
                    // Fade OFF at AC Power Dim Detect
                    FLAG_GREEN_FADE_OFF = 1;
                }
                
                #ifdef CONFIGURE_LGT_FADE_OFF_TO_MEASURE
                    // A Light Measurement needs to Fade OFF the Light Ring
                    if( FLAG_AC_DETECT && FLAG_GREEN_LED_ON &&
                        FLAG_LGT_MEAS_CYCLE && !FLAG_GREEN_FADE_OFF )
                    {
                        // Fade OFF for a Light Measurement
                        FLAG_GREEN_FADE_OFF = 1;
                    }
                #endif
                
                // Before Green Power Indication Control
                // 1st Check to see if a Power Fade transition is needed
                if(FLAG_GREEN_FADE_ON && !FLAG_LED_DIM)
                {
                    // Begin Fade ON Profile
                    profile_state = STATE_PROFILE_FADE_ON;

                    // This is cleared when profile is disabled
                    FLAG_LED_PROFILE_BUSY = 1;
                    FLAG_GLED_NOT_IDLE = 1;
                    gled_state = STATE_LED_GREEN_FADE_ON;
                    return  MAIN_Make_Return_Value(LED_BLINK_100_MS);
                }
                else if(FLAG_GREEN_FADE_OFF)
                {
                    // Begin Fade OFF Profile
                    profile_state = STATE_PROFILE_FADE_OFF;

                    // This is cleared when profile is disabled
                    FLAG_LED_PROFILE_BUSY = 1;
                    FLAG_GLED_NOT_IDLE = 1;
                    gled_state = STATE_LED_GREEN_FADE_OFF;
                    return  MAIN_Make_Return_Value(LED_BLINK_100_MS);
                }
                
                // We must be in Standby Mode (check if AC or DC power)
                if(FLAG_AC_DETECT)
                {
                    #ifndef CONFIGURE_LGT_SENSOR_PWR_IND
                        if(!FLAG_PWR_LED_OFF)  
                        {
                            GLED_On();
                        }
                        else
                        {
                            GLED_Off();
                            return  MAIN_Make_Return_Value(LED_BLINK_5_SEC);
                        }
                    #else
                        // If Not Dim Active, then Power Indicator is LED Ring
                        if(!FLAG_LED_DIM)
                        {
                            FLAG_BLINK_LIGHT_SENS_LED = 0;
                            
                            // There are times we can't light the Green LEDs
                            if(!FLAG_PWR_LED_OFF && !FLAG_LGT_MEAS_CYCLE) 
                            {
                                // Green ON continuously
                                GLED_On();
                            }
                            else
                            {
                                // PWR LED must be turned OFF
                                GLED_Off();
                                
                                return MAIN_Make_Return_Value(LED_BLINK_5_SEC);
                            }
                        }
                        else
                        {
                            GLED_Off(); // No Green LEDs on at Light Pipe
                            
                            // Flash Light Sensor LED next light measurement
                            FLAG_BLINK_LIGHT_SENS_LED = 1;
                            
                            // Increased to a 60 second blink
                            return  MAIN_Make_Return_Value(LED_BLINK_60_SEC);
                        }
                    #endif
                }
                else
                {
                    // Alarm running under Battery Power
                    GLED_Off();

                    if(!FLAG_POWERUP_DELAY_ACTIVE)
                    {
                        if(!FLAG_MODE_ACTIVE)
                        {
                            if(FLAG_LIGHT_MEAS_ACTIVE)
                            {
                                // Postpone Blink until Low Power Mode Light 
                                //  Sample completed
                                return MAIN_Make_Return_Value
                                                    (LED_BLINK_10_SEC);
                            }
                            else
                            {
                                // Battery Powered Low Power Standby Blink 
                                //  Execute LP Blink Now
                                LED_LP_green_blink();
                            }

                        }
                        else
                        {
                            // DC Powered but in Active Mode
                            #ifndef CONFIGURE_LGT_SENSOR_PWR_IND
                                if(!FLAG_PWR_LED_OFF)
                                {
                                    // Blink ring every 60 seconds
                                    //FLAG_GREEN_LED_BLINK = 1;
                                    
                                    // Short Blink Light Pipe Ring 
                                    GREEN_LED_ON
                                    for(uint i = 0; i < 1000; i++)
                                    {
                                        // Delay ~ 2.5 usec/count
                                    }
                                    GREEN_LED_OFF
                                }
                            #else
                                if(!FLAG_LED_DIM)
                                {
                                    if(!FLAG_PWR_LED_OFF)
                                    {
                                        // Blink ring every 60 seconds
                                        //FLAG_GREEN_LED_BLINK = 1;

                                        // Short Blink Light Pipe Ring 
                                        GREEN_LED_ON
                                        for(uint i = 0; i < 1000; i++)
                                        {
                                            // Delay ~ 2.5 usec/count
                                        }
                                        GREEN_LED_OFF
                                    }
                                    
                                }
                                else
                                {
                                    // Flash Light Sensor LED next measurement
                                    FLAG_BLINK_LIGHT_SENS_LED = 1;
                                }
                            #endif

                        }
                        
                        return  MAIN_Make_Return_Value(LED_BLINK_60_SEC);
                    }
                }
            }

        break;

    #ifdef CONFIGURE_WIRELESS_MODULE

    #ifndef CONFIGURE_WIFI
        case STATE_LED_WL_JOIN:
        {
            #ifdef CONFIGURE_VOICE
                if(FLAG_ANNOUNCE_GRN_VOICE)
                {
                    // Execute Voice Message Sequence as
                    // previous voices are completed  FLAG_VOICE_BUSY = 0;
                    // Voice module controls FLAG_VOICE_BUSY

                    switch(voice_seq_state)
                    {
                        case MSG_SEQ_1:
                            #ifdef CONFIGURE_UNDEFINED
                                // No Sonar Pings until after Voice message
                                if(!FLAG_VOICE_BUSY)
                                {
                                    VCE_Play(VCE_SND_SONAR_PING);
                                    voice_seq_state = MSG_SEQ_2;
                                }
                            #else
                                // No Entry Sonar Ping
                                voice_seq_state = MSG_SEQ_2;
                            #endif
                        break;

                        case MSG_SEQ_2:
                            if(!FLAG_VOICE_BUSY)
                            {

                                VCE_Play(VCE_NETWORK_SEARCH);

                                // End Voice Sequence
                                FLAG_ANNOUNCE_GRN_VOICE = 0;  
                                voice_seq_state = MSG_SEQ_1;
                            }
                        break;

                        default:
                            // End Voice Sequence
                            FLAG_ANNOUNCE_GRN_VOICE = 0;     
                        break;

                    }
                }
            #else
                FLAG_ANNOUNCE_GRN_VOICE = 0;
            #endif

            if( FLAG_APP_JOIN_MODE &&
                !FLAG_APP_DISPLAY_NETWORK_SIZE &&
                !FLAG_FAULT_FATAL &&
                !FLAG_CO_ALARM_ACTIVE && !FLAG_SMOKE_ALARM_ACTIVE &&
                !FLAG_SMOKE_HUSH_ACTIVE )
            {
                // We are in Wireless Join Mode
                if(FLAG_COMM_COORDINATOR)
                {
                    
                    // We are in Coord. Wireless Join Mode
                    if(!FLAG_LED_PRO_N_PROGRESS)
                    {
                       
                        switch(profile_state)
                        {
                            case STATE_PROFILE_FADE_ON:
                            {
                                led_set_profile(LED_PRO_FADE_ON_GRN_2);
                                profile_state = STATE_PROFILE_FADE_OFF;
                            }
                            break;

                            case STATE_PROFILE_FADE_OFF:
                            {
                                led_set_profile(LED_PRO_FADE_OFF_GRN_2);
                                profile_state = STATE_PROFILE_INTERVAL;

                                profile_int_count = LED_PROF_INT_500_MS;
                            }
                            break;

                            case STATE_PROFILE_INTERVAL:
                            {
                                if(0 == --profile_int_count)
                                {
                                    profile_state = STATE_PROFILE_FADE_ON;
                                }
                            }
                            break;
                        }
                    }
                }
                
                if(FLAG_JOIN_OPEN_INITIATING)
                {
                    // 2 Second Ping while in Join
                    if(0 == --voice_ping_timer)
                    {
                        voice_ping_timer = LED_PING_TIMER_2_SEC;

                        if(!FLAG_VOICE_BUSY)
                        {
                            VCE_Play(VCE_SND_SONAR_PING);
                        }
                    }
                }
            }
            else
            {
                // Trigger immediate CCI Test which also updates
                //  ID and # of Devices
                FLAG_CCI_TEST = 1;
                
                // Join Mode Must have ended, or is interrupted
                // Allow the Voice Sequence to Complete so it isn't cut off
                if(!FLAG_ANNOUNCE_GRN_VOICE)
                {
                    // Join Mode is inactive or we entered Alarm
                    // Exit this LED State
                    // Allow Profile to Complete 1st, then exit
                    if(!FLAG_LED_PRO_N_PROGRESS)
                    {
                        // Set LED Profiles Off
                        led_set_profile(LED_PRO_NONE);

                        // Search Mode is inactive or we entered Alarm
                        // Abort this State
                        GLED_Off();

                        if(!FLAG_APP_JOIN_MODE)
                        {
                            // Join Ended
                            // Proceed To Network Closed State
                            gled_state = STATE_LED_NETWORK_CLOSED;
                            
                            // Don't display returned Size from Join Close
                            FLAG_NET_SIZE_DISPLAY_OFF = 1;
                            
                            // Request Network Size at close
                            APP_Request_Net_Size();

                            FLAG_LED_GRN_PWR_DELAY = 1;
                            
                            #ifdef CONFIGURE_VOICE
                                // Announce Voice Messages upon State Entry
                                FLAG_ANNOUNCE_GRN_VOICE =1;
                                
                                // start with Sound
                                voice_seq_state = MSG_SEQ_1; 
                            #endif

                            // Reset Join Initiating if Join Ended    
                            FLAG_JOIN_OPEN_INITIATING = 0;      

                        }
                        else
                        {
                            // Join Mode still active but we need to
                            //  Interrupt this state for now. 
                            // Preserve Join Initiating if active
                            gled_state = STATE_LED_NORM_BLINKING;
                            
                        }
                    }
                }
            }
        }
        break;
    #endif

    #ifdef CONFIGURE_GLED_RFD_JOIN_MODE        
        case STATE_LED_WL_JOIN_RFD:
        {
            #ifdef CONFIGURE_VOICE
                if(FLAG_ANNOUNCE_GRN_VOICE)
                {
                    // Execute Voice Message Sequence as
                    // previous voices are completed  FLAG_VOICE_BUSY = 0;
                    // Voice module controls FLAG_VOICE_BUSY

                    switch(voice_seq_state)
                    {
                        case MSG_SEQ_1:
                            #ifdef CONFIGURE_UNDEFINED
                                // No Sonar Pings until after Voice message
                                if(!FLAG_VOICE_BUSY)
                                {
                                    VCE_Play(VCE_SND_SONAR_PING);
                                    voice_seq_state = MSG_SEQ_2;
                                }
                            #else
                                // No Entry Sonar Ping
                                voice_seq_state = MSG_SEQ_2;
                            #endif
                        break;

                        case MSG_SEQ_2:
                            if(!FLAG_VOICE_BUSY)
                            {

                                VCE_Play(VCE_NETWORK_SEARCH);

                                // End Voice Sequence
                                FLAG_ANNOUNCE_GRN_VOICE = 0;  
                                voice_seq_state = MSG_SEQ_1;
                            }
                        break;

                        default:
                            // End Voice Sequence
                            FLAG_ANNOUNCE_GRN_VOICE = 0;     
                        break;

                    }
                }
            #else
                FLAG_ANNOUNCE_GRN_VOICE = 0;
            #endif

                
            #ifdef CONFIGURE_WIFI
                if(!FLAG_AC_DETECT)
                {
                    if(FLAG_PREV_JOINED)
                    {
                        // First, If Stealth Search Active, clear Profile Active
                        FLAG_LED_PRO_N_PROGRESS = 0;
                    }
                    
                    // Lost WiFi Power; wait for profile to end then exit
                    if(!FLAG_LED_PRO_N_PROGRESS)
                    {
                        // Set LED Profiles Off
                        led_set_profile(LED_PRO_NONE);
                        
                        // WiFi Power Lost detected
                        // Stop Join Mode
                        FLAG_APP_JOIN_MODE = 0;
                        
                        // Clear Join in Progress (WiFi will resend))
                        FLAG_COMM_JOIN_IN_PROGRESS = 0;

                        // Green Power indication delay active
                        FLAG_LED_GRN_PWR_DELAY = 1;
                        
                        //
                        // Do not clear Previously Joined Flag
                        //  at AC Power Lost 
                        
                        // Stand Alone is set without WiFi
                        FLAG_STAND_ALONE_MODE = 1;

                        
                        // WiFi Power Lost, exit Search and Join modes
                        // End this State
                        gled_state = STATE_LED_NORM_BLINKING;
                        
                        FLAG_ACTIVE_TIMER = 1;
                        if(MAIN_ActiveTimer < TIMER_ACTIVE_MINIMUM_2_SEC)
                        {
                            // Extend time to remain in Active Mode
                            MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_2_SEC;
                        }
                    }
                    return  MAIN_Make_Return_Value(LED_BLINK_100_MS);
                }
            #endif

            if( FLAG_APP_JOIN_MODE &&
                !FLAG_APP_DISPLAY_NETWORK_SIZE &&
                !FLAG_FAULT_FATAL &&
                !FLAG_CO_ALARM_ACTIVE && !FLAG_SMOKE_ALARM_ACTIVE &&
                !FLAG_SMOKE_HUSH_ACTIVE )
            {
                // We are in Wireless RFD Join Mode
                if(FLAG_COMM_RFD)
                {
                  if( !FLAG_PREV_JOINED || FLAG_STEALTH_SEARCH_OFF )   
                  {
                    // We are in RFD Wireless Join Mode
                    if(!FLAG_LED_PRO_N_PROGRESS)
                    {
                        switch(profile_state)
                        {
                            case STATE_PROFILE_FADE_ON:
                            {
                                led_set_profile(LED_PRO_FADE_ON_GRN_2);
                                profile_state = STATE_PROFILE_FADE_OFF;
                            }
                            break;

                            case STATE_PROFILE_FADE_OFF:
                            {
                                led_set_profile(LED_PRO_FADE_OFF_GRN_2);
                                profile_state = STATE_PROFILE_INTERVAL;

                                // RFDs have a longer Dwell Time
                                profile_int_count = LED_PROF_INT_3_SEC;
                            }
                            break;

                            case STATE_PROFILE_INTERVAL:
                            {
                                if(0 == --profile_int_count)
                                {
                                    profile_state = STATE_PROFILE_FADE_ON;
                                }
                            }
                            break;
                        }
                    }
                  }
                  else
                  {
                    // Stealth Search Mode (no LED Profile output)
                    // Inhibit other profiles since we are in Join Mode
                    //  by setting Fake Profile Busy condition
                    led_set_profile(LED_PRO_NONE);
                    FLAG_LED_PRO_N_PROGRESS = 1;
                      
                  }
                }
            
                //
                //For WiFI we should be able to remove this Ping
                //
                #ifndef CONFIGURE_WIFI
                    // No PINGs during WiFi Join
                
                    if(FLAG_JOIN_OPEN_INITIATING)
                    {
                        // 2 Second Ping while in Join
                        if(0 == --voice_ping_timer)
                        {
                            voice_ping_timer = LED_PING_TIMER_2_SEC;
                            if(!FLAG_VOICE_BUSY)
                            {
                                VCE_Play(VCE_SND_SONAR_PING);
                            }
                        }
                    }
                #endif
            }
            else
            {
                #ifdef CONFIGURE_WIFI

                    if(FLAG_PREV_JOINED && !FLAG_STEALTH_SEARCH_OFF)
                    {
                        // Exiting Stealth Search Mode
                        // reset LED Profile Busy since it was forced to Busy State
                        // It is not forced when Stealth Off is active
                        FLAG_LED_PRO_N_PROGRESS = 0;
                    }
                
                    //
                    // WiFI - Join Completed and Connected to the Cloud
                    //
                    // 1.  Wait for profile to end
                    // 2.  Execute Success Flicker if ONLINE
                    // 3.  Announce "Success, Now Connected" voice message
                    // 4.  Return to Green Led Standby state
                    // 5.  If not ONLINE - return to standby
                    
                    if(!FLAG_LED_PRO_N_PROGRESS)
                    {
                        // Join Profile Ended
                        
                        // Set LED Profiles Off
                        led_set_profile(LED_PRO_NONE);
                        
                        // Test for a successful Join or did we abort
                        // Join without connecting to Cloud?
                        if(FLAG_COMM_ONLINE)
                        {
                            if(0 == Local_Count)  //Green Flicker Count
                            {

                                // No Voices in Stealth Join
                                if( !FLAG_PREV_JOINED || 
                                        FLAG_STEALTH_SEARCH_OFF )
                                {
                                    // RFD Success
                                    if(!FLAG_VOICE_BUSY && !FLAG_VOICE_ACTIVE)
                                    {
                                        VCE_Play(VCE_SND_DEV_ENROLLED);
                                        VCE_Play(VCE_SUCCESS);
                                        VCE_Play(VCE_NOW_CONNECTED);
                                    }
                                    else
                                    {
                                        // wait for Voice Clear
                                        return  MAIN_Make_Return_Value
                                                    (LED_BLINK_100_MS);
                                    }
                                }

                                // Green Power indication delay active
                                FLAG_LED_GRN_PWR_DELAY = 1;
                                
                                // Set the Previously Joined Flag
                                FLAG_PREV_JOINED = 1;
                                FLAG_STAND_ALONE_MODE = 0;
                                

                                // Join Mode is Over
                                gled_state = STATE_LED_NORM_BLINKING;

                            }
                            else
                            {
                                // Insure no Red LED Blinks during 
                                //  Green Flickering
                                RLED_Off();     

                                // Flickering to indicate RF Module Join has 
                                // completed and Cloud is connected
                                if(FLAG_GREEN_LED_ON)
                                {
                                    GLED_Off();
                                    Local_Count--;
                                }
                                else
                                {
                                    GLED_On();
                                }
                            }
                        }
                        else
                        {
                            //Cloud Join did not complete successfully
                            
                           // Join terminated 
                           gled_state = STATE_LED_NORM_BLINKING; 

                            if(FLAG_LED_OOB_PROFILE_ENABLE)
                            {
                                // OOB Reset was detected
                                
                                //
                                // Allow Re-Join Request after OOB reset
                                //  for WiFi
                                //
                                FLAG_OOB_RST_WITH_NO_REJOIN = 0;
                                FLAG_STAND_ALONE_MODE = 0;
                            }
                            else if(FLAG_APP_SEARCH_MODE)
                            {
                                //
                                // If Join was aborted to re-enter WiFi Search
                                //  do not clear Previously Joined status yet
                                //
                            }
                            else
                            {
                                // We did not complete Join to cloud so clear 
                                // Previously Joined
                                // Announce Voice 
                                 #ifdef CONFIGURE_VOICE
                                    //
                                    // Announce "not Connected" unless Stealth 
                                    //
                                    if(!FLAG_PREV_JOINED)
                                    {
                                        VCE_Play(VCE_NOT_CONNECTED);
                                    }
                                 #endif

                                #ifdef CONFIGURE_UNDEFINED
                                    //
                                    // Clear Previous connection 
                                    // Changed to only clear PREV_JOINED at OOB
                                    //  reset
                                    //
                                    FLAG_PREV_JOINED = 0;
                                #endif

                                // Set Standalone Mode since join was not 
                                // fully completed
                                FLAG_STAND_ALONE_MODE = 1;
                            }
                        }
                    }
                    return  MAIN_Make_Return_Value(LED_BLINK_50_MS);
                    
                #else
                    // Non-WiFI
                    
                    // Trigger immediate CCI Test which also updates
                    //  ID and # of Devices
                    FLAG_CCI_TEST = 1;


                    // Join Mode Must have ended, or is interrupted
                    // Allow the Voice Sequence to Complete so it isn't cut off
                    if(!FLAG_ANNOUNCE_GRN_VOICE)
                    {
                        // Join Mode is inactive or we entered Alarm
                        // Exit this LED State
                        // Allow Profile to Complete 1st, then exit
                        if(!FLAG_LED_PRO_N_PROGRESS)
                        {
                            // Set LED Profiles Off
                            led_set_profile(LED_PRO_NONE);

                            // Search Mode is inactive or we entered Alarm
                            // Abort this State
                            GLED_Off();

                            if(!FLAG_APP_JOIN_MODE)
                            {
                                // Join Ended
                                // Proceed To Network Closed State
                                gled_state = STATE_LED_NETWORK_CLOSED;

                                // Don't display returned Size from Join Close
                                FLAG_NET_SIZE_DISPLAY_OFF = 1;    

                                // Request Network Size at close
                                APP_Request_Net_Size();

                                FLAG_LED_GRN_PWR_DELAY = 1;


                                #ifdef CONFIGURE_VOICE
                                    // Announce Voice Messages upon State Entry
                                    FLAG_ANNOUNCE_GRN_VOICE =1;
                                    voice_seq_state = MSG_SEQ_1; 
                                #endif

                                // Reset Join Initiating if Join Ended    
                                FLAG_JOIN_OPEN_INITIATING = 0;    
                            }
                            else
                            {
                                // Join Mode still active but we need to
                                //  Interrupt this state for now. 
                                // Preserve Join Initiating if active
                                gled_state = STATE_LED_NORM_BLINKING;
                            }
                        }
                    }
                #endif
            }
        }
        break;
    #endif

    #ifndef CONFIGURE_WIFI
        case STATE_LED_NETWORK_CLOSED:
        {
             #ifdef CONFIGURE_VOICE   
                if(FLAG_ANNOUNCE_GRN_VOICE)
                {
                    // Execute Voice Message Sequence as
                    // previous voices are completed  FLAG_VOICE_BUSY = 0;
                    // Voice module controls FLAG_VOICE_BUSY

                    switch(voice_seq_state)
                    {
                        case MSG_SEQ_1:

                            if(!FLAG_VOICE_BUSY)
                            {
                                // All units play Closed sound
                                VCE_Play(VCE_SND_NETWORK_CLOSE);

                                voice_seq_state = MSG_SEQ_2;
                            }
                        break;

                        case MSG_SEQ_2:

                            if(FLAG_JOIN_CLOSE_INITIATING)
                            {
                                if(!FLAG_VOICE_BUSY)
                                {
                                    #ifdef CONFIGURE_VOICE
                                        VCE_Play(VCE_SETUP_COMPLETE);
                                        if(APP_Rf_Network_Size > 1)
                                        {
                                            VCE_Announce_Number
                                                (APP_Rf_Network_Size);
                                            VCE_Play(VCE_DEVICES_CONNECTED);
                                        }
                                        else
                                        {
                                            VCE_Play(VCE_NO_DEVICES_FOUND);
                                        }
                                    #endif

                                    // End Voice Sequence        
                                    FLAG_ANNOUNCE_GRN_VOICE = 0;  
                                    voice_seq_state = MSG_SEQ_1;
                                    FLAG_JOIN_CLOSE_INITIATING = 0;
                                }
                            }
                            else
                            {
                                // End Voice Sequence        
                                FLAG_ANNOUNCE_GRN_VOICE = 0; 
                                voice_seq_state = MSG_SEQ_1;
                            }

                        break;

                        default:
                            // End Voice Sequence
                            FLAG_ANNOUNCE_GRN_VOICE = 0;     
                        break;

                    }
                }
                else
                {
                    // After Voice Sequence proceed to IDLE
                    gled_state = STATE_LED_NORM_BLINKING;
                }
            #else
                    FLAG_ANNOUNCE_GRN_VOICE = 0;

                    // No Voice Chip proceed to IDLE
                    gled_state = STATE_LED_NORM_BLINKING;

            #endif
        }
        break;
    #endif

        case STATE_LED_WL_SEARCH:
        {
            // Once we enter Search Mode we can Clear this Flag
            FLAG_FORCE_INTO_SEARCH = 0;
            
            // Wait for Network Search to End before Success Indicators
            if( !FLAG_APP_SEARCH_MODE && 
                !FLAG_LED_PRO_N_PROGRESS && !FLAG_LED_PROFILE_BUSY )
                    
            {
                // Search Mode Led Profile Should be Completed Here
                if( FLAG_COMM_RFD || FLAG_COMM_COORDINATOR ) 
                {
                    if( !FLAG_COMM_JOIN_COMPLETE &&
                        !FLAG_COMM_JOIN_IN_PROGRESS )
                    {
                        // Search ended but, Network Was Not Found
                        //  End this State (no voice or Green Flicker)
                        gled_state = STATE_LED_NORM_BLINKING;
                        
                        // Not Used
                        //FLAG_GLED_SEARCH_MONITOR = 0;

                    }
                    else if(0 == Local_Count)  //Green Flicker Count
                    {
                        
                        // Successful Network Search
                        #ifdef CONFIGURE_VOICE
                            // Only announce Voices/Sounds if this is 
                            //  an Enrollment Join from OOB (not re-joins)
                            if(!FLAG_PREV_JOINED)
                            {
                                if(FLAG_COMM_RFD)
                                {
                                    
                                /* 
                                 * BC:
                                 * For WiFi we do not want voice announce here
                                 * 
                                 * Flicker and proceed to Join States
                                 */
                                #ifndef CONFIGURE_WIFI
                                
                                    // RFD Success
                                    if(!FLAG_VOICE_BUSY && !FLAG_VOICE_ACTIVE)
                                    {
                                        VCE_Play(VCE_SND_DEV_ENROLLED);
                                        VCE_Play(VCE_SUCCESS);
                                        VCE_Play(VCE_NOW_CONNECTED);
                                    }
                                    else
                                    {
                                        // wait for Voice Clear
                                        return  MAIN_Make_Return_Value
                                                (LED_BLINK_100_MS);
                                    }
                                
                                #endif

                                }
                                else
                                {
                                    
                                // No Coordinator actions for WiFi    
                                #ifndef CONFIGURE_WIFI
                                    // Coordinator established
                                    if(!FLAG_VOICE_BUSY && !FLAG_VOICE_ACTIVE)
                                    {
                                        VCE_Play(VCE_SND_DEV_ENROLLED);
                                    }
                                    else
                                    {
                                        // wait for Voice Clear
                                        return  MAIN_Make_Return_Value
                                                (LED_BLINK_100_MS);
                                    }
                                #endif

                                }
                            }
                        #endif

                        // End this State
                        gled_state = STATE_LED_NORM_BLINKING;
                        
                        // Not Used
                        //FLAG_GLED_SEARCH_MONITOR = 0;
                        
                        #ifndef CONFIGURE_WIFI
                            //
                            // Set the Prev Joined Flag
                            // For WiFi, set Previously Joined after 
                            //  Online Connection

                            FLAG_PREV_JOINED = 1;
                        #endif

                        #ifndef CONFIGURE_WIFI
                            // Should not be needed in Search for WiFi
                            FLAG_STAND_ALONE_MODE = 0;
                        #endif
                        
                        //No Local network IDs for WiFi
                        #ifndef CONFIGURE_WIFI
                            // Init Local ID after a successful Join
                            APP_Request_Local_Id();
                        #endif
                        
                    }
                    else
                    {
                        // Terminate any Red LED Blinks during Green Flickering
                        RLED_Off();     

                        // Flickering to indicate RF Module Join has occurred
                        if(FLAG_GREEN_LED_ON)
                        {
                            GLED_Off();
                            Local_Count--;
                        }
                        else
                        {
                            GLED_On();
                        }
                    }
                }
                else 
                {
                    // Search ended but, Network Was Not Found
                    //  End this State (no voice or Green Flicker)
                    gled_state = STATE_LED_NORM_BLINKING;
                    
                    // Not Used
                    //FLAG_GLED_SEARCH_MONITOR = 0;
                    
                    //No Local network IDs for WiFi
                    #ifndef CONFIGURE_WIFI
                        // Init a Local ID based upon message request results
                        APP_Request_Local_Id();
                    #endif
                }
            }
            else
            {
                // Still in Search Mode
                
                // Test for Search Mode Early Exit Conditions
                if( FLAG_FAULT_FATAL ||
                    FLAG_CO_ALARM_ACTIVE || FLAG_SMOKE_ALARM_ACTIVE ||
                    FLAG_SMOKE_HUSH_ACTIVE )
                {
                    // End this State
                    gled_state = STATE_LED_NORM_BLINKING;
                    
                    // Not Used
                    //FLAG_GLED_SEARCH_MONITOR = 0;
                }
                else
                {
                    // We are in Network Search Mode
                    // (Hang here until search over)
                    Local_Count = LED_FLICKER_700_MS;
                }
            }
            return  MAIN_Make_Return_Value(LED_BLINK_50_MS);
        }
    #endif

        case STATE_LED_GREEN_FADE_ON:
        {
            // We are in Green Fade On Profile
            if(!FLAG_LED_PRO_N_PROGRESS)
            {
                switch(profile_state)
                {
                    case STATE_PROFILE_FADE_ON:
                    {
                        led_set_profile(LED_PRO_FADE_ON_GRN_3);
                        profile_state = STATE_PROFILE_INTERVAL;
                        profile_int_count = LED_PROF_INT_200_MS;
                    }
                    break;

                    case STATE_PROFILE_INTERVAL:
                    {
                        if(0 == --profile_int_count)
                        {
                            // Fade ON Completed
                            FLAG_GREEN_FADE_ON = 0;
                            FLAG_PWR_LED_OFF = 0;
                            GREEN_LED_ON
                            GLED_On();
                            
                            // Set LED Profiles Off, Profile Busy Flag
                            // will be cleared
                            led_set_profile(LED_PRO_NONE);

                            // Search Mode is inactive or we entered 
                            // Alarm Abort this State
                            gled_state = STATE_LED_NORM_BLINKING;

                            return MAIN_Make_Return_Value(LED_BLINK_100_MS);                                
                        }
                    }
                    break;

                }
            }
        }
        break;
        
        case STATE_LED_GREEN_FADE_OFF:
        {
            // We are in Green Fade Off Profile
            if(!FLAG_LED_PRO_N_PROGRESS)
            {
                switch(profile_state)
                {
                    case STATE_PROFILE_FADE_OFF:
                    {
                        led_set_profile(LED_PRO_FADE_OFF_GRN_3);
                        profile_state = STATE_PROFILE_INTERVAL;
                        profile_int_count = LED_PROF_INT_200_MS;
                    }
                    break;

                    case STATE_PROFILE_INTERVAL:
                    {
                        if(0 == --profile_int_count)
                        {
                            // Fade OFF Completed
                            FLAG_GREEN_FADE_OFF = 0;
                            FLAG_LED_GRN_PWR_DELAY = 1;
                            FLAG_PWR_LED_OFF = 0;
                            GREEN_LED_OFF
                            GLED_Off();
                                    
                            // Set LED Profiles Off, Profile Busy Flag
                            // will be cleared
                            led_set_profile(LED_PRO_NONE);

                            // Search Mode is inactive or we entered 
                            // Alarm Abort this State
                            gled_state = STATE_LED_NORM_BLINKING;

                            return MAIN_Make_Return_Value(LED_BLINK_100_MS);                                
                        }
                    }
                    break;

                }
            }
        }
        break;
        
        default:
            gled_state = STATE_LED_INIT;
            break;
    }
    return  MAIN_Make_Return_Value(LED_BLINK_100_MS);
}

/*
+------------------------------------------------------------------------------
| Function:      RLED_Manager_Task
+------------------------------------------------------------------------------
| Purpose:       Handles Red LED State Control and Priorities
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Red LED Task Next Execution Time (in Task TICs)
+------------------------------------------------------------------------------
*/

uint RLED_Manager_Task(void)
{
    #ifdef DBUG_SERIAL_OUTPUT_RLED_STATE
        // Test Only
        SER_Send_String("RLED Stat");
        SER_Send_Int('e', rled_state, 10);
    #endif

    #ifdef CONFIGURE_DIAG_RED_LED_STATES
        if(rled_prev_state != rled_state)
        {
            // Test Only
            SER_Send_String("\rRLED Stat");
            SER_Send_Int('e', rled_state, 10);
            SER_Send_String("\r");
            rled_prev_state = rled_state;
        }
    #endif

    switch (rled_state)
    {
        case STATE_LED_INIT:
        {
            // Check Cal Status State
            rled_state = STATE_LED_LB_CAL;
        }
        break;

        // Add Smoke Calibration Blinking in this state
        case STATE_LED_LB_CAL:
            if(FLAG_LOW_BATTERY_CAL_MODE)
            {
                //Handle LB Cal Red Led control
                if(FLAG_LB_CAL_STATUS)
                {
                    //LB Cal completed
                    RLED_Off();
                }
                else if(FLAG_LB_CAL_SIGNAL)
                {
                    //LB Cal in progress indication
                    RLED_On();
                }
                else
                {
                    // Make sure LED is Off for 1/2 second Blinking
                    // LB not yet calibrated indication
                    RLED_Off();
                    FLAG_RED_LED_BLINK = 1;
                }
                return  MAIN_Make_Return_Value(LED_BLINK_500_MS);
            }
            else
            {
                //Proceed to Smoke Cal test state
                rled_state = STATE_LED_CAL_BLINKING;
            }
        break;

        // Add Smoke Calibration Blinking in this state
        case STATE_LED_CAL_BLINKING:
            if(FLAG_NO_SMOKE_CAL)
            {
                led_blink_count = 26;
                //Proceed to Smoke Cal test state
                rled_state = STATE_LED_SMK_CAL_INIT;
            }
            else
            {
                //Proceed to Normal Blinking
                rled_state = STATE_LED_NORM_BLINKING;

                return  MAIN_Make_Return_Value(LED_BLINK_500_MS);

            }
        break;

        case STATE_LED_NORM_BLINKING:
        {
            FLAG_RLED_NOT_IDLE = 0;
            
            // If Alarm Mode, Sound Module controls LED
            if(FLAG_CO_ALARM_ACTIVE)
            {
                // Allow CO Alarm to Operate from Smoke Hush
                break;
            }
            // Monitor Smoke Alarm Hush Mode Active
            if(FLAG_SMOKE_HUSH_ACTIVE && !FLAG_PTT_ACTIVE)
            {
                
                #ifdef CONFIGURE_HUSH_LED_PROFILE
                    // Begin any LED Profiles in 500 ms
                    profile_state = STATE_PROFILE_INTERVAL;
                    profile_int_count = LED_PROF_INT_500_MS;
                    FLAG_LED_PROFILE_BUSY = 1;
                #endif

                rled_state = STATE_LED_SMK_HUSH_MODE;
                break;

            }
            // If Alarm Mode, Sound Module controls LED
            if(FLAG_SMOKE_ALARM_ACTIVE)
            {
                // Sound Module needs LED Control, do nothing here
                break;
            }
            else if(FLAG_LED_PROFILE_BUSY)
            {
                // No LED Blink Control if other Profiling Active
                break;
            }
            else if(FLAG_PTT_ACTIVE)
            {
                // Let PTT control LED actions during PTT
                break;
            }
            else if(FLAG_LGT_MEAS_CYCLE)
            {
                // No LED activity during Light Sampling
                break;
            }

            // Error Code Blinking can occur during Fault Modes
            if(FLAG_REQ_ERROR_CODE_BLINK)
            {
                // Blinking Error Code has Higher Priority over
                //   Fault/Error mode blinks

                #ifdef CONFIGURE_WIRELESS_MODULE
                    // Determine what code to blink
                   if(FLAG_NETWORK_ERROR_MODE)
                    {
                       // Limit to 7 blink codes
                        led_blink_count = (FLT_NetworkFaultCode & 0x07);
                    }
                    else
                    {
                        led_blink_count = (FLT_Fatal_Code & 0x0F);
                    }
                #else
                    {
                        led_blink_count = (FLT_Fatal_Code & 0x0F);
                    }
                #endif

                FLAG_REQ_ERROR_CODE_BLINK = 0;
                FLAG_ERROR_CODE_BLINKING = 1;
                rled_state = STATE_LED_BLINK_ERR_CODE;
                FLAG_RLED_NOT_IDLE = 1;

                // Add some delay before starting the Blinks
                return  MAIN_Make_Return_Value(LED_BLINK_2_SEC);
            }
            
            if( FLAG_FAULT_FATAL && !FLAG_EOL_FATAL )
            {
                // Amber LED controls these
                break; 
            }
            
            if(FLAG_EOL_FATAL)
            {
                // Amber LED controls these
                break;
            }
            
            #ifdef CONFIGURE_WIRELESS_MODULE

            // Network size = 0 for WiFi wireless
            #ifndef CONFIGURE_WIFI
                // Moved Blink Size above Network Error to allow
                //  Blink Size Test Code to operate during Network Error
                if(FLAG_APP_DISPLAY_NETWORK_SIZE)
                {
                    // Execute Network Size Blink State
                    led_blink_count = APP_Rf_Network_Size;
                    rled_state = STATE_LED_BLINK_SIZE;
                    FLAG_RLED_NOT_IDLE = 1;
                    
                    #ifdef CONFIGURE_VOICE
                        if(!FLAG_VOICE_BUSY)
                        {
                            if(APP_Rf_Network_Size > 1)
                            {
                                VCE_Announce_Number
                                    (APP_Rf_Network_Size);
                                VCE_Play(VCE_DEVICES_CONNECTED);
                            }
                            else
                            {
                                VCE_Play(VCE_NO_DEVICES_FOUND);
                            }
                        }
                        
                    #endif

                    break;
                }
            #endif

            #endif

            if(FLAG_NETWORK_ERROR_MODE )
            {
                // Amber LED controls these
                break;
            }
            
            #ifdef CONFIGURE_WIFI
                // Monitor AC Power for WiFi
                if(FLAG_AC_DETECT)
                {
                    if(FLAG_APP_JOIN_MODE)
                    {
                        rled_state = STATE_LED_WL_JOIN;
                        FLAG_RLED_NOT_IDLE = 1;
                        
                        #ifndef CONFIGURE_WIFI
                            //
                            // Initialize Join Mode Timeout Timer
                            //
                            FLAG_TIMEOUT_TIMER_ACTIVE = 0;
                            led_start_timeout_timer();
                        #else
                            //
                            // WiFi Cloud Join does not use timeout timer
                            //
                            FLAG_TIMEOUT_TIMER_ACTIVE = 0;
                        #endif

                        break;
                    }
                    
                    if(FLAG_APP_SEARCH_MODE)
                    {
                        // Begin LED Profile in 500 ms
                        profile_state = STATE_PROFILE_INTERVAL;
                        profile_int_count = LED_PROF_INT_500_MS;
                        FLAG_LED_PROFILE_BUSY = 1;
                        rled_state = STATE_LED_WL_SEARCH;
                        FLAG_RLED_NOT_IDLE = 1;
                        
                        // Reset Stand Alone indicator when entering Search
                        // This will get set again if Search not successful
                        FLAG_STAND_ALONE_MODE = 0;

                        // Init Search Mode State Timeout Timer
                        FLAG_TIMEOUT_TIMER_ACTIVE = 0;
                        led_start_timeout_timer();

                        /*
                         * There is a sequence where after Enroll Search Timeout
                         *  user performs a 4 second button press Join Request 
                         *  to try to enroll again.
                         * This action set FLAG_REQ_VOICE_JOIN_RFD, but since
                         *  it was not already enrolled Network search is set
                         *  by RF Module... thus here we are.
                         * Clear the FLAG_REQ_VOICE_JOIN_RFD flag so if 
                         *  a join is successful, the voice msg and pings are
                         *  not announced when entering the RFD LED Join States
                         */
                         FLAG_REQ_VOICE_JOIN_RFD = 0;

                        if(FLAG_PREV_JOINED)
                        {
                            // Log in Event History when we enter
                            //  network search, that aren't enrollment
                            //  searches
                            #ifdef CONFIGURE_DIAG_HIST
                                DIAG_Hist_Que_Push(HIST_NETWORK_SEARCH);
                            #endif
                        }

                        #ifdef CONFIGURE_VOICE
                            if(!FLAG_PREV_JOINED)
                            {
                                // Upon Entry, announce Voice Message
                                FLAG_REQ_VOICE_SEARCH_1 = 1;
                            }
                        #endif

                        break;
                    }
                }
            
            #else

              #ifdef CONFIGURE_WIRELESS_MODULE
                if(FLAG_APP_JOIN_MODE)
                {
                    rled_state = STATE_LED_WL_JOIN;
                    FLAG_RLED_NOT_IDLE = 1;

                    // Init Join Mode State Timeout Timer
                    FLAG_TIMEOUT_TIMER_ACTIVE = 0;
                    led_start_timeout_timer();

                    break;
                }
              #endif
            
              #ifdef CONFIGURE_WIRELESS_MODULE
                if(FLAG_APP_SEARCH_MODE)
                {
                    // Begin LED Profile in 500 ms
                    profile_state = STATE_PROFILE_INTERVAL;
                    profile_int_count = LED_PROF_INT_500_MS;
                    FLAG_LED_PROFILE_BUSY = 1;
                    rled_state = STATE_LED_WL_SEARCH;
                    FLAG_RLED_NOT_IDLE = 1;
                    
                    // Init Search Mode State Timeout Timer
                    FLAG_TIMEOUT_TIMER_ACTIVE = 0;
                    led_start_timeout_timer();
                    
                    /*
                     * There is a sequence where after Enroll Search Timeout
                     *  user performs a 4 second button press Join Request 
                     *  to try to enroll again.
                     * This action set FLAG_REQ_VOICE_JOIN_RFD, but since
                     *  it was not already enrolled Network search is set
                     *  by RF Module... thus here we are.
                     * Clear the FLAG_REQ_VOICE_JOIN_RFD flag so if 
                     *  a join is successful, the voice msg and pings are
                     *  not announced when entering the RFD LED Join States
                     */
                     FLAG_REQ_VOICE_JOIN_RFD = 0;

                    if(FLAG_PREV_JOINED)
                    {
                        // Log in Event History when we enter
                        //  network search, that aren't enrollment
                        //  searches
                        #ifdef CONFIGURE_DIAG_HIST
                            DIAG_Hist_Que_Push(HIST_NETWORK_SEARCH);
                        #endif
                    }
                    
                    #ifdef CONFIGURE_VOICE
                        if(!FLAG_PREV_JOINED)
                        {
                            // Upon Entry, announce Voice Message
                            FLAG_REQ_VOICE_SEARCH_1 = 1;
                        }
                    #endif

                    break;
                }
              #endif
            #endif



            if(FLAG_EOL_MODE || FLAG_EOL_HUSH_ACTIVE)
            {
                // Non - Fatal EOL Modes, Red Does nothing
                // Join/Search Modes are Higher Priority
                break;
            }
            
            #ifdef CONFIGURE_WIRELESS_MODULE
            
                /* 
                 * Set Alert Priorities
                 *  1 Gas 2 Intruder
                 */
                if( FLAG_ALERT_EXP_GAS || FLAG_ALERT_INTRUDER )
                {
                        // Init Timers
                    led_alert_timer = 0;
                    // Init to 10 secs to announce voice on entry
                    led_alert_count = LED_ALERT_TIMER_10_SEC; 

                    rled_state = STATE_LED_WL_DETECTION_ALERT;
                    FLAG_RLED_NOT_IDLE = 1;
                    break;
                }
                    
                /* 3 Panic Alert */
                if(FLAG_ALERT_PANIC)
                {
                    // Init Timers
                    led_alert_timer = 0;
                    led_alert_count = 0;
                    
                    rled_state = STATE_LED_WL_PANIC_ALERT;
                    FLAG_RLED_NOT_IDLE = 1;
                    break;
                }
                    
                /* 4 Weather Alert */
                if(FLAG_ALERT_WEATHER)
                {
                    // Init Alert State
                    alert_state = STATE_ALERT_HORN;
                    
                    rled_state = STATE_LED_WL_SHORT_ALERT;
                    FLAG_RLED_NOT_IDLE = 1;
                    break;
                }
            
                /* 5. Water Leak 6. Temp Drop 7. General Alert */
                if( FLAG_ALERT_WATER || FLAG_ALERT_TEMP_DROP || 
                    FLAG_ALERT_GENERAL )
                {
                    // Init Alert State
                    alert_state = STATE_ALERT_HORN;

                    rled_state = STATE_LED_WL_SHORT_ALERT;
                    FLAG_RLED_NOT_IDLE = 1;
                    break;
                }
                
            
            #endif

            if(FLAG_LOW_BATTERY || FLAG_BAT_CONSRV_MODE_ACTIVE)  
            {
                // Hold Off On Alarm Memory Indications
                //  in Low Battery
            }
            else if( FLAG_SMOKE_ALARM_MEMORY || FLAG_CO_ALARM_MEMORY) 
            {
                #ifdef CONFIGURE_TIMER_SERIAL
                    SER_Send_String("Start AM 1Hr Tmr- ");
                    SER_Send_Prompt();
                #endif 
                    
                // Init a 1 Hour Timer
                led_alm_memory_timer = MAIN_OneMinuteTic;

                // Initialize Alarm memory Profile
                profile_state = STATE_PROFILE_FADE_ON;
                FLAG_LED_PROFILE_BUSY = 1;

                rled_state = STATE_LED_ALM_MEMORY;
                FLAG_RLED_NOT_IDLE = 1;
                break;
            }
            
            // Else- No Red LED activity
            if(!FLAG_MODE_ACTIVE)
            {
                // Increase Task Time if in LP Mode
                return  MAIN_Make_Return_Value(LED_BLINK_10_SEC);
            }
        }
        break;

        case STATE_LED_SMK_CAL_INIT:
            if(--led_blink_count != 0)
            {
                // Toggle the Red LED
                if(FLAG_RED_LED_ON)
                {
                    FLAG_RED_LED_ON = 0;
                }
                else
                {
                    FLAG_RED_LED_ON = 1;
                }
                return	MAIN_Make_Return_Value(LED_BLINK_30_MS);

            }
            else
            {
                // Proceed to Red LED Smoke Cal in process state
                RLED_Off();
                rled_state = STATE_LED_SMK_CAL;
            }
        break;

        case STATE_LED_SMK_CAL:
            if( SMK_DWELL == PHOTO_Get_State() )
            {
                // Cal attempt has ended - LED is OFF/ON based upon result
            }
            else if(FLAG_FAULT_FATAL)
            {
                // Allow Fault Code Blinks, stop Red LED Blinks
            }
            else
            {
                // Toggle the Red LED
                if(FLAG_RED_LED_ON)
                {
                    FLAG_RED_LED_ON = 0;
                }
                else
                {
                    FLAG_RED_LED_ON = 1;
                }

                return  MAIN_Make_Return_Value(LED_BLINK_1_SEC);
            }
        break;


        case STATE_LED_SMK_HUSH_MODE:
        {
            // Monitor Hush Mode Active
            // Allow CO Alarm to execute from Smoke Hush

            #ifdef CONFIGURE_HUSH_LED_PROFILE            
                if( FLAG_SMOKE_HUSH_ACTIVE && !FLAG_PTT_ACTIVE &&
                    !FLAG_CO_ALARM_ACTIVE )
                {

                    // Run the Hush Mode LED Profile
                    if(!FLAG_LED_PRO_N_PROGRESS)
                    {
                        switch(profile_state)
                        {
                            case STATE_PROFILE_FADE_ON:
                            {
                                led_set_profile(LED_PRO_FADE_ON_RED_1);
                                profile_state = STATE_PROFILE_FADE_OFF;
                            }
                            break;

                            case STATE_PROFILE_FADE_OFF:
                            {
                                led_set_profile(LED_PRO_FADE_OFF_RED_1);
                                profile_state = STATE_PROFILE_INTERVAL;

                                profile_int_count = LED_PROF_INT_2_SEC;
                            }
                            break;

                            case STATE_PROFILE_INTERVAL:
                            {
                                if(0 == --profile_int_count)
                                {
                                    profile_state = STATE_PROFILE_FADE_ON;
                                }
                            }
                            break;
                        }
                    }
                }
                else
                {
                    // Hush Mode has Ended
                    // Allow Profile to Complete, exit Hush State
                    if(!FLAG_LED_PRO_N_PROGRESS)
                    {
                        // Set LED Profiles Off
                        led_set_profile(LED_PRO_NONE);

                        // Search Mode is inactive or we entered Alarm
                        // Abort this State
                        RLED_Off();
                        rled_state = STATE_LED_NORM_BLINKING;

                        FLAG_ACTIVE_TIMER = 1;
                        if(MAIN_ActiveTimer < TIMER_ACTIVE_MINIMUM_2_SEC)
                        {
                            // Extend time to remain in Active Mode
                            MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_2_SEC;
                        }

                    }
                }

            #else

                // Use Smoke Hush LED Blink indication

                if(FLAG_SMOKE_HUSH_ACTIVE && !FLAG_PTT_ACTIVE &&
                   !FLAG_CO_ALARM_ACTIVE && !FLAG_LED_OOB_PROFILE_ENABLE)
                {
                    // Red Fault Mode Blinks
                    FLAG_RED_LED_BLINK = 1;
                    // Blink every 2 seconds as Smoke Hush Indication
                    return MAIN_Make_Return_Value(LED_BLINK_2_SEC);
                }
                else
                {
                    // Hush Mode has Ended
                    // Allow Profile to Complete, exit Hush State

                    // Hush Mode is inactive or we entered Alarm
                    // Abort this State
                    RLED_Off();
                    rled_state = STATE_LED_NORM_BLINKING;

                    FLAG_ACTIVE_TIMER = 1;
                    if(MAIN_ActiveTimer < TIMER_ACTIVE_MINIMUM_2_SEC)
                    {
                        // Extend time to remain in Active Mode
                        MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_2_SEC;
                    }

                }

            #endif

        }
        break;



        case STATE_LED_ALM_MEMORY:
        {
            
            #ifdef CONFIGURE_ACCEL_TIMERS    
                if( (uchar)(MAIN_OneMinuteTic - led_alm_memory_timer) >= 
                    (uchar)LED_ALM_MEMORY_5_MIN)
            #else
                if( (uchar)(MAIN_OneMinuteTic - led_alm_memory_timer) >= 
                    (uchar)LED_ALM_MEMORY_60_MIN)
            #endif

            {
                #ifdef CONFIGURE_TIMER_SERIAL
                    if(FLAG_SMOKE_ALARM_MEMORY)
                    {
                        SER_Send_String("AM 1Hr Expired- ");
                        SER_Send_Prompt();
                    }
                #endif             

                if(FLAG_SMOKE_ALARM_MEMORY)
                {
                    // End Smoke Alarm Memory
                    FLAG_SMOKE_ALARM_MEMORY = 0;
                    FLAG_SMK_ALARM_MEMORY_ANNOUNCE=1;
                }
                    
                if(FLAG_CO_ALARM_MEMORY)
                {
                    // End CO Alarm Memory
                    FLAG_CO_ALARM_MEMORY = 0;
                    FLAG_CO_ALARM_MEMORY_ANNOUNCE=1;
                }
                
                // End Alarm memory profile
                // Allow Profile to Complete, exit State
                if(!FLAG_LED_PRO_N_PROGRESS)
                {
                    // Set LED Profiles Off
                    led_set_profile(LED_PRO_NONE);

                    // Search Mode is inactive or we entered Alarm
                    // Abort this State
                    RLED_Off();
                    ALED_Off();
                    rled_state = STATE_LED_NORM_BLINKING;
                }
                
            }
            
            // Monitor Alarm Memory profile condition
            // Also Low battery and Active Alarms have priority over
            //  alarm memory
            if( (FLAG_SMOKE_ALARM_MEMORY || FLAG_CO_ALARM_MEMORY) &&
                 !FLAG_PTT_ACTIVE && !FLAG_LOW_BATTERY &&
                 !FLAG_CO_ALARM_ACTIVE && !FLAG_SMOKE_ALARM_ACTIVE &&
                 !FLAG_APP_JOIN_MODE && !FLAG_APP_SEARCH_MODE &&
                 !FLAG_NETWORK_ERROR_MODE &&
                 !FLAG_EOL_MODE &&
                 !FLAG_EOL_HUSH_ACTIVE && !FLAG_FAULT_FATAL &&
                 !FLAG_ALERT_PANIC && !FLAG_ALERT_WEATHER && 
                 !FLAG_ALERT_DETECT && !FLAG_ALARM_IDENTIFY &&
                 !FLAG_BAT_CONSRV_MODE_ACTIVE && 
                 !FLAG_LED_OOB_PROFILE_ENABLE ) 
            {
                // Run the Alarm Memory Profile
                if(!FLAG_LED_PRO_N_PROGRESS)
                {
                    // Run Red and Amber LED Profile every 10 seconds
                    
                    switch(profile_state)
                    {
                        case STATE_PROFILE_FADE_ON:
                        {
                            led_set_profile(LED_PRO_FADE_ON_RED_2);
                            profile_state = STATE_PROFILE_FADE_OFF;
                        }
                        break;

                        case STATE_PROFILE_FADE_OFF:
                        {
                            led_set_profile(LED_PRO_FADE_OFF_RED_2);
                            profile_state = STATE_PROFILE_INTERVAL;

                            profile_int_count = LED_PROF_INT_500_MS;
                        }
                        break;

                        case STATE_PROFILE_INTERVAL:
                        {
                            if(0 == --profile_int_count)
                            {
                                profile_state = STATE_PROFILE_FADE_ON_2;
                            }
                        }
                        break;

                        case STATE_PROFILE_FADE_ON_2:
                        {
                            led_set_profile(LED_PRO_FADE_ON_AMB_2);
                            profile_state = STATE_PROFILE_FADE_OFF_2;
                        }
                        break;

                        case STATE_PROFILE_FADE_OFF_2:
                        {
                            led_set_profile(LED_PRO_FADE_OFF_AMB_2);
                            profile_state = STATE_PROFILE_INTERVAL_2;

                            profile_int_count = LED_PROF_INT_10_SEC;
                        }
                        break;

                        case STATE_PROFILE_INTERVAL_2:
                        {
                            if(0 == --profile_int_count)
                            {
                                profile_state = STATE_PROFILE_FADE_ON;
                            }
                        }
                        break;

                    }
                }
            }
            else
            {
                // End Alarm memory profile
                // Allow Profile to Complete, exit State
                if(!FLAG_LED_PRO_N_PROGRESS)
                {
                    // Set LED Profiles Off
                    led_set_profile(LED_PRO_NONE);

                    // Search Mode is inactive or we entered Alarm
                    // Abort this State
                    RLED_Off();
                    ALED_Off();
                    rled_state = STATE_LED_NORM_BLINKING;
                    
                    FLAG_ACTIVE_TIMER = 1;
                    if(MAIN_ActiveTimer < TIMER_ACTIVE_MINIMUM_2_SEC)
                    {
                        // Extend time to remain in Active Mode
                        MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_2_SEC;
                    }
                }
            }
        }
        break;

        case STATE_LED_BLINK_SIZE:
        {
            
            #ifdef CONFIGURE_WIRELESS_MODULE
                if(0 == led_blink_count)
                {
                    RLED_Off();
                    rled_state = STATE_LED_NORM_BLINKING;
                    
                    // Network Size Blink Completed
                    FLAG_APP_DISPLAY_NETWORK_SIZE = 0;
                }
                else
                {
                    led_blink_count--;
                    FLAG_RED_LED_BLINK = 1;
                    
                    if(0 == led_blink_count)
                    {
                        // Insert some delay before ending Blink State
                        //  on last entry.
                        return  MAIN_Make_Return_Value(LED_BLINK_1_SEC);
                    }
                    else
                    {
                        return  MAIN_Make_Return_Value(LED_BLINK_500_MS);
                    }
                }
            #endif
        }
        break;
        
    #ifdef CONFIGURE_WIRELESS_MODULE

        case STATE_LED_WL_JOIN:
        {
            // Allow WiFi module to handle all Join Mode timeouts
            #ifdef CONFIGURE_WIFI  
                #ifdef CONFIGURE_UNDEFINED
                    // Check State Timeout Timer    
                    led_process_timeout_timer(LED_TIMER_40_MINS);

                    if(!FLAG_TIMEOUT_TIMER_ACTIVE)
                    {
                        // Timer Expired, Force Join End since it should have ended
                        //  by now, Abort Search/Join
                        FLAG_COMM_JOIN_IN_PROGRESS = 0;
                        FLAG_APP_JOIN_MODE = 0;
                    }
                #endif
            #else
                // Check State Timeout Timer    
                led_process_timeout_timer(LED_TIMER_40_MINS);

                if(!FLAG_TIMEOUT_TIMER_ACTIVE)
                {
                    // Timer Expired, Force Join End since it should have ended
                    //  by now, Abort Search/Join
                    FLAG_COMM_JOIN_IN_PROGRESS = 0;
                    FLAG_APP_JOIN_MODE = 0;
                }
            #endif

            if( FLAG_APP_JOIN_MODE &&
                !FLAG_APP_DISPLAY_NETWORK_SIZE &&
                !FLAG_FAULT_FATAL &&
                !FLAG_CO_ALARM_ACTIVE && !FLAG_SMOKE_ALARM_ACTIVE &&
                !FLAG_SMOKE_HUSH_ACTIVE )
            {
                // We are in Network Join Mode
                // No Red LED action, hang here until Join Ends 
                
            }
            else
            {
                // Allow Profile to Complete
                if(!FLAG_LED_PROFILE_BUSY)
                {
                    // If we are exiting because Join Ended
                    //  or needs to be aborted
                    rled_state = STATE_LED_NORM_BLINKING;
                }
            }
        }
        break;
        
        case STATE_LED_WL_SEARCH:
        {
            
            #ifdef CONFIGURE_VOICE
                // A single button Press will cause a voice announcement in
                // Search Mode, but 
                // Only non enrolled devices will announce this 
                //  voice message
                if(FLAG_REQ_VOICE_SEARCH_1)
                {
                    FLAG_REQ_VOICE_SEARCH_1 = 0;

                    // Announce Voice Messages upon State Entry and
                    //  when button is pressed
                    FLAG_ANNOUNCE_RED_VOICE =1;
                    voice_seq_state = MSG_SEQ_1;    // start with Voice
                }
            #endif

            #ifdef CONFIGURE_VOICE
                if(FLAG_ANNOUNCE_RED_VOICE)
                {
                    // Execute Voice Message Sequence as
                    // previous voices are completed  FLAG_VOICE_BUSY = 0;
                    // Voice module controls FLAG_VOICE_BUSY

                    switch(voice_seq_state)
                    {
                        case MSG_SEQ_1:
                            if(!FLAG_VOICE_BUSY)
                            {
                                VCE_Play(VCE_READY_TO_CONNECT);
                                voice_seq_state = MSG_SEQ_2;
                            }
                        break;

                        case MSG_SEQ_2:
                            if(!FLAG_VOICE_BUSY)
                            {
                                // Currently this is a single voice message

                                // End Voice Sequence
                                FLAG_ANNOUNCE_RED_VOICE = 0;  
                                voice_seq_state = MSG_SEQ_1;
                            }
                        break;

                        default:
                            // End Voice Sequence
                            FLAG_ANNOUNCE_RED_VOICE = 0;     
                        break;

                    }
                }
            #else
                FLAG_ANNOUNCE_GRN_VOICE = 0;
            #endif

            #ifdef CONFIGURE_WIFI
                // Do we want a Timeout for WiFi?
                
                // Check State Timeout Timer    
                led_process_timeout_timer(LED_TIMER_40_MINS);
                if(!FLAG_TIMEOUT_TIMER_ACTIVE)
                {
                    // Timer Expired, Force Search End since it should have ended
                    //  by now, Abort Search/Join
                    FLAG_COMM_JOIN_IN_PROGRESS = 0;
                    FLAG_APP_SEARCH_MODE = 0;

                    // Enter Stand Alone Mode under this timeout condition
                    FLAG_STAND_ALONE_MODE = 1;
                }
            #else
                // Check State Timeout Timer    
                led_process_timeout_timer(LED_TIMER_40_MINS);
                if(!FLAG_TIMEOUT_TIMER_ACTIVE)
                {
                    // Timer Expired, Force Search End since it should have ended
                    //  by now, Abort Search/Join
                    FLAG_COMM_JOIN_IN_PROGRESS = 0;
                    FLAG_APP_SEARCH_MODE = 0;

                    // Enter Stand Alone Mode under this timeout condition
                    FLAG_STAND_ALONE_MODE = 1;
                }
            #endif
            
            #ifdef CONFIGURE_WIFI
                if(!FLAG_AC_DETECT)
                {
                    if(FLAG_PREV_JOINED)
                    {
                        // First, If Stealth Search Active, clear Profile Active
                        FLAG_LED_PRO_N_PROGRESS = 0;
                    }
                    
                    // Lost WiFi Power; wait for profile to end then exit
                    if(!FLAG_LED_PRO_N_PROGRESS)
                    {
                        // Set LED Profiles Off
                        led_set_profile(LED_PRO_NONE);
                    
                        // WiFi Power Lost detected
                        // Stop Search Mode
                        FLAG_APP_SEARCH_MODE = 0;
                        
                        // If in Stealth Search when Power is Lost
                        //  do not clear Previously Joined Flag
                        
                        // Set Stand Alone when no WiFi
                        FLAG_STAND_ALONE_MODE = 1;
                        
                        // WiFi Power Lost, exit Search and Join modes
                        // End this State
                        rled_state = STATE_LED_NORM_BLINKING;
                        
                        FLAG_ACTIVE_TIMER = 1;
                        if(MAIN_ActiveTimer < TIMER_ACTIVE_MINIMUM_2_SEC)
                        {
                            // Extend time to remain in Active Mode
                            MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_2_SEC;
                        }
                    }
                    return  MAIN_Make_Return_Value(LED_BLINK_100_MS);
                }
            #endif
            
            
            /*
             * In Search Mode. if previously enrolled, we no longer
             * want to display LED Search Profile. This is called
             * Stealth Search Mode as it attempts reconnection to network
             * without operator knowing it
             */

            if( FLAG_APP_SEARCH_MODE &&
                !FLAG_FAULT_FATAL && 
                !FLAG_CO_ALARM_ACTIVE && !FLAG_SMOKE_ALARM_ACTIVE &&
                !FLAG_SMOKE_HUSH_ACTIVE )
            {
                
                // FLAG_STEALTH_SEARCH_OFF is controlled by Serial CMD
                // PREV_Joined normally defines Stealth Search ON/Off
                if( !FLAG_PREV_JOINED || FLAG_STEALTH_SEARCH_OFF )
                {

                    // We are still in Network Search Mode
                    // Run Red LED Search Profile
                    if(!FLAG_LED_PRO_N_PROGRESS)
                    {
                        switch(profile_state)
                        {
                            case STATE_PROFILE_FADE_ON:
                            {
                                led_set_profile(LED_PRO_FADE_ON_RED_1);
                                profile_state = STATE_PROFILE_FADE_OFF;
                            }
                            break;

                            case STATE_PROFILE_FADE_OFF:
                            {
                                led_set_profile(LED_PRO_FADE_OFF_RED_1);
                                profile_state = STATE_PROFILE_INTERVAL;

                                profile_int_count = LED_PROF_INT_500_MS;
                            }
                            break;

                            case STATE_PROFILE_INTERVAL:
                            {
                                if(0 == --profile_int_count)
                                {
                                    profile_state = STATE_PROFILE_FADE_ON;
                                }
                            }
                            break;

                        }
                    }
                }
                else
                {
                    // Stealth Search Mode (no LED Profile output)
                    // Inhibit other profiles since we are in Search Mode
                    //  by setting Fake Profile Busy condition
                    led_set_profile(LED_PRO_NONE);
                    FLAG_LED_PRO_N_PROGRESS = 1;
                }
            }
            else
            {
                if(FLAG_PREV_JOINED && !FLAG_STEALTH_SEARCH_OFF)
                {
                    // Exiting Stealth Search Mode
                    // reset LED Profile Busy since it was forced to Busy State
                    // It is not forced when Stealth Off is active
                    FLAG_LED_PRO_N_PROGRESS = 0;
                }

                // Allow Last Profile to Complete, then exit
                if(!FLAG_LED_PRO_N_PROGRESS)
                {
                    // Set LED Profiles Off, This also clears Profile Busy
                    led_set_profile(LED_PRO_NONE);

                    // Search Mode is inactive or we entered Alarm
                    // Exit this State
                    RLED_Off();
                    
                    // Network Search may end at OOB Reset or Timeout
                    //  so distinguish between Not Found and OOB Reset
                    // Search has ended without a successful Join
                    if( (0 == APP_Remote_COMM_Status.ALL) ||
                        (!FLAG_COMM_JOIN_COMPLETE  && 
                         !FLAG_COMM_JOIN_IN_PROGRESS) )
                    {
                        // If not Previously Enrolled or Search Aborted 
                        //  from OOB then do not issue Network Error actions
                        if(!FLAG_PREV_JOINED && !FLAG_LED_OOB_PROFILE_ENABLE)
                        {
                           
                           // If received COMM Status not Joined 
                           //  then a Network was never Found
                           //  Search Mode Ended without finding network
                           //  Execute Network not Found profile

                            // Begin LED Profile in 100 ms
                            profile_int_count = LED_PROF_INT_100_MS;
                            profile_state = STATE_PROFILE_INTERVAL;
                            
                            #ifdef CONFIGURE_WIFI
                                // Bypass Not Connected LED Profile for WiFi
                                cycle_count = 1;
                            #else
                                cycle_count = 7;
                            #endif
                            
                            FLAG_LED_PROFILE_BUSY = 1;


                            rled_state = STATE_LED_NETWORK_NOT_FOUND;

                            // Announce Voice 
                             #ifdef CONFIGURE_VOICE
                                // Devices not found, not connected
                                #ifndef CONFIGURE_WIFI
                                    // Devices not found, not connected
                                    VCE_Play(VCE_NO_DEVICES_FOUND);
                                #endif

                                VCE_Play(VCE_NOT_CONNECTED);
                             #endif

                            // Under this situation Enrollment Join Failed 
                            //  clear any Fault so the Alarm can run as stand 
                            //  alone unit
                            FLAG_STATUS_RMT_FAULT_ACTIVE = 0;
                            
                            //
                            // Enter Stand Alone Mode under this condition
                            // Stand alone mode still keeps RF active and 
                            //  performs CCI supervision, but does not enter 
                            //  Network Error Mode
                            // 
                            FLAG_STAND_ALONE_MODE = 1;
                        }
                        else
                        {
                            // Previously Joined Search Mode terminated 
                            //  but did not Join, if an error was received
                            //  do not clear error
                           rled_state = STATE_LED_NORM_BLINKING; 

                            if(FLAG_LED_OOB_PROFILE_ENABLE && !FLAG_PREV_JOINED)
                            {
                                #ifdef CONFIGURE_WIFI
                                    //
                                    // For WiFi allow a re-commission after 
                                    //  OOB Reset
                                    //
                                    FLAG_OOB_RST_WITH_NO_REJOIN = 0;
                                    FLAG_STAND_ALONE_MODE = 0;
                                #else
                                    //
                                    // If Search Aborted from OOB Reset by BTN press
                                    //  and not enrolled, then do not re-enter 
                                    //  search after OOB Reset
                                    //
                                    FLAG_OOB_RST_WITH_NO_REJOIN = 1;
                                #endif
                            }
                            else
                            {
                                #ifdef CONFIGURE_WIFI
                                    //
                                    // Arrived here from AP Search 15 minute
                                    //  timeout - Comm Status (0x80)
                                    // Go ahead and set stand alone
                                    //  even though we PREV have Joined
                                    //
                                    FLAG_STAND_ALONE_MODE = 1;
                                #endif
                                
                            }
                            
                        }
                        
                    }
                    else
                    {
                        if(FLAG_LED_OOB_PROFILE_ENABLE)
                        {

                            #ifdef CONFIGURE_WIFI
                                //
                                // For WiFi allow a re-commission after 
                                //  OOB Reset
                                //
                                FLAG_OOB_RST_WITH_NO_REJOIN = 0;
                                FLAG_STAND_ALONE_MODE = 0;
                            #else
                                //
                                // If Search Aborted from OOB Reset by BTN press
                                //  and not enrolled, then do not re-enter 
                                //  search after OOB Reset
                                //
                                FLAG_OOB_RST_WITH_NO_REJOIN = 1;
                            #endif
                            
                        }
                                                    
                        // RFD Search ended or needs to be Aborted
                        rled_state = STATE_LED_NORM_BLINKING;
                        
                    }

                    FLAG_ACTIVE_TIMER = 1;
                    if(MAIN_ActiveTimer < TIMER_ACTIVE_MINIMUM_2_SEC)
                    {
                        // Extend time to remain in Active Mode
                        MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_2_SEC;
                    }
                }
            }
        }
        break;
        

        case STATE_LED_NETWORK_NOT_FOUND:
        {
            /*
             *  State Entered when RF Module is in Search Mode for longer
             *  than 15 minutes (No Network Found to Join)
             *  Join Ends without a Join Complete (set FLAG_NETWORK_NOT_FOUND)
             *
             *  Exit this state after the LED profile has completed and
             *  return to IDLE
             *
             */            

            // Run the Network Not Found LED Profile
            if(!FLAG_LED_PRO_N_PROGRESS)
            {
                switch(profile_state)
                {
                    case STATE_PROFILE_FADE_ON:
                    {
                        led_set_profile(LED_PRO_FADE_ON_RED_2);
                        profile_state = STATE_PROFILE_FADE_OFF;
                    }
                    break;

                    case STATE_PROFILE_FADE_OFF:
                    {
                        led_set_profile(LED_PRO_FADE_OFF_RED_2);
                        profile_state = STATE_PROFILE_INTERVAL;

                        profile_int_count = LED_PROF_INT_3_SEC;
                    }
                    break;

                    case STATE_PROFILE_INTERVAL:
                    {
                        if(0 == --profile_int_count)
                        {
                            if(0 == --cycle_count)
                            {
                                // Set LED Profiles Off, Profile Busy Flag
                                //  will be cleared
                                led_set_profile(LED_PRO_NONE);

                                // Profile Completed
                                RLED_Off();
                                rled_state = STATE_LED_NORM_BLINKING;

                                FLAG_ACTIVE_TIMER = 1;
                                if(MAIN_ActiveTimer < 
                                        TIMER_ACTIVE_MINIMUM_2_SEC)
                                {
                                    // Extend time to remain in Active Mode
                                    MAIN_ActiveTimer = 
                                            TIMER_ACTIVE_MINIMUM_2_SEC;
                                }
                            }
                            else
                            {
                                // Repeat
                                profile_state = STATE_PROFILE_FADE_ON;
                            }
                        }
                    }
                    break;
                }
            }
        }
        break;

        case STATE_LED_WL_PANIC_ALERT:
        {
           // This is Panic Alert State
            // Red Blink until timeout or condition ended
            
            // Update Alert Timers (currently refresh is not implemented)
            if(FLAG_ALARM_REFRESH)
            {
                // Another Alert received - Refresh Timer
                led_alert_timer = 0;
                FLAG_ALARM_REFRESH = 0;
            }
            else if(LED_PANIC_ALERT_TIMER_30_SEC <= ++led_alert_timer)
            {
                // Panic Alert Timeout
                FLAG_ALERT_PANIC = 0;
            }
            
            if(LED_PANIC_ALERT_TIMER_5_SEC <= ++led_alert_count)
            {
                FLAG_ALERT_HORN_ENABLED = 0;
                HORN_OFF
                        
                // Time for a Voice Announcement, clear timer
                led_alert_count = 0;
                
                VCE_Play(VCE_EMERGENCY);

            }
            
            if( !FLAG_ALERT_PANIC  ||
                 FLAG_SMOKE_ALARM_ACTIVE || FLAG_CO_ALARM_ACTIVE )
            {
                // Terminate Alerts early if Alarm Detected
               
                FLAG_STATUS_2_PANIC = 0;
                FLAG_ALERT_HORN_ENABLED = 0;
                
                // No Fault blinking during Alarm, Sound needs LED control
                // Also Exit if Error Code Blinks requested
                rled_state = STATE_LED_NORM_BLINKING;
                RLED_Off();
                HORN_OFF
                
                FLAG_ACTIVE_TIMER = 1;        
                if(MAIN_ActiveTimer < TIMER_ACTIVE_MINIMUM_2_SEC)
                {
                    // Extend time to remain in Active Mode
                    MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_2_SEC;
                }
            }
            else 
            {
            
                // Allow Voice Message to Complete before Horn is Enabled
                if(!FLAG_VOICE_ACTIVE)
                {
                    FLAG_ALERT_HORN_ENABLED = 1;
                }
                
                // Flash every 1/2 second as Alert Indication
                if(FLAG_RED_LED_ON)
                {
                    FLAG_RED_LED_ON = 0;
                    HORN_OFF
                }
                else
                {
                    FLAG_RED_LED_ON = 1;  

                    // Turn On Horn if Alert Horn ON
                    if(FLAG_ALERT_HORN_ENABLED)
                    {
                        HORN_ON
                    }
                }
                
                return MAIN_Make_Return_Value(LED_BLINK_500_MS);
            }
        }
        break;

        
        case STATE_LED_WL_SHORT_ALERT:
        {
           // This is Weather Alert State
            //  Start 1 Sec RED LED flash
            //  1 Second Horn Beep
            //  5 Second Delay
            //  Voice Message Announced
            //  Exit Short Alert State
            
            switch(alert_state)
            {
                case STATE_ALERT_HORN:
                {
                    HORN_ON
                    // Init Timers
                    led_alert_timer = 0;
                    alert_state = STATE_ALERT_DELAY;
                }
                break;
                
                case STATE_ALERT_DELAY:
                {
                    HORN_OFF
                    
                    if(LED_ALERT_TIMER_5_SEC <= ++led_alert_timer)
                    {
                        // Announce Voice Msg After the 5 Sec Delay
                        if(FLAG_ALERT_WEATHER)
                        {
                            VCE_Play(VCE_WEATHER_ALERT);
                        }
                        
                        if(FLAG_ALERT_WATER)
                        {
                           VCE_Play(VCE_WATER_LEAK); 
                        }
                        
                        if(FLAG_ALERT_TEMP_DROP)
                        {
                            VCE_Play(VCE_TEMP_DROP);
                        }
                        
                        if(FLAG_ALERT_GENERAL)
                        {
                           // Currently No Voice Message for General Alert
                           
                        }

                        alert_state = STATE_ALERT_VOICE;
                    }
                }
                break;
                
                case STATE_ALERT_VOICE:
                {
                    // Allow Voice Message to Complete
                    if(!FLAG_VOICE_ACTIVE)
                    {
                        alert_state = STATE_ALERT_END;
                    }
                }
                break;
                
                case STATE_ALERT_END:
                {
                    // Terminate Alerts that use this Short Alert State
                    FLAG_STATUS_3_WEATHER = 0;
                    FLAG_STATUS_3_GEN_ALERT = 0;
                    FLAG_STATUS_2_WATER = 0;
                    FLAG_STATUS_2_TEMP_DROP = 0;
                    
                    FLAG_ALERT_DETECT = 0;
                
                    // No blinking during Alarm, Sound needs LED control
                    // Also Exit if Error Code Blinks requested
                    rled_state = STATE_LED_NORM_BLINKING;
                    RLED_Off();

                    // Insure Horn is OFF
                    HORN_OFF

                    FLAG_ACTIVE_TIMER = 1;        
                    if(MAIN_ActiveTimer < TIMER_ACTIVE_MINIMUM_2_SEC)
                    {
                        // Extend time to remain in Active Mode
                        MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_2_SEC;
                    }
                    
                    return MAIN_Make_Return_Value(LED_BLINK_1_SEC);
                }
                
            }
                
            // Flash every 1 second as Error Indication for duration
            //  of this Short Alert State
            if(FLAG_RED_LED_ON)
            {
                FLAG_RED_LED_ON = 0;
            }
            else
            {
                FLAG_RED_LED_ON = 1;  
            }

            return MAIN_Make_Return_Value(LED_BLINK_1_SEC);
        }

        
        case STATE_LED_WL_DETECTION_ALERT:
        {
           // This is Condition Detect Alert State
            // Red Blink until timeout or condition ended
             
            // Update Alert Timers (currently refresh is not implemented)
            if(FLAG_ALARM_REFRESH)
            {
                // Another Alert received - Refresh Timer
                led_alert_timer = 0;
                FLAG_ALARM_REFRESH = 0;
            }
            else if(LED_ALERT_TIMER_40_SEC <= ++led_alert_timer)
            {
                // Condition Alert Timeout
                FLAG_ALERT_DETECT = 0;
            }
            
            if(LED_ALERT_TIMER_10_SEC <= ++led_alert_count)
            {
                HORN_OFF
                        
                // Time for a Voice Announcement
                // Voice Depends upon Alert Detected        
                led_alert_count = 0;
                if(FLAG_ALERT_EXP_GAS)
                {
                    VCE_Play(VCE_EXPLOSIVE_GAS);
                }
                
                if(FLAG_ALERT_INTRUDER)
                {
                    VCE_Play(VCE_INTRUDER_ALERT);
                }
            }
 
           if( !FLAG_ALERT_DETECT  ||
                FLAG_SMOKE_ALARM_ACTIVE || FLAG_CO_ALARM_ACTIVE )
            {
                // Terminate Alerts early if Alarm Detected
                FLAG_ALERT_DETECT = 0;
                FLAG_STATUS_3_GAS = 0;
                FLAG_STATUS_3_INTRUDER = 0;
                
                // No blinking during Alarm, Sound needs LED control
                // Also Exit if Error Code Blinks requested
                rled_state = STATE_LED_NORM_BLINKING;
                // Clear LED flag
                RLED_Off();
                HORN_OFF
                        
                FLAG_ACTIVE_TIMER = 1;
                if(MAIN_ActiveTimer < TIMER_ACTIVE_MINIMUM_2_SEC)
                {
                    // Extend time to remain in Active Mode
                    MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_2_SEC;
                }
                
            }
            else 
            {
                // Flash every 1 second as Error Indication
                if(FLAG_RED_LED_ON)
                {
                  FLAG_RED_LED_ON = 0;
                  HORN_OFF
                }
                else
                {
                  FLAG_RED_LED_ON = 1;
                  
                  if(ALERT_HORN_DELAY_3_SECS <=led_alert_count)
                  {
                    // Delay to allow Voice to announce   
                    HORN_ON
                  }
                }
                return MAIN_Make_Return_Value(LED_BLINK_1_SEC);
            }
        }
        break;

    #endif

        case STATE_LED_BLINK_ERR_CODE:
        {
                    
            if(0 == led_blink_count)
            {
                FLAG_ERROR_CODE_BLINKING = 0;

                if(FLAG_REQ_RESET_AFTER_BLINKS)
                {
                    FLAG_REQ_RESET_AFTER_BLINKS = 0;
                    SOFT_RESET
                }
                else
                {
                    rled_state = STATE_LED_NORM_BLINKING;
                }
            }
            else
            {
                #ifdef CONFIGURE_WIRELESS_MODULE
                    // If Network Error or Fault (long blinks)
                    if(FLAG_NETWORK_ERROR_MODE)
                    {
                        // Wireless Fault Code Blink Pattern
                        // Blink out the Code set of 500ms blinks 
                        // Toggle the Amber LED
                        if(FLAG_RED_LED_ON)
                        {
                            FLAG_RED_LED_ON = 0;
                            led_blink_count--;
                        }
                        else
                        {
                            FLAG_RED_LED_ON = 1;
                        }

                        if(0 == led_blink_count)
                        {
                            // Insert some delay before ending Blink State
                            //  on last entry.
                            return  MAIN_Make_Return_Value(LED_BLINK_2_SEC);
                        }
                        else
                        {
                            return  MAIN_Make_Return_Value(LED_BLINK_500_MS);
                        }
                    }
                    else
                    {
                        // If Alarm error (short blinks)
                        led_blink_count--;
                        FLAG_RED_LED_BLINK = 1;

                        if(0 == led_blink_count)
                        {
                            // Insert some delay before ending Blink State
                            //  on last entry.
                            return  MAIN_Make_Return_Value(LED_BLINK_2_SEC);
                        }
                        else
                        {
                            return  MAIN_Make_Return_Value(LED_BLINK_500_MS);
                        }
                    }
                #else
                    // If Alarm error (short blinks)
                    led_blink_count--;
                    FLAG_RED_LED_BLINK = 1;

                    if(0 == led_blink_count)
                    {
                        // Insert some delay before ending Blink State
                        //  on last entry.
                        return  MAIN_Make_Return_Value(LED_BLINK_2_SEC);
                    }
                    else
                    {
                        return  MAIN_Make_Return_Value(LED_BLINK_500_MS);
                    }
                #endif
            }
        }
        break;
        
        default:
            rled_state = STATE_LED_INIT;
        break;


    }
    return  MAIN_Make_Return_Value(LED_BLINK_100_MS);
}

/*
+------------------------------------------------------------------------------
| Function:      ALED_Manager_Task
+------------------------------------------------------------------------------
| Purpose:       Handles Amber LED State Control and Priorities
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Amber LED Task Next Execution Time (in Task TICs)
+------------------------------------------------------------------------------
*/

uint ALED_Manager_Task(void)
{
    #ifdef DBUG_SERIAL_OUTPUT_ALED_STATE
        // Test Only
        SER_Send_String("ALED Stat");
        SER_Send_Int('e', aled_state, 10);
    #endif

  #ifdef CONFIGURE_DEBUG_ALED_STATES
    // Test Only
    for(uchar i = (aled_state + 1); i > 0; i-- )
    {
        // Test Only
        MAIN_TP2_On();
        PIC_delay_us(DBUG_DLY_25_USec);
        MAIN_TP2_Off();
        PIC_delay_us(DBUG_DLY_10_USec);
    }
  #endif

    #ifdef CONFIGURE_DIAG_AMB_LED_STATES
        if(aled_prev_state != aled_state)
        {
            // Test Only
            SER_Send_String("\rALED Stat");
            SER_Send_Int('e', aled_state, 10);
            SER_Send_String("\r");
            aled_prev_state = aled_state;
        }
    #endif
    
    switch (aled_state)
    {
        case STATE_LED_INIT:
        {
            #ifdef CONFIGURE_CO
                if( !FLAG_CALIBRATION_COMPLETE || FLAG_CALIBRATION_UNTESTED )
                {
                    // Remain in this state
                    break;
                }
            #endif

            if(FLAG_NO_SMOKE_CAL || FLAG_LOW_BATTERY_CAL_MODE)
            {
                // Remain in this state
                break;
            }

            // proceed if Unit is calibrated
            aled_state = STATE_LED_NORM_BLINKING;

        }
        break;

        case STATE_LED_NORM_BLINKING:
        {
            /*
             * FLAG_ALED_NOT_IDLE is only set when changing states to Active 
             * Mode Only states. Leave it cleared for states to be run
             * in Low Power mode.
             */
            FLAG_ALED_NOT_IDLE = 0;
            
            // If in Alarm, no Amber Blinks
            //  since Amber is created from Red and Green LEDs
            if( !FLAG_SMOKE_ALARM_ACTIVE && !FLAG_CO_ALARM_ACTIVE &&
                !FLAG_BAT_CONSRV_MODE_ACTIVE )
            {
                    
                if(FLAG_LGT_MEAS_CYCLE)
                {
                    // No LED activity during Light Sampling
                    break;
                }
                
                if(FLAG_SMOKE_HUSH_ACTIVE)
                {
                    // Hush is handled in Red Led Hush State
                    break;
                }
                
                //Some of these Fault Modes can start immediately
                // while others only begin if Day is detected
                if(FLAG_EOL_FATAL)
                {
                    // Begin Amber Led 5 Second Blinking
                    aled_state = STATE_LED_AMB_5_SEC_BLINK;
                    break;
                }
                
                if(FLAG_PTT_ACTIVE)
                {
                    // Let PTT control LED actions during PTT
                    break;
                }
            
                if(FLAG_FAULT_FATAL)
                {
                    // Enter Amber Led 5 Second Blinking  
                    aled_state = STATE_LED_AMB_5_SEC_BLINK;
                    break;
                }
                
                #ifdef CONFIGURE_WIFI
                    if(FLAG_AC_DETECT)
                    {
                        //
                        // Need to test for Search Mode before Profile Busy
                        // to guarantee operating with Red LED State correctly
                        //
                        if(FLAG_APP_SEARCH_MODE)
                        {
                            aled_state = STATE_LED_WL_SEARCH;
                            FLAG_ALED_NOT_IDLE = 1;
                            break;
                        }
                    }
                #else
                    //
                    // Need to test for Search Mode before Profile Busy
                    // to guarantee operating with Red LED State correctly
                    //
                    if(FLAG_APP_SEARCH_MODE)
                    {
                        aled_state = STATE_LED_WL_SEARCH;
                        FLAG_ALED_NOT_IDLE = 1;
                        break;
                    }
                #endif
                
                if(FLAG_LED_PROFILE_BUSY || FLAG_APP_DISPLAY_NETWORK_SIZE)
                {
                    // No LED Blink Control if other LED Activity
                    //  needs waiting on
                    break;
                }

                #ifdef CONFIGURE_WIRELESS_MODULE

                    // If in Out of Box after sending an OOB request
                    // Execute the OOB LED profile
                    if( FLAG_LED_OOB_PROFILE_ENABLE &&
                        !FLAG_COMM_RFD && !FLAG_COMM_COORDINATOR )
                    {
                        profile_state = STATE_PROFILE_FADE_ON;
                        FLAG_LED_PROFILE_BUSY = 1;
                        aled_state = STATE_LED_WL_OOB;
                        FLAG_ALED_NOT_IDLE = 1;
                        // Begin with the 2 Red Cycles 1st
                        cycle_count = 2;

                        FLAG_LED_OOB_PROFILE_ENABLE = 0;
                        
                    }
                    // Start the ID LED profile
                    else if(FLAG_ALARM_IDENTIFY)
                    {
                        // Begin LED Profile in 500 ms
                        profile_state = STATE_PROFILE_INTERVAL;
                        profile_int_count = LED_PROF_INT_500_MS;
                        FLAG_LED_PROFILE_BUSY = 1;
                        aled_state = STATE_LED_WL_IDENTIFY;
                        FLAG_ALED_NOT_IDLE = 1;
                        
                        // Init 3 minute ID Timer (use search Mode timer)
                        id_mode_timer = 0;
                        // Init 2 second timer (use led_alert_count)
                        led_alert_count = 0;
                    }
                #endif

                // Non-fatal EOL Modes are lower priority than
                //  Network Join/Search Modes
                else if( FLAG_EOL_MODE || FLAG_EOL_HUSH_ACTIVE )
                {
                    if(!FLAG_SOUND_INHIBIT)
                    {
                        // Enter Amber Led Fault Mode, if Day or Light ALG Off
                        aled_state = STATE_LED_AMB_5_SEC_BLINK;
                        break;
                    }
                }

                // Amber LED now controls LB Trouble indicators    
                else if(FLAG_LOW_BATTERY || FLAG_LB_HUSH_ACTIVE )
                {
                    // Must be Day or Light ALG Off to enter
                    if(!FLAG_SOUND_INHIBIT)
                    {
                        // LB Trouble State
                        aled_state = STATE_LED_LOW_BATTERY;
                        break;
                    }
                }

                #ifdef CONFIGURE_WIRELESS_MODULE
                    #ifndef CONFIGURE_NO_REMOTE_LB
                        else if(FLAG_STATUS_RMT_LB_ACTIVE)
                        {
                            // Must be Day or Light ALg Off to enter
                            if(!FLAG_SOUND_INHIBIT)
                            {
                                // LB Trouble State
                                aled_state = STATE_LED_LOW_BATTERY;
                                break;
                            }
                        }
                    #endif
                #endif

                //Some of these Fault Modes can start immediately
                // while others only begin if Day is detected
                else if(FLAG_NETWORK_ERROR_MODE)
                {
                    // Begin Amber Led 5 Second Blinking
                    aled_state = STATE_LED_AMB_5_SEC_BLINK;
                    break;
                }
                
            } // if Alarm or battery Conserve
        }
        break;

        case STATE_LED_AMB_5_SEC_BLINK:
        {
            //
            // Conditions that use the Amber LED 5_Second Blink indicator:
            // Fatal Fault, EOL Mode, EOL Hush, EOL Fatal, Network Error,
            // Network Error Hush
            //
            if( !FLAG_NETWORK_ERROR_HUSH_ACTIVE && !FLAG_FAULT_FATAL && 
                !FLAG_NETWORK_ERROR_MODE && !FLAG_EOL_FATAL && 
                !FLAG_EOL_MODE && !FLAG_EOL_HUSH_ACTIVE )
            {
                // Exit Amber blinking if 5 second fault or trouble conditions 
                //  ends. Return to Idle 
                aled_state = STATE_LED_NORM_BLINKING;
                break;
            }
            
            if( FLAG_SMOKE_ALARM_ACTIVE || FLAG_CO_ALARM_ACTIVE ||
                FLAG_PTT_ACTIVE || FLAG_SMOKE_HUSH_ACTIVE)
            {
                // Exit Amber blinking during Alarm, sound needs LED control
                // Return to Idle 
                aled_state = STATE_LED_NORM_BLINKING;
                break;
            }
            
            // 
            // Low Battery state takes precedence over Network Error 
            //
            if(FLAG_NETWORK_ERROR_MODE || FLAG_NETWORK_ERROR_HUSH_ACTIVE) 
            {
                // EOL states still have priority over Low Battery state
                if(FLAG_LOW_BATTERY && !FLAG_EOL_FATAL && !FLAG_EOL_MODE &&
                        !FLAG_EOL_HUSH_ACTIVE)
                {
                    // Low Battery while in Network Error State
                    aled_state = STATE_LED_NORM_BLINKING;
                    break;
                }
                
                #ifdef CONFIGURE_WIRELESS_MODULE
                    #ifndef CONFIGURE_NO_REMOTE_LB
                        // EOL states still have priority over Low Battery 
                        if(FLAG_STATUS_RMT_LB_ACTIVE && !FLAG_EOL_FATAL && 
                                !FLAG_EOL_MODE && !FLAG_EOL_HUSH_ACTIVE)
                        {
                            // Low Battery while in Network Error State
                            aled_state = STATE_LED_NORM_BLINKING;
                            break;
                        }
                    #endif
                #endif
                
            }
                
            // No Amber 5 Second Blinking while Red Error Code is Blinking
            if(FLAG_REQ_ERROR_CODE_BLINK  || FLAG_ERROR_CODE_BLINKING ||
                    FLAG_APP_DISPLAY_NETWORK_SIZE)
            {
                // Amber does nothing while Red Code Blinks are Active
                
            }
            else if( (FLAG_EOL_MODE || FLAG_EOL_HUSH_ACTIVE)  &&
                     (FLAG_APP_JOIN_MODE || FLAG_APP_SEARCH_MODE) )
            {
                // Allow Join or Search LED profiles if EOL Mode or EOL Hush
                aled_state = STATE_LED_NORM_BLINKING;

                // Maintain active Mode after profile ends
                FLAG_ACTIVE_TIMER = 1;
                if(MAIN_ActiveTimer < TIMER_ACTIVE_MINIMUM_2_SEC)
                {
                    // Extend time to remain in Active Mode
                    MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_2_SEC;
                }
            }
            else
            {
                // Code to Support Low Power Blink
                if(FLAG_MODE_ACTIVE)
                {
                    // Amber LB Trouble Blinks
                    FLAG_AMBER_LED_BLINK = 1;
                }
                else
                {
                    // Execute a Low Power Amber Blink
                    GREEN_LED_ON
                    RED_LED_ON
                    PIC_delay_ms(LED_BLINK_TIME_50MS);
                    RED_LED_OFF
                    GREEN_LED_OFF
                }
                // Blink every 5 seconds as Error Indication
                return MAIN_Make_Return_Value(LED_BLINK_5_SEC);

            }
        }
        break;
        
        case STATE_LED_LOW_BATTERY:
        {
            #ifdef CONFIGURE_WIRELESS_MODULE
                if(  FLAG_SMOKE_ALARM_ACTIVE || FLAG_CO_ALARM_ACTIVE ||
                      FLAG_BAT_CONSRV_MODE_ACTIVE || FLAG_SMOKE_HUSH_ACTIVE ||
                      FLAG_EOL_MODE  || FLAG_FAULT_FATAL || FLAG_PTT_ACTIVE ||
                      FLAG_EOL_HUSH_ACTIVE || FLAG_APP_JOIN_MODE ||
                      FLAG_LED_OOB_PROFILE_ENABLE || FLAG_APP_SEARCH_MODE ||
                      FLAG_ALERT_PANIC || FLAG_ALERT_WEATHER || 
                      FLAG_ALERT_DETECT || FLAG_ALARM_IDENTIFY ||  
                      (!FLAG_LOW_BATTERY && !FLAG_LB_HUSH_ACTIVE &&
                       !FLAG_STATUS_RMT_LB_ACTIVE) )
            #else
                if( FLAG_SMOKE_ALARM_ACTIVE || FLAG_CO_ALARM_ACTIVE ||
                    FLAG_BAT_CONSRV_MODE_ACTIVE ||
                    FLAG_EOL_MODE  || FLAG_FAULT_FATAL || FLAG_PTT_ACTIVE ||
                    FLAG_EOL_HUSH_ACTIVE ||
                   (!FLAG_LOW_BATTERY && !FLAG_LB_HUSH_ACTIVE) )
            #endif
            {
                // Exit to Idle if Low Battery Conditions End
                //  or EOL or Alarm becomes Active
                aled_state = STATE_LED_NORM_BLINKING;

            }
            // No LB Blinking while Error Code is Blinking
            else if(FLAG_REQ_ERROR_CODE_BLINK || FLAG_ERROR_CODE_BLINKING)
            {
                // Amber does nothing, Allow Red LED Error Code Blinks 
                
            }
            else
            {
                if(FLAG_MODE_ACTIVE)
                {
                    // Amber LB Trouble Blinks
                    FLAG_AMBER_LED_BLINK = 1;
                }
                else
                {
                    // Execute a Low Power Amber Blink
                    GREEN_LED_ON
                    RED_LED_ON
                    PIC_delay_ms(LED_BLINK_TIME_50MS);
                    RED_LED_OFF
                    GREEN_LED_OFF
                }
                // Blink every 5 seconds as Error Indication
                return MAIN_Make_Return_Value(LED_BLINK_5_SEC);

            }
        }
        break;

    #ifdef CONFIGURE_WIRELESS_MODULE
        case STATE_LED_WL_SEARCH:
        {
            
            if( FLAG_APP_SEARCH_MODE &&
                !FLAG_FAULT_FATAL &&
                !FLAG_CO_ALARM_ACTIVE && !FLAG_SMOKE_ALARM_ACTIVE )
            {
                // We are in Network Search Mode
                // No Amber LED action, hang here until search complete
            }
            else
            {
                // Allow Profile to Complete
                if(!FLAG_LED_PROFILE_BUSY)
                {
                    // RFD Search ended or needs to be Aborted
                    aled_state = STATE_LED_NORM_BLINKING;
                }
            }
        }
        break;

        case STATE_LED_WL_OOB:
        {
            // Short State to handle OOB LED profile
            // Profile.  cycle_count must be pre-set by previous state
            // This controls Red and Green Leds even though it is run
            //  from Amber LED handler

            // We are still in Network Search Mode
            // Run LED OOB Profile
            if(!FLAG_LED_PRO_N_PROGRESS)
            {
                switch(profile_state)
                {
                    case STATE_PROFILE_FADE_ON:
                    {
                        led_set_profile(LED_PRO_FADE_ON_RED_2);
                        profile_state = STATE_PROFILE_FADE_OFF;
                    }
                    break;

                    case STATE_PROFILE_FADE_OFF:
                    {
                        led_set_profile(LED_PRO_FADE_OFF_RED_2);
                        profile_state = STATE_PROFILE_INTERVAL;
                        profile_int_count = LED_PROF_INT_100_MS;
                    }
                    break;

                    case STATE_PROFILE_INTERVAL:
                    {
                        if(0 == --profile_int_count)
                        {
                            if(0 == --cycle_count)
                            {
                                // 1 cycle of Green Next
                                profile_state = STATE_PROFILE_FADE_ON_2;
                                cycle_count = 1;
                                
                                #ifdef CONFIGURE_VOICE
                                    // Output OOB Reset Sound
                                    VCE_Play(VCE_SND_RETURN_TO_OOB);                                    
                                #endif

                            }
                            else
                            {
                                // 2 cycles of Red
                                profile_state = STATE_PROFILE_FADE_ON;
                            }
                        }
                    }
                    break;

                    case STATE_PROFILE_FADE_ON_2:
                    {
                        led_set_profile(LED_PRO_FADE_ON_GRN_2);
                        profile_state = STATE_PROFILE_FADE_OFF_2;
                    }
                    break;

                    case STATE_PROFILE_FADE_OFF_2:
                    {
                        led_set_profile(LED_PRO_FADE_OFF_GRN_2);
                        profile_state = STATE_PROFILE_INTERVAL_2;
                        // Add some delay before Requesting Join Mode
                        profile_int_count = LED_PROF_INT_2_SEC;
                    }
                    break;

                    case STATE_PROFILE_INTERVAL_2:
                    {
                        if(0 == --profile_int_count)
                        {
                            if(0 == --cycle_count)
                            {
                                // 1 cycle of Green (we're done!)
                                // Set LED Profiles Off
                                led_set_profile(LED_PRO_NONE);

                                // OOB is done
                                // Exit this State
                                ALED_Off();

                                #ifndef CONFIGURE_OOB_RST_ALWAYS_REJOIN

                                    // If Search was aborted to OOB Reset by 
                                    //  user then we do not re-enter search 
                                    //  again. Run in stand alone mode
                                    if(!FLAG_OOB_RST_WITH_NO_REJOIN)
                                    {
                                        // Issue a new Join request after OOB 
                                        //  profile has completed
                                        FLAG_REQ_ENABLE_JOIN = 1;
                                    }
                                    else
                                    {
                                        // Reset NO Re-Join Flag 
                                        FLAG_OOB_RST_WITH_NO_REJOIN = 0;
                                    }
                                
                                #else

                                    // Reset NO Re-Join Flag 
                                    FLAG_OOB_RST_WITH_NO_REJOIN = 0;

                                    // Issue a new Join request after OOB 
                                    //  profile has completed
                                    FLAG_REQ_ENABLE_JOIN = 1;
                                    
                                #endif
                                
                                // Clear Previously Joined Flag since in OOB
                                FLAG_PREV_JOINED = 0;
                                
                                // return to Join state from Normal Blinking
                                aled_state = STATE_LED_NORM_BLINKING;
                                
                                // Keep in Active Mode
                                FLAG_ACTIVE_TIMER = 1;
                                if(MAIN_ActiveTimer < 
                                        TIMER_ACTIVE_MINIMUM_6_SEC)
                                {
                                    // Extend time to remain in Active Mode
                                    MAIN_ActiveTimer = 
                                            TIMER_ACTIVE_MINIMUM_6_SEC;
                                }
                            }
                            else
                            {
                                // Start Again if we want to re-cycle OOB
                                // profile
                                profile_state = STATE_PROFILE_FADE_ON;
                                cycle_count = 2;
                            }
                        }
                    }
                    break;
                }
            }
        }
        break;
        
        
        case STATE_LED_WL_IDENTIFY:
        {
            // Alarm Identify State
            
            // 3 minute ID Timer
            if(LED_ID_TIMER_3_MIN <= ++id_mode_timer)
            {
                FLAG_ALARM_IDENTIFY = 0;
            }
            else
            {
                // Sound ID Ping
                #ifdef CONFIGURE_UNDEFINE_1_MINUTE_PING
                    // Removed from requirement
                    //ID Locate Ping every 2 seconds for 1st minute ID mode
                    if(LED_ID_TIMER_1_MIN > id_mode_timer)
                    {
                        //ID Locate Chime every 2 seconds for 1st minute
                        if(LED_ID_TIMER_2_SEC <= ++led_alert_count)
                        {
                            //Reset Timer
                            led_alert_count=0;
                            // and Play Locate Sound
                            VCE_Play(VCE_SND_SONAR_PING);
                        }
                    }
                #else
                    //ID Locate Ping every 2 seconds for duration of ID mode
                    if(LED_ID_TIMER_2_SEC <= ++led_alert_count)
                    {
                        //Reset Timer
                        led_alert_count=0;
                        // and Play Locate Sound
                        VCE_Play(VCE_SND_SONAR_PING);
                    }
                #endif
            }
            
            if( FLAG_ALARM_IDENTIFY &&
                !FLAG_CO_ALARM_ACTIVE && !FLAG_SMOKE_ALARM_ACTIVE )
            {
                // We are still in ID Mode
                // Run Amber LED Search Profile
                if(!FLAG_LED_PRO_N_PROGRESS)
                {
                    switch(profile_state)
                    {
                        case STATE_PROFILE_FADE_ON:
                        {
                            led_set_profile(LED_PRO_FADE_ON_AMB_1);
                            profile_state = STATE_PROFILE_FADE_OFF;
                        }
                        break;

                        case STATE_PROFILE_FADE_OFF:
                        {
                            led_set_profile(LED_PRO_FADE_OFF_AMB_1);
                            profile_state = STATE_PROFILE_INTERVAL;

                            profile_int_count = LED_PROF_INT_1_SEC;
                        }
                        break;

                        case STATE_PROFILE_INTERVAL:
                        {
                            if(0 == --profile_int_count)
                            {
                                profile_state = STATE_PROFILE_FADE_ON;
                            }
                        }
                        break;

                    }
                }
            }
            else
            {
                // Allow Last Profile to Complete, then exit
                if(!FLAG_LED_PRO_N_PROGRESS)
                {
                    FLAG_ALARM_IDENTIFY = 0;
                    
                    // Set LED Profiles Off
                    led_set_profile(LED_PRO_NONE);

                    // Search Mode is inactive or we entered Alarm
                    // Exit this State
                    ALED_Off();
                    aled_state = STATE_LED_NORM_BLINKING;

                    // Extend Active Mode
                    FLAG_ACTIVE_TIMER = 1;
                    // Minimum time to remain in Active Mode
                    if(MAIN_ActiveTimer < TIMER_ACTIVE_MINIMUM_2_SEC)
                    {
                        // Minimum time to remain in Active Mode
                        MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_2_SEC;
                    }
                }
            }
        }
        break;
        
    #endif

        default:
            aled_state = STATE_LED_INIT;
        break;

    }
    
    // Determine Default Return Time 
    if(FLAG_MODE_ACTIVE)
    {
        return MAIN_Make_Return_Value(LED_BLINK_100_MS);
    }
    else
    {
        // For Low Power Mode operation
        return	MAIN_Make_Return_Value(LED_BLINK_5_SEC);
    }
    
}



/*
+------------------------------------------------------------------------------
| Functions:     LED On and Off Functions
+------------------------------------------------------------------------------
| Purpose:       Sets Green, Red and Amber Led On and Off
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void GLED_On(void)
{
    //GREEN_LED_ON
    FLAG_GREEN_LED_ON = 1;
}

void GLED_Off(void)
{
    //GREEN_LED_OFF
    FLAG_GREEN_LED_ON = 0;
}

void RLED_On(void)
{
    //RED_LED_ON
    FLAG_RED_LED_ON = 1;
}

void RLED_Off(void)
{
    //RED_LED_OFF
    FLAG_RED_LED_ON = 0;

}
void ALED_On(void)
{
    //AMBER_LED_ON
    FLAG_AMBER_LED_ON = 1;
}

void ALED_Off(void)
{
    //AMBER_LED_OFF
    FLAG_AMBER_LED_ON = 0;
}


/*
+------------------------------------------------------------------------------
| Functions:     GLED_Rst
+------------------------------------------------------------------------------
| Purpose:       Reset Green LED to Off State
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void GLED_Rst(void)
{
    gled_state = STATE_LED_NORM_BLINKING;
    FLAG_GREEN_LED_ON = 0;
}


/*
+------------------------------------------------------------------------------
| Functions:     LED_LP_green_blink
+------------------------------------------------------------------------------
| Purpose:       Low Power Green LED Blink
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void LED_LP_green_blink(void)
{
    #ifndef CONFIGURE_LGT_SENSOR_PWR_IND 

        /* NOTE:  BC
         * Oscillator Switch Code has not been tested with the New PIC
         * So CONFIGURE_LED_LP_CLOCK_SWITCH has not yet been developed
         */
    
        #ifdef CONFIGURE_LED_LP_CLOCK_SWITCH
            // Switch to LP Clock (128 usec/instruction)

            //Switch to Low Power OSC (32 KHZ)
            OSCCON1 = OSC_32KHZ_XTAL;

            while(!OSCCON3bits.ORDY)
            {
                // Wait for Switch to Complete
            }
        #endif

        #ifndef CONFIGURE_LED_LP_CLOCK_SWITCH    
            // If Light Detected use normal Blink   
            if(FLAG_LED_DIM)
            {
                GREEN_LED_ON
                for(uint i = 0; i < 100; i++)
                {
                    // Delay ~ 2.5 usec/count
                }
                GREEN_LED_OFF
            }
            else
            {
                GREEN_LED_ON
                for(uint i = 0; i < 1000; i++)
                {
                    // Delay ~ 2.5 usec/count
                }
                GREEN_LED_OFF
            }
        #else
            // If Light Detected use normal Blink   
            if(FLAG_LED_DIM)
            {
                GREEN_LED_ON
                for(uint i = 0; i < 3; i++)
                {
                    // Delay about 4ms, (=~ 1.466 ms / loop)
                }
                GREEN_LED_OFF
            }
            else
            {
                GREEN_LED_ON
                for(uint i = 0; i < 10; i++)
                {
                    // Delay about 14ms, (=~ 1.466 ms / loop)
                }
                GREEN_LED_OFF
            }
        #endif


        #ifdef CONFIGURE_LED_LP_CLOCK_SWITCH
            // Switch OSC back to 16MHZ
            OSCCON1 = OSC_HF_INT_OSC;

            while(!OSCCON3bits.ORDY)
            {
                // Wait for Switch to Complete
            }

        #endif

    #else  
        
          // If Dim Detected Blink Light Sensor LED   
        if(FLAG_LED_DIM)
        {
            // Flash Light Sensor LED next measurement
            // IN LP Mode Light is sampled every 30 seconds
            FLAG_BLINK_LIGHT_SENS_LED = 1;
        }
        else
        {
            // Light Detected, Blink Light Pipe Ring 
            GREEN_LED_ON
            for(uint i = 0; i < 1000; i++)
            {
                // Delay ~ 2.5 usec/count
            }
            GREEN_LED_OFF
        }
         
    #endif
}


/*
+------------------------------------------------------------------------------
| Function:     LED_Blink_Task
+------------------------------------------------------------------------------
| Purpose:      LED Blink control
|               Written so that only one color LED is ON at a time 
|               (common light pipe)
|               In active mode, states will cycle between ON and OFF 
|                each state time
|               LP Mode does not use this task to blink
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Blink LED Task Next Execution Time (in Task TICs)
+------------------------------------------------------------------------------
*/

uint LED_Blink_Task(void)
{
    
    switch (led_blink_state)
    {
        case STATE_LED_BLINK_ON:
        {
            // Default to next state
            led_blink_state = STATE_LED_BLINK_OFF;

            
            // Test LED Blink Request Flags
            if(FLAG_RED_LED_BLINK)
            {
                FLAG_RED_LED_BLINK = 0;

                if(FLAG_MODE_ACTIVE)
                {
                    FLAG_LED_BLINKS_ACTIVE = 1;

                    GREEN_LED_OFF
                    RED_LED_ON

                    // Longer Blink Time for Active Mode
                    return MAIN_Make_Return_Value(LED_BLINK_100_MS);
                }
                else
                {
                    // All LEDs should be normally Off in standby
                    // Standby Mode Blink
                    RED_LED_ON
                    PIC_delay_ms(LED_BLINK_TIME_50MS);
                    RED_LED_OFF

                }
            }
            else if(FLAG_AMBER_LED_BLINK)
            {
                FLAG_AMBER_LED_BLINK = 0;

                if(FLAG_MODE_ACTIVE)
                {
                    FLAG_LED_BLINKS_ACTIVE = 1;

                    // Set Flag to tell INT routine when to Dim Green LED
                    //   if Green LED DIM bit is active
                    FLAG_AMBER_LED_PULSE = 1;
                    
                    // Green On 1st
                    GREEN_LED_ON
                    PIC_delay_ms(2);
                    RED_LED_ON

                    // Longer Blink Time for Active Mode
                    return MAIN_Make_Return_Value(LED_BLINK_100_MS);
                }
                else
                {
                    // All LEDs should be normally Off in standby
                    // Standby Mode Blink
                    GREEN_LED_ON
                    PIC_delay_ms(10);
                    RED_LED_ON
                    PIC_delay_ms(LED_BLINK_TIME_30MS);
                    RED_LED_OFF
                    PIC_delay_ms(10);
                    GREEN_LED_OFF
                }
            }
            else if(FLAG_GREEN_LED_BLINK)
            {
                FLAG_GREEN_LED_BLINK = 0;

                if(FLAG_MODE_ACTIVE)
                {
                    FLAG_LED_BLINKS_ACTIVE = 1;

                    // Set Flag to tell INT routine when to Dim Green LED
                    //   if Green LED DIM bit is active
                    FLAG_GREEN_LED_PULSE = 1;

                    RED_LED_OFF
                    GREEN_LED_ON

                    // Longer Blink Time for Active Mode
                    return MAIN_Make_Return_Value(LED_BLINK_50_MS);
                }
                else
                {
                    // All LEDs should be normally Off in standby
                    // Standby Mode Blink
                    LED_LP_green_blink();                
                }
                
            }
            else
            {
                // No Blinks proceed to next state
                FLAG_LED_BLINKS_ACTIVE = 0;
                return MAIN_Make_Return_Value(LED_BLINK_10_MS);
            }
            
        }

        return MAIN_Make_Return_Value(LED_BLINK_50_MS);


        // This state should complete the blink and restore
        // Leds back to their ON or OFF state by priority. (RED highest)
        case STATE_LED_BLINK_OFF:
        {
            
            // End of Active Mode, Green LED Pulse
            FLAG_GREEN_LED_PULSE = 0;
            FLAG_AMBER_LED_PULSE = 0;

            // Test LED ON/OFF Status Flags
            if( FLAG_RED_LED_ON &&
               (CAL_STATE_FINAL_COMPLETE == CO_Cal_Get_State() ) )
            {
                // For FHK manufacturing
                // Allow Green LED and Red LED to both be ON for 400 PPM test
                RED_LED_ON
            }
            else if(FLAG_RED_LED_ON)
            {
                GREEN_LED_OFF
                RED_LED_ON

            }
            // Here, if in GLED Dim, do not turn Amber LED back on
            else if( FLAG_AMBER_LED_ON && !FLAG_LED_DIM )
            {
                GREEN_LED_ON
                RED_LED_ON

            }
            else if( FLAG_AMBER_LED_ON && FLAG_LED_DIM )
            {
                if(FLAG_PTT_ACTIVE)
                {
                    // During PTT We Ignore DIM feature to
                    // Drive All LEDS Bright
                    GREEN_LED_ON
                    RED_LED_ON
                }
                else
                {
                    //  Otherwise, Allow Dim feature to control both 
                    //  Red and Green.
                    
                }
                
            }
            // Here, if in GLED Dim, do not turn Green LED back on
            else if( FLAG_GREEN_LED_ON && !FLAG_LED_DIM )
            {
                RED_LED_OFF
                GREEN_LED_ON
            }
            else if( FLAG_GREEN_LED_ON && FLAG_LED_DIM )
            {
                if(FLAG_PTT_ACTIVE)
                {
                    // During PTT We Ignore DIM feature to
                    // Drive All LEDS Bright
                    RED_LED_OFF
                    GREEN_LED_ON
                }
                else
                {
                    //  Otherwise, Allow Dim feature to control both 
                    //  Green.
                    RED_LED_OFF
                    
                }
            }
            else
            {
                // Turn Off All Leds (they should be off in this case
                // Turn Off Red 1st
                RED_LED_OFF
                PIC_delay_ms(2);
                GREEN_LED_OFF
            }

            led_blink_state = STATE_LED_BLINK_ON;
        }
        break;

        default:
            gled_state = STATE_LED_BLINK_ON;
            break;

    }

    // Active mode, Led Blink Time
    return MAIN_Make_Return_Value(LED_BLINK_10_MS);

}


/*
+------------------------------------------------------------------------------
| Function:  LED_Init_PWM
+------------------------------------------------------------------------------
| Purpose:    Init PWM channels to Drive Red and Green LEDs
|             
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
+------------------------------------------------------------------------------
*/

void LED_Init_PWM(void)
{
        // Init PWM Channel for Green LED profiles
        // Using Timer 6 and CCP1, output on RC2 Pin

        // This is different from the LF1939
        // Select Timer 6 as CCP1 and CCP4 PWM Clock Source
        // bits - 10 00 00 10
        //CCPTMRS0 = 0x82;

        // bits - 11 00 00 11
        CCPTMRS0 = 0xC3;

        //CCP1 Control
        // Disabled, PWM Off, Left Align
        CCP1CON = 0x10;

        // Init Duty Cycle (LED On Pulse Width = 00 = Off)
        CCPR1 = 0x00;
        
        // Init PWM Channel for Red LED profiles
        
        // CCP4 output has been routed to RD1 pin in main Initialization
        // CCP4 Control
        // Disabled, PWM Off, Left Align
        CCP4CON = 0x10;

        // Init Duty Cycle (LED On Pulse Width = 00 = Off)
        CCPR4 = 0x00;

        // Setup Timer 6  (LED Source Timer)
        PIR4bits.TMR6IF = 0;        // INTs Off
        PIE4bits.TMR6IE = 0;

        // Set PWM Period
        PR6 = (uchar)250;    //250 x 16uSec = 4.000 ms Period

        // T6 Clock source = Fosc/4
        // 0b00000001
        T6CLKCON = 0x01;
                
        // T6 Control
        // Post-scaler = 0000 = 1:1
        // Timer On = 1
        // Pre-scale Select = 110 = 64
        // This results in a 16uSec PWM clock
        // 0b11100000
        T6CON = 0xE0;

        FLAG_LED_PRO_N_PROGRESS = 0;
}


/*
+------------------------------------------------------------------------------
| Function:  LED_PWM_Control   
+------------------------------------------------------------------------------
| Purpose:   LED Control for Green, Red and Amber PWM profiles  
|            Called from Main Interrupt Routine  
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
+------------------------------------------------------------------------------
*/

void LED_PWM_Control(void)
{
    // Hardware PWM Used for LED profiles
    // Here is where LED Light profiles are serviced
    if(FLAG_LED_PWM_ENABLE)
    {
        
        // Once FLAG_LED_PRO_N_PROGRESS is cleared, no need
        //  to update PWM (leave it in it's last state)
        if(FLAG_LED_PRO_GRN && FLAG_LED_PRO_N_PROGRESS)
        {
           
            if(0 == --led_pwm_dwell_count_grn)
            {
                
                // The Green LED PWM Interval is complete, determine next index
                if(FLAG_LED_PRO_DIR_GRN)
                {
                    // The PWM Dwell Time is Complete
                    // Init Next Profile data
                    led_profile.index_grn++;

                    // Profile Completed? (End of Profile table)
                    if(LED_PROFILE_END ==
                        led_profile.p_addr[led_profile.index_grn].grn_cnt_on)
                    {
                        // Maintain previous profile PWM cycle
                        // PWM will continue to run until
                        // FLAG_LED_PWM_ENABLE is cleared
                        led_profile.index_grn--;

                        // Profile Completed
                        // This can be monitored if we want to
                        // stack profiles (ie. Fade On, Fade Off
                        FLAG_LED_PRO_N_PROGRESS = 0;

                    }
                }
                else
                {
                    // The PWM Dwell Time is Complete
                    // Init Next Profile data
                    led_profile.index_grn--;

                    // Profile Completed? (End of Profile table)
                    if(LED_PROFILE_END ==
                      led_profile.p_addr[led_profile.index_grn].grn_cnt_on)
                    {
                        // Maintain previous profile PWM cycle
                        // PWM will continue to run until
                        // FLAG_LED_PWM_ENABLE is cleared
                        led_profile.index_grn++;

                        // Profile Completed
                        // This can be monitored if we want to
                        // stack profiles (ie. Fade On, Fade Off
                        FLAG_LED_PRO_N_PROGRESS = 0;

                    }
                }

                if(FLAG_LED_PRO_N_PROGRESS)
                {
                    CCPR1H =
                      led_profile.p_addr[led_profile.index_grn].grn_cnt_on;
                    
                    CCPR1L = 0x00;
                    
                    if(FLAG_LED_PRO_FAST)
                    {
                        // in Half the Time
                        led_pwm_dwell_count_grn =
                          (led_profile.p_addr[led_profile.index_grn].cnt_pwm
                                >> 1);
                    }
                    else if(FLAG_LED_PRO_SLOW)
                    {
                        // in Twice the Time
                        led_pwm_dwell_count_grn =
                          (led_profile.p_addr[led_profile.index_grn].cnt_pwm
                                << 1);
                    }
                    else
                    {
                         led_pwm_dwell_count_grn =
                           led_profile.p_addr[led_profile.index_grn].cnt_pwm;

                    }
                
                }

            }
        }        
    
        // Once FLAG_LED_PRO_N_PROGRESS is cleared, no need
        //  to update PWM (leave it in it's last state)
        if(FLAG_LED_PRO_RED && FLAG_LED_PRO_N_PROGRESS)
        {
            if(0 == --led_pwm_dwell_count_red)
            {
                // The RED LED PWM Interval is complete, determine next index
                if(FLAG_LED_PRO_DIR_RED)
                {
                    // The PWM Dwell Time is Complete
                    // Init Next Profile data
                    led_profile.index_red++;

                    // Profile Completed? (End of Profile table)
                    if(LED_PROFILE_END ==
                      led_profile.p_addr[led_profile.index_red].red_cnt_on)
                    {
                        // Maintain previous profile PWM cycle
                        // PWM will continue to run until
                        // FLAG_LED_PWM_ENABLE is cleared
                        led_profile.index_red--;

                        // Profile Completed
                        // This can be monitored if we want to
                        // stack profiles (ie. Fade On, Fade Off
                        FLAG_LED_PRO_N_PROGRESS = 0;

                    }
                }
                else
                {
                    // The PWM Dwell Time is Complete
                    // Init Next Profile data
                    led_profile.index_red--;

                    // Profile Completed? (End of Profile table)
                    if(LED_PROFILE_END ==
                      led_profile.p_addr[led_profile.index_red].red_cnt_on)
                    {
                        // Maintain previous profile PWM cycle
                        // PWM will continue to run until
                        // FLAG_LED_PWM_ENABLE is cleared
                        led_profile.index_red++;

                        // Profile Completed
                        // This can be monitored if we want to
                        // stack profiles (ie. Fade On, Fade Off
                        FLAG_LED_PRO_N_PROGRESS = 0;

                    }
                }

                if(FLAG_LED_PRO_N_PROGRESS)
                {
                    
                    // If Amber (Both Green and Red are being PWM'd)  then 
                    //  shorten the Red ON Time a bit
                    if(FLAG_LED_PRO_GRN)
                    {
                        if(led_profile.index_red > LED_PROFILE_IDX_10)
                        {
                            // Adjust PWM duty for Color selected
                            // Set next PWM Duty cycle and Interval
                            // Shorten Red Time
                            CCPR4H =
                             ( (led_profile.p_addr[led_profile.index_red].
                                    red_cnt_on) /(uchar)3 );

                        }
                        else
                        {
                            // Adjust PWM duty for Color selected
                            // Set next PWM Duty cycle and Interval
                            // Shorten Red Time
                            CCPR4H =
                             ( (led_profile.p_addr[led_profile.index_red].
                                    red_cnt_on) >>1 );
                            
                        }
                    }
                    else
                    {
                        // Adjust PWM duty for RED Color selected
                        // Set next PWM Duty cycle and Interval
                        CCPR4H =
                          led_profile.p_addr[led_profile.index_red].red_cnt_on;
                    }
                    
                    if(FLAG_LED_PRO_FAST)
                    {
                        // in Half the Time
                        led_pwm_dwell_count_red =
                          (led_profile.p_addr[led_profile.index_red].
                                cnt_pwm >> 1);
                    }
                    else if(FLAG_LED_PRO_SLOW)
                    {
                        // in Twice the Time
                        led_pwm_dwell_count_red =
                          (led_profile.p_addr[led_profile.index_red].
                                cnt_pwm << 1);
                    }
                    else
                    {
                         led_pwm_dwell_count_red =
                           led_profile.p_addr[led_profile.index_red].cnt_pwm;
                    }
                }
            }
        }


    }
    else
    {
        
        // Check for LED Dim PWM
        // If Green or Amber is On while FLAG_LED_DIM is enabled
        //  PWM the appropriate LEDs
        
        // If DIM is active, and no PTT and RED LED is OFF
        if( FLAG_LED_DIM && !FLAG_PTT_ACTIVE && 
            !FLAG_REMOTE_PTT_ACTIVE && !FLAG_RED_LED_ON )
        {
            // Test for Amber LED Dim Control
            if(FLAG_AMBER_LED_PULSE  || FLAG_AMBER_LED_ON)
            {
                // Set Red/Green Duty Cycles, Use a reduced pulse for Red
                //  when applying Amber
                CCPR4H = LED_AMBER_DIM_PWM;     
                CCPR1H = LED_GREEN_DIM_PWM;

                // Red and Green PWMs On
                led_select_pps(); //Route PWM to pins
                led_pwm_on();
            }
            
            // Test for Green LED Dim Control
            #ifdef CONFIGURE_UNDEFINED
                // If AC Powered, this code no longer performs GLED Dim
                // It is done in Light Module to Light Sensor LED
                else if(FLAG_GREEN_LED_PULSE  || FLAG_GREEN_LED_ON)
                {
                    // Set Duty Cycle
                    CCPR4H = 0;                   // Red PWM not needed
                    CCPR1H = LED_GREEN_DIM_PWM;   // Green Duty Cycle

                    // Green PWM On
                    RC2PPS = 0x09;   // Route Green PWM to pin
                    CCP1CON = 0x9F;
                }
            #endif

            else
            {
                // PWM Off if LED Off
                // CCPx control - disable PWM drives
                CCP4CON = 0x10;
                CCP1CON = 0x10;
                
               // Disable LED PWM drive to Pins
                led_select_latch();

            }

        }
        else
        {
            
            // PWM Off
            // CCPx control - disable PWM drives
            CCP4CON = 0x10;
            CCP1CON = 0x10;

            // Drive LEDs directly from PIC I/O Latch Outputs
            led_select_latch();

        }

    }

}

/*
+------------------------------------------------------------------------------
| Function:  LED_Get_Red_State   
+------------------------------------------------------------------------------
| Purpose:   Return current Red Led State 
|            
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  current RED LED Task Manager state
|
|
+------------------------------------------------------------------------------
*/

uchar LED_Get_Red_State(void)
{
    return (rled_state);
}

#ifdef CONFIGURE_WIRELESS_MODULE
#ifndef CONFIGURE_WIFI
/*
+------------------------------------------------------------------------------
| Function:  LED_Set_Join_Ping   
+------------------------------------------------------------------------------
| Purpose:   Set time for next Sonar Ping 
|            This allows Join Closed Requests to halt Sonar Pings for
|             a given period of time.
+------------------------------------------------------------------------------
| Parameters:    time - in seconds
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
+------------------------------------------------------------------------------
*/

void LED_Set_Join_Ping(uchar time)
{
    voice_ping_timer = time;
}
#endif
#endif


/*
+------------------------------------------------------------------------------
| Function:  void led_set_profile(uchar id)
+------------------------------------------------------------------------------
| Purpose:    Init an LED profile for Execution (ie. Fade On profile)
|             This is called to begin an LED profile (such as Fade On)
|             Led Profile is executed by Timer Interrupt Routine
|             FLAG_LED_PRO_N_PROGRESS is reset at completiong of LED Profile
|             FLAG_LED_PWM_ENABLE is reset to stop PWM LEd control
|              which re-enables LED State machine LED control (standard blinks)
|
+------------------------------------------------------------------------------
| Parameters:    id (profile to execute)
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
+------------------------------------------------------------------------------
*/

void led_set_profile(uchar id)
{
    if(!FLAG_LED_PRO_N_PROGRESS)
    {
        // ID of profile  (save, currently not used)
        led_profile.ID = id;

        // Init PWM and Start the LED Profile Timer
        // This is initialized one time in main initialization routine
        //LED_Init_PWM();

        switch(id)
        {
            case LED_PRO_FADE_ON_RED_1:
            {
                led_profile.p_addr = &PRO_FADE_LED1;

                // Select Color and rate
                FLAG_LED_PRO_RED = 1;
                FLAG_LED_PRO_GRN = 0;

                // Medium speed
                FLAG_LED_PRO_FAST = 0;
                FLAG_LED_PRO_SLOW = 0;

                // Forward Profile
                FLAG_LED_PRO_DIR_RED = 1;

            }
            break;

            case LED_PRO_FADE_OFF_RED_1:
            {
                led_profile.p_addr = &PRO_FADE_LED1;

                // Select Color and rate
                FLAG_LED_PRO_RED = 1;
                FLAG_LED_PRO_GRN = 0;
                FLAG_LED_PRO_FAST = 0;
                FLAG_LED_PRO_SLOW = 0;

                // Reverse Profile
                FLAG_LED_PRO_DIR_RED = 0;

            }
            break;

            
            case LED_PRO_FADE_ON_RED_2:
            {
                led_profile.p_addr = &PRO_FADE_LED1;

                // Select Color and rate
                FLAG_LED_PRO_RED = 1;
                FLAG_LED_PRO_GRN = 0;
                FLAG_LED_PRO_FAST = 1;
                FLAG_LED_PRO_SLOW = 0;

                // Forward Profile
                FLAG_LED_PRO_DIR_RED = 1;

            }
            break;


            case LED_PRO_FADE_OFF_RED_2:
            {
                led_profile.p_addr = &PRO_FADE_LED1;

                // Select Color and rate
                FLAG_LED_PRO_RED = 1;
                FLAG_LED_PRO_GRN = 0;
                FLAG_LED_PRO_FAST = 1;
                FLAG_LED_PRO_SLOW = 0;

                // Reverse Profile
                FLAG_LED_PRO_DIR_RED = 0;

            }
            break;
            
            case LED_PRO_FADE_ON_RED_3:
            {
                led_profile.p_addr = &PRO_FADE_LED1;

                // Select Color and rate
                FLAG_LED_PRO_RED = 1;
                FLAG_LED_PRO_GRN = 0;
                FLAG_LED_PRO_FAST = 0;
                FLAG_LED_PRO_SLOW = 1;

                // Forward Profile
                FLAG_LED_PRO_DIR_RED = 1;

            }
            break;


            case LED_PRO_FADE_OFF_RED_3:
            {
                led_profile.p_addr = &PRO_FADE_LED1;

                // Select Color and rate
                FLAG_LED_PRO_RED = 1;
                FLAG_LED_PRO_GRN = 0;
                FLAG_LED_PRO_FAST = 0;
                FLAG_LED_PRO_SLOW = 1;

                // Reverse Profile
                FLAG_LED_PRO_DIR_RED = 0;

            }
            break;


            case LED_PRO_FADE_ON_GRN_1:
            {
                led_profile.p_addr = &PRO_FADE_LED1;

                // Select Color and rate
                FLAG_LED_PRO_RED = 0;
                FLAG_LED_PRO_GRN = 1;
                FLAG_LED_PRO_FAST = 0;
                FLAG_LED_PRO_SLOW = 0;

                // Forward Profile
                FLAG_LED_PRO_DIR_GRN = 1;

            }
            break;

            case LED_PRO_FADE_OFF_GRN_1:
            {
                led_profile.p_addr = &PRO_FADE_LED1;

                // Select Color and rate
                FLAG_LED_PRO_RED = 0;
                FLAG_LED_PRO_GRN = 1;
                FLAG_LED_PRO_FAST = 0;
                FLAG_LED_PRO_SLOW = 0;

                // Reverse Profile
                FLAG_LED_PRO_DIR_GRN = 0;

            }
            break;

            
            case LED_PRO_FADE_ON_GRN_2:
            {
                led_profile.p_addr = &PRO_FADE_LED1;

                // Select Color and rate
                FLAG_LED_PRO_RED = 0;
                FLAG_LED_PRO_GRN = 1;
                FLAG_LED_PRO_FAST = 1;
                FLAG_LED_PRO_SLOW = 0;

                // Forward Profile
                FLAG_LED_PRO_DIR_GRN = 1;

            }
            break;

            case LED_PRO_FADE_OFF_GRN_2:
            {
                led_profile.p_addr = &PRO_FADE_LED1;

                // Select Color and rate
                FLAG_LED_PRO_RED = 0;
                FLAG_LED_PRO_GRN = 1;
                FLAG_LED_PRO_FAST = 1;
                FLAG_LED_PRO_SLOW = 0;

                // Reverse Profile
                FLAG_LED_PRO_DIR_GRN = 0;

            }
            break;

            case LED_PRO_FADE_ON_GRN_3:
            {
                led_profile.p_addr = &PRO_FADE_LED1;

                // Select Color and rate
                FLAG_LED_PRO_RED = 0;
                FLAG_LED_PRO_GRN = 1;
                FLAG_LED_PRO_FAST = 0;
                FLAG_LED_PRO_SLOW = 1;

                // Forward Profile
                FLAG_LED_PRO_DIR_GRN = 1;

            }
            break;

            case LED_PRO_FADE_OFF_GRN_3:
            {
                led_profile.p_addr = &PRO_FADE_LED1;

                // Select Color and rate
                FLAG_LED_PRO_RED = 0;
                FLAG_LED_PRO_GRN = 1;
                FLAG_LED_PRO_FAST = 0;
                FLAG_LED_PRO_SLOW = 1;

                // Reverse Profile
                FLAG_LED_PRO_DIR_GRN = 0;

            }
            break;
            
            
            case LED_PRO_FADE_ON_AMB_1:
            {
                led_profile.p_addr = &PRO_FADE_LED1;

                // Select Color and rate
                FLAG_LED_PRO_RED = 1;
                FLAG_LED_PRO_GRN = 1;
                FLAG_LED_PRO_FAST = 0;
                FLAG_LED_PRO_SLOW = 0;

                // Forward Profile
                FLAG_LED_PRO_DIR_RED = 1;
                FLAG_LED_PRO_DIR_GRN = 1;

            }
            break;

            case LED_PRO_FADE_OFF_AMB_1:
            {
                led_profile.p_addr = &PRO_FADE_LED1;

                // Select Color and rate
                FLAG_LED_PRO_RED = 1;
                FLAG_LED_PRO_GRN = 1;
                FLAG_LED_PRO_FAST = 0;
                FLAG_LED_PRO_SLOW = 0;

                // Reverse Profile
                FLAG_LED_PRO_DIR_RED = 0;
                FLAG_LED_PRO_DIR_GRN = 0;

            }
            break;

            
            case LED_PRO_FADE_ON_AMB_2:
            {
                led_profile.p_addr = &PRO_FADE_LED1;

                // Select Color and rate
                FLAG_LED_PRO_RED = 1;
                FLAG_LED_PRO_GRN = 1;
                FLAG_LED_PRO_FAST = 1;
                FLAG_LED_PRO_SLOW = 0;

                // Forward Profile
                FLAG_LED_PRO_DIR_RED = 1;
                FLAG_LED_PRO_DIR_GRN = 1;

            }
            break;

            case LED_PRO_FADE_OFF_AMB_2:
            {
                led_profile.p_addr = &PRO_FADE_LED1;

                // Select Color and rate
                FLAG_LED_PRO_RED = 1;
                FLAG_LED_PRO_GRN = 1;
                FLAG_LED_PRO_FAST = 1;
                FLAG_LED_PRO_SLOW = 0;

                // Reverse Profile
                FLAG_LED_PRO_DIR_RED = 0;
                FLAG_LED_PRO_DIR_GRN = 0;

            }
            break;
            
            case LED_PRO_NONE:
            {
                // Unless defined, no profiles are active
                led_profile.ID = LED_PRO_NONE;

                // Clear All Flags
                led_profile.status.ALL = 0;

                // Disable Profiles
                FLAG_LED_PWM_ENABLE = 0;

                // Back to Digital Output/Low for normal blink functions
                //TRIS_GREEN_LED = 0;
                //TRIS_RED_LED = 0;
                
                // Leave I/O Pin states as they were
                //LAT_RED_LED_PIN = 0;
                //LAT_GREEN_LED_PIN = 0;

            }
            break;


            default:
            {
                // Do nothing
            }
            break;
        }


        // Disable Ints during Initialization of Profile variables
        GLOBAL_INT_ENABLE = 0;

        if(LED_PRO_NONE != led_profile.ID)
        {
            // Direct PWM output to Pins
            led_select_pps();
            
            // Green used in profile?
            if(FLAG_LED_PRO_GRN)
            {
                // Green Selected in profile
                if(FLAG_LED_PRO_DIR_GRN)
                {
                    led_profile.index_grn = LED_PROFILE_FIRST;

                    CCPR1H =
                       led_profile.p_addr[LED_PROFILE_FIRST].grn_cnt_on;
                    
                    // Init with medium speed interval
                    led_pwm_dwell_count_grn =
                          led_profile.p_addr[LED_PROFILE_FIRST].cnt_pwm;
                    
                }
                else
                {
                   led_profile.index_grn = LED_PROFILE_LAST;
                   
                   CCPR1H =
                       led_profile.p_addr[LED_PROFILE_LAST].grn_cnt_on;

                   // Init with medium speed interval
                    led_pwm_dwell_count_grn =
                          led_profile.p_addr[LED_PROFILE_LAST].cnt_pwm;

                }
            }
            else
            {
                // Green not selected
                CCPR1H = 0;
            }

            // Red used in profile?
            if(FLAG_LED_PRO_RED)
            {
                // Red Selected in profile
                if(FLAG_LED_PRO_DIR_RED)
                {
                    led_profile.index_red = LED_PROFILE_FIRST;

                        CCPR4H =
                            led_profile.p_addr[LED_PROFILE_FIRST].red_cnt_on;

                    // Init with medium speed interval
                    led_pwm_dwell_count_red =
                          led_profile.p_addr[LED_PROFILE_FIRST].cnt_pwm;
                }
                else
                {
                   led_profile.index_red = LED_PROFILE_LAST;

                    if( FLAG_LED_PRO_GRN )
                    {
                        // Adjust PWM duty for Color selected
                        // Set next PWM Duty cycle and Interval
                        // Shorten Red Time
                        CCPR4H =
                         ((led_profile.p_addr[LED_PROFILE_LAST].red_cnt_on)
                                /(uchar)3 );
                    }
                    else
                    {
                        CCPR4H =
                            led_profile.p_addr[LED_PROFILE_LAST].red_cnt_on;
                    }

                   // Init with medium speed interval
                    led_pwm_dwell_count_red =
                          led_profile.p_addr[LED_PROFILE_LAST].cnt_pwm;
                }
            }
            else
            {
                // Red not selected
                CCPR4H = 0;
            }

           // Start running the LED profile
            led_pwm_on();

            // Enable Profiling and in Progress status bits
            FLAG_LED_PWM_ENABLE = 1;
            FLAG_LED_PRO_N_PROGRESS = 1;


        }
        else
        {
            led_select_latch();
            
            // No Profile Selected
            led_pwm_off();

             // Clear Pulse Times
            CCPR4H = 0;
            CCPR1H = 0;

            // Reset Profiling and in Progress status bits
            FLAG_LED_PWM_ENABLE = 0;
            FLAG_LED_PROFILE_BUSY = 0;  // Free up other LED blinks

        }

        GLOBAL_INT_ENABLE = 1;

    }
}


/*
+------------------------------------------------------------------------------
| Function:  led_pwm_on   
+------------------------------------------------------------------------------
| Purpose:    Enable PWM Drives (for Red and Green Leds) 
|             
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
+------------------------------------------------------------------------------
*/

void led_pwm_on(void)
{
   
    // CCPx control - enable PWM drives
    CCP4CON = 0x9F;
    CCP1CON = 0x9F;
    
}

/*
+------------------------------------------------------------------------------
| Function:  led_pwm_off   
+------------------------------------------------------------------------------
| Purpose:    Turn Off PWM Drives (for Red and Green Leds) 
|             
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
+------------------------------------------------------------------------------
*/
void led_pwm_off(void)
{
    
    // CCPx control - disable PWM drives
    CCP4CON = 0x10;
    CCP1CON = 0x10;
    
}

/*
+------------------------------------------------------------------------------
| Function:  led_select_pps   
+------------------------------------------------------------------------------
| Purpose:    Enable Peripheral Pin Select for PIC  
|             (for Red and Green Led PWM control)  
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
+------------------------------------------------------------------------------
*/

void led_select_pps(void)
{
  
    // Direct PWM to The LED pins
    RD1PPS = 0x0C;   // RD1->CCP4
    RC2PPS = 0x09;   // RC2->CCP1

}

/*
+------------------------------------------------------------------------------
| Function:  led_select_latch   
+------------------------------------------------------------------------------
| Purpose:    Disable Peripheral Pin Select  
|             (used for Red and Green Led PWM control) and 
|             return to Digital I/O latch pin control 
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
+------------------------------------------------------------------------------
*/

void led_select_latch(void)
{
    
    // Direct latch Outputs to The LED pins
    RD1PPS = 0x00;   // RD1-> Latch Output
    RC2PPS = 0x00;   // RC2-> Latch Output
    
}


#ifdef CONFIGURE_WIRELESS_MODULE
/*
+------------------------------------------------------------------------------
| Function:  led_start_timeout_timer   
+------------------------------------------------------------------------------
| Purpose:    Init the Timeout Timer unless it already is running
|             
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
+------------------------------------------------------------------------------
*/

void led_start_timeout_timer(void)
{
    if( !FLAG_TIMEOUT_TIMER_ACTIVE )
    {
        led_timeout_timer = MAIN_OneMinuteTic;
        FLAG_TIMEOUT_TIMER_ACTIVE = 1;

        #ifdef CONFIGURE_TIMER_SERIAL
            SER_Send_String("Start Timeout Timer- ");
            SER_Send_Prompt();
        #endif            
    }
}


/*
+------------------------------------------------------------------------------
| Function:  led_process_timeout_timer   
+------------------------------------------------------------------------------
| Purpose:    Monitor Timeout Timer and set Flags accordingly 
|             
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
+------------------------------------------------------------------------------
*/

void led_process_timeout_timer(uchar timeout)
{
    if(FLAG_TIMEOUT_TIMER_ACTIVE)
    {
        // Monitor Timer for expiration
        if( ( (uchar)(MAIN_OneMinuteTic - led_timeout_timer) ) >= timeout )
        {
            // Timer has expired
            FLAG_TIMEOUT_TIMER_ACTIVE = 0;

            #ifdef CONFIGURE_TIMER_SERIAL
                // Test Only
                SER_Send_String("Timeout Timer Expired- ");
                SER_Send_Prompt();
           #endif                
        }
        else
        {
            // Not Timed out yet
        }
    }
}
#endif