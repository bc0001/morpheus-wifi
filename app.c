/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        app.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Application level message processing for wireless host
|               controller.
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include "common.h"
#include "app.h"
#include "cci_phy.h"
#include "alarmmp.h"
#include "alarmprio.h"
#include "systemdata.h"
#include "fault.h"
#include "sound.h"
#include "led.h"
#include "life.h"
#include "comeasure.h"
#include "photosmoke.h"
#include "battery.h"
#include "a2d.h"
#include "voice.h"
#include "algorithm.h"


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

#ifdef CONFIGURE_WIRELESS_MODULE

#define APP_STATUS_MONITOR_10_MS        (uint)10
#define APP_STATUS_MONITOR_20_MS        (uint)20
#define APP_STATUS_MONITOR_50_MS        (uint)50
#define APP_STATUS_MONITOR_100_MS       (uint)100
#define APP_STATUS_MONITOR_500_MS       (uint)500
#define APP_STATUS_MONITOR_1000_MS      (uint)1000
#define APP_STATUS_MONITOR_2000_MS      (uint)2000
#define APP_STATUS_MONITOR_3000_MS      (uint)3000
#define APP_STATUS_MONITOR_4000_MS      (uint)4000
#define APP_STATUS_MONITOR_5000_MS      (uint)5000
#define APP_STATUS_MONITOR_6000_MS      (uint)6000
#define APP_STATUS_MONITOR_30_SEC       (uint)30000
#define APP_STATUS_MONITOR_60_SEC       (uint)60000

// Wireless Config. Mode Timeout Time
#define APP_TIMER_60_SEC                (uint)600      // In 100 ms units
#define APP_TIMER_2_MIN                 (uint)1200     // In 100 ms units
#define APP_TIMER_4_MIN                 (uint)2400     // In 100 ms units
#define APP_TIMER_8_MIN                 (uint)4800     // In 100 ms units

#define APP_TIMER_1_SEC                 (uint)10   // in 100 ms units
#define APP_TIMER_1_5_SEC               (uint)15   // in 100 ms units
#define APP_TIMER_4_SEC                 (uint)40   // in 100 ms units
#define APP_TIMER_5_SEC                 (uint)50   // in 100 ms units
#define APP_TIMER_6_SEC                 (uint)60   // in 100 ms units
#define APP_TIMER_9_SEC                 (uint)90   // in 100 ms units
#define APP_TIMER_10_SEC                (uint)100  // in 100 ms units

// CCI Supervision Test Time
#define APP_STATUS_TIME_10_SEC_SUPERVISION    (uint)100  // 100 ms units
#define APP_STATUS_TIME_30_SEC_SUPERVISION    (uint)300  // 100 ms units
#define APP_STATUS_TIME_60_SEC_SUPERVISION    (uint)600  // 100 ms units
#define APP_STATUS_TIME_15_MIN_SUPERVISION    (uint)9000 // 100 ms units
#define APP_STATUS_TIME_5_MIN_SUPERVISION     (uint)3000 // 100 ms units
#define APP_STATUS_TIME_2_SEC_SUPERVISION     (uint)20   // 100 ms units
#define APP_PTT_DELAY_1_SEC                   (uint)10   // 100 ms units
#define APP_PTT_DELAY_3_SEC                   (uint)30   // 100 ms units


#define APP_SPRV_1ST_15_MINUTES  (uchar)15   // 1 minute units
#define APP_SPRV_1ST_5_MINUTES   (uchar)5    // 1 minute units
#define APP_SPRV_1ST_10_MINUTES  (uchar)10   // 1 minute units
#define APP_SPRV_1ST_12_MINUTES  (uchar)12   // 1 minute units
#define APP_SPRV_1ST_30_MINUTES  (uchar)30   // 1 minute units
#define APP_SPRV_1ST_60_MINUTES  (uchar)60   // 1 minute units

#define LP_TIMER_ONE_MINUTE             (uchar)1  // 1 Min units
#define LP_TIMER_FOUR_MINUTES           (uchar)4  // 1 Min units
#define LP_TIMER_FIVE_MINUTES           (uchar)5  // 1 Min units
#define LP_TIMER_TEN_MINUTES            (uchar)10 // 1 Min units
#define LP_TIMER_FIFTEEN_MINUTES        (uchar)15 // 1 Min units
#define LP_TIMER_THIRTY_MINUTES         (uchar)30 // 1 Min units
#define LP_TIMER_SIXTY_MINUTES          (uchar)60 // 1 Min units

#define APP_SLAVE_DELAY_18_SEC          (uchar)18 // 1 Sec units
#define APP_SLAVE_DELAY_16_SEC          (uchar)16 // 1 Sec units
#define APP_SLAVE_DELAY_14_SEC          (uchar)14 // 1 Sec units
#define APP_SLAVE_DELAY_12_SEC          (uchar)12 // 1 Sec units
#define APP_SLAVE_DELAY_10_SEC          (uchar)10 // 1 Sec units
#define APP_SLAVE_DELAY_8_SEC           (uchar)8  // 1 Sec units
#define APP_SLAVE_DELAY_1_SEC           (uchar)1  // 1 Sec units


#define APP_PROCESS_RESPONSE_SUCCESS    0
#define APP_PROCESS_RESPONSE_ERROR      1

#define APP_RF_UNTESTED_4_BEEPS     (uchar)4

// 24 Devices in network (0-23)
#define APP_ZERO_ID (uchar)0

#ifdef CONFIGURE_WIFI
    #define APP_MAX_ID  (uchar)1
#else
    #define APP_MAX_ID  (uchar)23
#endif

#define APP_ID_FF   (uchar)0xFF
#define APP_ID_FE   (uchar)0xFE


// These APP_ID defines added 4/3/2017, but not currently used 
//  by this application
#define APP_ID_NEVER_ENROLLED   (uchar)0xFF
#define APP_ID_NOT_ENROLLED     (uchar)0xFE

// Statis 1 Output Bits to report
#define APP_STATUS1_OUT_MASK    0x1F

#define COMM_STATUS_RFD_AND_COORD_SET   0xC0

#ifdef CONFIGURE_WIRELESS_CCI_DEBUG
    #define APP_CCI_QUEUE_EMPTY (uint)0
#endif

// Define Masks for HW INT message parsing
#define MASK_HW_INT_SMOKE_CLR    (uchar)0xD7
#define MASK_HW_INT_CO_CLR       (uchar)0xCF
#define MASK_HW_INT_SMOKE_CO_CLR (uchar)0xC7   
#define MASK_SMOKE_STATUS_08     (uchar)0x08
#define MASK_CO_STATUS_10        (uchar)0x10

#define MASK_SMK_HW_MSG_RECVD       (uchar)0x28
#define MASK_CO_HW_MSG_RECVD        (uchar)0x30
#define MASK_CO_SMK_HW_MSG_RECVD    (uchar)0x38

#define MASK_STATUS_INVALID_MSG_RECVD   (uchar)0x06
#define MASK_STATUS1_INVALID_MSG_RECVD  (uchar)0x84
#define MASK_STATUS2_INVALID_MSG_RECVD  (uchar)0x10     
#define MASK_STATUS3_INVALID_MSG_RECVD  (uchar)0xC0

#define MASK_RF_ID (uchar)0x07

#define COMP_PERCENT_100 (uchar)100

#define APP_NET_SIZE_OF_3       (uchar)3

#define APP_MAX_ERR_CODE_REQS       (uchar)5
#define APP_ERR_CODE_REQ_TIMEOUT    (uchar)10
                
//TODO: TODO: Undefine Later  BC: Test Only
#ifdef CONFIGURE_UNDEFINED
    #define CONFIGURE_APP_DISABLE
#endif

/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/

// Define Alarm/Network Status Monitor States
enum
{
    APP_MONITOR_POWER_DELAY = 0,
    APP_MONITOR_START,
    APP_MONITOR_VERIFY_TESTED,
    APP_MONITOR_FINAL_TEST,
    APP_MONITOR_INIT,
    APP_MONITOR_IDLE,
    APP_MONITOR_ENROLL_STATE,
    APP_MONITOR_JOIN,
    APP_MONITOR_ERROR,
    APP_MONITOR_SUPERVISION,
    APP_MONITOR_FINAL_TEST_WAIT,
    APP_MONITOR_CCI_FAULT
};


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/
// Alarm Status sent to the RF Module
volatile Flags_t APP_Current_ALM_Status;
volatile Flags_t APP_Previous_ALM_Status;

// Alarm Status received from the RF Module
volatile Flags_t APP_Remote_ALM_Status;
volatile Flags_t APP_Remote_ALM_Status2;


// Status 1 Flags
persistent volatile Flags_t APP_ALM_Status_1;

// Status 2 Flags
volatile Flags_t APP_ALM_Status_2;
volatile Flags_t APP_Previous_ALM_Status_2;

// Status 3 Flags
volatile Flags_t APP_ALM_Status_3;
volatile Flags_t APP_Previous_ALM_Status_3;



// Number of devices in Network stored here
persistent volatile uchar APP_Rf_Network_Size;

persistent volatile uchar APP_RF_Module_FW_Version;

volatile uchar APP_CCI_Retry_Count;
union tag_CCI_Packet AMP_RetryPacket;



//******************************************************************************
// COMM Status Outputs sent to the RF Module
volatile Flags_t APP_Current_COMM_Status;
volatile Flags_t APP_Previous_COMM_Status;


// COMM Status Inputs received from the RF Module
persistent volatile Flags_t APP_Remote_COMM_Status;



/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
static uchar app_status_state= APP_MONITOR_POWER_DELAY;

#ifdef CONFIGURE_WIRELESS_CCI_DEBUG
        uchar APP_CCI_Queue[CQLENGTH];
        static uint app_cci_qin;
        static uint app_cci_qout;
#endif

static uint app_timer_wireless_supervision = APP_STATUS_TIME_60_SEC_SUPERVISION;

#ifdef CONFIGURE_APP_WIRELESS_SUPERVISION

  #ifndef CONFIGURE_WIFI
    static unsigned char app_lp_cci_supervisor_timer = LP_TIMER_FOUR_MINUTES;
  #endif
    static unsigned char app_cci_super_count = 0;
#endif

#ifdef CONFIGURE_APP_COMM_VERSION
    static uchar app_rf_amp_version = 0;
#endif

// Init to no ID set
persistent volatile uchar app_rf_module_id; 

static uchar app_rf_module_error = 0;

static uchar alm_timer_slave_min = 0;

#ifdef	CONFIGURE_INTERCONNECT
    static uint app_timer_wl_gateway = 0;
    static uchar alm_timer_slave_dly = 0;
    static uint alm_timer_remote_ptt_dly = 0;
    
    static uchar alm_slave_to_remote_delay_time;
#endif
    
static uchar local_status = 0;    

// Track Network Error code request timeout and attempts
static uchar app_net_err_cnt;
static uchar app_net_err_attempts;



/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/

uchar app_update_current_status_byte(void);
void app_request_error(void);
uchar app_send_ping(void);
uchar app_send_comm_status(uchar);
uchar app_process_comm_status(uchar);
uchar app_update_status1(uchar);

#ifdef	CONFIGURE_INTERCONNECT
    void app_wl_gateway_timer(void);
#endif
    
#ifdef CONFIGURE_UNDEFINED
    uchar app_update_status2(uchar);
    void app_send_fault(uchar);
    void app_request_amp_version(void);
#endif


/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/

#ifdef CONFIGURE_WIFI    
/*
+------------------------------------------------------------------------------
| Function:      APP_Status_Monitor_Reset
+------------------------------------------------------------------------------
| Purpose:       Reset Monitor Alarm Status to Power On state
|                This occurs when AC power is OFF, since WiFi module
|                requires AC Power
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
   
void APP_Status_Monitor_Reset(void)    
{   
    app_status_state = APP_MONITOR_POWER_DELAY;
}

/*
+------------------------------------------------------------------------------
| Function:      APP_Enable_WiFi_Port
+------------------------------------------------------------------------------
| Purpose:       Re-Initialize WiFI Port that was disabled.
|                This occurs when AC power is OFF, since WiFi module
|                requires AC Power
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void APP_Enable_WiFi_Port(void)
{
    // Clock and DATA pins set to inputs.
    mCCI_DATA_PIN_DIR = 1;
    mCCI_CLOCK_PIN_DIR = 1;

    // Enable CCI Data Pin Interrupt
    IOCBN = 0b00000010;        

    // Wireless should be re-enabled
    FLAG_WIRELESS_DISABLED = 0;
    
}
#endif


/*
+------------------------------------------------------------------------------
| Function:      APP_Status_Monitor
+------------------------------------------------------------------------------
| Purpose:       Monitor Alarm Status and Network Status Changes and take
|                appropriate wireless response actions
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Task Next Execution Return Time (in Task Engine TICs)
+------------------------------------------------------------------------------
*/

uint APP_Status_Monitor(void)
{
    #ifdef CONFIGURE_DEBUG_APP_STATES
        // Test Only
        for(uchar i = (app_status_state + 1); i > 0; i--)
        {
            MAIN_TP2_On();
            PIC_delay_us(DBUG_DLY_25_USec);
            MAIN_TP2_Off();
            PIC_delay_us(DBUG_DLY_10_USec);
        }
    #endif

    #ifdef CONFIGURE_UNDEFINED  
        // TODO: Undefine Later  BC: Test Only
        SER_Send_Int('x',app_status_state,10 );
    #endif

    #ifdef CONFIGURE_APP_DISABLE
        // TODO: Undefine Later  BC: Test Only
        // This is to test other WiFi features without wireless module
        
        // Do not proceed with wireless APP processing
        return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_1000_MS);

    #endif

#ifdef CONFIGURE_WIFI
    // WIFI
        
    // First Check to see if Wireless Module is functional    
    if(FLAG_DISABLE_WIFI)
    {
            #ifdef CONFIGURE_TEST_CODE
                log_seq(5);
            #endif

        /*
         * WiFi Module is no longer functional: 
         * 1. Disable CCI Ints
         * 2. Set CCI Clock and Data pins to output low
         * 
         * 3. Disable all CCI transmissions 
         *    a. Transport Manager call in Main INT routine
         * 
         * 4. Disable any APP Init calls
         * 
         * NOTE: Other code that is turned OFF
         * a. Any active remote alarms should time out
         * b. latch in this disabled condition
         * c. APP_Init should be disabled
         * d. CCI Supervision is disabled
         * 
         */ 

        FLAG_DISABLE_WIFI = 0;
        
        // Turn OFF CCI Data Pin Interrupt
        IOCBN = 0b00000000;     // CCI Data Negative edge bit-1 OFF
        
        // Allow Low Power Mode when Wireless is OFF
        FLAG_APP_NOT_IDLE = 0;

        //
        // Undefine to not Drive CCI pins Low
        // This was hanging up WiFi communication
        //
        #ifdef CONFIGURE_UNDEFINED
            // CCI Pins set to Output Low
            mCCI_DATA_PIN_DIR = 0;
            mCCI_CLOCK_PIN_DIR = 0;
            LAT_CCI_DATA_PIN = 0;            
            LAT_CCI_CLK_PIN = 0;  
        #endif
        
        if(FLAG_APP_SEARCH_MODE || FLAG_APP_JOIN_MODE)
        {
            // Abort these Modes if Active
            FLAG_APP_SEARCH_MODE=0;
            FLAG_APP_JOIN_MODE =0;
            FLAG_COMM_JOIN_COMPLETE = 0;
            FLAG_COMM_JOIN_IN_PROGRESS = 0;    
        }
       
        // Clear all Remote Flags that could hold unit in Active Mode
        Flags_Remote_Alerts.ALL = 0;
        Flags_Remote_Flags1.ALL = 0;
        FLAG_CCI_TEST = 0;
        FLAG_CCI_EDGE_DETECT = 0;
        FLAG_WAKE_FOR_CCI = 0;
        FLAG_REMOTE_PTT_ACTIVE = 0;
        FLAG_ALARM_LOCATE_MODE = 0;
        
        FLAG_WIRELESS_DISABLED = 1;
        
        // TODO: Undefine Later  BC: Test Only
        #ifndef CONFIGURE_UNDEFINED
            SER_Send_String("\rAPP WiFi Disabled\r");
        #endif

        FLAG_ACTIVE_TIMER = 1;
        if(MAIN_ActiveTimer < TIMER_ACTIVE_MINIMUM_2_SEC)
        {
            // Extend time to remain in Active Mode
            MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_2_SEC;
        }
            
        return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_1000_MS);
    }

#else
    // Morpheus

    // First Check to see if Wireless Module is functional    
    if(FLAG_WIRELESS_DISABLED)
    {
        /*
         * Wireless Module is no longer functional: 
         * 1. Disable CCI Ints
         * 2. Set CCI Clock and Data pins to output low
         * 
         * 3. Disable all CCI transmissions 
         *    a. Transport Manager call in Main INT routine
         * 
         * 4. Disable any APP Init calls
         * 
         * NOTE: Other code that is turned OFF
         * a. Any active remote alarms should time out
         * b. latch in this disabled condition
         * c. APP_Init should be disabled
         * d. LP Mode CCI Supervision is disabled
         * e. Battery Test RXTX OFF request is disabled
         * 
         */ 

        // Turn OFF CCI Data Pin Interrupt
        IOCBN = 0b00000000;     // CCI Data Negative edge bit-1 OFF
        
        // Allow Low Power Mode when Wireless is OFF
        FLAG_APP_NOT_IDLE = 0;

        // CCI Pins set to Output Low
        mCCI_DATA_PIN_DIR = 0;
        mCCI_CLOCK_PIN_DIR = 0;
        LAT_CCI_DATA_PIN = 0;            
        LAT_CCI_CLK_PIN = 0;   
        
        // Abort these Modes if Active
        FLAG_APP_SEARCH_MODE=0;
        FLAG_APP_JOIN_MODE =0;
        
       
        // Clear all Remote Flags that could hold unit in Active Mode
        Flags_Remote_Alerts.ALL = 0;
        Flags_Remote_Flags1.ALL = 0;
        FLAG_CCI_TEST = 0;
        FLAG_CCI_EDGE_DETECT = 0;
        FLAG_WAKE_FOR_CCI = 0;
        FLAG_REMOTE_PTT_ACTIVE = 0;
        FLAG_ALARM_LOCATE_MODE = 0;
        
         
        //If not in WM_EOL Network Error re-log WM EOL fault
        if(!FLAG_NETWORK_ERROR_MODE)
        {
            FLT_Fault_Manager(AMP_FAULT_WM_BATTERY_EOL);
        }
        else if(FLT_NetworkFaultCode != AMP_FAULT_WM_BATTERY_EOL)
        {
            FLT_Fault_Manager(AMP_FAULT_WM_BATTERY_EOL);
        }
        
        // Remain in this State permanently by simply returning
        return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_60_SEC);
        
    }
            
#endif

            
    #ifdef CONFIGURE_WIFI  
        
        if(!FLAG_AC_DETECT)
        {
            #ifdef CONFIGURE_TEST_CODE
                log_seq(1);
            #endif

            // TODO: Undefine Later  BC: Test Only
            #ifdef CONFIGURE_UNDEFINED
                SER_Send_String("\rAPP No AC Detect\r");
            #endif
            
            if(!FLAG_WIRELESS_DISABLED)
            {
                #ifdef CONFIGURE_TEST_CODE
                    log_seq(20);
                #endif
                
                //
                // No AC without Wireless Disabled Yet
                // We should Disable Wifi and set WiFi Reset
                // in Case power is restored soon.
                //
                FLAG_DISABLE_WIFI = 1;
                
                // For WiFi AC Power must be active to run wireless states
                FLAG_APP_NOT_IDLE = 0;
                
                // Remain in this State until AC is detected
                return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_100_MS);
           }
            
            // Remain in this State until AC is detected
            return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_500_MS);
        }
        else if(FLAG_WIRELESS_DISABLED)
        {
            //
            // AC Power is ON
            // Need to Re-Enable Wifi
            //
            #ifdef CONFIGURE_TEST_CODE
                log_seq(21);
            #endif

            APP_Enable_WiFi_Port();  
            FLAG_AC_WIFI_RST = 1;       // Insure WiFi Reset is also performed
            
            // Remain in this State until WiFi is back ON
            return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_500_MS);
        }
    #endif

    #ifdef CONFIGURE_WIFI        
        if(FLAG_APP_SEND_ALM_STATUS)
        {
            #ifdef CONFIGURE_TEST_CODE
                log_seq(2);
            #endif

            FLAG_APP_SEND_ALM_STATUS = 0;
            
            // At Soft Resets reset Alarm Status
            APP_Update_Status();

            // And request Comm Status
            APP_Request_Comm_Status();

            // And Re-Init Status 1
            APP_Request_Status1();
        }
    #endif
        
    switch (app_status_state)
    {
        case APP_MONITOR_POWER_DELAY:
        {
            // Hold in Active Mode until we reach APP Idle
            FLAG_APP_NOT_IDLE = 1;
            
            // Send a Reset Message and Request COMM status and Status1?
            if(FLAG_RST_FAULT_AT_RST)
            {
                
                #ifdef CONFIGURE_TEST_CODE
                    log_seq(3);
                #endif

                // YES, Send a Fault reset message to RF Module
                APP_Reset_Fault();
                FLAG_RST_FAULT_AT_RST = 0;
                
                // Should request COMM status under this condition.
                //  since it is not automatically sent.
                // Not sure this is required, since reset does not
                //  clear previous Alarm COMM Status register
                APP_Request_Comm_Status();
                APP_Request_Status1();
            }

            app_status_state = APP_MONITOR_START;

            #ifdef CONFIGURE_WIFI

                // TODO: Undefine Later  BC: Test Only
                #ifndef CONFIGURE_UNDEFINED
                    SER_Send_String("\rAPP Power Delay\r");
                #endif
                
                return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_3000_MS);
            #else
                return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_3000_MS);
            #endif
        }

        case APP_MONITOR_START:
        {
            /* The 1st state after Reset
             * Before proceeding to APP Initialization which requests Comm
             * Status, make sure we are calibrated and ready to begin
             * Wireless Processing.
             *
             * Test for Smoke Calibration Complete
             */
            if(FLAG_NO_SMOKE_CAL || FLAG_LOW_BATTERY_CAL_MODE)
            {
                // Not Smoke Calibrated yet, Wait at this state.
                return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_30_SEC);
            }

            #ifdef CONFIGURE_CO
                // For CO Models, wait until Calibration and Final test
                //  has Completed.
                if( !FLAG_CALIBRATION_COMPLETE || FLAG_CALIBRATION_UNTESTED )
                {
                    // CO Cal must be completed, remain in this State
                    return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_60_SEC);
                }
            #endif


            // Init Wireless Supervision Timer
            app_timer_wireless_supervision = APP_STATUS_TIME_60_SEC_SUPERVISION;
            app_cci_super_count = 0;
            
            // Init COMM Status Output bits
            APP_Current_COMM_Status.ALL = 0;
            APP_Previous_COMM_Status.ALL = 0;
            
            #ifdef CONFIGURE_WIFI
                if(FLAG_AC_WIFI_RST)
                {
                    FLAG_AC_WIFI_RST = 0;
                    
                    #ifdef CONFIGURE_TEST_CODE
                        log_seq(4);
                    #endif

                    // To Be Implemented when WiFi waits for Ping
                    // Send a Ping to WiFI Module so it knows CCI is ready
                    // for action
                    app_send_ping();
                    
                    //
                    // TODO: Define time delay to wait, BC: shorten later,
                    //       WiFi module takes a while to send COMM status
                    //
                    return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_2000_MS);
               }
            #endif

            #ifdef CONFIGURE_TEST_CODE
                log_seq(10);
            #endif
            

            // Smoke and Battery is calibrated, Proceed to Verify state
            app_status_state = APP_MONITOR_VERIFY_TESTED;
            
            
            #ifdef CONFIGURE_UNDEFINED
                //
                // TODO: Undefine Later  BC: Test Only
                // Output COMM Status and Status1
                //
                SER_Send_Int('x', APP_Remote_COMM_Status.ALL, 16);
                SER_Send_Prompt();
                SER_Send_Int('y', APP_ALM_Status_1.ALL, 16);
                SER_Send_Prompt();
            #endif

            #ifdef CONFIGURE_WIFI
                //return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_2000_MS);
                return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_100_MS);
            #else
                return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_100_MS);
            #endif

        }

        case APP_MONITOR_VERIFY_TESTED:
        {
                #ifdef CONFIGURE_TEST_CODE
                    log_seq(11);
                #endif
            
            // We should have received COMM Status by Now, check it.
            // Check that enrollment status has been received
            // If COMM_STATUS is not 0xFF, then it has been received
            if(APP_Remote_COMM_Status.ALL != 0xFF)
            {
                #ifdef CONFIGURE_APP_COMM_UNTESTED
                    // Here we test RF Untested bit 1st
                    if(FLAG_COMM_UNTESTED)
                    {
                        // RF module was never tested in production, ERROR
                        // Issue Beeps
                        SND_Set_Chirp_Cnt(APP_RF_UNTESTED_4_BEEPS);

                        app_status_state = APP_MONITOR_ERROR;

                        return  MAIN_Make_Return_Value
                                (APP_STATUS_MONITOR_500_MS);
                    }
                    else
                    {
                        // RF Module installed and Tested, Proceed to Check
                        //   Final Test
                        
                        // Now that COMM status has been received
                        //  Init to default RF ID
                        if(FLAG_COMM_COORDINATOR)
                        {
                            // Coord. ID is always Zero
                            app_rf_module_id = APP_ZERO_ID;
                        }
                        else
                        {
                            
                            // WiFi wireless only has RFD ID#1
                            #ifdef CONFIGURE_WIFI
                                app_rf_module_id = 1;
                                
                            #else
                                //
                                //  RFD ID will be requested and set at 
                                //   Enroll or Re-Join
                                //
                            #endif
                        }
                    }
                #endif

                app_status_state = APP_MONITOR_FINAL_TEST;
                return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_100_MS);

            }
            else
            {
                
                #ifdef CONFIGURE_TEST_CODE
                    log_seq(12);
                #endif
                
                // Comm status was not received, Go to Error State
                FLAG_FAULT_CCI_IMMED = 1;
                FLT_Fault_Manager(AMP_FAULT_CCI);
                app_status_state = APP_MONITOR_CCI_FAULT;
                
                // TODO: Undefine Later  BC: Test Only 
                #ifndef CONFIGURE_UNDEFINED
                    SER_Send_String("\rNo COMM Status Received\r");
                #endif

            }
        }
        break;

        case APP_MONITOR_FINAL_TEST:

        #ifdef CONFIGURE_APP_COMM_FINAL_TEST
            // Status 1 should have been received by now.
            // Verify that manufacturing Final Test has been completed
            //  before attempting an auto enrollment after Power On Resets
            if(APP_ALM_Status_1.ALL != 0xFF)
            {
                if(!FLAG_STATUS_1_FINAL_TESTED)
                {
                    // Transceiver has not been final tested so skip
                    //   the Auto Enrollment Process
                    
                    #ifdef CONFIGURE_UNDEFINED
                        SER_Send_String("Wait for Final Test...");
                        SER_Send_Prompt();
                    #endif

                    // Proceed to Wait State
                    app_status_state = APP_MONITOR_FINAL_TEST_WAIT;
                    return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_100_MS);
                }
                else
                {
                    
                    // Transceiver was Final Tested successfully
                    // Proceed to Next state
                    app_status_state = APP_MONITOR_INIT;
                    return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_100_MS);
                }
            }
            else
            {
                // Status1 was not received, Go to Error State for Now
                FLAG_FAULT_CCI_IMMED = 1;
                FLT_Fault_Manager(AMP_FAULT_CCI);
                app_status_state = APP_MONITOR_CCI_FAULT;
                
            // TODO: Undefine Later  BC: Test Only
            #ifndef CONFIGURE_UNDEFINED
                SER_Send_String("/rNo Status_1 Received/r");
            #endif
                
            }
            break;

        #else
            // Proceed to Next state
            app_status_state = APP_MONITOR_INIT;
            return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_100_MS);

        #endif


        case APP_MONITOR_INIT:
        {
            /*
             * After Power On/Reset Perform App Status Initialization here
             * At some point after Power On we need to receive COMM status
             * to test whether Alarm RF module is enrolled or not.
             * Since RF Module automatically sends COMM status at Power
             * On, we should already have this information.
             *
             * It is the APP's responsibility to issue join request if
             * RF module has not been registered into a network as either
             * coordinator or RFD device.
             *
             * 1st perform RF Module Verification since Cal is completed.
             *
             */

            #ifdef CONFIGURE_WIFI
                if( FLAG_NETWORK_ERROR_MODE &&
                   (AMP_FAULT_CCI == FLT_NetworkFaultCode ) )
                {
                    // Reset CCI Faults if we make it to here successfully
                    // WiFi potentially recovering from CCI 
                    FLT_Fault_Manager(AMP_SUCCESS_CCI);
                    
                    FLAG_REQ_JOIN_AT_RST = 1;
                }
            #endif

            // Transmit Initial Status to RF Module
            // No need to do this any longer....  comment out for Now
            #ifdef CONFIGURE_UNDEFINED
                APP_Update_Status();
            #endif

            // We have already received COMM status in previous state , so
            //  simply proceed to Check Enrollment State
            app_status_state = APP_MONITOR_ENROLL_STATE;

        break;
        }

        //**********************************************************************
        case APP_MONITOR_IDLE:
        {
            FLAG_APP_NOT_IDLE = 0;
            
            /*
             * From APP Idle we do multiple things.
             * 0. Test for any CCI Retransmissions 
             * 
             * 1. Send any Alarm Status changes to the RF module via CCI
             * 
             * 2. Monitor received Status changes from RF Module.
             *     (Low Battery, Wireless Errors, Remote alarms... etc.)
             *
             * 3. Monitor for Status1 Changes
             * 
             * 4. Monitor for Status2 Changes
             * 
             * 5. Monitor for Status3 Changes
             * 
             * 6. Supervise CCI operation
             *
             * 7. Send any COMM Status Output Changes to RF Module
             *    (i.e - OOB Reset, Join Request/End, Coordinator OUT)
             *
             * 8. Monitor COMM Status Input changes from RF Module
             *
             */
            
            
            // 0. 1st - Test for any CCI Retransmissions from Handshake errors
            if(FLAG_CCI_HS_RETRY)
            {
                if(0 == APP_CCI_Retry_Count)
                {
                    // Init Retry Counter for 2 more attempts
                    APP_CCI_Retry_Count = APP_CCI_HS_ATTEMPTS_3;
                    break;
                }
                
                if(0 == --APP_CCI_Retry_Count)
                {
                    // No More retries - give up without resending
                    FLAG_CCI_HS_RETRY = 0;
                }
                else
                {
                    // Resend the Packet
                    FLAG_CCI_HS_RETRY = 0;
                    AMP_Start_Transaction(AMP_RetryPacket);
                    
                    return  MAIN_Make_Return_Value(APP_STATUS_MONITOR_100_MS);
                }
            }
            else
            {
                APP_CCI_Retry_Count = 0;
            }

            // *****************************************************************
            // 1. Test Host Alarm Status conditions and update the
            //     Current AMP Status byte for Transmission
            //
            if(FLAG_ALARM_MIN_TIMER_ON)
            {
                // Delay status Change Transmissions while
                //  FLAG_ALARM_MIN_TIMER_ON

                // Test the Minimum Alarm Timer while it is running
                APP_Min_Alarm_Timer();
            }
            
            // Do not update Alarm status during PTT Active
            //  a PTT message will be sent
            if(app_update_current_status_byte()) 
            {
                // Status change detected
                if(!FLAG_ALARM_MIN_TIMER_ON)
                {
                     // Notify RF Module of Status Changes Only
                    APP_Update_Status();
                    // Save most recent status sent to RF Module
                    APP_Previous_ALM_Status.ALL = APP_Current_ALM_Status.ALL;
                    
                    if( FLAG_STATUS_ALM_SMK_ACTIVE || 
                        FLAG_STATUS_ALM_CO_ACTIVE )
                    {
                        // If this Status Transmission is for a CO or Smoke 
                        //  Alarm then begin a Minimum Alarm Status On Timer
                        //  so that this Alarm can be transmitted wirelessly
                        //  before it is cleared.  This is to try to help
                        //  a Smoke Only slave PTT to make it thru the network
                        APP_Init_Min_Alarm_Timer();
                    }
                
                }
                
            }

            #ifdef  CONFIGURE_WIRELESS_ALARM_LOCATE
                if( FLAG_ALARM_LOCATE_REQUEST )
                {
                    // User has requested a smoke alarm locate
                    APP_Send_Alarm_Locate();

                    // Only send it once per request
                    FLAG_ALARM_LOCATE_REQUEST = 0;
                    
                    /*
                     * Also need to Set locate Mode, since this alarm
                     *  is sending it not receiving it.
                     * 
                     *  We set/send Locate for Smoke Hush to
                     *  Immediately Stop all slave/remote alarm transmissions
                     */
                    FLAG_ALARM_LOCATE_MODE = 1;

                    // Initialize the Locate Mode Timer
                    ALM_Init_Locate_Timer();
                    
                }
            
                if( FLAG_ALARM_LOCATE_CLEAR )
                {
                    // Clear Locate Mode
                    APP_Send_Clr_Alarm_Locate();

                    // Only send it once per request
                    FLAG_ALARM_LOCATE_CLEAR = 0;
                    
                    /*
                     * Also need to Clear any active locate Mode, since this 
                     *  alarm is sending it not receiving it.
                     */
                    FLAG_ALARM_LOCATE_MODE = 0;
                }
            
            #endif

            // *****************************************************************
            // 2. Test Received AMP status and set appropriate Alarm flags
            //
            //  FLAG_STATUS_RMT_SMK_RCVD and FLAG_STATUS_RMT_CO_RCVD is set 
            //    about every 6 seconds while Network is in smoke alarm.  


            #ifndef CONFIGURE_INTERCONNECT  
            
                // No gateway logic required
                FLAG_GATEWAY_SLV_TO_REMOTE_OFF = 0;
            #else

                // First, Check the FLAG_GATEWAY_SLV_TO_REMOTE_OFF logic
                if( FLAG_STATUS_RMT_SMK_RCVD)                                        
                {
                    // Remote Smoke Alarm being Received
                    if(FLAG_SLAVE_SMOKE_ACTIVE)
                    {
                        // No Action
                    }
                    else
                    {
                        // Allow Slave to Remote to be Enabled
                        FLAG_GATEWAY_SLV_TO_REMOTE_OFF = 0;

                    }
                }
                else if(FLAG_STATUS_RMT_CO_RCVD)                                         
                {
                    // Remote CO Alarm being Received
                    if(FLAG_SLAVE_CO_ACTIVE)
                    {
                        // No Action
                    }
                    else
                    {
                        // Allow Slave to Remote to be Enabled
                        FLAG_GATEWAY_SLV_TO_REMOTE_OFF = 0;
                    }
                }
            #endif

            // Now Service the Remote Smoke or CO Alarm status flags
            if( FLAG_STATUS_RMT_SMK_RCVD)                                        
            {
                // Clear Smoke Received Status and Process it
                FLAG_STATUS_RMT_SMK_RCVD = 0;
                
                // No need to respond while in Master Smoke
                if(!FLAG_MASTER_SMOKE_ACTIVE)
                {
                    if(FLAG_ALM_REMOTE_ENABLE)
                    {
                        //  Notify AlmPrio module if Remote Alarms are 
                        //  Enabled
                        FLAG_REMOTE_SMOKE_ACTIVE = 1;
                    }
                }
            }
                
            if(FLAG_STATUS_RMT_SMK_HW_RCVD)
            {
                FLAG_STATUS_RMT_SMK_HW_RCVD = 0;
                
                // No need to respond while in Master Smoke
                if(!FLAG_MASTER_SMOKE_ACTIVE)
                {
                    if(FLAG_ALM_REMOTE_ENABLE)
                    {
                        //  Notify AlmPrio module if Remote Alarms are 
                        //  Enabled
                        FLAG_REMOTE_HW_SMOKE_ACTIVE = 1;
                    }
                }
            }

            //  FLAG_STATUS_RMT_CO_RCVD is sent every 6 seconds
            //    while in alarm.
            if(FLAG_STATUS_RMT_CO_RCVD)                                         
            {
                // Clear CO Received Status and Process it
                FLAG_STATUS_RMT_CO_RCVD = 0;
                
                // No need to respond while in Master CO
                if(!FLAG_CO_ALARM_CONDITION)
                {
                    if(FLAG_ALM_REMOTE_ENABLE)
                    {
                        //  Notify AlmPrio module if Remote Alarms are 
                        //  Enabled
                        FLAG_REMOTE_CO_ACTIVE = 1;
                    }
                }
            }
                
            if(FLAG_STATUS_RMT_CO_HW_RCVD)
            {
                FLAG_STATUS_RMT_CO_HW_RCVD = 0;
                
                // No need to respond while in Master Smoke
                if(!FLAG_CO_ALARM_CONDITION)
                {
                    if(FLAG_ALM_REMOTE_ENABLE)
                    {
                        //  Notify AlmPrio module if Remote Alarms are 
                        //  Enabled
                        FLAG_REMOTE_HW_CO_ACTIVE = 1;
                    }
                }
            }
                

            #ifdef CONFIGURE_INTERCONNECT

                // Check HW Interconnect Remote to Slave Gateway conditions
                // If Remote Smoke or CO Active check the RtoS Gateway Timer
                if( FLAG_REMOTE_SMOKE_ACTIVE || FLAG_REMOTE_CO_ACTIVE ||
                    FLAG_REMOTE_HW_SMOKE_ACTIVE || FLAG_REMOTE_HW_CO_ACTIVE)
                {
                    // Remote Smoke and Remote Co flags should remain
                    //  active during period gateway is being established
                    // They do get cleared after entering Alarm States
                    //   in almprio module, and during Remote Inhibit
                    
                    // Init and Execute RFID Delay Timer
                    //   this allows lowest ID to drive INT OUT for
                    //   Wireless to HW Interconnect Gateway
                    app_wl_gateway_timer();
                }
            
            #endif

            #ifndef CONFIGURE_ACDC
                /*
                 * DC Model RF Modules monitor their own Battery Power
                 *  and provide LB Battery Status to the Host Alarm
                 */
                if(FLAG_STATUS_RMT_LB_ACTIVE)
                {
                    #ifndef CONFIGURE_NO_REMOTE_LB
                        /* 
                         * RF Module LB Received, Trouble Manager 
                         * will enter Remote Low Battery if this status bit is 
                         * received.
                         */ 
                    
                    #else
                        // No Remote Low Battery indication, so clear it
                        FLAG_STATUS_RMT_LB_ACTIVE = 0;
                    #endif
                }
            #endif

                

//****************************************************************************** 
//******************************************************************************         
               
            // Test for RF Remote Fault Mode
            
            if(FLAG_STATUS_RMT_FAULT_ACTIVE)
            {
                // Currently in Network Error
                // FLAG_STATUS_RMT_FAULT_ACTIVE remains active until cleared 
                // by RF Module or Net Error reset (button or 24 hours)

                // RF Remote Error Received
                // 1. Request Error Code if New Remote Error
                // 2. Set FLAG_REMOTE_ERROR_REQ and Timeout Counter
                // 3. Process the Fault based upon Fault Code received
                // 4. Reset Remote fault New
                
                // Don't keep requesting Remote Fault Code if 
                //  in Remote Fault
                if(!FLAG_REMOTE_ERROR_REQ && FLAG_RMT_FAULT_NEW)
                {
                    /*
                     * Request New Error Code (only do this one time for
                     *  each RMT Fault Active received)
                     * 
                     * Error Code should be placed in 
                     * app_rf_module_error
                     */
                    app_rf_module_error = 0;    // Reset Network Error Code
                    app_request_error();        // Request new Error Code

                    // Only request Once with this Net Error
                    FLAG_REMOTE_ERROR_REQ = 1;
                    
                    // Set Error request Timeout 
                    app_net_err_cnt = APP_ERR_CODE_REQ_TIMEOUT;
                }
                else if(app_rf_module_error && FLAG_RMT_FAULT_NEW)
                {
                    // New Network Error code has been received
                    // in app_rf_module_error, process it
                    
                    // If the Error is a Join Error and the unit
                    //  has not Previously Joined (i.e. Enrollment Join error)
                    //  then do not enter error Mode
                    if( (AMP_FAULT_RFD_JOIN == app_rf_module_error) &&
                            !FLAG_PREV_JOINED )
                    {
                        // Clear Net Error Status and do not log a Fault
                        // Allow operation as a stand-alone Alarm
                        app_rf_module_error = 0;
                        FLAG_REMOTE_ERROR_REQ = 0;
                        FLAG_STATUS_RMT_FAULT_ACTIVE = 0;
                        FLAG_RMT_FAULT_NEW = 0;
                        
                    }
                    else if(AMP_FAULT_RFD_CHECK_IN == app_rf_module_error)
                    {
                        // Coordinator Only Receives this fault
                        // Already in RFD Checking Error?
                        if(AMP_FAULT_RFD_CHECK_IN != FLT_Net_Error_Status())
                        {
                            // No, Rec'd RFD Check-in Fault

                            // Network Size must be less than 3 devices for 
                            // Coordinator to enter Network Error
                            if(APP_Rf_Network_Size < APP_NET_SIZE_OF_3)
                            {
                                // Log This Network Error
                                FLT_Fault_Manager(app_rf_module_error);
                            }
                        }
                        else
                        {
                            // Already logged this fault
                            // Remain in Network Error
                        }
                        
                        // New Remote Error processed
                        FLAG_RMT_FAULT_NEW = 0;
                        FLAG_REMOTE_ERROR_REQ = 0;


                    }
                    else
                    {
                        // Log Network Error for all other received Net Errors
                        
                        // Pass Error Code to Fault Module for processing
                        FLT_Fault_Manager(app_rf_module_error);
                        
                        // New Remote Error processed, wait for next Error
                        // Status Message
                        FLAG_RMT_FAULT_NEW = 0;
                        FLAG_REMOTE_ERROR_REQ = 0;
                    }
                }
                else
                {
                    // Awaiting Network Error Code
                    if(FLAG_REMOTE_ERROR_REQ)
                    {
                        if(0 == --app_net_err_cnt)
                        {
                            if(0 == --app_net_err_attempts)
                            {
                                // Max # of attempts, Abort New Err Request
                                FLAG_REMOTE_ERROR_REQ = 0;
                                FLAG_RMT_FAULT_NEW = 0;
                            }
                            else
                            {
                                // Timed Out waiting for Error Code
                                // Enable Repeat Request for Error code
                                FLAG_REMOTE_ERROR_REQ = 0;
                            }
                        }
                    }
                }
            }
            else
            {
                // No Current Remote Network Error Status
                if(app_rf_module_error)
                {
                    // Clear Previous Error Received
                    FLT_Fault_Manager(FAULT_NO_FAULT | app_rf_module_error);

                    // Reset previous RF Module Received Error
                    app_rf_module_error = 0;
                    
                    // Allow potential New Network Error code to be requested
                    FLAG_REMOTE_ERROR_REQ = 0;
                    FLAG_RMT_FAULT_NEW = 0;
                }
            }
                
//******************************************************************************         
//******************************************************************************         
                
            // ***************************************************************
            // 3. Monitor for Status 1 Changes
            //

            // Check to see if Battery Test is requesting
            //  when the TXRX will be turned off
            if(FLAG_STATUS_1_TXRX_DISABLE_REQ)
            {
                app_update_status1(APP_ALM_Status_1.ALL);

                // Just Request TXRX Disabled One Time
                FLAG_STATUS_1_TXRX_DISABLE_REQ = 0;
            }
            
            // Test for a received Cancel Alarm request
            if(FLAG_STATUS_1_ALARM_CANCEL)
            {
                // Cancel all Potential Detection Alerts, Weather 
                //  or Panic Alarms
                APP_ALM_Status_2.ALL = 0x00;    
                APP_ALM_Status_3.ALL = 0x00; 
                
                FLAG_ALERT_DETECT = 0;
                
                // Cancel Identify if Enabled
                FLAG_ALARM_IDENTIFY = 0;
                
                #ifdef CONFIGURE_DEMO_TEST_FW
                    // Remote Alarm Cancel
                    FLAG_CNCL_SMK_CO_ALARM = 1;
                #endif

                #ifndef CONFIGURE_INTERCONNECT
                    // For DC Wireless Models, enable Alarm Cancel
                    //  It causes problems with Gateway Models

                    // This is disabled for now since it was 
                    //  cancelling at Smoke Over CO conditions and
                    //  causing problems in Gateway Units
                    
                    // Only Enable this if in a Remote Alarm condition
                    //  sometimes it gets sent from a previous alarm
                    //  when Real Time Mode is re-opened.
                    
                     uchar current_alm_state = ALM_Get_State();
                     if( (ALM_ALARM_REMOTE_SMOKE == current_alm_state) ||
                         (ALM_ALARM_REMOTE_CO == current_alm_state) ||
                         (ALM_ALARM_REMOTE_SMOKE_HW == current_alm_state) ||
                         (ALM_ALARM_REMOTE_CO_HW == current_alm_state) )
                     {
                            // Remote Alarm Cancel
                            FLAG_CNCL_SMK_CO_ALARM = 1;
                     }
                     else
                     {
                        // Ignore Remote Alarm Cancel
                     }

                #endif
                   
                FLAG_STATUS_1_ALARM_CANCEL = 0;
            }
            
            if(FLAG_STATUS_1_REMOTE_HUSH)
            {
                /*
                 This basically acts much like a remote Single Button Press
                  to Silence current condition based upon Alarm Context.
                 Smoke Hush - if in Smoke Alarm
                 EOL Hush - if in EOL Mode
                 LB Hush - if in LB Trouble
                 Network Error Hush - if in Network Error Mode
                */
                
                // RF Hush Request 
                FLAG_STATUS_1_REMOTE_HUSH = 0;
                
                // Test for Hush Request (Smoke Alarm and not above Hush TH)
                if(FLAG_MASTER_SMOKE_ACTIVE)
                {
                    
                    #ifdef CONFIGURE_AUSTRALIA
                        // No - Too Much Smoke limit for Australia
                        // Execute a Smoke Alarm Hush Request
                        FLAG_SMOKE_HUSH_REQUEST = 1;
                        FLAG_SMK_EXIT_TO_HUSH = 1;
                        MAIN_Reset_Task(TASKID_PHOTO_SMOKE);

                         // Send Hush Activated Voice
                        #ifdef CONFIGURE_VOICE
                            VCE_Play(VCE_HUSH_ACTIVATED);
                        #endif

                    #else
                        // Check last smoke measured level
                        if(PHOTO_Hush_Too_High())
                        {
                             // Send Hush Activated Voice
                            #ifdef CONFIGURE_VOICE
                                VCE_Play(VCE_TOO_MUCH_SMOKE);
                            #endif
                        }
                        else
                        {
                            // execute a Smoke Alarm Hush Request
                            FLAG_SMOKE_HUSH_REQUEST = 1;
                            FLAG_SMK_EXIT_TO_HUSH = 1;
                            MAIN_Reset_Task(TASKID_PHOTO_SMOKE);

                             // Send Hush Activated Voice
                            #ifdef CONFIGURE_VOICE
                                VCE_Play(VCE_HUSH_ACTIVATED);
                            #endif
                                
                            // WIFI models do not TX Locate    
                            #ifndef CONFIGURE_WIFI
                                #ifdef CONFIGURE_WIRELESS_ALARM_LOCATE
                                    // Don't Send Locate if already 
                                    // in Locate when Hush was requested
                                    if(!FLAG_ALARM_LOCATE_MODE)
                                    {
                                        // Send Locate Message from Hush App
                                        //  module so it transmits before Smoke 
                                        //  Alarm Clear. This is important for 
                                        //  Smoke Hush feature to work correctly
                                        APP_Send_Alarm_Locate();
                                    }
                                #endif
                            #endif
                                
                        } 
                    #endif
                }
                else if(FLAG_SMOKE_ALARM_ACTIVE)
                {
                    // This allows Remote and Slave Smoke Alarms to
                    //  react sooner to a potential Master Smoke Hush
                    FLAG_SMK_EXIT_TO_HUSH = 1;
                }
                else
                {
                    /* 
                     * Remote Hush Initiate Received 
                     * Based upon Context of Current Conditions, Initiate Hush
                     * 
                     * IF EOL Mode - EOL Hush
                     * If Low Battery - Low Battery Hush
                     * If Network Error - Net Error Hush
                     * 
                     * Note: Smoke Alarm Hush is process above
                     * 
                     */

                    if(FLAG_EOL_MODE && !FLAG_EOL_HUSH_ACTIVE)
                    {
                        LIFE_Init_EOL_Hush();
                    }

                    #ifdef CONFIGURE_LB_HUSH
                        else if( FLAG_LOW_BATTERY && !FLAG_LB_HUSH_INHIBIT
                                                  && !FLAG_LB_HUSH_DISABLED
                                                  && !FLAG_LB_HUSH_ACTIVE )
                        {
                            // If in low battery, initiate low battery hush
                            BAT_Init_LB_Hush();
                        }
                    #endif

                    else if( FLAG_NETWORK_ERROR_MODE &&
                             !FLAG_NETWORK_ERROR_HUSH_ACTIVE )
                    {
                        // Unit is in a Network Error Condition
                        //  (under 7 days of Error)

                        // Error Code should be blinked when Hush
                        // is entered.
                        FLAG_REQ_ERROR_CODE_BLINK = 1;
                        FLAG_REQ_RESET_AFTER_BLINKS = 0;

                        // If in Network Error Mode - Request Error Hush
                        FLT_Init_Error_Hush();

                    }

                }
                
            }

            
            // ***************************************************************
            // 4. Monitor for Status 2 Input Changes
            //
            if(APP_ALM_Status_2.ALL != APP_Previous_ALM_Status_2.ALL)
            {
                // Update Status 2 conditions
                if(FLAG_STATUS_2_TEMP_DROP)
                {
                    // Drop in Temp
                    FLAG_ALERT_TEMP_DROP = 1;
                }
                else
                {
                    FLAG_ALERT_TEMP_DROP = 0;
                }
                
                if(FLAG_STATUS_2_WATER)
                {
                    // Water Detected
                    FLAG_ALERT_WATER = 1;
                }
                else
                {
                    FLAG_ALERT_WATER = 0;
                }

                if(FLAG_STATUS_2_PANIC)
                {
                    // Panic Alarm
                    FLAG_ALERT_PANIC = 1;
                }
                else
                {
                    FLAG_ALERT_PANIC = 0;
                }
                
                
                APP_Previous_ALM_Status_2.ALL = APP_ALM_Status_2.ALL;
            }
            
            // ***************************************************************
            // 5. Monitor for Status 3 Input Changes
            //
            if(APP_ALM_Status_3.ALL != APP_Previous_ALM_Status_3.ALL)
            {
                // Update Status 3 conditions
                if(FLAG_STATUS_3_GAS)
                {
                    FLAG_ALERT_EXP_GAS = 1;
                }
                else
                {
                    FLAG_ALERT_EXP_GAS = 0;
                }
                
                if(FLAG_STATUS_3_INTRUDER)
                {
                    FLAG_ALERT_INTRUDER = 1;
                }
                else
                {
                    FLAG_ALERT_INTRUDER = 0;
                }
                
                if(FLAG_STATUS_3_WEATHER)
                {
                    FLAG_ALERT_WEATHER = 1;
                }
                else
                {
                    FLAG_ALERT_WEATHER = 0;
                }

                if(FLAG_STATUS_3_GEN_ALERT)
                {
                    FLAG_ALERT_GENERAL = 1;
                }
                else
                {
                    FLAG_ALERT_GENERAL = 0;
                }
                
                if(FLAG_STATUS_3_HEAT_ALARM)
                {
                    if(FLAG_ALM_REMOTE_ENABLE)
                    {
                        // Notify AlmPrio module only if Remote Alarms are 
                        //  Enabled
                        FLAG_REMOTE_SMOKE_ACTIVE = 1;
                    }
                    // and Clear Heat Received Status
                    FLAG_STATUS_3_HEAT_ALARM = 0;
                }
                
                APP_Previous_ALM_Status_3.ALL = APP_ALM_Status_3.ALL;
            }
            
            if( FLAG_ALERT_TEMP_DROP || FLAG_ALERT_GENERAL ||
                FLAG_ALERT_WATER || FLAG_ALERT_INTRUDER || 
                FLAG_ALERT_EXP_GAS || FLAG_ALERT_WEATHER)
            {
                // Begin Alert Notification
                FLAG_ALERT_DETECT = 1;
            }
            else
            {
                // Do not clear Alert Notifications currently active
                
            }
            

            // ****************************************************************
            // 6. Supervise CCI Interface connection
            //
                
            #ifdef CONFIGURE_WIFI
                // If PTT is active, then postpone CCI Supervision messages
                if(FLAG_PTT_ACTIVE)   
                {
                    app_timer_wireless_supervision =
                        APP_STATUS_TIME_60_SEC_SUPERVISION;                    
                }
            #endif

            #ifdef CONFIGURE_APP_WIRELESS_SUPERVISION
                // Only Enable this supervision if Enrolled
                // Update CCI Supervision Timer and test LP Mode Supervise Flag
                if( (FLAG_COMM_JOIN_COMPLETE && !FLAG_COMM_JOIN_IN_PROGRESS) ||
                     FLAG_PREV_JOINED || FLAG_STAND_ALONE_MODE )
                {
                    // LP Mode Supervision Test
                    if(FLAG_CCI_TEST)
                    {
                        #ifdef CONFIGURE_WIFI
                            // No Low Power supervision with WiFi
                            FLAG_CCI_TEST = 0;
                        #else
                            // Ping RF Module
                            FLAG_PING_ACK = 0;  // Clear Ping Flag
                            app_send_ping();

                            FLAG_APP_NOT_IDLE = 1;
                            app_status_state = APP_MONITOR_SUPERVISION;

                            // process faster
                            return MAIN_Make_Return_Value(APP_STATUS_MONITOR_10_MS);
                        #endif
                    }
                    else if( 0 == --app_timer_wireless_supervision)
                    {
                        // Increase Supervision Time after 1st 30 minutes
                        //  From Power On/Reset
                        if(APP_SPRV_1ST_30_MINUTES <= app_cci_super_count)
                        {
                            
                            #ifdef CONFIGURE_DEBUG_NET_DEVICES_TEST
                                app_timer_wireless_supervision =
                                    APP_STATUS_TIME_60_SEC_SUPERVISION;
                            #else
                                #ifdef CONFIGURE_WIFI
                                    // Hold at 60 sec supervision for WiFi
                                    app_timer_wireless_supervision =
                                        APP_STATUS_TIME_60_SEC_SUPERVISION;
                                #else
                                    // Change to longer Supervise Time Interval
                                    app_timer_wireless_supervision =
                                        APP_STATUS_TIME_15_MIN_SUPERVISION;
                                #endif
                            #endif

                        }
                        else
                        {
                            app_cci_super_count++;
                            app_timer_wireless_supervision =
                                    APP_STATUS_TIME_60_SEC_SUPERVISION;

                        }

                        // Ping RF Module
                        FLAG_PING_ACK = 0;  // Clear Ping Flag
                        app_send_ping();
                        
                        FLAG_APP_NOT_IDLE = 1;
                        app_status_state = APP_MONITOR_SUPERVISION;
                    }
                    
                    #ifdef CONFIGURE_MONITOR_INT
                        // Test only, monitor app_timer_wireless_supervision
                        MAIN_Monitor_Int1 = app_timer_wireless_supervision;
                    #endif
                    
                }
                else
                {
                    // Insure No out standing Low Power CCI test pending 
                    //  here if not enrolled
                    FLAG_CCI_TEST = 0;

                }

            #else
                if(0 == --app_timer_wireless_supervision)
                {
                    app_timer_wireless_supervision =
                                  APP_STATUS_TIME_60_SEC_SUPERVISION;
                }
            #endif



            // ****************************************************************
            // 7. Send RF Module Required COMM Status Outputs
            //
            if(FLAG_REQ_JOIN_AT_RST)
            {
                // Request to Join or Open a Network after a Soft reset
                FLAG_REQ_JOIN_AT_RST = 0;
                app_send_comm_status(AMP_COMM_STAT_ENABLE_REG);
            }
            else if(FLAG_REQ_NETWORK_CLOSE)
            {
                // Per Button Action, Request Network Close
                FLAG_REQ_NETWORK_CLOSE = 0;
                app_send_comm_status(AMP_COMM_STAT_ENABLE_REG);

            }
            else if(FLAG_REQ_ESTABLISH_COORD)
            {
                // Per Button Action, Create a New Network Coordinator
                FLAG_REQ_ESTABLISH_COORD = 0;
                app_send_comm_status(AMP_COMM_STAT_COORDINATOR);
            }
            else if(FLAG_REQ_RESET_OOB)
            {
                // Per Button Action, Reset to OOB
                FLAG_REQ_RESET_OOB = 0;

                app_send_comm_status(AMP_COMM_STAT_RESET_COMM_OOB);

                // Enable OOB LED Profile
                FLAG_LED_OOB_PROFILE_ENABLE = 1;
                
                // Start profile Immediately
                MAIN_Reset_Task(TASKID_ALED_MANAGER);
                
            #ifdef CONFIGURE_WIFI
                // Init Device ID at OOB
                app_rf_module_id = 1;
                APP_Rf_Network_Size = 0;
            #else
               // Clear Device ID at OOB
                app_rf_module_id = 0;
                APP_Rf_Network_Size = 0;
            #endif

            }
            else if(FLAG_REQ_ENABLE_JOIN)
            {
                // Request to Join or Open a Network
                FLAG_REQ_ENABLE_JOIN = 0;
                app_send_comm_status(AMP_COMM_STAT_ENABLE_REG);
                
                #ifdef CONFIGURE_WIFI
                    //
                    // And request Comm Status again
                    // Should not need to do this...
                    //
                
                    //APP_Request_Comm_Status();
                #endif
                
            }
            else
            {
                // TODO: Undefine Later
                // Should No Longer need since we test each COMM OUT condition
                //  above...
                // Test for Change in COMM Status Output Bits
                #ifdef CONFIGURE_UNDEFINED
                    if(APP_Current_COMM_Status.ALL != 
                           APP_Previous_COMM_Status.ALL)
                    {
                        // Send COMM Status out then save as Previous Status
                        app_send_comm_status(APP_Current_COMM_Status.ALL);
                        APP_Previous_COMM_Status.ALL =
                            APP_Current_COMM_Status.ALL;

                    }
                #endif
            }


            // ****************************************************************
            // 8. Monitor any received COMM Status Bit Changes here
            //    Enroll process is being monitored via Comm Status Bits
            //
                
            if(!FLAG_COMM_JOIN_COMPLETE)
            {

                // Alarm Join Mode is not Complete
                if(FLAG_COMM_COORDINATOR)
                {

                    // COORDINATOR
                    if(FLAG_COMM_JOIN_IN_PROGRESS)
                    {
                        // Alarm has not joined, Coordinator Join is in Progress
                        // Network search is over as RFD, and coordinator is
                        // established
                        if(!FLAG_APP_JOIN_MODE)
                        {
                            // Set Flags for LED Module
                            //   (Join Active, Search ended)
                            FLAG_APP_SEARCH_MODE = 0;
                            FLAG_APP_JOIN_MODE = 1;
                            MAIN_Reset_Task(TASKID_ALED_MANAGER);
                            MAIN_Reset_Task(TASKID_GLED_MANAGER);
                            MAIN_Reset_Task(TASKID_RLED_MANAGER);

                        }

                    }
                    else
                    {
                       // Not sure this condition will occur
                       // Coordinator Join Mode has ended and no Join Completed
                        FLAG_APP_SEARCH_MODE = 0;
                        FLAG_APP_JOIN_MODE = 0;
                        
                    }
                }
                else
                {
                    // Not a Coordinator, check for RFD
                    if(FLAG_COMM_RFD)
                    {
                        // RFD Join
                        if(FLAG_COMM_JOIN_IN_PROGRESS)
                        {
                            // Alarm not joined, but RFD Join is in Progress
                            // In this case blink Searching for Network LED
                            //    indication
                            if(!FLAG_APP_SEARCH_MODE)
                            {
                                // Set Flags for LED Module
                                // (searching for network)
                                FLAG_APP_SEARCH_MODE = 1;
                                FLAG_APP_JOIN_MODE = 0;
                                MAIN_Reset_Task(TASKID_ALED_MANAGER);
                                MAIN_Reset_Task(TASKID_GLED_MANAGER);
                                MAIN_Reset_Task(TASKID_RLED_MANAGER);

                            }

                        }
                        else
                        {
                           // 
                           // Not sure this condition will occur
                           // RFD No Join in Progress and no Join Completed
                           // 
                           // This will occur if RFD fault re-join times out
                           //  and RFD cannot Hear Time Sync in 15 minutes.
                           // Alarm should enter Error Mode
                           // 
                            
                            #ifdef CONFIGURE_WIFI
                                //
                                // If not in Search or Cloud Join state
                                //  and COMM Status 0x80 is received 
                                //  (RFD timeout)
                                //  then set Stand Alone Indicator
                                //
                                if(!FLAG_APP_SEARCH_MODE && 
                                   !FLAG_APP_JOIN_MODE  &&
                                   !FLAG_PREV_JOINED && 
                                   !FLAG_STAND_ALONE_MODE)
                                {
                                   // Set Stand Alone indication
                                   FLAG_STAND_ALONE_MODE = 1; 
                                }
                            #endif

                            // Terminate Search or Join states
                            FLAG_APP_SEARCH_MODE = 0;
                            FLAG_APP_JOIN_MODE = 0;

                        }
                    }
                    else
                    {
                        // This would be OOB State
                        // No RFD no COORD set
                        FLAG_APP_JOIN_MODE = 0;
                        FLAG_APP_SEARCH_MODE = 0;

                    }

                }
            }
            else
            {
                // Alarm Join Mode is Complete
                if(FLAG_COMM_COORDINATOR)
                {
                    if(FLAG_COMM_JOIN_IN_PROGRESS)
                    {
                        // Alarm has become a coordinator, Network Join in
                        // Progress is still Active.
                        // In this case blink the Join Mode Active LED
                        //     indication
                        if(!FLAG_APP_JOIN_MODE)
                        {
                            // Set Flags for LED Module (Join Mode still active)
                            FLAG_APP_SEARCH_MODE = 0;
                            FLAG_APP_JOIN_MODE = 1;
                            MAIN_Reset_Task(TASKID_ALED_MANAGER);
                            MAIN_Reset_Task(TASKID_GLED_MANAGER);
                            MAIN_Reset_Task(TASKID_RLED_MANAGER);
                        }

                    }
                    else    // Coordinator has ended Join Mode
                    {
                        // Set Flags for LED Module
                        // Network Join Mode has ended
                        FLAG_APP_JOIN_MODE = 0;
                        FLAG_APP_SEARCH_MODE = 0;

                    }

                }
                else    // Default to RFD indications
                {
                    if(FLAG_COMM_JOIN_IN_PROGRESS)
                    {

                        /* Alarm has found a network to join, but RFD Join in
                         * Progress is still Active.
                         * In this case continue the Join Mode Active LED
                         *     indication
                         */
                        if(!FLAG_APP_JOIN_MODE)
                        {
                            // Set Flags for LED Module (Join Mode still active)
                            FLAG_APP_SEARCH_MODE = 0;
                            FLAG_APP_JOIN_MODE = 1;
                            MAIN_Reset_Task(TASKID_ALED_MANAGER);
                            MAIN_Reset_Task(TASKID_GLED_MANAGER);
                            MAIN_Reset_Task(TASKID_RLED_MANAGER);
                        }
                    }
                    else
                    {
                        #ifdef CONFIGURE_WIFI
                            //
                            // RFD Join Complete is set if we get here
                            // Note for WiFi this occurs when COMM_ONLINE 
                            // becomes TRUE (Cloud Connected))
                            //
                            // If not in Join and we receive Cloud Connected 
                            //  COMM Status, allow Join entry to manage 
                            //  Previously Joined and give the green flicker
                            // Monitor GLED not idle to insure GRN Join state
                            //  is not in the process of ending
                            //
                            if(FLAG_COMM_ONLINE && !FLAG_PREV_JOINED &&
                                !FLAG_APP_JOIN_MODE &&
                                !FLAG_GLED_NOT_IDLE)
                            {
                                FLAG_APP_SEARCH_MODE = 0;
                                FLAG_APP_JOIN_MODE = 1;
                            }
                            else
                            {
                                // Network Join Mode has ended
                                FLAG_APP_JOIN_MODE = 0;
                                FLAG_APP_SEARCH_MODE = 0;
                            }
                            
                        #else

                            // Network Join Mode has ended
                            FLAG_APP_JOIN_MODE = 0;
                            FLAG_APP_SEARCH_MODE = 0;
                        #endif

                    }
                }
            }

            // Net Size = 0 for WiFi wireless    
            #ifndef CONFIGURE_WIFI
                // Has operator requested # of devices?
                if(FLAG_REQ_NUM_DEVICES)
                {
                    FLAG_REQ_NUM_DEVICES = 0;
                    APP_Request_Net_Size();

                }
            #endif

        break;
        }

        case APP_MONITOR_JOIN:
        {
            // According to Commissioning State Diagram Join is not automatic
            // by RF module, RF Module is waiting for a Join request to proceed

            // Request Join/Network Open
            FLAG_REQ_ENABLE_JOIN = 1;

            // Proceed back to Idle and allow Idle State to monitor Comm
            //  Status Join Progress
            app_status_state = APP_MONITOR_IDLE;

        }
        break;

        case APP_MONITOR_ENROLL_STATE:
        {
            // We should have received COMM Status by Now, check it.

            // Wait here until enrollment status has been received
            // If COMM_STATUS is not 0xFF, then it has been received
            if(APP_Remote_COMM_Status.ALL != 0xFF)
            {
                
                #ifndef CONFIGURE_UNDEFINED
                    SER_Send_String("\rAPP COMM Status - ");
                    SER_Send_Int(' ',(uint)APP_Remote_COMM_Status.ALL,16);
                    SER_Send_String(" received\r");
                #endif
                
                // Here we test enrollment status
                if(FLAG_COMM_RFD && FLAG_COMM_COORDINATOR)
                {
                    // An Invalid condition
                    FLAG_APP_NOT_IDLE = 1;
                    app_status_state = APP_MONITOR_ERROR;
                }
                // Here we test enrollment status
                // Do not set previously joined if Stand Alone was set
                else if(FLAG_COMM_RFD || FLAG_COMM_COORDINATOR) 
                {
                    // RF module is enrolled, proceed to IDLE state
                    app_status_state = APP_MONITOR_IDLE;
                    
                }
                else
                {
                    //
                    // RF Module is currently not enrolled in a Network
                    //  Auto Join
                    //
                    
                    app_status_state = APP_MONITOR_JOIN;
                    
                    #ifndef CONFIGURE_UNDEFINED
                        // TODO: Undefine Later  BC: Test Only
                        SER_Send_String("\rAPP_Join_Request\r");
                    #endif
                }
            }
            else
            {
                // Comm status was not received, Go to Error State for Now
                FLAG_FAULT_CCI_IMMED = 1;
                FLT_Fault_Manager(AMP_FAULT_CCI);
                app_status_state = APP_MONITOR_CCI_FAULT;
                
                // TODO: Undefine Later  BC: Test Only 
                #ifndef CONFIGURE_UNDEFINED
                    SER_Send_String("\rEnroll CCI Fault\r");
                #endif
                
            }
            
        break;
        }

        case APP_MONITOR_SUPERVISION:
            // Ping Acknowledge should have occurred by now
            if(FLAG_PING_ACK)
            {
                // All is well with CCI connection to RF module
                FLAG_PING_ACK = 0;
                FLT_Fault_Manager(AMP_SUCCESS_CCI);

                //No Local network IDs for WiFi
                #ifndef CONFIGURE_WIFI
                    // Also, Update RFID in case something has changed
                    //  with network ID assignments
                    APP_Request_Local_Id();
                #endif
                
                //No network size message for WiFi
                #ifndef CONFIGURE_WIFI
                    // Wait a ms before Net Size request for transaction
                    //  to send
                    PIC_delay_ms(1);

                    // Also update device Count in case something has changed
                    //  before an Error Condition
                    FLAG_NET_SIZE_DISPLAY_OFF = 1;
                    APP_Request_Net_Size();
                    
                    // Wait a ms before Net Size request for transaction
                    //  to send
                    PIC_delay_ms(1);
                #endif
                
                // Update Status request
                APP_Request_Status();
                

                if(FLAG_CCI_TEST)
                {
                    // LP Test Completed, allow LP Mode resumption
                    FLAG_CCI_TEST = 0;
                    
                    #ifdef CONFIGURE_DEBUG_NET_DEVICES_TEST
                        // Return to Sleep if From LP Mode
                        MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_2_SEC;
                        FLAG_ACTIVE_TIMER = 1;
                    #else
                        // Speed up Return to Sleep if From LP Mode
                        MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_30_MS;
                        FLAG_ACTIVE_TIMER = 1;
                    #endif

                    app_status_state = APP_MONITOR_IDLE;
                    return MAIN_Make_Return_Value(APP_STATUS_MONITOR_10_MS);
                }
            }
            else
            {
                #ifndef CONFIGURE_UNDEFINED
                    SER_Send_String("\rPing ACK CCI Fault\r");
                #endif
                
                
                //  Log a supervise Fault
                FLT_Fault_Manager(AMP_FAULT_CCI);

                // Override Supervisor Timer to test more often
                
                app_timer_wireless_supervision =
                                    APP_STATUS_TIME_60_SEC_SUPERVISION;
            #ifndef CONFIGURE_WIFI                
                if(FLAG_CCI_TEST)
                {
                    // Clear LP Mode CCI Test Flag
                    FLAG_CCI_TEST = 0;
                    // Override Supervisor Timer to test more often
                    //  for faster fault recovery.
                    app_lp_cci_supervisor_timer = LP_TIMER_ONE_MINUTE;
                }
            #endif

            }
            
            app_status_state = APP_MONITOR_IDLE;
        break;

        case APP_MONITOR_ERROR:
        {
            if(!FLAG_PTT_ACTIVE)
            {
                // Hang Here in Error State for Now with RED LED ON
                // A PTT/Reset is needed to clear from this state
                RLED_On();
            }
        break;
        }

        case APP_MONITOR_FINAL_TEST_WAIT:
        {
            // Wait for manuf. Final Test Completed, Power Cycle/Reset 
            //   required to exit this Wireless APP state.
            if(FLAG_STATUS_1_FINAL_TESTED)
            {
                //  Global Ints Disabled
                GLOBAL_INT_ENABLE = 0;
                
                // Turn Off WDT before sleep
                WDTCON0bits.SWDTEN = 0;
                
                #ifdef CONFIGURE_UNDEFINED
                    SER_Send_String("Tested...");
                #endif
                
                // Turn Off Red LED and Go To Sleep
                //  This allows a Current Measurement to be done in Production
                RLED_Off();
                RED_LED_OFF
                        
                GLED_Off();        
                GREEN_LED_OFF
                
                LAT_VBOOST_PIN = 0;     // same as VBOOST_OFF

                // Remember Turn off Drive to Voice Clock/Data transistors
                PORT_VOICE_EN_PIN = 0;      // Voice Chip Power Off
                FLAG_VCE_PWR_ON_DELAY_COMPLETE = 0;
                
                LAT_VOICE_CLK_PIN = 0;      // and no Voltage on Voice Inputs
                LAT_VOICE_DATA_PIN = 0;

                // Sleep Mode - T1 Int Flag used to wake from sleep
                PIE4bits.TMR1IE = 0;
                PIR4bits.TMR1IF = 0;
                PERIPERAL_INT_ENABLE = 0;

                // Turn Timer 4 Off (1 ms Timer)
                T4CONbits.TMR4ON = 0;
                // Disable 1MS Timer Ints
                PIR4bits.TMR4IF = 0;
                PIE4bits.TMR4IE = 0;

                // Timers Off
                T0CON0bits.T0EN = 0;
                T6CONbits.T6ON = 0;

                CCP1CONbits.CCP1EN = 0;
                CCP4CONbits.CCP4EN = 0;

                IOC_INTERRUPT_FLAG = 0;
                IOC_INTERRUPT_ENABLE = 0;

                // Turn Off AD during Sleep
                A2D_Off();
                
                // Turn off Brown Out (saves current)
                BORCONbits.SBOREN = 0;
                // Insure Turned off FVR to save power
                FVRCONbits.FVREN = 0;
                
                
                // --------------------------------------------------------
                GOTOSLEEP
                // --------------------------------------------------------
                
            }
            else
            {
                #ifdef CONFIGURE_UNDEFINED
                    // TODO: Undefine Later  BC: Test Only
                    SER_Send_String("Waiting...");
                #endif
                RLED_On();
                
            }

            return MAIN_Make_Return_Value(APP_STATUS_MONITOR_1000_MS);

        }

        case APP_MONITOR_CCI_FAULT:
        {
            /*
             * This state is entered if during Power On / Reset
             *  we received no communication via CCI interface
             *             
             * If in Low Power Mode and CCI Test is received while
             *  in this state, clear CCI Test and set FLAG_RST_FAULT_AT_RST
             *  in an attempt to recover
             */
            if(FLAG_CCI_TEST)
            {
                // Low Power Mode requested a CCI supervision test
                FLAG_RST_FAULT_AT_RST = 1;
                FLAG_CCI_TEST = 0;
                return MAIN_Make_Return_Value(APP_STATUS_MONITOR_100_MS);
            }
            else
            {
                // CCI Fault Mode 
                app_status_state = APP_MONITOR_POWER_DELAY;
                
                #ifdef CONFIGURE_WIFI
                    // Repeat Ping at Power Delay State
                    FLAG_AC_WIFI_RST = 1;
                #endif
                
            }
            
            #ifdef CONFIGURE_WIFI
                return MAIN_Make_Return_Value(APP_STATUS_MONITOR_5000_MS);
            #else
                return MAIN_Make_Return_Value(APP_STATUS_MONITOR_5000_MS);
            #endif
        }

        default:
            app_status_state = APP_MONITOR_IDLE;
        break;

    }

    return MAIN_Make_Return_Value(APP_STATUS_MONITOR_100_MS);

}

/*
+------------------------------------------------------------------------------
| Function:      APP_Init
+------------------------------------------------------------------------------
| Purpose:       Reset APP module variables as needed depending upon
|                Reset Type
+------------------------------------------------------------------------------
| Parameters:    action - APP_INIT_ACTION_PWR_RST
|                         APP_INIT_ACTION_SOFT_RST
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void APP_Init(uchar action)
{
    // If Wireless Module is not operating, simply return
    if(!FLAG_WIRELESS_DISABLED)
    {
        switch(action)
        {
            case APP_INIT_ACTION_PWR_RST:
                // Initialize Status variables at Power On Reset
                APP_Current_ALM_Status.ALL = 0;
                APP_Previous_ALM_Status.ALL = 0;
                APP_Remote_ALM_Status.ALL = 0;

                APP_ALM_Status_2.ALL = 0;
                APP_Previous_ALM_Status_2.ALL = 0;

                APP_ALM_Status_3.ALL = 0;
                APP_Previous_ALM_Status_3.ALL = 0;

                APP_Current_COMM_Status.ALL = 0;
                APP_Previous_COMM_Status.ALL = 0;

                Flags_Remote_Alerts.ALL = 0;
                Flags_Remote_Flags1.ALL = 0;

                // These 2 parameters should be received at Power reset
                //  automatically over CCI before they are processed..
                APP_Remote_COMM_Status.ALL = 0xFF;
                APP_ALM_Status_1.ALL = 0xFF;
                
            #ifdef CONFIGURE_WIFI
                // Init Device ID at OOB
                app_rf_module_id = 1;
                APP_Rf_Network_Size = 0;
            #else
                // Clear Device ID at OOB
                app_rf_module_id = 0;
                // Clear Network Size (unknown at POR)
                APP_Rf_Network_Size = 0;
            #endif
                
            break;

            case APP_INIT_ACTION_SOFT_RST:
                // Initialization after soft Reset 

                // Clear Current status Only, if a change from previous status
                //  then updated status will be sent via CCI
                APP_Current_ALM_Status.ALL = 0;
                APP_Previous_ALM_Status.ALL = 0;

                // Clear Alarm Status after Reset Initialization
                FLAG_APP_SEND_ALM_STATUS = 1;
                
                #ifdef CONFIGURE_WIFI
                    //
                    // This will also request COMM Status and Status 1 again
                    //  so initialize Status 1 and COMM status to un-received
                    //  Initial value
                    //
                    APP_Remote_COMM_Status.ALL = 0xFF;
                    APP_ALM_Status_1.ALL = 0xFF;
                #endif

            break;

            case APP_INIT_ACTION_NOT_PWR_RST:
                // Not a true Power Reset or Soft Reset

                // Probably From Brown Out, Stack Error or WDT reset
                // Initialize Status variables for this type of Reset
                // Leave COMM Status variable to existing status, but request it

                APP_Current_ALM_Status.ALL = 0;
                APP_Previous_ALM_Status.ALL = 0;
                APP_Remote_ALM_Status.ALL = 0;

                APP_ALM_Status_2.ALL = 0;
                APP_Previous_ALM_Status_2.ALL = 0;

                APP_ALM_Status_3.ALL = 0;
                APP_Previous_ALM_Status_3.ALL = 0;

                APP_Current_COMM_Status.ALL = 0;
                APP_Previous_COMM_Status.ALL = 0;

                Flags_Remote_Alerts.ALL = 0;
                Flags_Remote_Flags1.ALL = 0;

                // Reset Alarm Status via CCI after Reset Initialization
                //  and request Comm Status again.
                FLAG_APP_SEND_ALM_STATUS = 1;

            break;

        }
    }
}


#ifndef CONFIGURE_WIFI 
/*
+------------------------------------------------------------------------------
| Function:      APP_LP_CCI_Timer
+------------------------------------------------------------------------------
| Purpose:       Update and Test the LP CCI Supervision Timer
|                In LP Mode call this routine to periodically test the
|                CCI interface by transition to Active Mode for a Ping Test
|                FLAG_CCI_TEST becomes active when Timer Expires.
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void APP_LP_CCI_Timer(void)
{
    // Initially Set for 4 minutes
    if(0 == --app_lp_cci_supervisor_timer)
    {
        // Trigger LP to Active Mode CCI Supervision Test
        FLAG_CCI_TEST = 1;

        #ifdef CONFIGURE_DEBUG_NET_DEVICES_TEST
            // Reset the LP Mode CCI Supervision Timer (1 minute)
            app_lp_cci_supervisor_timer = LP_TIMER_ONE_MINUTE;
        #else
            // Reset the LP Mode CCI Supervision Timer 
            // (increased to 30 minutes after 1st 4 minutes in LP Mode)
            app_lp_cci_supervisor_timer = LP_TIMER_THIRTY_MINUTES;
        #endif
        
    }
    else
    {
        // Request Device Count if not yet initialized in LP Mode
        if((0 == APP_Rf_Network_Size) && (!FLAG_STAND_ALONE_MODE))
        {
            // Trigger LP to Active Mode CCI Supervision Test
            FLAG_CCI_TEST = 1;
        }
    }
}
#endif

/*
+------------------------------------------------------------------------------
| Function:      app_process_comm_status
+------------------------------------------------------------------------------
| Purpose:       Examine Received Comm status for valid data
|                and save if valid.
|
|
+------------------------------------------------------------------------------
| Parameters:    received_status - Received COMM status byte
+------------------------------------------------------------------------------
| Return Value:  Returns zero if no error during processing.
|                Returns one  if error.
+------------------------------------------------------------------------------
*/

uchar app_process_comm_status(uchar received_status)
{
    // Some bits should not be set by the RF module.  If any of these are set
    //  that's an error.
    if(received_status & (AMP_COMM_STAT_ENABLE_REG |
                           AMP_COMM_STAT_RESET_COMM_OOB) )
    {
        #ifndef CONFIGURE_UNDEFINED
            // TODO: Undefine Later  BC: Test Only
            SER_Send_String("RX COMM Status ERR");
            SER_Send_Prompt();
        #endif

        return 1;
    }
    else
    {
        APP_Remote_COMM_Status.ALL = received_status;
        
        #ifdef CONFIGURE_UNDEFINED
            // Test Only
            SER_Send_String("RX_STAT - ");
            SER_Send_Int(' ',APP_Remote_COMM_Status.ALL,16);
            SER_Send_Prompt();
        #endif
        
        if(FLAG_COMM_JOIN_IN_PROGRESS && !FLAG_COMM_JOIN_COMPLETE)
        {
            if(!FLAG_PREV_JOINED)
            {
                // If not enrolled and in Network Search, notify Green LED  
                //   Search State to support Green LED flickering when 
                //   not previously enrolled
                FLAG_FORCE_INTO_SEARCH = 1;
            }
            else
            {
                // If already enrolled do not force Green LED flickering
            }
        }
    }

    return 0;
}


/*
+------------------------------------------------------------------------------
| Function:      APP_Process_Response
+------------------------------------------------------------------------------
| Purpose:       Message Response Processing
|                Based on the message sent, decide what to do with the
|                message received.
|
+------------------------------------------------------------------------------
| Parameters:    sent and received packets of type tag_CCI_Packet
+------------------------------------------------------------------------------
| Return Value:  Returns zero if no errors detected and message was
|                processed successfully.
+------------------------------------------------------------------------------
*/

uchar APP_Process_Response(union tag_CCI_Packet *SentPacket,
                           union tag_CCI_Packet *RxPacket)
{

    switch (SentPacket->tag)
    {

        case AMP_PING:
            if(AMP_PING_ACK == RxPacket->tag)
            {
                if(RxPacket->data == SentPacket->data + SentPacket->tag)
                {
                    FLAG_PING_ACK = 1;
                    
                    return APP_PROCESS_RESPONSE_SUCCESS;
                }
                else
                {
                    return APP_PROCESS_RESPONSE_ERROR;
                }
            }
            else
            {
                return APP_PROCESS_RESPONSE_ERROR;
            }

        case AMP_RF_TEST_STATUS:
            if(AMP_RF_TEST_STATUS == RxPacket->tag)
            {
                if(RxPacket->data == SentPacket->data + SentPacket->tag)
                {
                    return APP_PROCESS_RESPONSE_SUCCESS;
                }
                else
                {
                    return APP_PROCESS_RESPONSE_ERROR;
                }
            }
            else
            {
                return APP_PROCESS_RESPONSE_ERROR;
            }
            
       case AMP_STATUS:
            if(AMP_STATUS == RxPacket->tag)
            {
                if(RxPacket->data == SentPacket->data + SentPacket->tag)
                {
                    return APP_PROCESS_RESPONSE_SUCCESS;
                }
                else
                {
                    return APP_PROCESS_RESPONSE_ERROR;
                }
            }
            else
            {
                return APP_PROCESS_RESPONSE_ERROR;
            }


	case AMP_COMM_STATUS:
            if(AMP_COMM_STATUS_ACK == RxPacket->tag)
            {
                if(RxPacket->data == SentPacket->data + SentPacket->tag)
                {
                    return APP_PROCESS_RESPONSE_SUCCESS;
                }
                else
                {
                    return APP_PROCESS_RESPONSE_ERROR;
                }
            }
            else
            {
                return APP_PROCESS_RESPONSE_ERROR;
            }

	case AMP_STATUS1:
            if(AMP_STATUS1 == RxPacket->tag)
            {
                if(RxPacket->data == SentPacket->data + SentPacket->tag)
                {
                    return APP_PROCESS_RESPONSE_SUCCESS;
                }
                else
                {
                    return APP_PROCESS_RESPONSE_ERROR;
                }
            }
            else
            {
                return APP_PROCESS_RESPONSE_ERROR;
            }

	case AMP_STATUS2:
            if(AMP_STATUS2 == RxPacket->tag)
            {
                if(RxPacket->data == SentPacket->data + SentPacket->tag)
                {
                    return APP_PROCESS_RESPONSE_SUCCESS;
                }
                else
                {
                    return APP_PROCESS_RESPONSE_ERROR;
                }
            }
            else
            {
                return APP_PROCESS_RESPONSE_ERROR;
            }
            
	case AMP_ERROR_CODE:
            if(AMP_ERROR_CODE == RxPacket->tag)
            {
                    // Save the RF error code
                    app_rf_module_error = RxPacket->data;
                    return APP_PROCESS_RESPONSE_SUCCESS;
            }
            else
            {
                return APP_PROCESS_RESPONSE_ERROR;
            }

        #ifdef CONFIGURE_APP_COMM_VERSION
            case AMP_REQUEST_AMP_VER:
                if(AMP_REQUEST_AMP_VER == RxPacket->tag)
                {
                    app_rf_amp_version = RxPacket->data;
                    return APP_PROCESS_RESPONSE_SUCCESS;
                }
                else
                {
                    return APP_PROCESS_RESPONSE_ERROR;
                }
        #endif

        #ifdef AMP_STATUS_REQUEST_SUPPORTED
            case AMP_STATUS_REQUEST:
                if(AMP_STATUS_REQUEST == RxPacket->tag)
                {
                        // Save the received status and process as needed
                        //     in APP_Monitor
                        APP_Remote_ALM_Status.ALL = RxPacket->data;

                        // Acknowledge Received OK
                        return APP_PROCESS_RESPONSE_SUCCESS;
                }
                else
                {
                    return APP_PROCESS_RESPONSE_ERROR;
                }

        #endif

	case AMP_STATUS1_REQ:
            if(AMP_STATUS1_REQ == RxPacket->tag)
            {
                APP_ALM_Status_1.ALL = RxPacket->data;

                // Acknowledge Received OK
                return APP_PROCESS_RESPONSE_SUCCESS;
            }
            else
            {
                return APP_PROCESS_RESPONSE_ERROR;
            }

	case AMP_COMM_STATUS_REQ:
            if(AMP_COMM_STATUS_ACK == RxPacket->tag)
            {
                if(app_process_comm_status(RxPacket->data) )
                {

                    // There was an error processing the data.
                    return APP_PROCESS_RESPONSE_ERROR;
                }
                else
                {
                    // Acknowledge Received OK
                    return APP_PROCESS_RESPONSE_SUCCESS;
                }
            }
            else
            {
                return APP_PROCESS_RESPONSE_ERROR;
            }


        case AMP_GET_LOGICAL_ADDRESS:
            if(AMP_GET_LOGICAL_ADDRESS == RxPacket->tag)
            {
                if( RxPacket->data > APP_MAX_ID )
                {
                    if(FLAG_COMM_COORDINATOR)
                    {
                        // Coord ID is always Zero
                        app_rf_module_id = APP_ZERO_ID;
                    }
                    else if(APP_ID_FF == RxPacket->data)
                    {
                        // Not part of a network and never enrolled
                        // No Action Taken
                    }
                    else if(APP_ID_FE == RxPacket->data)
                    {
                       // Not part of a network but previously enrolled 
                       // No Action Taken
                    }
                    else
                    {
                        // On illegal ID set Max ID for RFDs
                        app_rf_module_id = APP_MAX_ID;
                    }
                    
                    // Unexpected Result but report as Success since FF and FE
                    //  are valid responses if not joined
                    return APP_PROCESS_RESPONSE_SUCCESS;
                }
                else
                {
                    // Force to 1 on WiFi Wireless
                    #ifdef CONFIGURE_WIFI                    
                        app_rf_module_id = 1;
                    #else
                        // Valid enrolled ID received
                        app_rf_module_id = RxPacket->data;
                    #endif
                }

                // Acknowledge Received OK
                return APP_PROCESS_RESPONSE_SUCCESS;
            }
            else
            {
                return APP_PROCESS_RESPONSE_ERROR;
            }

            
            
//****************************************************************************** 
//******************************************************************************         
// Network Size Processing
            
        case AMP_NETWORK_SIZE:
            if(AMP_NETWORK_SIZE == RxPacket->tag)
            {
                // Net Size may not be valid if Searching for Network, 
                //  in Net Error, or FLAG_REMOTE_ERROR_REQ is active
                if(!FLAG_APP_SEARCH_MODE)
                {
                    if(!FLAG_STATUS_RMT_FAULT_ACTIVE && 
                       !FLAG_NETWORK_ERROR_MODE && !FLAG_REMOTE_ERROR_REQ)
                    {
                        
                        #ifdef CONFIGURE_WIFI
                            // No network size for WiFi
                            APP_Rf_Network_Size = 0;
                        #else
                            // Only update network size if not in a Network Error 
                            // condition. Network Error may cause # of devices to 
                            // change if coordinator drops a missing device.
                            APP_Rf_Network_Size = RxPacket->data;
                       #endif
                    }
                }
                
                // See if we want to blink out current network size
                if(APP_Rf_Network_Size != 0)
                {
                    if(FLAG_NET_SIZE_DISPLAY_OFF)
                    {
                        // Don't Blink out Network Size this time                        
                        FLAG_NET_SIZE_DISPLAY_OFF = 0;
                        
                        #ifdef CONFIGURE_DEBUG_NET_DEVICES_TEST
                            // Force Size Blinking when test is configured
                            FLAG_APP_DISPLAY_NETWORK_SIZE = 1;
                        #endif
                    }
                    else
                    {
                        // LED Module will monitor This Flag and Respond
                        //  by blinking out network size
                        FLAG_APP_DISPLAY_NETWORK_SIZE = 1;
                    }
                }

                // Acknowledgement Received OK
                return APP_PROCESS_RESPONSE_SUCCESS;
            }
            else
            {
                return APP_PROCESS_RESPONSE_ERROR;
            }

//****************************************************************************** 
//******************************************************************************         

            


#ifdef AMP_VERSION_REQ_SUPPORTED
            case AMP_VERSION_REQ:
                
                if(AMP_VERSION_REQ == RxPacket->tag)
                {
                    APP_RF_Module_FW_Version = RxPacket->data;

                    // Acknowledge Received OK
                    return APP_PROCESS_RESPONSE_SUCCESS;
                }
                else
                {
                    return APP_PROCESS_RESPONSE_ERROR;
                }
        #endif


        #ifdef AMP_LOCATE_SUPPORTED
            case AMP_LOCATE:
                if(AMP_LOCATE == RxPacket->tag)
                {
                    if(RxPacket->data == SentPacket->data + SentPacket->tag)
                    {
                        // Acknowledge Received OK
                        return APP_PROCESS_RESPONSE_SUCCESS;
                    }
                    else
                    {
                        return APP_PROCESS_RESPONSE_ERROR;
                    }
                }
                else
                {
                    return APP_PROCESS_RESPONSE_ERROR;
                }
        #endif


	case AMP_PTT:
            if(AMP_PTT == RxPacket->tag)
            {
                if(RxPacket->data == SentPacket->data + SentPacket->tag)
                {
                    return APP_PROCESS_RESPONSE_SUCCESS;
                }
                else
                {
                    return APP_PROCESS_RESPONSE_ERROR;
                }
            }
            else
            {
                return APP_PROCESS_RESPONSE_ERROR;
            }

	default:
            return APP_PROCESS_RESPONSE_ERROR;
    }

}



/*
+------------------------------------------------------------------------------
| Function:      APP_Process_Request
+------------------------------------------------------------------------------
| Purpose:       APP_Process_Request is used to process an unsolicited
|                message. Based upon message received, data is processed
|                and response packet is built and returned.
|
+------------------------------------------------------------------------------
| Parameters:    Received packet of type tag_CCI_Packet
+------------------------------------------------------------------------------
| Return Value:  Returns response packet of type tag_CCI_Packet
|
+------------------------------------------------------------------------------
*/

union tag_CCI_Packet APP_Process_Request(union tag_CCI_Packet *pRxPacket)
{
    union tag_CCI_Packet TxPacket;
    TxPacket.word = 0;

    switch (pRxPacket->tag)
    {

        #ifdef AMP_CAPABILITY1_SUPPORTED
            case AMP_CAPABILITY1:
                TxPacket.tag = AMP_CAPABILITY1;
                // For this message the data byte is the sum of the received
                // bytes
                TxPacket.data = APP_MY_CAPABILITY1;
            break;
        #endif

        #ifdef AMP_CAPABILITY2_SUPPORTED
            case AMP_CAPABILITY2:
                TxPacket.tag = AMP_CAPABILITY2;
                // For this message the data byte is the sum of the received
                // bytes
                TxPacket.data = APP_MY_CAPABILITY2;
            break;

        #endif

        case AMP_RF_TEST_STATUS:
            
            TxPacket.tag = AMP_RF_TEST_STATUS;
            // For this message the data byte is the sum of the received
            // bytes
            TxPacket.data = pRxPacket->data + pRxPacket->tag;
            
        break;

        #ifdef AMP_STATUS_SUPPORTED
            case AMP_STATUS:
                /* 
                 *  Alarm Status Bits that are directed to the Alarm and
                 *  sent unsolicited as listed below:
                 * 
                 * FLAG_STATUS_RMT_LB_ACTIVE        0  
                 * FLAG_STATUS_RMT_SMK_RCVD         3
                 * 
                 * FLAG_STATUS_RMT_CO_RCVD          4
                 * FLAG_STATUS_RMT_HW_INT_ACTIVE    5
                 * FLAG_STATUS_RMT_FAULT_ACTIVE     6
                 * FLAG_STATUS_RMT_PTT_RCVD         7
                 */

                /*
                 * If this is a HW INT Message
                 * 
                 * Set special HW INT Flags and maintain previous Remote Status
                 * This is done because we expect to see both type of messages
                 * and do not want to overwrite previous initiating alarm 
                 * status that may not be processed yet
                 */
                
                local_status = pRxPacket->data;
                
                // First perform a sanity check on received Status Data
                if(MASK_STATUS_INVALID_MSG_RECVD & local_status)
                {
                    // Should not receive a message with either Alarm output 
                    // bit active. If invalid, Tx packet remains cleared which
                    // ignores Status message and no Acknowledge is sent
                    
                    break;
                }
                
                if(MASK_CO_SMK_HW_MSG_RECVD == 
                        (MASK_CO_SMK_HW_MSG_RECVD & local_status))
                {
                    /* 
                     * We have received a HW, Smoke AND CO bit combination
                     * Smoke takes priority, so select HW Smoke
                     * This if() statement could be removed but was kept 
                     * in case we wanted to treat this status bit combination 
                     * differently.
                     */
                    
                    FLAG_STATUS_RMT_SMK_HW_RCVD = 1;
                    
                    // Clear HW INT CO and and Smoke Status Bits
                    local_status &= MASK_HW_INT_SMOKE_CO_CLR;
                    
                    // Merge in Previous Smoke Alarm Status bit if still set
                    local_status |= 
                            (APP_Remote_ALM_Status.ALL & MASK_SMOKE_STATUS_08);
                            
                    APP_Remote_ALM_Status.ALL = local_status;
                }
                else if(MASK_SMK_HW_MSG_RECVD == 
                        (MASK_SMK_HW_MSG_RECVD & local_status))
                {
                    FLAG_STATUS_RMT_SMK_HW_RCVD = 1;
                    
                    // Clear HW INT and Smoke Status Bits
                    local_status &= MASK_HW_INT_SMOKE_CLR;
                    
                    // Merge in Previous Smoke Alarm Status bit if still set
                    local_status |= 
                            (APP_Remote_ALM_Status.ALL & MASK_SMOKE_STATUS_08);
                            
                    APP_Remote_ALM_Status.ALL = local_status;
                }
                else if(MASK_CO_HW_MSG_RECVD == 
                           (MASK_CO_HW_MSG_RECVD & local_status))
                {
                    FLAG_STATUS_RMT_CO_HW_RCVD = 1;
                    
                    // Clear HW INT and CO Bit Status Bits
                    local_status &= MASK_HW_INT_CO_CLR;
                    
                    // Merge in Previous CO Alarm Status bit if still set
                    local_status |= 
                            (APP_Remote_ALM_Status.ALL & MASK_CO_STATUS_10);
                            
                    APP_Remote_ALM_Status.ALL = local_status;
                    
                }
                else
                {
                    // Not HW INT message, Save the received status and process 
                    // as needed in APP_Monitor
                    APP_Remote_ALM_Status.ALL = pRxPacket->data;
                }

                //Have we received a Remote PTT message?
                if(FLAG_STATUS_RMT_PTT_RCVD)
                {
                    // Force immediate PTT response and clear the Remote Flag
                    FLAG_STATUS_RMT_PTT_RCVD = 0;

                    // But, Ignore Remote PTT if already in Alarm
                    //  or Remote Alarms are disabled or Reset Stabilization 
                    //  is active
                    if(!FLAG_SMOKE_ALARM_ACTIVE && !FLAG_CO_ALARM_ACTIVE &&
                         FLAG_ALM_REMOTE_ENABLE )
                    {
                        // Start the Remote PTT sequence
                        FLAG_REMOTE_PTT_ACTIVE = 1;
                                
                        // Begin PTT Next Task Tic Time
                        MAIN_Reset_Task(TASKID_PTT_TASK);

                    }
                }
                
                // Remote Fault active?
                if(FLAG_STATUS_RMT_FAULT_ACTIVE)
                {
                    // Enable Network Error processing for this status msg
                    // Only set if a New Remote Fault message is received
                    FLAG_RMT_FAULT_NEW = 1;
                    
                    // Init max num of attempts for error request
                    app_net_err_attempts = APP_MAX_ERR_CODE_REQS;
                }
                
                // Allow Plenty of Time for Status message processing
                if(FLAG_WAKE_FOR_CCI)
                {
                    FLAG_WAKE_FOR_CCI = 0;
                    FLAG_ACTIVE_TIMER = 1;
                    MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_200_MS;
                    MAIN_Reset_Task(TASKID_APP_STAT_MONITOR);
                }
                
                // A status message has been received.
                //  Prepare Acknowledge Response Packet
                TxPacket.tag = AMP_STATUS;
                // For this message the data byte is the sum of the received
                // bytes
                TxPacket.data = pRxPacket->data + pRxPacket->tag;

            break;
        #endif

        #ifdef AMP_STATUS1_SUPPORTED
            case AMP_STATUS1:

                
                // First perform a sanity check on received Status Data
                if(MASK_STATUS1_INVALID_MSG_RECVD & pRxPacket->data)
                {
                    // Some bits should never be received, ignore
                    //  message if they are
                    
                    break;
                }
                
                
                // Save the status.
                APP_ALM_Status_1.ALL = pRxPacket->data;
                
                // Allow Plenty of Time for Status message processing
                if(FLAG_WAKE_FOR_CCI)
                {
                    FLAG_WAKE_FOR_CCI = 0;
                    FLAG_ACTIVE_TIMER = 1;
                    MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_200_MS;
                    MAIN_Reset_Task(TASKID_APP_STAT_MONITOR);
                }
                

                // A status request message has been received.
                TxPacket.tag = AMP_STATUS1;
                // For this message the data byte is the sum of the received
                // bytes
                TxPacket.data = pRxPacket->data + pRxPacket->tag;

            break;
        #endif

        #ifdef AMP_STATUS2_SUPPORTED
            case AMP_STATUS2:

                // First perform a sanity check on received Status Data
                if(MASK_STATUS2_INVALID_MSG_RECVD & pRxPacket->data)
                {
                    // Some bits should never be received, ignore
                    //  message if they are
                    
                    break;
                }
                // Save the status.
                APP_ALM_Status_2.ALL = pRxPacket->data;

                // A status request message has been received.
                TxPacket.tag = AMP_STATUS2;
                // For this message the data byte is the sum of the received
                // bytes
                TxPacket.data = pRxPacket->data + pRxPacket->tag;
                

            break;
        #endif

        #ifdef AMP_STATUS3_SUPPORTED
            case AMP_STATUS3:

                // First perform a sanity check on received Status Data
                if(MASK_STATUS3_INVALID_MSG_RECVD & pRxPacket->data)
                {
                    // Some bits should never be received, ignore
                    //  message if they are
                    
                    break;
                }
                // Save the status.
                APP_ALM_Status_3.ALL = pRxPacket->data;
                
                // Allow Plenty of Time for Status message processing
                if(FLAG_WAKE_FOR_CCI)
                {
                    FLAG_WAKE_FOR_CCI = 0;
                    FLAG_ACTIVE_TIMER = 1;
                    MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_200_MS;
                    MAIN_Reset_Task(TASKID_APP_STAT_MONITOR);
                }

                // A status request message has been received.
                TxPacket.tag = AMP_STATUS3;
                // For this message the data byte is the sum of the received
                // bytes
                TxPacket.data = pRxPacket->data + pRxPacket->tag;

            break;
        #endif

        #ifdef AMP_PING_SUPPORTED
            case AMP_PING:
                TxPacket.tag = AMP_PING_ACK;
                // For this message the data byte is the sum of the received
                // bytes
                TxPacket.data = pRxPacket->data + pRxPacket->tag;
            break;
        #endif

        #ifdef AMP_COMM_STATUS_SUPPORTED
            case AMP_COMM_STATUS:

                TxPacket.tag = AMP_COMM_STATUS_ACK;
                // For this message the data byte is the sum of the received
                // bytes
                TxPacket.data = pRxPacket->data + pRxPacket->tag;
                
                #ifdef CONFIGURE_UNDEFINED
                    // TODO: Undefine Later  BC: Test Only 
                    // To create an ACK Error
                    TxPacket.data = 0x00;
                    TxPacket.tag = 0x01;
                #endif
                
                // Process the COMM status bits here. (save in APP_COMM_Status)
                app_process_comm_status(pRxPacket->data);
                
            break;
        #endif

        #ifdef AMP_ERROR_CODE_SUPPORTED
            case AMP_ERROR_CODE:
                // The error code was requested.  Send it.
                TxPacket.tag = AMP_ERROR_CODE;

                // For this message the data byte is the current error code.
                TxPacket.data = FLT_Fatal_Code;

            break;
        #endif

        #ifdef AMP_VERSION_REQ_SUPPORTED
            case AMP_VERSION_REQ:
                // The firmware version was requested.  Send it.
                TxPacket.tag = AMP_VERSION_REQ;

                // For this message the data byte consists of the major 
                //     revision in the upper nibble and the minor revision
                //     in the lower nibble.

                // Get Revision then shift and mask as applicable.
                TxPacket.data = (REVISION_MAJOR) << 4;
                TxPacket.data |= (REVISION_MINOR);

            break;
        #endif

        #ifdef AMP_MODEL_NUMBER_SUPPORTED
            case AMP_MODEL_NUMBER:
                // The Model Number Data was requested.  Send it.
                TxPacket.tag = AMP_MODEL_NUMBER;

                // Get Model data value
                TxPacket.data = APP_MODEL_DATA;
            break;
        #endif


        #ifdef AMP_STATUS_REQUEST_SUPPORTED
        //
        // Currently the RF module never requests AMP Alarm Status
        // We now also request Alarm Status at PING transmissions    
        //
            case AMP_STATUS_REQUEST:
                // The Current Alarm Status was requested.  Send it.
                TxPacket.tag = AMP_STATUS_REQUEST;

                // Get Status
                TxPacket.data = APP_Current_ALM_Status.ALL;

            break;
         #endif

        #ifdef AMP_LIFE_SUPPORTED
            case AMP_LIFE:
            
                // The Current Life was requested.  Send it.
                TxPacket.tag = AMP_LIFE;
                
                uint weeks = 0;

                // Calculate number of days left before expiration
                uint day = LIFE_Get_Current_Day();
                
                #ifdef CONFIGURE_UNDEFINED
                    SER_Send_String("\rCurrent Day - ");
                    SER_Send_Int(' ',day,10);
                    SER_Send_String("\r");
                #endif
                
                // at least  1 week left?
                if(day <= (LIFE_EXPIRATION_DAY - 7))
                {
                    day = (LIFE_EXPIRATION_DAY - day);
                    // Weeks left
                    weeks = (day/7);
                }
                else
                {
                    if(day <= (LIFE_EXPIRATION_DAY))
                    {
                        day = (LIFE_EXPIRATION_DAY - day);
                    }
                    else
                    {
                        day = 0;
                    }
                    
                    weeks = 0;
                }
                
                #ifdef CONFIGURE_UNDEFINED
                    SER_Send_String("\rDays Left - ");
                    SER_Send_Int(' ',day,10);
                    SER_Send_String("\r");

                    SER_Send_String("\rWeeks Left - ");
                    SER_Send_Int(' ',weeks,10);
                    SER_Send_String("\r");
                #endif

                // Place weeks left in Packet
                TxPacket.tag = AMP_LIFE | (0x03 & ((uchar)(weeks>>8)));
                TxPacket.data = (uchar)weeks;
                
            break;
        
        #endif

        #ifdef AMP_REQ_BAT_VOLT_SUPPORTED
            case AMP_REQ_BAT_VOLT:
                // Place Battery Status in Packet Data
                TxPacket.tag = AMP_REQ_BAT_VOLT;
                TxPacket.data = BAT_Get_Status();
            break;
        #endif

        #ifdef AMP_C0_PPM_SUPPORTED
            case AMP_C0_PPM:
                // Place CO PPM in Packet Data  and 2 LS bits of MTS
                TxPacket.tag = (AMP_C0_PPM | (0x03 & ((uchar)(CO_wPPM >>8))) );
                TxPacket.data = (uchar)CO_wPPM;
            break;
        #endif

        #ifdef AMP_SMOKE_SUPPORTED

            #ifdef CONFIGURE_SEND_RAW_DATA

                case AMP_SMOKE:
                {
                    TxPacket.tag = AMP_SMOKE;
                    TxPacket.data =  PHOTO_Reading;
                }
                break;
            
            #else
                case AMP_SMOKE:
                {
                    uchar percent;
                    uchar change;

                    // Place % of Smoke level measured in Packet.
                    // Data = % of CAV to Alarm TH Range
                    //  if measured >= Corrected Alarm TH == 100%
                    //  if measured <= Average CAV == 0%
                    TxPacket.tag = AMP_SMOKE;

                    // define the Range (CAV to Alarm TH)
                    // (Corrected Alarm Threshold - PhotoCAV Average)
                    //  Takes into consideration Drift Comp. if applicable
                    uchar range = (SYS_RamData.Smk_Corrected_Alm_TH -
                                   PHOTO_Cav_Average.byte.msb);

                    // Define % of Current Photo measurement change.
                    if(PHOTO_Reading > PHOTO_Cav_Average.byte.msb)
                    {
                        if(PHOTO_Reading < SYS_RamData.Smk_Corrected_Alm_TH )
                        {
                            // Smoke at some point between CAV and Alarm
                            change = (PHOTO_Reading - 
                                    PHOTO_Cav_Average.byte.msb);
                            percent = (uchar)(((unsigned long)change * 100) /
                                                               range);
                        }
                        else
                        {
                            // At or Above Alarm Threshold
                            percent = 100;

                            // No SMoke Hush limit for Australia
                            #ifndef CONFIGURE_AUSTRALIA
                                if(PHOTO_Reading >= (PHOTO_Alarm_Thold + 
                                                        ALG_SMK_HUSH_FIXED) )
                                {
                                    // Set MSB if above Hush Threshold
                                    percent = (percent | 0x80);
                                }
                            #endif
                        }
                    }
                    else
                    {
                        // If Photo measured VALUE <= Compensation CAV 
                        //   PhotoAverage, then set to 0% (No Smoke Detected)
                        percent = 0;
                    }

                    TxPacket.data = percent;

                }
                break;
            #endif
        #endif

        #ifdef AMP_SMOKE_READING_SUPPORTED

            case AMP_SMOKE_COMP:
            {
                uchar range;
                uchar change;
                uchar percent;
                
                

                // Place % of Smoke Drift compensation in packet data.
                // This will be % Average CAV has drifted 
                //  from Calibrated CAV to it's Compensation Limit

                TxPacket.tag = AMP_SMOKE_COMP;


                if(SYS_RamData.Smk_Cav_At_Cal <= PHOTO_Cav_Average.byte.msb)
                {
                    // Set percentage range for Dust Drift Comp Limit
                    range = (PHOTO_Comp_Max_Cav - 
                                SYS_RamData.Smk_Cav_At_Cal);
                    
                    // Average CAV has increased (dust/contamination)
                    change = (PHOTO_Cav_Average.byte.msb -
                                SYS_RamData.Smk_Cav_At_Cal);
                    
                    #ifdef CONFIGURE_UNDEFINED
                        SER_Send_Int('R',range,10);
                        SER_Send_Int('C',change,10);
                    #endif

                    if(change >= range)
                    {
                        // Range = Max allowed Change
                        percent = COMP_PERCENT_100;   
                    }
                    else
                    {
                        percent = (uchar)( ((unsigned long)change * 
                                            COMP_PERCENT_100) / range);
                    }
                    
                    if(percent > COMP_PERCENT_100)
                    {
                        percent = COMP_PERCENT_100;
                    }
                    TxPacket.data = percent;
                    
                    #ifdef CONFIGURE_UNDEFINED
                        SER_Send_Prompt();
                        SER_Send_String("percent: ");
                        SER_Send_Int(' ',TxPacket.data,10);
                        SER_Send_Prompt();
                    #endif

                }
                else
                {
                    // Set percentage range for IRed degradation Comp Limit
                    range = (SYS_RamData.Smk_Cav_At_Cal - 
                                PHOTO_Comp_Min_Cav);
                    
                    // Average CAV has dropped (IRed degradation)
                    change = (SYS_RamData.Smk_Cav_At_Cal -
                               PHOTO_Cav_Average.byte.msb);

                    if(0 == change)
                    {
                        // No Drift
                        TxPacket.data = 0;
                    }
                    else
                    {
                        #ifdef CONFIGURE_UNDEFINED
                            SER_Send_Int('R',range,10);
                            SER_Send_Int('C',change,10);
                        #endif

                        if(change >= range)
                        {
                            // Make sure percentage calc. cannot exceed 255
                            //  by rolling past 8 bit boundary.
                            percent = COMP_PERCENT_100;   
                        }
                        else
                        {
                            percent = (((unsigned long)change * 
                                            COMP_PERCENT_100) / range);
                        }
                        
                        #ifdef CONFIGURE_UNDEFINED
                            SER_Send_Int('P',percent,10);
                        #endif
                        
                        
                        if(percent > COMP_PERCENT_100)
                        {
                            percent = COMP_PERCENT_100;
                        }

                        TxPacket.data = ((uchar)0 - percent);

                    }
                    
                    #ifdef CONFIGURE_UNDEFINED
                        SER_Send_Prompt();
                        SER_Send_String("percent: -");
                        SER_Send_Int(' ',(256 - TxPacket.data),10);
                        SER_Send_Prompt();
                    #endif
                    
                }
                
                #ifdef CONFIGURE_UNDEFINED
                    SER_Send_String("CAV Max Limit =");
                    SER_Send_Int(' ',PHOTO_Comp_Max_Cav,10);
                    SER_Send_Prompt();
                    SER_Send_String("CAV Min Limit =");
                    SER_Send_Int(' ',PHOTO_Comp_Min_Cav,10);
                    SER_Send_Prompt();
                #endif
                
            }
            break;
            
        #endif

        #ifdef AMP_TEMPERATURE_SUPPORTED
            case AMP_TEMPERATURE:
                TxPacket.tag = AMP_TEMPERATURE;
                TxPacket.data = A2D_Get_Degrees_F();
            break;
        #endif

        #ifdef AMP_ID_SUPPORTED
            case AMP_IDENTIFY:
            {
                //Set Identify Mode
                FLAG_ALARM_IDENTIFY = 1;

                TxPacket.tag = AMP_IDENTIFY;
                TxPacket.data = pRxPacket->data + pRxPacket->tag;
            }            
            break;
        #endif
            
        #ifdef AMP_LOCATE_SUPPORTED
            case AMP_LOCATE:
                if(AMP_LOCATE_ALARM == pRxPacket->data)
                {
                    if(!FLAG_ALARM_LOCATE_MODE)
                    {
                        // Stop all slave alarm transmissions
                        FLAG_ALARM_LOCATE_MODE = 1;

                        // Initialize the Locate Mode Timer
                        ALM_Init_Locate_Timer();
                    }
                    else
                    {
                        // Currently in Locate Mode 
                        //  Refresh the locate timer
                        #ifdef CONFIGURE_UNDEFINED
                            // Changed to do nothing if already in Locate
                            ALM_Init_Locate_Timer();
                        #endif
                    }
                    
                }
                else if(AMP_LOCATE_CLEAR == pRxPacket->data)
                {
                    
                    /*
                     * If received Alarm Locate CLEAR message, terminate
                     *  Locate Mode that may be active
                     * 
                     * This enables host to transmit remote/slave alarms
                     *  without waiting for the 2 minute locate to end.
                     */
                    FLAG_ALARM_LOCATE_MODE = 0;
                    
                }

                TxPacket.tag = AMP_LOCATE;
                TxPacket.data = pRxPacket->data + pRxPacket->tag;
            break;
        #endif

        #ifdef AMP_FUNCTION_SUPPORTED    
            case AMP_FUNCTION:
            {
                #ifdef CONFIGURE_ESCAPE_LIGHT
                    if(APP_FUNCTION_ESC_LIGHT_ON == pRxPacket->data)
                    {

                        // Turn on ESC Light
                        // Esc Light module controls maximum on timeout timer
                        FLAG_REMOTE_ESC_LIGHT_ON = 1;
                        FLAG_ESC_LIGHT_ON = 1;

                    }
                    else
                    {
                        // ESC Light OFF, allow Sound Module to turn it off
                        FLAG_REMOTE_ESC_LIGHT_ON = 0;
                    }
                #endif

                TxPacket.tag = AMP_FUNCTION;
                TxPacket.data = pRxPacket->data + pRxPacket->tag;
            }
            break;
        #endif

        case AMP_NOT_SUPPORTED:
            /*
             * If a "not supported" message occurs on an unsolicited basis, it
             * means that something got out of sync.  How could the other CCI
             * interface send an unsupported message without receiving one from
             * this interface?
             * So, return a TxPacket.tag of zero (initialized above)
             */
        break;

            
        // By Default Messages are Unsupported
        default:
            TxPacket.tag = AMP_NOT_SUPPORTED;
            // For this message the data byte is the tag received
            TxPacket.data = pRxPacket->tag;
        break;
    }

    // If this was serviced from Low Power Mode, them Shorten Active Timer
    //  to return to LP Mode sooner.
    if(FLAG_WAKE_FOR_CCI)
    {
        FLAG_WAKE_FOR_CCI = 0;
        FLAG_ACTIVE_TIMER = 1;
        MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_30_MS;
        MAIN_Reset_Task(TASKID_APP_STAT_MONITOR);
    }
    
    return TxPacket;
}

// Refresh the Current Alarm status byte

/*
+------------------------------------------------------------------------------
| Function:      app_update_current_status_byte
+------------------------------------------------------------------------------
| Purpose:       Refresh the Current Alarm status byte
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Return TRUE if change in status is detected
|                Return FALSE if no change in status
+------------------------------------------------------------------------------
*/

uchar app_update_current_status_byte(void)
{
    /*
     * Depending on Alarm condition, update the status byte.
     * Smoke Alarm Status has priority over CO
     * If Master or Slave Alarm conditions, set CCI Alarm Status
     *
     * However, if PTT is active then we have sent a PTT message, so its not
     * necessary to transmit the Alarm Status bits to the RF Module
     * during PTT execution
     */
    if( !FLAG_PTT_ACTIVE && !FLAG_REMOTE_PTT_ACTIVE )
    {
        // Refresh the Current Alarm status (default to no alarms))
        APP_Current_ALM_Status.ALL = 0;
        
        if(FLAG_MASTER_SMOKE_ACTIVE)
        {
            /*
             * In this case Smoke is detected and in Master Alarm State
             * This takes CCI Alarm status priority over slave alarm
             * So we should send CCI msg 6C,02 FLAG_STATUS_ALM_SMK_ACTIVE
             * If Smoke Hush state is entered, then Master Smoke Active
             *  is cleared and next alarm condition is tested
             */
            
                // Halt Smoke Alarm Status while in Smoke Hush
                //  even Locate/Hush (1st 2 minutes of Hush)
                if(!FLAG_SMOKE_HUSH_ACTIVE)
                {
                    FLAG_STATUS_ALM_SMK_ACTIVE = 1;

                    // TODO: Undefine Later  BC: Test Only
                    #ifdef CONFIGURE_UNDEFINED
                        if(FLAG_SMK_ALMPRIO_TRANS)
                        {
                            SER_Send_String("SMK_Transition_OFF_SB1");
                            SER_Send_Prompt();
                        }
                    #endif

                    // Reset alarm Status Clear to be sent again
                    FLAG_SMK_ALMPRIO_TRANS = 0;
                }

        }
        #ifdef CONFIGURE_INTERCONNECT
            /*
             * This host alarm is not detecting smoke, so check for slave 
             *  smoke alarm and confirm if this slave alarm should pass it to
             *  wireless network
             */
        
            else if( FLAG_SLAVE_SMOKE_ACTIVE && 
                    !FLAG_GATEWAY_SLV_TO_REMOTE_OFF )
            {
               /*
                 * Do not act as a Slave Alarm gateway to the Network if 
                 *  already Receiving Wireless Smoke Alarms 
                 * Also, If another Remote Alarm is the Remote to Slave Gateway
                 *  Then no need to transmit Wireless alarm Status
                 * In Slave Alarm State and NOT receiving Wireless Smoke Alarms
                 * Allow Slave to Wireless Alarm Status Delay to complete
                 */
                
                // Also, Monitor Slave to Remote Gateway Delay Timer
                APP_Slave_To_Remote_Alarm_Timer();

                if(!FLAG_SLAVE_ALARM_TIMER_ON)
                {
                    // Timeout Out Send Remote Alarm
                    FLAG_STATUS_ALM_SMK_ACTIVE = 1;
                    
                    // TODO: Undefine Later  BC: Test Only
                    #ifdef CONFIGURE_UNDEFINED
                        if(FLAG_SMK_ALMPRIO_TRANS)
                        {
                            SER_Send_String("SMK_Transition_OFF_SB2");
                            SER_Send_Prompt();
                        }
                    #endif
                    
                    // Reset alarm Status Clear to be sent again
                    FLAG_SMK_ALMPRIO_TRANS = 0;
                    
                    /*
                     * In this case Smoke is not detected and in Slave Alarm 
                     * So we should send CCI msg 6C,22  Alarm Status together 
                     *  with FLAG_STATUS_ALM_HW_INT_ACTIVE
                     */

                    // Indicate alarm condition is HW Interconnect 
                    //  initiated 
                    FLAG_STATUS_ALM_HW_INT_ACTIVE = 1;

                }
            }
        #endif

        else if(FLAG_CO_ALARM_CONDITION)
        {
            /*
             * In this case CO is detected and in Master Alarm State
             * This takes CCI Alarm status priority over slave alarm
             * So we should send CCI msg 6C,04 FLAG_STATUS_ALM_CO_ACTIVE
             */
            
            // While in CO Alarm Battery Conserve do not
            //  transmit CO Alarm Status bit this effectively clears
            // CO Alarm during the 60 second CO alarm conserve time 
            if(!FLAG_WL_ALM_BAT_CONSERVE) 
            {
               FLAG_STATUS_ALM_CO_ACTIVE = 1;
            }
             
        }
        
        #ifdef CONFIGURE_INTERCONNECT

            // See if this slave alarm should echo to wireless network
            else if( FLAG_SLAVE_CO_ACTIVE && 
                     !FLAG_GATEWAY_SLV_TO_REMOTE_OFF )
            {
                /*
                 * Do not act as a Slave Alarm gateway to the Network if 
                 *  already Receiving Wireless CO Alarms 
                 * Also, If another Remote Alarm is the Remote to Slave Gateway
                 *  Then no need to transmit Wireless alarm Status
                 * In Slave Alarm State and NOT receiving Wireless CO Alarms
                 * Allow Slave to Wireless Alarm Status Delay to complete
                 */
                
                // Also, Monitor Slave to Remote gateway Delay
                APP_Slave_To_Remote_Alarm_Timer();

                if(!FLAG_SLAVE_ALARM_TIMER_ON)
                {
                    /*
                     * Timeout Out Send Remote Alarm 
                     */
                        FLAG_STATUS_ALM_CO_ACTIVE = 1;
                       
                        /*
                         * In this case CO is not detected and in Slave Alarm 
                         * So we should send CCI msg 6C,24  Alarm Status  
                         *  together with FLAG_STATUS_ALM_HW_INT_ACTIVE
                         */
                        // Indicate alarm condition is HW Interconnect 
                        //  initiated 
                        FLAG_STATUS_ALM_HW_INT_ACTIVE = 1;
                }
            }
        #endif


        // Fault Status
        // If in Alarm Fatal Fault, set Alarm Fault status bit
        if(FLAG_FAULT_FATAL && !FLAG_REMOTE_ERROR_REQ)
        {
            // Fault Active is a bi-directional status bit, but output and input
            //  status is kept in separate Flag Registers to avoid conflicts
            FLAG_STATUS_ALM_FAULT_ACTIVE = 1;
        }

        // Low Battery Status
        if(FLAG_LOW_BATTERY && !FLAG_CO_ALARM_ACTIVE &&
                                !FLAG_SMOKE_ALARM_ACTIVE)
        {
            // LB Active is a bi-directional status bit, output and input
            //  status is isolated in separate Flag Registers to avoid 
            //  conflicts
            FLAG_STATUS_ALM_LB_ACTIVE = 1;
        }
    
    }
    
    
    // If a change in status has just occurred, return True 
    //  otherwise return False 
    if(APP_Current_ALM_Status.ALL != APP_Previous_ALM_Status.ALL)
    {
        
        // If No Alarm Status, but Smoke over CO Transition is in progress
        //  do not send Alarm Status Clear
        if(APP_ALARM_STATUS_CLEAR == APP_Current_ALM_Status.ALL)
        {
            if(FLAG_SMK_ALMPRIO_TRANS)
            {
                // TODO: Undefine Later  BC: Test Only
                #ifdef CONFIGURE_UNDEFINED
                    SER_Send_String("FLAG_SMK_ALMPRIO_TRANS is Active ");
                    SER_Send_Prompt();
                #endif
                
                
                // No Alarm Status Clear Change during Transition
                APP_Current_ALM_Status.ALL = APP_Previous_ALM_Status.ALL;
                
                // No change in Status
                return FALSE;
            }
            
        }
        
        // A Status Change has occurred
        return TRUE;
    }
    else
    {
        // No change in Status
        return FALSE;
    }

}


/*
+------------------------------------------------------------------------------
| Function:      app_update_status
+------------------------------------------------------------------------------
| Purpose:       This function will send the current status to receiver.
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  return zero on successful transaction
|
+------------------------------------------------------------------------------
*/

uchar APP_Update_Status( void )
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_STATUS;
    pkt.data = APP_Current_ALM_Status.ALL;

    // Send the status via CCI.
    return AMP_Start_Transaction(pkt);

}


/*
+------------------------------------------------------------------------------
| Function:      app_update_status1
+------------------------------------------------------------------------------
| Purpose:       This function will send the current status1 to receiver.
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  return zero on successful transaction
|
+------------------------------------------------------------------------------
*/

uchar app_update_status1( uchar status1 )
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_STATUS1;
    
    // Mask Out any input bits, and send updated Status1
    pkt.data = (status1 & APP_STATUS1_OUT_MASK);

    // Send the status via CCI.
    return AMP_Start_Transaction(pkt);

}

#ifdef CONFIGURE_UNDEFINED
/*
+------------------------------------------------------------------------------
| Function:      app_update_status2
+------------------------------------------------------------------------------
| Purpose:       This function will send the current status2 to receiver.
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  return zero on successful transaction
|
+------------------------------------------------------------------------------
*/

uchar app_update_status2( uchar status2 )
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_STATUS2;
    pkt.data = status2;

    // Send the status via CCI.
    return AMP_Start_Transaction(pkt);

}
#endif

/*
+------------------------------------------------------------------------------
| Function:      app_request_error
+------------------------------------------------------------------------------
| Purpose:       This function will request error code from RF Module
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void app_request_error( void )
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_ERROR_CODE;
    pkt.data = 0;

    // Send the request via CCI.
    AMP_Start_Transaction(pkt);
}

/*
+------------------------------------------------------------------------------
| Function:      APP_PTT_Arm
+------------------------------------------------------------------------------
| Purpose:       PTT about to begin, notify RF module, send packet
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void APP_PTT_Arm( void )
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_PTT;
    pkt.data = AMP_PTT_ARM;

    // Send the request via CCI.
    AMP_Start_Transaction(pkt);
}

/*
+------------------------------------------------------------------------------
| Function:      APP_PTT_Local
+------------------------------------------------------------------------------
| Purpose:       PTT is in progress, notify RF module, send packet
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void APP_PTT_Local( void )
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_PTT;
    pkt.data = AMP_PTT_LOCAL;

    // Send the request via CCI.
    AMP_Start_Transaction(pkt);
}


#ifdef CONFIGURE_UNDEFINED
/*
+------------------------------------------------------------------------------
| Function:      APP_PTT_Clear
+------------------------------------------------------------------------------
| Purpose:       PTT is done, send Packet to indicate PTT over
|                (Currently not Used)
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void APP_PTT_Clear( void )
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_PTT;
    pkt.data = 0;

    // Send the request via CCI.
    AMP_Start_Transaction(pkt);
}
#endif


#ifdef CONFIGURE_UNDEFINED
/*
+------------------------------------------------------------------------------
| Function:      app_request_version
+------------------------------------------------------------------------------
| Purpose:       This will request current AMP FW Version from the RF module
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void app_request_amp_version( void )
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_REQUEST_AMP_VER;
    pkt.data = 0;

    // Send the request via CCI.
    AMP_Start_Transaction(pkt);
}
#endif

/*
+------------------------------------------------------------------------------
| Function:      APP_Get_Local_Id
+------------------------------------------------------------------------------
| Purpose:       Return current local ID#
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  local ID #  
|
+------------------------------------------------------------------------------
*/

uchar APP_Get_Local_Id( void )
{
    return(app_rf_module_id);
}

/*
+------------------------------------------------------------------------------
| Function:      app_request_local_id
+------------------------------------------------------------------------------
| Purpose:       This will request current local ID from the RF module
|                This will update Logical ID #
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void APP_Request_Local_Id( void )
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_GET_LOGICAL_ADDRESS;
    pkt.data = 0;

    // Send the request via CCI.
    AMP_Start_Transaction(pkt);
}


/*
+------------------------------------------------------------------------------
| Function:      APP_Request_Net_Size
+------------------------------------------------------------------------------
| Purpose:       This will request # of units in network from the RF module
|                Returned result will be saved in global APP_Rf_Network_Size
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void APP_Request_Net_Size( void )
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_NETWORK_SIZE;
    pkt.data = 0;

    // Send the request via CCI.
    AMP_Start_Transaction(pkt);
}

/*
+------------------------------------------------------------------------------
| Function:      APP_Request_RF_Version
+------------------------------------------------------------------------------
| Purpose:       This will request RF Module Firmware Version
|                Returned result will be saved in APP_RF_Module_FW_Version
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void APP_Request_RF_Version( void )
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_VERSION_REQ;
    pkt.data = 0;

    // Send the request via CCI.
    AMP_Start_Transaction(pkt);
}



/*
+------------------------------------------------------------------------------
| Function:      app_send_ping
+------------------------------------------------------------------------------
| Purpose:       This function will send a PING to the RF Module
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  return zero on successful transaction
|
+------------------------------------------------------------------------------
*/

uchar app_send_ping( void )
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_PING;
    pkt.data = 0x0;

    // Send the status via CCI.
    return AMP_Start_Transaction(pkt);
}

/*
+------------------------------------------------------------------------------
| Function:      APP_Send_Alarm_Locate
+------------------------------------------------------------------------------
| Purpose:       This function will send an Alarm Locate via CCI
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  return zero on successful transaction
|
+------------------------------------------------------------------------------
*/

uchar APP_Send_Alarm_Locate( void )
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_LOCATE;
    pkt.data = AMP_LOCATE_ALARM;

    // Send the status via CCI.
    return AMP_Start_Transaction(pkt);
}

/*
+------------------------------------------------------------------------------
| Function:      APP_Send_Clr_Alarm_Locate
+------------------------------------------------------------------------------
| Purpose:       This function will send a clear Alarm Locate via CCI
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  return zero on successful transaction
|
+------------------------------------------------------------------------------
*/

uchar APP_Send_Clr_Alarm_Locate( void )
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_LOCATE;
    pkt.data = AMP_LOCATE_CLEAR;

    // Send the status via CCI.
    return AMP_Start_Transaction(pkt);
}


/*
+------------------------------------------------------------------------------
| Function:      app_request_status1
+------------------------------------------------------------------------------
| Purpose:       This function will request Status 1 information from the
|                RF Module
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void APP_Request_Status1( void )
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_STATUS1_REQ;
    pkt.data = 0;

    // Send the request via CCI.
    AMP_Start_Transaction(pkt);
}

/*
+------------------------------------------------------------------------------
| Function:      app_request_comm_status
+------------------------------------------------------------------------------
| Purpose:       This function will request COMM Status information from the
|                RF Module
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void APP_Request_Comm_Status( void )
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_COMM_STATUS_REQ;
    pkt.data = 0;

    // Send the request via CCI.
    AMP_Start_Transaction(pkt);
}

/*
+------------------------------------------------------------------------------
| Function:      app_send_comm_status
+------------------------------------------------------------------------------
| Purpose:       This function will send COMM Status information to the
|                RF Module
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  return zero on successful transaction
|
+------------------------------------------------------------------------------
*/

uchar app_send_comm_status( uchar status )
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_COMM_STATUS;
    pkt.data = status;

    // Send the request via CCI.
    return AMP_Start_Transaction(pkt);
}


/*
+------------------------------------------------------------------------------
| Function:      APP_Request_Status
+------------------------------------------------------------------------------
| Purpose:       This function will request Status from
|                RF Module
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void APP_Request_Status(void)
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_STATUS_REQUEST;
    pkt.data = 0;

    AMP_Start_Transaction(pkt);
}

#ifdef CONFIGURE_UNDEFINED
/*
+------------------------------------------------------------------------------
| Function:      app_send_fault
+------------------------------------------------------------------------------
| Purpose:       This function will send fault information to the
|                RF Module
+------------------------------------------------------------------------------
| Parameters:    faultcode
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void app_send_fault(uchar faultcode)
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_ERROR_CODE;
    pkt.data = faultcode;

    AMP_Start_Transaction(pkt);
}
#endif

/*
+------------------------------------------------------------------------------
| Function:      APP_Reset_Fault
+------------------------------------------------------------------------------
| Purpose:       This function will send Fault Reset to the
|                RF Module
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void APP_Reset_Fault(void)
{
    union tag_CCI_Packet pkt;
    pkt.tag = AMP_STATUS2;
    pkt.data = AMP_STAT2_FAULT_RESET;

    AMP_Start_Transaction(pkt);
    
    // On Reset Fault clear Network error Mode/Flags
    FLAG_NETWORK_ERROR_MODE = 0;
    FLAG_NETWORK_ERROR_HUSH_ACTIVE = 0;
    
   // Clear Received Remote Fault Status
    FLAG_STATUS_RMT_FAULT_ACTIVE = 0;
    
}






#ifdef	CONFIGURE_INTERCONNECT

/*
+------------------------------------------------------------------------------
| Function:      app_wl_gateway_timer
+------------------------------------------------------------------------------
| Purpose:       Process Alarm gateway Timer which controls
|                 FLAG_WL_GATEWAY_ENABLE
|                Should be called every 100 ms after Remote Alarm is received
|                 until entering Remote Alarm Mode
|                It is designed to stagger alarms using Local ID# to allow
|                 only one alarm to take control of the Interconnect Line
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void app_wl_gateway_timer(void)
{
    if(!FLAG_WL_GATEWAY_ENABLE)
    {
        /*
         * *******************************************************************
         * While Gateway Timer is Active monitor HW Interconnect Line
         * to determine whether another unit is driving HW INT or has already 
         * placed this unit in Slave Alarm
         *  
         * Monitor INT Slave Alarm flags to see
         *  if another alarm has already taken control of HW INT signal
         *  or in the process of doing so.
         */
        if(FLAG_REMOTE_SMOKE_ACTIVE || FLAG_REMOTE_HW_SMOKE_ACTIVE)
        {
            /*
             * If During Remote Smoke gateway establishment time
             * Slave Smoke has become Active OR
             * Slave Smoke signal is detected on the Interconnect Line
             * then Terminate Gateway with it disabled since another alarm
             * is driving HW INT
             */ 
            if( FLAG_SLAVE_SMOKE_ACTIVE || FLAG_INTCON_SMOKE_RECEIVED || 
               (FLAG_INT_ACTIVITY_DETECTED && !FLAG_INT_CO_ACTIVITY_DETECTED) )
            {
                // Slave Smoke detected
                
                /*
                 * During Remote Smoke active
                 * Another Alarm has taken control of HW Interconnect 
                 * Line, Stop Gateway Timer since Slave Smoke Alarm 
                 * should be active
                 */
                FLAG_WL_GATEWAY_ENABLE = 1;  // Stops Gateway Timer

                // Disable RtoS gateway
                FLAG_GATEWAY_REMOTE_TO_SLAVE = 0;

                app_timer_wl_gateway = 0;


                // Clear any Remote COs since Smoke takes priority 
                FLAG_REMOTE_CO_ACTIVE = 0;
                FLAG_REMOTE_HW_CO_ACTIVE = 0;
                
                // TODO: Undefine Later  BC: Test Only
                #ifndef CONFIGURE_UNDEFINED
                    SER_Send_String("SMK RtoS Gateway OFF...");
                    SER_Send_Prompt();
                #endif
            }
            
        }
        else if(FLAG_REMOTE_CO_ACTIVE || FLAG_REMOTE_HW_CO_ACTIVE)
        {
            // This alarm is currently receiving a Slave CO Alarm Signal
            if(FLAG_SLAVE_CO_ACTIVE || FLAG_INT_CO_ACTIVITY_DETECTED ||
               FLAG_INTCON_CO_RECEIVED)
            {
                // Slave CO alarm detected
                
                /*
                 * During Remote CO active
                 * Another Alarm has taken control of HW Interconnect 
                 * Line Terminate Gateway Timer since Slave CO Alarm 
                 * should NOW be active
                 */

                FLAG_WL_GATEWAY_ENABLE = 1;  // Stops Gateway Timer

                // Set Flag to indicate that another unit has taken
                //  Gateway responsibily, this alarm will transition to
                //  Slave Alarm
                FLAG_GATEWAY_REMOTE_TO_SLAVE = 0;

                // Reset the timer for next Gateway
                app_timer_wl_gateway = 0;

                // TODO: Undefine Later  BC: Test Only
                #ifndef CONFIGURE_UNDEFINED
                    SER_Send_String("CO RtoS Gateway OFF...");
                    SER_Send_Prompt();
                #endif
                
            }
            
        }
        
        /*
         *******************************************************************        
         *
         * Now if gateway still not established, allow a unit to make
         * itself a Gateway to drive HW Interconnect
         * 
         * Alarm with lowest RF ID value should take control.
         */
        if(!FLAG_WL_GATEWAY_ENABLE)   
        {
            // Gateway Timer Still Active
            
            // Init the RFID Delay Timer
            if(APP_ZERO_ID == app_rf_module_id)
            {
                // Enter Alarm Immediately as Gateway if Coordinator
                //  and Stop Timer
                // When Remote Alarm begins, Interconnect Transmissions 
                //  will begin
                FLAG_WL_GATEWAY_ENABLE = 1;
                
                // Granted Permission to Drive HW Interconnect
                FLAG_GATEWAY_REMOTE_TO_SLAVE = 1;
                
                // TODO: Undefine Later  BC: Test Only
                #ifndef CONFIGURE_UNDEFINED
                    SER_Send_String("Coord RtoS Gateway Active...");
                    SER_Send_Prompt();
                #endif


            }
            else if(0 == app_timer_wl_gateway)
            {
                // TODO: Undefine Later  BC: Test Only
                #ifdef CONFIGURE_DIAG_TIMER
                    // Triggers start of Diag Timer for Serial Port Output
                    MAIN_Init_Diag_Timer();
                #endif
                
                // This must be an RFD device, start Gateway Timer
                // Initialize the Timer (1500 ms x RF_ID)
                app_timer_wl_gateway = (app_rf_module_id  * 
                                            APP_TIMER_1_5_SEC);

                // Clear Gateway Flag until Remote to Slave Gateway 
                //  established
                FLAG_GATEWAY_REMOTE_TO_SLAVE = 0;

                // TODO: Undefine Later  BC: Test Only
                #ifndef CONFIGURE_UNDEFINED
                    SER_Send_String("Start RFD RtoS Gateway Timer...");
                    SER_Send_Prompt();
                #endif

            }
            else if(0 == --app_timer_wl_gateway)
            {
                // RF ID Delay is complete
                // Allow Remote Alarm to proceed and Stop Timer
                // When Remote Alarm begins, Interconnect Transmissions 
                //  will begin
                FLAG_WL_GATEWAY_ENABLE = 1;
                
                // Granted Permission to Drive HW Interconnect
                FLAG_GATEWAY_REMOTE_TO_SLAVE = 1;
                
                // TODO: Undefine Later  BC: Test Only
                #ifndef CONFIGURE_UNDEFINED
                    SER_Send_String("RFD RtoS Gateway Active...");
                    SER_Send_Prompt();
                #endif

            }
        }
    }
}
#endif

#ifdef CONFIGURE_WIRELESS_CCI_DEBUG

/*
+------------------------------------------------------------------------------
| Function:      APP_CCI_QInit
+------------------------------------------------------------------------------
| Purpose:       Initialize the CCI message queue
|
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

    void APP_CCI_QInit (void)
    {
        for(uint i = 0; i < CQLENGTH; i++)
        {
            // Init Queue to Empty
            APP_CCI_Queue[i] = 0;
            app_cci_qin = 0;
            app_cci_qout = 0;
        }
    }

/*
+------------------------------------------------------------------------------
| Function:      APP_CCI_QPop
+------------------------------------------------------------------------------
| Purpose:       Pop a byte from the CCI message queue
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  return byte popped from the queue
+------------------------------------------------------------------------------
*/

    uchar APP_CCI_QPop(void)
    {
        
        uchar c = APP_CCI_Queue[app_cci_qout];

        if((CQLENGTH-1) == app_cci_qout)
        {
            // Wrap to Start of Queue
            app_cci_qout = 0;
        }
        else
        {
            // Next Queue location
            app_cci_qout++;
        }
        
        return c;
    }

/*
+------------------------------------------------------------------------------
| Function:      APP_CCI_QPush
+------------------------------------------------------------------------------
| Purpose:       Push byte into the CCI message queue
|
+------------------------------------------------------------------------------
| Parameters:    c = byte to push into queue
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

    void APP_CCI_QPush(uchar c)
    {
        APP_CCI_Queue[app_cci_qin] = c;
        if((CQLENGTH-1) == app_cci_qin)
        {
            // Wrap to Start of Queue
            app_cci_qin = 0;
        }
        else
        {
            // Next Queue location
            app_cci_qin++;
        }
    }

/*
+------------------------------------------------------------------------------
| Function:      APP_CCI_Exque
+------------------------------------------------------------------------------
| Purpose:       Return Status of CCI Message Queue
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  return # of chars in Queue, 0 = empty
+------------------------------------------------------------------------------
*/

    uint APP_CCI_Exque(void)
    {
        uint s;
        
        // While testing qin prevent CCI interrupts
        GLOBAL_INT_ENABLE = 0;

        if(app_cci_qin == app_cci_qout)
        {
            s = APP_CCI_QUEUE_EMPTY;
        }
        else if(app_cci_qin > app_cci_qout)
        {
            s = (app_cci_qin - app_cci_qout);
        }
        else
        {
            // QIN must have wrapped so add in CQLENGTH to calc. # of chars
            //   in Que
            s = ((app_cci_qin + CQLENGTH) - app_cci_qout);

        }
        
        GLOBAL_INT_ENABLE = 1;

        return s;

    }
#endif

#ifdef	CONFIGURE_INTERCONNECT    
    
/*
+------------------------------------------------------------------------------
| Function:      APP_Init_Slave_To_Remote_Alarm_Timer
+------------------------------------------------------------------------------
| Purpose:       Start Slave Alarm Timer
|   
|               When an Alarm Signal is detected at the HW Interconnect
|               a delay must be inserted before passing ths condition
|               in CCI Alarm Status to the RF Module. When 2 or more
|               Interconnected Alarms try to act as gateways (Slave to Remote)
|               Alarms were trying to enter Real Time mode at the same time
|               which cause lockup issues at Alarm Exit. 
|
+------------------------------------------------------------------------------
| Parameters:    Timer Timeout Time in seconds (0-255)
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
    
void APP_Init_Slave_To_Remote_Alarm_Timer(uchar time)
{
   alm_timer_slave_dly = MAIN_OneSecTimer;
   FLAG_SLAVE_ALARM_TIMER_ON = 1;
   
   alm_slave_to_remote_delay_time = time;
   
    #ifndef CONFIGURE_UNDEFINED
        // TODO: Undefine Later  BC: Test Only
        SER_Send_String("Start Slv to Rmt timer");
        SER_Send_Prompt();
    #endif
}

/*
+------------------------------------------------------------------------------
| Function:      APP_Slave_Alarm_Timer
+------------------------------------------------------------------------------
| Purpose:       Test Slave Alarm Timer
|                FLAG_SLAVE_ALARM_TIMER_ON reset if Timer Expires
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void APP_Slave_To_Remote_Alarm_Timer(void)
{
    /* 
     * Shortened Delay to test if we need 
     * Delay anymore. Since RF module now handles Real Time Mode
     * coordination between multi-gateway configurations
     * 
     */
    if(FLAG_SLAVE_ALARM_TIMER_ON)
    {
        if( (uchar)(MAIN_OneSecTimer - alm_timer_slave_dly) >
             (uchar)(alm_slave_to_remote_delay_time + 
                    (app_rf_module_id & MASK_RF_ID )) )
        {
            FLAG_SLAVE_ALARM_TIMER_ON = 0;    
            
            #ifndef CONFIGURE_UNDEFINED
                // TODO: Undefine Later  BC: Test Only
                SER_Send_String("End Slv to Rmt timer");
                SER_Send_Prompt();
            #endif
            
        }
    }
}


/*
+------------------------------------------------------------------------------
| Function:      APP_Init_Rmt_PTT_Dly_Timer
+------------------------------------------------------------------------------
| Purpose:      Start Remote PTT to HW Interconnect Delay if not
|               the Coordinator
|   
|               When a Remote PTT is received wirelessly
|               a delay must be inserted before passing ths condition
|               via HW Interconnect. When 2 or more RFD units
|               Interconnected Alarms try to act as gateways (Remote to Slave)
|               Alarms were trying to both drive the Smoke alarm PTT signal
|               and CO PTT signals.
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
    
void APP_Init_Rmt_PTT_Dly_Timer(void)
{
       // Use 100 ms timer for finer delay resolution
       alm_timer_remote_ptt_dly = MAIN_100_MS_Timer;

#ifdef CONFIGURE_WIFI
       // We should not need this for WiFi
       FLAG_REMOTE_PTT_TIMER_ON = 0;
#else
       FLAG_REMOTE_PTT_TIMER_ON = 1;
       
        #ifndef CONFIGURE_UNDEFINED
            // TODO: Undefine Later  BC: Test Only
            SER_Send_String("Start Rmt PTT to HW INT timer");
            SER_Send_Prompt();
        #endif
       
#endif

}

/*
+------------------------------------------------------------------------------
| Function:      APP_Rmt_PTT_Timer
+------------------------------------------------------------------------------
| Purpose:       Test Remote PTT Timer
|                FLAG_SLAVE_ALARM_TIMER_ON reset if Timer Expires
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void APP_Rmt_PTT_Timer(void)
{
    if(FLAG_REMOTE_PTT_TIMER_ON)
    {
        if( (uint)(MAIN_100_MS_Timer - alm_timer_remote_ptt_dly) >
            (uint)(APP_PTT_DELAY_3_SEC + (app_rf_module_id * 2) ) )
        {
            FLAG_REMOTE_PTT_TIMER_ON = 0;    
            
            #ifndef CONFIGURE_UNDEFINED
                // TODO: Undefine Later  BC: Test Only
                SER_Send_String("End Rmt PTT to HW INT timer");
                SER_Send_Prompt();
            #endif
            
        }
    }
}



#endif


/*
+------------------------------------------------------------------------------
| Function:      APP_Init_Min_Alarm_Timer
+------------------------------------------------------------------------------
| Purpose:       Start Minimum Alarm Timer
|   
|               Start Minimum Alarm RF Notification Timer
|               
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
    
void APP_Init_Min_Alarm_Timer(void)
{
   alm_timer_slave_min = MAIN_OneSecTimer;
   FLAG_ALARM_MIN_TIMER_ON = 1;
   
    #ifndef CONFIGURE_UNDEFINED
        // TODO: Undefine Later  BC: Test Only
        SER_Send_String("Start Min Alm Timer ");
        SER_Send_Prompt();
    #endif
}


/*
+------------------------------------------------------------------------------
| Function:      APP_Min_Alarm_Timer
+------------------------------------------------------------------------------
| Purpose:       Test Minimum Alarm Timer
|                FLAG_ALARM_MIN_TIMER_ON reset if Timer Expires
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void APP_Min_Alarm_Timer(void)
{
    if(FLAG_ALARM_MIN_TIMER_ON)
    {
        if( (uchar)(MAIN_OneSecTimer - alm_timer_slave_min) >
            (uchar)(APP_SLAVE_DELAY_18_SEC) )
        {
            FLAG_ALARM_MIN_TIMER_ON = 0;    
            
            #ifndef CONFIGURE_UNDEFINED
                // TODO: Undefine Later  BC: Test Only
                SER_Send_String("End Min Alm Timer ");
                SER_Send_Prompt();
            #endif
            
        }
    }
}




#else   /* Wireless Module Not Configured */

    #define APP_RETURN_TIME_60_SEC  60000

    uint APP_Status_Monitor(void)
    {
        return APP_RETURN_TIME_60_SEC;
    }

#endif


   
    
