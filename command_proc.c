/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        command_proc.c
|  Author:      Bill chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Processes commands from serial port
|
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include "common.h"
#include "command_proc.h"
#include "sound.h"
#include "systemdata.h"
#include "comeasure.h"
#include "life.h"
#include "memory_18877.h"
#include "battery.h"
#include "photosmoke.h"
#include "light.h"
#include "led.h"

#include "cci_phy.h"
#include "alarmmp.h"
#include "fault.h"

#ifdef CONFIGURE_VOICE
    #include "voice.h"
#endif

#ifdef CONFIGURE_WIRELESS_MODULE
    #include "app.h"
#endif



/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

#define	NUM_RX_BUFFER_CHARS     7

#ifdef CONFIGURE_WIRELESS_MESSAGE_TEST
    // Commands for Manufacturing RF Module Reset Only
    #define CMD_AMP_RF_TEST_STATUS      (uchar)0x70
    #define CMD_RST_RF_FINAL_ONLY       (uchar)0x01
    #define CMD_RST_RF_FINAL_UNTESTED   (uchar)0x02
#endif

#define MASK_0xFF (uchar)0xFF
#define MASK_0xF0 (uchar)0xF0
#define MASK_0x0F (uchar)0x0F


/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/
char CMD_ReceiveBuffer[NUM_RX_BUFFER_CHARS];

#ifdef CONFIGURE_SMK_SLOPE_SIMULATION
    uint CMD_smk_slope = 0x100;
#endif

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
static uint current_address = 0;

static unsigned char bufferptr = 0;

/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/


/*
+------------------------------------------------------------------------------
| Function:      CMD_Processor
+------------------------------------------------------------------------------
| Purpose:       Process Commands received via Serial Port
|
|
+------------------------------------------------------------------------------
| Parameters:    *string   pointer to the received Command String
+------------------------------------------------------------------------------
| Return Value:  0  (currently not used)
+------------------------------------------------------------------------------
*/

unsigned char CMD_Processor( char const *string)
{
    switch (*string)
    {
        // A return only was sent.  Reply with a prompt.
        case '\0':

          
        break;

        case '@':
        // Returns the byte and word addressed by the HEX value sent
        // with the command.
        {
            SER_Send_Char('\r');
            SER_Send_String("0x");

            volatile int address = SER_Strx_To_Int(string + 1);
            SER_Send_Int( ' ', *(uchar *)(address), 16 );
            SER_Send_Char(',');
            SER_Send_String("0x");
            SER_Send_Int( ' ', *(uint *)(address), 16 );
        }
        break;

        // Sets the address to be referenced by the "D" command
        case 'A':
            current_address = SER_Strx_To_Int(string + 1);
            SER_Send_Int( ' ', current_address, 10 );
        break;

        // Controls serial output.
        case 'B':
            SER_out_flags.ALL = SER_Strx_To_Int(string + 1);
        break;
        
        // Battery Test
        case 'b':
            //Schedule Battery Test now...
            MAIN_Reset_Task(TASKID_BATTERY_TEST);
        break;


        // Saves the current contents of the system data structure to
        //   INFOA.
        case 'C':
            SYS_Save();
        break;

        // Writes the data sent with the command to the address set with the
        //   "A" command.  Only one byte is written.
        case 'D':
        {
            unsigned char data;
            data = (unsigned char)SER_Strx_To_Int(string + 1);
            *(unsigned char *)current_address = data;
        }
        break;

        case 'e':
        case 'E':
        {
            #ifndef	CONFIGURE_PRODUCTION_UNIT

                // Model based Test Commands (development only))

                #ifdef CONFIGURE_UNDEFINED
                    // Error Test ('E',Err# (0-FF) )
                    uint err = SER_Strx_To_Int(string + 1);
                    err = (err & 0xFF);

                    SER_Send_Int( 'E', err, 16 );

                    // Use Vxx to Simulate a Fault
                    FLT_Fault_Manager((uchar)err);
                #endif

                #ifdef CONFIGURE_WIRELESS_CCI_DEBUG
                    #ifdef CONFIGURE_SER_QUE_MAX
                        SER_Send_String("SERQue Max - ");
                        SER_Send_Int(' ',SER_quemax,10);
                        SER_Send_String(", ");

                        SER_Send_String("CCIQue Max - ");
                        SER_Send_Int(' ',SER_cci_quemax,10);

                        SER_Send_Prompt();
                    #endif
                #endif
            #endif

            #ifdef CONFIGURE_MAN_NIGHT_DAY_TOGGLE
                // Toggle Night Day for Testing only
                        
                // Simulate  Valid Light algorithm with Night/Day toggle       
                FLAG_LIGHT_ALG_ON = 1;
                
                // simulate Day/Night switch transition detect
                FLAG_DAY_NIGHT_SWITCHED = 1;
                
                if(FLAG_SOUND_INHIBIT)
                {
                    //day
                    FLAG_SOUND_INHIBIT = 0;
                    FLAG_LED_DIM = 0;
                    SND_Set_Chirp_Cnt((uchar)1);
                    
                    SER_Send_String("Day Set ");
                }
                else
                {
                    //night
                    FLAG_SOUND_INHIBIT= 1;
                    FLAG_LED_DIM = 1;
                    SND_Set_Chirp_Cnt((uchar)2);
                    
                    SER_Send_String("Night Set ");
                }
                SER_Send_Prompt();

            #endif

        }
        break;

        case 'F':

            #ifdef CONFIGURE_SERIAL_CO_ALARM
                // Toggle CO Alarm for Interconnect testing only
                if(FLAG_CO_TEST_ALARM)
                {
                    FLAG_CO_TEST_ALARM = 0;
                    FLAG_CO_ALARM_CONDITION = 0;
                }
                else
                {
                    FLAG_CO_TEST_ALARM = 1;
                }

                MAIN_Reset_Task(TASKID_COALARM);

            #endif

        break;

        case 'G':
        {
            uchar r, c, value;
            uchar csum = 0;
            uchar addr = 0;

            // Point to start of Non Volatile Data structures
            uint hist_addr = PRIMARY_START;

            #ifdef CONFIGURE_SERIAL_QUEUE
                FLAG_SER_SEND_DIRECT = 1;   // Do not buffer
            #endif

            SER_Send_String("H:UU");

            for(r = 0; r < 16; r++)
            {
                // New row
                SER_Send_Char('\r');
                // TX row starting Address
                SER_Send_Byte(addr);

                SER_Send_String(":  ");

                for(c = 0; c < 16; c++)
                {
                    // Read Diag History Structure (256 bytes)
                    value = MEMORY_Byte_Read(hist_addr++);
                    addr++;

                    // Update Checksum every byte
                    csum += value;

                    // Transmit hex asacii byte and space
                    SER_Send_Byte(value);
                    SER_Send_Char('\x20');
                }
            }
            // Hist Dump complete
            SER_Send_Char('\r');

            // Tx Checksum
            SER_Send_String("+:");
            SER_Send_Byte(csum);

        }

        #ifdef CONFIGURE_SERIAL_QUEUE
            FLAG_SER_SEND_DIRECT = 0;   // Re-enable Buffering
        #endif

        break;

        #ifdef  CONFIGURE_UNDEFINED
            // Writes the data sent with the command to the address set with
            // the "A" command.  Two bytes are written (integer version of
            // "D" command). Data must be in hex format.
            case 'H':
            {
                UINT udata;
                udata = SER_Strx_To_Int(string + 1);
                *(UINT*)current_address = udata;
            }
            break;
        #endif

            #ifdef CONFIGURE_ID_MANAGEMENT
                // Writes an ID number to the unit
                // "I" command.  2 bytes written
                case 'I':
                {
                    uint IDdata = SER_Strx_To_Int(string + 1);

                    MEMORY_Byte_Write_Verify(IDdata,DIAG_UNIT_ID_LOW);
                    MEMORY_Byte_Write_Verify(IDdata >> 8,DIAG_UNIT_ID_HIGH);

                    #ifdef CONFIGURE_ID_MANAGEMENT
                        MAIN_Output_Unit_ID();
                    #endif
                }
                break;
            #endif


        #ifdef CONFIGURE_MANUAL_PPM_COMMAND
            // Used for testing Algorithm and Jump states on Bench
            case 'P':
            {
                //
                // PPM Set ('P',ppm value (0000 - FFFF) )
                // Data must be in hex format.
                //
                uint udata;
                
                // Convert Ascii String into Unsigned Integer
                udata = SER_Strx_To_Int(string + 1);
                
                CO_manual_PPM_value = udata;
                
                #ifdef CONFIGURE_UNDEFINED
                    SER_Send_String("\rudata = ");
                    SER_Send_Int(' ', udata, 16);
                #endif
                
                SER_Send_Prompt();
                SER_Send_Int(' ', CO_manual_PPM_value, 16);
                SER_Send_Char(',');
                SER_Send_Int(' ', CO_manual_PPM_value, 10);
            }
            break;
        #endif

        #ifdef CONFIGURE_WIRELESS_MESSAGE_TEST
                //
                // 'M' Data TX Tag/Data(0x0000-0xFFFF)
                //  i.e. CCI Ping Tx = M0C00
                //
                // Data must be in hex format.
                //
            
            case 'm':
            case 'M':
            {
                #ifndef CONFIGURE_PRODUCTION_UNIT
                    union tag_CCI_Packet pkt;
                    uint udata;
      
                    #ifdef CONFIGURE_UNDEFINED
                        // Test Output the String
                        SER_Send_String(string);
                    #endif
                    
                    // Convert Ascii String into Unsigned Integer
                    udata = SER_Strx_To_Int(string + 1);
                    
                    #ifdef CONFIGURE_UNDEFINED
                        //SER_Send_String("\rudata = ");
                        //SER_Send_Int(' ', udata, 16);
                    #endif

                    pkt.data = (uchar)udata;
                    pkt.tag = (udata >> 8);
                    
                    SER_Send_Prompt();
                    SER_Send_Int(' ', pkt.tag, 16);
                    SER_Send_Char(',');
                    SER_Send_Int(' ', pkt.data, 16);

                    AMP_Start_Transaction(pkt);

                #else
                    // Production Configurations have manufacturing 
                    //  commands to reset RF test status bits
                    union tag_CCI_Packet pkt;
                    
                    // 1st determine which manuf. reset message to send
                    uchar test = *(string+1);
                    test = (uchar)(test & 0x03);

                    switch(test)
                    {
                        case 1:
                        {
                            
                            SER_Send_Prompt();
                            SER_Send_String
                                   ("Pwr Off and Re-Power unit for Final TST");
                            
                            // Clear RF Module Final Test bit only
                            pkt.data = CMD_RST_RF_FINAL_ONLY;
                            pkt.tag = (CMD_AMP_RF_TEST_STATUS);

                            // Allow serial message to transmit
                            for(uint x = 0; x<100; x++)
                            {
                                PIC_delay_ms(10);
                                SER_Xmit_Queue();
                            }
                            
                            AMP_Start_Transaction(pkt);
                           
                            FLAG_RST_FAULT_AT_RST = 1;
                            WDTCON0bits.SWDTEN = 0;
                            do
                            {
                                // Wait here for Power Down
                                // WDT Reset
                            }
                            while(1);
                            
                        }
                        break;

                        case 2:
                        {
                            // Wait here for Power Down
                            SER_Send_Prompt();
                            SER_Send_String
                                    ("Pwr Off for RF Module FCT Re-Test");
                            
                            // Clear Both RF Module Test bits
                            pkt.data = CMD_RST_RF_FINAL_UNTESTED;
                            pkt.tag = (CMD_AMP_RF_TEST_STATUS);
                            
                            // Allow serial message to transmit
                            for(uint x = 0; x<100; x++)
                            {
                                PIC_delay_ms(10);
                                SER_Xmit_Queue();
                            }
                            
                            
                            AMP_Start_Transaction(pkt);
                            
                            FLAG_RST_FAULT_AT_RST = 1;
                            WDTCON0bits.SWDTEN = 0;
                            do
                            {
                                
                                // Wait here for Power Down
                                // WDT Reset
                            }
                            while(1);
                            
                        }
                        break;
                        
                        default:
                        {
                            break;
                        }
                    }     

                #endif
            }
            break;
        #endif


        #ifdef CONFIGURE_MANUAL_SMK_OUT
            // Used for testing Algorithm and Hush Smoke states on Bench
            case 'S':
            {
                // Data received in in hex format.
                uchar udata = (uchar)SER_Strx_To_Int(string + 1);
                
                #ifdef CONFIGURE_MANUAL_CAV_Average
                    // Update Photo CAV average manually
                    // This was used to test CCI Smoke Comp message percentage
                    PHOTO_Cav_Average.byte.msb = udata;
                #else
                    // Smoke Set ('S',smoke value (00 - FF) )
                    PHOTO_Man_Reading = udata;
                #endif

                SER_Send_Prompt();
                SER_Send_Int(' ', udata, 16);
                SER_Send_Char(',');
                SER_Send_Int(' ', udata, 10);

            }
            break;
        #endif


        #ifdef CONFIGURE_MANUAL_SMK_TH
            // Used for testing Algorithm and Hush Smoke states on Bench
            case 't':
            {
                // Smoke Alarm Threshold Set ('T',smoke value (00 - FF) )
                // Data received in in hex format.
                uchar udata = (uchar)SER_Strx_To_Int(string + 1);
                SYS_RamData.Smk_Corrected_Alm_TH = udata;
                SYS_RamData.Smk_Alarm_TH_At_Cal = udata;

                // Save the new Threshold in EE Memory
                SYS_Save();

                SER_Send_Prompt();
                SER_Send_Int(' ', SYS_RamData.Smk_Alarm_TH_At_Cal, 16);
                SER_Send_Char(',');
                SER_Send_Int(' ', SYS_RamData.Smk_Alarm_TH_At_Cal, 10);

            }
            break;
        #endif

        #ifdef CONFIGURE_ACTIVE_TIMEOUT_TIMER
            // Used for Testing Active Mode Timeouts
            case 't':
            {
                MAIN_Active_Mode_Timer_Timeout();
                FLAG_ACTIVE_TIMEOUT_TEST = 1;
                SER_Send_String("\rActive Mode Timeout Set!\r");
            }
            break;
        #endif

        #ifdef CONFIGURE_VOICE_COMMAND
            #ifdef CONFIGURE_VOICE
                case 'V':
                {
                    // Voice Test ('V',voice# (0-FF) )
                    uint voice = SER_Strx_To_Int(string + 1);

                    voice = (voice & 0xFF);

                    SER_Send_Int( 'v', voice, 16 );
                    SER_Send_String(", ");
                    SER_Send_Int( ' ', voice, 10 );

                    // This Voice Chip needs guaranteed Power Setup Time
                    PORT_VOICE_EN_PIN = 1;      // Voice Chip Power On
                    PIC_delay_ms(240);
                    VCE_Play((uchar)voice);

                }
                break;
            #endif
        #endif



        #ifdef CONFIGURE_OUTPUT_RAM_DATA
            // For testing only, output Ram data structure
            case 'R':
            {
                uchar r, c, value;
                uchar addr = 0;
                // Point to start of Non Volatile Data structures
                uchar* ram_addr = (uchar*)&SYS_RamData;

                #ifdef CONFIGURE_SERIAL_QUEUE
                    FLAG_SER_SEND_DIRECT = 1;   // Do not buffer
                #endif

                SER_Send_String("Ram Data Structure (0xA0)");


                for(r = 0; r < 1; r++)
                {
                    // New row
                    SER_Send_Char('\r');
                    // TX row starting Address
                    SER_Send_Byte(addr);

                    SER_Send_String(":  ");

                    for(c = 0; c < 16; c++)
                    {
                     // Read Diag History Structure (256 bytes)
                        value = *ram_addr++;
                        addr++;

                        // Transmit hex asacii byte and space
                        SER_Send_Byte(value);
                        SER_Send_Char('\x20');
                    }
                }
                // Hist Dump complete
                SER_Send_Char('\r');

                #ifdef CONFIGURE_NO_REGULATOR
                    SER_Send_Prompt();
                    SER_Send_String("Current VDD Voltage (mv), ");
                    SER_Send_Int( ' ', CO_vdd_calc, 10 );
                #endif

                #ifdef CONFIGURE_SERIAL_QUEUE
                    FLAG_SER_SEND_DIRECT = 0;   // Re-enable Buffering
                #endif

            }
            break;
        #endif

        #ifdef CONFIGURE_OUTPUT_BACKUP_STRUCT

            case 'K':
            {
                uchar r, c, value;
                uchar addr = 0;
                //UINT hist_addr = 0;

                // Point to start of Non Volatile Data structures
                uint hist_addr = BACKUP_START;

                #ifdef CONFIGURE_SERIAL_QUEUE
                    FLAG_SER_SEND_DIRECT = 1;   // Do not buffer
                #endif

                SER_Send_String("Backup Copy of Structure (0x10)");

                for(r = 0; r < 1; r++)
                {
                    // New row
                    SER_Send_Char('\r');
                    // TX row starting Address
                    SER_Send_Byte(addr);

                    SER_Send_String(":  ");

                    for(c = 0; c < 16; c++)
                    {
                     // Read Diag History Structure (256 bytes)
                        value = MEMORY_Byte_Read(hist_addr++);
                        addr++;

                        // Transmit hex asacii byte and space
                        SER_Send_Byte(value);
                        SER_Send_Char('\x20');
                    }
                }
                // Hist Dump complete
                SER_Send_Char('\r');

                #ifdef CONFIGURE_SERIAL_QUEUE
                    FLAG_SER_SEND_DIRECT = 0;   // Re-enable Buffer
                #endif

            }
            break;
        #endif

        #ifdef CONFIGURE_MANUAL_LIFE_COMMAND    
            case 'L':
            case 'l':
            {
                uchar cmd;
                uchar year;

                // L0 - Clears Accelerated Life Counter & Life Expiration Test
                // L1 - Enables Life Expiration Test
                // L2 - Set Life counter to Year L2(1-9)
                // L8 - Enables Accelerated Life Counter
                //      (for diag History or other testing)

                cmd = SER_Strx_To_Int(string + 1);
                if(cmd & 0xF0)
                {
                   // Shift Command Char into position
                   cmd = cmd >> 4;
                   if(cmd != 2)
                   {
                       // Invalid, Default to Off Command
                       cmd = 0;
                   }
                }
                
                #ifdef CONFIGURE_UNDEFINED
                    SER_Send_String("\rCmd ");
                    SER_Send_Int(' ', cmd, 10);
                    SER_Send_Prompt();
                #endif
                
                
                if(cmd == 0x00)
                {
                    SER_Send_String("\rAccel Life OFF\r");
                    LIFE_Clr_Accel_Life();
                    
                    // Insure EOL is cleared
                    FLAG_EOL_FATAL = 0;
                    FLAG_EOL_MODE = 0;
                    FLAG_FAULT_FATAL = 0;
                    
                    FLT_Fatal_Code = 0;
                }
                else if(cmd == 0x01)
                {
                    SER_Send_String("\rAccel Life EXP ON\r");
                    LIFE_Set_Eol_Test();
                }
                else if(cmd == 0x02)
                {
                    uint mem_life_num_days = 0;
                    
                    year = SER_Strx_To_Int(string + 2);
                    
                    if(year < 11)
                    {
                        #ifdef CONFIGURE_UNDEFINED
                            SER_Send_String("\rLife to Year ");
                            SER_Send_Int(' ', year, 10);
                            SER_Send_Prompt();
                        #endif
                        
                        mem_life_num_days = (year * 365);
                        LIFE_Set_Life_Days(mem_life_num_days);
                        
                        SER_Send_String("\rLife Days set to ");
                        SER_Send_Int(' ', mem_life_num_days, 10);
                        SER_Send_Prompt();
                    
                    }
                    
                }
                else if(cmd == 0x08)
                {
                    
                    SER_Send_String("\rAccel Life Timing ON\r");
                    LIFE_Set_Accel_Life();
                    
                }

            }
            break;
        #endif

        #ifdef CONFIGURE_LIGHT_SENSOR
         #ifdef CONFIGURE_LIGHT_COMMAND
            case 'L':
            {
                uchar r;

                #ifdef CONFIGURE_SERIAL_QUEUE
                    FLAG_SER_SEND_DIRECT = 1;   // Do not buffer
                #endif

                SER_Send_String("Light Structure ");

                for(r = 0; r < 24; r++)
                {
                    // New row
                    SER_Send_Char('\r');
                    // TX row starting Address
                    if(r == LIGHT_Current_Hour)
                    {
                        SER_Send_Char('*');
                    }
                    SER_Send_Int( ' ',r, 10 );
                    SER_Send_String(":  ");
                    SER_Send_Int( ' ',LIGHT_Hour[r].Light_Current, 10 );
                    SER_Send_Char('\x20');
                    SER_Send_Int( ' ',LIGHT_Hour[r].Light_Previous, 10 );
                    SER_Send_Char('\x20');
                    SER_Send_Int( ' ',LIGHT_Hour[r].Attrib, 16 );

                }

                SER_Send_Prompt();
                SER_Send_String("TH = ");
                if(FLAG_LIGHT_ALG_ON)
                {
                    SER_Send_Int( ' ',LIGHT_Night_Day_TH, 10 );
                }
                else
                {
                    SER_Send_Int( ' ',NIGHT_DAY_TH_DEFAULT, 10 );
                }

                SER_Send_Prompt();

                if(FLAG_LIGHT_ALG_ON)
                {
                    SER_Send_String("Alg ON");
                }
                else
                {
                    SER_Send_String("Alg OFF");
                }
                SER_Send_Prompt();
                if(FLAG_LIGHT_SIGNAL_GOOD)
                {
                    SER_Send_String("Sig Good");
                }
                else
                {
                    SER_Send_String("Sig Low");
                }

                #ifdef CONFIGURE_SERIAL_QUEUE
                    FLAG_SER_SEND_DIRECT = 0;   // Re-enable Buffer
                #endif

            }
            break;

          #endif
        #endif


        #ifndef CONFIGURE_LED_CMD

            //BC: Test command only for development only
            case 'T':
            {
                // LED Test ('T',test# (0-9) )
                uchar test = *(string+1);
                test = (test & 0x0F);
                uchar param2 = *(string+2);
                param2 = (param2 & 0x0F);

                switch(test)
                {
                    case 1:
                    {
                        // Smoke Hush
                        if(!FLAG_SMOKE_HUSH_ACTIVE)
                        {
                            FLAG_SMOKE_HUSH_ACTIVE = 1;
                        }
                        else
                        {
                            FLAG_SMOKE_HUSH_ACTIVE = 0;
                        }
                    }
                    break;

                    case 2:
                    {
                        // Alarm Memory
                        if(!FLAG_SMOKE_ALARM_MEMORY)
                        {
                            FLAG_SMOKE_ALARM_MEMORY = 1;
                        }
                        else
                        {
                            FLAG_SMOKE_ALARM_MEMORY = 0;
                        }
                    }
                    break;
                    
                    case 3:
                    {
                    #ifdef CONFIGURE_WIRELESS_MODULE
                        // Set Search Mode
                        FLAG_COMM_RFD = 1;
                        
                        FLAG_APP_SEARCH_MODE = 1;
                        FLAG_APP_JOIN_MODE = 0;
                        FLAG_COMM_JOIN_COMPLETE = 0;
                        FLAG_COMM_JOIN_IN_PROGRESS = 1;
                        FLAG_COMM_ONLINE = 0;
                    #endif
                   }
                    break;

                    case 4:
                    {
                    #ifdef CONFIGURE_WIRELESS_MODULE
                        // Set Join Mode (Offline)
                        FLAG_COMM_RFD = 1;
                        
                        FLAG_APP_SEARCH_MODE = 0;
                        FLAG_APP_JOIN_MODE = 1;
                        FLAG_COMM_JOIN_IN_PROGRESS = 1;
                        FLAG_COMM_JOIN_COMPLETE = 1;
                        FLAG_COMM_ONLINE = 0;
                    #endif                        
                    }
                    break;

                    case 5:
                    {
                    #ifdef CONFIGURE_WIRELESS_MODULE
                        // Set Join Mode (Online)
                        FLAG_COMM_RFD = 1;
                        
                        FLAG_APP_SEARCH_MODE = 0;
                        FLAG_APP_JOIN_MODE = 0;
                        FLAG_COMM_JOIN_IN_PROGRESS = 0;
                        FLAG_COMM_JOIN_COMPLETE = 1;
                        FLAG_COMM_ONLINE = 1;
                    #endif                        
                    }
                    break;

                    case 6:
                    {
                    #ifdef CONFIGURE_WIRELESS_MODULE
                        // Simulate a 0x80 RFD timeout
                        FLAG_COMM_RFD = 1;
                        
                        FLAG_APP_SEARCH_MODE = 0;
                        FLAG_APP_JOIN_MODE = 0;
                        FLAG_COMM_JOIN_IN_PROGRESS = 0;
                        FLAG_COMM_JOIN_COMPLETE = 0;
                        FLAG_COMM_ONLINE = 0;
                    #endif
                    }
                    break;

                    case 7:
                    {
                    #ifdef CONFIGURE_WIRELESS_MODULE
                        // OOB 
                        FLAG_COMM_RFD = 0;
                        
                        FLAG_APP_SEARCH_MODE = 0;
                        FLAG_APP_JOIN_MODE = 0;
                        FLAG_COMM_JOIN_IN_PROGRESS = 0;
                        FLAG_COMM_JOIN_COMPLETE = 0;
                        FLAG_COMM_ONLINE = 0;
                        
                        FLAG_LED_OOB_PROFILE_ENABLE = 1;
                        
                    #endif
                    }
                    break;

                    case 8:
                    {
                        
                         
                    }
                    break;

                    case 9:
                    {
                        switch(param2)
                        {
                            case 1:
                                #ifdef CONFIGURE_WIRELESS_MODULE
                                    if(!FLAG_STATUS_3_INTRUDER)
                                    {
                                        FLAG_STATUS_3_INTRUDER = 1;
                                    }
                                #endif
                             break;
                            
                            case 2:
                                #ifdef CONFIGURE_WIRELESS_MODULE
                                    if(!FLAG_STATUS_3_WEATHER)
                                    {
                                        FLAG_STATUS_3_WEATHER = 1;
                                    }
                                #endif
                             break;
                             
                            case 3:
                                #ifdef CONFIGURE_WIRELESS_MODULE
                                    if(!FLAG_ALARM_IDENTIFY)
                                    {
                                        FLAG_ALARM_IDENTIFY = 1;
                                    }
                                    else
                                    {
                                        FLAG_ALARM_IDENTIFY = 0;
                                    }
                                #endif
                            break;
                            
                            case 4:
                                #ifdef CONFIGURE_WIRELESS_MODULE
                                    if(!FLAG_ALERT_PANIC)
                                    {
                                        FLAG_ALERT_PANIC = 1;
                                    }
                                    else
                                    {
                                        FLAG_ALERT_PANIC = 0;
                                    }
                                #endif
                            break;
                            
                        }
                    }
                    break;


                    default:
                    {

                    }
                    break;

                }
                
                // set this flag to inhibit immediate GLED blinks
                //  when starting a profile
                FLAG_LED_GRN_PWR_DELAY = 1;
                
                MAIN_Reset_Task(TASKID_GLED_MANAGER);
                
            }
            break;
        #endif

        #ifdef CONFIGURE_SMK_SLOPE_SIMULATION
            // X Command
            // Entry Format 3 characters followed by <ENTER>
            // 100 = slope of 1.00 (no adj. change from smoke measurement)
            // 101 = 1.01 slope increase  (can be set to 101-199)
            // 099 - 0.99 slope decrease  (can be set to 099-000)
            case 'X':
            {
                uint low;
                uint hi;

                uint x = SER_Strx_To_Int(string + 1);
                SER_Send_Int( 'x', x, 16 );
                SER_Send_Prompt();

                hi = *(string + 1);
                hi = (hi & 0x01);                        // Integer part

                // Separate Nibbles (fractional part)
                uchar low_msb = (*(string + 2) & 0x0F);  // 10s
                uchar low_lsb = (*(string + 3) & 0x0F);  // 100s

                // Covert decimal fractional input into hex format
                low = (low_msb * 10);
                low += low_lsb;

                // Convert Slope to Hex_8_8 format for Integer Math
                CMD_smk_slope = ( (hi*(uint)256) + ((low * (uint)256) / 100) );

                SER_Send_Int( 'l', (uint)low, 16 );
                SER_Send_Prompt();
                
                SER_Send_String("slope = ");
                SER_Send_Int( ' ', CMD_smk_slope, 16 );
                SER_Send_Prompt();

                // Save in Non-Volatile Memory
                MEMORY_Byte_Write_Verify(CMD_smk_slope,DIAG_SLOPE_LOW);
                MEMORY_Byte_Write_Verify(CMD_smk_slope >> 8,DIAG_SLOPE_HIGH);


            }
            break;

        #endif

        case 'O':
        {
            #ifndef	CONFIGURE_PRODUCTION_UNIT
                #ifdef CONFIGURE_UNDEFINED
                    // Request a Light sensor LED Blink
                    FLAG_BLINK_LIGHT_SENS_LED  = 1;
                #endif
            #endif

            #ifdef CONFIGURE_ACTIVE_TIMEOUT_TIMER
                // Used for Testing Active Mode Timeouts
                SER_Send_String("\rSet Active Flag!\r");
                    
                // Set an some Active Flags to Test Here
                FLAG_HUSH_CANCEL_REQUEST = 1;  
                FLAG_SMK_ALMPRIO_TRANS = 1;
            #endif

            #ifdef CONFIGURE_TEST_CODE
                log_out();
            #endif
                    
        }
        break;

        case 'W':
        {
            GLOBAL_INT_ENABLE = 0;  
            
            // Test WD Timer reset
            do
            {
                #ifdef CONFIGURE_WDT_RST_FLAGS
                    // Test Only, Remove later
                    FLAG_WDT_1_7 = 1;
                #endif

                // WDT Reset
            }
            while(1);
            
        }
        break;
        
        case 'Z':
        case 'z':
        {
            // Toggle Network Stealth Search Mode Off/On
            if(FLAG_STEALTH_SEARCH_OFF)
            {
                FLAG_STEALTH_SEARCH_OFF = 0;
                SER_Send_String("Stealth Search ON");
                SER_Send_Prompt();
            }
            else
            {
                FLAG_STEALTH_SEARCH_OFF = 1;
                
                if(FLAG_APP_JOIN_MODE || FLAG_APP_SEARCH_MODE)
                {
                    // This Allows LED profile to run if in Search Mode
                    FLAG_LED_PRO_N_PROGRESS = 0;
                }
                SER_Send_String("Stealth Search OFF");
                SER_Send_Prompt();
                
            }
            
        }
        break;

        
        default:
            SER_Send_Char('\r');
        break;
    }

    SER_Send_Prompt();

    return 0;
}

/*
+------------------------------------------------------------------------------
| Function:      CMD_Buffer_Push
+------------------------------------------------------------------------------
| Purpose:       Push Command Data into command buffer for processing
|                Command received when New Line Character is received
|
+------------------------------------------------------------------------------
| Parameters:    Received Character to Push
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void CMD_Buffer_Push(char ch)
{
    if('\r' == ch)
    {
        // Newline character
        CMD_ReceiveBuffer[bufferptr]= 0;
        bufferptr=0;
        FLAG_RX_COMMAND_RCVD = 1;

    }
    else
    {
        // Push into Command Buffer
        CMD_ReceiveBuffer[bufferptr++]= ch;
        if(bufferptr > NUM_RX_BUFFER_CHARS -1)
        {
          bufferptr = 0;
        }
    }    
    
}

/*
+------------------------------------------------------------------------------
| Function:      CMD_Process_Command
+------------------------------------------------------------------------------
| Purpose:       Process the current data in Command Buffer
|                
|
+------------------------------------------------------------------------------
| Parameters:    none  (Command sits in the Command Buffer)
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void CMD_Process_Command(void)
{
    CMD_Processor(CMD_ReceiveBuffer);
}


#ifdef CONFIGURE_SMK_SLOPE_SIMULATION

/*
+------------------------------------------------------------------------------
| Function:      CMD_Get_Slope
+------------------------------------------------------------------------------
| Purpose:       Return the Simulated Smoke Sensitivity Slope
|                
|
+------------------------------------------------------------------------------
| Parameters:    none  
+------------------------------------------------------------------------------
| Return Value:  Slope value
+------------------------------------------------------------------------------
*/

uint CMD_Get_Slope(void)
{
    return CMD_smk_slope;
}



/*
+------------------------------------------------------------------------------
| Function:      CMD_Set_Slope
+------------------------------------------------------------------------------
| Purpose:       Set the Simulated Smoke Sensitivity Slope
|                
|
+------------------------------------------------------------------------------
| Parameters:    Slope value
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

CMD_Set_Slope(uint slope)
{
    CMD_smk_slope = slope;
}

#endif