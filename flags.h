/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        flags.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file to define project's Global FLAGS
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

#ifndef FLAGS_H
#define	FLAGS_H

/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

// Non-Reset Flags are preserved on Soft Resets

// Non-Reset Flags 1
#define FLAG_SENSOR_FAULT_RST           Flags_NR._0
#define FLAG_POWERUP_COMPLETE           Flags_NR._1
#define FLAG_REQ_JOIN_AT_RST            Flags_NR._2
#define FLAG_RST_FAULT_AT_RST           Flags_NR._3
#define FLAG_LIGHT_SIGNAL_GOOD          Flags_NR._4
#define FLAG_LIGHT_ALG_ON               Flags_NR._5
#define FLAG_ALG_ENABLED                Flags_NR._6
#define FLAG_BUTTON_ALG_ENABLED         Flags_NR._7

// Non-Reset Flags 2
#define FLAG_RST_SOFT                   Flags_NR2._0
#define FLAG_NET_ERR_7_DAY_EXP          Flags_NR2._1
#define FLAG_FAULT_7_DAY_TIMER_ACTIVE  	Flags_NR2._2
#define FLAG_VOICE_TIMER_ACTIVE        	Flags_NR2._3
#define FLAG_VOICE_TIMER_EXP            Flags_NR2._4
#define FLAG_LB_HUSH_DISABLED           Flags_NR2._5
#define FLAG_LB_TIMER_ACTIVE            Flags_NR2._6
#define FLAG_VOICE_FRENCH               Flags_NR2._7

// Non-Reset Flags 3
#define FLAG_CO_ALARM_MEMORY            Flags_NR3._0
#define FLAG_SMOKE_ALARM_MEMORY         Flags_NR3._1
#define FLAG_CO_ALARM_MEMORY_ANNOUNCE   Flags_NR3._2
#define FLAG_SMK_ALARM_MEMORY_ANNOUNCE  Flags_NR3._3
#define FLAG_PREV_JOINED                Flags_NR3._4
#define FLAG_DISABLE_WIFI               Flags_NR3._5
#define FLAG_STAND_ALONE_MODE           Flags_NR3._6
#define FLAG_WIRELESS_DISABLED          Flags_NR3._7



// Active Mode Only Trigger Flags
#define FLAG_PTT_ACTIVE                 Flags_Active1._0
#define FLAG_BUTTON_EDGE_DETECT         Flags_Active1._1
#define FLAG_POWERUP_DELAY_ACTIVE       Flags_Active1._2
#define FLAG_BUTTON_NOT_IDLE            Flags_Active1._3
#define FLAG_APP_NOT_IDLE               Flags_Active1._4
#define FLAG_CHIRPS_ACTIVE              Flags_Active1._5
#define FLAG_SOUND_NOT_IDLE             Flags_Active1._6
#define FLAG_VOICE_SEND                 Flags_Active1._7

#define FLAG_CO_ALARM_CONDITION         Flags_Active2._0
#define FLAG_CALIBRATION_NONE           Flags_Active2._1
#define FLAG_CALIBRATION_PARTIAL        Flags_Active2._2
#define FLAG_CALIBRATION_UNTESTED       Flags_Active2._3
#define FLAG_CO_ALARM_ACTIVE            Flags_Active2._4
#define FLAG_SLAVE_CO_ACTIVE            Flags_Active2._5
#define FLAG_AC_DETECT                  Flags_Active2._6
#define FLAG_SERIAL_PORT_ACTIVE         Flags_Active2._7

#ifdef CONFIGURE_DIAG_ACTIVE_FLAGS
    #define FLAG_CCI_ACTIVE             Flags_Active3._0
#else
    #define FLAG_3_0                    Flags_Active3._0
#endif
#define FLAG_VOICE_ACTIVE               Flags_Active3._1
#define	FLAG_MASTER_SMOKE_ACTIVE       	Flags_Active3._2
#define FLAG_SLAVE_SMOKE_ACTIVE        	Flags_Active3._3
#define FLAG_SMOKE_ALARM_ACTIVE         Flags_Active3._4
#define FLAG_INTCON_SMOKE_RECEIVED      Flags_Active3._5
#define FLAG_NO_SMOKE_CAL              	Flags_Active3._6
#define FLAG_SLAVE_ALARM_MODE         	Flags_Active3._7

#define FLAG_SMOKE_HUSH_REQUEST         Flags_Active4._0
#define FLAG_HUSH_CANCEL_REQUEST        Flags_Active4._1    // currently unused
#define	FLAG_LOW_BATTERY_CAL_MODE      	Flags_Active4._2
#define FLAG_SMK_STABILIZATION         	Flags_Active4._3
#define FLAG_BATTERY_NOT_IDLE           Flags_Active4._4
#define FLAG_INTCON_EDGE_DETECT         Flags_Active4._5
#define FLAG_INTCON_CO_RECEIVED         Flags_Active4._6
#define FLAG_INTCON_STATE_NOT_IDLE     	Flags_Active4._7

#define FLAG_CCI_EDGE_DETECT            Flags_Active5._0
#define FLAG_LED_BLINKS_ACTIVE          Flags_Active5._1
#define	FLAG_LP_BATTERY_TEST            Flags_Active5._2
#define FLAG_SMOKE_HUSH_ACTIVE          Flags_Active5._3
#define FLAG_REMOTE_SMOKE_ACTIVE        Flags_Active5._4
#define FLAG_REMOTE_CO_ACTIVE           Flags_Active5._5
#define FLAG_STROBE_NOT_IDLE            Flags_Active5._6
#define FLAG_LED_PWM_ENABLE             Flags_Active5._7

#define FLAG_REMOTE_ALARM_MODE          Flags_Active6._0
#define FLAG_REMOTE_PTT_ACTIVE          Flags_Active6._1
#define	FLAG_ACTIVE_TIMER               Flags_Active6._2
#define FLAG_ALM_PRIO_NOT_IDLE          Flags_Active6._3
#define FLAG_CCI_TEST                   Flags_Active6._4
#define FLAG_LGT_MEAS_CYCLE             Flags_Active6._5
#define FLAG_APP_SEARCH_MODE            Flags_Active6._6
#define FLAG_APP_JOIN_MODE              Flags_Active6._7

#define FLAG_GLED_NOT_IDLE              Flags_Active7._0
#define FLAG_RLED_NOT_IDLE              Flags_Active7._1
#define	FLAG_ALED_NOT_IDLE              Flags_Active7._2
#define FLAG_T1_XTAL_NOT_STABLE         Flags_Active7._3
#define FLAG_REMOTE_HW_SMOKE_ACTIVE     Flags_Active7._4
#define FLAG_REMOTE_HW_CO_ACTIVE        Flags_Active7._5
#define FLAG_ALARM_MIN_TIMER_ON         Flags_Active7._6
#define FLAG_SMK_ALMPRIO_TRANS          Flags_Active7._7

//******************************************************************************
// Condition Alert Flags

//  CCI APP_Remote_Alerts
#define FLAG_ALERT_PANIC                Flags_Remote_Alerts._0
#define FLAG_ALERT_WEATHER              Flags_Remote_Alerts._1
#define FLAG_ALERT_WATER                Flags_Remote_Alerts._2
#define FLAG_ALERT_EXP_GAS              Flags_Remote_Alerts._3 
#define FLAG_ALERT_TEMP_DROP            Flags_Remote_Alerts._4
#define FLAG_ALERT_INTRUDER             Flags_Remote_Alerts._5
#define FLAG_ALERT_GENERAL              Flags_Remote_Alerts._6
#define FLAG_ALERT_DETECT               Flags_Remote_Alerts._7

//******************************************************************************
// App Remote Flags

//  CCI APP_Remote_Flags
#define FLAG_ALARM_IDENTIFY             Flags_Remote_Flags1._0
#define FLAG_RF_1_1                     Flags_Remote_Flags1._1
#define FLAG_RF_1_2                     Flags_Remote_Flags1._2
#define FLAG_RMT_FAULT_NEW              Flags_Remote_Flags1._3
#define FLAG_DIAG_TIMER                 Flags_Remote_Flags1._4
#define FLAG_REMOTE_NET_ERR_HUSH        Flags_Remote_Flags1._5
#define FLAG_REMOTE_EOL_HUSH            Flags_Remote_Flags1._6
#define FLAG_REMOTE_LB_HUSH             Flags_Remote_Flags1._7

//******************************************************************************
//***************************End of Active Flags********************************
//******************************************************************************

#define FLAG_MODE_ACTIVE                Flags1._0
#define FLAG_RESET                      Flags1._1
#define FLAG_VCE_BUTTON_PRESS           Flags1._2
#define FLAG_BAT_TST_SHORT              Flags1._3
#define FLAG_LB_HUSH_ACTIVE             Flags1._4
#define FLAG_1_5                        Flags1._5
#define FLAG_NIGHT_DETECT               Flags1._6
#define FLAG_RX_COMMAND_RCVD            Flags1._7


#define FLAG_LOW_BATTERY                Flags2._0
#define FLAG_SOUND_INHIBIT              Flags2._1
#define FLAG_LB_PENDING                 Flags2._2
#define FLAG_SOUND_INHIBIT_DISABLE      Flags2._3
#define FLAG_EOL_HUSH_ACTIVE            Flags2._4
#define FLAG_CAL_ERROR                  Flags2._5
#define FLAG_INTCON_TX_CO_SIGNAL       	Flags2._6
#define FLAG_EOL_FATAL                  Flags2._7


#define FLAG_SENSOR_CIRCUIT_STABLE      Flags3._0
#define FLAG_SENSOR_TEST                Flags3._1
#define FLAG_CALIBRATION_COMPLETE       Flags3._2
#define FLAG_RECOV_INHIBIT_PPM_CALC     Flags3._3
#define FLAG_AD_USE_2048_FVR            Flags3._4
#define FLAG_FUNC_CO_TEST               Flags3._5
#define FLAG_SMOKE_ERROR_PENDING        Flags3._6
#define FLAG_CO_FAULT_PENDING           Flags3._7


#define FLAG_PLAY_VOICE                	Flags4._0
#define FLAG_BAT_CONSRV_MODE_ACTIVE     Flags4._1
#define FLAG_VOICE_INHIBIT              Flags4._2
#define FLAG_LB_HUSH_INHIBIT        	Flags4._3
#define FLAG_DIAG_HIST_DISABLE          Flags4._4
#define FLAG_ESC_LIGHT_ON               Flags4._5
#define FLAG_LOW_POWER_ACTIVE_CYCLE     Flags4._6
#define FLAG_INHIBIT_REV_MSG            Flags4._7

#define FLAG_PHOTO_HIGH_CAL            	Flags5._0
#define FLAG_FIND_DRIVE_ERROR          	Flags5._1
#define FLAG_FIND_LOW_DRIVE_ACTIVE     	Flags5._2
#define FLAG_SMOKE_HUSH_ARM_SENT        Flags5._3
#define FLAG_SMOKE_FAST_SAMPLE         	Flags5._4
#define FLAG_BAT_TEST_DLY               Flags5._5
#define FLAG_INT_ACTIVITY_DETECTED     	Flags5._6
#define FLAG_INIT_CAV_AVG               Flags5._7

// Led Flags
#define FLAG_RED_LED_BLINK             	Flags6._0
#define FLAG_RED_LED_ON                 Flags6._1
#define FLAG_AMBER_LED_BLINK         	Flags6._2
#define FLAG_AMBER_LED_ON               Flags6._3
#define FLAG_GREEN_LED_BLINK            Flags6._4
#define FLAG_GREEN_LED_ON               Flags6._5
#define FLAG_LIGHT                      Flags6._6
#define FLAG_LIGHT_MEAS_ACTIVE          Flags6._7

//
#define FLAG_APP_REQ_COMM_STATUS        Flags7._0
#define FLAG_PING_ACK                   Flags7._1
#define FLAG_WAKE_FOR_CCI               Flags7._2
#define FLAG_REQUEST_TRANSCEIVER_STATUS Flags7._3
#define FLAG_WL_GATEWAY_ENABLE          Flags7._4
#define FLAG_REMOTE_ERROR_REQ           Flags7._5
#define FLAG_APP_SEND_ALM_STATUS        Flags7._6
#define FLAG_REMOTE_HUSH_RECEIVED       Flags7._7


#define FLAG_LB_CAL_STATUS              Flags8._0
#define FLAG_LB_CAL_SIGNAL              Flags8._1
#define FLAG_LP_60_SEC                 	Flags8._2
#define FLAG_FAST_SAMPLE_ENABLE        	Flags8._3
#define FLAG_1SEC_TIMER                	Flags8._4
#define FLAG_CO_TRIG_DISABLE           	Flags8._5
#define FLAG_JUMPED_STATE              	Flags8._6
#define FLAG_MEM_ERR                   	Flags8._7


#define FLAG_SER_SEND_DIRECT            Flags9._0
#define FLAG_9_1                        Flags9._1
#define FLAG_LED_DIM                    Flags9._2
#define FLAG_GREEN_LED_PULSE            Flags9._3
#define FLAG_AMBER_LED_PULSE          	Flags9._4
#define FLAG_LED_OOB_PROFILE_ENABLE     Flags9._5
#define FLAG_LED_PROFILE_BUSY           Flags9._6

#ifdef CONFIGURE_SERIAL_CO_ALARM
    #define FLAG_CO_TEST_ALARM          Flags9._7
#else
    #define FLAG_9_7                   	Flags9._7
#endif

#define FLAG_FAULT_DELAY_ACTIVE         Flags10._0
#define FLAG_TM_NO_ACTIONS              Flags10._1
#define FLAG_AC_WIFI_RST                Flags10._2
#define FLAG_PTT_RESUME                 Flags10._3
#define FLAG_ALARM_LOCATE_MODE          Flags10._4
#define FLAG_ALARM_LOCATE_REQUEST       Flags10._5
#define FLAG_FAULT_CCI_IMMED            Flags10._6
#define FLAG_10_7                       Flags10._7

#define FLAG_REQ_NETWORK_CLOSE          Flags11._0
#define FLAG_REQ_ESTABLISH_COORD        Flags11._1
#define FLAG_REQ_RESET_OOB              Flags11._2
#define FLAG_REQ_ENABLE_JOIN            Flags11._3
#define FLAG_REQ_NUM_DEVICES            Flags11._4
#define FLAG_APP_DISPLAY_NETWORK_SIZE   Flags11._5
#define FLAG_REQ_ERROR_CODE_BLINK       Flags11._6
#define FLAG_REQ_RESET_AFTER_BLINKS     Flags11._7


#define FLAG_NETWORK_ERROR_MODE         Flags12._0
#define FLAG_FAULT_FATAL                Flags12._1
#define FLAG_NETWORK_ERROR_HUSH_ACTIVE  Flags12._2
#define FLAG_ERROR_CODE_BLINKING        Flags12._3
#define FLAG_GLED_SEARCH_MONITOR        Flags12._4
#define FLAG_LED_GRN_PWR_DELAY          Flags12._5
#define FLAG_DO_NOT_ARM                 Flags12._6
#define FLAG_JOIN_OPEN_INITIATING       Flags12._7

#define FLAG_REMOTE_PTT_TIMER_ON        Flags13._0
#define FLAG_ALERT_HORN_ENABLED         Flags13._1
#define FLAG_BLINK_LIGHT_SENS_LED       Flags13._2
#define FLAG_ALARM_REFRESH              Flags13._3
#define FLAG_FORCE_INTO_SEARCH          Flags13._4
#define FLAG_ANNOUNCE_GRN_VOICE         Flags13._5
#define FLAG_ANNOUNCE_RED_VOICE         Flags13._6
#define FLAG_ANNOUNCE_AMB_VOICE         Flags13._7

#define FLAG_PWR_LED_OFF                Flags14._0
#define FLAG_ALARM_LOCATE_CLEAR         Flags14._1
#define FLAG_VCE_PWR_ON_DELAY_COMPLETE  Flags14._2
#define FLAG_LIGHT_FIRST_MEAS           Flags14._3
#define FLAG_DAY_NIGHT_SWITCHED         Flags14._4
#define FLAG_LB_CHIRP_NOW               Flags14._5
#define FLAG_VOICE_BUSY                 Flags14._6
#define FLAG_VOICE_TX_ACTIVE            Flags14._7


#define FLAG_SLAVE_ALARM_TIMER_ON       Flags15._0
#define FLAG_CCI_HS_RETRY               Flags15._1
#define FLAG_ALM_REMOTE_ENABLE          Flags15._2
#define FLAG_SLAVE_PTT_DETECTED         Flags15._3
#define FLAG_INT_SMK_INTERRUPTED_CO     Flags15._4
#define FLAG_GATEWAY_SLV_TO_REMOTE_OFF  Flags15._5
#define FLAG_GATEWAY_REMOTE_TO_SLAVE    Flags15._6
#define FLAG_GATEWAY_MASTER             Flags15._7

#define FLAG_NET_SIZE_DISPLAY_OFF       Flags16._0
#define FLAG_SMK_EXIT_TO_HUSH           Flags16._1
#define FLAG_WL_ALM_BAT_CONSERVE        Flags16._2

#ifdef CONFIGURE_ACTIVE_TIMEOUT_TIMER
    #define FLAG_ACTIVE_TIMEOUT_TEST    Flags16._3
#else
    #define FLAG_16_3                   Flags16._3
#endif

#define FLAG_OOB_RST_WITH_NO_REJOIN     Flags16._4
#define FLAG_EOL_MODE                   Flags16._5
#define FLAG_PTT_CANCEL                 Flags16._6
#define FLAG_JOIN_CLOSE_INITIATING      Flags16._7

// Voice Announce Request Flags
#define FLAG_REQ_VOICE_SEARCH_1         Flags17._0
#define FLAG_REQ_VOICE_JOIN_COORD       Flags17._1
#define FLAG_REQ_VOICE_JOIN_RFD         Flags17._2
#define FLAG_INT_DRV_OFF_DURING_PTT     Flags17._3
#define FLAG_ALM_INT_DRV_ENABLE         Flags17._4
#define FLAG_TIMEOUT_TIMER_ACTIVE       Flags17._5
#define FLAG_GREEN_FADE_ON              Flags17._6
#define FLAG_GREEN_FADE_OFF             Flags17._7

#define FLAG_PTT_COUNDOWN_ACTIVE        Flags18._0
#define FLAG_TMR_INT_DRV_ACTIVE         Flags18._1
#define FLAG_BAT_ABNORMALLY_LOW         Flags18._2
#define FLAG_INT_CO_ACTIVITY_DETECTED   Flags18._3

#ifdef CONFIGURE_DIAG_ACTIVE_FLAGS
    #define FLAG_DIAG_ACTIVE_RST        Flags18._4
#else
    #define FLAG_18_4                   Flags18._4
#endif

#define FLAG_STEALTH_SEARCH_OFF         Flags18._5
#define FLAG_AMP_TIMEOUT                Flags18._6
#define FLAG_CNCL_SMK_CO_ALARM          Flags18._7

#define FLAG_LED_RING_OFF               Flags19._0

#ifdef CONFIGURE_DIAG_ACTIVE_FLAGS
    #define FLAG_SERIAL_PORT_OFF        Flags19._1
#else
    #define FLAG_19_1                   Flags19._1
#endif

#define FLAG_REFRESH_REMOTE_STATE       Flags19._2      // Not Used
#define FLAG_CO_ALARM_OFF_TIMER         Flags19._3
#define FLAG_RST_INT_CO_RCVD            Flags19._4
#define FLAG_DIAG_PWR_RESET             Flags19._5
#define FLAG_REMOTE_ESC_LIGHT_ON        Flags19._6
#define FLAG_FAULT_BEEP_INHIBIT         Flags19._7

#define FLAG_20_0                       Flags20._0
#define FLAG_CO_SYNC_PULSE_ENA          Flags20._1
#define FLAG_SMK_SYNC_PULSE_ENA         Flags20._2
#define FLAG_SMK_SYNC_ACTIVE            Flags20._3
#define FLAG_CO_SYNC_RESET_ACTIVE       Flags20._4
#define FLAG_CO_SYNC_RESET_ENABLE       Flags20._5
#define FLAG_CO_SYNC_RESET_DSCHRG       Flags20._6
#define FLAG_CO_STROBE_1ST_SET          Flags20._7

//  Algorithm Status Byte Flags
#define FLAG_ALG_SMK_DETECT          	bALG_Status._0
#define FLAG_ALG_ALM_DETECT            	bALG_Status._1
#define FLAG_CAL_ALM_DETECT            	bALG_Status._2
#define FLAG_SMK_DELTA_ACTIVE          	bALG_Status._3

#define FLAG_SRR_ACTIVE                	bALG_Status._4
#define FLAG_CO_PPM_ACTIVE             	bALG_Status._5
#define FLAG_CRR_ACTIVE                	bALG_Status._6
#define FLAG_TRR_ACTIVE                	bALG_Status._7


#ifdef CONFIGURE_WDT_RST_FLAGS
    //**************************************************************************
    // WDT Debug Flags - Test Only

    //  WDT Debug Flags
    #define FLAG_WDT_1_0                    Flags_WDT_Flags._0
    #define FLAG_WDT_1_1                    Flags_WDT_Flags._1
    #define FLAG_WDT_1_2                    Flags_WDT_Flags._2
    #define FLAG_WDT_1_3                    Flags_WDT_Flags._3
    #define FLAG_WDT_1_4                    Flags_WDT_Flags._4
    #define FLAG_WDT_1_5                    Flags_WDT_Flags._5
    #define FLAG_WDT_1_6                    Flags_WDT_Flags._6
    #define FLAG_WDT_1_7                    Flags_WDT_Flags._7

#endif

/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/
typedef union {
    struct {
        unsigned _0   :1;
        unsigned _1   :1;
        unsigned _2   :1;
        unsigned _3   :1;
        unsigned _4   :1;
        unsigned _5   :1;
        unsigned _6   :1;
        unsigned _7   :1;
    };

    unsigned char ALL;
} Flags_t;


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/
// Define Flag bit structures
volatile Flags_t Flags1;
volatile Flags_t Flags2;
volatile Flags_t Flags3;
volatile Flags_t Flags4;
volatile Flags_t Flags5;
volatile Flags_t Flags6;
volatile Flags_t Flags7;
volatile Flags_t Flags8;
volatile Flags_t Flags9;
volatile Flags_t Flags10;
volatile Flags_t Flags11;
volatile Flags_t Flags12;
volatile Flags_t Flags13;
volatile Flags_t Flags14;
volatile Flags_t Flags15;
volatile Flags_t Flags16;
volatile Flags_t Flags17;
volatile Flags_t Flags18;
volatile Flags_t Flags19;
volatile Flags_t Flags20;

#ifdef CONFIGURE_WDT_RST_FLAGS
    volatile Flags_t persistent Flags_WDT_Flags;
#endif


volatile Flags_t bALG_Status;
volatile Flags_t Flags_Active1;
volatile Flags_t Flags_Active2;
volatile Flags_t Flags_Active3;
volatile Flags_t Flags_Active4;
volatile Flags_t Flags_Active5;
volatile Flags_t Flags_Active6;
volatile Flags_t Flags_Active7;

volatile Flags_t persistent Flags_NR;
volatile Flags_t persistent Flags_NR2;
volatile Flags_t persistent Flags_NR3;

// Wireless Flags
volatile Flags_t Flags_Remote_Alerts;
volatile Flags_t Flags_Remote_Flags1;



// Declare as Global
extern volatile Flags_t Flags1;
extern volatile Flags_t Flags2;
extern volatile Flags_t Flags3;
extern volatile Flags_t Flags4;
extern volatile Flags_t Flags5;
extern volatile Flags_t Flags6;
extern volatile Flags_t Flags7;
extern volatile Flags_t Flags8;
extern volatile Flags_t Flags9;
extern volatile Flags_t Flags10;
extern volatile Flags_t Flags11;
extern volatile Flags_t Flags12;
extern volatile Flags_t Flags13;
extern volatile Flags_t Flags14;
extern volatile Flags_t Flags15;
extern volatile Flags_t Flags16;
extern volatile Flags_t Flags17;
extern volatile Flags_t Flags18;
extern volatile Flags_t Flags19;
extern volatile Flags_t Flags20;

extern volatile Flags_t Flags_Active1;
extern volatile Flags_t Flags_Active2;
extern volatile Flags_t Flags_Active3;
extern volatile Flags_t Flags_Active4;
extern volatile Flags_t Flags_Active5;
extern volatile Flags_t Flags_Active6;
extern volatile Flags_t Flags_Active7;

extern volatile Flags_t persistent Flags_NR;
extern volatile Flags_t persistent Flags_NR2;
extern volatile Flags_t persistent Flags_NR3;

extern volatile Flags_t Flags_Remote_Alerts;
extern volatile Flags_t Flags_Remote_Flags1;

#ifdef CONFIGURE_WDT_RST_FLAGS
    extern volatile Flags_t persistent Flags_WDT_Flags;
#endif

/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
//Not applicable



#endif	/* FLAGS_H */

