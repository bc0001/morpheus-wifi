/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  Workfile:   main.c
|  Author:     Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Main module which contains:
|               main()function, Hardware / Initialization,
|                Task Management Engine, and Interrupt routines
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               Kidde, United Technologies
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 Kidde UTC Building and Industrial Systems.
|               All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Date           	Name               Description
| ========      ===============    ==========================================
| 12/29/2014	 Bill Chandler     Initial draft
*/

/*  
 * Project Revision History:
 *
 *  Revision
 *  Number          Date            Author
 *  ---------------------------------------------------------------------------
 *
 * Maintained in project Rev History file and/or SVN
 */
 


/* 
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include <pic16lf18877.h>

#include    "common.h"
#include    "defmsgs.h"

#include    "memory_18877.h"
#include    "diaghist.h"

#include    "a2d.h"
#include    "alarmprio.h"
#include    "battery.h"
#include    "button.h"
#include    "coalarm.h"
#include    "cocalibrate.h"
#include    "cocompute.h"
#include    "comeasure.h"
#include    "command_proc.h"
#include    "fault.h"
#include    "life.h"
#include    "ptt.h"
#include    "sound.h"
#include    "systemdata.h"
#include    "led.h"
#include    "esclight.h"
#include    "voice.h"
#include    "photosmoke.h"
#include    "interconnect.h"
#include    "algorithm.h"
#include    "light.h"
#include    "VBoost.h"
#include    "strobe.h"
#include    "cci_phy.h"
#include    "alarmMP.h"
#include    "app.h"



/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/



// *****************************************************************************
// Debug  Section, Test Only Defines (un-comment as needed)

//#define CONFIGURE_DBUG_HANG_IN_SLEEP        // To Measure Sleep Current

//#define CONFIGURE_DBUG_STACK_TEST           // Used to Monitor stack Usage
#define CONFIGURE_DBUG_SERIAL_RESET_STATUS    // Outputs Source of Reset

// *****************************************************************************

#ifndef TMR1
    // This didn't work if PIC header file already has TMR1 defined
    extern volatile unsigned short          TMR1   @ 0x20C;
#endif

#define DEVICE_REVISION_A2  (uint)0x2002
#define DEVICE_REVISION_A3  (uint)0x2003
    
#define	MAX_NUM_TASKS       22
#define	NUM_MODES           2

#define	NUM_TASKS_ACTIVE	20
#define	NUM_TASKS_LOWPOWER	3

#define	MODE_ACTIVE         0
#define	MODE_LOW_POWER      1

#define	TIMER1_INIT_VALUE_10MS	(uint)(65536 - 328)

// Test Only (timing was off 5 secs/min)  Under Investigation
// This was right at 1 minute times  (we are 327-280= 47 counts off)
//#define	TIMER1_INIT_VALUE_10MS	(uint)(65536 - 280)

#define	MAIN_TIC_INTERVAL_100MS	100	// milliseconds
#define	TIMER1_INIT_VALUE_100MS	(uint)(65536 - 3277)

#define	MAIN_TIC_INTERVAL_1000MS 1000	// milliseconds
#define	TIMER1_INIT_VALUE_1000MS (uint)(65536 - 32768)

#define MAIN_PWR_DELAY_TIME         (uint)600       // 6 second

#define MAIN_INTERVAL_30_SECS       (uint)30000     // in ms

#define TIMER_32KHZ_STABLE_12_SEC   (uchar)12

//*********************************  WDT  **************************************
//                                  0b00wwwwws
#define	MAIN_WDT_STARTUP_TIMEOUT    0b00011000		//4 Sec w/WDT Off


// Increase WDT to 4 seconds to see if periodic resets stop
// Only saw it on bench testing once every couple weeks...
//  BC - 3/6/2017

#define	MAIN_WDT_NORMAL_TIMEOUT     0b00010110		//2 Sec w/WDT Off
#define MAIN_PCON_WDT_RESET         (uchar)0x2F


//****************************  Clock Stuff  ***********************************

// Task Time Tic Intervals in ms
#define	MAIN_TIC_INTERVAL_ACTIVE        (uint)10
#define	MAIN_TIC_INTERVAL_LOWPOWER      (uint)1000

#define TIME_ACTIVE_60_SEC              (uint)60000
#define TIME_ACTIVE_30_SEC              (uint)30000
#define TIME_ACTIVE_15_SEC              (uint)15000
#define TIME_ACTIVE_10_SEC              (uint)10000
#define TIME_ACTIVE_5_SEC               (uint)5000
#define TIME_ACTIVE_2_SEC               (uint)2000

    
#define TIME_150_CLOCKS                 (uint)150
#define TIME_100_CLOCKS                 (uint)100
#define TIME_90_CLOCKS                  (uint)90
#define TIME_80_CLOCKS                  (uint)80
#define TIME_50_CLOCKS                  (uint)50


// Define in Active Tics 1 Sec Count (100 - 10 ms tics))
#define	TIMER_1_SEC_ACTIVE_COUNT    (uint)100
#define	TIMER_100_MS_ACTIVE_COUNT   (uint)10

#define TIMER_5_SECOND_COUNT        5     // in 1000ms Tics (LP Mode only)  
#define TIMER_30_SECOND_COUNT       30    // in 1000ms Tics (LP Mode only)
#define	TIMER_ONE_MINUTE_COUNT      60	  // in 1000ms Tics
#define TIMER_FIVE_MINUTE_COUNT     300
#define TIMER_ONE_HOUR_COUNT        60    // in Minutes
#define TIMER_5_MINUTES             5

    
#ifdef CONFIGURE_DIAG_ACTIVE_FLAGS  
    #define MAIN_TIMER_1_SEC       1
    #define MAIN_TIMER_2_SEC       2
    #define MAIN_TIMER_5_SEC       5

    #define MAIN_TIMER_200_MS      2        // 100 ms Tics
    #define MAIN_DIAG_COUNT_INIT   10       // Data Output Counter
#endif

#ifdef CONFIGURE_ACTIVE_TIMEOUT_TIMER
    #define MAIN_ACTIVE_MODE_TIMEOUT_2_MIN    (uchar)2     // 2 Minutes
    #define MAIN_ACTIVE_MODE_TIMEOUT_2_HR     (uchar)120   // 2 Hours
    #define MAIN_ACTIVE_MODE_TIMEOUT_1HR_15MIN (uchar)75   // 1 Hr 15 Min

    #define MAIN_MASK_SERIAL_ACTIVE_FLAG      (uchar)0x7F

#endif
    


// Set Timer0 - 8 bit, 1:1 post-scaler, Timer Enabled
#define TIMER0_INIT_T0CON0   0b10000000
// Clk Source FOSC/4, pre-scaler divide by 4  (equals 1 usec clock))
#define TIMER0_INIT_T0CON1   0b01000010

// Timer4, clock=fosc/4 (16 Mhz/4 = 4Mhz),
//    pre-scaler 011 = divide by 16 (4 usec clock)
//#define TIMER_1_MS_INIT 0b00000110

// Setup T4 for a 1 ms Timer
// Timer 4 Interrupts (1 ms Timer)
#define TMR4_INT_FLAG           PIR4bits.TMR4IF
#define TMR4_INT_ENABLE         PIE4bits.TMR4IE

#define MAIN_TIMER_ONE_MS       TMR4
#define TIMER_ONE_MS_INT_FLAG   TMR4_INT_FLAG
#define TIMER_ONE_MS_INT_ENAB   TMR4_INT_ENABLE
#define TIMER_ONE_MS_CONTROL    T4CON


// Defined Clock Sources for 1 MS Timer        
#define T4_CLK_SOSC  (uchar)1
#define T4_CLK_HFOSC (uchar)2

//
// Run T4 from crystal for tighter strobe sync. control    
// Clock Source - Sosc 32.768  KHz (=30.52 uSec)
//
#define TMR4_CLOCK_CON_INIT_1     0b00000110    // SOSC
    
// Clock Source - Fosc/4
#define TMR4_CLOCK_CON_INIT_2     0b00000001    // HF OSC
    
//pre-scaler 1 (1:1)   
#define TMR4_CON_INIT_1           0b00000000    // SOSC
    
    //pre-scaler 100 = divide by 16 (4 usec clock)
#define TMR4_CON_INIT_2           0b11000000    // HF OSC
    
#define TIME_1_MS_1  ((UCHAR)(256-33))   // 33 clocks at 30.5176 usec
#define TIME_1_MS_2  ((UCHAR)(256-250))  // 250 clocks at 4 usec = 1 ms
    
    
//#else
    
// Clock Source - Fosc/4
//#define TMR4_CLOCK_CON_INIT     0b00000001
    
//pre-scaler 100 = divide by 16 (4 usec clock)
//#define TMR4_CON_INIT           0b11000000    
//#define TIME_1_MS  ((UCHAR)(256-250))   // 250 clocks at 4 usec = 1 ms
    
//#endif

// Low Power Mode Blink rate
#define MAIN_GRN_BLINK_1_MIN    1
#define MAIN_GRN_BLINK_2_MIN    2
#define MAIN_GRN_BLINK_5_MIN    5
#define MAIN_GRN_BLINK_10_MIN   10

#define MAIN_ACTIVE_DEBUG_TIME  100     // 1 second in 10 ms TICs


#define TIME_DIAG_TIMER_60_SEC  (uchar)60

#define WPUB_ALL_PULLUPS_OFF    0x00;

/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/
struct TaskInfo
{
  volatile uint TaskTime;
  volatile uint TaskInterval;
  uint (*funcptr)(void);
};

/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/
uchar MAIN_OneMinuteTic = 0;
uchar MAIN_OneSecTimer = 0;
uchar MAIN_OneHourTic = 0;

#ifdef	CONFIGURE_INTERCONNECT
    #ifdef CONFIGURE_WIRELESS_MODULE
        uint  MAIN_100_MS_Timer = 0;
    #endif
#endif

uint MAIN_ActiveTimer;

#ifdef CONFIGURE_MONITOR_INT
    uint MAIN_Monitor_Int1 = 0;
    uint MAIN_Monitor_Int2 = 0;
#endif


#ifdef CONFIGURE_DBUG_STACK_TEST
    uchar MAIN_Stack_Ptr_Peak = 0;
    uchar MAIN_Stack_Ptr_Now = 0;
#endif
    
#ifdef CONFIGURE_TEST_SMK_COMP
    uchar Timer_Test_Alm_Comp = (uchar)24;  // every 24 minutes 
    uchar Timer_Test_Init_Comp = (uchar)10;  // every 10 seconds
#endif
    

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
    
static uint main_tic_timer = 0;
static uint main_current_ms_per_tic = 0;
static uchar main_current_mode = 0;
static uchar main_sec_count = TIMER_1_SEC_ACTIVE_COUNT;
static uint  main_powerup_awake_timer = 0;
static uchar main_t4_select = 0;

#ifdef CONFIGURE_ACTIVE_TIMEOUT_TIMER
    static uchar main_active_timeout_timer = 0;
#endif

#ifdef CONFIGURE_UNDEFINED
    // TODO: Undefine Later, BC: Test Only 
    static uint main_while_timer_ms = 0;
#endif
    
//#define CONFIGURE_XTAL_SERIAL     
#ifdef CONFIGURE_XTAL_SERIAL
    static uint main_xtal_count = 0;
#endif

uchar main_hour_count = TIMER_ONE_HOUR_COUNT;
uchar main_one_minute_count = TIMER_ONE_MINUTE_COUNT;
uchar main_thirty_sec_count = TIMER_30_SECOND_COUNT;
uchar main_five_sec_count = TIMER_5_SECOND_COUNT;
uchar main_100_ms_count = TIMER_100_MS_ACTIVE_COUNT; 

static uchar main_sys_validate_count = 0;

uchar main_lp_blink_timer = 1;

#ifdef CONFIGURE_DBUG_SERIAL_RESET_STATUS
    uchar volatile main_pcon_save = 0;
    uchar volatile main_status_save = 0;
    uchar volatile main_wdt_flags_save = 0;
#endif

static uchar main_xtal_osc_timer = 0;

#ifdef CONFIGURE_DIAG_TIMER
    static uchar main_diag_timer = 0;
#endif

#ifdef CONFIGURE_DIAG_ACTIVE_FLAGS
    static uint main_diag_active_timer = 0;
    static uchar main_diag_count = 0;
    static uint main_diag_sec_time = 0;  // Time Low Power Time Intervals
#endif
    
///////////////////////////////////////////////////////////////////////////////
// To add another task:
//	1. Declare a TaskInfo variable and initialize.
//	2. Add a pointer to the TaskInfo structure to TaskTable
//	3. Add a task ID in main.h.  Make sure that the ID corresponds to the
//		position of the TaskInfo pointer in TaskTable
//	4. Include the header file of the task module above.  Task prototype is
//		UINT Task(void)
//  5. Add the task ID to the desired mode in Main_Mode and increment the
//		NUM_TASKS_xxx for the mode.
///////////////////////////////////////////////////////////////////////////////

struct TaskInfo SoundTaskInfo;
struct TaskInfo ButtonTaskInfo;
struct TaskInfo PTT_TaskInfo;
struct TaskInfo BatteryTestInfo;
struct TaskInfo DebugTaskInfo;
struct TaskInfo AlarmPrioInfo;
struct TaskInfo COAlarmInfo;
struct TaskInfo CalManagerInfo;
struct TaskInfo COMeasureInfo;
struct TaskInfo TroubleManagerInfo;
struct TaskInfo GLedInfo;
struct TaskInfo RLedInfo;
struct TaskInfo ALedInfo;
struct TaskInfo LedBlinkInfo;
struct TaskInfo EscLightInfo;
struct TaskInfo VoiceInfo;
struct TaskInfo PhotoSmokeInfo;
struct TaskInfo InterconnectInfo;
struct TaskInfo LightInfo;
struct TaskInfo AppStatusMonitorInfo;

//  The number of tasks in each mode.
const unsigned int TaskNumbers[NUM_MODES] =
    {NUM_TASKS_ACTIVE, NUM_TASKS_LOWPOWER};

struct TaskInfo  * const(TaskTable)[] =
{
    &SoundTaskInfo,
    &VoiceInfo,
    &ButtonTaskInfo,
    &PTT_TaskInfo,
    &BatteryTestInfo,
    &DebugTaskInfo,
    &AlarmPrioInfo,
    &COAlarmInfo,
    &CalManagerInfo,
    &COMeasureInfo,
    &TroubleManagerInfo,
    &RLedInfo,
    &GLedInfo,
    &ALedInfo,
    &LedBlinkInfo,
    &EscLightInfo,
    &PhotoSmokeInfo,
    &InterconnectInfo,
    &LightInfo,
    &AppStatusMonitorInfo,
};

#ifdef CONFIGURE_UNDEFINED
// Used for task Testing Only
const uchar Main_Mode[NUM_MODES][MAX_NUM_TASKS] =
{
    {
            TASKID_SOUND_TASK,
            TASKID_BUTTON_TASK,
            TASKID_PTT_TASK,
            TASKID_BATTERY_TEST,
            TASKID_ALARM_PRIORITY,
            TASKID_DEBUG_TASK,
            TASKID_COALARM,
            TASKID_CALIBRATE,
            TASKID_COMEASURE,
            TASKID_PHOTO_SMOKE,
            TASKID_TROUBLE_MANAGER,
            TASKID_RLED_MANAGER,
            TASKID_GLED_MANAGER,
            TASKID_ALED_MANAGER,
            TASKID_LED_BLINK_TASK,
            TASKID_INTERCONNECT,
            TASKID_ESCAPE_LIGHT,
            TASKID_VOICE_TASK,
            TASKID_LIGHT_TASK,
            TASKID_APP_STAT_MONITOR
    },

    {
            TASKID_COMEASURE,
            TASKID_PHOTO_SMOKE,
            TASKID_LIGHT_TASK
                 
    }
};


#else

const uchar Main_Mode[NUM_MODES][MAX_NUM_TASKS] =
{
    {
            TASKID_SOUND_TASK,
            TASKID_VOICE_TASK,
            TASKID_BUTTON_TASK,
            TASKID_PTT_TASK,
            TASKID_BATTERY_TEST,
            TASKID_ALARM_PRIORITY,
            TASKID_DEBUG_TASK,
            TASKID_COALARM,
            TASKID_CALIBRATE,
            TASKID_COMEASURE,
            TASKID_PHOTO_SMOKE,
            TASKID_TROUBLE_MANAGER,
            TASKID_RLED_MANAGER,
            TASKID_GLED_MANAGER,
            TASKID_ALED_MANAGER,
            TASKID_LED_BLINK_TASK,
            TASKID_INTERCONNECT,
            TASKID_ESCAPE_LIGHT,
            TASKID_LIGHT_TASK,
            TASKID_APP_STAT_MONITOR
    },

    {
            TASKID_COMEASURE,
            TASKID_PHOTO_SMOKE,
            TASKID_LIGHT_TASK
    }
};

#endif

#ifdef CONFIGURE_TEST_CODE
//
// Test Log array and array index
//
uchar PwrIndex = 0;
uchar PwrArray[25] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

uchar PwrSize = sizeof(PwrArray);


// Usage Example
// log_seq(1);

#endif



/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/
void interrupt main_interrupt_routine(void);
void main_initialize_powerup(void);
void main_check_revision(void);
void main_init_powerup(void);
void main_initialize(void);
void main_initialize_engine(void);

void main_wait_t1osc_stable(void);
void main_reset_sleep_timer(void);

void main_set_low_power(void);
void main_set_active(void);
void main_check_low_power_logic(void);

uint16_t main_flash_read_device_revision(void);

void main_PMD_init(void);

uint main_debug_task(void);

void main_clear_flags(void);

#ifdef CONFIGURE_SMK_SLOPE_SIMULATION
    void main_init_slope(void);
#endif
    
#ifdef CONFIGURE_DIAG_ACTIVE_FLAGS
    //
    // Handles Transmission of Active Flags Status (Diagnostic Tool only)
    //
    void Main_Diag_Active_Flags(void);   
#endif
    
#ifdef CONFIGURE_ACTIVE_TIMEOUT_TIMER
    void main_active_timeout_timer_rst(void);
    void main_active_mode_timer_update(void);
#endif

    
    


/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/

#ifdef CONFIGURE_TEST_CODE

void log_seq(uchar seqid);   
//
// Used to analyze DC/AC Power Race condition issue
//
// This code allows defined ID numbers to be Logged in sequence.
// Bread Crumb Pass-Thru points embedded in selected code routines/states.
//
// Used at Power Initialization and WiFi Initialization
// to log the sequence these IDs are executed.
// log_seq(ID) can be embedded at various points of the code
//  and later Sequence Log output via serial port using 'O' command
//
    
void log_seq(uchar seqid)
{
    if(PwrIndex < PwrSize)      // Currently max of 25 logs allowed
    {    
        PwrArray[PwrIndex] = seqid;
        PwrIndex ++;
    }
    
    return ;   
}

void log_out(void)
{
    //
    // Dump Sequence Log via serial port to see bread Crumb points.
    //  in sequence.
    //
    for(uchar i=0; i< PwrSize; i++)
    {
        SER_Send_String("\r");
        SER_Send_Int('s', i, 10);
        SER_Send_Int('-', PwrArray[i], 10);
    }
    SER_Send_Prompt();
    
    return;
}
#endif    

/*
+------------------------------------------------------------------------------
| Function:  void main_init_powerup(void)
+------------------------------------------------------------------------------
| Purpose:    Determine Type of Reset and Initialize Non-reset values 
|               for Power Resets
|             Then initialize Reset Flags
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
+------------------------------------------------------------------------------
*/

void main_init_powerup(void)
{
     // Test for Soft Reset (reset instruction)
    if(PCON0bits.nRI)
    {
        // Not a Soft Reset so perform Full Reset
        
        #ifdef CONFIGURE_UNDEFINED
            // Startup Code should clear these global flags
            //  so should not be needed (currently testing this to be sure)
            main_clear_flags();   
        #endif
        
        // Power Reset, clear accumulators & non-reset flags
        CO_Alarm_Init();
        
        // Initialize Battery Last Voltage measured value on Power Resets
        BAT_Init_Last_Voltage();
        
        // Re-Init CAV average since it was probably destroyed by Power Reset
        PHOTO_Init_CAV_Avg();
        
        // Init Trouble/Fault Mode Timers at Power Reset only
        SND_Init_Voice_Timer_Off();
        
        #ifdef CONFIGURE_LB_HUSH        
            SND_Init_LB_Timer_Off();
        #endif

        //Distinguish between WDT and Power resets
        if(0 == PCON0bits.nRWDT)
        {
            // Clear All Flags except Flags_NR3 on WDT Resets
            //  this preserves Wireless Disable if active
            Flags_NR.ALL = 0;
            Flags_NR2.ALL = 0;
        }
        else
        {
            // Clear All Flags at Power Resets, Brown Out or WDT Resets
            Flags_NR.ALL = 0;
            Flags_NR2.ALL = 0;
            Flags_NR3.ALL = 0;
        }
        
    #ifdef CONFIGURE_WIFI
        //
        // On Power Reset, Detect AC Power ASAP since WiFi module 
        // may be sending messages
        //
        if(TRUE == A2D_Test_AC_Sample())
        {
            FLAG_AC_DETECT = 1;
            FLAG_AC_WIFI_RST = 1;
            
            #ifdef CONFIGURE_TEST_CODE
                log_seq(7);
            #endif
            
        }
    #endif

    #ifdef CONFIGURE_WIRELESS_MODULE

        // If this is not a True Power Reset, (i.e. WDT, Brown Out, Stack Err)
        //  we should request COMM Status again since it will not be sent 
        //  automatically by RF Module.
        // At Power On Resets the RF Module automatically sends COMM Status
        //  so no need to request COMM status
        if(PCON0bits.nPOR)
        {
            // Treat this as a Fault, Send Fault Reset and COMM status will 
            //  also be requested again
            FLAG_RST_FAULT_AT_RST = 1;
            
           
            // If this is not a Power On Reset, we should request
            //  COMM Status info same as Soft resets since RF module may not
            //  have reset. (WDT reset or Brown out Reset)
            
            // Init Wireless Module to request COMM Status again
            //  but re-init Alarm Status variables
            APP_Init(APP_INIT_ACTION_NOT_PWR_RST);
        }
        else
        {
           
            // Initialize Comm Status and Status 1 to un-received values
            //  For a True Power Reset
            APP_Init(APP_INIT_ACTION_PWR_RST);

        }
        
        SND_Init_Fault_Timer_Off();
        
    #endif
        
        /*
         *
         * Power on with Smoke ALG Disabled
         * Set FLAG_BUTTON_ALG_ENABLED to initializes ALG 1 minute after
         *  Power ON. During normal operation after 1 Minute Init, these
         *  2 flags will both be ON or OFF
         *
         */
        FLAG_ALG_ENABLED = 0;
        FLAG_BUTTON_ALG_ENABLED	= 1;

        #ifdef CONFIGURE_PHOTO_SMOKE
            // Initialize CAV Average at Power On
            PHOTO_Init_CAV_Avg();
        #endif

        #ifdef CONFIGURE_LIGHT_SENSOR
            // Initialize the light data Structure on Power Reset
            LIGHT_Data_Init();
        #endif

        // Init Life Counter on Power Resets    
        LIFE_Init_Life_Count();
    }
    else
    {
        #ifdef CONFIGURE_WIFI
            //
            // For Soft Reset Check AC Power ASAP since WiFi module may 
            //  be sending messages
            //
            if(TRUE == A2D_Test_AC_Sample())
            {
                #ifdef CONFIGURE_TEST_CODE
                    log_seq(8);
                #endif

                FLAG_AC_DETECT = 1;
                FLAG_AC_WIFI_RST = 1;
                
                if(FLAG_WIRELESS_DISABLED)
                {
                    APP_Enable_WiFi_Port();
                }
            }
        #endif
        
        // Soft Reset
        #ifdef CONFIGURE_WIRELESS_MODULE
            // Init Wireless Module as needed
            APP_Init(APP_INIT_ACTION_SOFT_RST);
        #endif

        FLAG_RST_SOFT = 1;

    }
    
    // Re-init the Power Control bits after examining them
    PCON0bits.STKOVF = 0;
    PCON0bits.STKUNF = 0;
    PCON0bits.nRMCLR = 1;
    PCON0bits.nRWDT = 1;
    PCON0bits.nRI = 1;
    
    //
    // If Power Reset or Brownout Reset
    // Set DIAG Hist Power ResetRecord flag
    //
    if((BIT_CLR == PCON0bits.nPOR) || (BIT_CLR == PCON0bits.nBOR ))
    {
        FLAG_DIAG_PWR_RESET = 1;
    }
    
    // Brown Out Flag and Power Reset are recorded outside this routine    
    PCON0bits.nBOR = 1;
    PCON0bits.nPOR =1;
 
}

//
// Initialize 1 ms Timer Clock Source
//
void main_T4_Init_Clk(uchar clk)
{
    switch(clk)
    {
        case T4_CLK_SOSC:
        {
            // Init the 1 MS Main Interrupt Timer
            T4CLKCON = TMR4_CLOCK_CON_INIT_1;
            T4CON = TMR4_CON_INIT_1;
            TMR4 = TIME_1_MS_1;
            main_t4_select = T4_CLK_SOSC;
        }
        break;
        
        case T4_CLK_HFOSC:
        {
            // Init the 1 MS Main Interrupt Timer
            T4CLKCON = TMR4_CLOCK_CON_INIT_2;
            T4CON = TMR4_CON_INIT_2;
            TMR4 = TIME_1_MS_2;
            main_t4_select = T4_CLK_HFOSC;
        }
        break;
 
        default:
        {
            // Init the 1 MS Main Interrupt Timer
            T4CLKCON = TMR4_CLOCK_CON_INIT_1;
            T4CON = TMR4_CON_INIT_1;
            TMR4 = TIME_1_MS_1;
        }
    }
}


/*
+------------------------------------------------------------------------------
| Function:  int main( void )
+------------------------------------------------------------------------------
| Purpose:    main() routine
|             Program Start entry and task management
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
+------------------------------------------------------------------------------
*/
int main(void) 
{
    unsigned int idx = 0;       // task table index
    unsigned int interval;

    GLOBAL_INT_ENABLE = 0;    
                            
                            
    #ifdef CONFIGURE_DBUG_SERIAL_RESET_STATUS
        // Saved for serial Port Output
        main_pcon_save = PCON0;
        main_status_save = STATUS;
        
        #ifdef CONFIGURE_WDT_RST_FLAGS
            main_wdt_flags_save = Flags_WDT_Flags.ALL;
            
            // Reset WDT Flags with each reset after saving them
            Flags_WDT_Flags.ALL = 0;

        #endif
    #endif

    // Initialize Data Structure from non-Volatile Flash
    if(SYS_Init())
    {
       FLAG_MEM_ERR = 1;
    }
        
    // Init non-reset data if Power Reset
    main_init_powerup();

    // Init the WatchDog Timer
    #ifdef CONFIGURE_WATCHDOG_TIMER_OFF
        WDTCON0bits.SWDTEN = 0;
    #else
        // Cleared when WDTCON written to
        WDTCON0 = MAIN_WDT_STARTUP_TIMEOUT;
        WDTCON0bits.SWDTEN = 1;
    #endif

    FLAG_RESET = 1;

    #ifdef CONFIGURE_LBAT_CAL_ENABLE
        // Test Low Battery Cal Status from Data Structure
        BAT_LB_Cal_Init();
    #endif

    #ifdef CONFIGURE_SMK_SLOPE_SIMULATION
        main_init_slope();
    #endif

        
    // Initialize I/O
    main_initialize();
    
    // Log as Power Reset?
    if(FLAG_DIAG_PWR_RESET)
    {
        FLAG_DIAG_PWR_RESET = 0;      // Reset PowerOn Flag
       
        #ifdef  CONFIGURE_DIAG_HIST
            // Production Firmware does not record
            // Power Reset if serial Port is connected; allows history to
            // be downloaded without adding another event..
            #ifdef CONFIGURE_PRODUCTION_UNIT
                if(!FLAG_SERIAL_PORT_ACTIVE)
                {
                    // Record Diagnostic History Power Reset
                    DIAG_Hist_Que_Push(HIST_POWER_RESET);
                }
            #else
                // Record Diagnostic History Power Reset
                DIAG_Hist_Que_Push(HIST_POWER_RESET);
            #endif
        #endif
    }

    #ifdef CONFIGURE_LBAT_CAL_ENABLE
        //Test the LB Cal Signal to set LB Cal Mode

        TRISBbits.TRISB7 = 1;   // Set LB Cal input
        if(PORT_LB_CAL_PIN)
        {
            // LB Cal Process Enabled
            FLAG_LB_CAL_SIGNAL = 1;
            FLAG_LOW_BATTERY_CAL_MODE = 1;

            RLED_On();  //Immediately Signal LB Cal

        }
        else
        {
            if(FLAG_LB_CAL_STATUS)
            {
                //LB Already calibrated
                TRISBbits.TRISB7 = 0;   // Set LB Cal back to output
            }
            else
            {
                //LB needs calibration
                FLAG_LOW_BATTERY_CAL_MODE = 1;

            }
        }

    #endif

    #ifdef CONFIGURE_DEBUG_PIN_ENABLED
        // Test Only
        MAIN_TP_On();   // start in Active Mode
        MAIN_TP2_Off();
    #endif

    #ifndef CONFIGURE_WIFI   
        // Moved to Wireless APP module in WiFi
        #ifdef CONFIGURE_WIRELESS_MODULE   
            if(FLAG_APP_SEND_ALM_STATUS)
            {
                FLAG_APP_SEND_ALM_STATUS = 0;
                // At Soft Resets reset Alarm Status
                APP_Update_Status();

                // And request Comm Status
                APP_Request_Comm_Status();

                // And Re-Init Status 1
                APP_Request_Status1();
            }
        #endif
    #endif
        
    LIFE_Init();   // Verify Valid Life Checksum and Life Expiration
    
    
    //	Main Task Process/Sleep Loop
    while(1)
    {
        if( (main_tic_timer -
              TaskTable[ Main_Mode[main_current_mode][idx] ]->TaskTime) >=
             (TaskTable[ Main_Mode[main_current_mode][idx] ]->TaskInterval) )
        {

            // task timer has expired
            #ifdef CONFIGURE_DEBUG_TASK_TIMING
                MAIN_TP_On();
            #endif

            interval = 
                    TaskTable[ Main_Mode[main_current_mode][idx] ]->funcptr();


            #ifdef CONFIGURE_DEBUG_TASK_TIMING
                MAIN_TP_Off();
            #endif

            // Always Set Returned interval and update Task Time
            TaskTable[Main_Mode[main_current_mode][idx]]->TaskInterval =
                                                        interval;

            TaskTable[Main_Mode[main_current_mode][idx]]->TaskTime =
                                                        main_tic_timer;

        }

        idx++;

        if(idx > TaskNumbers[main_current_mode] - 1)
        {
            // Reset Task pointer and Bump TIC Time
            idx = 0;
            main_tic_timer++;

            // ****************************************************************
            // Put anything here that has to happen at the end of all the tasks

            // Update any Global system timers
            // i.e - b1SecTimer, bOneMinuteTimer... etc

            if(FLAG_MODE_ACTIVE)
            {
                
                #ifdef CONFIGURE_WIFI    
                    // If Active Mode, Monitor AC Power more frequently in 
                    // WiFi Models - Test and Update Power Status 
                    A2D_Test_AC();
                #endif

                // At reset we begin in Active mode, so clr the Reset Flag here
                FLAG_RESET = 0; // Clear Reset Flag after 1st task Init.

                if(FLAG_SERIAL_PORT_ACTIVE)
                {
                    // Check the Serial Port for input
                    SER_Process_Rx();

                    #ifdef CONFIGURE_SERIAL_QUEUE
                        // Check Queues for Serial Data to send
                        //   but wait if Voice is transmitting
                        SER_Xmit_Queue();

                    #endif

                #ifdef CONFIGURE_DIAG_ACTIVE_FLAGS
                    if(TRUE == SER_Queue_Empty())   
                    {
                        //    
                        // Disable Serial Port for Low Power Mode transition
                        //
                        if(FLAG_SERIAL_PORT_OFF)  
                        {
                            //
                            // Only Serial Port is Enabled
                            // End Tx, Turn it Off and Allow Low Power Mode
                            //
                            SER_Off();
                            FLAG_SERIAL_PORT_OFF = 0;
                        }
                    }
                #endif

               }
                
                #ifdef CONFIGURE_DIAG_ACTIVE_FLAGS
                    // Enable Diag Active Tx on next transition into Active Mode
                    if(FLAG_DIAG_ACTIVE_RST)
                    {
                        if(main_diag_sec_time)
                        {
                            SER_Send_String("\r\rLP Time ");
                            SER_Send_Int(' ',main_diag_sec_time,10);
                            SER_Send_String(" Secs");
                            
                            // Reset LP Timer
                            main_diag_sec_time = 0;
                        }
                         
                        Main_Diag_Active_Flags();   // Test for Diag Tx
                    }
                #endif
                

                // ACTIVE TIMER: Sets a minimum Active Mode Time(10 ms units)
                if(0 == MAIN_ActiveTimer)
                {
                    // Allow Active Mode to end
                    FLAG_ACTIVE_TIMER = 0;
                    
                    #ifdef CONFIGURE_DIAG_ACTIVE_FLAGS
                        // Turn Off CCI Active (This added for Diagnostics)
                        FLAG_CCI_ACTIVE = 0;
                    #endif
                }
                else
                {
                    // countdown to LP Mode enable
                    MAIN_ActiveTimer--;
                }

                if(0 == --main_100_ms_count)
                {
                    // Update 100 ms Timer
                    #ifdef	CONFIGURE_INTERCONNECT
                        #ifdef CONFIGURE_WIRELESS_MODULE
                            MAIN_100_MS_Timer ++;
                        #endif
                    #endif
                    main_100_ms_count = TIMER_100_MS_ACTIVE_COUNT;
                    
                    
                    #ifdef CONFIGURE_DIAG_ACTIVE_FLAGS
                        // Update Diag Active Timer if not Timed Out
                        if(main_diag_active_timer)
                        {
                            main_diag_active_timer--;
                        }
                        else
                        {
                            // Enable Next Diag Active Flags Tx
                            FLAG_DIAG_ACTIVE_RST = 1;
                        }
                    #endif
                    
                }
                
                // based on 10 ms Tics
                if(0 == --main_sec_count)
                {
                    CLRWDT();
                    MAIN_OneSecTimer ++;
                    main_sec_count = TIMER_1_SEC_ACTIVE_COUNT;
                    
                    #ifdef CONFIGURE_ACTIVE_TIMEOUT_TIMER
                        // If Active Mode Timeout Timer Test Flag is enabled
                        if(FLAG_ACTIVE_TIMEOUT_TEST)
                        {
                            FLAG_ACTIVE_TIMEOUT_TEST = 0;
                            main_active_mode_timer_update();
                        }
                    #endif
                    
                    
                    #ifdef CONFIGURE_WIRELESS_MODULE
                     #ifdef CONFIGURE_INTERCONNECT
                      #ifdef CONFIGURE_OUTPUT_GATEWAY
                        if(0 == (MAIN_OneSecTimer & 0x07) )
                        {
                            // Run every 8 seconds
                            if(FLAG_GATEWAY_SLV_TO_REMOTE_OFF)
                            {
                                SER_Send_String("S-to-R Gateway OFF");
                                SER_Send_Prompt();
                            }
                            else
                            {
                                SER_Send_String("S-to-R Gateway ON");
                                SER_Send_Prompt();
                            }

                            if(FLAG_GATEWAY_REMOTE_TO_SLAVE)
                            {
                                SER_Send_String("R-to-S Gateway ON");
                                SER_Send_Prompt();
                            }
                            else
                            {
                                SER_Send_String("R-to-S Gateway OFF");
                                SER_Send_Prompt();
                            }
                            
                        }
                      #endif
                     #endif
                    #endif

                    
                    #ifdef CONFIGURE_DIAG_TIMER
                        /*
                         * If this Timer is Active, output Timer value every 
                         * second. This is to aid in syncing Serial Port Events
                         * during debugging.
                         * 
                         * Trigger event must call MAIN_Init_Diag_Timer
                         * Timer times out after 60 seconds
                         */
                        if(FLAG_DIAG_TIMER)
                        {
                            main_diag_timer++;
                            SER_Send_Prompt();
                            SER_Send_Int(' ',main_diag_timer,10);
                            SER_Send_Prompt();
                            if(TIME_DIAG_TIMER_60_SEC == main_diag_timer)
                            {
                                FLAG_DIAG_TIMER = 0;
                            }
                        }
                    #endif                    
                    
                    #ifdef CONFIGURE_MONITOR_INT
                        // Test Code only
                            SER_Send_String("Test INT");
                            SER_Send_Int('1',MAIN_Monitor_Int1,10);
                            SER_Send_Prompt();

                            SER_Send_String("test int");
                            SER_Send_Int('2',MAIN_Monitor_Int2,10);
                            SER_Send_Prompt();
                    #endif

                    FLAG_BAT_TEST_DLY = 0;

                    #ifdef CONFIGURE_ACTIVE_TEST
                            // Test Only (Output Active Flags)
                            SER_Send_Int(' ', *( (uint*)&Flags_Active1), 16);
                            SER_Send_Char(',');
                            SER_Send_Int(' ', *( (uint*)&Flags_Active2), 16);
                            SER_Send_Prompt();
                            SER_Send_Int(' ', *( (uint*)&Flags_Active3), 16);
                            SER_Send_Prompt();
                    #endif

                    #ifndef CONFIGURE_WIFI        
                        #ifdef CONFIGURE_ACDC
                            // Test and Update Power Status every second
                            A2D_Test_AC();
                        #endif
                    #endif

                    #ifdef CONFIGURE_DBUG_STACK_TEST
                            // Test Only - Monitor Peak Stack Pointer
                            MAIN_Stack_Ptr_Now = STKPTR;
                            SER_Send_Int('k', (uint)MAIN_Stack_Ptr_Now, 16);
                            SER_Send_Prompt();

                            SER_Send_Int('K', (uint)MAIN_Stack_Ptr_Peak, 16);
                            SER_Send_Prompt();
                    #endif

                    if(FLAG_T1_XTAL_NOT_STABLE) 
                    {
                        // Update XTAL not stable Timer
                        // This only runs the 1st 12 seconds after a reset
                        //  in active mode.
                        if(0 == main_xtal_osc_timer--)
                        {
                            // XTAL OSC Timer has expired, transition T1 to 
                            //  32 KHZ XTAL Oscillator and test if XTAL stable
                            OSCEN = OSC_ENABLE_SOSC_SELECT;
                            T1CLK = T1_CLK_INIT_SOSC;

                            #ifndef CONFIGURE_SIMULATOR_DEBUG
                                // Timers 0 and 1 should already be initialized 
                                //  before this call
                                main_wait_t1osc_stable();
                            #endif

                            #ifdef CONFIGURE_STROBE_1_MS_TASK    
                                // Transition to SOSC for 1 MS 
                                //  Main Interrupt Timer
                                main_T4_Init_Clk(T4_CLK_SOSC);  
                            #endif

                            // Time to Verify if 32KHz Crystal is stable
                            FLAG_T1_XTAL_NOT_STABLE = 0;

                            #ifndef CONFIGURE_UNDEFINED
                                SER_Send_String("32Khz XTAL Stable ");
                                
                                #ifdef CONFIGURE_XTAL_SERIAL 
                                    SER_Send_Int(' ',main_xtal_count, 10);
                                #endif
                                SER_Send_Prompt();
                            #endif
                        }
                    }
                }
            }
            else
            {
                //*************************************************************
                // We're in Low Power Mode
                //  which is based on 1 Second Tics
                MAIN_OneSecTimer ++;
                CLRWDT();
                
                #ifdef CONFIGURE_DIAG_ACTIVE_FLAGS
                    ++main_diag_sec_time;
                #endif
                        
                FLAG_BAT_TEST_DLY = 0;

            #ifdef CONFIGURE_ACDC
                #ifndef CONFIGURE_WIFI
                    // Test and Update Power Status every second
                    //A2D_Test_AC();
                #else
                    if(A2D_Test_AC_Sample())
                    {
                        // AC Power detected - enter Active on next wake...
                        FLAG_ACTIVE_TIMER = 1;
                        if(MAIN_ActiveTimer < TIMER_ACTIVE_MINIMUM_2_SEC)
                        {
                            // Extend time to remain in Active Mode
                            MAIN_ActiveTimer = 
                                    TIMER_ACTIVE_MINIMUM_2_SEC;
                        }
                    }
                #endif
            #endif

                // 5 Second tasks    
                if(COUNT_ZERO == --main_five_sec_count)   
                {
                    main_five_sec_count = TIMER_5_SECOND_COUNT;
                    
                    // Amber Task should be able to be serviced here every 
                    //  5 seconds in LP Mode. since 5 Sec Low Battery and Fault
                    //  are the only Amber Low Power modes.
                    ALED_Manager_Task();
                    
                    
                    // Check Sound Task processing
                    if(FLAG_EOL_MODE || FLAG_FAULT_FATAL || 
                       FLAG_LOW_BATTERY || FLAG_NETWORK_ERROR_MODE)
                    {
                        // Don't Service every 5 seconds if Error or Trouble
                    }
                    else
                    {
                        // 5 second task if not in error or trouble
                        SND_Trouble_Manager_Task();
                    }
                }

                /*
                 * Here we can perform other Low Power Mode tasks
                 * not included in the Task engine table (to save power)
                 * i.e
                 * 30 Second Task Calls
                 * 60 Second Task Calls
                 */
                   
                if(COUNT_ZERO == --main_thirty_sec_count)
                {
                    /*
                     * Reset Low Power 30 second Timer
                     * Some Low Power Mode tasks require a transition to 
                     *  Active Mode to execute.
                     */
                    main_thirty_sec_count = TIMER_30_SECOND_COUNT;

                    // Call 30 Second tasks this wake cycle in LP Mode
                    #ifdef CONFIGURE_CO
                        CO_Alarm_Task();
                    #endif

                    // Check Sound Task processing
                    if(FLAG_EOL_MODE || FLAG_FAULT_FATAL || 
                       FLAG_LOW_BATTERY || FLAG_NETWORK_ERROR_MODE)
                    {

                        // 30 second task if in error or trouble
                        SND_Trouble_Manager_Task();
                    }
                    else
                    {
                        // If no Error or Trouble don't service every 30 secs                        
                    }

                    if(FLAG_LP_60_SEC)
                    {
                        // 60 Second tasks this wake cycle in LP Mode
                        FLAG_LP_60_SEC = 0;
                        
                        #ifdef CONFIGURE_UNDEFINED
                            // Test Only
                            MAIN_TP2_On();
                        #endif       
                            
                        // Monitor Green Led blink timer
                        if(COUNT_ZERO == --main_lp_blink_timer)
                        {
                           // Every 60 seconds in LP Mode
                            main_lp_blink_timer = MAIN_GRN_BLINK_1_MIN;  
                            
                            /*
                             * If Low Battery Trouble or Faults active, then
                             *  Halt LP Green Blinks since Amber Blinks 
                             *  are blinking every 5 seconds.
                             */
                            if(!FLAG_LOW_BATTERY && !FLAG_LB_HUSH_ACTIVE &&
                               !FLAG_EOL_MODE && !FLAG_EOL_HUSH_ACTIVE &&
                               !FLAG_EOL_FATAL && !FLAG_FAULT_FATAL &&
                               !FLAG_NETWORK_ERROR_HUSH_ACTIVE &&
                               !FLAG_NETWORK_ERROR_MODE )
                            {
                                LED_LP_green_blink();
                            }
                        }

                        // Update LP Mode Battery Test Timer
                        if(BAT_Check_LP_Mode_Timer() )
                        {
                            // Unit should transition to Active Mode
                            //  and Battery Test performed immediately

                            // Time to Run Battery Test task
                            FLAG_LP_BATTERY_TEST = 1;   
                        }

                        #ifdef CONFIGURE_APP_WIRELESS_SUPERVISION
                          #ifndef CONFIGURE_WIFI
                            // No WiFI or CCI in Low Power Mode (DC Only)
                            
                            // No need for CCI supervision if Wireless Disabled
                            if(!FLAG_WIRELESS_DISABLED)
                            {
                                // Update/Process LP Mode CCI supervision Timer
                                APP_LP_CCI_Timer();
                            }
                            
                          #endif
                        #endif
                    }
                    else
                    {
                        FLAG_LP_60_SEC = 1;
                        
                        #ifdef CONFIGURE_UNDEFINED
                            // Test Only
                            MAIN_TP2_Off();
                        #endif       
                    }
                }
            }

            if(main_one_minute_count == MAIN_OneSecTimer)
            {
                MAIN_OneMinuteTic ++;
                
                #ifdef CONFIGURE_ACTIVE_TIMEOUT_TIMER
                    main_active_mode_timer_update();
                #endif
                
                if(TIMER_5_MINUTES <= ++main_sys_validate_count)
                {
                    main_sys_validate_count = 0;
                    
                    /*
                     * Validate Data Structure Checksum every minute
                     * Not only does this supervise for Data Structure
                     *  corruption, but it prevents writing a corrupt structure
                     *  back into Non-Volatile memory copies with LFE_Check
                     *  below.  make sure LFE_Check() is after this validation.
                     */
                    if(FALSE == SYS_Validate_Data_Structure())
                    {
                        // Reset to restore Data structure and Test for
                        //  Memory Errors
                        SOFT_RESET
                    }
                }
                
                #ifdef CONFIGURE_UNDEFINED
                    // Test Only
                    SER_Send_String("    Minute TIC ");
                    SER_Send_Prompt();
                #endif
                
                main_one_minute_count = (MAIN_OneSecTimer +
                                        (uchar)TIMER_ONE_MINUTE_COUNT);

                #ifdef CONFIGURE_ID_MANAGEMENT
                    if(FLAG_MODE_ACTIVE)
                    {
                        MAIN_Output_Unit_ID();
                    }
                #endif

                #ifdef CONFIGURE_CO
                  #ifdef CONFIGURE_TEST_CO_AMBIENT
                     // For Testing only
                     ALG_CO_Ambient_Compensate();
                  #endif
                #endif

                #ifdef CONFIGURE_LB_HUSH
                    // Timers to Run Every Minute
                    BAT_Check_LB_Hush_Timer();
                #endif

                #ifdef CONFIGURE_WIRELESS_MODULE
                    FLT_Check_Error_Hush();     // Test 24 Hr Error Hush Timer
                    
                    #ifdef CONFIGURE_UNDEFINED
                        if(!FLAG_WIRELESS_DISABLED)
                        {
                            // Test only, output current RF ID
                            SER_Send_String("RF Logical ID = ");
                            SER_Send_Int(' ',APP_Get_Local_Id(),10);
                            SER_Send_Prompt();
                            
                            // Test only, output current # of devices
                            SER_Send_String("Devices  = ");
                            SER_Send_Int(' ',APP_Rf_Network_Size,10);
                            SER_Send_Prompt();
                            
                        }
                    #endif
                #endif

                #ifdef CONFIGURE_TEST_SMK_COMP
                    //
                    // 1 Minute Accelerated CAV Averaging (normally every hour)
                    //
                    PHOTO_Compute_CAV_Avg();
                    
                    //
                    // For accelerated testing use 24 minutes instead of 
                    // 24 hours to execute Alarm Compensation
                    //
                    if(0 == --Timer_Test_Alm_Comp)
                    {
                        Timer_Test_Alm_Comp = 24;
                        PHOTO_Compensate_Alm_Thold();
                    }
                #else
                    //
                    // Call the clean air compensation routine every
                    //   minute for accelerated Initialization
                    //   This is only active, when chamber dust cleaning
                    //   has been detected.
                    //
                    if(FLAG_INIT_CAV_AVG)
                    {
                        PHOTO_Compute_CAV_Avg();
                    }
                #endif

                #ifdef CONFIGURE_ACCEL_LIGHT_TEST
                    // Hour Record every minute, therefore, 1 Day = 24 minutes
                    #ifdef CONFIGURE_LIGHT_SENSOR
                        LIGHT_Record();
                    #endif
                #endif

                /*
                 * If Init Algorithm condition is set,
                 *  (FLAG_BUTTON_ALG_ENABLED = 1 and FLAG_ALG_ENABLED = 0)
                 *  then enable the Algorithm. This condition should only
                 *   occur after Power On Reset
                 */
                if(FLAG_BUTTON_ALG_ENABLED) // If Button ALG Flag is enabled
                {
                    FLAG_ALG_ENABLED = 1;   // Algorithm should be ON
                }

                // Update Hour Count every Minute
                if(COUNT_ZERO == --main_hour_count)
                {
                    // Bump Hour TIC time
                    MAIN_OneHourTic++;
                    main_hour_count = TIMER_ONE_HOUR_COUNT;

                    // Hourly tasks
                    #ifdef CONFIGURE_LIGHT_SENSOR
                        LIGHT_Record();
                    #endif

                    #ifdef CONFIGURE_CO
                      #ifndef CONFIGURE_TEST_CO_AMBIENT
                          ALG_CO_Ambient_Compensate();
                      #endif
                    #endif

                    // Normally, Call the clean air compensation routine
                    //  every hour.
                    PHOTO_Compute_CAV_Avg();

                }

                // Update and Test life counter every minute
                LIFE_Check();

                #ifdef CONFIGURE_BUTTON_ALG_ENABLE
                    // Test AlG OFF from Button Timer
                    BTN_Chk_ALG_Off_Timer();
                #endif

            }
            
            
            #ifdef CONFIGURE_TEST_SMK_COMP
                //
                // Accelerated Initialization Drift Comp Alarm
                //
                if(Timer_Test_Init_Comp == MAIN_OneSecTimer)
                {
                    // Every 10 seconds
                    Timer_Test_Init_Comp = (MAIN_OneSecTimer + 10);
                    //
                    // Call the clean air compensation routine every
                    //   10 seconds for accelerated Initialization
                    //   This is only active, when chamber dust cleaning
                    //   has been detected.
                    //
                    if(FLAG_INIT_CAV_AVG)
                    {
                        PHOTO_Compute_CAV_Avg();
                    }
                }
            #endif
            

            // If a command has been received, send to command processor
            if(FLAG_SERIAL_PORT_ACTIVE && FLAG_RX_COMMAND_RCVD)
            {
                FLAG_RX_COMMAND_RCVD = 0;
                CMD_Process_Command();
            }


            // *****************************************************************
            // *****************************************************************
            // 	All tasks have finished.  Set wakeup time & then sleep
            // *****************************************************************
            // *****************************************************************

            if(FLAG_POWERUP_DELAY_ACTIVE)
            {
                // This code runs in Active Mode Timing Only as 
                //   FLAG_POWERUP_DELAY_ACTIVE is an Active Mode Flag.

                main_powerup_awake_timer++;
                
              
                if(MAIN_PWR_DELAY_TIME < main_powerup_awake_timer)
                {

                    FLAG_POWERUP_DELAY_ACTIVE = 0;

                    if(!FLAG_INHIBIT_REV_MSG)
                    {
                        main_check_revision();
                    }

                }

                // Use Active Mode ms Tic Timer during Power Up delay
                do
                {
                    //WDT will expire if Timer 1 hangs (xtal short)

                }while(!PIR4bits.TMR1IF);

            }
            else
            {
                if(FLAG_MODE_ACTIVE)
                {
                    // *********************************************************
                    // ****************** Normal Active Mode *******************
                    // *********************************************************
                    // *** We're in Normal Operation Active Mode

                    #ifdef CONFIGURE_DEBUG_WAKE_SLEEP_TIMING
                        MAIN_TP_On();       // TP always On in Active Mode
                    #endif

                    // Wait for Active Mode Tic to expire Here (10 ms TICs)
                    // We're not sleeping so Ints should be enabled and running
                    do
                    {
                        //WDT will expire if Timer 1 hangs (xtal short)

                    }while(!PIR4bits.TMR1IF);


                    // *********************************************************
                    // **************** END Normal Active Mode *****************
                    
                    #ifdef CONFIGURE_DEBUG_WAKE_SLEEP_TIMING
                        MAIN_TP_On();       // TP On in Active Run Mode
                    #endif
                }
                else
                {
                    // *********************************************************
                    // ******************** Low Power Mode *********************
                    // *********************************************************
                    // *** We're in Normal Operation Low Power Sleep Mode


                    // Turn Off AD during Sleep
                    A2D_Off();

                    // Turn off Brown Out (saves current)
                    BORCONbits.SBOREN = 0;
                    // Insure Turned off FVR to save power
                    FVRCONbits.FVREN = 0;

                    #ifdef CONFIGURE_DEBUG_WAKE_SLEEP_TIMING
                        MAIN_TP_Off();      // TP Off during Sleep Time
                    #endif

                    #ifdef CONFIGURE_DBUG_HANG_IN_SLEEP
                        //*****************************************************
                        // Test Only - Hang in Sleep (for current measure)
                        // Turn off sleep Timer to Lock into Sleep
                        T1CONbits.TMR1ON = 0;   // Timer1 Off
                        WDTCON0bits.SWDTEN = 0; // Turn WDT off for this test

                        // Insure Serial Port is Off
                        RCSTAbits.SPEN = 0;
                        TXSTAbits.TXEN = 0;

                        // Voice Chip Power Off and no Voltage on Voice Inputs
                        PORT_VOICE_EN_PIN = 0;      
                        LAT_VOICE_CLK_PIN = 0;      
                        LAT_VOICE_DATA_PIN = 0;


                        // Sleep Mode - T1 Int Flag used to wake from sleep
                        PIE4bits.TMR1IE = 0;
                        PIR4bits.TMR1IF = 0;
                        PERIPERAL_INT_ENABLE = 0;

                        // Turn Timer 4 Off (1 ms Timer)
                        T4CONbits.TMR4ON = 0;
                        
                        // Disable 1MS Timer Ints
                        TIMER_ONE_MS_INT_ENAB = 0;
                        TIMER_ONE_MS_INT_FLAG = 0;

                        // Timers Off
                        T0CON0bits.T0EN = 0;
                        T6CONbits.T6ON = 0;

                        CCP1CONbits.CCP1EN = 0;
                        CCP4CONbits.CCP4EN = 0;

                        IOCBN = 0b00000000;     // No IOC
                        IOCBP = 0b00000000;     // No IOC

                        // Turn Off WPUs
                        WPUB = 0x00;   
                        WPUA = 0x00;
                        WPUC = 0x00;
                        WPUD = 0x00;
                        WPUE = 0x00;

                        // Clock and DATA pins output Low
                        mCCI_DATA_PIN_DIR = 0;
                        mCCI_CLOCK_PIN_DIR = 0;
                        LAT_CCI_DATA_PIN = 0;            
                        LAT_CCI_CLK_PIN = 0;    

                        //Disable Scanner and CRC Modules
                        PMD0 = 0x18;        
                        PMD0 = 0xFF;        //Disable All

                        //Disable Timers 3,5 and Numeric Controlled OSC
                        PMD1 = 0xA8;        
                        PMD1 = 0xFF;        //Disable All

                        //Disable DAC, Zero Cross Detect, and Comparators
                        PMD2 = 0x47;        
                        PMD2 = 0xFF;        //Disable All

                        //Disable PWM7, PWM6 and CCP5, CCP3, CCP2
                        PMD3 = 0x76;        
                        PMD3 = 0xFF;        //Disable ALL

                        //Disable MSSP1,2  and CWG1,2,3
                        PMD4 = 0x37;        
                        PMD4 = 0xFF;        //Disable ALL

                        //disable SMT2,1 and CLC4,3,2,1 and DSM
                        PMD5 = 0xFF;        


                        // Then Sleep Forever

                        //*****************************************************
                    #endif
                        
                    #ifdef CONFIGURE_WIFI
                        //
                        // Has Active Timer been enabled?  (i.e. AC Detect))
                        // If so, then do not sleep - we want to continue to 
                        // enter Active Mode now
                        //
                        if(!FLAG_ACTIVE_TIMER)
                        {
                            // See if Packet was received since last wake time
                            // If yes, then do not sleep
                            if(!FLAG_CCI_EDGE_DETECT)
                            {
                                // ---------------------------------------------
                                GOTOSLEEP
                                // ---------------------------------------------
                            }
                        }
                    #else
                        // See if Packet was received since last wake time
                        // If yes, then do not sleep
                        if(!FLAG_CCI_EDGE_DETECT)
                        {
                            // -------------------------------------------------
                            GOTOSLEEP
                            // -------------------------------------------------
                        }
                    #endif

                    #ifdef CONFIGURE_DEBUG_WAKE_SLEEP_TIMING
                        MAIN_TP_On();      // TP On during Wake Time
                    #endif

                    // Turn On AD during Run Time
                    //  (this saves some time during some A/D measurements)
                    A2D_On();

                    // Turn on Brown Out
                    BORCONbits.SBOREN = 1;

                    // Can Determine sources of Wake here
                    // Ints are disabled in LP Mode, but Int Flags are still
                    //    active and will wake from sleep when set.

                    // First! Check the CCI to see if a packet
                    //  needs to be received or has been received

                    // ISR tests and handles packet reception, and interrupt
                    // source polling

                    // 1. Int. on Change Edge, CCI
                    // 2. Look for WDT Timeout (nTO bit == 0)
                    if(BIT_CLR == STATUSbits.nTO)
                    {
                        #ifdef CONFIGURE_UNDEFINED
                            // Test Only
                            SER_Send_String("WDT"); 
                            SER_Xmit_Queue();
                        #endif
                        
                        HORN_ON                 // Turn on Horn
                        while(1);              // Wait here for WDT reset

                    }

                    // 3. Int. on Change Edge, Button
                    // 4. Int. on Change Edge, INT IN
                    // These should have been set in the INT Routine

                    // Test Button Edge Detect
                    // Filter transients on button inputs (ESD)
                    //  as a short transient will wake briefly.
                    if(FLAG_BUTTON_EDGE_DETECT)
                    {
                        PIC_delay_ms(50);
                       if(LOGIC_ONE != PORT_TEST_BTN_PIN)
                       {
                           // This must have been a transient, clear
                           //   Button Edge Detect
                            FLAG_BUTTON_EDGE_DETECT = 0;
                       }
                    }

                    // 5. If none of the Above, then 
                    //    Wake must be because of Sleep Timer (default)

                    // *********************************************************
                    // ******************** END Low Power Mode *****************

                }
            }

            // Here we have completed Tic times
            //    (either by wake from sleep or simulated sleep timeout
            // 1. Reset the Sleep Timer
            // 2. Check for Low Power / Active Mode Change
            // 3. WDT control if needed

            // Test for need to Change to Active or Standby
            main_check_low_power_logic();

            // Reset Sleep Timer for next TIC depending upon Mode
            main_reset_sleep_timer();

            #ifdef CONFIGURE_WATCHDOG_TIMER_OFF
                WDTCON0bits.SWDTEN = 0;
            #else
                // Reset WDT each Wake from Sleep
                // WDT Automatically Cleared on Wake from Sleep, so this
                //  should not be required
                #ifdef CONFIGURE_UNDEFINED
                    CLRWDT();
                #endif
            #endif

        }   // if(i > TaskNumbers[CurrentMode] - 1)

   }	//end of Engine Task Process/Sleep Loop

}   // end of Main


/*
+------------------------------------------------------------------------------
| Function:  void MAIN_Reset_Task(UCHAR taskID)
+------------------------------------------------------------------------------
| Purpose:    Reset task for immediate execution
|
|
+------------------------------------------------------------------------------
| Parameters:    Task_ID  (0-255)  Task ID number to reset
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|                Task Time and Task Interval are updated
|
+------------------------------------------------------------------------------
*/
void MAIN_Reset_Task(uchar taskID)
{
    // To reset the task, set the time to the current time and
    // set the interval to 0.  This will cause the task to execute
    // on the next tic.
    TaskTable[taskID]->TaskTime = main_tic_timer;
    TaskTable[taskID]->TaskInterval = 0;
}


/*
+------------------------------------------------------------------------------
| Function:  unsigned int DebugTask(void)
+------------------------------------------------------------------------------
| Purpose:    This is a dummy task for testing only
|
|
+------------------------------------------------------------------------------
| Parameters:    Task_ID  (0-255)  Task ID number to reset
|
+------------------------------------------------------------------------------
| Return Value:  Task Return Time (for next time task is to be executed)
|                Currently returns 30 seconds in Tic Times
|
|
+------------------------------------------------------------------------------
*/
unsigned int main_debug_task(void)
{
    return MAIN_Make_Return_Value(MAIN_INTERVAL_30_SECS);
}


/*
+------------------------------------------------------------------------------
| Function:  void Main_Check_Revision(void)
+------------------------------------------------------------------------------
| Purpose:    Output Revision Information via Serial Port if configured
|
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|              Revision, Model and status information output the Serial Port
|               if configured to do so.
|
|
+------------------------------------------------------------------------------
*/
void main_check_revision(void)
{
    
#ifdef CONFIGURE_WIFI
    #ifdef CONFIGURE_WIRELESS_MODULE
        if((!FLAG_WIRELESS_DISABLED) && (FLAG_AC_DETECT))
        {
            if(0 == APP_RF_Module_FW_Version)
            {
                 // Request FW version
                 APP_Request_RF_Version();
            }
        }
    #endif
#else    
    #ifdef CONFIGURE_WIRELESS_MODULE
        if(!FLAG_WIRELESS_DISABLED)
        {
            if(0 == APP_RF_Module_FW_Version)
            {
                 // Request FW version
                 APP_Request_RF_Version();
            }
        }
    #endif
#endif

    // Only let this occur at Power On
    FLAG_INHIBIT_REV_MSG = 1;
    
    #ifdef CONFIGURE_OUTPUT_REVISION
        // To track interim FW version changes during development
        // append letter of primary version number
            #ifdef CONFIGURE_UNDEFINED
                // Test Only
                if(FLAG_MEM_ERR)
                {
                  SER_Send_String("MemErr");
                  SER_Send_Prompt();
                }
            #endif

            SER_Send_Prompt();

            #ifdef CONFIGURE_DBUG_SERIAL_RESET_STATUS
                //Test Only
                SER_Send_String("Sta-");
                SER_Send_Byte(main_status_save);
                SER_Send_Prompt();

                SER_Send_String("PCon-");
                SER_Send_Byte(main_pcon_save);
                SER_Send_Prompt();

                #ifdef CONFIGURE_WDT_RST_FLAGS
                    // TODO: Undefine Later, BC: Test Only
                    if(MAIN_PCON_WDT_RESET == main_pcon_save)
                    {
                        // Test Only
                        SER_Send_String("*** WDT RESET *** ");
                        SER_Send_Int(' ',main_wdt_flags_save,16);
                        SER_Send_Prompt();
                    }
                    
                #endif

                #ifdef CONFIGURE_UNDEFINED
                    // Test Only
                    SER_Send_String("nr-");
                    SER_Send_Byte(Flags_NR.ALL);
                    SER_Send_Prompt();
                #endif

                #ifndef CONFIGURE_PRODUCTION_UNIT
                    #ifdef  CONFIGURE_DIAG_HIST
                        // For Testing Only, Don't want repeated WDTs to flood
                        //  History Memory
                        if( !(0x10 & main_pcon_save) )
                        {
                            // Record Diagnostic History WDT Reset
                            DIAG_Hist_Que_Push(HIST_WDT_RESET);
                        }
                    #endif
                #endif

                    
            #endif

            #ifdef CONFIGURE_2556_US
                SER_Send_String("Model 2556 ");
            #endif
            #ifdef CONFIGURE_2557_US
                SER_Send_String("Model 2557 ");
            #endif
            #ifdef CONFIGURE_2558_US
                SER_Send_String("Model 2558 ");
            #endif
            #ifdef CONFIGURE_2559_US
                SER_Send_String("Model 2559 ");
            #endif
            #ifdef CONFIGURE_2562_US
                SER_Send_String("Model 2562 ");
            #endif

            #ifdef CONFIGURE_2556_CA
                SER_Send_String("Model 2556 Canada ");
            #endif
            #ifdef CONFIGURE_2557_CA
                SER_Send_String("Model 2557 Canada ");
            #endif
            #ifdef CONFIGURE_2558_CA
                SER_Send_String("Model 2558 Canada ");
            #endif
            #ifdef CONFIGURE_2559_CA
                SER_Send_String("Model 2559 Canada ");
            #endif
            #ifdef CONFIGURE_2562_CA
                SER_Send_String("Model 2562 Canada ");
            #endif


            #ifdef CONFIGURE_1314_US
                SER_Send_String("Model 1314 ");
            #endif
            #ifdef CONFIGURE_1315_US
                SER_Send_String("Model 1315 ");
            #endif
            #ifdef CONFIGURE_1316_US
                SER_Send_String("Model 1316 ");
            #endif
            #ifdef CONFIGURE_1317_US
                SER_Send_String("Model 1317 ");
            #endif
            #ifdef CONFIGURE_1319_US
                SER_Send_String("Model 1319 ");
            #endif

            #ifdef CONFIGURE_1314_CA
                SER_Send_String("Model 1314 Canada ");
            #endif
            #ifdef CONFIGURE_1315_CA
                SER_Send_String("Model 1315 Canada ");
            #endif
            #ifdef CONFIGURE_1316_CA
                SER_Send_String("Model 1316 Canada ");
            #endif
            #ifdef CONFIGURE_1317_CA
                SER_Send_String("Model 1317 Canada ");
            #endif
            #ifdef CONFIGURE_1319_CA
                SER_Send_String("Model 1319 Canada ");
            #endif


            #ifdef CONFIGURE_0314_US
                SER_Send_String("Model 0314 ");
            #endif
            #ifdef CONFIGURE_0315_US
                SER_Send_String("Model 0315 ");
            #endif
            #ifdef CONFIGURE_0316_US
                SER_Send_String("Model 0316 ");
            #endif
            #ifdef CONFIGURE_0317_US
                SER_Send_String("Model 0317 ");
            #endif

            #ifdef CONFIGURE_0314_CA
                SER_Send_String("Model 0314 Canada ");
            #endif
            #ifdef CONFIGURE_0315_CA
                SER_Send_String("Model 0315 Canada ");
            #endif
            #ifdef CONFIGURE_0316_CA
                SER_Send_String("Model 0316 Canada ");
            #endif
            #ifdef CONFIGURE_0317_CA
                SER_Send_String("Model 0317 Canada ");
            #endif
                
            SER_Send_Prompt();
            SER_Send_String(REV_STRING);
            SER_Send_Prompt();
            
            #ifdef CONFIGURE_WIRELESS_MODULE
                if(!FLAG_WIRELESS_DISABLED)
                {
                    #ifndef CONFIGURE_UNDEFINED
                        SER_Send_Int('R',APP_RF_Module_FW_Version,16);
                        SER_Send_Prompt();
                    #endif

                    uchar major_rev = ((APP_RF_Module_FW_Version & 0xF0) >> 4);
                    uchar minor_rev = (APP_RF_Module_FW_Version & 0x0F);

                    SER_Send_String("RF Module FW Version - ");
                    SER_Send_Int(' ',(uint)major_rev,10);
                    SER_Send_String(".");
                    SER_Send_Int(' ',(uint)minor_rev,10);
                    SER_Send_Prompt();
                }
                else
                {
                    SER_Send_String("Wireless Disabled!");
                    SER_Send_Prompt();
                }
            #endif      

            #ifdef CONFIGURE_ID_MANAGEMENT
                MAIN_Output_Unit_ID();
            #endif

    #endif
}


/*
+------------------------------------------------------------------------------
| Function:  void MAIN_Output_Unit_ID(void)
+------------------------------------------------------------------------------
| Purpose:    Output programmable ID number if configured
|             This ID is used by Test lab to electrically identify a unit
|              with it's logged test results.  (Used for Test Only)
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
+------------------------------------------------------------------------------
*/
#ifdef CONFIGURE_ID_MANAGEMENT

    void MAIN_Output_Unit_ID(void)
    {
        if(!FLAG_NO_SMOKE_CAL)
        {
            uchar low = MEMORY_Byte_Read(DIAG_UNIT_ID_LOW);
            uchar high = MEMORY_Byte_Read(DIAG_UNIT_ID_HIGH);

            //SER_Send_Prompt();
            SER_Send_String("I");
            SER_Send_Int('D', ( (uint)((high << 8) + low ) ), 16);
            SER_Send_Prompt();
        }
    }
#endif

#ifdef CONFIGURE_SMK_SLOPE_SIMULATION
    
 /*
+------------------------------------------------------------------------------
| Function:  void main_init_slope(void)
+------------------------------------------------------------------------------
| Purpose:    When using UL Submission slope hi/low sensitivity firmware
|             this initializes slope to value saved in non-volatile memory
|              
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
+------------------------------------------------------------------------------
*/
   
    void main_init_slope(void)
    {
        uchar low = MEMORY_Byte_Read(DIAG_SLOPE_LOW);
        uchar high = MEMORY_Byte_Read(DIAG_SLOPE_HIGH);
        uint main_slope = (uint)(high << 8) + low;
        CMD_Set_Slope(main_slope);

    }
#endif

    
/*
+------------------------------------------------------------------------------
| Function:  void MAIN_Init_Diag_Timer(void)
+------------------------------------------------------------------------------
| Purpose:    Initialize and begin the Diagnostic Timer
|             This timer will output time starting at zero every second
|               for 60 seconds
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
+------------------------------------------------------------------------------
*/
#ifdef CONFIGURE_DIAG_TIMER
    void MAIN_Init_Diag_Timer(void)
    {
        if(!FLAG_DIAG_TIMER)
        {
            // Reset Timer to zero
            main_diag_timer = 0;

            // Start the Timer
            FLAG_DIAG_TIMER = 1;
        }
    }
#endif

/*
+------------------------------------------------------------------------------
| Function:  void main_initialize(void)
+------------------------------------------------------------------------------
| Purpose:    Initialize Controller Hardware at Power On / Reset
|
|
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
+------------------------------------------------------------------------------
*/
void main_initialize(void)
{
    CLRWDT();
    
    // Init the Oscillator and Timer0 
    
    // Select High Frequency Internal OSC of 16 MHZ
    // OSCEN also Enables Timer1 External Crystal 32KHz OSC
    OSCCON1 = OSC_HF_INT_OSC;
    
    #ifdef CONFIGURE_ACDC
        // Only allowed on ACDC models because of Higher Current
        OSCCON3 = OSC_SOSC_PWR_HI;
    #endif

    OSCFRQ = OSC_16_MHZ;
    
    // Default Timer 1 Clock to Run off Internal LF Clock 
    // Init to Crystal Not stable condition each power reset 
    if(FLAG_RST_SOFT)
    {
        // Soft resets do not need XTAL stabilization time
        FLAG_T1_XTAL_NOT_STABLE = 0;
    }
    else
    {
        // Other resets need XTAL stabilization time
        FLAG_T1_XTAL_NOT_STABLE = 1;
    }
        
    if(!FLAG_T1_XTAL_NOT_STABLE)
    {
        // Not using XTAL Not Stable Code (old way))
        OSCEN = OSC_ENABLE_SOSC_SELECT;
    }
    else
    {
        // Enable 31KHZ OSC until 32KHZ XTAL Stable
        OSCEN = OSC_LO_FREQ_31_KHZ_SELECT;
        
        // Init 12 second XTAL OSC Timer
        main_xtal_osc_timer = TIMER_32KHZ_STABLE_12_SEC;
    }

    // Set Timer0 to 8-bit, 1:1 output pre-scaler, Timer Enabled
    T0CON0 = TIMER0_INIT_T0CON0;
    // Timer0 clock is 16MHz/4 = 4MHZ  with /4 pre-scaler = 1 MHz
    // (i.e a 1 usec clock.)
    T0CON1 = TIMER0_INIT_T0CON1;

    /***************************************************************************
     * Port A Initialization
     *
     * Bit 7 -  VBoost Drv  - Output/Low  
     * Bit 6 -  IR_LED_ON   - Output/Low
     * Bit 5 -  Temp_Volt   - Input (analog input)
     * Bit 4 -  Escape Lgt  - Output/Low  
     * Bit 3 -  Photo_Out   - Input (analog input)
     * Bit 2 -  AC_Detect   - Input (analog input)
     * Bit 1 -  BAT_Volt    - Input (analog input)
     * Bit 0 -  CO_Out      - Input (analog input)
     *
     */

    TRISA  = 0b00101111;        // Inputs: Bit 0-3,5 -Analog input
    ANSELA = 0b00101111;
    PORTA  = 0b00000000;
    LATA   = 0b00000000;

    /***************************************************************************
     * Port B Initialization
     *
     * Bit 7 -  ICSP_Data   - Output/Low  (used for LB Cal Input too)
     * Bit 6 -  ICSP_Clk    - Output/Low
     * Bit 5 -  Photo_Pwr   - Output/Low
     * Bit 4 -  Light_Sense - Input (analog input)
     * Bit 3 -  Test_Btn    - Input (digital input)
     * Bit 2 -  CCI_Clk     - Input (digital input)
     * Bit 1 -  CCI_Data    - Input (digital input)
     * Bit 0 -  INT_In      - Input (digital input)
     *
     */

    /*
     * Since the radio module pulls up CCI Data and Clock
     * only set them to inputs if CONFIGURE_WIRELESS is selected
     * as the pins will float otherwise
     */
    
    

    #ifdef CONFIGURE_WIRELESS_MODULE
        if(FLAG_WIRELESS_DISABLED)
        {
            // Wireless Outputs should be set to Output Low (unused)
            #ifdef CONFIGURE_INTERCONNECT
                ANSELB = 0b00010000;    // Bit 4 - Light Sensor Analog Input
                TRISB  = 0b00011001;    // CCI Data/Clk set as Outputs Low
                PORTB  = 0b00000000;
                LATB   = 0b00000000;
            #else
                ANSELB = 0b00010000;    // Bit 4 - Light Sensor Analog Input
                TRISB  = 0b00011000;    // CCI Data/Clk set as Outputs Low 
                PORTB  = 0b00000000;
                LATB   = 0b00000000;
            #endif
            
        }
        else
        {
            // // Wireless Outputs should be set to Inputs
            #ifdef CONFIGURE_INTERCONNECT
                ANSELB = 0b00010000;
                TRISB  = 0b00011111;
                PORTB  = 0b00000000;
                LATB   = 0b00000000;
            #else
                ANSELB = 0b00010000;    // Bit 4 - Light Sensor Analog Input
                TRISB  = 0b00011110;    // INT IN set to output low (unused)
                PORTB  = 0b00000000;
                LATB   = 0b00000000;
            #endif
        }
    #else

      // No Wireless Module
    
      #ifdef CONFIGURE_INTERCONNECT
        ANSELB = 0b00010000;    // Bit 4 - Light Sensor Analog Input
        TRISB  = 0b00011001;    // CCI Data and Clk set as Outputs Low (unused)
        PORTB  = 0b00000000;
        LATB   = 0b00000000;
      #else
        ANSELB = 0b00010000;    // Bit 4 - Light Sensor Analog Input
        TRISB  = 0b00011000;    // CCI Data and Clk set as Outputs Low (unused)
        PORTB  = 0b00000000;
        LATB   = 0b00000000;
      #endif

    #endif

        
    // Port B bits that require Interrupt on Change
    // Bit 0 - Interconnect Input, Rising Edge
    // Bit 1 - CCI Data, Falling Edge
    // Bit 3 - Test Btn, Rising Edge

    #ifdef CONFIGURE_WIRELESS_MODULE
        if(FLAG_WIRELESS_DISABLED)
        {
            #ifdef CONFIGURE_INTERCONNECT
                // Configure IOC
                IOCBP = 0b00001001;     // Test Btn and Interconnect pos. edge
                IOCBN = 0b00000000;     // CCI Data neg. edge (not used)
            #else
                // Configure IOC
                IOCBP = 0b00001000;     // Test Btn pos. edge, No Interconnect
                IOCBN = 0b00000000;     // CCI Data neg. edge (not used)
            #endif
        }
        else
        {
            #ifdef CONFIGURE_INTERCONNECT
                // Configure IOC
                IOCBP = 0b00001001;     // Test Btn and Interconnect pos. edge
                IOCBN = 0b00000010;     // CCI Data neg. edge
            #else
                // Configure IOC
                IOCBP = 0b00001000;     // Test Btn pos. edge, No Interconnect
                IOCBN = 0b00000010;     // CCI Data neg. edge
            #endif
        }
    #else
            #ifdef CONFIGURE_INTERCONNECT
                // Configure IOC
                IOCBP = 0b00001001;     // Test Btn and Interconnect pos. edge
                IOCBN = 0b00000000;     // CCI Data neg. edge (not used)
            #else
                // Configure IOC
                IOCBP = 0b00001000;     // Test Btn pos. edge, No Interconnect
                IOCBN = 0b00000000;     // CCI Data neg. edge (not used)
            #endif
    #endif


    /***************************************************************************
     * Port C Initialization
     *
     * Bit 7 -  RX              - Input (digital input), Serial Port
     * Bit 6 -  TX              - Init to Input Serial Port (per data sheet)
     * Bit 5 -  VOICE_Data      - Output/Low
     * Bit 4 -  VOICE_Clk       - Output/Low
     * Bit 3 -  VOICE_EN        - Output/High (Power to Voice Chip)
     * Bit 2 -  Green LED       - Output/Low
     * Bit 1 -  SOSI            - Input (xtal input), 32 KHz XTAL
     * Bit 0 -  SOSO            - Input (xtal input), 32 KHz XTAL
     *
     */
    ANSELC = 0b00000011;    // Must be set to Zero to use as Digital Inputs    
    TRISC  = 0b11000011;    // Init Bits 0,1,7 Inputs (6 is TX Out pin))
    PORTC  = 0b00000000;
    LATC   = 0b00001000;    // Voice Enable Active


    T1GCON = 0x00;            // gating Off
    
    if(FLAG_T1_XTAL_NOT_STABLE)
    {
        // Configure Timer 1 for LF Oscillator. 
        // Use Internal 31KHZ Osc until Crystal is stable
        T1CLK = T1_CLK_INIT_LFOSC; 
    }
    else
    {
        // Configure Timer 1 for Secondary OSC. 
        // (32.768 crystal clock,XTAL on SOSC pins)
        // OSCEN has SOSCEN enabled to enable XTAL Oscillator
        T1CLK = T1_CLK_INIT_SOSC;
    }
    T1CON = T1_CON_INIT;     // Timer 1 On 

    /***************************************************************************
     * Port D Initialization
     *
     * Bit 7 -  BAT_Test        - Output/Low
     * Bit 6 -  INTCON_Hi_Drv   - Output/Low
     * Bit 5 -  INTCON_Lo_Drv   - Output/Low
     * Bit 4 -                  - Output/Low
     * Bit 3 -  CO_Test         - Output/Low
     * Bit 2 -  Horn_Disable    - Output/High
     * Bit 1 -  Red LED         - Output/Low
     * Bit 0 -  Disable_IN      - Input (digital input) Pulled Low on Board
     *
     */

    ANSELD = 0b00000000;        // Allow Digital Inputs to work
    TRISD  = 0b00000001;        // Bits 1-7 Outputs, Bit 0 Input
                                // Bit 1 must be cleared for PWM output
    PORTD  = 0b00000100;        // Init Horn Disable Output/High
    LATD   = 0b00000100;

    #ifdef CONFIGURE_INTERCONNECT
      INT_Intercon_Init();
    #endif

    #ifdef CONFIGURE_UNDEFINED
        // Not required here anymore 
        // Start with VBoost ON (Boost for Interconnect Circuit)
        VBOOST_On();
    #endif

    /***************************************************************************
     * Port E Initialization (4 bit wide)
     *
     * Bit 3 -  MCLR            - Init to MCLR function
     * Bit 2 -  INT_OUT         - Input (Analog Input) when Interconnect is Off
     * Bit 1 -  IR_LED_Sense    - Input (Analog Input)
     * Bit 0 -  VBoost_FB       - Input (Analog Input) (currently not used)
     *
     */

    ANSELE = 0b00000111;
    TRISE  = 0b00001111;
    PORTE  = 0b00000000;
    LATE   = 0b00000000;

    
    /*
     * Note: for PPS operation
     * 1. Only need to Unlock PPS feature one time during Init.
     * 2. Leds will operate under 2 modes (PWM and Pulse Modes)
     * 3. RD1PPS must be set to CCP4 when PWM mode is selected
     *    RD1PPS must be cleared to 0 for RD1 latch output (pulse mode)
     *    RC2PPS must be set to CCP1 when PWM mode is selected
     *    RC2PPS must be cleared to 0 for RC2 latch output (pulse mode)
     * 
     * 4. Serial Port TX must be directed to RC6 pin during Power On Init.
     *    Is does not default to RC6 as originally believed.
     * 
     *    Serial Port RX defaults to RC7 input pin at Power On Init.
     *
     *    If no Serial Port detected - set RX and TX to outputs Low
     *    
     */
    PPSLOCK = 0x55;
    PPSLOCK = 0xAA;
    PPSLOCKbits.PPSLOCKED = 0x00; // unlock PPS

    // Route Serial Port TX to Pin
    RC6PPS = 0x0010;   //RC6 -> TX

    // Leave PPS Unlocked 
    #ifdef NOT_DEFINED
        PPSLOCK = 0x55;
        PPSLOCK = 0xAA;
        PPSLOCKbits.PPSLOCKED = 0x01; // lock PPS
    #endif

    // Initialize Serial Port as soon as possible to capture any CCI
    // Power On messages from RF module
    #ifdef CONFIGURE_WIRELESS_CCI_DEBUG
        APP_CCI_QInit();
    #endif
    SER_Init();    // Test for Serial Port Initialization

    // Initialize Photo Smoke Cal status
    PHOTO_Photo_Init();

    // Read CO Cal Status &Init CO Cal status
    CO_Cal_Init();

    FLAG_POWERUP_DELAY_ACTIVE = 1;  
    
    #ifdef CONFIGURE_SPECIAL_LIGHT_TEST_FW
        // Disable LED Ring Power Indication
        // Button Press will re-enable it
        FLAG_LED_RING_OFF = 1;
    #endif
    

    main_initialize_engine();
   

    #ifdef CONFIGURE_WIRELESS_MODULE
        
        if(!FLAG_WIRELESS_DISABLED)
        {
            // Initial CCI Hardware if Wireless Module is functional
            CCI_Phy_Init();
        }
        
        // Init allowing Remote Alarm Response
        FLAG_ALM_REMOTE_ENABLE = 1;
    #else
        // Weak Pull ups enabled to prevent unused inputs from floating

        // Enable Weak Pull-ups for CCI, clear all bits 1st
        WPUB = WPUB_ALL_PULLUPS_OFF;   

        // For CCI Data and Clock Pins, enable Weak Pull-ups to
        //  keep these unused inputs from floating.
        CCI_DATA_WPU_ENABLE
        CCI_CLK_WPU_ENABLE

        // Clock and DATA pins start off as inputs.
        mCCI_DATA_PIN_DIR = 1;
        mCCI_CLOCK_PIN_DIR = 1;

    #endif

    // Setup the 1 ms Timer, INTS Off
    TIMER_ONE_MS_INT_FLAG = 0;
    TIMER_ONE_MS_INT_ENAB = 0;

    //
    // Init the 1 MS Timer Clock (T4)
    //
    if(FLAG_T1_XTAL_NOT_STABLE)
    {
        // Init the 1 MS Main Interrupt Timer
        main_T4_Init_Clk(T4_CLK_HFOSC); 
    }
    else
    {
        #ifdef CONFIGURE_STROBE_1_MS_TASK    
            // Transition to SOSC for 1 MS 
            //  Main Interrupt Timer
            main_T4_Init_Clk(T4_CLK_SOSC);  
        #else
            // Init the 1 MS Main Interrupt Timer
            main_T4_Init_Clk(T4_CLK_HFOSC); 
        #endif
    }
    
    // Enable 1 MS Timer INTS
    TIMER_ONE_MS_INT_ENAB = 1;

    // Enable IOC Interrupts
    // Global IOC Flag Cleared
    IOC_INTERRUPT_FLAG = 0;
    
    // Global IOC Interrupts enabled
    IOC_INTERRUPT_ENABLE = 1;

    
    // Disable unused Modules (clocks to these modules turned off))
    main_PMD_init();
    
    // Init PWM and Start the LED Profile Timer
    LED_Init_PWM();
    
    // This forces Initialization into Active Mode.
    FLAG_MODE_ACTIVE = 0;
    main_set_active();
    
    // Reset task Engine Timer and Enable Global Interrupts
    main_reset_sleep_timer();   // Init to Active Mode TIC Timing

    // OSC stable routine is not run here if FLAG_T1_XTAL_NOT_STABLE is active
    if(FLAG_T1_XTAL_NOT_STABLE == 0)
    {
        #ifndef CONFIGURE_SIMULATOR_DEBUG
            // Timers 0 and 1 should already be initialized before this call
            main_wait_t1osc_stable();
        #endif
    }

    #ifdef CONFIGURE_WIRELESS_MODULE
         // Request FW version
         APP_RF_Module_FW_Version = 0;
    #endif
    
}


/*
+------------------------------------------------------------------------------
| Function:  void Main_Wait_T1Osc_Stable(void)
+------------------------------------------------------------------------------
| Purpose:    This function waits until T1 Osc stabilizes.  Circuit tolerances
|             can effect the startup time.  
|
|             Models with BEEP-BE-GONE hardware will need longer timeout
|              to allow XTAL to Stabilize    
|
+------------------------------------------------------------------------------
| Parameters:    none
|
|
+------------------------------------------------------------------------------
| Return Value:    none
|
|
+------------------------------------------------------------------------------
*/
void main_wait_t1osc_stable(void)
{
    unsigned long horn_timer_1;
    unsigned int horn_timer_2;
    unsigned int timer_32KHz;
    
    CLRWDT();
   
    //  Delay HORN_ON for BEEP_BE_GONE feature to work properly 
    //   by use of horn_timers

    horn_timer_2 = 10000;     // Horn Delay for OSC off frequency
    
    horn_timer_1 = 400000;     // ~4 Seconds
    
    //
    // Wait for Timer Bit 2 to be Zero
    //
    while(TMR1L & 0x04)
    {
        CLRWDT();
        // Wait for Timer 1 bit 2 Low
        PIC_delay_us(10);
        if(0 == --horn_timer_1)
        {
           RED_LED_ON
           HORN_ON
            #ifdef CONFIGURE_XTAL_SERIAL
                // Test Only
                horn_timer_1 = 400000;        
                SER_Send_String("HA");
                SER_Xmit_Queue();
            #endif
        }
    }

    // Turn Off CCI 1 ms Timer during measurements
    TIMER_ONE_MS_INT_ENAB = 0;
    
    for(uchar loop_ctr = 10; loop_ctr != 0; loop_ctr--)
    {
        horn_timer_1 = 400000;     // ~4 Seconds
        
        //
        // Wait for Timer 1 bit 2 High
        //
        while(!(TMR1L & 0x04))
        {
            CLRWDT();
            // Wait for bit 2 high
            PIC_delay_us(10);
            if(0 == --horn_timer_1)
            {
               RED_LED_ON
               HORN_ON
                       
                #ifdef CONFIGURE_XTAL_SERIAL
                    // Test Only
                    horn_timer_1 = 400000;        
                    SER_Send_String("HB");
                    SER_Xmit_Queue();
                #endif
            }
        }

        horn_timer_1 = 400000;     // ~4 Seconds
        
        // Clear Timer0 to time the Timer1 rollover (bit 2).
        TMR0 = 0;
        
        #ifdef CONFIGURE_XTAL_SERIAL
            MAIN_TP2_On();
        #endif
        
        //
        // Wait for Timer1 Bit 2 Low
        //
        while(TMR1L & 0x04)
        {
            CLRWDT();
            // Wait for bit 2 low
            PIC_delay_us(10);
            if(0 == --horn_timer_1)
            {
               RED_LED_ON
               HORN_ON
                       
                #ifdef CONFIGURE_XTAL_SERIAL
                    // Test Only
                    horn_timer_1 = 400000;        
                    SER_Send_String("HC");
                    SER_Xmit_Queue();
                #endif
            }
        }

        #ifdef CONFIGURE_XTAL_SERIAL
            MAIN_TP2_Off();
        #endif

        // TMR0 contains the number of clock cycles in four
        // TMR1 LSB.  If greater than 100 usec, continue
        // 100 clocks @ 1 usec (4 MHz or fosc/4)/4, with fosc = 16 MHz
        // 32KHz Osc would be about 122 clocks, once stable
        
        // Capture Current Timer value
        timer_32KHz = TMR0;    
        
        if((timer_32KHz < TIME_100_CLOCKS) || (timer_32KHz > TIME_150_CLOCKS))
        {
            CLRWDT();
            loop_ctr = 10;          // Remain in Loop with Horn ON

            // Wait for bit 2 low
            if(0 == --horn_timer_2)
            {
                horn_timer_2 = 10000;
                
                RED_LED_ON
                HORN_ON
                        
                #ifdef CONFIGURE_XTAL_SERIAL
                    // Test Only
                    SER_Send_String("HD");
                    SER_Xmit_Queue();
                    SER_Send_Int(',',timer_32KHz,10);
                    SER_Xmit_Queue();
                    SER_Xmit_Queue();
                    SER_Xmit_Queue();
                #endif
            }

        }
        
   }
    
    #ifdef CONFIGURE_XTAL_SERIAL
        // Save TMR0 value for later
        main_xtal_count = timer_32KHz;
    #endif
    
   RED_LED_OFF
   HORN_OFF       // Horn Off
           
    // Re-Enable CCI 1 ms Timer after successful stabilization
    TIMER_ONE_MS_INT_ENAB = 1;
           

}

/*
+------------------------------------------------------------------------------
| Function:  void Main_Reset_Sleep_Timer(void)
+------------------------------------------------------------------------------
| Purpose:    Stop the timer and configure it to timeout in
|             100/1000 ms.  Alarm will wake up from sleep when the timer
|             interrupts, thus the enable bit and the peripheral int
|             enable must both be set.
|
+------------------------------------------------------------------------------
| Parameters:    none
|
|
+------------------------------------------------------------------------------
| Return Value:    none
|
|
+------------------------------------------------------------------------------
*/
void main_reset_sleep_timer(void)
{
    /*
     Timer must be stopped to ensure that the value is not
     corrupted during the write.
    */
    if(FLAG_MODE_ACTIVE)
    {
        // Global Ints Enabled
        GLOBAL_INT_ENABLE = 0;
        T1CONbits.TMR1ON = 0;   // Timer1 Off


        //  Added this for 1 ms timer support in non-wireless
        // In active mode we do not Sleep (so we don't need Timer 1 Ints)
        PIE4bits.TMR1IE = 0;
        PIR4bits.TMR1IF = 0;
        PERIPERAL_INT_ENABLE = 1;     // Keep Enabled for 1 MS Timer Ints

        // Enable Timer 1MS Ints
        TIMER_ONE_MS_INT_FLAG = 0;
        TIMER_ONE_MS_INT_ENAB = 1;
        // Turn Timer 4 Back On (1 ms Timer)
        T4CONbits.TMR4ON = 1;

        TMR1 = TIMER1_INIT_VALUE_10MS;
        T1CONbits.TMR1ON = 1;    // Timer1 On
        
        // Global Ints Enabled
        GLOBAL_INT_ENABLE = 1;

    }
    else
    {
        T1CONbits.TMR1ON = 0;   // Timer1 Off

        // Sleep Timing set Timer 1 Ints to Wake from Sleep
        PIE4bits.TMR1IE = 1;
        PIR4bits.TMR1IF = 0;
        PERIPERAL_INT_ENABLE = 1;

        //  Added this for 1 ms timer support in non-wireless
        // Turn Timer 4 Off (1 ms Timer)
        T4CONbits.TMR4ON = 0;
        // Disable 1MS Timer Ints
        TIMER_ONE_MS_INT_ENAB = 0;
        TIMER_ONE_MS_INT_FLAG = 0;

        TMR1 = TIMER1_INIT_VALUE_1000MS;
        T1CONbits.TMR1ON = 1;    // Timer1 On
        
        //  Global Ints Enabled for Low Power CCI Ints
        GLOBAL_INT_ENABLE = 1;

    }

}

/*
+------------------------------------------------------------------------------
| Function:  unsigned int Make_Return_Value(unsigned int milliseconds)
+------------------------------------------------------------------------------
| Purpose:    Converts milliseconds to task engine tic times and returns to
|              calling routine. Value is used to inform task engine next
|              time to service this task.
|
+------------------------------------------------------------------------------
| Parameters:    milliseconds  (0-65535)
|
+------------------------------------------------------------------------------
| Return Value:  number of task engine clock tics
|
|
+------------------------------------------------------------------------------
*/
unsigned int MAIN_Make_Return_Value(unsigned int milliseconds)
{
    unsigned int num_tics;

    if(milliseconds > main_current_ms_per_tic)
    {
        num_tics = (milliseconds / main_current_ms_per_tic);
    }
    else
    {
        // Minimum of 1 Tic Time
        num_tics = 1;
    }

    return num_tics;
}


/*
+------------------------------------------------------------------------------
| Function:  UINT Main_Calc_Current_Tics(UCHAR Task_ID)
+------------------------------------------------------------------------------
| Purpose:    This function calculates the number of tics in the current mode
|              before the task is scheduled to execute.
|
+------------------------------------------------------------------------------
| Parameters:    Task_ID  (0-255)  Assigned Task ID number
|
+------------------------------------------------------------------------------
| Return Value:  Time until task execution (in task time tics)
|
|
+------------------------------------------------------------------------------
*/
uint Main_Calc_Current_Tics(uchar Task_ID)
{
    uint tics;
    //
    // Time in tics = Interval - (tics - Last time)
    // ==>Time in tics = Interval - tics + Last time
    //
    tics = TaskTable[Task_ID]->TaskInterval;
    tics -= main_tic_timer;
    tics += TaskTable[Task_ID]->TaskTime;

    return tics;
}


/*
+------------------------------------------------------------------------------
| Function:  void Main_Recalc_Int_Active(UCHAR Task_ID)
+------------------------------------------------------------------------------
| Purpose:    Recalculate task time execution to run under Active Mode timing
|
+------------------------------------------------------------------------------
| Parameters:    Task_ID  (0-255)  Assigned Task ID number
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|                Task Interval and Task Time are updated.
|
|
+------------------------------------------------------------------------------
*/
void Main_Recalc_Int_Active(uchar Task_ID)
{
    if(TaskTable[Task_ID]->TaskInterval != 0)
    {
        uint tics = Main_Calc_Current_Tics(Task_ID);

        // Multiply the number of tics by 100 to get the number of tics
        // in active mode and update interval.
        TaskTable[Task_ID]->TaskInterval = tics * 100;

        // Change last execution time of task to current time.
        TaskTable[Task_ID]->TaskTime = main_tic_timer;
    }
}


/*
+------------------------------------------------------------------------------
| Function:  void Main_Recalc_Int_Standby(UCHAR Task_ID)
+------------------------------------------------------------------------------
| Purpose:    Recalculate task time execution to run under Standby Mode timing
|
+------------------------------------------------------------------------------
| Parameters:    Task_ID  (0-255)  Assigned Task ID number
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|                Task Interval and Task Time are updated.
|
|
+------------------------------------------------------------------------------
*/
void Main_Recalc_Int_Standby(uchar Task_ID)
{
    // In certain cases a task may have reset another task. If this happens,
    // the TaskInterval will be zero. The task recalculations will break if
    // the TaskInterval is zero.
    if(TaskTable[Task_ID]->TaskInterval != 0)
    {
        uint tics = Main_Calc_Current_Tics(Task_ID);

        // Divide the number of tics by 100 to get the number of tics
        // in standby mode and update interval.
        TaskTable[Task_ID]->TaskInterval = tics
                     / (MAIN_TIC_INTERVAL_LOWPOWER/MAIN_TIC_INTERVAL_ACTIVE);

        // Add one to make up for truncation in division
        TaskTable[Task_ID]->TaskInterval++;

        // Change last execution time of task to current time.
        TaskTable[Task_ID]->TaskTime = main_tic_timer;
    }
}


/*
+------------------------------------------------------------------------------
| Function:  void MAIN_Reschedule_Int_Active(UCHAR Task_ID, UINT time)
+------------------------------------------------------------------------------
| Purpose:    Reschedule the passed Task_ID to execute at the next passed
|              Time interval.
+------------------------------------------------------------------------------
| Parameters:    Task_ID  (0-255)  Assigned Task ID number
|                time in milliseconds (10 - 65535)
+------------------------------------------------------------------------------
| Return Value:  none
|
|                Task Interval and Task Time are updated.
|
|
+------------------------------------------------------------------------------
*/
void MAIN_Reschedule_Int_Active(uchar Task_ID, uint time)
{

    uint tics = (time/MAIN_TIC_INTERVAL_ACTIVE);

     //Set Interval in active tics and update Task Time
    TaskTable[Task_ID]->TaskInterval = tics;
    TaskTable[Task_ID]->TaskTime = main_tic_timer;

}

#ifdef CONFIGURE_ACTIVE_TIMEOUT_TIMER
// Reset the Active Mode Timeout Timer
void MAIN_Reset_Active_Mode_Timer(void)
{
    main_active_timeout_timer = 0;
}

// Force Active Mode Timeout on next 1 minute Tic
void MAIN_Active_Mode_Timer_Timeout(void)
{
    main_active_timeout_timer = MAIN_ACTIVE_MODE_TIMEOUT_2_HR;
}

// Update and Test Active Mode Timer
void main_active_mode_timer_update(void)
{
    if(FLAG_MODE_ACTIVE)
    {
        // Update Active Mode Timeout Timer
        main_active_timeout_timer++;

        #ifdef CONFIGURE_UNDEFINED
            // Test only, to monitor Active Timer value
            SER_Send_Prompt();
            SER_Send_String("Active Timer -- ");
            SER_Send_Int('>', main_active_timeout_timer, 10);
            SER_Send_Prompt();
        #endif

        //
        // Conditions we do not want Active Timeout Soft Reset
        //
        // AC Detect and CO Cal Flags  
        // No Smoke Cal 
        // Low Bat Cal 
        // Any active alarm Condition
        //
        if(FLAG_AC_DETECT || FLAG_CALIBRATION_NONE ||
           FLAG_CALIBRATION_PARTIAL || 
           FLAG_CALIBRATION_UNTESTED ||
           FLAG_NO_SMOKE_CAL || FLAG_LOW_BATTERY_CAL_MODE ||
           FLAG_ALM_PRIO_NOT_IDLE)  
        {
            // Reset the Active Mode Timer 
            MAIN_Reset_Active_Mode_Timer();
        }
        // Ignore the Serial Port Active Flag in Active2 reg.   
        else if(Flags_Active1.ALL ||
               (Flags_Active2.ALL && 
                    MAIN_MASK_SERIAL_ACTIVE_FLAG) ||
                Flags_Active3.ALL ||
                Flags_Active4.ALL ||
                Flags_Active5.ALL ||
                Flags_Active6.ALL ||
                Flags_Active7.ALL )
        {

        #ifdef CONFIGURE_ACCEL_ACTIVE_TIMEOUT
            // Not Used - Currently using serial Port Commands 
            //  to Test Timeout Timer and Soft Reset
            //
            // Use 2 Minute Timing
            // Test the Timer
            if(MAIN_ACTIVE_MODE_TIMEOUT_2_MIN <= 
                    main_active_timeout_timer)
            {
                // Timeout - Perform Soft reset now
                SOFT_RESET
            }

        #else
            // Use 2 Hour Timing
            // Test the Timer
            if(MAIN_ACTIVE_MODE_TIMEOUT_2_HR <= 
                    main_active_timeout_timer)
            {
                // Record Active Timeout
                DIAG_Hist_Que_Push(HIST_ACTIVE_TIMEOUT);

                // Clear Non-Reset Alarm memory flags
                // After 2 Hrs in Active Mode
                FLAG_CO_ALARM_MEMORY = 0;
                FLAG_SMOKE_ALARM_MEMORY = 0;

                // Timeout - Perform Soft reset now
                SOFT_RESET
            }
        #endif


        }

    }
    else
    {
        // Reset the Active Mode Timer if in LP Mode
        MAIN_Reset_Active_Mode_Timer();
    }
    
}

#endif


/*
+------------------------------------------------------------------------------
| Function:  void Main_Set_Active(void)
+------------------------------------------------------------------------------
| Purpose:    Call this function to transition from low power to active mode.
|
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
|
+------------------------------------------------------------------------------
*/
void main_set_active(void)
{
    // If we're already in active mode just return.
    if(!FLAG_MODE_ACTIVE)
    {
        GLOBAL_INT_ENABLE = 0;

        #ifdef CONFIGURE_DIAG_ACTIVE_FLAGS   
            // Test For Serial Port and Enable if Detected
            SER_On();
        #endif
        
        //
        // This flag notifies all interested tasks that they are operating
        // in active mode.
        //
        FLAG_MODE_ACTIVE = 1;

        #ifdef CONFIGURE_DEBUG_WAKE_SLEEP_TIMING
            MAIN_TP_On();       // TP On in Active Run Mode
        #endif

        FLAG_ACTIVE_TIMER = 1;
        // Minimum time to remain in Active Mode
        MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_2_SEC;

        // Added this for 1 ms timer support in non-wireless versions
        PIE4bits.TMR1IE = 0;
        PIR4bits.TMR1IF = 0;
        PERIPERAL_INT_ENABLE = 1;        // Keep Enabled for 1MS Timer Ints

        // Enable Timer 1MS Ints
        TIMER_ONE_MS_INT_FLAG = 0;
        TIMER_ONE_MS_INT_ENAB = 1;
        // Turn Timer 4 Back On (1 ms Timer)
        T4CONbits.TMR4ON = 1;
        
        // Timers On
        T0CON0bits.T0EN = 1;
        T6CONbits.T6ON = 1;

        CCP1CONbits.CCP1EN = 1;
        CCP4CONbits.CCP4EN = 1;
        
        //
        // Set the mode.
        //
        main_current_mode = MODE_ACTIVE;
        main_current_ms_per_tic = MAIN_TIC_INTERVAL_ACTIVE;

        // Wake events may require immediate alarm action on these Tasks
        MAIN_Reset_Task(TASKID_PTT_TASK);
        MAIN_Reset_Task(TASKID_SOUND_TASK);
        MAIN_Reset_Task(TASKID_RLED_MANAGER);
        
        #ifdef CONFIGURE_INTERCONNECT
            MAIN_Reset_Task(TASKID_INTERCONNECT);
        #endif

        #ifdef CONFIGURE_STROBE
            #ifndef CONFIGURE_STROBE_1_MS_TASK
                MAIN_Reset_Task(TASKID_STROBE_TASK);
            #endif
        #endif

        #ifdef CONFIGURE_ESCAPE_LIGHT
            MAIN_Reset_Task(TASKID_ESCAPE_LIGHT);
        #endif

        #ifdef CONFIGURE_WIRELESS_MODULE
            // Monitor Wireless Message Process Immediately
            MAIN_Reset_Task(TASKID_APP_STAT_MONITOR);
        #endif

        #ifdef CONFIGURE_CO    
            /*
             * Init CO alarm accumulation time to 30 seconds
             * Resetting this task here will cause extra accumulations
             * and shorter alarm times.If active mode runs less than 30 secs
             * then the LP 30 second timer will still run the accumulator
             */    
            MAIN_Reschedule_Int_Active(TASKID_COALARM, TIME_ACTIVE_30_SEC);
        #endif
            
        /*
         * Reset these Tasks as they were run outside Task Manager
         *  while in LP Mode
         */

        if(!FLAG_RESET && !FLAG_LP_BATTERY_TEST)
        {
            // Prevent Bat Test Task Running each time we enter Active Mode
            MAIN_Reschedule_Int_Active(TASKID_BATTERY_TEST, TIME_ACTIVE_60_SEC);
        }
        else
        {
            // Except on Power Resets and required Battery Tests from LP mode
           MAIN_Reset_Task(TASKID_BATTERY_TEST);
        }

        // During Normal Low Power Mode we need to transition to Active 
        // periodically to Send/Process CCI communication for 
        // 1. Low Battery Test
        // 2. CCI Supervision
        // But we do not want Green Led task to Flash when this occurs
        if(!FLAG_RESET)
        {
            // Delay Green LED Task to begin in 5 seconds, that is, if 
            //  we remain in active mode that long. This prevents Green LED 
            //  from blinking during wakes to service LP BatTest and CCI 
            //  supervise
            MAIN_Reschedule_Int_Active(TASKID_GLED_MANAGER, TIME_ACTIVE_5_SEC);
        }
        
        // Re-calculate these LP Mode Tasks for Active Mode Timing
        Main_Recalc_Int_Active(TASKID_PHOTO_SMOKE);
        Main_Recalc_Int_Active(TASKID_COMEASURE);
        Main_Recalc_Int_Active(TASKID_LIGHT_TASK);
        
#ifdef CONFIGURE_UNDEFINED
        // These re-calculations were undefined since their task times are 
        // not maintained nor valid in LP Mode
        Main_Recalc_Int_Active(TASKID_TROUBLE_MANAGER);
        Main_Recalc_Int_Active(TASKID_ALED_MANAGER);
        Main_Recalc_Int_Active(TASKID_TROUBLE_MANAGER);
#else
        // Trouble manager task has been running directly from
        //  LP Mode timer. 
        // Reschedule to begin in 15 Seconds in active Mode
        MAIN_Reschedule_Int_Active(TASKID_TROUBLE_MANAGER, TIME_ACTIVE_15_SEC);
        
        // Prevent this task from executing every active transition
        MAIN_Reschedule_Int_Active(TASKID_ALED_MANAGER, TIME_ACTIVE_5_SEC);
 #endif
        
        GLOBAL_INT_ENABLE = 1;
    }
}


/*
+------------------------------------------------------------------------------
| Function:  void Main_Set_Low_Power(void)
+------------------------------------------------------------------------------
| Purpose:    Call this function to transition from active to low power mode.
|
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
|
+------------------------------------------------------------------------------
*/
void main_set_low_power(void)
{
    // If we're already in low power mode just return.
    if(FLAG_MODE_ACTIVE)
    {
        //  Global Ints Disabled
        GLOBAL_INT_ENABLE = 0;

        //
        // This flag notifies all interested tasks that they are not
        //    operating in active mode.
        //
        FLAG_MODE_ACTIVE = 0;

        GLED_Off();
        GREEN_LED_OFF

        // Keep VBoost off in Low Power Mode
        VBOOST_Off();

        // Remember Turn off Drive to Voice Clock/Data transistors
        PORT_VOICE_EN_PIN = 0;      // Voice Chip Power Off
        FLAG_VCE_PWR_ON_DELAY_COMPLETE = 0; // Reset Power On Delay 
        
        LAT_VOICE_CLK_PIN = 0;      // and no Voltage on Voice Inputs
        LAT_VOICE_DATA_PIN = 0;
        

        // Sleep Mode - T1 Int Flag used to wake from sleep
        PIE4bits.TMR1IE = 1;
        PIR4bits.TMR1IF = 0;
        PERIPERAL_INT_ENABLE = 1;

        // Turn Timer 4 Off (1 ms Timer)
        T4CONbits.TMR4ON = 0;
        // Disable 1MS Timer Ints
        TIMER_ONE_MS_INT_ENAB = 0;
        TIMER_ONE_MS_INT_FLAG = 0;

        // Timers Off
        T0CON0bits.T0EN = 0;
        T6CONbits.T6ON = 0;
        
        CCP1CONbits.CCP1EN = 0;
        CCP4CONbits.CCP4EN = 0;

        // Interrupt on Change Interrupt Enable must be set
        //  to Allow Interrupt on Change to Wake from Sleep
        IOC_INTERRUPT_FLAG = 0;
        IOC_INTERRUPT_ENABLE = 1;

        //
        // Set the mode.
        //
        main_current_mode = MODE_LOW_POWER;
        main_current_ms_per_tic = MAIN_TIC_INTERVAL_LOWPOWER;

        #ifdef CONFIGURE_ESCAPE_LIGHT
            // Make Sure Escape Light was not left On
            Esc_Light_Off();
        #endif

        //
        // Tasks need to be reset here so that they start fresh
        // with the new wakeup time.
        //
        Main_Recalc_Int_Standby(TASKID_PHOTO_SMOKE);
        Main_Recalc_Int_Standby(TASKID_COMEASURE);
        Main_Recalc_Int_Standby(TASKID_LIGHT_TASK);
        
        Main_Recalc_Int_Standby(TASKID_TROUBLE_MANAGER);
        Main_Recalc_Int_Standby(TASKID_ALED_MANAGER);
        
        //  Global Ints Enabled
        GLOBAL_INT_ENABLE = 1;

        #ifdef CONFIGURE_UNDEFINED
            /*
             * These tasks are run outside of Task Manager Loop by
             *   30 second LP Mode timer to Reduce Power, so they do not
             *   require initialization until returning to Active Mode.
             */
             Main_Recalc_Int_Standby(TASKID_BATTERY_TEST);
             Main_Recalc_Int_Standby(TASKID_COALARM);
             MAIN_Reset_Task(TASKID_GLED_MANAGER);
             
        #endif

    #ifdef CONFIGURE_DIAG_ACTIVE_FLAGS
        // Enable Diag Active Tx on next transition into Active Mode
        FLAG_DIAG_ACTIVE_RST = 1;
        
        // Enable Data Output on Active Transition
        main_diag_count = 0;
        main_diag_sec_time = 0;
    #endif

    #ifdef CONFIGURE_ACTIVE_TIMEOUT_TIMER 
        MAIN_Reset_Active_Mode_Timer();
    #endif
            
    }
    

}


/*
+------------------------------------------------------------------------------
| Function:  void Main_Check_Low_Power_Logic(void)
+------------------------------------------------------------------------------
| Purpose:    This function performs the logic to determine if the firmware
|              needs to be in low power or active.
|
|             Any Active Mode bit that is set will keep firmware in Active Mode
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
|
+------------------------------------------------------------------------------
*/
void main_check_low_power_logic(void)
{
    // Set Active Mode if any Active Flags are set (see flags.h)

    #ifdef CONFIGURE_DEBUG_ACTIVE_FLAGS
        uchar tp2_dbug_count = 0;
    #endif

    if(FLAGS_ALL_CLEARED != Flags_Active1.ALL)
    {
        #ifdef CONFIGURE_DEBUG_ACTIVE_FLAGS
            tp2_dbug_count = 1;
        #endif
        main_set_active();
    }
    else if(FLAGS_ALL_CLEARED != Flags_Active2.ALL)
    {
        #ifdef CONFIGURE_DEBUG_ACTIVE_FLAGS
            tp2_dbug_count = 2;
        #endif
        main_set_active();
    }
    else if(FLAGS_ALL_CLEARED != Flags_Active3.ALL)
    {
        #ifdef CONFIGURE_DEBUG_ACTIVE_FLAGS
            tp2_dbug_count = 3;
        #endif
        main_set_active();
    }
    else if(FLAGS_ALL_CLEARED != Flags_Active4.ALL)
    {
        #ifdef CONFIGURE_DEBUG_ACTIVE_FLAGS
            tp2_dbug_count = 4;
        #endif
        main_set_active();
    }
    else if(FLAGS_ALL_CLEARED != Flags_Active5.ALL)
    {
        #ifdef CONFIGURE_DEBUG_ACTIVE_FLAGS
            tp2_dbug_count = 5;
        #endif
        main_set_active();
    }
    else if(FLAGS_ALL_CLEARED != Flags_Active6.ALL)
    {
        #ifdef CONFIGURE_DEBUG_ACTIVE_FLAGS
            tp2_dbug_count = 6;
        #endif
        main_set_active();
    }
    else if(FLAGS_ALL_CLEARED != Flags_Active7.ALL)
    {
        #ifdef CONFIGURE_DEBUG_ACTIVE_FLAGS
            tp2_dbug_count = 7;
        #endif
        main_set_active();
    }
        
    #ifdef CONFIGURE_WIRELESS_MODULE
        else if(FLAGS_ALL_CLEARED != Flags_Remote_Alerts.ALL)
        {
            #ifdef CONFIGURE_DEBUG_ACTIVE_FLAGS
                tp2_dbug_count = 8;
            #endif
            main_set_active();
        }
        else if(FLAGS_ALL_CLEARED != Flags_Remote_Flags1.ALL)
        {
            #ifdef CONFIGURE_DEBUG_ACTIVE_FLAGS
                tp2_dbug_count = 9;
            #endif
            main_set_active();
        }
        else if(FLAG_STATUS_RMT_SMK_HW_RCVD || FLAG_STATUS_RMT_CO_HW_RCVD)
        {
             #ifdef CONFIGURE_DEBUG_ACTIVE_FLAGS
                tp2_dbug_count = 10;
            #endif
            main_set_active();
        }
    #endif

    else
    {
        #ifndef CONFIGURE_ACTIVE_MODE_ONLY
            main_set_low_power();
        #else
            main_set_active();
        #endif
    }

    #ifdef CONFIGURE_DEBUG_ACTIVE_FLAGS_OFF
        for(uchar i = tp2_dbug_count; i > 0; i--)
        {
            // Test Only
            MAIN_TP2_On();
            PIC_delay_us(DBUG_DLY_25_USec);
            MAIN_TP2_Off();
        }
    #endif

}

/*
+------------------------------------------------------------------------------
| Function:  void Initialize_Engine(void)
+------------------------------------------------------------------------------
| Purpose:    Initialize the Task Engine
|             Task Intervals, Task Time are reset
|             Task Function Pointer is set to Location of Task Entry address
|
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
|
+------------------------------------------------------------------------------
*/
void main_initialize_engine(void)
{
    uint i;

    // Since TaskTable is made of integer pointers, divide by two
    //   to get actual number of structs to init.
    for(i = 0; i < (sizeof(TaskTable) / 2); i++)
    {
        TaskTable[i]->TaskInterval = 0;
        TaskTable[i]->TaskTime = 0;
    }

    SoundTaskInfo.funcptr = SND_Process_Task;
    ButtonTaskInfo.funcptr = BTN_Process_Task;
    PTT_TaskInfo.funcptr = PTT_Process_Task;
    BatteryTestInfo.funcptr = BAT_Test_Task;
    DebugTaskInfo.funcptr = main_debug_task;
    AlarmPrioInfo.funcptr = ALM_Priority_Task;
    COAlarmInfo.funcptr = CO_Alarm_Task;
    CalManagerInfo.funcptr = CO_Cal_Manager_Task;
    COMeasureInfo.funcptr = CO_Measure_Task;
    TroubleManagerInfo.funcptr = SND_Trouble_Manager_Task;
    RLedInfo.funcptr = RLED_Manager_Task;
    GLedInfo.funcptr = GLED_Manager_Task;
    ALedInfo.funcptr = ALED_Manager_Task;
    LedBlinkInfo.funcptr = LED_Blink_Task;
    EscLightInfo.funcptr = Esc_Light_Process_Task;
    VoiceInfo.funcptr = VCE_Process_Task;
    PhotoSmokeInfo.funcptr = PHOTO_Process_Task;
    InterconnectInfo.funcptr = INT_Process_Task;
    LightInfo.funcptr = LIGHT_Manager;
    AppStatusMonitorInfo.funcptr = APP_Status_Monitor;

}

/*
+------------------------------------------------------------------------------
| Function:  void main_PMD_init(void)
|            
|            
|
+------------------------------------------------------------------------------
| Purpose:    Disable Modules that are not being used for power saving
|
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
|
+------------------------------------------------------------------------------
*/

void main_PMD_init(void)
{

    PMD0 = 0x18;        //Disable Scanner and CRC Modules
    PMD1 = 0xA8;        //Disable Timers 3,5 and Numeric Controlled OSC
    PMD2 = 0x47;        //Disable DAC, Zero Cross Detect, and Comparators
    PMD3 = 0x76;        //Disable PWM7, PWM6 and CCP5, CCP3, CCP2
    PMD4 = 0x37;        //Disable MSSP1,2  and CWG1,2,3
    PMD5 = 0xDF;        //disable SMT2,1 and CLC4,3,2,1 and DSM
    
}


// TODO: Undefine Later, BC: Test Only
#ifdef CONFIGURE_UNDEFINED

/*
+------------------------------------------------------------------------------
| Function:  MAIN_Init_While_Timer
|
+------------------------------------------------------------------------------
| Purpose:    Initialize Timer that may be used to test and limit while loops
|             To see what while statements may be resulting in WDTs  
+------------------------------------------------------------------------------
| Parameters:    time in ms
|
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/
void MAIN_Init_While_Timer(uint timer_ms)
{
    main_while_timer_ms = timer_ms;
}

/*
+------------------------------------------------------------------------------
| Function:  MAIN_While_Timer
|
+------------------------------------------------------------------------------
| Purpose:    Service Timer
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  TRUE if expired, FALSE if not expired
|
+------------------------------------------------------------------------------
*/
uchar MAIN_While_Timer(void)
{
    
    if(main_while_timer_ms != 0)
    {
        PIC_delay_ms(1);
        
        if(0 == --main_while_timer_ms)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    return TRUE;
}

#endif


/*
+------------------------------------------------------------------------------
| Function:  main_flash_read_device_revision
|
+------------------------------------------------------------------------------
| Purpose:    Return Device Revision
|             An A3 device will return the value of 0x2003 and an A2 device will 
|             return the value of 0x2002. Now the Device Revision is part of the 
|             configuration space and not the normal program memory so 
|             the NVMREGS register needs to be set to ?1?.  
|               
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  Device Revision
|
+------------------------------------------------------------------------------
*/

uint16_t main_flash_read_device_revision(void)
{
    uint8_t GIEBitValue = INTCONbits.GIE;   // Save interrupt enable
    
    INTCONbits.GIE = 0;     // Disable interrupts
    NVMADRL = 0x05;
    NVMADRH = 0x80;

    NVMCON1bits.NVMREGS = 1;    // Select Configuration space
    NVMCON1bits.RD = 1;      // Initiate Read
    NOP();
    NOP();
    INTCONbits.GIE = GIEBitValue;               // Restore interrupt enable

    return ((uint16_t)((NVMDATH << 8) | NVMDATL));
}
 

/*
+------------------------------------------------------------------------------
| Function:  void Main_TP_On(void)
|            void Main_TP_Off(void)
|            void Main_TP_Toggle(void)
|
+------------------------------------------------------------------------------
| Purpose:    Debug Test Routines to drive test points for Scope Monitoring
|
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
|
+------------------------------------------------------------------------------
*/
void MAIN_TP_On(void)
{
    #ifdef    CONFIGURE_DEBUG_PIN_ENABLED
        LAT_DEBUG_TP1_PIN = 1;          // Used as Test Point Signal
    #endif
}

void MAIN_TP_Off(void)
{
    #ifdef    CONFIGURE_DEBUG_PIN_ENABLED
        LAT_DEBUG_TP1_PIN = 0;          // Used as Test Point Signal
    #endif
}

void MAIN_TP_Toggle(void)
{
    #ifdef    CONFIGURE_DEBUG_PIN_ENABLED
        LATB ^= LAT_DEBUG_TP1_PIN;    // Used as Test Point Signal
    #endif
}

void MAIN_TP2_On(void)
{
    #ifdef    CONFIGURE_DEBUG_PIN_ENABLED
        LAT_DEBUG_TP2_PIN = 1;          // Used as Test Point Signal
    #endif
}

void MAIN_TP2_Off(void)
{
    #ifdef    CONFIGURE_DEBUG_PIN_ENABLED
        LAT_DEBUG_TP2_PIN = 0;          // Used as Test Point Signal
    #endif
}

void MAIN_TP2_Toggle(void)
{
    #ifdef    CONFIGURE_DEBUG_PIN_ENABLED
        LATB ^= LAT_DEBUG_TP2_PIN;    // Used as Test Point Signal
    #endif
}


#ifdef CONFIGURE_DIAG_ACTIVE_FLAGS

#define CONFIGURE_TX_UART           // Echo out Serial Port

void Main_Diag_Active_Flags(void)
{
    uint8_t active_id = 0;
    uint8_t active_on = 0;
    
    FLAG_DIAG_ACTIVE_RST = 0;
    main_diag_active_timer = MAIN_TIMER_200_MS;

    if(!main_diag_count)
    {
        #ifdef CONFIGURE_TX_UART
            // Output via Uart for Testing only
            SER_Send_String("\rActive On\r");
        #endif
    }

    // Test Active Flags
    if(Flags_Active1.ALL)
    {
        active_on = 1;

        if(!main_diag_count)
        {
            active_id = 0x31;

            #ifdef CONFIGURE_TX_UART
                // Output via Uart for Testing only
                SER_Send_Int('1',Flags_Active1.ALL,16);
                SER_Send_String("  ");
            #endif
        }
    }

    if(Flags_Active2.ALL)
    {
        if(0x80 == Flags_Active2.ALL)
        {
            // Serial Port Only Active, don't set active_on
        }
        else
        {
            active_on = 1;
        }

        if(!main_diag_count)
        {
            active_id = 0x32;

            #ifdef CONFIGURE_TX_UART
                // Output via Uart for Testing only
                SER_Send_Int('2',Flags_Active2.ALL,16);
                SER_Send_String("  ");
            #endif
        }
    }

    if(Flags_Active3.ALL)
    {
        active_on = 1;

        if(!main_diag_count)
        {
            active_id = 0x33;

            #ifdef CONFIGURE_TX_UART
                // Output via Uart for Testing only
                SER_Send_Int('3',Flags_Active3.ALL,16);
                SER_Send_String("  ");
            #endif
        }
    }

    if(Flags_Active4.ALL)
    {
        active_on = 1;

        if(!main_diag_count)
        {
            active_id = 0x34;

            #ifdef CONFIGURE_TX_UART
                // Output via Uart for Testing only
                SER_Send_Int('4',Flags_Active4.ALL,16);
                SER_Send_String("  ");
            #endif
        }
    }

    if(Flags_Active5.ALL)
    {
        active_on = 1;

        if(!main_diag_count)
        {
            active_id = 0x35;

            #ifdef CONFIGURE_TX_UART
                // Output via Uart for Testing only
                SER_Send_Int('5',Flags_Active5.ALL,16);
                SER_Send_String("  ");
            #endif
        }
    }

    if(Flags_Active6.ALL)
    {
        active_on = 1;

        if(!main_diag_count)
        {
            active_id = 0x36;

            #ifdef CONFIGURE_TX_UART
                // Output via Uart for Testing only
                SER_Send_Int('6',Flags_Active6.ALL,16);
                SER_Send_String("  ");
            #endif
        }
    }

    if(Flags_Active7.ALL)
    {
        active_on = 1;

        if(!main_diag_count)
        {
            active_id = 0x37;

            #ifdef CONFIGURE_TX_UART
                // Output via Uart for Testing only
                SER_Send_Int('7',Flags_Active7.ALL,16);
                SER_Send_String("  ");
            #endif
        }
    }

    
#ifdef CONFIGURE_DIAG_GRN_LED_STATES    
    if(!main_diag_count)
    {
        SER_Send_String("\rGLED Stat");
        SER_Send_Int('e', GLED_Get_State(), 10);
        SER_Send_String("\r");
    }
#endif
    
#ifdef CONFIGURE_DIAG_RED_LED_STATES    
    if(!main_diag_count)
    {
        SER_Send_String("\rRLED Stat");
        SER_Send_Int('e', RLED_Get_State(), 10);
        SER_Send_String("\r");
    }

    #ifdef CONFIGURE_UNDEFINED 
        // TODO: Test Only, undefine later
        if(FLAG_LED_PRO_N_PROGRESS)
        {
            SER_Send_String("\rPNP\r");
        }
    #endif
    
#endif
    
#ifdef CONFIGURE_DIAG_AMB_LED_STATES    
    if(!main_diag_count)
    {
        SER_Send_String("\rALED Stat");
        SER_Send_Int('e', ALED_Get_State(), 10);
        SER_Send_String("\r");
    }
#endif
    
#ifdef CONFIGURE_WIFI
    #ifndef CONFIGURE_UNDEFINED
        if(!main_diag_count)
        {
            
        #ifndef CONFIGURE_UNDEFINED
            if(FLAG_PREV_JOINED)
            {
                SER_Send_String("\rPREV_JOINED ON");
            }
            else
            {
                SER_Send_String("\rPREV_JOINED off");
            }
            
            if(FLAG_STAND_ALONE_MODE)
            {
                SER_Send_String("\rSTAND_ALONE ON");
            }
            else
            {
                SER_Send_String("\rSTAND_ALONE off");
            }
        #endif
            
            // TODO: Undefine later BC: Test Only
            if(FLAG_WIRELESS_DISABLED)
            {
                SER_Send_String("\rWiFi_Off\r");
            }
            else if(FLAG_PREV_JOINED && !FLAG_APP_SEARCH_MODE && 
                    !FLAG_STAND_ALONE_MODE && !FLAG_APP_JOIN_MODE)
            {
                SER_Send_String("\rWiFi_Connected\r");
            }
            
            if(FLAG_APP_SEARCH_MODE && !FLAG_PREV_JOINED)
            {
                SER_Send_String("\rAP Search Mode\r");
            }
            if(FLAG_APP_SEARCH_MODE && FLAG_PREV_JOINED)
            {
                SER_Send_String("\rStealth AP Search Mode\r");
            }
            if(FLAG_APP_JOIN_MODE && !FLAG_PREV_JOINED)
            {
                SER_Send_String("\rCloud Connect Mode\r");
            }
            if(FLAG_APP_JOIN_MODE && FLAG_PREV_JOINED)
            {
                SER_Send_String("\rStealth Cloud Connect Mode\r");
            }
        }
    #endif
#endif
    
    
#ifdef CONFIGURE_WIRELESS_MODULE    
    if(Flags_Remote_Alerts.ALL)
    {
        active_on = 1;

        if(!main_diag_count)
        {
            active_id = 0x41;

            #ifdef CONFIGURE_TX_UART
                // Output via Uart for Testing only
                SER_Send_Int('A',Flags_Remote_Alerts.ALL,16);
                SER_Send_String("  ");
           #endif
        }
    }

    if(Flags_Remote_Flags1.ALL)
    {
        active_on = 1;

        if(!main_diag_count)
        {
            active_id = 0x42;

            #ifdef CONFIGURE_TX_UART
                // Output via Uart for Testing only
                SER_Send_Int('B',Flags_Remote_Flags1.ALL,16);
                SER_Send_String("  ");
            #endif
        }
    }

    if(FLAG_STATUS_RMT_SMK_HW_RCVD || FLAG_STATUS_RMT_CO_HW_RCVD)
    {
        active_on = 1;

        if(!main_diag_count)
        {
            active_id = 0x43;

            #ifdef CONFIGURE_TX_UART
                if(FLAG_STATUS_RMT_SMK_HW_RCVD)
                {
                    // Output via Uart for Testing only
                    SER_Send_Int('C',0x01,16);
                    SER_Send_String("  ");
                }
                if(FLAG_STATUS_RMT_CO_HW_RCVD)
                {
                    // Output via Uart for Testing only
                    SER_Send_Int('C',0x02,16);
                    SER_Send_String("  ");
                }
             #endif
        }
    }
#endif
    
    //
    // Test to see if everything except the Serial Port was In-Active
    //
    if(!active_on)
    {
        SER_Send_String("\rActive Off\r ");

        // Enable Serial Port Off
        FLAG_SERIAL_PORT_OFF = 1;
    }
    else
    {
        if(!main_diag_count)
        {
            SER_Send_Prompt();
        }
    }
    
    if(active_id)
    {
        // For Unused Warning Suppression
        active_id = 0;
    }
        
    // Update Data Output Counter for next time   
    if(!main_diag_count)  
    {
        // Reset Counter
        main_diag_count = MAIN_DIAG_COUNT_INIT;
    }
    else
    {
        main_diag_count--;
    }
}
#endif


/*
+------------------------------------------------------------------------------
| Function:  void interrupt main_interrupt_routine(void)
+------------------------------------------------------------------------------
| Purpose:    Handle CCI Communication Interrupts
|             Handle Green and Amber LED Dim feature
|             Timer Interrupts for 1MS Timer
|             Interrupt on Change for Button Press and Interconnect Hardware
|
+------------------------------------------------------------------------------
| Parameters:    none
|
+------------------------------------------------------------------------------
| Return Value:  none
|
|
+------------------------------------------------------------------------------
*/
void interrupt main_interrupt_routine(void)
{
    
    if(!FLAG_MODE_ACTIVE)
    {
        // Low Power Mode Service Routine
        
        // Interrupt Flag checks to determine Interrupt Source
        #ifdef CONFIGURE_WIRELESS_MODULE
        
            //Check CCI Interrupt and service the interrupt
            if(CCI_PIN_INTERRUPT_FLAG)
            {
                //    
                // Should never happen with AC only powered WiFi
                //  so simply clear INT flag and continue on WiFi models
                //    
                #ifndef CONFIGURE_WIFI

                    // CCI_Phy_Receive uses Timer 2
                    //  To Time handshaking T2 needs to be running

                    #ifdef CONFIGURE_DEBUG_LP_CCI_RESPONSE
                        MAIN_TP2_On();      // TP2 On to Begin
                    #endif

                    if(!FLAG_WIRELESS_DISABLED)
                    {
                        // Receive this 16 bit packet
                        if(CCI_RECEIVE_SUCCESS == CCI_Phy_Receive())
                        {
                             // Set flag for foreground.
                            CCI_Flags.FLAG_CCI_PACKET_RECEIVED = 1;

                            FLAG_CCI_EDGE_DETECT = 1;
                            FLAG_WAKE_FOR_CCI = 1;

                            #ifdef CONFIGURE_DIAG_ACTIVE_FLAGS
                                FLAG_CCI_ACTIVE = 1;
                            #endif

                        }
                    }
                #endif

                // Clear interrupt flag 
                CCI_PIN_INTERRUPT_FLAG = 0;

                // Reset the Global IOC Flag
                IOC_INTERRUPT_FLAG = 0;
            }
        #endif

        // Test Button Interrupt
        if(IOCBFbits.IOCBF3)
        {
            // Setting Edge Detect should enter Active Mode
           FLAG_BUTTON_EDGE_DETECT = 1;
           IOC_INTERRUPT_FLAG = 0;
           IOCBFbits.IOCBF3 = 0;
        }

        #ifdef CONFIGURE_INTERCONNECT
            // Test Interconnect Interrupt
            if(IOCBFbits.IOCBF0)
            {
               // Setting Edge Detect should enter Active Mode
               FLAG_INTCON_EDGE_DETECT = 1;

               IOC_INTERRUPT_FLAG = 0;
               IOCBFbits.IOCBF0 = 0;
            }
        #endif
        
        // If this Interrupt is from Sleep Timer, clear the Timer Int Flag
        if(PIR4bits.TMR1IF)
        {
            PIR4bits.TMR1IF = 0;
        }
        
    }
    else
    {
        // Active Mode Interrupt Service
        
        //Timer0 Timer Interrupts for 1MS Timer
        #ifdef CONFIGURE_DEBUG_INTS
            MAIN_TP_On();
        #endif

        // Add Int Flag checks to determine Interrupt Source
        #ifdef CONFIGURE_WIRELESS_MODULE
            
            //Check CCI Interrupt and service the interrupt
            if(CCI_PIN_INTERRUPT_FLAG)
            {
                #ifdef CONFIGURE_UNDEFINED
                    // Test only, to see if we ever hung in this routine 
                    //  before a WDT Reset
                    SER_Send_String("I-");
                #endif
                
                // Validate that WiFi AC Power is on 
                #ifdef CONFIGURE_WIFI
                    // WiFi power?
                    if(FALSE == A2D_Test_AC_Sample())
                    {
                        
                        #ifdef CONFIGURE_TEST_CODE
                            log_seq(9);
                        #endif
                        
                        // Oops no AC, this is invalid data interrupt
                        
                        #ifdef CONFIGURE_UNDEFINED
                            // TODO: Undefine Later, BC: TEst Only
                            SER_Send_String("WiFi Off INT");
                            SER_Send_Prompt();
                        #endif
                        
                        // Turn OFF CCI Data Pin Interrupt Now
                        // CCI Data Negative edge bit-1 OFF
                        IOCBN = 0b00000000;     
                        
                        // Notify Wireless Task to disable WiFi Port
                        FLAG_DISABLE_WIFI = 1;
                        FLAG_WIRELESS_DISABLED = 1;
                        // Run Wireless Monitor Task ASAP
                        MAIN_Reset_Task(TASKID_APP_STAT_MONITOR);      
                        
                        // Reset APP Monitor State
                        APP_Status_Monitor_Reset();
                        
                        // Insure we remain in Active mode until
                        //  Port is completely disabled
                        FLAG_ACTIVE_TIMER = 1;
                        if(MAIN_ActiveTimer < TIMER_ACTIVE_MINIMUM_2_SEC)
                        {
                            // Extend time to remain in Active Mode
                            MAIN_ActiveTimer = 
                                    TIMER_ACTIVE_MINIMUM_2_SEC;
                        }
                        
                    }
                    
                    if(!FLAG_WIRELESS_DISABLED)
                    {
                        // While receiving this 16 bit packet, we want to prevent
                        //  additional data edge interrupts.
                        if(CCI_RECEIVE_SUCCESS == CCI_Phy_Receive())
                        {
                             // Set flag for foreground.
                            CCI_Flags.FLAG_CCI_PACKET_RECEIVED = 1;
                        }
                    }

                #else
                    if(!FLAG_WIRELESS_DISABLED)
                    {
                        // While receiving this 16 bit packet, we want to prevent
                        //  additional data edge interrupts.
                        if(CCI_RECEIVE_SUCCESS == CCI_Phy_Receive())
                        {
                             // Set flag for foreground.
                            CCI_Flags.FLAG_CCI_PACKET_RECEIVED = 1;
                        }
                    }
                #endif

                // Clear interrupt flag 
                CCI_PIN_INTERRUPT_FLAG = 0;

                // Reset the Global IOC Flag
                IOC_INTERRUPT_FLAG = 0;

                #ifdef CONFIGURE_UNDEFINED
                    // Test only, to see if we ever hung in this routine 
                    //  before a WDT Reset
                    SER_Send_String("-I ");
                #endif
                
            }

        #endif

        #ifdef CONFIGURE_WIRELESS_MODULE            
           // Test:     
           // BC: 2021 - run Transport Manager immediately on received CCI packet   
           // To speed up response time.
           //     
           if(CCI_Flags.FLAG_CCI_PACKET_RECEIVED)
           {
                //
                // Just received a CCI message (respond ASAP)
                //
                if(!FLAG_WIRELESS_DISABLED)
                {
                    // AMP Transport Manager is called every 1ms in Active
                    // Mode. Here we also call it if a Packet has been received
                    // We need to get in and out of Transport manager
                    // ASAP  (under 50 usecs)
                    AMP_Transport_Manager();
                }
           }
        #endif

       // Check to see if this is a Timer Interrupt
       if(TIMER_ONE_MS_INT_FLAG)
       {
            //
            // 1 MS Timer Ints only active During Active Mode
            // Reset 1 MS Time (1 ms timer)
            //
            if(T4_CLK_SOSC == main_t4_select)
            {
                MAIN_TIMER_ONE_MS = TIME_1_MS_1;
            }
            else
            {
                MAIN_TIMER_ONE_MS = TIME_1_MS_2;
            }

            #ifdef CONFIGURE_DEBUG_INT_1MS
                // Test Only
                MAIN_TP_On();
                PIC_delay_us(DBUG_DLY_20_USec);
                MAIN_TP_Off();
            #endif

            #ifdef CONFIGURE_WIRELESS_MODULE
               #ifdef CONFIGURE_WIFI
                    if((0 == FLAG_WIRELESS_DISABLED) && 
                       (0 == FLAG_DISABLE_WIFI))
                    {
                        // AMP Transport Manager is called every 1ms in Active
                        // Mode. We need to get in and out of Transport manager
                        // ASAP  (under 50 usecs)
                        AMP_Transport_Manager();
                    }
                #else
                    if(!FLAG_WIRELESS_DISABLED)
                    {
                        // AMP Transport Manager is called every 1ms in Active
                        // Mode. We need to get in and out of Transport manager
                        // ASAP  (under 50 usecs)
                        AMP_Transport_Manager();
                    }
                #endif
           #endif


            #ifdef CONFIGURE_UNDEFINED 
                // Does not look like this is needed    
                if(FLAG_TM_NO_ACTIONS)
                {
                    // Postpone PWM updates until no CCI actions
                    // Check for active LED profile or Green/Amber Led Dim
                    LED_PWM_Control();
                }
            #else
                    // Check for active LED profile or Green/Amber Led Dim
                    LED_PWM_Control();
            #endif

            #ifdef CONFIGURE_STROBE_1_MS_TASK
                //    
                // For tighter timing call Strobe 
                //     Task for Strobe Timing updates if needed
                //
               
                #ifdef CONFIGURE_UNDEFINED 
                    // Test only
                    // Update Strobe Timers and Process Strobe Sync if detected
                    MAIN_TP_Toggle();
                #endif

                STROBE_Task();  

            #endif

            // Timer 1 MS Interrupt Flag reset
            TIMER_ONE_MS_INT_FLAG = 0;

       }    // End 1 ms INT code
       else
       {
             /*
              * To get in and out of CCI INTs faster, we only
              *    test these Interrupt On Change bits when NOT during
              *    CCI TM calls
              *
              * Monitor Interrupt on Change Flags as a source of the Interrupt
              *
              * The Test Button will generate an Interrupt on Change in
              *    Active Mode, Clear the IOC flags
              */
             if(IOCBFbits.IOCBF3)
             {
                 // Button Test is on Bit 3 (clear the IOC flags)
                 IOCBFbits.IOCBF3 = 0;

                 FLAG_BUTTON_EDGE_DETECT = 1;

                 // Reset the IOC Flag
                 IOC_INTERRUPT_FLAG = 0;
             }

            #ifdef CONFIGURE_INTERCONNECT
             // INT_IN Pin will generate an Interrupt on Change in Active Mode
             // Clear the IOC flags
             if(IOCBFbits.IOCBF0)
             {
                 // INT_IN is on Bit 0 (clear the IOC flags)
                 IOCBFbits.IOCBF0 = 0;


                 FLAG_INTCON_EDGE_DETECT = 1;

                 // Reset the IOC Flag
                 IOC_INTERRUPT_FLAG = 0;

             }
            #endif

       }

        #ifdef CONFIGURE_DEBUG_INTS
            MAIN_TP_Off();
        #endif
    }
}

#ifdef CONFIGURE_UNDEFINED
// TODO: Undefine  BC: Initialized in startup code 
//
// Initialize all Flags except non-reset flags
//
void main_clear_flags(void)
{
    // Clear flag structures
    Flags1.ALL=0;
    Flags2.ALL=0;
    Flags3.ALL=0;
    Flags4.ALL=0;
    Flags5.ALL=0;
    Flags6.ALL=0;
    Flags7.ALL=0;
    Flags8.ALL=0;
    Flags9.ALL=0;
    Flags10.ALL=0;
    Flags11.ALL=0;
    Flags12.ALL=0;
    Flags13.ALL=0;
    Flags14.ALL=0;
    Flags15.ALL=0;
    Flags16.ALL=0;
    Flags17.ALL=0;
    Flags18.ALL=0;
    Flags19.ALL=0;
    Flags20.ALL=0;

    bALG_Status.ALL=0;
    Flags_Active1.ALL=0;
    Flags_Active2.ALL=0;
    Flags_Active3.ALL=0;
    Flags_Active4.ALL=0;
    Flags_Active5.ALL=0;
    Flags_Active6.ALL=0;
    Flags_Active7.ALL=0;


    // Wireless Flags
    Flags_Remote_Alerts.ALL=0;
    Flags_Remote_Flags1.ALL=0;

};
#endif