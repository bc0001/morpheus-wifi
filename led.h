/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        led.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for led.c module
|               This file contains LED definitions.
|               Green, Red and Amber will be supported
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


#ifndef	LED_H_
#define LED_H_

/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include "flags.h"


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/
// Led I/O
#define GREEN_LED_ON        LAT_GREEN_LED_PIN = 1;
#define GREEN_LED_OFF       LAT_GREEN_LED_PIN = 0;
#define RED_LED_ON          LAT_RED_LED_PIN = 1;
#define RED_LED_OFF         LAT_RED_LED_PIN = 0;

// Define LED Profile Status Flags
#define FLAG_LED_PRO_N_PROGRESS     led_profile.status._0

// These Flags Select the Color, both for Amber
#define FLAG_LED_PRO_RED            led_profile.status._1
#define FLAG_LED_PRO_GRN            led_profile.status._2

// These Flags used to Ramp up or down
#define FLAG_LED_PRO_DIR_RED        led_profile.status._3
#define FLAG_LED_PRO_DIR_GRN        led_profile.status._4

// Rate  (set for faster/slower fade, 3 speeds avail)
#define FLAG_LED_PRO_FAST           led_profile.status._5
#define FLAG_LED_PRO_SLOW           led_profile.status._6


// Profile End of Table indicator
#define LED_PROFILE_END     0xFF

#define LED_JOIN_PING_12_SEC        (uchar)120


/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/

// Define LED Task States
enum
{
    STATE_LED_INIT = 0,
    STATE_LED_LB_CAL,
    STATE_LED_CAL_BLINKING,
    STATE_LED_RESET_PROFILE,
    STATE_LED_NORM_BLINKING,
    STATE_LED_SMK_CAL_INIT,
    STATE_LED_SMK_CAL,
    STATE_LED_AMB_5_SEC_BLINK,
    STATE_LED_SMK_HUSH_MODE,
    STATE_LED_ALM_MEMORY,
    STATE_LED_WL_JOIN,
    STATE_LED_WL_JOIN_RFD,
    STATE_LED_WL_SEARCH,
    STATE_LED_WL_OOB,
    STATE_LED_WL_DETECTION_ALERT,
    STATE_LED_WL_SHORT_ALERT,
    STATE_LED_WL_PANIC_ALERT,
    STATE_LED_WL_IDENTIFY,
    STATE_LED_BLINK_SIZE,
    STATE_LED_BLINK_ERR_CODE,
    STATE_LED_NETWORK_CLOSED,
    STATE_LED_NETWORK_NOT_FOUND,
    STATE_LED_LOW_BATTERY,
    STATE_LED_GREEN_FADE_ON,
    STATE_LED_GREEN_FADE_OFF
};

// Defined LED Profile IDs
enum
{
    LED_PRO_NONE = 0x00,
    LED_PRO_FADE_ON_RED_1 = 0x01,
    LED_PRO_FADE_OFF_RED_1= 0x02,
    LED_PRO_FADE_ON_GRN_1= 0x03,
    LED_PRO_FADE_OFF_GRN_1= 0x04,
    LED_PRO_FADE_ON_AMB_1= 0x05,
    LED_PRO_FADE_OFF_AMB_1= 0x06,
    LED_PRO_FADE_ON_RED_2= 0x07,
    LED_PRO_FADE_OFF_RED_2= 0x08,
    LED_PRO_FADE_ON_GRN_2= 0x09,
    LED_PRO_FADE_OFF_GRN_2= 0x0A,
    LED_PRO_FADE_ON_AMB_2= 0x0B,
    LED_PRO_FADE_OFF_AMB_2= 0x0C,
    LED_PRO_FADE_ON_RED_3= 0x0D,
    LED_PRO_FADE_OFF_RED_3= 0x0E,
    LED_PRO_FADE_ON_GRN_3= 0x0F,
    LED_PRO_FADE_OFF_GRN_3= 0x10,
    LED_PRO_FADE_ON_AMB_3= 0x11,
    LED_PRO_FADE_OFF_AMB_3= 0x12

};


// Define the LED PWM table structures
typedef struct
{
    uchar red_cnt_on;  // Red PWM pulse time
    uchar grn_cnt_on;  // Green PWM pulse time
    uint  cnt_pwm;     // PWM Interval
                       // (x 1 ms)
}LED_PWM_Profile_t;

typedef struct
{
    volatile uchar ID;
    volatile uchar index_red;
    volatile uchar index_grn;
    volatile uchar spare;
    volatile Flags_t status;
    volatile const LED_PWM_Profile_t * p_addr;
}LED_Profile_t;


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/
// Create instance of the led_profile structure
LED_Profile_t led_profile;


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/

uint GLED_Manager_Task(void);
uint RLED_Manager_Task(void);
uint ALED_Manager_Task(void);
void GLED_On(void);
void GLED_Off(void);
void GLED_Rst(void);
void RLED_On(void);
void RLED_Off(void);
void ALED_On(void);
void ALED_Off(void);

uchar LED_Get_Red_State(void);
uint LED_Blink_Task(void);
void LED_Init_PWM(void);
void LED_PWM_Control(void);
void LED_LP_green_blink(void);

#ifdef CONFIGURE_WIRELESS_MODULE
    #ifndef CONFIGURE_WIFI
        void LED_Set_Join_Ping(uchar time);
    #endif
#endif
    
#ifdef CONFIGURE_DIAG_GRN_LED_STATES 
    // Test Only
    uchar GLED_Get_State(void);
#endif  
    
#ifdef CONFIGURE_DIAG_RED_LED_STATES 
    // Test Only
    uchar RLED_Get_State(void);
#endif    
    
#ifdef CONFIGURE_DIAG_AMB_LED_STATES 
    // Test Only
    uchar ALED_Get_State(void);
#endif    
    

#endif  // LED_H_

