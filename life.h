/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        life.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for life.c module
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


#ifndef	LIFE_H
#define	LIFE_H


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/
    
	#define	LIFE_EXPIRATION_DAY  (uint)(3650 + 100)
    #define LFE_DAY_ZERO         (uint)0

        // EOL Mode is now 7 Days Max, then EOL is Fatal  (No Hush allowed)
	#define	LIFE_UL_EXTENSION    (uint)(LIFE_EXPIRATION_DAY + 7)

 

/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/

    uint LIFE_Get_Current_Day(void);
    void LIFE_Check(void);
    void LIFE_Init(void);
    void LIFE_Init_Life_Count(void);

    #ifdef	CONFIGURE_EOL_HUSH
        void LIFE_Init_EOL_Hush(void);
    #endif

    #ifdef CONFIGURE_MANUAL_LIFE_COMMAND    
        void LIFE_Set_Accel_Life(void);        
        void LIFE_Clr_Accel_Life(void);
        void LIFE_Set_Eol_Test(void);
        void LIFE_Set_Life_Days(uint life_days);
    #endif

#endif  // LIFE_H

