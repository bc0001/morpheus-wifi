/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        comeasure.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  CO Sensor measurement functionality
|
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include    "common.h"
#include    "comeasure.h"
#include    "a2d.h"
#include    "systemdata.h"
#include    "sound.h"
#include    "fault.h"
#include    "cocompute.h"
#include    "cocalibrate.h"
#include    "algorithm.h"
#include    "photosmoke.h"

#include    "led.h"



/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

#define	CO_SAMPLE_INT_1000_MS	(uint)1000	// 1 second Interval
#define	CO_SAMPLE_INT_3000_MS	(uint)3000	// 3 second Interval
#define	CO_SAMPLE_INT_5000_MS	(uint)5000	// 5 second Interval

#define	STABILIZE_INITIAL_1_SEC     (uint)1000	// 1 Second
#define	MAX_STABILIZE_TIME_45_SEC   (uchar)45   /* 45 seconds(+ last 10 seconds)
                                                = 55 seconds total*/
#define STABILIZE_ADDITIONAL_10_SEC	(uint)10000	// 10 Seconds

#ifdef CONFIGURE_CO
//
// Note:  These were originally defined in A/D counts
//        They need to be scaled to mvolts since we convert
//        A/D counts to millivolts in the measurement
//
//        At 3.0 volts, 1 A/D count = 3.0/1024 = 2.93mv
//        At 2.5 volts, 1 A/D count = 2.5/1024 = 2.44mv
//          3.0 volts (DC model)
//        250 A/D counts = 250*2.93 = 733 mv
//        300 A/D counts = 300*2.93 = 879 mv
//        400 A/D counts = 400*2.93 = 1172 mv
//
//          2.5 volts (AC model)
//        250 A/D counts = 250*2.44 = 610 mv
//        300 A/D counts = 300*2.44 = 732 mv
//        400 A/D counts = 400*2.44 = 976 mv
//
//
//        For now pick a value between the 2 VDD voltages

#ifdef CONFIGURE_NO_REGULATOR
    #define SENSOR_SHORT_LIMIT          (uint)700   // mv
    #define VMEAS_AD_CHARGE_THRESHOLD	(uint)500   // mv
    //// (0.6 volt Output is Off the Rail)
    #define VMEAS_CIRCUIT_STABILIZE     (uint)600	
#else
    #define SENSOR_SHORT_LIMIT          (uint)250
    #define VMEAS_AD_CHARGE_THRESHOLD	(uint)300
    // 400 counts (Output is Off the Rail)
    #define VMEAS_CIRCUIT_STABILIZE 	(uint)400	
#endif

// These values should be OK for mvolt measurements
#ifdef CONFIGURE_NO_REGULATOR
    #define	VHIGH_TEST_DIFFERENTIAL		(uint)100
    #define	VHIGH_TEST_MAX              (uint)2000
    #define	VLOW_TEST_DIFFERENTIAL		(uint)70
#else
    #define	VHIGH_TEST_DIFFERENTIAL		(uint)50
    #define	VHIGH_TEST_MAX              (uint)1024
    #define	VLOW_TEST_DIFFERENTIAL		(uint)30
#endif


// Normal Test Interval
#define CO_TEST_INTERVAL_90_SEC     (uchar)90
// Short Test Interval
#define CO_TEST_INTERVAL_10_SEC     (uchar)10
#define CO_TEST_INTERVAL_15_SEC     (uchar)15
#define CO_TEST_INTERVAL_20_SEC     (uchar)20

#define	MEAS_PPM_TEST_THRESHOLD		(uint)30    // in PPM


#define	TEST_LENGTH_COUNTER		(uchar)20
#define	VHIGH_TEST_SAMPLE		(TEST_LENGTH_COUNTER - 1)
#define	VLOW_TEST_SAMPLE		(uchar)2

// Charge Pulse Timing (usecs)
#define CO_CHARGE_PULSE_HI      (uint)60
#define CO_CHARGE_PULSE_LO      (uint)400
#define NUM_CHARGE_PULSES       (uint)100
#define AD_10_COUNTS            (uint)10


// This is the number of samples that will be averaged during CO measurement.
#define	NUM_SAMPLES_CO_AVG      (uint)4


#ifdef CONFIGURE_NO_REGULATOR
    // A/D Reference for PIC = VDD
    // FVR to calcultae VDD = 1024 mv
    #define FVR_MVOLTS  (uint)1024

    // FVR_MVOLTS * 1024
    #define FVR_FACTOR  (unsigned long)1048576

    //Max battery voltage
    #define VBAT_MAX    (uint)3200

    #define AD_20_MV    (uint)16


#endif


/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/
// Global variables
uint CO_wPPM = 0;
uint CO_raw_counts;
uint CO_raw_avg_counts;

// This is typical CAV value
uint CO_vout_baseline = CALIBRATION_OFFSET_TYPICAL;   

#ifdef CONFIGURE_NO_REGULATOR
    uint CO_vdd_calc;
#endif

#ifdef CONFIGURE_MANUAL_PPM_COMMAND
    uint CO_manual_PPM_value = 0;
#endif


/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
static uchar co_state = CO_MEAS_INIT;
static uchar co_test_counter;
static uint co_prev_raw_co_count;
static uint co_test_baseline;
static uchar co_sec_timer;
static uchar co_sec_time_target;


#ifdef CONFIGURE_NO_REGULATOR
    static unsigned int CO_FVR_Measured;
#endif

/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/
uint co_meas_make_measurement(void);
void co_meas_charge_pulse(void);
void co_timer_init(uchar);

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/



/*
+------------------------------------------------------------------------------
| Function:      co_timer_init
+------------------------------------------------------------------------------
| Purpose:       Initialize and the CO Second Timer 
|
|                CO timer is set to the current MAIN_OneSecTimer value
|
+------------------------------------------------------------------------------
| Parameters:    Inputs - time (targeted timer expiration time)
|
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void co_timer_init(uchar time)
{
    // Initialize CO Second Timer (up to 255 seconds)
    co_sec_timer = MAIN_OneSecTimer;
    co_sec_time_target = time;
 }


/*
+------------------------------------------------------------------------------
| Function:      CO_Measure_Task
+------------------------------------------------------------------------------
| Purpose:       State machine for CO Sensor measurement
|                Controls CO Measurements and Co Sensor Supervision
|
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  CO Measurement task return time (in Task TIC units)
+------------------------------------------------------------------------------
*/

uint CO_Measure_Task(void)
{
  #ifdef CONFIGURE_DEBUG_CO_MEAS_STATES
    // Test Only
    for(uchar i = (co_state + 1); i > 0; i--)
    {
        // Test Only
        MAIN_TP2_On();
        PIC_delay_us(DBUG_DLY_25_USec);
        MAIN_TP2_Off();
    }
  #endif

    switch (co_state)
    {
        case CO_MEAS_INIT:

            if(FLAG_NO_SMOKE_CAL)
            {
                // Smoke Cal must be completed
                return MAIN_Make_Return_Value(STABILIZE_INITIAL_1_SEC);
            }

            // Init the CO Stabilization Time
            co_timer_init(MAX_STABILIZE_TIME_45_SEC);
            
            CO_raw_avg_counts = 0;
            co_state++;

            return MAIN_Make_Return_Value(STABILIZE_INITIAL_1_SEC);


        case CO_MEAS_STABILIZE:

            if(FLAG_PTT_ACTIVE)
            {
                // If PTT, Proceed to Sample State now
                co_state++;
                
                // Init the CO Health test Timer
                co_timer_init(CO_TEST_INTERVAL_90_SEC);

                return MAIN_Make_Return_Value(CO_SAMPLE_INT_1000_MS);
            }

            if( ((uchar)(MAIN_OneSecTimer - co_sec_timer)) >= 
                    co_sec_time_target ) 
            {
                // Since the stabilize counter has expired, there must be 
                // something wrong.  Test for Fault or High CO
                // Also if in 400PPM Co Final Test, the unit may be powered
                //  up in 400PPM CO, so continue to measurement state
                if(!FLAG_POWERUP_COMPLETE || !FLAG_SENSOR_FAULT_RST ||
                    FLAG_CALIBRATION_UNTESTED )
                {
                    // Proceed to Sample State (Assume this is high CO)
                    co_state++;
                
                    // Init the CO Health test Timer
                    co_timer_init(CO_TEST_INTERVAL_90_SEC);
                    

                    return MAIN_Make_Return_Value(CO_SAMPLE_INT_1000_MS);
                }
                else
                {
                    FLAG_SENSOR_FAULT_RST = 0;

                    if(!FLAG_FAULT_FATAL)
                    {
                        #ifndef CONFIGURE_NO_FAULT
                            //
                            // If not already in Fatal Fault 
                            // Setup Co measurement for CO Short
                            //
                        
                            // Init CO measurement for Re-testing CO short
                            CO_raw_counts = ((SYS_RamData.CO_Cal.offset_LSB) + 
                                        (256 * SYS_RamData.CO_Cal.offset_MSB));
                            
                            co_state++;
                        #endif
                    }
                    else
                    {
                        // If Fatal Fault, Proceed to Sample State
                        co_state++;
                
                        // Init the CO Health test Timer
                        co_timer_init(CO_TEST_INTERVAL_90_SEC);
                    
                    }

                }
            }
            else
            {
                CO_raw_counts = co_meas_make_measurement();

                if(FLAG_PTT_ACTIVE)
                {
                    // If PTT, Proceed to Sample State now
                    co_state++;
                
                    // Init the CO Health test Timer
                    co_timer_init(CO_TEST_INTERVAL_90_SEC);

                    return MAIN_Make_Return_Value(CO_SAMPLE_INT_1000_MS);
                }

                // See if the sensor output is less than the stabilize 
                // threshold.  If so go to next state.
                // If not, stay in this state to measure again.
                if( VMEAS_CIRCUIT_STABILIZE > CO_raw_counts )
                {
                    co_state++;
                
                    // Init the CO Health test Timer
                    co_timer_init(CO_TEST_INTERVAL_90_SEC);

                    return MAIN_Make_Return_Value(STABILIZE_ADDITIONAL_10_SEC);
                }
            }
        break;

        case CO_MEAS_SAMPLE:

            FLAG_SENSOR_CIRCUIT_STABLE = 1;
            FLAG_POWERUP_COMPLETE =1;

            // Delay CO measurement after battery test Pulse
            //  1st measurement after Battery Test was affected.
            if(FLAG_BAT_TEST_DLY)
            {
                if(FLAG_MODE_ACTIVE)
                {
                    return MAIN_Make_Return_Value(CO_SAMPLE_INT_1000_MS);
                }
                else
                {
                    return MAIN_Make_Return_Value(CO_SAMPLE_INT_5000_MS);
                }
            }

            // Save last raw reading for later.
            co_prev_raw_co_count = CO_raw_counts;
            
            CO_raw_counts = co_meas_make_measurement();
            
            #ifdef CONFIGURE_UNDEFINED
                //TODO: Test Only
                SER_Send_String("\r Prev_Count = ");
                SER_Send_Int(' ', co_prev_raw_co_count, 10);
                SER_Send_String("\r");

                SER_Send_String("\r Current_Count = ");
                SER_Send_Int(' ', CO_raw_counts, 10);
                SER_Send_String("\r");
            #endif

            CO_raw_avg_counts = ( (((unsigned long)CO_raw_avg_counts *
                    (NUM_SAMPLES_CO_AVG-1)) + CO_raw_counts) /
                     NUM_SAMPLES_CO_AVG );

            if(FLAG_PTT_ACTIVE)
            {
                // If PTT calculate PPM and return
                COMP_Co_Calc_PPM(CO_raw_counts);

                return MAIN_Make_Return_Value(CO_SAMPLE_INT_1000_MS);
            }


            //If CO Sensor Fault Status skip Short Test
            // and check Sensor Test count expiration
            if(!(FLAG_CO_FAULT_PENDING && FLAG_FAULT_FATAL))
            {
                // No CO Short testing during CO Calibration
                if( FLAG_CALIBRATION_COMPLETE && !FLAG_CALIBRATION_UNTESTED )
                {
                  #ifdef CONFIGURE_MANUAL_PPM_COMMAND
                    // If the rate of rise is greater than the limit, start
                    // short processing.
                    if((CO_raw_counts > co_prev_raw_co_count) &&
                        (0 == CO_manual_PPM_value))
                  #else
                    // If the rate of rise is greater than the limit, start
                    // short processing.
                    if((CO_raw_counts > co_prev_raw_co_count))
                  #endif
                    {
                      #ifndef CONFIGURE_NO_FAULT
                        if( (CO_raw_counts - co_prev_raw_co_count) >
                                SENSOR_SHORT_LIMIT)
                        {
                            // Short has been detected.
                            FLT_Fault_Manager(FAULT_SENSOR_SHORT);

                            // Reset back to previous measurement so Short
                            // Fault can be tested again on next measurement.
                            // (else there will be no Delta)
                            CO_raw_counts = co_prev_raw_co_count;

                            return MAIN_Make_Return_Value
                                    (CO_SAMPLE_INT_1000_MS);
                         }
                      #endif
                    }
                    else
                    {
                        // No short.  Log a success.
                        FLT_Fault_Manager(SUCCESS_SENSOR_SHORT);
                    }
                }
                COMP_Co_Calc_PPM(CO_raw_avg_counts);

                // Update CO rate of Rise
                ALG_ROR_Monitor_CO();

            }

            // See if it is time to do a sensor test.
            if( ((uchar)(MAIN_OneSecTimer - co_sec_timer)) >= 
                         co_sec_time_target )
            {
                #ifdef CONFIGURE_MANUAL_PPM_COMMAND
                    if(CO_manual_PPM_value != 0)
                    {
                        // Manually set CO being Used, no Sensor Test
                        co_timer_init(CO_TEST_INTERVAL_90_SEC);
                        return MAIN_Make_Return_Value(CO_SAMPLE_INT_1000_MS);

                    }
                #endif

                #ifdef CONFIGURE_DISABLE_CO_HEALTH_TEST
                        // No Sensor Test
                        co_timer_init(CO_TEST_INTERVAL_90_SEC);
                        return MAIN_Make_Return_Value(CO_SAMPLE_INT_1000_MS);

                #else
                    // Test interval has expired.
                    if( (1 == SYS_RamData.CO_Cal.status) ||
                        (0 == SYS_RamData.CO_Cal.status) )
                    {
                        // CO Cal not complete, no Sensor Test
                        co_timer_init(CO_TEST_INTERVAL_90_SEC);
                    }
                    else if(FLAG_CO_FAULT_PENDING)
                    {
                        // This is required for Fault recovery
                        // Transition to Sensor Test Init State regardless
                        //  of CO PPM levels, since Sensor Short looks like
                        //  CO
                       co_state++;
                    }
                    else if( (CO_wPPM > MEAS_PPM_TEST_THRESHOLD) ||
                               FLAG_CO_ALARM_ACTIVE )
                    {
                        // Don't do the test if the sensor output is above the
                        // "no-test" threshold or in Alarm, but not in fault
                        co_timer_init(CO_TEST_INTERVAL_90_SEC);
                    }
                    else if(SMK_STANDBY != PHOTO_Get_State())
                    {
                        // Don't do the test if Smoke is not in Standby State
                        co_timer_init(CO_TEST_INTERVAL_90_SEC);
                    }
                    else
                    {
                        // Baseline Co test starting value
                        CO_vout_baseline = CO_raw_counts;

                        // Transition to Sensor Test Init State
                        co_state++;
                    }
                #endif
            }
        break;

        case CO_MEAS_TEST_INIT:

            #ifdef CONFIGURE_UNDEFINED
                // Test Only
                RED_LED_ON
            #endif

            #ifdef CONFIGURE_CO_FUNC_TEST
                // No CO Health test if Functional Co test was selected
                if(FLAG_FUNC_CO_TEST)
                {
                    co_state = CO_MEAS_SAMPLE;
                    co_timer_init(CO_TEST_INTERVAL_90_SEC);

                    return MAIN_Make_Return_Value(CO_SAMPLE_INT_1000_MS);
                }
            #endif

            // Calculate the Test Baseline for charge control
            co_test_baseline = CO_vout_baseline + VMEAS_AD_CHARGE_THRESHOLD;

            CO_Meas_Charge(co_test_baseline);

            FLAG_SENSOR_TEST = 1;
            co_test_counter = TEST_LENGTH_COUNTER;

            if(SERIAL_ENABLE_HEALTH)
            {
                SER_Send_Char('B');
                SER_Send_Int('L', CO_vout_baseline, 10);
                SER_Send_Prompt();
            }

            co_state++;
            
            return MAIN_Make_Return_Value(CO_SAMPLE_INT_1000_MS);


        case CO_MEAS_TEST_MONITOR:

            #ifdef CONFIGURE_UNDEFINED
                // Test Only
                if(co_test_counter & 0x01)
                {
                    RED_LED_ON
                }
                else
                {
                    RED_LED_OFF
                }
            #endif

            // Decrement the test counter
            if( 0 == --co_test_counter)
            {
                #ifdef CONFIGURE_UNDEFINED
                    // Test only
                    RED_LED_OFF
                #endif

                //
                // Test Complete - reset the raw counts to Base Line value
                //  at the beginning of the test.
                // This is for proper Sensor Short measurements             
                //            
                CO_raw_counts = CO_vout_baseline;
                            
                // Test timer has expired.  Return to the measurement state.
                co_state = CO_MEAS_SAMPLE;
                
                // Re-init the interval timer for measurement state.
                co_timer_init(CO_TEST_INTERVAL_90_SEC);

                FLAG_SENSOR_TEST = 0;
                
                return MAIN_Make_Return_Value(CO_SAMPLE_INT_1000_MS);

            }
            else
            {
                if( !FLAG_CO_FAULT_PENDING &&
                     (FLAG_FUNC_CO_TEST || 
                      SMK_STANDBY != PHOTO_Get_State() ) )
                {
                    // No Action, just return but remain in this state
                    //  until the Test Count Time has completed to
                    //  allow any CO test charge to return to baseline.
                }
                else
                {
                    // Make a CO Measurement to use in this state
                    CO_raw_counts = co_meas_make_measurement();

                    if(VHIGH_TEST_SAMPLE == co_test_counter)
                    {
                        // Send out counts if enabled.
                        if(SERIAL_ENABLE_HEALTH)
                        {
                            SER_Send_Char('H');
                            SER_Send_Int('1', CO_raw_counts, 10);
                            SER_Send_Prompt();
                        }

                        /*
                         * At this test sample, the sensor output should be
                         * above the test threshold.
                         * Is (Vmeasured - Vbaseline) =
                         * Vdifferential > VHIGH_TEST_DIFFERENTIAL ?
                         * If measurement is less than baseline
                         * (occurs sometimes with sensor Open)
                         * delta will be negative (> VHIGH_TEST_MAX)
                         *
                         */
                    #ifndef CONFIGURE_NO_FAULT
                        if((CO_raw_counts - CO_vout_baseline <
                             VHIGH_TEST_DIFFERENTIAL) ||
                            (CO_raw_counts - CO_vout_baseline > VHIGH_TEST_MAX))
                        {
                            if (!FLAG_FAULT_FATAL)
                            {
                                // The amount of output from the sensor was not 
                                // enough. Log a conversion error and abort the
                                // test cycle.
                                FLT_Fault_Manager(FAULT_CO_HEALTH);

                                // Reset Trouble Indicator to begin now
                                MAIN_Reset_Task(TASKID_TROUBLE_MANAGER);

                                // Go back to measurement state.
                                co_state = CO_MEAS_SAMPLE;

                                // Re-init the interval timer for measurement 
                                // state. Since a fault was detected, repeat 
                                // test at shorter interval
                                co_timer_init(CO_TEST_INTERVAL_10_SEC);

                                FLAG_SENSOR_TEST = 0;
                            }
                            else
                            {
                                
                                // Go back to measurement state.
                                co_state = CO_MEAS_SAMPLE;

                                // Re-init the interval timer for measurement 
                                // state. Since a fault was detected, repeat 
                                // test at shorter interval
                                co_timer_init(CO_TEST_INTERVAL_20_SEC);

                                FLAG_SENSOR_TEST = 0;
                                
                            }
                        }
                    #endif
                        else
                        {
                            // Log Success and complete Test Cycle
                            FLT_Fault_Manager(SUCCESS_CONVERSION_SETUP);
                        }
                    }

                    // If the test sample is the low test sample, compare to 
                    // the low test threshold.
                    if(VLOW_TEST_SAMPLE == co_test_counter)
                    {
                        // Send out counts if enabled.
                        if(SERIAL_ENABLE_HEALTH)
                        {
                            SER_Send_Char('H');
                            SER_Send_Int('2', CO_raw_counts, 10);
                            SER_Send_Prompt();
                        }

                        /*
                         * This Sample Test was undefined as its not needed to
                         * detect Opens and CO introduced in middle of sensor
                         * test can cause this Sample Test to fail, as Sensor
                         * Output may remain above Baseline.
                         */
                        if(CO_raw_counts - CO_vout_baseline <
                                VLOW_TEST_DIFFERENTIAL)
                        {
                            // If Sensor Output has returned to near Baseline,
                            // Clear any Sensor Short faults
                            FLT_Fault_Manager(SUCCESS_SENSOR_SHORT);
                            FLT_Fault_Manager(SUCCESS_CONVERSION_SETUP);
                        }
                    }
                }
                
                return MAIN_Make_Return_Value(CO_SAMPLE_INT_1000_MS);
            }

        default:
            co_state = CO_MEAS_SAMPLE;
        break;

    }

    if(FLAG_MODE_ACTIVE)
    {
        return MAIN_Make_Return_Value(CO_SAMPLE_INT_1000_MS);
    }
    else
    {
        // Slower Sampling in LP Mode
        return MAIN_Make_Return_Value(CO_SAMPLE_INT_5000_MS);
    }
}


/*
+------------------------------------------------------------------------------
| Function:      co_meas_make_measurement
+------------------------------------------------------------------------------
| Purpose:       Make a CO Sensor Circuit Measurement and output results
|                thru Serial Port if enabled
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  adval   CO measurement in A/D counts, 10 bit result)
+------------------------------------------------------------------------------
*/

uint co_meas_make_measurement(void)
{
    uint adval;

    // If VDD is powered directly from battery, we need to scale measurement
    //  since VRef uses VDD in this controller.
    #ifdef CONFIGURE_NO_REGULATOR
        // Read 1.024 FVR for VDD calculation
	CO_FVR_Measured = A2D_Convert_10Bit(A2D_FVR_CHAN);

        // Take CO measurement
        adval = A2D_Convert_10Bit(A2D_CO_CHAN);

        // Calculate current VDD voltage
        unsigned long CO_Vdd = (unsigned long)(FVR_FACTOR / 
                               (unsigned long)CO_FVR_Measured);
        CO_vdd_calc = (uint)CO_Vdd;

        // From VDD calculation, calculate the Battery Test Voltage
        unsigned long CO_Measured = ((unsigned long)(CO_Vdd * adval)/1024);
        adval = (uint)CO_Measured;

        // There is some CO sensor circuit effects from VBAT changes
        //  about 1 mv per 20 mv of VDD change from 3.1 volts VBAT
        if(CO_vdd_calc < VBAT_MAX )
        {
            uint CO_Vdd_Delta = (VBAT_MAX - CO_vdd_calc);

            // Don't adjust if < 20 mv of VBat drop from VBat Max
            if(CO_Vdd_Delta > AD_20_MV)
            {
                adval = adval + (CO_Vdd_Delta / (AD_20_MV+1) ;
            }
        }

    #else
        adval = A2D_Convert_10Bit(A2D_CO_CHAN);
    #endif

  #ifdef CONFIGURE_MANUAL_PPM_COMMAND	

        // Override measurement with Manually Set Co Sensor value if enabled
    if(CO_manual_PPM_value != 0)
    {
        adval = CO_manual_PPM_value;
    }
  #endif

    if(SERIAL_ENABLE_CO)
    {

        SER_Send_Int('V', adval, 10);

        // If CO Sensor Fault Pending, append 1 pipe char
        // If Fatal, append 2 pipe chars
        if(FLAG_CO_FAULT_PENDING)
        {
            SER_Send_Char('|');

            if(FLAG_FAULT_FATAL)
            {
                SER_Send_Char('|');
            }

        }
        if(!FLAG_SENSOR_CIRCUIT_STABLE)
        {
            SER_Send_Char('?');
        }

        SER_Send_Prompt();
    }

    return adval;
}


/*
+------------------------------------------------------------------------------
| Function:      CO_Meas_Get_State
+------------------------------------------------------------------------------
| Purpose:       Return the current CO Measure State machine state
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  co_state
+------------------------------------------------------------------------------
*/

uchar CO_Meas_Get_State(void)
{
   return  co_state;
}


/*
+------------------------------------------------------------------------------
| Function:      CO_Meas_Charge
+------------------------------------------------------------------------------
| Purpose:       Place Negative Charge on CO Sensor for PTT operation
|                and also used in CO Sensor test supervision
|
+------------------------------------------------------------------------------
| Parameters:    threshold   (charge sensor to this A/D count threshold)
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void CO_Meas_Charge(uint threshold)
{
    uint i;
    uint adval;

    for(i = 0; i < NUM_CHARGE_PULSES; i++)
    {
        // If VDD is powered directly from battery, we need to scale 
        //  measurement since VRef uses VDD in this controller.
        #ifdef CONFIGURE_NO_REGULATOR
            // Read 1.024 FVR for VDD calculation
            CO_FVR_Measured = A2D_Convert_10Bit(A2D_FVR_CHAN);

            // Take CO measurement
            adval = A2D_Convert_10Bit(A2D_CO_CHAN);

            // Calculate current VDD voltage
            unsigned long CO_Vdd = (FVR_FACTOR / CO_FVR_Measured);
            // From VDD calculation, calculate the Battery Test Voltage
            unsigned long CO_Measured = ((unsigned long)(CO_Vdd * adval)/1024);
            adval = (uint)CO_Measured;

        #else
            adval = A2D_Convert_10Bit(A2D_CO_CHAN);
        #endif

        if(adval <= (threshold + AD_10_COUNTS))
        {
            co_meas_charge_pulse();
        }
        else
        {
            // If above Baseline we're done
            i = NUM_CHARGE_PULSES;
        }
    }

}

/*
+------------------------------------------------------------------------------
| Function:      co_meas_charge_pulse
+------------------------------------------------------------------------------
| Purpose:       Applies a pulse on the CO Sensor Test pin for
|                placing a negative charge on the Co Sensor (simulates CO)
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void co_meas_charge_pulse(void)
{
    LAT_CO_TEST_PIN = 1;
    PIC_delay_us(CO_CHARGE_PULSE_HI);
    LAT_CO_TEST_PIN = 0;
    PIC_delay_us(CO_CHARGE_PULSE_LO);
}


/*
+------------------------------------------------------------------------------
| Function:      CO_Meas_Reset
+------------------------------------------------------------------------------
| Purpose:       Reset CO Meas State in preparation for PTT 
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void CO_Meas_Reset(void)
{
    FLAG_SENSOR_TEST = 0;
    
    // Init the CO Health test Timer
    co_timer_init(CO_TEST_INTERVAL_90_SEC);
    
    co_state = CO_MEAS_SAMPLE;

}

#else   // #ifdef CONFIGURE_CO

// No Co Sensor (function place holders)

uint CO_Measure_Task(void)
{
    return  MAIN_Make_Return_Value(65000);
}

void CO_Meas_Reset(void)
{

}
void CO_Meas_Charge(uint)
{

}
uchar CO_Meas_Get_State(void)
{
    return 0;
}

#endif  // #ifdef CONFIGURE_CO