/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        diaghist.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for diaghist.c module
|               Routines for the logging of events.
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


#ifndef DIAGHIST_H
#define DIAGHIST_H


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

    /* 48 Events per Queue
    *  2 bytes per event */
    #define MAX_EVENTS      48


    //Definitions for Battery Voltage Recording
    #define BAT_DAY_ONE     1
    #define BAT_YEAR_ONE    2
    #define BAT_CURRENT     3

    #define DIAG_EVENT_CODE (uchar)0
    #define DIAG_EVENT_DAY  (uchar)1

    //
    // History Events (Note: Bit 7 is reset to indicate a Minor Event)
    //
                                            
    #define HIST_WDT_RESET          0x00	// WDT Reset (non-Production)
    #define HIST_MINOR_1_EVENT		0x01	// Unused
    #define HIST_MINOR_2_EVENT		0x02	// Unused
    #define HIST_MINOR_3_EVENT		0x03	// Unused
    #define HIST_LOW_BATT_MODE		0x04	// Low Battery Mode Entered
    #define HIST_ABN_LOW_BATT_EVENT 0x05	// Abnormally Low Battery
    #define HIST_MINOR_6_EVENT		0x06	// Unused
    #define HIST_NETWORK_SEARCH		0x07	// Network Search
    #define HIST_NETWORK_ERROR		0x08	// Network Error
    #define HIST_TAMPER_EVENT		0x09	// Unused
    #define HIST_AC_POWER_OFF		0x0A	// AC Power Off detected
    #define HIST_AC_POWER_ON		0x0B	// AC Power On detected
    #define HIST_POWER_RESET		0x0C	// Power On Reset Event
    #define HIST_PTT_EVENT          0x0D	// Push to Test Pressed
    #define HIST_PEAK_EVENT         0x0E	// Unused
    #define HIST_MINOR_F_EVENT		0x0F	// Unused

    // History Events (Note: Bit 7 is set to indicate a Major Event)
    #define HIST_ALG_JUMP           0x80	// Smoke Algorithm Jump entered
    #define HIST_ALG_SLUMP1         0x81	// Smoke Algorithm Slump1 entered
    #define HIST_SENS_HEALTH_FAULT	0x82	// 'E02' Sensor Health
    #define HIST_ALG_SLUMP2     	0x83	// Smoke Algorithm Slump2 entered
    #define HIST_SENS_SHORT_FAULT	0x84	// 'E04' Sensor Short
    #define HIST_DRIFT_COMP_FAULT	0x85	// Drift Comp Limit Fault
    #define HIST_INT_SUPER_FAULT    0x86	// INT Supervision FLT
    #define HIST_PTT_FAULT          0x87	// Push to Test Fault
    #define HIST_MEMORY_FAULT		0x88	// Eeprom Memory Fault 'E08'
    #define HIST_LIFE_EXP           0x89	// Life Expiration Fault 'E09'
    #define HIST_PHOTOCHAMBER_FAULT	0x8A	// Photo Smoke Chamber Fault
    #define HIST_SMK_ALARM_HUSH_EVENT   0x8B	// Photo Smoke Hush Event 
    #define HIST_SMK_ALARM_ON_EVENT     0x8C	// Photo Smoke Alarm

#ifdef CONFIGURE_ACTIVE_TIMEOUT_TIMER 
    #define HIST_ACTIVE_TIMEOUT          0x8D	// Active Mode Timeout
#else
    #define HIST_MAJOR_D_EVENT           0x8D   // Unused         
#endif

    #define HIST_ALARM_OFF_EVENT        0x8E	// Alarm OFF Event Detected
    #define HIST_ALARM_ON_EVENT         0x8F	// CO Alarm ON  Event Detected

    // Offset into EEProm
    #define DIAG_HIST_START         0x20

    #define DIAG_MAJOR_QUE_START    0x40
    #define DIAG_MAJOR_QUE_END      0x9F
    #define DIAG_MINOR_QUE_START    0xA0
    #define DIAG_MINOR_QUE_END      0xFF

    // Event Queue Pointers
    #define MAJOR_QPTR_INIT         DIAG_MAJOR_QUE_START
    #define MINOR_QPTR_INIT         DIAG_MINOR_QUE_START
    #define QPTR_CKSUM_INIT         (MAJOR_QPTR_INIT + MINOR_QPTR_INIT)


    // Define offsets
    #define DIAG_CNT_PTT          0x20          // 32
    #define DIAG_CNT_NET_SEARCH   0x21          // 33         
    #define DIAG_CNT_LOW_BATT     0x22          // 34
    #define DIAG_BATTERY_DAY_1    0x23          // 35
    #define DIAG_BATTERY_YEAR_1   0x24          // 36
    #define DIAG_BATTERY_CURRENT  0x25          // 37
    #define DIAG_SMK_CAV_AVERAGE  0x26          // 38


    #ifdef CONFIGURE_SMK_SLOPE_SIMULATION
        #define DIAG_SLOPE_LOW        0x27
        #define DIAG_SLOPE_HIGH       0x28
    #else
        #define DIAG_UNUSED_0X27      0x27      //16 bits, Ox27 and 0x28
    #endif

    #define DIAG_CNT_NET_ERR      0x29          // 41

#ifdef CONFIGURE_ACTIVE_TIMEOUT_TIMER 
    #define DIAG_CNT_ACTIVE_T_O   0x2A          // 42
#else
    #define DIAG_UNUSED_0X2A      0x2A          // 42         
#endif

    #define DIAG_CNT_EE_ERR       0x2B          // 43

    #define DIAG_CO_LAST_ALARM    0x2C          // 44,45
    #define DIAG_VERSION_MAJOR    0x2E          // 46
    #define DIAG_VERSION_MINOR    0x2F          // 47

    #define DIAG_CNT_ALARM_ON     0x30          // 48
    #define DIAG_CNT_SENS_HEALTH  0x31          // 49
    #define DIAG_CNT_SENS_SHORT   0x32          // 50
    #define DIAG_CNT_SMK_CHAMBER  0x33          // 51
    #define DIAG_PEAK_CO_VALUE    0x34          // 52,53
    #define DIAG_PEAK_CO_DAY      0x36          // 54,55
    #define DIAG_CNT_POWER_RESET  0x38          // 56
    #define DIAG_CNT_AC_POWER     0x39          // 57
    #define DIAG_CNT_SMK_ALARM    0x3A          // 58
    #define DIAG_UNIT_ID_LOW      0x3B          // 59
    #define DIAG_UNIT_ID_HIGH     0x3C          // 60
    #define DIAG_QUE_CKSUM        0x3D          // 61
    #define DIAG_MAJOR_QUE_PTR    0x3E          // 62
    #define DIAG_MINOR_QUE_PTR    0x3F          // 63

    #define DIAG_EVENT_NO_COUNTER 0xFF


/*
|-----------------------------------------------------------------------------
| Typedef, Enums and Structures
|-----------------------------------------------------------------------------
*/
    struct DIAG_event_info
    {
        uchar	eventcode;         // Event Code in MS nibble
        uchar	eventday;
    };

    /*
       Define Diag History data structure:
       1. Must be 256 bytes (for backward compatibility)
       2. Pad as needed to keep 16 bit word alignment for UINT values
       3. Major and Minor queues should be 96 bytes (48 events each)
    */
    struct  DIAG_HistData 
    {
        //***** 0x00 - 0x0F locations *****
        // Primary Copy of Data Structure

        //*****	0x10 - 0x1F locations *****
        // backup Copy of Data Structure


        //*****	0x20 - 0x2F locations *****
        uchar   PTT_Count;        		// 20  (32)
        uchar   Net_Search_Count;       // 21  (33)
        uchar   Low_Battery_Count;		// 22  (34)
        uchar   Battery_Day_1;    		// 23  (35)
        uchar   Battery_Year_1;			// 24  (36)
        uchar   Battery_Now;			// 25  (37)
        uchar   Smk_CAV_Avg;			// 26  (38)

        uint    SMK_Sim_Slope;			// 27, 28  (not used normally)

        uchar   Net_Err_Count;			// 29  (41) 
        uchar   Spare_2A;               // 2A  (42)
        uchar 	EE_Err_Cnt;             // 2B  (43)

        uint	CO_Last_Alarm;  		// 2C, 2D  (44,45)
        uchar   REV_MAJOR;              // 2E  (46)
        uchar   REV_MINOR;              // 2F  (47)

        //***** 0x30 - 0x3F locations *****
        uchar	CO_Alarm_Count;			// 30  (48)
        uchar	CO_Fault_Count;			// 31  (49)
        uchar	CO_Short_Count;         // 32  (50)
        uchar	SMK_Chamber_Fault_Count;    // 33 (51)
        uint	Peak_CO_Value;			// 34, 35  (52,53)
        uint	Peak_CO_Day;			// 36, 37  (54,55)
        uchar	PWR_Reset_Count;		// 38  (56)
        uchar	AC_PWR_Count;			// 39  (57)
        uchar	SMK_Alarm_Count;		// 3A
        uchar 	Unit_ID_Low;       		// 3B
        uchar 	Unit_ID_High;			// 3C
        uchar   QPTR_Cksum;             // 3D
        uchar   Major_Queue_Pointer;    // 3E
        uchar   Minor_Queue_Pointer;	// 3F

        //	0x40 - 0xFF locations
        //      These Event Queues must be initialized (filled with 0xFF)
        
        // 40 - 9F major queue data
        struct DIAG_event_info Major_Event_Queue[MAX_EVENTS];
        // A0 - FF minor queue data
        struct DIAG_event_info Minor_Event_Queue[MAX_EVENTS];	

    };


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/



/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/

    #ifdef CONFIGURE_DIAG_HIST
        //Prototypes
        void DIAG_Hist_Que_Push(uchar event_code);
        void DIAG_Hist_Bat_Record(uchar battime, uchar batvolt);

        void DIAG_Hist_EE_Error(void);
        void DIAG_Hist_Record_Avg_CAV(uchar avg);

        #ifdef	CONFIGURE_CO
            void DIAG_Hist_Test_Peak(uint ppm);
        #endif

    #else
        void DIAG_Hist_EE_Error(void);

    #endif

#endif  // DIAGHIST_H


