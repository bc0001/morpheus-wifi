/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  Workfile:   voice.h
|  Author:     Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  header File for voice.c module
|               This file contains voice file definitions for the 119-message
|               Voice Chip
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
*/


#ifndef	VOICE_H
#define VOICE_H


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

    //*************************** Messages ************************************
    //  Voice Chip Messages
    //*************************** Messages ************************************
    
    #define VCE_ENGLISH_SELECTED            0  
    #define VCE_FR_FRENCH_SELECTED          1     
    #define VCE_READY_TO_CONNECT            2
    #define VCE_FR_READY_TO_CONNECT         3
    #define VCE_SETUP_COMPLETE              4
    #define VCE_FR_SETUP_COMPLETE           5
    #define VCE_DEVICES_CONNECTED           6
    #define VCE_FR_DEVICES_CONNECTED        7
    #define VCE_SUCCESS                     8
    #define VCE_FR_SUCCESS                  9       
	#define	VCE_HUSH_ACTIVATED              10      //0x0A
	#define	VCE_FR_HUSH_ACTIVATED           11      //0x0B
    #define VCE_NO_DEVICES_FOUND            12      //0x0C
    #define VCE_FR_NO_DEVICES_FOUND         13      //0x0D
	#define	VCE_HUSH_CANCELLED              14      //0x0E
	#define	VCE_FR_HUSH_CANCELLED           15      //0x0F
    #define VCE_TEMP_SILENCED               16      //0x10
    #define VCE_FR_TEMP_SILENCED            17      //0x11
	#define	VCE_FIRE_ENGLISH_1              18      //0x12
	#define	VCE_FIRE_FRENCH_1               19      //0x13
    #define VCE_TEST_CANCELLED              20      //0x14
    #define VCE_FR_TEST_CANCELLED           21      //0x15
    #define VCE_ONE                         22      //0x16
    #define VCE_FR_ONE                      23      //0x17
    #define VCE_TWO                         24      //0x18
    #define VCE_FR_TWO                      25      //0x19
    #define VCE_THREE                       26      //0x1A
    #define VCE_FR_THREE                    27      //0x1B
    #define VCE_FOUR                        28      //0x1C
    #define VCE_FR_FOUR                     29      //0x1D
    #define VCE_FIVE                        30      //0x1E
    #define VCE_FR_FIVE                     31      //0x1F
    #define VCE_SIX                         32      //0x20
    #define VCE_FR_SIX                      33      //0x21
    #define VCE_SEVEN                       34      //0x22
    #define VCE_FR_SEVEN                    35      //0x23
    #define VCE_EIGHT                       36      //0x24
    #define VCE_FR_EIGHT                    37      //0x25
    #define VCE_NINE                        38      //0x26
    #define VCE_FR_NINE                     39      //0x27
    #define VCE_TEN                         40      //0x28
    #define VCE_FR_TEN                      41      //0x29
    #define VCE_ELEVEN                      42      //0x2A
    #define VCE_FR_ELEVEN                   43      //0x2B
    #define VCE_TWELVE                      44      //0x2C
    #define VCE_FR_TWELVE                   45      //0x2D
    #define VCE_THIRTEEN                    46      //0x2E
    #define VCE_FR_THIRTEEN                 47      //0x2F
    #define VCE_FOURTEEN                    48      //0x30
    #define VCE_FR_FOURTEEN                 49      //0x31
    #define VCE_FIFTEEN                     50      //0x32
    #define VCE_FR_FIFTEEN                  51      //0x33
    #define VCE_SIXTEEN                     52      //0x34
    #define VCE_FR_SIXTEEN                  53      //0x35
    #define VCE_SEVENTEEN                   54      //0x36
    #define VCE_FR_SEVENTEEN                55      //0x37
    #define VCE_EIGHTEEN                    56      //0x38
    #define VCE_FR_EIGHTEEN                 57      //0x39
    #define VCE_NINETEEN                    58      //0x3A
    #define VCE_FR_NINETEEN                 59      //0x3B
    #define VCE_TWENTY                      60      //0x3C
    #define VCE_FR_TWENTY                   61      //0x3D
    #define VCE_TWENTY_ONE                  62      //0x3E
    #define VCE_FR_TWENTY_ONE               63      //0x3F
    #define VCE_TWENTY_TWO                  64      //0x40
    #define VCE_FR_TWENTY_TWO               65      //0x41
    #define VCE_TWENTY_THREE                66      //0x42
    #define VCE_FR_TWENTY_THREE             67      //0x43
    #define VCE_TWENTY_FOUR                 68      //0x44
    #define VCE_FR_TWENTY_FOUR              69      //0x45
    #define VCE_NETWORK_SEARCH              70      //0x46
    #define VCE_FR_NETWORK_SEARCH           71      //0x47
    #define VCE_TESTING_AND_LOUD            72      //0x48
    #define VCE_FR_TESTING_AND_LOUD         73      //0x49
    #define VCE_TEST_COMPLETE               74      //0x4A
    #define VCE_FR_TEST_COMPLETE            75      //0x4B
    #define VCE_RESET_WIRELESS_SETTINGS     76      //0x4C
    #define VCE_FR_RESET_WIRELESS_SETTINGS  77      //0x4D 
    #define VCE_NOW_CONNECTED               78      //0x4E
    #define VCE_FR_NOW_CONNECTED            79      //0x4F
    #define VCE_TOO_MUCH_SMOKE              80      //0x50
    #define VCE_FR_TOO_MUCH_SMOKE           81      //0x51
    #define VCE_NOT_CONNECTED               82      //0x52
    #define VCE_FR_NOT_CONNECTED            83      //0x53
    #define VCE_PRESS_TO_CANCEL_TEST        84      //0x54
    #define VCE_FR_PRESS_TO_CANCEL_TEST     85      //0x55
	#define	VCE_CO_ENGLISH                  86      //0x56
	#define	VCE_CO_FRENCH                   87      //0x57
    #define VCE_SMK_PREVIOUSLY              88      //0x58
    #define VCE_FR_SMK_PREVIOUSLY           89      //0x59
    #define	VCE_CO_PREVIOUSLY               90      //0x5A
    #define	VCE_FR_CO_PREVIOUSLY            91      //0x5B
    #define VCE_ERROR_SEE_GUIDE             92      //0x5C
    #define VCE_FR_ERROR_SEE_GUIDE          93      //0x5D
    #define VCE_REPLACE_ALARM               94      //0x5E
    #define VCE_FR_REPLACE_ALARM            95      //0x5F
    #define VCE_BUTTON_TO_SILENCE           96      //0x60
    #define VCE_FR_BUTTON_TO_SILENCE        97      //0x61
    #define VCE_NETWORK_CONN_LOST           98      //0x62
    #define VCE_FR_NETWORK_CONN_LOST        99      //0x63
    #define VCE_EMERGENCY                   100     //0x64
    #define VCE_FR_EMERGENCY                101     //0x65
    #define VCE_WEATHER_ALERT               102     //0x66
    #define VCE_FR_WEATHER_ALERT            103     //0x67
    #define VCE_WATER_LEAK                  104     //0x68
    #define VCE_FR_WATER_LEAK               105     //0x69
    #define VCE_EXPLOSIVE_GAS               106     //0x6A
    #define VCE_FR_EXPLOSIVE_GAS            107     //0x6B
    #define VCE_TEMP_DROP                   108     //0x6C
    #define VCE_FR_TEMP_DROP                109     //0x6D
    #define VCE_INTRUDER_ALERT              110     //0x6E
    #define VCE_FR_INTRUDER_ALERT           111     //0x6F


    
    // Voice Chip Sounds
    //*************************** Sounds *************************************
    #define VCE_SND_SONAR_PING      112     //0x70, Sonar Ping
    #define VCE_SND_BTN_PRESS       113     //0x71, Button Press Beep
    #define VCE_SND_NETWORK_CLOSE   114     //0x72, High to Low Tone (short)
    #define VCE_SND_NETWORK_ERROR   115     //0x73, High to Low Tone (long)
    #define VCE_SND_POWER_ON        116     //0x74, Longer Low to High Tone 
    #define VCE_SND_PUSH_TO_TEST    117     //0x75, Low to High (happy ping)
    #define VCE_SND_RETURN_TO_OOB   118     //0x76, Four Syllable tone 

    
    // Alias with PUSH_TO_TEST happy sound :)
    #define VCE_SND_DEV_ENROLLED    117     //0x75, happy ping


#ifdef CONFIGURE_2018_VOICE_CHIP
    // Latest proposed Voice message changes (7/24/2018)
    #define VCE_UNDEFINED_119           119     //0x77  (not used))
    #define VCE_ACTIVATE_BATTERY        120     //Ox78
    #define VCE_FR_ACTIVATE_BATTERY     121     //Ox79

    // These 2 messages operate opposite language select logic of all others
    // (announced in French when English is selected)
    #define VCE_FR_FOR_FRENCH_PUSH_BTN  122     //0x7A

    // (announced in English when French is selected
    #define VCE_FOR_ENGLISH_PUSH_BTN    123     //0x7B  

#else
    // Recent Voice message added (6/14/2017))
    #define VCE_FR_FOR_FRENCH_PUSH_BTN  119     //0x77
#endif

    #define VCE_PAUSE_200_MS        0xF0
    #define VCE_PAUSE_500_MS        0xF1
    


/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/

	uint VCE_Process_Task(void);
	uchar VCE_Play(uchar voice);
    void VCE_Announce_Number(uchar number);
    
   
#endif  // VOICE_H


    
    