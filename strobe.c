/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        strobe.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Functions for strobe initialization and Strobe Pulse Timing
|               
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/



/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include "common.h"
#include "strobe.h"
#include "interconnect.h"
#include "alarmprio.h"


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

#ifdef CONFIGURE_STROBE_SYNC

// Task return times
#define STROBE_INTERVAL_10_MS       (uint)10        // 10 ms
#define STROBE_INTERVAL_60000_MS    (uint)60000     // 60 seconds

// Strobe Task States
#define	STROBE_STATE_INIT               0
#define	STROBE_STATE_IDLE               1
#define	STROBE_STATE_SMK_PULSE_ON		2
#define	STROBE_STATE_SMK_PULSE_OFF		3
#define STROBE_STATE_CO_PULSE_ON        4
#define STROBE_STATE_CO_PULSE_OFF       5
#define STROBE_STATE_CO_PULSE_DWELL     6


// Strobe CO Reset Sync Pulse Monitor States
#define STROBE_CO_RST_IDLE              0
#define STROBE_CO_RST_DELAY             1
#define STROBE_CO_RST_CHECK_1           2
#define STROBE_CO_RST_CHECK_2           3
#define STROBE_CO_RST_DETECT            4


#ifdef CONFIGURE_STROBE_1_MS_TASK
    //
    // 1 MS Timing (Timer 4 w/32KHz clock measures closer to 976uSec)
    //

//*****************************************************************************

#ifdef CONFIGURE_STROBE_PULSE_20MS
    // Test only for 20 ms development

    // Strobe Timer Times (1 ms Task Time)
    #define STROBE_TIMER_SMOKE_OFF_930_MS   (uint)960   // 960 ms
    #define STROBE_TIMER_SMOKE_OFF_830_MS   (uint)860   // 860 ms
    #define STROBE_TIMER_SMOKE_ON_50_MS     (uint)20    // 20 ms

    #define STROBE_TIMER_CO_OFF_930_MS      (uint)960   // 960 ms
    #define STROBE_TIMER_CO_ON_50_MS        (uint)20    // 20 ms

    // TODO: BC
    // Not sure if this needs adjusting for 20 ms timing
    #define STROBE_TIMER_SMOKE_OFF_860_MS       (uint)880   // ~860 ms


#else
    // 50 ms Strobe Pulse Times

    // Strobe Timer Times (10 ms Clock)
    // (20 ms faster than 950ms for 2% osc. tolerance)
    // UL requires Strobe Pulse Timing no greater than 1 Second period
    //#define STROBE_TIMER_SMOKE_OFF_930_MS   (uint)930   // 930 ms
    #define STROBE_TIMER_SMOKE_OFF_930_MS   (uint)953   // 930 ms
                                                
    //#define STROBE_TIMER_SMOKE_OFF_830_MS   (uint)830   // 830 ms
    #define STROBE_TIMER_SMOKE_OFF_860_MS   (uint)880   // ~860 ms

    //#define STROBE_TIMER_SMOKE_ON_50_MS     (uint)50    // 50 ms
    #define STROBE_TIMER_SMOKE_ON_50_MS     (uint)51    // 50 ms

    // (20 ms faster than 950ms for 2% osc. tolerance)
    // UL requires Strobe Pulse Timing no greater than 1 Second period
    //#define STROBE_TIMER_CO_OFF_930_MS      (uint)930   // 930 ms
    #define STROBE_TIMER_CO_OFF_930_MS      (uint)953   // 930 ms

    //#define STROBE_TIMER_CO_ON_50_MS        (uint)50    // 50 ms
    #define STROBE_TIMER_CO_ON_50_MS        (uint)51    // 50 ms

#endif

//#define STROBE_TIMER_SMK_SYNC       (uint)100   // 100 ms sync pulse
#define STROBE_TIMER_SMK_SYNC       (uint)102   // 100 ms sync pulse

//#define STROBE_TIMER_SMOKE_INIT     (uint)1000  // For 1st Strobe Pulse  1 Sec
#define STROBE_TIMER_SMOKE_INIT     (uint)1024  // For 1st Strobe Pulse  1 Sec

//#define STROBE_TIMER_SMOKE_PTT      (uint)4000  // Longer delay for PTTs 4 Sec
#define STROBE_TIMER_SMOKE_PTT      (uint)4098  // Longer delay for PTTs 4 Sec

#define STROBE_SYNC_OFF_12_SEC      (uint)12288 // Inhibit Strobe Sync Time
#define STROBE_SYNC_OFF_18_SEC      (uint)18432 // Inhibit Strobe Sync Time

//#define STROBE_TIMER_CO_INIT        (uint)1000  // For 1st Strobe Pulse 1 Sec
#define STROBE_TIMER_CO_INIT        (uint)1024  // For 1st Strobe Pulse 1 Sec
#define STROBE_TIMER_CO_1ST_INIT    (uint)2888  // ~2820ms For 1st Set 

//#define STROBE_TIMER_CO_DWELL       (uint)5000  // Dwell Time 5 Sec
#define STROBE_TIMER_CO_DWELL       (uint)5123  // Dwell Time 5 Sec

#define STROBE_PULSE_CO_COUNT       (uchar)4    // 4 Strobes every 5 Secs

 #ifdef CONFIGURE_STROBE_SYNC
    // Smoke SYNC defines
    #define STROBE_PULSE_SYNC_COUNT (uchar)20    // every 20 Strobes
    #define STROBE_PULSE_SYNC_INIT  (uchar)1     // after 1 strobe

    // CO SYNC defines
    #define CO_SYNC_COUNT           (uint)7     // Run off a 7 CO signal cycle

    //#define STROBE_TIMER_CO_1_SEC   (uint)1000  // 1st Strobe Pulse 1 second
    #define STROBE_TIMER_CO_1_SEC     (uint)1024  // 1st Strobe Pulse 1 second
    #define STROBE_TIMER_CO_1P8_SEC   (uint)1834  // Preloaded CO Strobe Time
                                                
    //#define CO_SYNC_TIME_3_SEC      (uint)3000  // Minimum non-active Time
                                                //  required for next Sync
    #define CO_SYNC_TIME_3_SEC      (uint)3092  // Minimum non-active Time
                                                //  required for next Sync
    // CO Reset pulse Tx times
    #define CO_SYNC_RESET_TIME      (uint)1024  // Reset Time = 1 Sec
    #define CO_SYNC_RESET_PULSE     (uint)82    // 80 ms reset pulse
    #define CO_SYNC_RESET_DISCHARGE (uint)204   // 200 ms discharge pulse

    // CO Reset Pulse Rx times
    #define CO_SYNC_WAIT_TIME       (uint)921   // Begin looking for Rst Pulse
    #define CO_SYNC_TIMEOUT         (uint)255   // Abort on Timeout time
    #define CO_SYNC_RESET_TIME_CHK2 (uint)61    // Pulse Width Check

#endif

//*****************************************************************************

#else
    //
    // 10 ms Timing
    //

#ifdef CONFIGURE_STROBE_PULSE_20MS
    // Test only for 20 ms development

    // Strobe Timer Times (10 ms Clock)
    #define STROBE_TIMER_SMOKE_OFF_930_MS   (uint)96  // 960 ms
    #define STROBE_TIMER_SMOKE_OFF_830_MS   (uint)86  // 860 ms
    #define STROBE_TIMER_SMOKE_ON_50_MS     (uint)2   // 20 ms

    #define STROBE_TIMER_CO_OFF_930_MS      (uint)96    // 960 ms
    #define STROBE_TIMER_CO_ON_50_MS        (uint)2     // 20 ms

#else
    // 50 ms Strobe Pulse Times

    // Strobe Timer Times (10 ms Clock)
    // (20 ms faster than 950ms for 2% osc. tolerance)
    // UL requires Strobe Pulse Timing no greater than 1 Second period
    #define STROBE_TIMER_SMOKE_OFF_930_MS   (uint)93  // 930 ms
                                                
    #define STROBE_TIMER_SMOKE_OFF_830_MS   (uint)83  // 830 ms
    #define STROBE_TIMER_SMOKE_ON_50_MS     (uint)5   // 50 ms

    // (20 ms faster than 950ms for 2% osc. tolerance)
    // UL requires Strobe Pulse Timing no greater than 1 Second period
    #define STROBE_TIMER_CO_OFF_930_MS      (uint)93    // 930 ms
    #define STROBE_TIMER_CO_ON_50_MS        (uint)5     // 50 ms

#endif

#define STROBE_TIMER_SMK_SYNC       (uint)10    // 100 ms sync pulse

#define STROBE_TIMER_SMOKE_INIT     (uint)100   // For 1st Strobe Pulse
#define STROBE_TIMER_SMOKE_PTT      (uint)400   // Longer delay for PTTs

#define STROBE_TIMER_CO_INIT        (uint)100       // For 1st Strobe Pulse
#define STROBE_TIMER_CO_1ST_INIT    (uint)282       // ~2820ms For 1st Set 

#define STROBE_TIMER_CO_DWELL       (uint)500       // Dwell Time

#define STROBE_PULSE_CO_COUNT       (uchar)4        // 4 Strobes every 5 secs

 #ifdef CONFIGURE_STROBE_SYNC
    // Smoke SYNC defines
    #define STROBE_PULSE_SYNC_COUNT (uchar)5        // every 5 Strobes

    // CO SYNC defines
    #define CO_SYNC_COUNT           (uint)7     // Run off a 7 CO signal cycle
    #define STROBE_TIMER_CO_1_SEC   (uint)100   // 1st Strobe Pulse 1 second
                                                //  after Sync
    #define CO_SYNC_TIME_3_SEC      (uint)300   // Minimum non-active Time
                                                //  required for next Sync
#endif

#endif  // CONFIGURE_STROBE_1_MS_TASK


/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/


static unsigned char strobe_state = STROBE_STATE_INIT;
static unsigned char strobe_co_rst_state = STROBE_CO_RST_IDLE;
static unsigned char strobe_pulse_cnt;
static unsigned int strobe_timer_co = 1;;
static unsigned int strobe_timer_smk;
static unsigned int strobe_sync_timer;

static unsigned int strobe_smk_sync_off = 1;


#ifdef CONFIGURE_STROBE_1_MS_TASK
    static unsigned int strobe_state_timer = 1;
#endif

 #ifdef CONFIGURE_STROBE_SYNC
    static unsigned char strobe_co_sync_count;
    static unsigned int strobe_co_sync_timer;
    static unsigned int strobe_co_reset_timer;
#endif


/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/


/*
+------------------------------------------------------------------------------
| Function:      STROBE_Task
+------------------------------------------------------------------------------
| Purpose:       Strobe Task Processing
|                If Smoke Alarm is Active Start the 1 Second Strobe Pulsing
|
|                
|                
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Strobe Process State Next Execution Time (in Task TICs)
+------------------------------------------------------------------------------
*/

unsigned int STROBE_Task(void)
{
 
    #ifdef CONFIGURE_DEBUG_STROBE_STATES
        // Test Only
        for(uchar i = (strobe_state + 1); i > 0; i-- )
        {
            // Test Only
            MAIN_TP2_On();
            PIC_delay_us(DBUG_DLY_25_USec);
            MAIN_TP2_Off();
        }
    #endif

    #ifdef CONFIGURE_DEBUG_STROBE_STATES
        // Test Only
        PIC_delay_us(DBUG_DLY_50_USec);
        for(uchar i = strobe_pulse_cnt; i > 0; i-- )
        {
            // Test Only
            MAIN_TP2_On();
            PIC_delay_us(DBUG_DLY_25_USec);
            MAIN_TP2_Off();
        }
    #endif
    
    #ifdef CONFIGURE_STROBE_SYNC

        // SMOKE SYNC is tested every 1ms, Master and Slave Smokes should all
        //  Monitor for Strobe Sync Pulse to account for delays on INT line.
        if(FLAG_SMOKE_ALARM_ACTIVE)
        {
            // Update Smoke SYNC Inhibit Timer
            if(0 == strobe_smk_sync_off)
            {
                // Smoke SYNC Inhibit Timed Out, Hold at 0
            }
            else
            {
                strobe_smk_sync_off--;
            }

            if(INT_Is_Int_In_Active() )
            {
                // INT_IN is High (active)
                // Monitor INT In for rising Edges (Strobe SYNC)
                if(FLAG_SMK_SYNC_PULSE_ENA)
                {
                    
                    //INT_IN ws previously Low (Rising Edge of Sync Detected)
                    FLAG_SMK_SYNC_PULSE_ENA = FALSE;

                            #ifdef CONFIGURE_UNDEFINED
                                SER_Send_String("SMK SYNC! ");
                                SER_Send_Prompt();
                            #endif
                    // This means that the Initiating Strobe Alarm just reset
                    //  it's Strobe Timer
                    // We should re-sync States and Timer Too

                    // Strobe OFF and Reset Strobe Timer
                    STROBE_PULSE_OFF
                                        
                    // 930 ms - Pulse Rise Detect time (~70ms )
                    strobe_timer_smk = STROBE_TIMER_SMOKE_OFF_860_MS;
                    strobe_state = STROBE_STATE_SMK_PULSE_ON;
                    
                    strobe_smk_sync_off = STROBE_SYNC_OFF_18_SEC;
                    
                    #ifdef CONFIGURE_STROBE_1_MS_TASK
                        strobe_state_timer = 1;
                        return (1);
                    #else
                        return MAIN_Make_Return_Value(STROBE_INTERVAL_10_MS);
                    #endif
                }
            }
            else
            {
                //
                // Interconnect Low detected during Smoke alarm
                //
                if(strobe_smk_sync_off)
                {
                    // Filter any Interconnect Pulses until SYNC Off expires
                }
                else
                {
                    // In Smoke Alarm and INT_IN is Low, set Flag
                    FLAG_SMK_SYNC_PULSE_ENA = TRUE;
                }
            }
        }
        else if(FLAG_CO_ALARM_ACTIVE)
        {
            // Find the 1st CO Interconnect Pulse (use it as a Sync)
            if(INT_Is_Int_In_Active() )
            {
                // Always Re-Init Sync Timer when a CO pulse is detected
                strobe_co_sync_timer = CO_SYNC_TIME_3_SEC;

                if(FLAG_CO_SYNC_PULSE_ENA)
                {
                    // 1st CO Pulse after dwell time  
                    FLAG_CO_SYNC_PULSE_ENA = FALSE;

                    // Flag was TRUE when INT_IN was previously low
                    //  for greater than 3 seconds. This means this is the 
                    //  1st of 4 CO INTERCONNECT pulses. 
                    // We re-sync the Strobe every 7 CO Signals

                    //
                    // Every new CO Signal, if in Slave Alarm look for 
                    // a CO Sync Reset Pulse to resync the CO signal
                    //  alarm count to 1
                    //
                    if(FLAG_SLAVE_CO_ACTIVE)
                    {
                        // Initiate CO Reset Timer to start sampling
                        strobe_co_rst_state = STROBE_CO_RST_DELAY;
                        strobe_co_reset_timer = CO_SYNC_WAIT_TIME;
                    }
                    else
                    {
                        // Initiate CO Reset Timer to start sampling
                        strobe_co_rst_state = STROBE_CO_RST_IDLE;
                    }

                    //    
                    // Update CO Sync Count (counts 7 cycles of CO signal)
                    //
                    --strobe_co_sync_count;

                    #ifdef CONFIGURE_UNDEFINED
                      SER_Send_Prompt();
                      SER_Send_Int('*', (uint)(strobe_co_sync_count), 10);
                      SER_Send_Prompt();
                    #endif

                    //
                    // Do we need to issue a CO Reset Pulse? 
                    // This is needed in case slave alarms lose track
                    //  of which CO Alarm Signal pattern is CO Signal #1 
                    // This allows Sync Recovery for CO Alarm Strobing
                    //
                    if(1 == strobe_co_sync_count)
                    {
                        // Must be Master or Remote CO alarm driving 
                        //  the Interconnect
                        if( (FLAG_CO_ALARM_CONDITION && 
                                !FLAG_SLAVE_CO_ACTIVE) ||

                        ( (ALM_ALARM_REMOTE_CO == ALM_Get_State() ) &&
                                FLAG_GATEWAY_REMOTE_TO_SLAVE && 
                                !FLAG_SLAVE_CO_ACTIVE)  ||

                        ( (ALM_ALARM_REMOTE_CO_HW == ALM_Get_State() ) &&
                                FLAG_GATEWAY_REMOTE_TO_SLAVE && 
                                !FLAG_SLAVE_CO_ACTIVE) )                      
                        {
                            // Set CO Reset Timer to Issue pulse in 1 Sec
                            strobe_co_reset_timer = CO_SYNC_RESET_TIME;
                            FLAG_CO_SYNC_RESET_ACTIVE = 0;
                            FLAG_CO_SYNC_RESET_ENABLE = 1;

                        #ifdef CONFIGURE_UNDEFINED
                            SER_Send_String("CO SYNC RST! ");
                            SER_Send_Prompt();
                        #endif

                        }
                    }

                    // Re-Sync CO Strobe Timing? 
                    // Is this the Sync Signal leading Pulse?
                    if(0 == strobe_co_sync_count)
                    {
                        #ifdef CONFIGURE_UNDEFINED  
                            #ifdef CONFIGURE_DEBUG_STROBE_TEST
                                MAIN_TP_On();
                            #endif
                        #endif

                        #ifdef CONFIGURE_UNDEFINED
                            SER_Send_String("CO SYNC! ");
                            SER_Send_Prompt();
                        #endif

                        STROBE_PULSE_OFF
                        strobe_co_sync_count = CO_SYNC_COUNT;

                        // Set Strobe to start 4 pulses in 1 second
                        strobe_timer_co = STROBE_TIMER_CO_1_SEC;
                        strobe_state = STROBE_STATE_CO_PULSE_ON;

                        // We Need 4 Pulses every 5 Seconds
                        strobe_pulse_cnt = STROBE_PULSE_CO_COUNT;

                        #ifdef CONFIGURE_STROBE_1_MS_TASK
                            strobe_state_timer = 1;
                            return (1);
                        #else
                            return MAIN_Make_Return_Value
                                    (STROBE_INTERVAL_10_MS);
                        #endif
                    }
                }
                else
                {
                    // This must be T4 int. signal pulse 2, 3 or 4
                    // Do not re-sync at this time
                }
            }
            else
            {
                // Measure INT_IN dwell Time
                // INT-IN is Low (inactive) Has it been Low for
                //  a minimum of 3 seconds?
                if(!FLAG_CO_SYNC_PULSE_ENA)
                {
                    if(0 == --strobe_co_sync_timer)
                    {
                        // Yes, Set Flag... next pulse should be 1st of
                        //  set of 4 CO Pulses
                        FLAG_CO_SYNC_PULSE_ENA = TRUE;

                        #ifdef CONFIGURE_UNDEFINED
                            SER_Send_String("CO SYNC ENABLE ");
                            SER_Send_Prompt();
                        #endif
                    }
                }

            }

            //
            // See if a CO Sync Reset pulse is needed
            // Must be initiating CO alarm driving the Interconnect
            //
            if( (FLAG_CO_SYNC_RESET_ENABLE && FLAG_CO_ALARM_CONDITION && 
                 !FLAG_SLAVE_CO_ACTIVE)  ||

                ( (ALM_ALARM_REMOTE_CO == ALM_Get_State() ) &&
                   FLAG_CO_SYNC_RESET_ENABLE && !FLAG_SLAVE_CO_ACTIVE &&
                   FLAG_GATEWAY_REMOTE_TO_SLAVE )  ||

                ( (ALM_ALARM_REMOTE_CO_HW == ALM_Get_State() ) &&
                   FLAG_CO_SYNC_RESET_ENABLE && !FLAG_SLAVE_CO_ACTIVE &&
                   FLAG_GATEWAY_REMOTE_TO_SLAVE ) )                      
            {
                if(0 == --strobe_co_reset_timer) 
                {
                    if(FLAG_CO_SYNC_RESET_ACTIVE)
                    {
                        if(FLAG_CO_SYNC_RESET_DSCHRG)
                        {
                            // End CO Reset Signal and 
                            //  begin discharge time
                            INT_OUT_LOW
                            FLAG_CO_SYNC_RESET_DSCHRG = 0;

                            strobe_co_reset_timer = CO_SYNC_RESET_DISCHARGE;
                        }
                        else
                        {
                            FLAG_CO_SYNC_RESET_ACTIVE = 0;
                            FLAG_CO_SYNC_RESET_ENABLE = 0;
                            INT_set_input();
                        }

                    }
                    else
                    {
                        #ifdef CONFIGURE_UNDEFINED
                            SER_Send_String("CO SYNC TIME ");
                            SER_Send_Prompt();
                        #endif

                        // Issue CO Reset Signal
                        FLAG_CO_SYNC_RESET_ACTIVE = 1;
                        FLAG_CO_SYNC_RESET_DSCHRG = 1;
                        strobe_co_reset_timer = CO_SYNC_RESET_PULSE;

                        // Begin Pulse
                        INT_OUT_HIGH
                        INT_set_output();
                    }
                }
            }


            // CO SYNC Reset sampling and processing states
            // initiating alarms remain in Idle state
            switch (strobe_co_rst_state)
            {
                case    STROBE_CO_RST_IDLE:
                {
                    // No Reset Pulse detected, no action required
                }
                break;

                case    STROBE_CO_RST_DELAY:
                {
                    // Wait time to beginning of Reset pulse sampling
                    if(0 == --strobe_co_reset_timer) 
                    {
                        // Start sampling with Timeout Time
                        strobe_co_rst_state = STROBE_CO_RST_CHECK_1;
                        strobe_co_reset_timer = CO_SYNC_TIMEOUT;

                        #ifdef CONFIGURE_UNDEFINED
                            // BC: Test Only
                            MAIN_TP2_On();
                        #endif

                    }
                }
                break;

                case    STROBE_CO_RST_CHECK_1:
                {
                    // Wait time to Reset Timeout
                    if(0 == --strobe_co_reset_timer) 
                    {
                        // Timeout Out!
                        strobe_co_rst_state = STROBE_CO_RST_IDLE;

                        #ifdef CONFIGURE_UNDEFINED
                            // BC: Test Only
                            MAIN_TP2_Off();
                        #endif

                    }
                    else
                    {
                        // Sample for Reset Pulse until timeout
                        if(INT_Is_Int_In_Active() )
                        {
                            // Detected a Signal, init pulse width test
                            strobe_co_rst_state = STROBE_CO_RST_CHECK_2;
                            strobe_co_reset_timer = CO_SYNC_RESET_TIME_CHK2;
                        }
                        else
                        {
                            // Not detected, remain in this state
                        }
                    }
                }
                break;

                case    STROBE_CO_RST_CHECK_2:
                {
                    // Wait time to resample Reset Pulse
                    if(0 == --strobe_co_reset_timer) 
                    {
                        #ifdef CONFIGURE_UNDEFINED
                            // BC: Test Only
                            MAIN_TP2_Off();
                        #endif

                        // Sample Rst Signal again
                        // Sample for Reset Pulse
                        if(INT_Is_Int_In_Active() )
                        {
                            // PASS - Re-Init The Sync Counter to 1
                            strobe_co_sync_count = 1;

                            #ifdef CONFIGURE_UNDEFINED
                                SER_Send_String("CO SYNC Detected! ");
                                SER_Send_Prompt();
                            #endif
                        }
                        else
                        {
                            // Fails Pulse Width Test
                            strobe_co_rst_state = STROBE_CO_RST_IDLE;
                        }
                    }
                }
                break;

                default:
                {
                    strobe_co_rst_state = STROBE_CO_RST_IDLE;
                }
                break;
            }                
        }
        else
        {
            // We are here because we are NOT in CO Alarm or Smoke Alarm
            // Initialize CO Sync Detect Timer and Flag for when we
            //  enter CO alarm, Sync on 1st CO Signal after 3
            //  Second INT_IN Dwell Time

            //
            // While not in Alarm, if any Interconnect Edge is detected
            // pre-load the CO Strobe Timer
            //
            if(INT_Is_Int_In_Active() )
            {
                //
                // Detected INT In activity, this might be an 
                //  incoming CO message. so preload the CO Strobe Timer
                //  in case we enter slave CO Alarm
                //
                // This is for trying to Sync up during CO Bat Conserve
                //
                if(1 == strobe_timer_co)
                {
                    // This is updated every 1ms until alarm
                    strobe_timer_co = STROBE_TIMER_CO_1_SEC;

                    #ifdef CONFIGURE_UNDEFINED
                        SER_Send_String("CO Timer Pre-load! ");
                        SER_Send_Prompt();
                    #endif

                }
                else
                {
                    // Already pre-loaded
                }
            }

            strobe_co_sync_count = CO_SYNC_COUNT;   // 7 CO Signals
            strobe_co_sync_timer = CO_SYNC_TIME_3_SEC;

            // Init for when CO Alarm becomes active
            FLAG_CO_STROBE_1ST_SET = 1;

            FLAG_CO_SYNC_PULSE_ENA = FALSE;
            FLAG_SMK_SYNC_PULSE_ENA = FALSE;

            // Re-Init CO Sync Reset Flags
            FLAG_CO_SYNC_RESET_ACTIVE = 0;
            FLAG_CO_SYNC_RESET_ENABLE = 0;

            strobe_co_rst_state = STROBE_CO_RST_IDLE;

            #ifdef CONFIGURE_STROBE_1_MS_TASK
                strobe_state_timer = 1;
            #endif


        }

    #endif  // CONFIGURE_STROBE_SYNC

    #ifdef CONFIGURE_STROBE_1_MS_TASK
        //
        // Add State Timer for 1 ms Task Timing   
        // This waits for defined Strobe state delay to complete
        //  before processing next state
        //
        if(--strobe_state_timer)
        {
            // Wait to process next state
            return (1);
        }
    #endif

    switch (strobe_state)
    {
        case	STROBE_STATE_INIT:
        {
            // Init Strobe I/O to Output Low
            STROBE_PULSE_OUTPUT

            strobe_state = STROBE_STATE_IDLE;
        }
        break;

        case	STROBE_STATE_IDLE:
        {
            FLAG_STROBE_NOT_IDLE = 0;

            if(FLAG_SMOKE_ALARM_ACTIVE)
            {
                strobe_timer_smk = STROBE_TIMER_SMOKE_INIT;

                FLAG_STROBE_NOT_IDLE = 1;
                strobe_state = STROBE_STATE_SMK_PULSE_ON;

                #ifdef CONFIGURE_STROBE_SYNC
                    strobe_pulse_cnt = STROBE_PULSE_SYNC_INIT;
                    strobe_smk_sync_off = 1;    // enable 1st Sync
                #endif

                #ifdef CONFIGURE_STROBE_1_MS_TASK
                    strobe_state_timer = 1;
                    return (1);
                #else
                    return MAIN_Make_Return_Value(STROBE_INTERVAL_10_MS);
                #endif
            }

            if(FLAG_CO_ALARM_ACTIVE)
            {
                //
                // If this is the Master or Gateway unit driving
                //  HW Interconnect Line increase the delay before
                //  beginning CO Strobe Pattern to more closely
                //  align strobe with receiving units. 
                // But only on the initial set of CO Strobes!
                //
                if(FLAG_CO_STROBE_1ST_SET  && 
                   (FLAG_CO_ALARM_CONDITION && !FLAG_SLAVE_CO_ACTIVE) ||
                   (FLAG_REMOTE_ALARM_MODE && FLAG_WL_GATEWAY_ENABLE &&
                      FLAG_GATEWAY_REMOTE_TO_SLAVE) )
                {
                    //
                    // Wait for Start of CO Alarm Signal transmission
                    //
                    if(INT_Is_Int_In_Active() )
                    {
                        FLAG_CO_STROBE_1ST_SET = 0;
                        
                        //strobe_timer_co = STROBE_TIMER_CO_1ST_INIT;
                        strobe_timer_co = STROBE_TIMER_CO_1_SEC;
                        
                        #ifdef CONFIGURE_UNDEFINED
                            SER_Send_String("CO Timer Init ");
                            SER_Send_Prompt();
                        #endif
                        
                    }
                    else
                    {
                        // Do not Proceed to STROBE_STATE_CO_PULSE_ON Yet!
                        break;
                    }
                     
                }
                else
                {
                    // Must be in CO Slave Alarm
                    FLAG_CO_STROBE_1ST_SET = 0;
                    
                    if( 1 == strobe_timer_co)
                    {
                        // In case there was no preload or preload timed out
                        //  before alarming
                        strobe_timer_co = STROBE_TIMER_CO_INIT;
                    }
                    else
                    {
                        // Use Preloaded Strobe Timer value
                        // which is based upon INT_IN leading edge
                    }
                }
                
                FLAG_STROBE_NOT_IDLE = 1;
                strobe_state = STROBE_STATE_CO_PULSE_ON;

                // We Need 4 Pulses every 5 Seconds
                strobe_pulse_cnt = STROBE_PULSE_CO_COUNT;

                #ifdef CONFIGURE_STROBE_1_MS_TASK
                    strobe_state_timer = 1;
                    return (1);
                #else
                    return MAIN_Make_Return_Value(STROBE_INTERVAL_10_MS);
                #endif
            }
            
            //
            // If Not in Alarm Yet, Update preloaded CO Strobe Timer
            //
            if(0 == --strobe_timer_co)
            {
                //
                // Hold at 1 count if CO Strobe Timer timed out before alarming
                // 
                strobe_timer_co = 1;
            }
            
        }
        break;

        case	STROBE_STATE_SMK_PULSE_ON:
        {
            if(FLAG_SMOKE_ALARM_ACTIVE)
            {
                #ifdef CONFIGURE_STROBE_SYNC
                    // Update the Strobe Sync Timer
                    if( strobe_sync_timer != 0 )
                    {
                        if( 0 == --strobe_sync_timer)
                        {
                            //
                            // If master Smoke and Not in slave alarm
                            // or Remote driving Interconnect end sync pulse
                            //
                            if( (FLAG_MASTER_SMOKE_ACTIVE && 
                                !FLAG_SLAVE_SMOKE_ACTIVE) ||
                                    
                                    
                                (!FLAG_SLAVE_SMOKE_ACTIVE &&
                                INTCON_STATE_REMOTE_SMOKE == INT_get_state()) )
                            {
                                // Interconnect Sync Pulse End for Master Smoke
                                // Slave Strobe units will reset their strobe
                                // Timers on this Rising Edge
                                INT_OUT_HIGH
                                        
                                FLAG_SMK_SYNC_ACTIVE = 0;        
                            }
                        }
                    }
                #endif

                if(0 == --strobe_timer_smk)
                {
                    // Drive Strobe Pulse if sync has been detected
                    STROBE_PULSE_ON

                    strobe_timer_smk = STROBE_TIMER_SMOKE_ON_50_MS;
                    strobe_state = STROBE_STATE_SMK_PULSE_OFF;

                    #ifdef CONFIGURE_DEBUG_STROBE_TEST
                        MAIN_TP2_On();
                    #endif

                    #ifdef CONFIGURE_STROBE_SYNC
                    // Master Smokes with Strobe will drive a Strobe Sync Pulse
                    //  every 5 strobe periods.
                        
                        //
                        // If master Smoke and Not in slave alarm
                        // or Remote driving Interconnect start sync pulse
                        //

                        if( (FLAG_MASTER_SMOKE_ACTIVE && 
                                !FLAG_SLAVE_SMOKE_ACTIVE) ||
                                
                            (!FLAG_SLAVE_SMOKE_ACTIVE &&
                              INTCON_STATE_REMOTE_SMOKE == INT_get_state()) )
                        {
                            if(0 == --strobe_pulse_cnt)
                            {
                                // Interconnect Sync Pulse for Master Smoke
                                 INT_OUT_LOW
                                 FLAG_SMK_SYNC_ACTIVE = 1;    
                                 
                                // Reset the Sync. Counter
                                strobe_pulse_cnt = STROBE_PULSE_SYNC_COUNT;

                                // Start the Sync Pulse width Timer
                                strobe_sync_timer = STROBE_TIMER_SMK_SYNC;
                            }
                        }
                    #endif
                }
            }
            else
            {
                strobe_state = STROBE_STATE_IDLE;
                
                // Init some CO stuff in case CO Alarm is active at this time
                FLAG_CO_STROBE_1ST_SET = 0;
                strobe_timer_co = 1;
                strobe_co_sync_count = CO_SYNC_COUNT;

                strobe_co_sync_timer = CO_SYNC_TIME_3_SEC;
                
                FLAG_CO_SYNC_PULSE_ENA = FALSE;
                
                // Re-Init CO Sync Reset Flags
                FLAG_CO_SYNC_RESET_ACTIVE = 0;
                FLAG_CO_SYNC_RESET_ENABLE = 0;
                
                strobe_co_rst_state = STROBE_CO_RST_IDLE;
                
            }
        }
        break;

        case	STROBE_STATE_SMK_PULSE_OFF:
        {
            #ifdef CONFIGURE_STROBE_SYNC
                // Update Strobe Sync Pulse Timer
                strobe_sync_timer--;
            #endif

            if(0 == --strobe_timer_smk)
            {
                // Drive Strobe Pulse
                STROBE_PULSE_OFF
                strobe_timer_smk = STROBE_TIMER_SMOKE_OFF_930_MS;
                strobe_state = STROBE_STATE_SMK_PULSE_ON;

                #ifdef CONFIGURE_DEBUG_STROBE_TEST
                    MAIN_TP2_Off();
                #endif
            }
        }
        break;

        case	STROBE_STATE_CO_PULSE_ON:
        {
            #ifdef CONFIGURE_UNDEFINED  
                #ifdef CONFIGURE_DEBUG_STROBE_TEST
                    MAIN_TP_Off();
                #endif
            #endif
            
            if( FLAG_CO_ALARM_ACTIVE  ||
                 (!FLAG_CO_ALARM_ACTIVE && 
                   strobe_pulse_cnt != STROBE_PULSE_CO_COUNT) )
            {
                if(0 == --strobe_timer_co)
                {
                    // Drive Strobe Pulse
                    STROBE_PULSE_ON

                    strobe_timer_co = STROBE_TIMER_CO_ON_50_MS;
                    strobe_state = STROBE_STATE_CO_PULSE_OFF;
                    
                    #ifdef CONFIGURE_DEBUG_STROBE_TEST
                        MAIN_TP2_On();
                    #endif

                }
            }
            else
            {
                strobe_state = STROBE_STATE_IDLE;
            }
        }
        break;

        case	STROBE_STATE_CO_PULSE_OFF:
        {
            if(0 == --strobe_timer_co)
            {
                // Drive Strobe Pulse
                STROBE_PULSE_OFF
                
                #ifdef CONFIGURE_DEBUG_STROBE_TEST
                    MAIN_TP2_Off();
                #endif

                if(0 == --strobe_pulse_cnt)
                {
                    // We have completed the 4 CO pulses
                    strobe_pulse_cnt = STROBE_PULSE_CO_COUNT;
                    strobe_timer_co = STROBE_TIMER_CO_DWELL;
                    strobe_state = STROBE_STATE_CO_PULSE_DWELL;
                }
                else
                {
                    strobe_timer_co = STROBE_TIMER_CO_OFF_930_MS;
                    strobe_state = STROBE_STATE_CO_PULSE_ON;
                }
            }
        }
        break;

        case	STROBE_STATE_CO_PULSE_DWELL:
        {
            if(0 == --strobe_timer_co)
            {
                // We have completed the Dwell Time, next set of pulses
                strobe_timer_co = 1;   // Start next set immediately
                strobe_state = STROBE_STATE_CO_PULSE_ON;
            }
        }
        break;
        
        default:
        {
            strobe_state = STROBE_STATE_INIT;
        }
        break;
    }
    
    #ifdef CONFIGURE_STROBE_1_MS_TASK
        strobe_state_timer = 1;
        return (1);
    #else
        return MAIN_Make_Return_Value(STROBE_INTERVAL_10_MS);
    #endif
}


#endif //CONFIGURE_STROBE_SYNC