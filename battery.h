/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        battery.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for battery.c module
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

#ifndef	BATTERY_H
#define	BATTERY_H



/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

/*
 * R1 = 13.3 ohm
 * R2 = 6.65 ohm
 * LB volt = 2550mv
 * A/D Vref = 2.048 volts (+/- 2%)
 *
 * I = 2550/(13.3+6.65) = 127.8 mamps
 * VLB = 6.65 x 127.8 = 850 mv
 * LB A/D value = 850/2048 * 256 = 106
 */

// Note: One A.D count change at 2.048 VRef equals 23.5 mvolt change at the 
//       battery using R1/R2 above


//*****************************************************************************/
/* If we Increase LB Threshold to 2.60 Volts
 * I = 2600/13.3+6.65 = 130.33 mamps
 * VLB = 6.65 x 130.33 = 866.69 mv
 * LB A/D value = 867/2048 * 256 = 108.375
 */

#define BAT_LOW_BAT_THRESHOLD_DEFAULT   (uchar)106


/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
unsigned int BAT_Test_Task(void);
void BAT_LB_Cal_Init(void);
uchar BAT_Check_LP_Mode_Timer(void);
uchar BAT_Get_Last_Voltage(void);
void BAT_Init_Last_Voltage(void);

#ifdef  CONFIGURE_WIRELESS_BAT_STATUS
    uchar BAT_Get_Status (void);
#endif


#ifdef CONFIGURE_LB_HUSH
    void BAT_Init_LB_Hush(void);
    void BAT_Check_LB_Hush_Timer(void);
#endif




	
#endif  // BATTERY_H

