/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        alarmmp.h
|  Author:      Stan Burnette, updated for MOAL Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for alarmmp.c module
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

#ifndef ALARMMP_H
#define	ALARMMP_H


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include "cci_phy.h"


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/
// This is the version that will be reported by the AMP_REQUEST_AMP_VER
// request.
#define AMP_MAJOR_VERSION   2
#define AMP_MINOR_VERSION   0


// Bits for AMP_COMM_STATUS and AMP_COMM_STATUS_REQ
#define AMP_COMM_STAT_JOIN_COMPLETE         0x01
#define AMP_COMM_STAT_JOIN_NPROGRESS        0x02
#define AMP_COMM_STAT_COMM_ONLINE           0x04
#define AMP_COMM_STAT_ENABLE_REG            0x08
#define AMP_COMM_STAT_RESET_COMM_OOB        0x10
#define AMP_COMM_STAT_UNTESTED              0x20
#define AMP_COMM_STAT_COORDINATOR           0x40
#define AMP_COMM_STAT_RFD                   0x80


// Bits for AMP_STATUS1
#define AMP_STAT1_ALARM_CANCEL_HUSH         0x01
#define AMP_STAT1_ALARM_CANCEL_ACTIVE       0x02
#define AMP_STAT1_HUSH_ACTIVE               0x04
#define AMP_STAT1_SELFTEST_ACTIVE           0x08
#define AMP_STAT1_TXRX_DISABLE_REQ          0x10
#define AMP_STAT1_TXRX_DISABLED             0x20
#define AMP_STAT1_TXRX_FINAL_TESTED         0x40

// Bits for AMP_STATUS2
#define AMP_ALARM_STAT2_TEMP_INCR           0x01
#define AMP_ALARM_STAT2_TEMP_DROP           0x02
#define AMP_ALARM_STAT2_WATER               0x04
#define AMP_ALARM_STAT2_TAMPER              0x08
#define AMP_ALARM_STAT2_SOUND_DETECT        0x10
#define AMP_ALARM_STAT2_RMT_LOWBAT          0x20
#define AMP_STAT2_FAULT_RESET               0x40
#define AMP_STAT2_PANIC                     0x80

// Bits for AMP_STATUS3
#define AMP_ALARM_STAT3_GAS                 0x01
#define AMP_ALARM_STAT3_INTRUDER            0x02
#define AMP_ALARM_STAT3_WEATHER             0x04
#define AMP_ALARM_STAT3_HEAT_ALARM          0x08
#define AMP_ALARM_STAT3_SPARE               0x10
#define AMP_ALARM_STAT3_GEN_ALERT           0x20


// Bits for AMP_STATUS and AMP_STATUS_REQ
#define AMP_STATUS_LB_ACTIVE                0x01
#define AMP_STATUS_SMK_ALARM_ACTIVE         0x02
#define AMP_STATUS_CO_ALARM_ACTIVE          0x04
#define AMP_STATUS_RMT_SMOKE_RCVD           0x08
#define AMP_STATUS_RMT_CO_RCVD              0x10
#define AMP_STATUS_HW_INT_ACTIVE            0x20
#define AMP_STATUS_FAULT_ACTIVE             0x40
#define AMP_STATUS_RMT_PTT_RCVD             0x80

// Bits for AMP_PTT
#define AMP_PTT_SILENT_REQUEST              0x01
#define AMP_PTT_LOCAL                       0x02
#define AMP_PTT_ARM                         0x04

// Bits for AMP_LOCATE
#define AMP_LOCATE_ALARM                    0x01
#define AMP_LOCATE_LOW_BAT                  0x02
#define AMP_LOCATE_FAULT                    0x04
#define AMP_LOCATE_CLEAR                    0x10

// Model Numbers for AMP_MODEL_NUMBER
#define AMP_MODEL_10Y29Q3                   0x01



// Bits for AMP_CAPABILITY1
#define AMP_CAP1_CO_PPM                     0x01
#define AMP_CAP1_SMOKE_READING              0x02
#define AMP_CAP1_SMOKE_COMP                 0x04
#define AMP_CAP1_BAT_VOLT                   0x08
#define AMP_CAP1_TEMPERATURE                0x10
#define AMP_CAP1_HUMIDITY                   0x20
#define AMP_CAP1_SOUND_READING              0x40
#define AMP_CAP1_HEAT_ROR                   0x80

// Bits for AMP_CAPABILITY2
#define AMP_CAP2_RESERVED1                  0x01
#define AMP_CAP2_RESERVED2                  0x02
#define AMP_CAP2_RESERVED3                  0x04
#define AMP_CAP2_RESERVED4                  0x08
#define AMP_CAP2_RESERVED5                  0x10
#define AMP_CAP2_RESERVED6                  0x20
#define AMP_CAP2_RESERVED7                  0x40
#define AMP_CAP2_RESERVED8                  0x80

// Bits for AMP_FUNCTION
#define AMP_FUNCTION_ESCAPE_LIGHT           0x01

// Fault Codes
// Alarm Faults
#define AMP_FAULT_1                         1
#define AMP_FAULT_CONVERSION_SETUP          2
#define AMP_FAULT_SENSOR_COMP               3
#define AMP_FAULT_SENSOR_SHORT              4
#define AMP_FAULT_GAS                       5
#define AMP_FAULT_CALIBRATION               6
#define AMP_FAULT_PTT                       7
#define AMP_FAULT_MEMORY                    8
#define AMP_FAULT_EXPIRATION                9
#define AMP_FAULT_SMK_SUPERVISION           10
#define AMP_FAULT_MEMORY_DIAG               11
#define AMP_FAULT_TEMPERATURE               12
#define AMP_FAULT_INTERCON_SUPERVISION      13
#define AMP_FAULT_SMK_DRIFT_COMP            14

#define AMP_FAULT_WIRELESS_HOST             20
#define AMP_FAULT_ACOUSTIC_COMM             21
#define AMP_FAULT_ACOUSTIC_SENSOR           22

// Wireless Fault Codes
#define AMP_FAULT_COORDINATOR               34      //0x22
#define AMP_FAULT_RFD_JOIN                  35      //0x23
#define AMP_FAULT_CCI                       36      //0x24
#define AMP_FAULT_RFD_CHECK_IN              37      //0x25
#define AMP_FAULT_TIME_SYNC                 38      //0x26
#define AMP_FAULT_WM_BATTERY_EOL            39      //0x27


// For a remote fault, or in the MSB
#define AMP_REMOTE_FAULT                    0x80

// Model Numbers for AMP_MODEL_NUMBER
#define AMP_MODEL_10Y29Q3                   0x01
#define P4010ACSCO_W_2556                   0x02	
#define P4010DCSCO_W_2558                   0x03	
#define P4010ACSCO_W_2556_CA                0x04	
#define P4010DCSCO_W_2558_CA                0x05
#define P4010ACSCO_W_2556_EN                0x06	
#define P4010DCSCO_W_2558_EN                0x07	
#define P4010LACS_W_1314                    0x08	
#define P4010ACS_W_1316                     0x09	
#define P4010LDCS_W_0314                    0x0A	
#define P4010DCS_W_0316                     0x0B	
#define P4010LACS_W_1314_CA                 0x0C	
#define P4010ACS_W_1316_CA                  0x0D	
#define P4010LDCS_W_0314_CA                 0x0E	
#define P4010DCS_W_0316_CA                  0x0F	
#define KMSLW                               0x10	
#define KMSW                                0x11	
#define KMSCOW                              0x12	
#define KMHCOW                              0x13	
#define SDX_135Z                            0x14
#define CDX_135Z                            0x15
#define PROPWIRELESS_US                     0x16	
#define PROPWIRELESS_EN                     0x17	
#define PROPWIRELESS_US_HUB                 0x18
#define K4000LDCS_WB                        0x19

// Models 0x1A - 0x1F not yet defined

#define K4000DCS_WB                         0x20
#define KSBW                                0x21
#define KBSLW                               0x22
#define KBSCOW                              0x23
#define KBHCOW                              0x24
#define COX_135z                            0x25


// Added WiFi models
#define P4010ACSCO_W_2581                   0x26
#define P4010ACS_W_1381                     0x27
/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/

// AMP Status Bit Constants
enum
{
    FLAG_AMP_STATUS_START_TRANSACTION = 0x01,
    FLAG_AMP_STATUS_RESPONSE_RECEIVED = 0x02,
    FLAG_AMP_STATUS_RESPONSE_TIMEOUT = 0x04,
    FLAG_AMP_STATUS_TX_ERROR = 0x08
};


// AMP transport manager results
// Currently not used
enum
{
    // Response received successfully.
    AMP_TM_SUCCESS = 0,
    // No response available.
    AMP_TM_NONE = 1,
    // Timeout waiting for response.
    AMP_TM_TIMEOUT = 2,
    // Error during transmission of packet
    FLAG_TM_TX_ERROR = 3
};

// Message protocol tag definitions
enum
{
    AMP_DO_NOT_USE = 0x00,
    AMP_PTT = 0x04,
    AMP_REQ_BAT_VOLT = 0x08,
    AMP_PING = 0x0c,
    AMP_NOT_SUPPORTED = 0x10,
    AMP_C0_PPM = 0x14,
    AMP_COMM_STATUS = 0x18,
    AMP_COMM_INIT = 0x1c,
    AMP_COMM_LEARN = 0x20,
    AMP_COMM_STATUS_REQ = 0x24,
    AMP_SMOKE_COMP = 0x28,
    AMP_PING_ACK = 0x2c,
    AMP_SMOKE = 0x30,
    AMP_SERIAL_DATA = 0x34,
    AMP_ERROR_CODE = 0x38,
    AMP_LIFE = 0x3c,
    AMP_MODEL_NUMBER = 0x40,
    AMP_STATUS_REQUEST = 0x44,
    AMP_ACOUSTIC_STATUS = 0x48,
    AMP_ACOUSTIC_STATUS_REQ = 0x4c,
    AMP_SET_ID = 0x50,
    AMP_LOCATE = 0x54,
    AMP_STATUS1 = 0x58,
    AMP_COMM_STATUS_ACK = 0x5c,
    AMP_VERSION_REQ = 0x60,
    AMP_REQUEST_AMP_VER = 0x64,
    AMP_GET_LOGICAL_ADDRESS = 0x68,
    AMP_STATUS = 0x6C,
    AMP_RF_TEST_STATUS = 0x70,
    AMP_RF_PACKET = 0x74,
    AMP_STATUS1_REQ = 0x78,
    AMP_TEMPERATURE = 0x7C,
    AMP_FUNCTION = 0x80,
    AMP_HUMIDITY = 0x84,
    AMP_SOUND = 0x88,
    AMP_CAPABILITY1 = 0x8C,
    AMP_CAPABILITY2 = 0x90,
    AMP_HEAT_ROR = 0x94,
    AMP_STATUS2 = 0x98,
    AMP_NETWORK_SIZE = 0x9C,
    AMP_SET_TIME_SLOT = 0xA0,
    AMP_RF_DATA_REQUEST = 0xA4,
    AMP_SN_BYTE0 = 0xA8,
    AMP_SN_BYTE1 = 0xAC,
    AMP_SN_BYTE2 = 0xB0,
    AMP_SN_BYTE3 = 0xB4,
    AMP_NET_LIB_VER = 0xB8,
    AMP_STATUS3 = 0xBC,
    AMP_STATUS2_REQUEST = 0xC0,
    AMP_IDENTIFY = 0xC4,
    AMP_HOST_VERSION = 0xC8,
    AMP_FREEZE = 0xCC
            
} ;



/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/
    extern uchar AMP_Callback_Status;

/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
uchar AMP_Transport_Manager(void);

#ifdef  ALARMMP_C
    // already defined
#else

    uchar AMP_Start_Transaction(union  tag_CCI_Packet TxPacket);
    
    #ifdef CONFIGURE_UNDEFINED
        void AMP_Register_Callback(void (*funcptr)(uchar data));
    
        uchar AMP_Get_Packet(union tag_CCI_Packet *AMP_RxPacket);
        void AMP_Start_Enroll(void);
        void AMP_Get_Comm_Status(void);
        void AMP_Set_Comm_Status(uchar status_byte);
    #endif
    

#endif


#endif	/* ALARMMP_H */

