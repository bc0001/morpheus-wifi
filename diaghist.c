/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        diaghist.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Routines to support recording of paramaters and events in
|                Non-Volatile memory referred to as Diagnostic History
|               
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/



/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include    "common.h"
#include    "memory_18877.h"
#include    "diaghist.h"

#include    "life.h"
#include    "systemdata.h"
#include    "cocalibrate.h"
#include    "comeasure.h"

#include    <stddef.h>


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

#define DIAG_QUE_PTR_CSUM    0
#define DIAG_QUE_PTR_MAJOR   1
#define DIAG_QUE_PTR_MINOR   2

#define DIAG_HIST_MAX_COUNT (uchar)255

#define DIAG_NOT_INITIALIZED 0xFFFF

/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
#ifdef CONFIGURE_DIAG_HIST
    uchar diag_event_buf[2];
    
    #ifdef	CONFIGURE_CO
        // Declare and Init Data value for Peak CO record
        static uint diag_peak_CO = DIAG_NOT_INITIALIZED;
    #endif

#endif
        

/*
 * DiagHist is declared and initialized within the sys_nv_data structure
 * so that it can be placed within EE data starting at EE Address 0x20
 * 
 * Initialize Diag History in Flash Memory
 */
#ifdef CONFIGURE_UNDEFINED
    // For Reference Only
struct DIAG_HistData eeprom DiagHist =
{
    // 0x20 - 0x2F
    0,0,0,00,00,00,0,0,00,0,REVISION_MAJOR,REVISION_MINOR,
    // 0x30 - 0x3F
    0,0,0,0,00,00,0,0,0,0,0,QPTR_CKSUM_INIT,MAJOR_QPTR_INIT,MINOR_QPTR_INIT,

    // 0x40 - 0x9F
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,

    // 0xA0 - 0xFF
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
};
#endif
        

/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/

#ifdef CONFIGURE_DIAG_HIST

/*
+------------------------------------------------------------------------------
| Function:      DIAG_Hist_Que_Push
+------------------------------------------------------------------------------
| Purpose:       Push a recordable Event Code into the History memory
|
|
+------------------------------------------------------------------------------
| Parameters:    Inputs - Event Code to record 
|                         (see header file event definitions)
|                Outputs - none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void DIAG_Hist_Que_Push(uchar event_code)
{
    uint day = 0000;
    uchar day_h, day_l = 00;
    uchar event;
    uchar event_ptr[3];     //Holds Event Queue Pointer values
    uchar count_address;

    #ifdef CONFIGURE_CO
        // No Diagnostic Record, If not Calibrated except for Cal faults
        if( CAL_STATE_CALIBRATED != CO_Cal_Get_State() )
        {
            return;
        }
    #else
        // If it is a Smoke Only Model
        if(FLAG_NO_SMOKE_CAL)
        {
            return;
        }
    #endif

    if(HIST_NETWORK_SEARCH == event_code)
    {
        // Do not record this Event, update its event counter only
        // Save for Event Counter Lookup
        event = event_code;

    }
    else if(HIST_NETWORK_ERROR == event_code)
    {
        // Do not record this Event, update its event counter only
        // Save for Event Counter Lookup
        event = event_code;

    }
    else
    {
        // First read the 3 Event Queue Pointer values for use by QPush
        MEMORY_Read_Block(3, DIAG_QUE_CKSUM, event_ptr);

        // 1st Verify Event Pointers are valid before any writes to EEProm
        // Had to Cast the CKSUM PTR addition to UCHAR for the comparison to 
        //  work
        if(event_ptr[DIAG_QUE_PTR_CSUM] != 
                (uchar)(event_ptr[DIAG_QUE_PTR_MAJOR] +
                       event_ptr[DIAG_QUE_PTR_MINOR]) )
        {
            // Try a second time
            MEMORY_Read_Block(3, DIAG_QUE_CKSUM, event_ptr);

            // Verify Event Pointers are valid before any writes to EEProm
            if(event_ptr[DIAG_QUE_PTR_CSUM] != 
                    (uchar)(event_ptr[DIAG_QUE_PTR_MAJOR] + 
                            event_ptr[DIAG_QUE_PTR_MINOR]) )
            {
                #ifdef CONFIGURE_UNDEFINED
                    // Test Only
                    //SER_Send_Int('X', event_ptr[DIAG_QUE_PTR_CSUM], 16);
                #endif

                // Inhibit Writing to EE Memory if Diag Hist Que pointers are 
                //  corrupted
                FLAG_DIAG_HIST_DISABLE = 1;
                return;
            }
        }
        else
        {
            #ifdef CONFIGURE_UNDEFINED
                // Test Only
                //SER_Send_Int('+', event_ptr[DIAG_QUE_PTR_CSUM], 16);
            #endif

            FLAG_DIAG_HIST_DISABLE = 0;
        }

        // Save for Event Counter Lookup
        event = event_code;

        // If MSB is set, this is a major event
        if(event_code & 0x80)
        {
            // Major Events
            event_code = event_code << 4;		// Place in MS nibble
            day = LIFE_Get_Current_Day();

            // Format Day to fit into Diag data Structure
            //day_h = day & 0x0F00;
            day_h = day >> 8;
            day_l = day & 0x00FF;
            event_code = event_code | day_h;

            diag_event_buf[DIAG_EVENT_CODE] = event_code;
            diag_event_buf[DIAG_EVENT_DAY] = day_l;

            // At this point need to write Hist events into EE memory
            /*
             * 1. Write Event Code/Day at Queue pointer address
             * 2. Update Queue pointer and Checksum
             * 3. Write back into EEProm
             */

            // Record Event in EEProm History
            MEMORY_Write_Block(2, &diag_event_buf[DIAG_EVENT_CODE], 
                                   event_ptr[DIAG_QUE_PTR_MAJOR]);

            // Update Queue Pointer
            event_ptr[DIAG_QUE_PTR_MAJOR]+= 2;

            if(event_ptr[DIAG_QUE_PTR_MAJOR] > DIAG_MAJOR_QUE_END )
            {
                event_ptr[DIAG_QUE_PTR_MAJOR] = DIAG_MAJOR_QUE_START;
            }

        }
        else
        {

            // Minor Events
            event_code = event_code << 4;		// Place in MS nibble
            day = LIFE_Get_Current_Day();

            // Format Day to fit into Diag data Structure
            //day_h = day & 0x0F00;
            day_h = day >> 8;
            day_l = day & 0x00FF;
            event_code = event_code | day_h;

            diag_event_buf[DIAG_EVENT_CODE] = event_code;
            diag_event_buf[DIAG_EVENT_DAY] = day_l;

            // At this point need to write Hist events into EE memory
            /*
             * 1. Write Event Code/Day at Queue pointer address
             * 2. Update Queue pointer and Checksum
             * 3. Write back into EEProm
             */

            // Record Event in EEProm History
            MEMORY_Write_Block(2, &diag_event_buf[DIAG_EVENT_CODE], 
                                   event_ptr[DIAG_QUE_PTR_MINOR]);

            // Update Queue Pointer
            event_ptr[DIAG_QUE_PTR_MINOR]+= 2;

            if(event_ptr[DIAG_QUE_PTR_MINOR] < DIAG_MINOR_QUE_START )
            {
                event_ptr[DIAG_QUE_PTR_MINOR] = DIAG_MINOR_QUE_START;
            }

        }

        // Update the Queue Checksum
        // Add the 2 Queue pointers and write into queue csum
        event_ptr[DIAG_QUE_PTR_CSUM] = (uchar)(event_ptr[DIAG_QUE_PTR_MAJOR] +
                                               event_ptr[DIAG_QUE_PTR_MINOR]);

        MEMORY_Write_Block(3, event_ptr, DIAG_QUE_CKSUM);
    
    }

	// Test Event for counter updates
	// Add Events if event counter is supported for that event
	switch (event)
	{
		case HIST_POWER_RESET:
             count_address = DIAG_CNT_POWER_RESET;
		break;

		case HIST_PTT_EVENT:
             count_address = DIAG_CNT_PTT;
		break;

        case HIST_LOW_BATT_MODE:
            count_address = DIAG_CNT_LOW_BATT;
 		break;

        case HIST_ABN_LOW_BATT_EVENT:
            count_address = DIAG_CNT_LOW_BATT;
 		break;

        case HIST_ALARM_ON_EVENT:
            count_address = DIAG_CNT_ALARM_ON;

            #ifdef CONFIGURE_CO
                // Also good place to record Alarm CO PPM
                MEMORY_Byte_Write_Verify((uchar)(CO_wPPM & 0xff),
                                                 DIAG_CO_LAST_ALARM);
                MEMORY_Byte_Write_Verify((uchar)(CO_wPPM >> 8),
                                                 DIAG_CO_LAST_ALARM + 1);
            #endif

		break;

		case HIST_SENS_SHORT_FAULT:
            count_address = DIAG_CNT_SENS_SHORT;
		break;

		case HIST_SENS_HEALTH_FAULT:
            count_address = DIAG_CNT_SENS_HEALTH;
 		break;

		case HIST_AC_POWER_ON:
            count_address = DIAG_CNT_AC_POWER;
		break;

        case HIST_PHOTOCHAMBER_FAULT:
            count_address = DIAG_CNT_SMK_CHAMBER;
        break;

        case HIST_SMK_ALARM_ON_EVENT:
            count_address = DIAG_CNT_SMK_ALARM;
        break;
        
        case HIST_NETWORK_SEARCH:
            count_address = DIAG_CNT_NET_SEARCH;
        break;
        
        case HIST_NETWORK_ERROR:
            count_address = DIAG_CNT_NET_ERR;
        break;
        
#ifdef CONFIGURE_ACTIVE_TIMEOUT_TIMER 
        case HIST_ACTIVE_TIMEOUT:
            count_address = DIAG_CNT_ACTIVE_T_O;
        break;
#endif

        default:
            // Not a counter event
            count_address = DIAG_EVENT_NO_COUNTER;
        break;

    }
    if(count_address != DIAG_EVENT_NO_COUNTER)
    {
        // Check/Update the count here
        uchar count = MEMORY_Byte_Read(count_address);
        if(count < DIAG_HIST_MAX_COUNT)
        {
            count++;
            MEMORY_Byte_Write_Verify(count,count_address);
        }
    }
}

/*
+------------------------------------------------------------------------------
| Function:      DIAG_Hist_Bat_Record
+------------------------------------------------------------------------------
| Purpose:       Record Battery Voltages
|                Battery voltages are recorded at end of Day 1, Year 1 and
|                at the end of each 24 hour period.
|
|
+------------------------------------------------------------------------------
| Parameters:    Inputs - battime = (BAT_DAY_ONE, BAT_YEAR_ONE or BAT_CURRENT)
|                         batvolt =  value to be recorded (8 bit value)
|                Outputs - none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void DIAG_Hist_Bat_Record(uchar battime, uchar batvolt)
{

    switch (battime)
    {
        case BAT_DAY_ONE:
            MEMORY_Byte_Write_Verify(batvolt, DIAG_BATTERY_DAY_1);
        break;

        case BAT_YEAR_ONE:
            MEMORY_Byte_Write_Verify(batvolt, DIAG_BATTERY_YEAR_1);
        break;

        case  BAT_CURRENT:
            MEMORY_Byte_Write_Verify(batvolt, DIAG_BATTERY_CURRENT);
        break;
    }
}

#ifdef	CONFIGURE_CO

/*
+------------------------------------------------------------------------------
| Function:      DIAG_Hist_Test_Peak
+------------------------------------------------------------------------------
| Purpose:       Test for Peak CO update to record unit lifetime Peak CO
|                
|
|
+------------------------------------------------------------------------------
| Parameters:    Inputs  - ppm value to test against the current peak value
|                          in history memory
|                Outputs - none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void DIAG_Hist_Test_Peak(uint ppm)
{
    uint day;

    // Do not record Peak CO if in CO fault or if
    // unit is not calibrated.
    if( CAL_STATE_CALIBRATED == CO_Cal_Get_State() )
    {
        if(!FLAG_CO_FAULT_PENDING)
        {
            
            // If ppm > recorded peak CO, save new peak CO
            if(DIAG_NOT_INITIALIZED == diag_peak_CO)
            {
                // Init Peak Co Data variable
                uint peak_low = (uint)MEMORY_Byte_Read(DIAG_PEAK_CO_VALUE);
                uint peak_high = (uint)MEMORY_Byte_Read(DIAG_PEAK_CO_VALUE + 1);
                uint  peak = ((256 * peak_high ) + peak_low);
                
                // Init Peak Data copy
                diag_peak_CO = peak;
            }
            else
            {
                // Normally we use Peak CO Data copy to speed up peak test 
                //  and prevent 10 ms tic time overrun
            }

            if(ppm > diag_peak_CO)
            {
                // Write New peak
                uchar low = (uchar) ppm & 0xff;
                uchar high = (ppm >> 8);
                MEMORY_Byte_Write_Verify(low,DIAG_PEAK_CO_VALUE);
                MEMORY_Byte_Write_Verify(high,(DIAG_PEAK_CO_VALUE + 1) );
                
                // Update Peak Day
                day = LIFE_Get_Current_Day();
                low = (uchar) day & 0xff;
                high = (day >> 8);
                MEMORY_Byte_Write_Verify(low,DIAG_PEAK_CO_DAY);
                MEMORY_Byte_Write_Verify(high,(DIAG_PEAK_CO_DAY + 1) );
                
                // Also update Data copy of Peak Value
                diag_peak_CO = ppm;
            }
        }
    }
	
}

#endif




/*
+------------------------------------------------------------------------------
| Function:      DIAG_Hist_EE_Error
+------------------------------------------------------------------------------
| Purpose:       Monitor # of EE Write errors encountered
|                
|
+------------------------------------------------------------------------------
| Parameters:    Inputs  - none
|                Outputs - none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void DIAG_Hist_EE_Error(void)
{
    // Inc EE Error Count
    uchar count = MEMORY_Byte_Read(DIAG_CNT_EE_ERR);
    if(count < DIAG_HIST_MAX_COUNT)
    {
        count++;
        MEMORY_Byte_Write(count,DIAG_CNT_EE_ERR);
    }
}

/*
+------------------------------------------------------------------------------
| Function:      DIAG_Hist_Record_Avg_CAV
+------------------------------------------------------------------------------
| Purpose:       Record CAV average
|                
|
+------------------------------------------------------------------------------
| Parameters:    Inputs  - none
|                Outputs - none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void DIAG_Hist_Record_Avg_CAV(uchar avg)
{
    MEMORY_Byte_Write_Verify(avg,DIAG_SMK_CAV_AVERAGE);
}

#else

// Diag History not configured
// Dummy function placeholders when Diag History is Disabled

void DIAG_Hist_EE_Error(void)
{
}

#endif





