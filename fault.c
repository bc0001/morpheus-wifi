/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        fault.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Fault Handler Routines
|               
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/



/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/

#include    "common.h"


#include    "fault.h"
#include    "diaghist.h"
#include    "led.h"

#ifdef CONFIGURE_WIRELESS_MODULE
    #include    "alarmmp.h"
    #include    "app.h"
#endif


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

#define FAULT_CONVERSION_THRESHOLD      (uchar)2	// 2 attempts (60 sec apart)
#define FAULT_SENSOR_SHORT_THRESHOLD	(uchar)30	// 30 attempts (1 sec apart)
#define FAULT_GAS_THRESHOLD             (uchar)4	// 4 attempts
#define FAULT_SMOKE_CHAMBER_THRESHOLD   (uchar)2    // 2 attempts
#define FAULT_CCI_THRESHOLD             (uchar)3    // 3 attempts

#define FLT_ERR_HUSH_24_HOURS       (uchar)24
#define FLT_ERR_HUSH_3_MINUTES      (uchar)3

#define FLT_NET_SIZE_OF_3           (uchar)3

#define FLT_NET_ERROR_BIT           (uchar)0x20

/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/
uchar FLT_Fatal_Code = 0;

#ifdef CONFIGURE_WIRELESS_MODULE
    uchar FLT_NetworkFaultCode = 0;
#endif


/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
static uchar bErrorConversionCount = 0;
static uchar bErrorShortCount = 0;
static uchar bErrorChamberCount = 0;

#ifdef CONFIGURE_WIRELESS_MODULE
    static uchar fault_CCI_count = 0;
    static uchar fault_err_hush_timer = 0;
#endif

/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/
void fault_set_fatal_mode(uchar fault);

#ifdef CONFIGURE_WIRELESS_MODULE
    void fault_set_network_error_mode(uchar fault);
#endif

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/

    
/*
+------------------------------------------------------------------------------
| Function:      FLT_Fault_Manager
+------------------------------------------------------------------------------
| Purpose:       Handles Passed Fault and Success conditions
|                Automatically handles fault entry and fault recovery 
|                for Fault Codes passed to Fault manager
|
+------------------------------------------------------------------------------
| Parameters:    faultcode - Fault or Success Code
+------------------------------------------------------------------------------
| Return Value:  8 bit result of fault processing action
+------------------------------------------------------------------------------
*/
    
uchar FLT_Fault_Manager(uchar faultcode)
{
     // Success Code or Fault Code?
    if(faultcode & FAULT_NO_FAULT)
    {
        // Success Code was passed
        // Process Passed Success Code
        switch (faultcode)
        {

            case SUCCESS_CONVERSION_SETUP:
                bErrorConversionCount=0;
                // If current fault status is Sensor Test Fault
                if(FAULT_CO_HEALTH == FLT_Fatal_Code)
                {
                    FLAG_CO_FAULT_PENDING = 0;
                }

            break;

            case SUCCESS_SENSOR_SHORT:
                bErrorShortCount=0;
                // If current fault status is Sensor Short Fault
                if(FAULT_SENSOR_SHORT == FLT_Fatal_Code)
                {
                    FLAG_CO_FAULT_PENDING = 0;
                }

            break;

            case SUCCESS_GAS_SENSOR:
                // no action
            break;

            case SUCCESS_PHOTOCHAMBER:
                bErrorChamberCount = 0;
                FLAG_SMOKE_ERROR_PENDING = 0;
            break;

            case SUCCESS_SMK_DRIFT_COMP:
                FLAG_FAULT_BEEP_INHIBIT = 0; 
            break;

            #ifdef CONFIGURE_WIRELESS_MODULE

                case AMP_SUCCESS_CCI:
                    fault_CCI_count = 0;
                break;

                case AMP_SUCCESS_COORDINATOR:
                case AMP_SUCCESS_RFD_JOIN:
                case AMP_SUCCESS_RFD_CHECK_IN:
                case AMP_SUCCESS_COORDINATOR_LOST:

                break;

            #endif

            default:
            {
               return (FAULT_UNRECOGNIZED);
            }
        }

       /*
        * For Success Code processing we can clear pending
        * Fault Flags in Fault manager or also let calling function do that
        * since fault manager returns FAULT_RECOVERY_ACTION when applicable.
        */

        #ifdef CONFIGURE_WIRELESS_MODULE
            if((FLAG_NETWORK_ERROR_MODE) && (faultcode & FLT_NET_ERROR_BIT))
            {
                // If in Network Error and Received Success Network Error
                // Mask Success Bit & compare against current Fault
                if( (FLT_NetworkFaultCode == (faultcode & 0x7F))  ||
                    (FLT_NetworkFaultCode == (faultcode & 0x00)) )
                {

                    // Clear Current Network Error condition if a match
                    //  or if Error Code was reset
                    FLT_NetworkFaultCode = faultcode;
                    FLAG_NETWORK_ERROR_MODE=0;
                    
                    // Also End Network Error Hush if case it is active
                    FLAG_NETWORK_ERROR_HUSH_ACTIVE = 0;

                    MAIN_Reset_Task(TASKID_ALED_MANAGER);
                    MAIN_Reset_Task(TASKID_RLED_MANAGER);
                    
                    return (FAULT_RECOVERY_ACTION);
                }
            }
        #endif

        if(FLAG_FAULT_FATAL && !(faultcode & FLT_NET_ERROR_BIT))
        {
            // In Fatal Fault and Received A Success Fatal Error
            // Mask Success Bit and compare against current Fatal Fault
            if(FLT_Fatal_Code == (faultcode & 0x7F))
            {
                // Clear Current Fatal Fault condition if a match
                FLT_Fatal_Code = faultcode;
                FLAG_FAULT_FATAL=0;

                // Test to see if the Smoke ALG needs to be re-enabled
                //   after fault recovery.
                if(!FLAG_ALG_ENABLED)
                {
                  #ifdef CONFIGURE_BUTTON_ALG_ENABLE
                    if(!FLAG_BUTTON_ALG_ENABLED)
                    {
                        // Re-enable the ALG if the button
                        //  did not disable it and there are no Faults
                        FLAG_ALG_ENABLED = 1;
                    }
                  #else
                        FLAG_ALG_ENABLED = 1;
                  #endif
                }

                MAIN_Reset_Task(TASKID_ALED_MANAGER);
                MAIN_Reset_Task(TASKID_RLED_MANAGER);
                
                return (FAULT_RECOVERY_ACTION);
            }
        }

        // Insure no Pending CO Faults if both error counts are cleared
        if((0 == bErrorShortCount) && (0 == bErrorConversionCount))
        {
            FLAG_CO_FAULT_PENDING = 0;
        }

        return (FAULT_SUCCESS_ACTION);
	  
    }
  
    //************************ Fault/Error Received ****************************
    else
    {
        // Determine type of Fault (Alarm or Network Error?)
        if(faultcode & FLT_NET_ERROR_BIT) 
        {
          #ifdef CONFIGURE_WIRELESS_MODULE
            
            // Allow Fault Beeps    
            FLAG_FAULT_BEEP_INHIBIT = 0; 
            
            // A Wireless or Network Error has been Received
            // New Network errors can override any previous network error
            switch (faultcode)
            {
                
            #ifndef CONFIGURE_WIFI
                case AMP_FAULT_WM_BATTERY_EOL:
                {
                    /*
                     * RF Module Fault indicating, Battery is too low for
                     *  wireless operation - Shut Down Wireless features
                     * We should disable all Wireless Operation
                     * when we receive this Fault (Wireless module dead)
                     * 
                     * This Error is non-recoverable
                     */
                    
                    // This Flag is a non-reset Flag (to remain disabled)
                    // Once disabled, no new errors will be received
                    FLAG_WIRELESS_DISABLED = 1;
                    
                    #ifndef CONFIGURE_UNDEFINED
                        // Test Only Remove later
                        SER_Send_String("Wireless Disabled!");
                        SER_Send_Prompt();
                    #endif

                    // Enter Network Error    
                    fault_set_network_error_mode(faultcode);    
                    
                }
                break;
            #endif

                case AMP_FAULT_CCI:
                {
                    fault_CCI_count++;
                    
                    #ifdef CONFIGURE_MONITOR_INT
                        // Test only, monitor app_timer_wireless_supervision
                        MAIN_Monitor_Int2 = fault_CCI_count;
                    #endif
        
                    // RF Module CCI Communication Failed
                    if(FLAG_FAULT_CCI_IMMED)
                    {
                        // In some cases we want to fault immediately
                        //  on CCI fault detection
                        FLAG_FAULT_CCI_IMMED = 0;
                        fault_set_network_error_mode(faultcode);
                    }
                    else if(FAULT_CCI_THRESHOLD <= fault_CCI_count)
                    {
                         fault_set_network_error_mode(faultcode);
                    }
                    else
                    {
                        // allow several attempts before Fault is logged
                        return (FAULT_PENDING_ACTION);
                    }
                }
                break;
                
                
            #ifndef CONFIGURE_WIFI
                case AMP_FAULT_RFD_JOIN:
                     // RFD failed to Join (Network Not Found Condition)
                    
                    /*
                     * If previously enrolled in a network, enter Net Error 
                     * 
                     * AMP_FAULT_RFD_JOIN is only passed here if previously
                     *  enrolled.
                     */

                    fault_set_network_error_mode(faultcode);
                    
                break;
            #endif
                
            #ifndef CONFIGURE_WIFI
                case AMP_FAULT_TIME_SYNC:
                    /*
                     * RFD failed to see a time sync. from Coordinator
                     * This should start a re-join attempt after multiple 
                     *  attempts on not hearing Coordinator 
                     * 
                     * After unsuccessful re-join RFD Join fault is received
                     *  So this fault is currently never received
                     */
                    
                    // If AMP_FAULT_TIME_SYNC does get received enter Network 
                    //  Error
                    fault_set_network_error_mode(faultcode);
                    
                break;
            #endif
                
            #ifndef CONFIGURE_WIFI
                case AMP_FAULT_COORDINATOR:
                    // Coordinator failed to see ANY enrolled RFDs
                    
                    // Enter Network Error
                    fault_set_network_error_mode(faultcode);

                break;
            #endif
                
            #ifndef CONFIGURE_WIFI
                case AMP_FAULT_RFD_CHECK_IN:
                {
                    // Coordinator failed to see ONE of the enrolled RFDs
                    if(FLAG_COMM_COORDINATOR)
                    {
                        /*
                         * Coordinator Fault when RFDs fail to check-in
                         * AMP_FAULT_RFD_CHECK_IN will only log a fault here
                         * if less than 3 Devices are in the network
                         * 
                         * So by design we do not want the Coordinator
                         *  entering Network Error Mode unless there
                         *  is only 1 or 2 devices in the Network
                         */
                        fault_set_network_error_mode(faultcode);
                        
                    }
                    else
                    {
                        // This fault is supposedly never sent to RFDs
                        // However, in case it is Received - enter Network 
                        // Error
                        fault_set_network_error_mode(faultcode);
                    }
                }
                break;
            #endif
                
                default:
                {
                    return (FAULT_UNRECOGNIZED);
                }
                
            }   // End of Network Error Processing
            
          #endif
        }
        else
        {
            // Alarm Error Received (Fatal Faults)
            if(FLAG_FAULT_FATAL)
            {
                // No New Action - Already in Fatal Fault Mode
                // Only Log and Process the 1st Fatal Fault received so 
                // error code always matches the inital fault  

            }
            else
            {
                // If we reached this point, then continue and 
                //  process the received Alarm Fault
                
                // Allow Fault Beeps    
                FLAG_FAULT_BEEP_INHIBIT = 0; 
        
                // Process Received Alarm Fault Code
                switch (faultcode)
                {
                
                    case FAULT_CO_HEALTH:

                        if( FAULT_CONVERSION_THRESHOLD == bErrorConversionCount)
                        {
                            #ifdef CONFIGURE_DIAG_HIST
                                DIAG_Hist_Que_Push(HIST_SENS_HEALTH_FAULT);
                            #endif

                            fault_set_fatal_mode(faultcode);
                        }
                        else
                        {
                            FLAG_CO_FAULT_PENDING = 1;
                            // Turn off the Smoke Alg on Faults
                            FLAG_ALG_ENABLED = 0;

                            bErrorConversionCount++;
                            return (FAULT_PENDING_ACTION);
                        }
                    break;

                    case FAULT_SENSOR_SHORT:

                        if(FAULT_SENSOR_SHORT_THRESHOLD == bErrorShortCount)
                        {
                            #ifdef CONFIGURE_DIAG_HIST
                                DIAG_Hist_Que_Push(HIST_SENS_SHORT_FAULT);
                            #endif

                            fault_set_fatal_mode(faultcode);

                        }
                        else
                        {
                            FLAG_CO_FAULT_PENDING = 1;
                            // Turn off the Smoke Alg on Faults
                            FLAG_ALG_ENABLED = 0;

                            bErrorShortCount++;
                            return (FAULT_PENDING_ACTION);

                        }

                    break;

                    case FAULT_PHOTOCHAMBER:

                        if(FAULT_SMOKE_CHAMBER_THRESHOLD == bErrorChamberCount)
                        {
                            #ifdef CONFIGURE_DIAG_HIST
                                DIAG_Hist_Que_Push(HIST_PHOTOCHAMBER_FAULT);
                            #endif

                            fault_set_fatal_mode(faultcode);

                        }
                        else
                        {
                            FLAG_SMOKE_ERROR_PENDING = 1;
                            // Turn off the Smoke Alg on Faults
                            FLAG_ALG_ENABLED = 0;

                            bErrorChamberCount++;
                            return (FAULT_PENDING_ACTION);

                        }

                    break;

                    #ifdef CONFIGURE_THERMISTOR_TEST

                        case FAULT_TEMPERATURE:
                            fault_set_fatal_mode(faultcode);
                        break;

                    #endif

                    case FAULT_SMK_DRIFT_COMP:

                        #ifdef CONFIGURE_DIAG_HIST
                            DIAG_Hist_Que_Push(HIST_DRIFT_COMP_FAULT);
                        #endif

                        // Turn off the Smoke Alg on Faults
                        FLAG_ALG_ENABLED = 0;

                        fault_set_fatal_mode(faultcode);

                    break;

                    case FAULT_GAS_SENSOR:
                        // Not Used
                    break;

                    case FAULT_CALIBRATION:

                        #ifdef CONFIGURE_DIAG_HIST
                            #ifdef CONFIGURE_UNDEFINED
                                DIAG_Hist_Que_Push(HIST_SENS_CALIB_FAULT);
                            #endif
                        #endif

                        fault_set_fatal_mode(faultcode);

                    break;

                    case FAULT_PTT:

                        #ifdef CONFIGURE_DIAG_HIST
                            DIAG_Hist_Que_Push(HIST_PTT_FAULT);
                        #endif

                        fault_set_fatal_mode(faultcode);

                    break;

                    case FAULT_INT_SUPERVISION:

                        #ifdef CONFIGURE_DIAG_HIST
                            DIAG_Hist_Que_Push(HIST_INT_SUPER_FAULT);
                        #endif

                        fault_set_fatal_mode(faultcode);

                    break;

                  case FAULT_MEMORY:

                        #ifdef CONFIGURE_DIAG_HIST
                            DIAG_Hist_Que_Push(HIST_MEMORY_FAULT);
                        #endif

                        fault_set_fatal_mode(faultcode);

                    break;

                    case FAULT_EXPIRATION:

                        #ifdef CONFIGURE_DIAG_HIST
                            DIAG_Hist_Que_Push(HIST_LIFE_EXP);
                        #endif

                        FLAG_EOL_FATAL = 1;

                        fault_set_fatal_mode(faultcode);

                    break;
                    
                    default:
                    {
                        return (FAULT_UNRECOGNIZED);
                    }
                    
                }
                
            }
                
        } // End of Alarm Error Processing
        
        
    }   // End of  Alarm Fault/Network Error Processing
    
    // Determine Return Response
    if(FLAG_FAULT_FATAL)
    {
        // In Fatal Fault (highest priority))
        return (FAULT_FATAL_ACTION);
    }
    else if(FLAG_NETWORK_ERROR_MODE)
    {
        // In Network Error 
        return (FAULT_NETWORK_ACTION);
    }
    else
    {
        // No Fault or Faults ignored
        return (FAULT_NO_ACTION_TAKEN);
    }
        
}
        



#ifdef CONFIGURE_WIRELESS_MODULE
/*
+------------------------------------------------------------------------------
| Function:      FLT_Init_Error_Hush
+------------------------------------------------------------------------------
| Purpose:       Start the Error 24 Hour Hush Timer
|
|
|                Timer is to inhibit Network Error Chirps for 24 hours at 
|                a time.
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void FLT_Init_Error_Hush(void)
{
    #ifdef CONFIGURE_ACCEL_TIMERS
        // Dummy instruction to remove compile unused warning 
        //  during test config.
        fault_err_hush_timer = MAIN_OneHourTic;
        
        
        // Initialize 24 Hour Timer with accelerated time
        fault_err_hush_timer = MAIN_OneMinuteTic;
        FLAG_NETWORK_ERROR_HUSH_ACTIVE = 1;
        
        #ifdef CONFIGURE_TIMER_SERIAL
            SER_Send_String("Start Err Hush 24Hr- ");
            SER_Send_Prompt();
        #endif        
    #else
        // Initialize 24 Hour Timer
        fault_err_hush_timer = MAIN_OneHourTic;
        FLAG_NETWORK_ERROR_HUSH_ACTIVE = 1;
    #endif

}



/*
+------------------------------------------------------------------------------
| Function:      FLT_Check_Error_Hush
+------------------------------------------------------------------------------
| Purpose:       Monitor the Error 24 Hour Hush Timer
|                It is based upon 1 Minute TICs so call every minute
|
|                End Error Hush at Timer Expiration
|                Called from Main every Minute
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|
+------------------------------------------------------------------------------
*/

void FLT_Check_Error_Hush(void)
{
    if(FLAG_NETWORK_ERROR_HUSH_ACTIVE)
    {
        #ifdef CONFIGURE_ACCEL_TIMERS
            // Check here to see if Network Err hush mode should be canceled.
            // Subtract the stored timer from the current minute to see if 
            // enough time has expired.
            if( (uchar)(MAIN_OneMinuteTic - fault_err_hush_timer) >= 
                 (uchar)FLT_ERR_HUSH_3_MINUTES)
            {

                #ifdef CONFIGURE_TIMER_SERIAL 
                    SER_Send_String("ERR Hush 24 hr Expired- ");
                    SER_Send_Prompt();
                #endif

                // If Error Hush has ended, send Network Error reset
                //  to RF Module for another recovery attempt.
                APP_Reset_Fault();
                
                // Reset task for Faster Response to Error Reset
                MAIN_Reset_Task(TASKID_TROUBLE_MANAGER);
                    
            }
            
            
        #else        
            // Check here to see if Network Err hush mode should be canceled.
            // Subtract the stored timer from the current minute to see if 
            // enough time has expired.
            if( (uchar)(MAIN_OneHourTic - fault_err_hush_timer) >= 
                 (uchar)FLT_ERR_HUSH_24_HOURS)
            {
                // If 24 Hour Hush has ended, send Network Error reset
                //  to RF Module for another recovery attempt.
                APP_Reset_Fault();
            
                // Reset task for Faster Response to Error Reset
                MAIN_Reset_Task(TASKID_TROUBLE_MANAGER);
            }
            
            
        #endif
    }
}


/*
+------------------------------------------------------------------------------
| Function:      fault_set_network_error_mode
+------------------------------------------------------------------------------
| Purpose:       Sets Network Error Mode
|                Sets current network error code until cleared 
|
+------------------------------------------------------------------------------
| Parameters:    fault - fault code to maintain in FLT_NetworkFaultCode
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void fault_set_network_error_mode(uchar fault)
{
    
    #ifdef CONFIGURE_DIAG_HIST
        if(!FLAG_NETWORK_ERROR_MODE)
        {
            DIAG_Hist_Que_Push(HIST_NETWORK_ERROR);
        }
    #endif

    #ifndef CONFIGURE_DUCT_TEST
        // Save Fault Code (used for Fault recovery)
        //  and enter Fault Mode
        FLT_NetworkFaultCode = fault;
        FLAG_NETWORK_ERROR_MODE=1;
    #endif
}


/*
+------------------------------------------------------------------------------
| Function:      FLT_Net_Error_Status
+------------------------------------------------------------------------------
| Purpose:       Return Network Error Status Code
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  FLT_NetworkFaultCode (network error code status)
+------------------------------------------------------------------------------
*/

uchar FLT_Net_Error_Status(void)
{
    return FLT_NetworkFaultCode;
}


#endif

/*
+------------------------------------------------------------------------------
| Function:      fault_set_fatal_mode
+------------------------------------------------------------------------------
| Purpose:       Sets Fatal Fault Mode
|                Sets current fault code until cleared 
|
+------------------------------------------------------------------------------
| Parameters:    fault - faultcode to maintain in FLT_Fatal_Code
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void fault_set_fatal_mode(uchar fault)
{
#ifndef CONFIGURE_DUCT_TEST
    // Save Fault Code (used for Fault recovery)
    //  and enter Fault Mode
    FLT_Fatal_Code = fault;
    FLAG_FAULT_FATAL=1;
#endif
}



/*
+------------------------------------------------------------------------------
| Function:      FLT_Status
+------------------------------------------------------------------------------
| Purpose:       Return Fault Status Code
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  FLT_Fatal_Code (Fault status)
+------------------------------------------------------------------------------
*/

uchar FLT_Status(void)
{
    return FLT_Fatal_Code;
}
