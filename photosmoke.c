/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        photosmoke.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Routines for Photo Smoke sensing support
|               
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/



/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include    "common.h"
#include    "photosmoke.h"
#include    "systemdata.h"
#include    "sound.h"
#include    "a2d.h"
#include    "serial.h"
#include    "algorithm.h"
#include    "fault.h"
#include    "voice.h"
#include    "led.h"
#include    "comeasure.h"
#include    "cocalibrate.h"
#include    "alarmprio.h"
#include    "command_proc.h"
#include    "app.h"


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

// ****************************************************************************
// Debug  Section, Test Only Defines (un-comment as needed)

// #define DBUG_SERIAL_OUTPUT_SMK_AVG
// #define DBUG_SERIAL_OUTPUT_FILTER_SAMPLES

// #define CONFIGURE_PHOTO_OFFSET_MEASURE
// #define CONFIGURE_SLUMP_PERCENT

// ****************************************************************************


#define CONFIGURE_SLOPEOFFSET_CAL
#define CONFIGURE_HALF_OHM_CURRENT_SENSE


#define SMK_RETURN_100_MS   (uint)100         // 100 ms Task Return Time
#define SMK_SAMPLE_1_SEC    (uint)1000        // 1 Second Sampling rate
#define SMK_SAMPLE_3_SEC    (uint)3000        // 3 Second Sampling rate
#define SMK_SAMPLE_5_SEC    (uint)5000        // 5 Second Sampling rate
#define SMK_SAMPLE_10_SEC   (uint)10000       // 10 Second Sampling rate
#define SMK_SAMPLE_11_SEC   (uint)11000       // 11 Second return time
#define SMK_SAMPLE_15_SEC   (uint)15000       // 15 Second Sampling rate

#define TIMER_240_SEC       (uchar)240        // Fast Sample Time for CO Detect

#define ALM_RESCHEDULE_TIME_500_MS  (uint)500
#define ALM_RESCHEDULE_TIME_100_MS  (uint)100


#define SMK_OUTPUT_RAILED           (uchar)255
#define SMK_OUTPUT_ALMOST_RAILED    (uchar)254

#define MAX_IRED_DRIVE              (uchar)255

#ifdef  CONFIGURE_AC_TRANSIENT_FILTERING_SMK
    #define PHOTO_NUM_SAMPLES   (uchar)6
#endif

#define PHOTO_SMK_CAL_4_SAMPLES (uchar)4

#ifdef  CONFIGURE_AC_TRANSIENT_FILTERING_TEMP
    #define TEMP_NUM_SAMPLES    (uchar)6
#endif

// For Thermistor Test (Old test))
#define THERMISTOR_FAULT_HIGH       (uint)830
#define THERMISTOR_FAULT_LOW        (uint)630

// For New Thermistor Test
//#define THERMISTOR_FAULT_HIGH       (uint)920
//#define THERMISTOR_FAULT_LOW        (uint)720


#define OP_AMP_PWR_STABLE_TIME  (unsigned long)650      //650 uSecs


#define	SMK_HUSH_TIMEOUT_MINS       9   // minutes (9 Max for Canada)
// For standard 10 second smoke sampling
#define	SMK_HUSH_TIME               (uchar)(SMK_HUSH_TIMEOUT_MINS * 6)

#ifdef CONFIGURE_WIRELESS_ALARM_LOCATE
    #define SMK_HUSH_TIME_1_MINUTE  (uchar)6    // in 10 second units
    #define SMK_HUSH_TIME_50_SECONDS (uchar)5   // in 10 second units    
#endif

/* Define delay after IRed Drive Pulse before making a Photo measurement
 * Measured required delay time using a scope triggered on Current Pulse
 */
#define	SMK_TIME_ELEC_WARMUP_5_USECS    5	// 5 microseconds
#define	SMK_TIME_ELEC_WARMUP_60_USECS   60	// 60 microseconds

// Starting points, LED drive for Cal.
#define	SMK_IRLED_DRIVE_START_HIGH  (uchar)30  
#define	SMK_IRLED_DRIVE_START_LOW   (uchar)30  

#define	SMK_HIGH_DRIVE_MA           (uint)500
#define	SMK_LOW_DRIVE_MA            (uint)300
#define	SMK_LED_CAL_DRIVE_DELTA     (uint)(SMK_HIGH_DRIVE_MA - SMK_LOW_DRIVE_MA)


//*************************************************************************
//****************** Calibration Formula Definitions **********************
//                           (model dependant)
//*************************************************************************

// Macro to calculate fixed point 8_8.
// A is the whole part of fractional number 0-255 
// B is fractional part, MUST have 2 decimal places  
// Example: 2.45 => FIXED8_8CONST(2,45) = 0x0273
// Example: 18.6 => FIXED8_8CONST(18,60) = 0x1299
#define FIXED8_8CONST(A,B)  ( (A * 256) + ((B * 256)/100) )

/*
 *  Calibration constants will need to be redefined for
 *    the new Photo Chambers and New Models.
*/

#ifdef	CONFIGURE_SLOPEOFFSET_CAL
//*****************************************************************************
//**************************** Photo/CO ***************************************
//*****************************************************************************
/*               *** Smoke Alarm Calibration Equation ***
 *
 * Slope = (measured output voltage delta / current drive delta)
 *       = (bHighCalVout-bLowCalVout) /(photo_drive_high - photo_drive_low)
 *                                     (      500ma     -       300ma   )
 *
 * VCav = average measured chamber output at Low Drive (currently =300 ma)
 *
 * Valarm = SMK_ALARM_OFFSET_8_8  + (SMK_ALARM_OFFSET_SCALE_8_8 * VCav)
 *                                + (SMK_ALARM_SENS_SCALE_8_8   * Slope)
 */



//************************************************************************
// All Configurations for Now
/* These Calibration Coefs. need to be adjusted for the new enclosure
 *  bases, sweep data was with the old bases which affect Target
 *  sensitivities by as much as 6-7 uAmps
 */


#ifdef CONFIGURE_SMK_CC_TEST

// **************************************************************************
//          Equations for Conductive Chamber Sensitivity
// **************************************************************************
    //
    // CPE Chamber has been previously approved by UL
    // This change is to center the new UL window for CPE in M_1_5 models
    //
    // 12/02/2020   CPE Chamber Equation from Jack Wang
    // Target 92uA = 8.74 + 1.062 CAV + 81.5 Chamber Slope
    //
    // SMK_ALARM_OFFSET_8_8 = 8.74
    // SMK_ALARM_OFFSET_SCALE_8_8 = 1.062
    // SMK_ALARM_SENS_SCALE_8_8 = +81.5
    //

#ifndef CONFIGURE_UNDEFINED
	#define	SMK_ALARM_OFFSET_8_8			FIXED8_8CONST(8,74)     // 8.74
	#define	SMK_ALARM_OFFSET_SCALE_8_8		FIXED8_8CONST(1,06)     // 1.06

	#define	SMK_ALARM_SENS_SCALE_8_8_SIGN	POSITIVE	
	#define	SMK_ALARM_SENS_SCALE_8_8		FIXED8_8CONST(81,50)	// 81.50
#endif

//******************************** Obsolete ***********************************
    //
    // 03/07/2018  X1 Equation
    // 89.5 = 46.8 + 1.096 CAV Serial + 21 Chamber Slope
    //
    // SMK_ALARM_OFFSET_8_8 = 46.8
    // SMK_ALARM_OFFSET_SCALE_8_8 = 1.096
    // SMK_ALARM_SENS_SCALE_8_8 = +21
    //

#ifdef CONFIGURE_UNDEFINED
	#define	SMK_ALARM_OFFSET_8_8			FIXED8_8CONST(46,80)    // 46.80
	#define	SMK_ALARM_OFFSET_SCALE_8_8		FIXED8_8CONST(1,10)     // 1.10

	#define	SMK_ALARM_SENS_SCALE_8_8_SIGN	POSITIVE	
	#define	SMK_ALARM_SENS_SCALE_8_8		FIXED8_8CONST(21,00)	// 21.00
#endif


    //
    // 05/08/2018   X2 Equation (Created without Smoke Box Cal. Sweeps))
    // 92.5 = 29.96 + 1.141 CAV Serial - 13.3 Chamber Slope
    //
    // SMK_ALARM_OFFSET_8_8 = 29.96
    // SMK_ALARM_OFFSET_SCALE_8_8 = 1.141
    // SMK_ALARM_SENS_SCALE_8_8 = -13.3
    //

#ifdef CONFIGURE_UNDEFINED
	#define	SMK_ALARM_OFFSET_8_8			FIXED8_8CONST(29,96)    // 29.96
	#define	SMK_ALARM_OFFSET_SCALE_8_8		FIXED8_8CONST(1,14)     // 1.14

	#define	SMK_ALARM_SENS_SCALE_8_8_SIGN	NEGATIVE	
	#define	SMK_ALARM_SENS_SCALE_8_8		FIXED8_8CONST(13,30)	// 13.30
#endif

#else

// **************************************************************************
//          Equations for Non-Conductive Chamber Sensitivity
// **************************************************************************

    // Reverted back to Old DC Worry Free Equation (Kitchen) CO/Smoke Combo
	// 89 uA Target 4/3/2013
    // 89uA = 7.51 + (1.34 * CAV) + (45.4 * slope)
#ifndef CONFIGURE_UNDEFINED
	#define	SMK_ALARM_OFFSET_8_8			FIXED8_8CONST(7,51)     // 7.51
	#define	SMK_ALARM_OFFSET_SCALE_8_8		FIXED8_8CONST(1,34)     // 1.34

	#define	SMK_ALARM_SENS_SCALE_8_8_SIGN	POSITIVE	
	#define	SMK_ALARM_SENS_SCALE_8_8		FIXED8_8CONST(45,40)	// 45.4
#endif

#endif  // #ifdef CONFIGURE_SMK_CC_TEST

    // Common Photo Smoke defines 

    //
    //  These values carry over from DC Worry-Free Models
    //
    //  Used for IRed Degradation COMP (increasing sensitivity)
    //#define   SMK_ALARM_COMP_SCALE_8_8    FIXED8_8CONST(1,70)   	// 1.7

    #define	SMK_ALARM_COMP_SCALE_8_8        0x01B3              	// 1.7

    //  Used for Drift (Dust) COMP (decreasing sensitivity)
    #define	SMK_ALARM_DRIFT_COMP_SCALE_8_8	0x01B3              	// 1.7



    /*************************************************************************
     * FAULT THRESHOLDs - Based upon FHK pilot data AND
     * leaving Room for Slump, Hush and Dust Drift compensation 
     * Clean Air values near or above 100 are high risk to enter
     *  drift compensation faults as there is little room to compensate
     *  for dust. See SMK_CAV_CAL_FAULT_HI value.
     */
    #define	SMK_FAULT_THRESHOLD         (uchar)20  // A/D counts

#ifdef CONFIGURE_DUCT_TEST
    #define	SMK_CAV_CAL_FAULT_LO        (uchar)35  // Lo Limit during Smoke Cal
    #define	SMK_CAV_CAL_FAULT_HI        (uchar)150 //  Hi Limit during Smoke Cal
#else
    #define	SMK_CAV_CAL_FAULT_LO        (uchar)35  // Lo Limit during Smoke Cal
    #define	SMK_CAV_CAL_FAULT_HI        (uchar)100 //  Hi Limit during Smoke Cal
#endif

#ifdef CONFIGURE_DUCT_TEST
    #define SMK_CAL_FAULT_THOLD_LOW     (uchar)60  // A/D counts
    #define	SMK_CAL_FAULT_THOLD_HIGH    (uchar)220 // A/D counts
#else
    #define SMK_CAL_FAULT_THOLD_LOW     (uchar)60  // A/D counts
    #define	SMK_CAL_FAULT_THOLD_HIGH    (uchar)180 // A/D counts
#endif

    // Set a Smoke Low Sensitivity Limit (based upon worse case 6 counts/uAmp)
    // For this model this is about 3.65 %/ft
    #define SMK_CAL_FAULT_SENSITIVITY   (uchar)106 // A/D Counts

    // PTT Target A/D counts above Alarm Threshold
    #define SMK_PTT_TARGET              (uchar)14  // ~10% below typical AT

#endif // CONFIGURE_SLOPEOFFSET_CAL




//*****************************************************************************
// The feedback resistor is SMK_R_FEEDBACK_OHMS ohms.  
//  The equation to calculate counts is:
//
//	Cfb = ((SMK_R_FEEDBACK_OHMS * SMK_HIGH_DRIVE_MA) * 256)/(2.5 * 1000)
//
#ifdef	CONFIGURE_HALF_OHM_CURRENT_SENSE
   #define SMK_R_FEEDBACK_MOHMS    500
#else
   #define SMK_R_FEEDBACK_MOHMS    1000
#endif

#define SMK_HIGH_DRIVE_CTS  (uchar)26
#define SMK_LOW_DRIVE_CTS   (uchar)15

/*
 * #define SMK_HIGH_DRIVE_CTS  ((( (( SMK_R_FEEDBACK_MOHMS * 
 *                      SMK_HIGH_DRIVE_MA)/1000) * 256)/ 2500) )
 * #define SMK_LOW_DRIVE_CTS   ((( (( SMK_R_FEEDBACK_MOHMS * 
 *                      SMK_LOW_DRIVE_MA)/1000) * 256)/ 2500) )
 */

//*****************************************************************************

#ifdef CONFIGURE_CO
    // This is 1 second smoke sampling
    #define	SMK_ALARM_COUNTS    (uchar)2
    #define	SMK_NO_ALARM_COUNTS (uchar)2

    //#define	SMK_NO_ALARM_COUNTS (uchar)10
#else
    // This is 10 second smoke sampling
    #define	SMK_ALARM_COUNTS    (uchar)2
    #define	SMK_NO_ALARM_COUNTS (uchar)2
#endif

#define	SMK_NUM_CAL_SAMPLES 10
#define	SMK_NUM_CAL_PASSES  (uchar)(SMK_NUM_CAL_SAMPLES * 2)

#define	SMK_ALARM_HYSTERESIS_CNTS   -5

// Smoke Sample Type
#define IRLED_ON        1

#ifdef CONFIGURE_PHOTO_OFFSET_MEASURE
    #define IRLED_OFF       0
#endif

#ifdef CONFIGURE_PHOTO_OFFSET_MEASURE
    // Limit Dark Offset  (value = TBD)
    #define PHOTO_DARK_LIMIT (uchar)20
#endif

#ifdef CONFIGURE_SMK_SLOPE_SIMULATION
    // Counts above CAV to begin simulation
    #define SLOPE_SIM_COUNT_THRESHOLD   (uchar)10
    #define SLOPE_SIM_ROUND_UP          (uchar)127
#endif

//
// Since the Hush timer will never be used in calibration mode or other smoke 
// states, alias it with photo_cal_counter and Algorithm Timers.
//
#define	photo_cal_counter   hush_timeout_timer
#define	photo_alg_timer     hush_timeout_timer

// Only used during the Smoke Calibration process
#define photo_smoke_voltage  PHOTO_Alarm_Thold

// define an Absolute Maximum alarm Threshold Limit
#define SMK_ALARM_ABS_MAX_LIMIT (uchar)251


// Limit Hourly CAV Average change
#define CAV_INCREASE_LIMIT  (uchar)4
#define CAV_DECREASE_LIMIT  (uchar)4

#define SMK_CHAMBER_SHIFT_TH (uchar)20		//20 A/D counts


#define SMK_CAV_DELTA_MAX (uchar)130

#define SMK_COMP_SLOPE_1P2  (uchar)0x33
#define SMK_COMP_SLOPE_1P4  (uchar)0x66
#define SMK_COMP_SLOPE_1P6  (uchar)0x9A
#define SMK_COMP_SLOPE_1P8  (uchar)0xCD
#define SMK_COMP_SLOPE_1P95  (uchar)0xF5
#define SMK_COMP_SLOPE_1P98  (uchar)0xFA



/*
|-----------------------------------------------------------------------------
| Typedef and Enums and Constatnts
|-----------------------------------------------------------------------------
*/


enum
{
    NEGATIVE = 0,
    POSITIVE
};


#ifdef CONFIGURE_UNDEFINED
    // This table was used by DC Worryfree Models
    //  for UL Testing when we set High/Low Sensitivity units artificially
    // It should no Longer be needed

    // Table used to select CAV multiplier when adjusting alarm Threshold
    //  during auto drift compensation. index = 
    const unsigned char SMK_X_TABLE[8]=
    {
        0x00,           //0-15          1.0
        0x33,           //16-31 		1.2
        0x9A,           //32-47 		1.6
        0x9A,           //48-63 		1.6
        0xB3,           //64-79 		1.7
        0xCD,           //80-95         1.8
        0xCD,           //96-111	 	1.8
        0xCD            //112-127       1.8
    };
    
#else
    // CAV to A.T. Multiplier from Collected Smoke Cal Data
    #define SMK_MULT_1_6    (uchar)0x9A
#endif

/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/
uchar PHOTO_Alarm_Thold;
uchar PHOTO_Reading = 0;
uchar PHOTO_Cav_Timer = 0;

uchar PHOTO_Comp_Max_Cav = 0;
uchar PHOTO_Comp_Min_Cav = 0;

persistent avg_t PHOTO_Cav_Average;

#ifdef CONFIGURE_MANUAL_SMK_OUT
    uchar PHOTO_Man_Reading = 0;
#endif

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
uchar photo_ired_voltage = 0;



#ifdef CONFIGURE_SMK_SLOPE_SIMULATION
    // This is actual saved S: value
    uchar photo_reading_actual = 0;
#endif

// Temp
avg_t photo_temp_avg = 0;



#ifdef CONFIGURE_PHOTO_OFFSET_MEASURE
    uchar photo_reading_offset = 0;
#endif


#ifdef CONFIGURE_MIN_MAX_OUT
    static uchar photo_min = 255;
    static uchar photo_max = 0;
#endif

static uchar photo_state = SMK_PHOTO_STARTUP_DELAY;
static uchar ired_pulse_width;
static uchar photo_drive_high;
static uchar photo_drive_low;
static uchar smoke_update_count;
static uchar hush_timeout_timer;
static uchar photo_ptt_target_th;

// Init with default Drift Comp slope factor
static uchar smk_drift_comp_factor = SMK_COMP_SLOPE_1P6;

static uint temp1 = 0;    // temporary variable

static avg_t high_cal_vout;
static avg_t low_cal_vout;

 
#ifdef CONFIGURE_HUSH_LOCATE_REFRESH
    static uchar hush_minute_count;
#endif

#ifdef CONFIGURE_TEMP_DEG_F
    static uchar photo_temp_deg_f;
#endif
    
    
/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/
uint photo_set_sample_rate(void);
uchar photo_measure_smk(uchar*);
uchar photo_smk_sample(uchar type);
void photo_temp_measure(void);
void photo_temp_reading_avg(void);
void photo_send_irled_data(void);
void photo_send_cal_info(void);
void photo_send_smk_data(void);
uchar photo_find_drive(void);

uchar photo_dust_shift_detect(void);
uchar photo_comp_alm_compute_corr(uchar CavDelta);
uchar photo_compensate_alm_chk_limit(uchar AlmDelta);
void photo_compensate_alm_serial_out(void);
uchar photo_get_x(void);

void photo_temp_ror_test(void);
void photo_smk_ror_test(void);
void photo_co_ror_test(void);
void photo_co_output_test(void);
void photo_smk_output_test(void);
void photo_alg_update_inputs(void);
uchar photo_smk_chamber_test(void);

void photo_init_smk_state(uchar smk_state);

uchar photo_alg_standby_state_test(void);
uchar photo_alg_alarm_state_test(void);
uchar photo_alg_jump_state_test(void);
uchar photo_alg_slump1_state_test(void);
uchar photo_alg_slump2_state_test(void);

void photo_init_minute_timer(void);
void photo_init_second_timer(void);
uchar photo_get_alg_min_et(void);
uchar photo_get_alg_sec_et(void);

void smk_set_cal_corr_slope(void);
void smk_set_cav_corr_limits(void);

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/


/*
+------------------------------------------------------------------------------
| Function:     PHOTO_Photo_Init
+------------------------------------------------------------------------------
| Purpose:      Test the Smoke Cal to see if Smoke Cal is completed on this 
|               unit.
|               If yes - Initialize the IRLED drive and Alarm Threshold 
|                        variables
|               If no - Set the Smoke Not Calibrated Flag for Auto Cal states
|   
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void PHOTO_Photo_Init(void)
{

    // Get the Alarm Threshold
    PHOTO_Alarm_Thold = SYS_RamData.Smk_Corrected_Alm_TH;

    // Get the drive level and set PW.
    // If unit is uncalibrated, set the No Cal Flag
    if( (SYS_RamData.Smk_IRled_Drive_Level == 0) ||
         (SYS_RamData.Smk_IRled_PTT_Drive == 0) )
    {
        // Unit is Uncalibrated
        FLAG_NO_SMOKE_CAL = 1;
    }
    else
    {
        // Unit is Calibrated
        PHOTO_Set_Drive_Pulse_Width(SYS_RamData.Smk_IRled_Drive_Level);
        
        // Set this alarm's CAV limits based upon calibration results
        smk_set_cav_corr_limits();
        
    }

}




/*
+------------------------------------------------------------------------------
| Function:     PHOTO_Process_Task
+------------------------------------------------------------------------------
| Purpose:      Monitors Photo Smoke Chamber and controls Smoke Chamber
|               Calibration Process and Smoke Alarm algorithm based upon 
|               Smoke, CO and Temperature Sensor analysis.
|                        
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Photo Process Task Next Execution Time (in Task TICs)
+------------------------------------------------------------------------------
*/

uint PHOTO_Process_Task(void)
{
  #ifdef CONFIGURE_DEBUG_SMOKE_STATES
    // Test Only
    for(uchar i = (photo_state + 1); i > 0; i-- )
    {
        // Test Only
        MAIN_TP2_On();
        PIC_delay_us(DBUG_DLY_25_USec);
        MAIN_TP2_Off();
    }
  #endif

    switch (photo_state)
    {
        case SMK_PHOTO_STARTUP_DELAY:
        {
            FLAG_SMK_STABILIZATION = 1;
            
            if(FLAG_POWERUP_DELAY_ACTIVE)
            {
                // Wait for Power Up stuff to complete
            }
            else
            {
                // After Power Up delay complete
                photo_state = SMK_PHOTO_IDLE;
                
                if(FLAG_NO_SMOKE_CAL)
                {
                    return MAIN_Make_Return_Value(SMK_SAMPLE_1_SEC);
                }
                else
                {
                    // Stabilization Time after a Reset
                    return MAIN_Make_Return_Value(SMK_SAMPLE_1_SEC);
                }
            }
            return MAIN_Make_Return_Value(SMK_RETURN_100_MS);
        }

        case SMK_PHOTO_IDLE:
        {
            // Clear Stabilization Flag
            FLAG_SMK_STABILIZATION = 0;

            if(FLAG_LB_CAL_STATUS)
            {
                // Wait for LB Calibration to complete before Smoke Calibration
                if(FLAG_NO_SMOKE_CAL)
                {
                    if(FLAG_FAULT_FATAL)
                    {
                        
                    #ifdef CONFIGURE_THERMISTOR_TEST
                        //
                        //If Fatal Fault is Thermistor Fault, keep Red Led On
                        //
                        if (FAULT_TEMPERATURE == FLT_Fatal_Code)
                        {
                            //
                            // There was a fault in the photo algorithm 
                            // thermistor circuit, this is not the Heat 
                            // Alarm Thermistor (FHK requested this test)
                            //
                            // Keep Red Led On
                            //
                            RLED_On();
                        }
                    #endif
                        
                        // No smoke Cal if in Fault (most likely memory fault)
                        return MAIN_Make_Return_Value(SMK_SAMPLE_1_SEC);
                    }

                    #ifdef CONFIGURE_THERMISTOR_TEST
                        // Check for missing or open/shorted Thermistor
                        //  before Smoke Calibration
                        LAT_PHOTO_PWR_PIN = 1;

                        // OpAmp Power On Stabilization Time
                        PIC_delay_us(OP_AMP_PWR_STABLE_TIME);     // Delay usecs

                        // Take the Temp Measurement
                        ALG_ADTemp = A2D_Temp();

                        // Power down electronics
                        LAT_PHOTO_PWR_PIN = 0;

                        if( (ALG_ADTemp > THERMISTOR_FAULT_HIGH) ||
                            (ALG_ADTemp < THERMISTOR_FAULT_LOW))
                        {
                            // Log Temperature Fault
                            FLT_Fault_Manager(FAULT_TEMPERATURE);
                            //
                            //Fatal Fault has been set
                            // Remain in this state
                            //
                            return MAIN_Make_Return_Value(SMK_SAMPLE_1_SEC);
                        }
                    #endif

                    /*
                     * The unit is uncalibrated.  Initialize for calibration.
                     * The high drive is found first.
                     */

                    // Initialize the variable that will be doing the counting 
                    // for sample counting during calibration.
                    smoke_update_count = PHOTO_SMK_CAL_4_SAMPLES;

                    // Initialize for Low Drive
                    FLAG_FIND_LOW_DRIVE_ACTIVE = 1;

                    // Clear Temp variable for serial output
                    PHOTO_Alarm_Thold = 0;

                    high_cal_vout.byte.msb = SMK_IRLED_DRIVE_START_LOW;
                    PHOTO_Set_Drive_Pulse_Width(SMK_IRLED_DRIVE_START_LOW);

                    // Transition to the state that will find PWM setting that
                    // corresponds to the high drive current.
                    photo_state = SMK_SET_LOW_DRIVE;

                }
                else
                {
                    // Proceed to Smoke Standby
                    photo_init_smk_state(SMK_STANDBY);
                }
            }
            return MAIN_Make_Return_Value(SMK_SAMPLE_1_SEC);
        }
        
        case SMK_SET_HIGH_DRIVE:
        {
            if(photo_find_drive())
            {
                //Correct level has been found.
                // Init variables for theCalibration process
                // Start with High drive
                PHOTO_Set_Drive_Pulse_Width(photo_drive_high);
                FLAG_PHOTO_HIGH_CAL = 1;

                photo_cal_counter = SMK_NUM_CAL_PASSES;
                // Init averages
                high_cal_vout.word = 0;
                low_cal_vout.word = 0;

                // Transition to the Smoke Calibration state
                photo_state = SMK_CALIBRATE;

            }
            else
            {
                if(FLAG_FIND_DRIVE_ERROR)
                {
                    //Send the calibration info for troubleshooting purposes
                    photo_send_cal_info();

                    // Drive Fault
                    RLED_On();
                    photo_state = SMK_DWELL;
                }
                else
                {
                    // Else remain in state and check next drive
                }

            }
        }
        break;

        case SMK_SET_LOW_DRIVE:
        {
            if(photo_find_drive())
            {

                // Initialize for High Drive
                PHOTO_Alarm_Thold = 0;

                FLAG_FIND_LOW_DRIVE_ACTIVE = 0;

                // Transition to the state that will find PWM setting that
                // corresponds to the high drive current.
                photo_state = SMK_SET_HIGH_DRIVE;

            }
            else
            {
                if(FLAG_FIND_DRIVE_ERROR)
                {
                    //Send the calibration info for troubleshooting purposes
                    photo_send_cal_info();

                    // Drive Fault
                    RLED_On();
                    photo_state = SMK_DWELL;
                }
                else
                {
                    // Else remain in state and check next drive
                }

            }
        }
        break;

        case SMK_CALIBRATE:
        {
            /*
             * In Calibrate mode, we measure the Photo Chamber output at two 
             * drive levels.  Using the difference between the drive current 
             * and the difference between the voltage output at the two drive 
             * levels, a slope is then calculated.
             *
             * First measure the Photo output and compute running average 
             * depending on which drive level was being used.
             */
            
            // Photo Drive current Pulse Width value
            SER_Send_Int('I', (uint)ired_pulse_width, 10);

            photo_measure_smk(&PHOTO_Reading);

            if(FLAG_PHOTO_HIGH_CAL)
            {
                if(0 == high_cal_vout.word)
                {
                    high_cal_vout.byte.msb = PHOTO_Reading;
                }
                else
                {
                    high_cal_vout.word = 
                         ALG_Running_Average(PHOTO_Reading,high_cal_vout.word);
                }

                PHOTO_Set_Drive_Pulse_Width(photo_drive_low);
                FLAG_PHOTO_HIGH_CAL = 0;
            }
            else
            {
                if(0 == low_cal_vout.word)
                {
                    low_cal_vout.byte.msb = PHOTO_Reading;
                }
                else
                {
                    low_cal_vout.word = 
                          ALG_Running_Average(PHOTO_Reading,low_cal_vout.word);
                }
                
                PHOTO_Set_Drive_Pulse_Width(photo_drive_high);
                FLAG_PHOTO_HIGH_CAL = 1;
            }

            if(0 == --photo_cal_counter)
            {
                // Before setting the Alarm Threshold, test the Chamber
                ///Output @ Low Drive against the Low Drive CAV limits.
                // Issue Smoke Cal fault if outside these limits.
                if( (low_cal_vout.byte.msb > SMK_CAV_CAL_FAULT_HI) ||
                    (low_cal_vout.byte.msb < SMK_CAV_CAL_FAULT_LO))
                {
                    //Send the calibration info for troubleshooting purposes
                    photo_send_cal_info();

                    // Cal Error
                    RLED_On();
                    photo_state = SMK_DWELL;
                    
                    return photo_set_sample_rate();
                }


                /*
                 * The data has been gathered for the calibration calculations.  
                 * First calculate the slope:
                 *   slope = (bHighCalVout-bLowCalVout) / (SMK_HIGH_DRIVE_MA - 
                 *    SMK_LOW_DRIVE_MA)
                 * Then:
                 *   Valarm = SMK_ALARM_SENS_SCALE_8_8 * slope
                 */

                // High calibration value is in accum A.
                //  Subtract low value to get numerator of slope.
                uint MathA = (high_cal_vout.byte.msb << 8);  // put in MS byte
                uint MathB = (low_cal_vout.byte.msb << 8);   // put in MS byte
                MathA = (MathA - MathB);

                //Turn numerator into 16.16F fixed format
                unsigned long MathA_L = MathA;
                MathA_L = (MathA_L << 16);      // shift to MS word

                // Put denominator into 8.8F fixed format.
                // Division will then produce an 8.8F number.
                MathB = (SMK_LED_CAL_DRIVE_DELTA << 8);
                MathA = (MathA_L / MathB);

                // Slope (sensitivity) is in MathA in 8.8 format.
                // Multipy by the slope constant.
                MathA_L = MathA;
                MathA_L = (MathA_L * (uint)SMK_ALARM_SENS_SCALE_8_8);

                // Turn result, 16.16F, into 8.8F and save for later in local
                // temporary variable

                // is not needed yet
                MathA_L = (MathA_L >> 16);

                uint wLocal = (uint)MathA_L;

                // Calculate CAV term
                MathB = (low_cal_vout.byte.msb << 8);

                MathA_L = MathB;
                MathA_L = (MathA_L * SMK_ALARM_OFFSET_SCALE_8_8);

                // Scale result to 8.8f
                MathA = (MathA_L >> 8);
                
                // CAV term is in MathA.  Add the two terms together
                MathB = wLocal;
                /*
                 * Here we add or Subtract depending on
                 * Sign of SMK_ALARM_OFFSET_SCALE_8_8
                 * Sometimes SMK_ALARM_OFFSET_SCALE_8_8 from MiniTab equation
                 * coef. calculation is a negative value.
                 * 
                 * Test for Negative or positive SMK_ALARM_OFFSET_SCALE_8_8 
                 *  result
                 */
                if(SMK_ALARM_SENS_SCALE_8_8_SIGN == POSITIVE)
                {
                    // Add if Sign Positive
                    MathA = (MathA + MathB);
                }
                else
                {
                    // Subtract if Sign Negative
                    MathA = (MathA - MathB);
                }

                // Sum of the two scale terms is in MathA. Add offset term

                MathA = (MathA + SMK_ALARM_OFFSET_8_8);
                // Round if needed
                MathA += 0x0080;

                // Integer portion of alarm point (in counts) is in MathA
                // MS byte
                MathA = (MathA >> 8);

                /*
                 * Integer portion of alarm point (in counts) is in MathA, now.
                 * Check value against high and low calibration error limits.
                 * Enter fatal fault mode if not within limits.
                 */
                if( (MathA > SMK_CAL_FAULT_THOLD_HIGH) ||
                     (MathA < SMK_CAL_FAULT_THOLD_LOW) )
                {
                    // Put in Ram data for serial output
                    SYS_RamData.Smk_Alarm_TH_At_Cal = (uchar)MathA;

                    //Send the calibration info for troubleshooting purposes
                    photo_send_cal_info();

                    // Clear after value has been output
                    SYS_RamData.Smk_Alarm_TH_At_Cal = 0;

                    // smoke Cal Fault
                    RLED_On();
                    photo_state = SMK_DWELL;
                    
                    return photo_set_sample_rate();
                }

                /*
                 * Also Verify that the Delta from CAV to Alarm TH is
                 * under 3.7 percent/foot. This so we guarantee no unit
                 * will be manufactured out of our low sens window limit.
                 */
                if( ((uchar)MathA - low_cal_vout.byte.msb) >
                        SMK_CAL_FAULT_SENSITIVITY)
                {
                    // Put in Ram data for serial output
                    SYS_RamData.Smk_Alarm_TH_At_Cal = (uchar)MathA;

                    //Send the calibration info for troubleshooting purposes
                    photo_send_cal_info();

                    // Clear after value has been output
                    SYS_RamData.Smk_Alarm_TH_At_Cal = 0;

                    // smoke Cal Fault
                    RLED_On();
                    photo_state = SMK_DWELL;
                    
                    return photo_set_sample_rate();
                }

                // Alarm point is withing acceptable limits.  Write it to the
                //  system data structure.
                SYS_RamData.Smk_Alarm_TH_At_Cal = (uchar)MathA;
                SYS_RamData.Smk_Corrected_Alm_TH = (uchar)MathA;

                /* 
                 * Put IR LED drive into buffer for EEPROM save
                 * Since the calibration is at 300 and 500 ma the low drive
                 * is the one that we want to use when in normal operation.
                 */
                SYS_RamData.Smk_IRled_Drive_Level = photo_drive_low;
                
                // Init Low Drive in Temporary variable used in PTT Drive State
                photo_cal_counter = photo_drive_low;

                //  Save photochamber output at calibration in EE for
                //  later compenstion
                SYS_RamData.Smk_Cav_At_Cal = low_cal_vout.byte.msb;

                // Save updated Data Structure in Non-Volatile Memory
                SYS_Save();

                // Completion of this phase of calibration
                photo_send_cal_info();
                SER_Send_Prompt();
                
                // Set Cal slope to use for Drift Comp
                smk_set_cal_corr_slope();

                // Init Low drive for Finding PTT Drive start since Cal ends
                // with High Drive Set
                PHOTO_Set_Drive_Pulse_Width(photo_drive_low);

                SER_Send_Int('I', (uint)ired_pulse_width, 10);
                SER_Send_Prompt();

                // Proceed to PTT Find Drive state
                photo_state = SMK_FIND_PTT_DRIVE;

                // The current method drives to Alarm Threshold
                photo_ptt_target_th = (SYS_RamData.Smk_Alarm_TH_At_Cal);
                
                #ifdef CONFIGURE_UNDEFINED
                    /*
                     * Potentially we could drive less and still have margin
                     *  to pass PTT Smoke Alarm test
                     * Calculate the Target Threshold for successful PTT Drive
                     * Make it about 10% below alarm Threshold
                     */
                    photo_ptt_target_th = (SYS_RamData.Smk_Alarm_TH_At_Cal - 
                            SMK_PTT_TARGET);
                #endif

            }
            else
            {
                // Transmit Serial Data, set Sample Rate, and return
                photo_send_smk_data();
            }

        }
        break;

        case SMK_FIND_PTT_DRIVE:
        {
            /*
             * The IRLED drive is increased a little at a time until the 
             * photochamber output voltage is equal to 1.xx? times the 
             * alarm voltage.
             */

            // Measure the photochamber output
            photo_measure_smk(&PHOTO_Reading);

            // Check the output here to see if it has railed.  
            // If so, enter error mode
            if(SMK_OUTPUT_RAILED == PHOTO_Reading)
            {
                // Drive Fault
                RLED_On();
                photo_state = SMK_DWELL;
            }
            else
            {
                /*
                 * IRED drive above Alarm Point is difficult in Clean Air
                 * Especially for lower targeted Smoke Sensitivites
                 * Since PTT only requires 50% of Alarm Threshold values,
                 * there is no need to drive above alarm point.
                 * 
                 * Drive to Target PTT Threshold
                 */
                
                if(photo_ptt_target_th <=  PHOTO_Reading)
                {
                    //Copy the IRLED drive to the system data structure
                    // and save
                    SYS_RamData.Smk_IRled_PTT_Drive = photo_cal_counter;
                    SYS_Save();

                    RLED_Off();
                    photo_state = SMK_DWELL;

                    SER_Send_Int('T', (uint)photo_cal_counter, 16);
                    SER_Send_Prompt();

                    #ifdef CONFIGURE_CO
                        // Add some delay to allow T value to TX. before reset
                        PIC_delay_ms(100);

                        /*
                         *   Right here we reset the Unit so that CO Cal begins 
                         *   if the unit is a CO Model and SMK has calibrated 
                         *   sucessfully.
                         *	 Also, Init CAV average before Reset (since this 
                         *    isn't a POR)
                         */
                        PHOTO_Init_CAV_Avg();
                        SOFT_RESET
                    #else
                        photo_state = SMK_DWELL;
                    #endif
                }
                else
                {
                    // photo_cal_counter holds the current IRLED Drive.  
                    // Increment it before configuring the PWM drive.  
                    //  Limit it to max
                   if(photo_cal_counter  >= SMK_OUTPUT_ALMOST_RAILED)
                   {
                        // Drive Fault
                        RLED_On();
                        photo_state = SMK_DWELL;
                   }
                   else
                   {
                        photo_cal_counter += 2;

                        // Copy the Photo reading into a variable that the 
                        //  drive setting routine uses for output.
                        PHOTO_Alarm_Thold = PHOTO_Reading;
                        // Set next pulse IRED drive width
                        PHOTO_Set_Drive_Pulse_Width(photo_cal_counter);
                   }
                }
            }
        }
        break;

        case SMK_STANDBY:
        {
            // Take a smoke measurement
            if(FALSE == photo_measure_smk(&PHOTO_Reading))
            {
                // Delay the measurement
                return MAIN_Make_Return_Value(SMK_RETURN_100_MS);
            }
            else
            {
              if(photo_smk_chamber_test())
              {
                // Smoke measurement looks good, process it

                #ifdef CONFIGURE_CO
                    // But only continue if CO Sensor has been Calibrated
                    if(CAL_STATE_CALIBRATED != CO_Cal_Get_State() )
                    {
                        return MAIN_Make_Return_Value(SMK_SAMPLE_1_SEC);
                    }
                #endif

                // Average Photo reading (Standby State Only)
                ALG_Photo_Reading_Avg();

                #ifdef DBUG_SERIAL_OUTPUT_SMK_AVG
                    // Test Only
                    SER_Send_String("PhotoAVG");
                    SER_Send_Int('-', PhotoReadingAvg.word, 16);
                    SER_Send_Prompt();

                    SER_Send_String("CAV AVG");
                    SER_Send_Int('-', PHOTO_Cav_Average.word, 16);
                    SER_Send_Prompt();
                #endif

               #ifdef CONFIGURE_ALG_OFF
                    FLAG_ALG_ENABLED = 0;
                #endif

                if(FLAG_PTT_ACTIVE || !FLAG_ALG_ENABLED)
                {
                    // During PTT wait until Chamber has been stimulated
                    //  by PTT before entering alarm so we do not
                    //  respond to Smoke events during PTT active
                    if(!FLAG_PTT_COUNDOWN_ACTIVE)
                    {
                        // No Algorithm processing
                        /*
                         *****************************************
                         * Non Alg Standby Measurement Processing
                         *	1. Test Smoke Reading
                         *	2. Set Alarm or Remain in Standby
                         *	3. Maintain Alarm Count as applicable
                         *****************************************
                         */

                        #ifdef CONFIGURE_DUCT_TEST
                            // No alarms with Duct Test FW unless PTT
                            if(FLAG_PTT_ACTIVE)
                            {
                                photo_smk_output_test();
                            }
                        #else
                            //Test Smoke Reading for current state
                            photo_smk_output_test(); 
                        #endif

                        if(FLAG_CAL_ALM_DETECT)
                        {
                            //Smoke Alarm Detected
                            if(0 == --smoke_update_count)
                            {
                                // Proceed to Smoke Alarm state
                                photo_init_smk_state(SMK_ALARM_NORMAL);
                            }
                        }
                        else
                        {
                            //Reset Alarm Count
                            smoke_update_count = SMK_ALARM_COUNTS;
                        }
                    }
                }
                else
                {
                    // Algorithm Processing

                    /*
                     *****************************************
                     * Alg Standby Measurement Processing
                     *	1. Refresh Alg. Inputs
                     *	2. Determine Algorithm Action
                     *	3. Initialize State Action
                     *	4. Update Sample Rate per Action
                     *****************************************
                     */
                    ALG_ROR_Monitor_SMK();          // Refresh ROR average


                    photo_alg_update_inputs();      // Update Alg. Inputs
                                                    // (ALG_Status updated)

                    // Determine Next Action
                    uchar astate = photo_alg_standby_state_test();

                    if(SMK_STATE_NO_ACTION != astate)
                    {
                        // Change to next alg. state
                        photo_init_smk_state(astate);
                    }
                }
              }
            }
            
            // Transmit Serial Data, set Sample Rate, and return
            photo_send_smk_data();            

        }
        break;

        case SMK_ALARM_NORMAL:
        {
            if(FLAG_SMOKE_HUSH_REQUEST)
            {
                photo_init_smk_state(SMK_ALARM_HUSH);
            }
            else
            {
                // Take a smoke measurement
                if(FALSE == photo_measure_smk(&PHOTO_Reading))
                {
                    // Delay the measurement
                    return MAIN_Make_Return_Value(SMK_RETURN_100_MS);
                }
                else
                {
                    #ifdef  CONFIGURE_ALG_OFF
                        FLAG_ALG_ENABLED = 0;
                    #endif

                    // For PTT don't use Alg Alarm point.
                    if(FLAG_PTT_ACTIVE)
                    {
                        // No Algorithm processing

                        //Check the smoke level against the PTT Threshold
                        photo_smk_output_test();
                        //
                        // Since we are in PTT, Use Calibrated Alarm threshold.
                        //
                        if(FLAG_CAL_ALM_DETECT)    
                        {
                            // SMK level still in Alarm
                            // Reset Alarm exit counter
                            smoke_update_count = SMK_NO_ALARM_COUNTS;
                            
                            #ifdef CONFIGURE_PRODUCTION_UNIT
                                // Test Only
                                SER_Send_String("PTT Alm");
                                SER_Send_Prompt();
                            #endif
                            
                        }
                        else
                        {
                            // The chamber voltage is less than the threshold,
                            //  decrement the counter. At zero counts the alarm
                            //  is off and we change to the Alarm STBY state.
                            if(0 == --smoke_update_count)
                            {
                                // return to Standby State
                                photo_init_smk_state(SMK_STANDBY);
                            }
                        }
                    }
                    else if(!FLAG_ALG_ENABLED )
                    {
                        // No Algorithm processing
                        // For ALG Off use Alg Alarm point.

                        //Check the smoke level against the Active Alarm point.
                        //  (Hysteresis included)
                        photo_smk_output_test();
                        // Since we are not using ALG, Use Calibrated Alarm
                        //  threshold.
                        //
                        // Use FLAG_ALG_ALM_DETECT for Hysteresis
                        //
                        if(FLAG_ALG_ALM_DETECT)    
                        {
                            // SMK level still in Alarm
                            // Reset Alarm exit counter
                            smoke_update_count = SMK_NO_ALARM_COUNTS;
                        }
                        else
                        {
                            // The chamber voltage is less than the threshold,
                            //  decrement the counter. At zero counts the alarm
                            //  is off and we change to the Alarm STBY state.
                            if(0 == --smoke_update_count)
                            {
                                // return to Standby State
                                photo_init_smk_state(SMK_STANDBY);
                            }
                        }
                    }
                    else
                    {
                       // Algorithm Processing
                       /*
                        *****************************************
                        * Alg Measurement Processing
                        *	1. Refresh Alg. Inputs
                        *	2. Determine Algorithm Action
                        *	3. Initialize State Action
                        *	4. Update Sample Rate per Action
                        */
                        ALG_ROR_Monitor_SMK();          // Refresh ROR average
                       
                        photo_alg_update_inputs();      // Update Alg. Inputs
                                                        // (ALG_Status updated)

                        // Determine Next Action
                        uchar astate = photo_alg_alarm_state_test();
                        
                        if(SMK_STATE_NO_ACTION != astate)
                        {
                            // Change to next alg. state
                            photo_init_smk_state(astate);
                        }
                    }
                }
            }

            // Transmit Serial Data, set Sample Rate, and return
            photo_send_smk_data();
            
        }
        break;

        case SMK_ALARM_HUSH:
        {
          // In case Hush was initiated from Remote Hush from Master Smoke
          // Clear Exit to Hush Flag to insure master smoke state re-entry 
          //  does not exit smoke again 
          FLAG_SMK_EXIT_TO_HUSH = 0;
          
          #ifdef CONFIGURE_SMK_HUSH_ACTIVATE

            // While in Hush, if the button is pressed again, Cancel Hush
            //  This feature, BTN Hush Cancel, disabled in this model
            if(FLAG_SMOKE_HUSH_REQUEST)
            {
                
                #ifdef CONFIGURE_HUSH_CANCEL                
                    // Determine which state to exit Hush to:
                    if(FLAG_CAL_ALM_DETECT)
                    {
                        photo_init_smk_state(SMK_ALARM_NORMAL);
                    }
                    else
                    {
                        #ifdef CONFIGURE_VOICE
                            // Announce Hush Cancelled when returning to Stby
                            // Currently announced at Button press
                            VCE_Play(VCE_HUSH_CANCELLED);
                        #endif

                        photo_init_smk_state(SMK_STANDBY);
                    }
                    
                    #ifdef  CONFIGURE_WIRELESS_ALARM_LOCATE  
                    
                        // If LOCATE CLEAR ARM not Sent
                        if(!FLAG_SMOKE_HUSH_ARM_SENT)
                        {
                            APP_PTT_Arm();
                            FLAG_SMOKE_HUSH_ARM_SENT = 1;

                            // Wait at least 10 seconds
                            return MAIN_Make_Return_Value
                                    (SMK_SAMPLE_10_SEC);
                        }
                        else
                        {
                            #ifdef CONFIGURE_VOICE
                                // Announce Hush Cancelled when
                                // returning to Standby
                                VCE_Play(VCE_HUSH_CANCELLED);
                            #endif

                            // Send Locate Clear message in 
                            //  case Locate is still active 
                            //  in Remotes
                            FLAG_ALARM_LOCATE_CLEAR = 1;
                            FLAG_SMOKE_HUSH_ARM_SENT = 0;

                            photo_init_smk_state(SMK_STANDBY);
                        }
                    #endif
                    
                #else
                    // Clear Hush Cancel Request
                    FLAG_SMOKE_HUSH_REQUEST = 0;
                #endif

            }
          #endif
            else
            {
                // Take a smoke measurement
                if(FALSE == photo_measure_smk(&PHOTO_Reading))
                {
                    // Delay the measurement
                    return MAIN_Make_Return_Value(SMK_RETURN_100_MS);
                }
                else
                {
                    /* 
                     * Allow Hush Processing to Continue even during a Chamber
                     *  Fault. This should cause Hush to be terminated and Fault 
                     *  to be reported to user (Fatal Fault Mode) since
                     *  a low output is below Alarm Threshold
                     *
                     *  if(photo_smk_chamber_test()) changed to:
                     * 
                     *  photo_smk_chamber_test();
                     */
                    photo_smk_chamber_test();
                    
                        photo_smk_output_test();
                        
                        #ifdef CONFIGURE_AUSTRALIA
                            /*
                             * Australia has no Smoke Hush limits
                             *  so always clear Smoke Hush Alarm detect flag
                             *  to ignore Too Much Smoke ending Hush
                             */
                            FLAG_ALG_ALM_DETECT = 0;
                        #endif

                        if(FLAG_ALG_ALM_DETECT)
                        {
                            // If LOCATE CLEAR ARM not Sent
                            if(!FLAG_SMOKE_HUSH_ARM_SENT)
                            {
                                #ifdef CONFIGURE_WIRELESS_MODULE
                                    APP_PTT_Arm();
                                #endif

                                FLAG_SMOKE_HUSH_ARM_SENT = 1;

                                #ifdef CONFIGURE_WIRELESS_MODULE
                                    // Wait 11 seconds for Real Time
                                    //  mode to activate
                                    return MAIN_Make_Return_Value
                                                    (SMK_SAMPLE_11_SEC);
                                #else
                                    // Return sooner if not wireless
                                    return MAIN_Make_Return_Value
                                                    (SMK_SAMPLE_1_SEC);
                                #endif
                            }
                            else
                            {
                                FLAG_SMOKE_HUSH_ARM_SENT = 0;
                                
                                #ifdef CONFIGURE_WIRELESS_MODULE
                                    // Send Locate Clear directly from here
                                    //  to prevent sending on top of CCI
                                    //  Alarm activity (CCI message collisions 
                                    //  were ocurring about 50% of the time)
                                    APP_Send_Clr_Alarm_Locate();

                                #endif

                                // Level above Hush Threshold, return to Smoke 
                                //  Alarm State
                                photo_init_smk_state(SMK_ALARM_NORMAL);
                                
                                // The Master Smoke Flag has just been set
                                //  which will tranmit CCI next APP Monitor
                                //  task. Delay this transmission
                                MAIN_Reschedule_Int_Active
                                    (TASKID_APP_STAT_MONITOR, 
                                        ALM_RESCHEDULE_TIME_100_MS);
                            }
                            
                        }
                        else
                        {
                            /*
                             * The Photo chamber output is below the alarm + 
                             *  hush limit point.
                             * See if hush timeout has occurred
                             */
                            if(0 == --hush_timeout_timer)
                            {
                                // Determine which state to exit Hush to:
                                if(FLAG_CAL_ALM_DETECT)
                                {
                                    photo_init_smk_state(SMK_ALARM_NORMAL);
                                }
                                else
                                {
                                    #ifdef CONFIGURE_VOICE
                                        // Announce Hush Canceled when
                                        // returning to Standby
                                        VCE_Play(VCE_HUSH_CANCELLED);
                                    #endif

                                    FLAG_ACTIVE_TIMER = 1;
                                    // Minimum time to remain in Active Mode
                                    MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_2_SEC;

                                    photo_init_smk_state(SMK_STANDBY);
                                    
                                }
                            }
                            else
                            {
                                #ifdef CONFIGURE_AUSTRALIA

                                    // Australia Smoke Hush exits on Timeout
                                    //  only

                                #else
                                    // For this model We Cancel Hush if
                                    //   back in clean air or below Alarm
                                    //   threshold is detected.
                                    if(!FLAG_CAL_ALM_DETECT)
                                    {
                                        #ifdef CONFIGURE_WIRELESS_ALARM_LOCATE 

                                            // If LOCATE CLEAR ARM not Sent
                                            if(!FLAG_SMOKE_HUSH_ARM_SENT)
                                            {
                                                APP_PTT_Arm();
                                                FLAG_SMOKE_HUSH_ARM_SENT = 1;

                                                // Wait 11 seconds for 
                                                //  Real Time mode to activate
                                                return MAIN_Make_Return_Value
                                                        (SMK_SAMPLE_11_SEC);
                                            }
                                            else
                                            {
                                              #ifdef CONFIGURE_VOICE
                                                // Announce Hush Canceled 
                                                // when returning to 
                                                // Standby
                                                VCE_Play
                                                    (VCE_HUSH_CANCELLED);
                                              #endif

                                              // Send Locate Clear directly 
                                              //  from here
                                              APP_Send_Clr_Alarm_Locate();

                                              FLAG_SMOKE_HUSH_ARM_SENT = 0;

                                              photo_init_smk_state(SMK_STANDBY);
                                              
                                              // Delay Alarm Clear transmission
                                              MAIN_Reschedule_Int_Active
                                                (TASKID_APP_STAT_MONITOR, 
                                                  ALM_RESCHEDULE_TIME_100_MS);
                                                
                                            }

                                        #else
                                            #ifdef CONFIGURE_VOICE
                                                // Announce Hush Canceled when
                                                // returning to Standby
                                                VCE_Play(VCE_HUSH_CANCELLED);
                                            #endif

                                            photo_init_smk_state(SMK_STANDBY);
                                        #endif

                                    }
                                    else
                                    {
                                        // Smoke Hush still active

                                        #ifdef CONFIGURE_HUSH_LOCATE_REFRESH
                                            // Refresh Locate Timer if 
                                            //  configured
                                            hush_minute_count++;

                                            if(SMK_HUSH_TIME_50_SECONDS ==
                                                            hush_minute_count)
                                            {
                                                // 10 seconds before minute 
                                                //  timer expires we need to  
                                                //  send Real Time ARM message
                                                APP_PTT_Arm();

                                            }
                                            else if(SMK_HUSH_TIME_1_MINUTE <= 
                                                           hush_minute_count)
                                            {
                                                // Every minute refresh Locate 
                                                //  Mode
                                                hush_minute_count = 0;
                                                FLAG_ALARM_LOCATE_REQUEST = 1;
                                            }
                                        #endif

                                    }
                                #endif
                            }
                        }
                }
            }
            // Transmit Serial Data, set Sample Rate, and return
            photo_send_smk_data();
            
        }
        break;

        case SMK_JUMP_MODE:
        {
            
            // Take a smoke measurement
            if(FALSE == photo_measure_smk(&PHOTO_Reading))
            {
                // Delay the measurement
                return MAIN_Make_Return_Value(SMK_RETURN_100_MS);
            }
            else
            {
                if(photo_smk_chamber_test())
                {
                    // Smoke measurement looks good, process it

                    /*
                     *****************************************
                     * Alg Measurement Processing
                     *	1. Refresh Alg. Inputs
                     *	2. Determine Algorithm Action
                     *	3. Initialize State Action
                     *	4. Update Sample Rate per Action
                     */
                    ALG_ROR_Monitor_SMK();          // Refresh ROR average
                    photo_alg_update_inputs();      // Update Alg. Inputs
                                                    // (ALG_Status updated)

                    // Determine Next Action
                    uchar astate = photo_alg_jump_state_test();

                    if(SMK_STATE_NO_ACTION != astate)
                    {
                        // Change to next alg. state
                        photo_init_smk_state(astate);
                    }
                }
            }

            // Transmit Serial Data, set Sample Rate, and return
            photo_send_smk_data();
            
        }
        break;

        case SMK_SLUMP_MODE_1:
        {
            // Take a smoke measurement
            if(FALSE == photo_measure_smk(&PHOTO_Reading))
            {
                // Delay the measurement
                return MAIN_Make_Return_Value(SMK_RETURN_100_MS);
            }
            else
            {
                if(photo_smk_chamber_test())
                {
                    // Smoke measurement looks good, process it

                    /*
                     *****************************************
                     * Alg Measurement Processing
                     *	1. Refresh Alg. Inputs
                     *	2. Determine Algorithm Action
                     *	3. Initialize State Action
                     *	4. Update Sample Rate per Action
                     */
                    ALG_ROR_Monitor_SMK();          // Refresh ROR average
                    photo_alg_update_inputs();      // Update Alg. Inputs
                                                    // (ALG_Status updated)

                    // Determine Next Action
                    uchar astate = photo_alg_slump1_state_test();

                    if(SMK_STATE_NO_ACTION != astate)
                    {
                        // Change to next alg. state
                        photo_init_smk_state(astate);
                    }
                }
            }

            // Transmit Serial Data, set Sample Rate, and return
            photo_send_smk_data();
            
        }
        break;

        case SMK_SLUMP_MODE_2:
        {
            // Take a smoke measurement
            if(FALSE == photo_measure_smk(&PHOTO_Reading))
            {
                // Delay the measurement
                return MAIN_Make_Return_Value(SMK_RETURN_100_MS);
            }
            else
            {
                if(photo_smk_chamber_test())
                {
                    // Smoke measurement looks good, process it

                    /*
                     *****************************************
                     * Alg Measurement Processing
                     *	1. Refresh Alg. Inputs
                     *	2. Determine Algorithm Action
                     *	3. Initialize State Action
                     *	4. Update Sample Rate per Action
                     */
                    ALG_ROR_Monitor_SMK();          // Refresh ROR average
                    photo_alg_update_inputs();      // Update Alg. Inputs
                                                    // (ALG_Status updated)

                    // Determine Next Action
                    uchar astate = photo_alg_slump2_state_test();

                    if(SMK_STATE_NO_ACTION != astate)
                    {
                        // Change to next alg. state
                        photo_init_smk_state(astate);
                    }
                }
            }

            // Transmit Serial Data, set Sample Rate, and return
            photo_send_smk_data();            
        }
        break;

        case SMK_DWELL:
        {
            // 'Just Hangin'            
        }
        break;

        default:
            photo_state = SMK_PHOTO_IDLE;
        break;
    }

    return photo_set_sample_rate();
}


/*
+------------------------------------------------------------------------------
| Function:      photo_init_smk_state
+------------------------------------------------------------------------------
| Purpose:       Initializes and Sets selected Photo state
|                Outputs: photo_state set
|                   State's Active Alarm Point Set (for ALGORITHM)
|                   State Entry Trigger variables Initialized
+------------------------------------------------------------------------------
| Parameters:    smk_state - holds state to Initialize
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void photo_init_smk_state(uchar smk_state)
{
    // Set the state
    photo_state = smk_state;

    switch (smk_state)
    {
        case SMK_STANDBY:
        {
            /*
             * Initialize the Following
             * 1. Alarm Counter
             * 2. Algorithm Input Triggers (SMK, SRR, CO, CRR, Temp)
             * 3. Alarm and Hush Flags (cleared for Standby State)
             */
            // set # of consequtive measurements to enter alarm
            smoke_update_count = SMK_ALARM_COUNTS;
            
            // Clear the Hush request flag.
            FLAG_SMOKE_HUSH_REQUEST = 0;    // Insure Hush is Cleared
            FLAG_MASTER_SMOKE_ACTIVE = 0;   // Clear Smoke Alarm Active
            FLAG_SMOKE_HUSH_ACTIVE = 0;     // Insure Smoke Hush is Off

            FLAG_JUMPED_STATE = 0;

            // Init Algorithm Input Triggers and Jump Timer
            ALG_SMK_Alarm_Trigger = (PHOTO_Alarm_Thold);

            ALG_SMK_Delta_Trigger = ALG_SMK_STBY_DELTA_TRIGGER;

            #ifdef	CONFIGURE_CO
                ALG_CO_PPM_Trigger = ALG_SMK_STBY_CO_TRIGGER;
                ALG_CO_ROR_Trigger = ALG_SMK_STBY_CROR_TRIGGER;
            #endif

            ALG_SMK_ROR_Trigger = ALG_SMK_STBY_SROR_TRIGGER;

            #ifdef	CONFIGURE_TEMPERATURE
                ALG_TMP_ROR_Trigger = ALG_SMK_STBY_TROR_TRIGGER;
            #endif
        }
        break;

        case SMK_ALARM_NORMAL:
        {

            // set # of consecutive measurements to exit alarm
            smoke_update_count = SMK_NO_ALARM_COUNTS;

            // Clear the Hush request flag.
            FLAG_SMOKE_HUSH_REQUEST = 0;
            FLAG_MASTER_SMOKE_ACTIVE = 1;   // Set Smoke Alarm Active
            FLAG_SMOKE_HUSH_ACTIVE = 0;     //Insure Smoke Hush is Off


            if(FLAG_JUMPED_STATE)
            {
                // We alarmed from the Jump State
                FLAG_JUMPED_STATE = 0;

                // Simple exit, Jump Alarm Trigger already set

            }
            else
            {
                // Set Normal Alarm Exit Threshold
                // Set negative hysteresis to exit Alarm
                ALG_SMK_Alarm_Trigger = (PHOTO_Alarm_Thold + 
                        SMK_ALARM_HYSTERESIS_CNTS);
            }
        }
        break;

        case SMK_ALARM_HUSH:
        {
            // Clear the Hush request flag.
            FLAG_SMOKE_HUSH_REQUEST = 0;
            FLAG_MASTER_SMOKE_ACTIVE = 0;
            FLAG_SMOKE_HUSH_ACTIVE = 1;

            // Set the hush timeout timers
            hush_timeout_timer = SMK_HUSH_TIME;
            
            #ifdef CONFIGURE_HUSH_LOCATE_REFRESH 
                hush_minute_count = 0;
            #endif

            // Set the Hush Alarm Trigger
            // Limit amount of Hush to about 6 uAmps
            uchar hush_alarm = ALG_SMK_HUSH_FIXED;

            ALG_SMK_Alarm_Trigger = (PHOTO_Alarm_Thold + hush_alarm);

            // Check to see if hush_alarm wrapped back around thru 0
            // exceeds Alarm Limit
            if( (ALG_SMK_Alarm_Trigger < PHOTO_Alarm_Thold) ||
                 (SMK_ALARM_ABS_MAX_LIMIT < ALG_SMK_Alarm_Trigger) )
            {
                ALG_SMK_Alarm_Trigger = SMK_ALARM_ABS_MAX_LIMIT;
            }

            #ifdef CONFIGURE_DIAG_HIST
                DIAG_Hist_Que_Push(HIST_SMK_ALARM_HUSH_EVENT);
            #endif
        }
        break;

        case SMK_JUMP_MODE:
        {
            FLAG_JUMPED_STATE = 1;
            
            // Init Algorithm Input Triggers and Jump Timer
            unsigned long Matha = (((unsigned long)PHOTO_Alarm_Thold - 
                            PHOTO_Cav_Average.byte.msb) << 16);
            
            Matha = ( ((Matha * ALG_SMK_JUMP_PERCENT)/100)  >> 16);
            uchar bAlgSmokeJump = (uchar)Matha;

            // Limit max amount of Jump (~ 3 uAmps)
            if(ALG_SMK_JUMP_LIMIT < bAlgSmokeJump)
            {
                bAlgSmokeJump = ALG_SMK_JUMP_LIMIT;
            }
            ALG_SMK_Alarm_Trigger = (PHOTO_Alarm_Thold - bAlgSmokeJump);
            
            ALG_SMK_Delta_Trigger = ALG_SMK_JUMP_DELTA_TRIGGER;

            #ifdef CONFIGURE_CO
                ALG_CO_PPM_Trigger = ALG_SMK_JUMP_CO_TRIGGER;
                ALG_CO_ROR_Trigger = ALG_SMK_JUMP_CROR_TRIGGER;
            #endif

            ALG_SMK_ROR_Trigger = ALG_SMK_JUMP_SROR_TRIGGER;

            #ifdef CONFIGURE_TEMPERATURE
                ALG_TMP_ROR_Trigger = ALG_SMK_JUMP_TROR_TRIGGER;
            #endif

            photo_init_minute_timer();    // Init Jump Algorithm Timer

            #ifdef CONFIGURE_DIAG_HIST
                DIAG_Hist_Que_Push(HIST_ALG_JUMP);
            #endif

        }
        break;

        case SMK_SLUMP_MODE_1:
        {
            // Init Algorithm Input Triggers and Slump Timer
            FLAG_JUMPED_STATE = 0;  //Clear Jumped State

            // Add Slump to Alarm Threshold
            #ifdef CONFIGURE_SLUMP_PERCENT
                unsigned long Matha = ((( unsigned long)PHOTO_Alarm_Thold -
                                            PHOTO_Cav_Average.byte.msb) << 16);
                Matha = ( ((Matha * ALG_SMK_SLUMP1_PERCENT)/(uint)100)  >> 16);
                uchar bAlgSmokeSlump1 = (uchar)Matha;

                // Limit max amount of Slump (~ 3 uAmps)
                if(ALG_SMK_SLUMP1_LIMIT < bAlgSmokeSlump1)
                {
                    bAlgSmokeSlump1 = ALG_SMK_SLUMP1_LIMIT;
                }
                ALG_SMK_Alarm_Trigger = (PHOTO_Alarm_Thold + bAlgSmokeSlump1);
            #else
                ALG_SMK_Alarm_Trigger = (PHOTO_Alarm_Thold +
                                         (uchar)ALG_SMK_SLUMP1_FIXED);
            #endif

            // Check to see if hush_alarm wrapped back around thru 0
            // exceeds Alarm Limit
            if( (ALG_SMK_Alarm_Trigger < PHOTO_Alarm_Thold) ||
                 (SMK_ALARM_ABS_MAX_LIMIT < ALG_SMK_Alarm_Trigger) )
            {
                ALG_SMK_Alarm_Trigger = SMK_ALARM_ABS_MAX_LIMIT;
            }

            ALG_SMK_Delta_Trigger = ALG_SMK_SLUMP1_DELTA_TRIGGER;

            #ifdef CONFIGURE_CO
                ALG_CO_PPM_Trigger = ALG_SMK_SLUMP1_CO_TRIGGER;
                ALG_CO_ROR_Trigger = ALG_SMK_SLUMP1_CROR_TRIGGER;
            #endif

            ALG_SMK_ROR_Trigger = ALG_SMK_SLUMP1_SROR_TRIGGER;

            #ifdef CONFIGURE_TEMPERATURE
                ALG_TMP_ROR_Trigger = ALG_SMK_SLUMP1_TROR_TRIGGER;
            #endif

            photo_init_minute_timer();    // Init Slump1 Algorithm Timer

            #ifdef CONFIGURE_DIAG_HIST
                DIAG_Hist_Que_Push(HIST_ALG_SLUMP1);
            #endif
        }
        break;

        case SMK_SLUMP_MODE_2:
        {
            // Init Algorithm Input Triggers and Slump Timer
            FLAG_JUMPED_STATE = 0;  //Clear Jumped State

            // Add Slump to Alarm Threshold
            #ifdef CONFIGURE_SLUMP_PERCENT
                unsigned long Matha = ((( unsigned long)PHOTO_Alarm_Thold -
                                            PHOTO_Cav_Average.byte.msb) << 16);
                Matha = ( ((Matha * ALG_SMK_SLUMP2_PERCENT)/100)  >> 16);
                uchar bAlgSmokeSlump2 = (uchar)Matha;

                // Limit max amount of Slump (~ 3 uAmps)
                if(ALG_SMK_SLUMP2_LIMIT < bAlgSmokeSlump2)
                {
                    bAlgSmokeSlump2 = ALG_SMK_SLUMP2_LIMIT;
                }
                ALG_SMK_Alarm_Trigger = (PHOTO_Alarm_Thold + bAlgSmokeSlump2);
            #else
                ALG_SMK_Alarm_Trigger = (PHOTO_Alarm_Thold +
                                                (uchar)ALG_SMK_SLUMP2_FIXED);
            #endif

            // Check to see if hush_alarm wrapped back around thru 0
            // exceeds Alarm Limit
            if( (ALG_SMK_Alarm_Trigger < PHOTO_Alarm_Thold) ||
                 (SMK_ALARM_ABS_MAX_LIMIT < ALG_SMK_Alarm_Trigger) )
            {
                ALG_SMK_Alarm_Trigger = SMK_ALARM_ABS_MAX_LIMIT;
            }

            ALG_SMK_Delta_Trigger = ALG_SMK_SLUMP2_DELTA_TRIGGER;

            #ifdef CONFIGURE_CO
                ALG_CO_PPM_Trigger = ALG_SMK_SLUMP2_CO_TRIGGER;
                ALG_CO_ROR_Trigger = ALG_SMK_SLUMP2_CROR_TRIGGER;
            #endif

            ALG_SMK_ROR_Trigger = ALG_SMK_SLUMP2_SROR_TRIGGER;

            #ifdef CONFIGURE_TEMPERATURE
                ALG_TMP_ROR_Trigger = ALG_SMK_SLUMP2_TROR_TRIGGER;
            #endif

            // Init the Slump 2 Timer Algorithm Timer (10 seconds)
            photo_init_second_timer();

            #ifdef CONFIGURE_DIAG_HIST
                DIAG_Hist_Que_Push(HIST_ALG_SLUMP2);
            #endif
        }
        break;

    }
}


/*
+------------------------------------------------------------------------------
| Function:     photo_alg_standby_state_test
+------------------------------------------------------------------------------
| Purpose:      Smoke Algorithm State Tests
|               Determines Action to take based upon Algorithm Status Inputs
|               (in bALG_status) 
|
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Actions to Take returned (Smoke State to transition to)
|                return SMK_STATE_NO_ACTION if not state change required
+------------------------------------------------------------------------------
*/

uchar photo_alg_standby_state_test(void)
{
    /*
     * Algorithm Conditions to Monitor
     *	1. SMK Alarm Triggers Active  AND
     *	   (NO SRR, NO CRR, NO TRR, SDELTA inactive) bALG_Status = 0x03
     *	   Action = Slump1 State
     */
    if(FLAG_CAL_ALM_DETECT)
    {
        //Smoke > CAl Alarm point
        if(0 == (bALG_Status.ALL & 0xF8))
        {
            // and NO SRR, NO CRR, NO TRR, SDELTA
            return SMK_SLUMP_MODE_1;
        }
    }

    #ifdef CONFIGURE_CO
        /*
         * Algorithm Conditions to Monitor
         *	2. SMK Delta Trigger Active  (rapid increase in smoke detected)
         *	   Action = Slump2 State
         */
        if(FLAG_SMK_DELTA_ACTIVE)
        {
            return SMK_SLUMP_MODE_2;
        }

        /*
         *	3. If Smoke above Alarm point here;
         *     test for and TRR and Slump2 condition
         */
        if(FLAG_CAL_ALM_DETECT && FLAG_TRR_ACTIVE)
        {
            return SMK_SLUMP_MODE_2;
        }
    #endif

    // If Smoke above Alarm point here simply enter Alarm
    if(FLAG_CAL_ALM_DETECT)
    {
        return SMK_ALARM_NORMAL;
    }

#ifdef CONFIGURE_CO
    /*
     *4. If Smoke NOT above Alarm point here
     *     and CO detected - enter Jump State
     */
    if(FLAG_CO_PPM_ACTIVE)
    {
        return SMK_JUMP_MODE;
    }
    else
    {
        return SMK_STATE_NO_ACTION;
    }
#else
    return SMK_STATE_NO_ACTION;
#endif
}


/*
+------------------------------------------------------------------------------
| Function:     photo_alg_alarm_state_test
+------------------------------------------------------------------------------
| Purpose:      Smoke Algorithm Alarm State Tests
|               Determines Action to take based upon Algorithm Status Inputs
|               (in bALG_status) 
|
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Actions to Take returned (Smoke State to transition to)
|                return SMK_STATE_NO_ACTION if not state change required
+------------------------------------------------------------------------------
*/

uchar photo_alg_alarm_state_test(void)
{

    if(FLAG_ALG_ALM_DETECT)
    {
        // Smoke still above Threshold (+ Hysteresis)
        // Reload the Smoke alarm Counter
        smoke_update_count = SMK_NO_ALARM_COUNTS;
        // and remain in Smoke Alarm state
        return SMK_STATE_NO_ACTION;
    }

    // Test the No Alarm counter
    if(--smoke_update_count == 0)
    {
        // After counter reaches zero, OK to return to Standby state
        return SMK_STANDBY;
    }
    else
    {
        return SMK_STATE_NO_ACTION;
    }

}


/*
+------------------------------------------------------------------------------
| Function:     photo_alg_jump_state_test
+------------------------------------------------------------------------------
| Purpose:      Smoke Algorithm Jump State Tests
|               Determines Action to take based upon Algorithm Status Inputs
|               (in bALG_status) 
|
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Actions to Take returned (Smoke State to transition to)
|                return SMK_STATE_NO_ACTION if not state change required
+------------------------------------------------------------------------------
*/

uchar photo_alg_jump_state_test(void)
{

    //Test for Smoke above Alarm Jump Threshold and CO
    if( FLAG_CO_PPM_ACTIVE && FLAG_ALG_ALM_DETECT )
    {
        return SMK_ALARM_NORMAL;
    }

    //Test for Smoke above CAL Alarm Threshold
    if(FLAG_CAL_ALM_DETECT)
    {
        // Alarm (regardless of CO level)
        return SMK_ALARM_NORMAL;
    }

    //If No ALG condition detected Check Timer
    if((uchar)ALG_JUMP_TIME > photo_get_alg_min_et())
    {
        return SMK_STATE_NO_ACTION;
    }
    else
    {
        // Timed Out
        if(FLAG_CO_PPM_ACTIVE)
        {
            // CO Level still detected
            return SMK_STATE_NO_ACTION;

        }
        else
        {
            // CO Level back near Clean Air
            return SMK_STANDBY;
        }
    }

}


/*
+------------------------------------------------------------------------------
| Function:     photo_alg_slump1_state_test
+------------------------------------------------------------------------------
| Purpose:      Smoke Algorithm Slump1 State Tests
|               Determines Action to take based upon Algorithm Status Inputs
|               (in bALG_status) 
|
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Actions to Take returned (Smoke State to transition to)
|                return SMK_STATE_NO_ACTION if not state change required
+------------------------------------------------------------------------------
*/

uchar photo_alg_slump1_state_test(void)
{
    /*
     *	1. SMK CAL Alarm Trigger Active (other inputs- don't care)
     *	   bALG_Status = xxxx x0xx
     *	   Action = Standby State
     */
    if(!FLAG_CAL_ALM_DETECT)
    {
        return SMK_STANDBY;
    }

    /*
     *	2. Any Other Input Triggers Active
     *	   bALG_Status = CRR or CO Trigger
     *	   Action = Alarm State
     */
    #ifdef	CONFIGURE_CO
        if((bALG_Status.ALL & 0x60) != 0)
        {
            // Smoke > ASMK AND CO or CRR
            //CRR or CO Triggered
            return SMK_ALARM_NORMAL;
        }
    #endif

    /*
     *	3. Any Other Input Triggers Active
     *	   bALG_Status = TRR or S_Delta Trigger
     *	   Action = Slump2 State
     */
    #ifdef	CONFIGURE_CO
        if((bALG_Status.ALL & 0x88) != 0)
        {
            // Smoke > ASMK AND TRR or S_Delta
            return SMK_SLUMP_MODE_2;
        }
    #else
        if((bALG_Status.ALL & 0x88) != 0)
        {
            // Smoke > ASMK AND TRR or S_Delta
            return SMK_ALARM_NORMAL;
        }
    #endif

    /*
     * Algorithm Conditions to Monitor
     *	4. SMK Slump Alarm Trigger Active (other inputs- don't care)
     *	   bALG_Status = xxxx x111
     *	   Action = Alarm State
     */
    if(FLAG_ALG_ALM_DETECT)
    {
        return SMK_ALARM_NORMAL;
    }

    /*
     *
     *	If No ALG conditions detected Check Timer
     *
     *	Test Slump Timeout Timer
     *	If Timed Out - Proceed to Alarm Mode
     */
    if((uchar)ALG_SLUMP1_TIME > photo_get_alg_min_et())
    {
        return SMK_STATE_NO_ACTION;
    }
    else
    {
        // Timed Out
        return SMK_ALARM_NORMAL;
    }
}


/*
+------------------------------------------------------------------------------
| Function:     photo_alg_slump2_state_test
+------------------------------------------------------------------------------
| Purpose:      Smoke Algorithm Slump2 State Tests
|               Determines Action to take based upon Algorithm Status Inputs
|               (in bALG_status) 
|
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Actions to Take returned (Smoke State to transition to)
|                return SMK_STATE_NO_ACTION if not state change required
+------------------------------------------------------------------------------
*/

uchar photo_alg_slump2_state_test(void)
{
    // Algorithm Conditions to Monitor
    /*
     *	   SMK Alarm Trigger Inactive/Active (other inputs- don't care)
     *	   bALG_Status = x11x xxxx (CO detected overrides Timer)
     *	   Action = Alarm
     */
    if(FLAG_CAL_ALM_DETECT)
    {
        //Smoke Above Alarm Threshold
        if((bALG_Status.ALL & 0x60) != 0)
        {
            //CRR or CO Triggered
            return SMK_ALARM_NORMAL;
        }

    }

    //	Test Slump Delay Timeout Timer
    //	If Timed Out - Proceed to Alarm Mode
    if((uchar)ALG_ALM_DELAY > photo_get_alg_sec_et())
    {
        return SMK_STATE_NO_ACTION;
    }
    else
    {
        // Timer Timed Out
        if(FLAG_CAL_ALM_DETECT)
        {
            return SMK_ALARM_NORMAL;
        }
        else
        {
            return SMK_STANDBY;
        }
    }
}


/*
+------------------------------------------------------------------------------
| Function:      photo_init_minute_timer
+------------------------------------------------------------------------------
| Purpose:       Initialize the Minute Timer
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void photo_init_minute_timer(void)
{
    photo_alg_timer = MAIN_OneMinuteTic;
}


/*
+------------------------------------------------------------------------------
| Function:      photo_init_second_timer
+------------------------------------------------------------------------------
| Purpose:       Initialize the Second Timer
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void photo_init_second_timer(void)
{
    photo_alg_timer = MAIN_OneSecTimer;
}


/*
+------------------------------------------------------------------------------
| Function:      photo_get_alg_min_et
+------------------------------------------------------------------------------
| Purpose:       Return Elapsed time in minutes
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  elapsed number of minutes
+------------------------------------------------------------------------------
*/

uchar photo_get_alg_min_et(void)
{
    return (uchar)(MAIN_OneMinuteTic - photo_alg_timer );
}


/*
+------------------------------------------------------------------------------
| Function:      photo_get_alg_sec_et
+------------------------------------------------------------------------------
| Purpose:       Return Elapsed time in seconds
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  elapsed number of seconds
+------------------------------------------------------------------------------
*/

uchar photo_get_alg_sec_et(void)
{
    return (uchar)(MAIN_OneSecTimer - photo_alg_timer );
}


/*
+------------------------------------------------------------------------------
| Function:      PHOTO_Set_Drive_Pulse_Width
+------------------------------------------------------------------------------
| Purpose:       Set Photo Smoke IRED Drive Pulse Width used in smoke
|                measurements
|
+------------------------------------------------------------------------------
| Parameters:    8 bit pulse width time
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void PHOTO_Set_Drive_Pulse_Width(uchar pw)
{
    ired_pulse_width = pw;

    // Output IRLed data
    photo_send_irled_data();
}


/*
+------------------------------------------------------------------------------
| Function:      PHOTO_Reset
+------------------------------------------------------------------------------
| Purpose:       reset Photo Smoke Task State
|               
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void PHOTO_Reset(void)
{
    photo_state = SMK_PHOTO_IDLE;
}


/*
+------------------------------------------------------------------------------
| Function:      PHOTO_Get_State
+------------------------------------------------------------------------------
| Purpose:       return current state of the Photo process task
|               
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  current Photo Smoke Task state
+------------------------------------------------------------------------------
*/

uchar PHOTO_Get_State(void)
{
    return (photo_state);
}


/*
+------------------------------------------------------------------------------
| Function:     photo_set_sample_rate
+------------------------------------------------------------------------------
| Purpose:      Checks algorithm status and sets Fast sample Flag accordingly
|               Smoke sampling can be either 10 seconds (slow)  
|                or 1 Second (fast).
|               During Algorithm states detecting levels of smoke, or smoke 
|                alarm fast sampling is used. (except Smoke Hush condition)
|               When CO is detected, fast sampling is enabled for 4 minutes 
|                while monitoring for smoke (Jump state)
|
|               Smoke sampling returns to Slow sampling if the 4 minute 
|                Timer expires
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  smoke sample rate to use for next measurement (16 bit value)
+------------------------------------------------------------------------------
*/

uint photo_set_sample_rate(void)
{
    // Fast sampling override for Calibration or PTT
    if(FLAG_NO_SMOKE_CAL  || FLAG_PTT_ACTIVE)
    {
        return MAIN_Make_Return_Value(SMK_SAMPLE_1_SEC);    // 1 second sampling
    }

    #ifdef CONFIGURE_DUCT_TEST
        // Use Slow sample Rate
        return MAIN_Make_Return_Value(SMK_SAMPLE_10_SEC);
    #else

    // While still in SMK Alarm State (after Alarm Flags are cleared)
    //  maintain fast sampling until returned to Standby
    if(SMK_ALARM_NORMAL == photo_state)
    {
        return MAIN_Make_Return_Value(SMK_SAMPLE_1_SEC);    // 1 second sampling
    }

    // If Alg. Off Default to slow sample rate
    if(!FLAG_ALG_ENABLED)
    {
        // Force Slow sample rate if ALG OFF
        FLAG_SMOKE_FAST_SAMPLE = 0;
    }
    
    // 1st, If in Smoke Hush state, we sample at Slow 10s rate
    if(FLAG_SMOKE_HUSH_ACTIVE)
    {
        FLAG_SMOKE_FAST_SAMPLE = 0;         //10 Second Sampling
    }
    else if(FLAG_ALG_SMK_DETECT  || FLAG_MASTER_SMOKE_ACTIVE)
    {
        // If Smoke Detected and not in Hush, the use fast sample rate
        ALG_Fast_Sample_Timer = 0;          // Reset Fast Sample Timer
        FLAG_SMOKE_FAST_SAMPLE = 1;         // Fast Sampling
    }
    // No Smoke, Test for CO
    else if(FLAG_CO_PPM_ACTIVE || FLAG_CRR_ACTIVE )
    {
        if(FLAG_FAST_SAMPLE_ENABLE)
        {
            //Fast Sampling Enabled (Timer not Timed Out)
            // Start 1 Second Sampling Rate (CO Triggered)
            FLAG_SMOKE_FAST_SAMPLE = 1;

            if(0 == ALG_Fast_Sample_Timer)
            {
                //Init the 4 minute timer
                ALG_Fast_Sample_Timer = TIMER_240_SEC;        // 4 minutes
            }
            else
            {
                if(0 == --ALG_Fast_Sample_Timer)
                {
                    // Timer Expired Disable Fast Sampling
                    FLAG_FAST_SAMPLE_ENABLE = 0;
                }
            }
        }
        else
        {
            FLAG_SMOKE_FAST_SAMPLE = 0;         //10 Second Sampling
        }

    }
    else
    {
        // No Smoke or CO is active
        FLAG_FAST_SAMPLE_ENABLE = 1;        // Re-enable the Timer
        ALG_Fast_Sample_Timer = 0;         // Clear the Timer

        FLAG_SMOKE_FAST_SAMPLE = 0;         // 10 Second Sampling

    }

    // Return Sample rate return value based upon sample Flag
    if(FLAG_SMOKE_FAST_SAMPLE)
    {
         // 1 second sampling
        return MAIN_Make_Return_Value(SMK_SAMPLE_1_SEC);  
    }
    else
    {
        // LP Mode Hush timing requires 10 second Sampling
        if(FLAG_MODE_ACTIVE  || FLAG_SMOKE_HUSH_ACTIVE)
        {
            // 10 second sampling
            return MAIN_Make_Return_Value(SMK_SAMPLE_10_SEC);  
        }
        else
        {
            // Saves current while in LP mode,  15 second sampling
            return MAIN_Make_Return_Value(SMK_SAMPLE_15_SEC);  
        }
    }
  #endif
}


/*
+------------------------------------------------------------------------------
| Function:     photo_measure_smk
+------------------------------------------------------------------------------
| Purpose:      Makes a photo chamber measurement.
|               Place 8-Bit PhotoChamber result in *pSmkResult
|
+------------------------------------------------------------------------------
| Parameters:   pointer to place smoke measurement result
+------------------------------------------------------------------------------
| Return Value: Return FALSE if Photo measurement is delayed
|               Return TRUE if Photo measurement taken
+------------------------------------------------------------------------------
*/

uchar photo_measure_smk(uchar* pSmkResult)
{
    #ifdef CONFIGURE_UNDEFINED
        /*
        * Long Voice times may prevent smoke measurements ie. Canadian messages
        *  that take longer than 5 seconds, so we cannot delay
        *  smoke sampling when voice is active.
        */
        #ifdef CONFIGURE_VOICE
            if(FLAG_VOICE_ACTIVE)
            {
                // Delay the measurement since Voice is active
                return FALSE;
            }
        #endif
    #endif

    //  If Sound is Active (Alarm Mode) and the Horn is On
    //   (Red LED follows the Horn)  Delay smoke measurement
    if( SND_STATE_IDLE != SND_Get_State() && FLAG_RED_LED_ON )
    {
        // Delay the measurement since Horn is Active
        return FALSE;

    }

    // Save previous smoke reading for any ROR calculations
    // This assumes bPhotoReading was used for Previous Sample
    ALG_PrevPhotoReading = PHOTO_Reading;

/*
 *
 * Here's the plan for making a measurement.
 *
 *
 * 1. Take a Smoke Reading with IRed LED Off (Offset measurement)
 *
 * 2. Turn on the IRLED drive pin and electronics power pin
 * 3. The drive will initialize at the same time as the
 *		electronics is initializing.
 * 4.  Tristate the drive pin.
 * 5.  Wait a short time to let the electronics finish warming up.
 * 6.  Make the A/D measurement.
 * 7.  Turn off the electronics.
 *
 * 8.  Subtract Offset measurement
 *
 */

    // Power On the Photo Circuit for both Temperature and
    //  Photo chamber Measurements
    LAT_PHOTO_PWR_PIN = 1;

    // Pre-enable A2D ON
    A2D_On();

    // OpAmp Power On Stabilization Time
    PIC_delay_us(OP_AMP_PWR_STABLE_TIME);     // Delay usecs

    #ifdef CONFIGURE_TEMPERATURE
        // Measure Temp Sensor and Update Temp ROR (if Smoke Cal complete)
        photo_temp_measure();
    #endif

    #ifdef CONFIGURE_PHOTO_OFFSET_MEASURE
        // Measure Photochamber output with IRED OFF
        photo_reading_offset = photo_smk_sample(IRLED_OFF);
    #endif

    // Disable Interrupts to protect Drive Pulse/Measurement Time
    GLOBAL_INT_ENABLE = 0;

    // Sample with IRED ON
    uchar bPhotoTemp = photo_smk_sample(IRLED_ON);

    // Only Measure Sense Current during Smoke calibration
    if(FLAG_NO_SMOKE_CAL)
    {
        // Sample IRED current while IRED is ON
        photo_ired_voltage = A2D_Convert_8Bit(A2D_IRED_SENSE_CHAN);
    }

    // Turn off the Drive IRLED Pin
    LAT_IRLED_DRIVE_PIN = 0;
    IRED_DRIVE_PIN_OUTPUT       // Discharge the Drive Cap

    // Power down electronics
    LAT_PHOTO_PWR_PIN = 0;

    //  Global Ints Enabled for Both Low Power CCI Ints and Active mode
    GLOBAL_INT_ENABLE = 1;

    #ifdef CONFIGURE_MANUAL_SMK_OUT
        /* For development testing only
         * If the Photo manual reading is not zero, use it in place of actual
         * Photo Reading. Phot manual reading can be set by Serial Port command
        */
        if(PHOTO_Man_Reading != 0)
        {
            bPhotoTemp = PHOTO_Man_Reading;
        }
    #endif

    #ifdef CONFIGURE_SMK_SLOPE_SIMULATION
        /* Only adjust if Smoke Cal is Completed and not during PTTs */

        if(!FLAG_NO_SMOKE_CAL && !FLAG_PTT_ACTIVE)
        {
            // If Smoke Slope Simulation is configured, adjust measurement
            // and save actual measurement for Serial Output
            photo_reading_actual = bPhotoTemp;
            
            uint photo_smk_slope = CMD_Get_Slope();

            // Start Slope simulation after Smoke level has begun increasing
            if((bPhotoTemp - SYS_RamData.Smk_Cav_At_Cal) > 
                    SLOPE_SIM_COUNT_THRESHOLD)
            {
                // Multiply the slope by the actual measurement;
                unsigned long MathA_L = (bPhotoTemp * photo_smk_slope);

                uint MathA_R = MathA_L & 0xFF;  // Save remainder
                MathA_L = MathA_L >> 8;         // and keep Integer value
                if(MathA_L > (SMK_OUTPUT_RAILED-1) )
                {
                    // Set to rail
                    MathA_L = SMK_OUTPUT_RAILED;
                }
                else
                {
                    // Test Remainder
                    if(MathA_R > SLOPE_SIM_ROUND_UP)
                    {
                        // Round Up
                        MathA_L ++;
                    }
                }

                // Save adjusted Smoke reading (8 bit value)
                bPhotoTemp = (uchar)MathA_L;
            }
        }
    #endif

    #ifdef CONFIGURE_PHOTO_OFFSET_MEASURE
        // Process the 2 measurements
        if(photo_reading_offset > PHOTO_DARK_LIMIT)
        {
            photo_reading_offset = PHOTO_DARK_LIMIT;
        }
        // Subtract Offset Reading
        if(bPhotoTemp > photo_reading_offset)
        {
            bPhotoTemp = bPhotoTemp - photo_reading_offset;
        }
    #endif

   /*
    *
    * 1st Smoke Reading after Reset, stores 0 in the Previous Buffer
    * If Previous reading is 0, then Init Previous Reading with Current Reading
    *
    */
    if(0 == ALG_PrevPhotoReading)
    {
        // Initialize the Previous Photo Reading
        ALG_PrevPhotoReading = bPhotoTemp;
    }

    /*
     * For Smoke Calibration we use variable photo_smoke_voltage
     * otherwise saved in bPhotoReading
     */
    *pSmkResult = bPhotoTemp;
    
    return TRUE;
}


#ifdef CONFIGURE_TEMPERATURE
/*
+------------------------------------------------------------------------------
| Function:      photo_temp_measure
+------------------------------------------------------------------------------
| Purpose:       Sample Temp Sensor and Update the Temperature Algorithm ROR
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void photo_temp_measure(void)
{
    #ifdef CONFIGURE_AC_TRANSIENT_FILTERING_TEMP
        unsigned int wTempReadingAvg;

        uint wTMPsamples[TEMP_NUM_SAMPLES];
        uchar bMaxPtr;
        uchar bMinPtr;
        uchar bNomPtr;
    #endif


    #ifdef CONFIGURE_CO
        if(!FLAG_CALIBRATION_COMPLETE)
        {
            return;
        }
    #endif

    //No Temp until calibrated
    if(!FLAG_NO_SMOKE_CAL)
    {
        // Record Temp into Prev Temp for ROR calculations
        ALG_Prev_Temp = ALG_ADTemp;

        #ifdef CONFIGURE_AC_TRANSIENT_FILTERING_TEMP

            #ifdef CONFIGURE_UNDEFINED
                // Test Only
                wTMPsamples[0] = (UINT)760;
                wTMPsamples[1] = (UINT)762;
                wTMPsamples[2] = (UINT)999;
                wTMPsamples[3] = (UINT)764;
                wTMPsamples[4] = (UINT)761;
                wTMPsamples[5] = (UINT)758;
            #endif

            #ifdef CONFIGURE_UNDEFINED
                wTMPsamples[0] = (UINT)764;
                wTMPsamples[1] = (UINT)555;
                wTMPsamples[2] = (UINT)761;
                wTMPsamples[3] = (UINT)759;
                wTMPsamples[4] = (UINT)761;
                wTMPsamples[5] = (UINT)758;
            #endif
            

            // If AC Power is ON, take multiple samples to filter
            //   AC Power Switching transients that may affect Temp measurement
            if(FLAG_AC_DETECT)
            {
                wTempReadingAvg = 0;
                bMaxPtr = 0;
                bMinPtr = 0;

                for(uchar i = 0; i < TEMP_NUM_SAMPLES; i++)
                {
                    #ifdef CONFIGURE_DEBUG_TEMP_SAMPLES
                        MAIN_TP2_On();
                    #endif

                    // Take the Temp Measurement
                    ALG_ADTemp = A2D_Temp();

                    #ifdef CONFIGURE_DEBUG_TEMP_SAMPLES
                        MAIN_TP2_Off();
                    #endif

                    // Fill Sample Array
                    wTMPsamples[i] = ALG_ADTemp;

                    // Track the min and max samples
                    if(wTMPsamples[i] < wTMPsamples[bMinPtr])
                    {
                        bMinPtr = i;
                    }
                    if(wTMPsamples[i] > wTMPsamples[bMaxPtr])
                    {
                        bMaxPtr = i;
                    }

                    // Add filtering delay between Samples
                    PIC_delay_us(DBUG_DLY_20_USec);     // Delay usecs

                }

                // Find a nominal value of the sample Collection
                for(uchar i = 0; i < TEMP_NUM_SAMPLES; i++)
                {
                    if((bMaxPtr != i) && (bMinPtr != i))
                    {
                        bNomPtr = i;
                        // End Loop
                        i = TEMP_NUM_SAMPLES;
                    }
                }

                // Elliminate the Min and Max values from sample collection
                wTMPsamples[bMaxPtr] = wTMPsamples[bNomPtr];
                wTMPsamples[bMinPtr] = wTMPsamples[bNomPtr];

                // Average the filtered samples
                for(uchar i = 0; i < TEMP_NUM_SAMPLES; i++)
                {
                    wTempReadingAvg = (wTempReadingAvg + wTMPsamples[i]);
                }
                ALG_ADTemp = (uint)( (wTempReadingAvg /(TEMP_NUM_SAMPLES)) );

            }
            else
            {
                #ifdef CONFIGURE_DEBUG_TEMP_SAMPLES
                    MAIN_TP2_On();
                #endif

                // Take the Temp Measurement
                ALG_ADTemp = A2D_Temp();

                #ifdef CONFIGURE_DEBUG_TEMP_SAMPLES
                    MAIN_TP2_Off();
                #endif

            }

            A2D_Set_Temp_Degress_F(photo_temp_avg.word);
            photo_temp_deg_f = A2D_Get_Degrees_F();

        #else

            // Take the Temp Measurement
            ALG_ADTemp = A2D_Temp();

            // Update temp sample average
            photo_temp_reading_avg();

            #ifdef CONFIGURE_TEMP_DEG_F
                A2D_Set_Temp_Degress_F(photo_temp_avg.word);
                photo_temp_deg_f = A2D_Get_Degrees_F();
            #endif

        #endif

        #ifdef  CONFIGURE_SERIAL_PORT_ENABLED
            if(SERIAL_ENABLE_TEMPERATURE)
            {
                // Send Temp Data
                SER_Send_Int('t', ALG_ADTemp, 10);
                SER_Send_String(",Avg ");
                SER_Send_Int('t', photo_temp_avg.word, 10);
                #ifdef CONFIGURE_TEMP_DEG_F
                    SER_Send_String(",Deg");
                    SER_Send_Int('F', (uint)photo_temp_deg_f, 10);
                #endif
                SER_Send_Prompt();
            }
        #endif

        // If previous temp is zero, then this is the 1st measurement
        //   after reset, so initialize previous temp.
        if(ALG_Prev_Temp == 0)
        {
            ALG_Prev_Temp = ALG_ADTemp;
        }
        
        // Refresh Temp ROR average
        ALG_ROR_Monitor_TMP();

    }
}
#endif


/*
+------------------------------------------------------------------------------
| Function:      photo_temp_reading_avg
+------------------------------------------------------------------------------
| Purpose:       Update the Temp Reading Running Average
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void photo_temp_reading_avg(void)
{
    // Initialize the average for the 1st sample
    if(0 == photo_temp_avg.word)
    {
        // Init reading 
        photo_temp_avg.word = ALG_ADTemp;
    }
    else
    {
        unsigned long x = (((unsigned long)photo_temp_avg.word * 3) 
                                + ALG_ADTemp);
        
        // Add for Rounding before divide
        x += 0x0002;
        // Divide by 4
        x = x >> 2;
        photo_temp_avg.word = (uint)x;
    }
}


/*
+------------------------------------------------------------------------------
| Function:      photo_smk_sample
+------------------------------------------------------------------------------
| Purpose:       Take a Smoke Measurement Sample
|
|
+------------------------------------------------------------------------------
| Parameters:    type  (IRLED_OFF, IRLED_ON)
+------------------------------------------------------------------------------
| Return Value:  8 bit smoke sample result
+------------------------------------------------------------------------------
*/

uchar photo_smk_sample(uchar type)
{
    uchar bSMKsample;

    #ifdef CONFIGURE_AC_TRANSIENT_FILTERING_SMK
        uint  wPhotoReadingAvg;

        uchar bSMKsamples[PHOTO_NUM_SAMPLES];
        uchar bMaxPtr;
        uchar bMinPtr;
        uchar bNomPtr;
    #endif


    // A/D Pre-enable Code to save some measurement time
    A2D_On();

    // Make Sure to Disable Interrupts to protect Drive Pulse Time
    // INTs Should already be off at this point
    if(IRLED_ON == type)
    {
        // Drive IRLED Pin
        LAT_IRLED_DRIVE_PIN = 1;
        IRED_DRIVE_PIN_OUTPUT

        // Apply bIREDPulse Width (usecs)
        uchar i = ired_pulse_width;
        while(--i > 0)
        {
            // Wait for pulse complete
        }

        // Turn off the IRED Drive
        IRED_DRIVE_PIN_INPUT
    }

    /*
     *
     * Delay before making measurement.  Since the IR LED current is supplied
     * by a cap, a short amount of time is required to reach steady state.  
     * This time also allows the electronics to warm up.
     *
     */
    #ifdef CONFIGURE_PHOTO_OFFSET_MEASURE
        PIC_delay_us(SMK_TIME_ELEC_WARMUP_60_USECS);
    #else
        PIC_delay_us(SMK_TIME_ELEC_WARMUP_5_USECS);
    #endif

    #ifdef  CONFIGURE_AC_TRANSIENT_FILTERING_SMK
        // If AC Power is ON, take multiple samples to filter
        //   AC Power Switching transients that may affect Smoke measurement
        if(FLAG_AC_DETECT)
        {
            wPhotoReadingAvg = 0;
            bMaxPtr = 0;
            bMinPtr = 0;

            // Collect samples and locate min and max values of sample 
            //  collection
            for(uchar i = 0; i < PHOTO_NUM_SAMPLES; i++)
            {
                #ifdef CONFIGURE_DEBUG_TEMP_SAMPLES
                    MAIN_TP2_On();
                #endif

                // Take the Smoke Measurement
                bSMKsample = A2D_Convert_8Bit(A2D_PHOTO_OUT_CHAN);

                #ifdef CONFIGURE_DEBUG_TEMP_SAMPLES
                    MAIN_TP2_Off();
                #endif

                // Fill the sample array
                bSMKsamples[i] = bSMKsample;

                // Track the min and max samples
                if(bSMKsamples[i] < bSMKsamples[bMinPtr])
                {
                    bMinPtr = i;
                }
                if(bSMKsamples[i] > bSMKsamples[bMaxPtr])
                {
                    bMaxPtr = i;
                }

                // Add filtering delay between Samples
                PIC_delay_us(DBUG_DLY_20_USec);     // Delay usecs
            }

            // Find a nominal value of the sample Collection
            for(uchar i = 0; i < PHOTO_NUM_SAMPLES; i++)
            {
                if((bMaxPtr != i)  && (bMinPtr != i))
                {
                    bNomPtr = i;
                    // End Loop
                    i = PHOTO_NUM_SAMPLES;
                }
            }

            #ifdef DBUG_SERIAL_OUTPUT_FILTER_SAMPLES
                // Test Only
                SER_Send_String("S1-");
                for(uchar i = 0; i < PHOTO_NUM_SAMPLES; i++)
                {
                    SER_Send_Int(' ', bSMKsamples[i], 10);
                    SER_Send_Char(',');
                }
                SER_Send_Prompt();
            #endif

            // Elliminate the Min and Max values from sample collection
            bSMKsamples[bMaxPtr] = bSMKsamples[bNomPtr];
            bSMKsamples[bMinPtr] = bSMKsamples[bNomPtr];

            // Average the filtered samples
            for(uchar i = 0; i < PHOTO_NUM_SAMPLES; i++)
            {
                wPhotoReadingAvg = (wPhotoReadingAvg + (uint)bSMKsamples[i]);
            }
            bSMKsample = (uchar)( (wPhotoReadingAvg /(PHOTO_NUM_SAMPLES)) );

            #ifdef DBUG_SERIAL_OUTPUT_FILTER_SAMPLES
                // Test Only
                SER_Send_String("S2-");
                for(uchar i = 0; i < PHOTO_NUM_SAMPLES; i++)
                {
                    SER_Send_Int(' ', bSMKsamples[i], 10);
                    SER_Send_Char(',');
                }
                SER_Send_Prompt();
            #endif

        }
        else
        {
            #ifdef CONFIGURE_DEBUG_TEMP_SAMPLES
                MAIN_TP2_On();
            #endif

            // Take a Smoke Measurement
            bSMKsample = A2D_Convert_8Bit(A2D_PHOTO_OUT_CHAN);

            #ifdef CONFIGURE_DEBUG_TEMP_SAMPLES
                MAIN_TP2_Off();
            #endif

            #ifdef DBUG_SERIAL_OUTPUT_FILTER_SAMPLES
                // Test Only
                SER_Send_String("S3-");
                SER_Send_Int(' ', bSMKsample, 10);
                SER_Send_Prompt();
            #endif

        }
    #else
        // Take a Smoke Measurement
        bSMKsample = A2D_Convert_8Bit(A2D_PHOTO_OUT_CHAN);
    #endif

    return bSMKsample;
}


/*
+------------------------------------------------------------------------------
| Function:      photo_send_irled_data
+------------------------------------------------------------------------------
| Purpose:       Output IR Led data via serial port
|                This function will only behave properly if called during the 
|                IRLED drive locating process.
|                Do not call it any other time because of varible sharing.
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void photo_send_irled_data(void)
{
    if(SERIAL_ENABLE_SMOKE && !FLAG_PTT_ACTIVE)
    {
        if( SMK_CALIBRATE != photo_state)
        {
            // Photo Drive current Pulse Width value
            SER_Send_Int('I', (uint)ired_pulse_width, 10);

            SER_Send_Char(',');
            if(FLAG_NO_SMOKE_CAL)
            {
                // photo_smoke_voltage is alias to bPhotoAlarmThold
                SER_Send_Int('S', (uint)photo_smoke_voltage, 10);
            }
            else
            {
                // At: = Compensated Alarm threshold (if Smoke calibrated unit)
                SER_Send_Char('A');
                SER_Send_Int('t', (uint)PHOTO_Alarm_Thold, 10);
            }
        }
        SER_Send_Prompt();
    }
}


/*
+------------------------------------------------------------------------------
| Function:      photo_send_cal_info
+------------------------------------------------------------------------------
| Purpose:       Send the three P values (High drive voltage, Lo Drive Voltage,
|                 and calibrated alarm threshold)
|                
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void photo_send_cal_info(void)
{
    if(SERIAL_ENABLE_SMOKE)
    {
        SER_Send_Int('P', (uint)high_cal_vout.byte.msb, 10);
        SER_Send_Char(',');
        SER_Send_Int(' ', (uint)low_cal_vout.byte.msb, 10);
        SER_Send_Char(',');
        SER_Send_Int(' ', (uint)SYS_RamData.Smk_Alarm_TH_At_Cal, 10);

    }
}


/*
+------------------------------------------------------------------------------
| Function:      photo_send_smk_data
+------------------------------------------------------------------------------
| Purpose:       Send the Smoke Data (s:values) with algorithm state,
|                 fault and alarm information
|                
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void photo_send_smk_data(void)
{
    if(FLAG_SERIAL_PORT_ACTIVE)
    {
        if(SERIAL_ENABLE_SMOKE)
        {

            #ifdef CONFIGURE_PHOTO_OFFSET_MEASURE
                SER_Send_Int('O', (uint)photo_reading_offset, 10);
                SER_Send_Char(',');
            #endif

            #ifdef CONFIGURE_SMK_SLOPE_SIMULATION

                if(FLAG_NO_SMOKE_CAL)
                {
                    SER_Send_Int('S', (uint)PHOTO_Reading, 10);
                }
                else
                {
                    // Output Slope Adjusted output
                    SER_Send_Int('S', (uint)photo_reading_actual, 10);
                    SER_Send_Char(',');

                    // Output Slope Adjusted output
                    SER_Send_String(" Sad");
                    SER_Send_Int('j', (uint)PHOTO_Reading, 10);
                }

            #else
                // Normal Smoke Output
                SER_Send_Int('S', (uint)PHOTO_Reading, 10);

            #endif

            if(FLAG_SMOKE_ERROR_PENDING)
            {
                SER_Send_Char('|');
                if(FLAG_FAULT_FATAL)
                {
                    SER_Send_Char('|');
                }
            }
            else
            {
                // Algorithm is Off during PTT and Smoke Calibration
                if( !FLAG_NO_SMOKE_CAL && !FLAG_PTT_ACTIVE &&
                    !FLAG_REMOTE_PTT_ACTIVE )
                {
                    // If unit is smoke calibrated send state character
                    // Offset into ASCII chars (Space, !, ", #, $, %)
                    uchar state_char = (photo_state + 26);
                    SER_Send_Char(state_char);
                }
                else if(FLAG_MASTER_SMOKE_ACTIVE)
                {
                    SER_Send_Char('!');
                }
            }

            #ifdef CONFIGURE_SERIAL_ALM_SOURCE_OUT
                // Output an alarm source character if in Alarm
                uchar alarm_state = ALM_Get_State();
                if( (alarm_state != ALM_STATE_ALARM_NONE) &&
                    (alarm_state != ALM_STATE_CO_CLR) &&
                    (alarm_state != ALM_STATE_SMK_CLR) )
                {
                    SER_Send_Prompt();
                    SER_Send_String("ALM");
                }
                

                switch (alarm_state)
                {
                    case ALM_ALARM_MASTER_CO:
                        SER_Send_String("-M CO");
                    break;
                    case ALM_STATE_MASTER_SMOKE:
                        SER_Send_String("-M SMK");
                    break;

                    case ALM_ALARM_SLAVE_CO:
                        SER_Send_String("-S CO");
                    break;
                    case ALM_ALARM_SLAVE_SMOKE:
                        SER_Send_String("-S SMK");
                    break;

                    case ALM_ALARM_REMOTE_CO:
                        SER_Send_String("-R CO");
                    break;
                    case ALM_ALARM_REMOTE_SMOKE:
                        SER_Send_String("-R SMK");
                    break;
                    
                    case ALM_ALARM_REMOTE_CO_HW:
                        SER_Send_String("-R CO HW");
                    break;
                    case ALM_ALARM_REMOTE_SMOKE_HW:
                        SER_Send_String("-R SMK HW");
                    break;

                }
                
                if(FLAG_ALARM_LOCATE_MODE)
                {
                    SER_Send_String(" - LOCATE ON ");
                }
                
 
            #endif

            SER_Send_Prompt();
                
            #ifdef CONFIGURE_MIN_MAX_OUT
                // Test Only
                if(PHOTO_Reading > photo_max)
                {
                    photo_max = PHOTO_Reading;
                }

                if(PHOTO_Reading < photo_min)
                {
                    photo_min = PHOTO_Reading;
                }

                SER_Send_String("S Mi");
                SER_Send_Int('n', (uint)photo_min, 10);
                SER_Send_Char(',');

                SER_Send_String("S Ma");
                SER_Send_Int('x', (uint)photo_max, 10);
                SER_Send_Prompt();

            #endif

        }
    }
}


/*
+------------------------------------------------------------------------------
| Function:      photo_alg_update_inputs
+------------------------------------------------------------------------------
| Purpose:       Update All Algorithm Inputs based upon Current conditions
|                Inputs:   Current Smoke, CO and Temp. Inputs
|                Outputs:  bALG_Status flags updated (FLAG_ALG_SMK_DETECT)
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void photo_alg_update_inputs(void)
{
    photo_smk_output_test();
    photo_smk_ror_test();

    #ifdef	CONFIGURE_CO
        photo_co_output_test();
        photo_co_ror_test();
    #endif

    #ifdef	CONFIGURE_TEMPERATURE
        photo_temp_ror_test();
    #endif

    #ifdef	CONFIGURE_SERIAL_PORT_ENABLED
        if(SERIAL_ENABLE_ALG)
        {
            // Send Algorithm Status in Hex, 'as:' for Algorithm Status
            SER_Send_Char('a');
            SER_Send_Int('s', (uint)bALG_Status.ALL, 16);
            SER_Send_Prompt();
        }
    #endif

}


/*
+------------------------------------------------------------------------------
| Function:     photo_find_drive
+------------------------------------------------------------------------------
| Purpose:      Find IRED Drive Drive level and return TRUE when correct level
|               is found.
| 
|               Uses bPhotoReading as temporary variable.
|               HighCalVout.byte.msb - contains the drive level used by this 
|                routine, must be initialized before first calling this routine
|
|               LowCalVout.byte.msb -  is a temporary variable in this routine
|
|               1. Take 4 samples at the current drive level
|               2. Average the 4 samples (/4)
|               3. Test against target drive level
|                 a. if reached target - output IRLED data, save drive level
|                    and return TRUE
|                 b. if under target - set next drive pulse (error based)
|                    and return FALSE
|
+------------------------------------------------------------------------------
| Parameters:   none
+------------------------------------------------------------------------------
| Return Value: TRUE or FALSE (TRUE if target reached)
+------------------------------------------------------------------------------
*/

uchar photo_find_drive(void)
{
    
    uint temp_drive;

    // Measure Smoke with IRed Current, and direct Smoke Measurement
    //   to photo_smoke_voltage
    photo_measure_smk(&photo_smoke_voltage);

    // Current measurement is in photo_ired_voltage
    temp1 = (temp1 + photo_ired_voltage);

    if(--smoke_update_count == 0)
    {
        // Reset sample counter
        smoke_update_count = 4;

        // Divide by four to get the average of the last four readings.
        // This depends on the average photo reading being less than 255.
        if(temp1 & 0x0002)
        {
            temp1 = ((temp1>>2) + 1); // Divide by 4 and round up
        }
        else
        {
            temp1 = (temp1>>2);       // Divide by 4
        }

        // Check to see if we reached the target drive level
        // We need to perform subtraction (not a compare) to use any error
        // value to set the next drive value
        if(FLAG_FIND_LOW_DRIVE_ACTIVE)
        {
            if(SMK_LOW_DRIVE_CTS <= temp1)
            {
                // Drive target reached
                photo_send_irled_data();
                photo_drive_low = ired_pulse_width;

                temp1 = 0;
                FLAG_FIND_DRIVE_ERROR = 0;
                return TRUE;
            }
            else
            {
                // Drive target not reached, determine error
                low_cal_vout.byte.msb = (SMK_LOW_DRIVE_CTS - temp1);
            }
        }
        else
        {
            if(SMK_HIGH_DRIVE_CTS <= temp1)
            {
                // Drive target reached
                photo_send_irled_data();
                photo_drive_high = ired_pulse_width;

                temp1 = 0;
                FLAG_FIND_DRIVE_ERROR = 0;
                
                return TRUE;

            }
            else
            {
                // Drive target not reached, determine error
                low_cal_vout.byte.msb = (SMK_HIGH_DRIVE_CTS - temp1);
            }
        }

        // Using the error, Set the next drive Pulse Width value
        if(low_cal_vout.byte.msb & 0x0001)
        {
            // divide by 2 and round
            low_cal_vout.byte.msb = ((low_cal_vout.byte.msb >> 1) +1);
        }
        else
        {
            // divide by 2
            low_cal_vout.byte.msb = (low_cal_vout.byte.msb >> 1);
        }

        if(low_cal_vout.byte.msb & 0x0001)
        {
            // divide by 2 and round
            low_cal_vout.byte.msb = ((low_cal_vout.byte.msb >> 1) +1);
        }
        else
        {
            // divide by 2
            low_cal_vout.byte.msb = (low_cal_vout.byte.msb >> 1);
        }

        // Increase amount of drive for next pass
        temp_drive = (uint) high_cal_vout.byte.msb + 
                     (uint)low_cal_vout.byte.msb;

        if(temp_drive > MAX_IRED_DRIVE)
        {
            // Overflow, set error flag
            FLAG_FIND_DRIVE_ERROR = 1;
        }
        else
        {
            // We reached here because drive target was not yet found
            high_cal_vout.byte.msb = (uchar)temp_drive;
            PHOTO_Set_Drive_Pulse_Width(high_cal_vout.byte.msb);

        }

        // Reset Drive sense value register
        temp1 = 0;
    }

    return FALSE;
}


/*
+------------------------------------------------------------------------------
| Function:      photo_temp_ror_test
+------------------------------------------------------------------------------
| Purpose:       Tests current Temp. Rate of Rise against the Active Temp
|                   ROR Trigger Threshold (wTMP_ROR_Trigger)
|
|                 Inputs:	wTMPAverageROR holds current Rate of Rise Average
|                           wTMP_ROR_Trigger holds ROR trigger value
|                 Outputs:  bALG_Status flag updated (FLAG_TRR_ACTIVE)
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void photo_temp_ror_test(void)
{
    // Compare Trigger against average ROR
    if(ALG_TMPAverageROR > ALG_TMP_ROR_Trigger)
    {
        // Trigger is set
        FLAG_TRR_ACTIVE = 1;
    }
    else
    {
        // Trigger is cleared
        FLAG_TRR_ACTIVE = 0;    
    }
}


/*
+------------------------------------------------------------------------------
| Function:      photo_smk_ror_test
+------------------------------------------------------------------------------
| Purpose:       Tests current Smoke Rate of Rise against the Active Temp
|                   ROR Trigger Threshold (wSMK_ROR_Trigger)
|
|                 Inputs:	wSMKAverageROR holds current Rate of Rise Average
|                           wSMK_ROR_Trigger holds ROR trigger value
|                 Outputs:  bALG_Status flag updated (FLAG_SRR_ACTIVE)
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void photo_smk_ror_test(void)
{
    // Compare Trigger against average ROR
    if(ALG_SMKAverageROR > ALG_SMK_ROR_Trigger)
    {
        // Trigger is set
        FLAG_SRR_ACTIVE = 1;
    }
    else
    {
        // Trigger is cleared
        FLAG_SRR_ACTIVE = 0;
    }

}


#ifdef CONFIGURE_CO
/*
+------------------------------------------------------------------------------
| Function:      photo_co_ror_test
+------------------------------------------------------------------------------
| Purpose:       Tests current CO Rate of Rise against the Active Temp
|                   ROR Trigger Threshold (wCO_ROR_Trigger)
|
|                 Inputs:	wCOAverageROR holds current Rate of Rise Average
|                           wCO_ROR_Trigger holds ROR trigger value
|                 Outputs:  bALG_Status flag updated (FLAG_CRR_ACTIVE)
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void photo_co_ror_test(void)
{
    // Compare Trigger against average ROR
    if(ALG_COAverageROR > ALG_CO_ROR_Trigger)
    {
        // Trigger is set
        FLAG_CRR_ACTIVE = 1;
    }
    else
    {
        // Trigger is cleared
        FLAG_CRR_ACTIVE = 0;
    }
}


/*
+------------------------------------------------------------------------------
| Function:      photo_co_output_test
+------------------------------------------------------------------------------
| Purpose:       Tests current CO PPM measurement against the Active CO
|                   PPM Trigger Threshold (wCO_PPM_Trigger)
|
|                 Inputs:	CO_wPPM holds current CO PPM
|                           ALG_CO_PPM_Trigger holds PPM trigger value
|                 Outputs:  bALG_Status flag updated (FLAG_CO_PPM_ACTIVE)
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void photo_co_output_test(void)
{
    // Compare Trigger against average ROR
    if(CO_wPPM > (uint)(ALG_CO_PPM_Trigger + ALG_CO_Offset))
    {
        // Trigger is set
        FLAG_CO_PPM_ACTIVE = 1;
    }
    else
    {
        // Trigger is cleared
        FLAG_CO_PPM_ACTIVE = 0;
    }
}

#endif


/*
+------------------------------------------------------------------------------
| Function:      PHOTO_Hush_Too_High
+------------------------------------------------------------------------------
| Purpose:       Test to see if Smoke Hush is allowed 
|                (i.e.  not too much Smoke)
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  TRUE if Smoke above Hush Level, otherwise FALSE
+------------------------------------------------------------------------------
*/

uchar PHOTO_Hush_Too_High(void)
{
    if(PHOTO_Reading >= (PHOTO_Alarm_Thold + ALG_SMK_HUSH_FIXED) )
    {
        // Smoke above Hush Level
        return TRUE;
    }
    else
    {
       // Smoke below Hush Level 
       return FALSE;
    }
}


/*
+------------------------------------------------------------------------------
| Function:      photo_smk_output_test
+------------------------------------------------------------------------------
| Purpose:       Tests current Smoke Output level against the Active Smoke
|                 Alarm Trigger Threshold (bSMK_Alarm_Trigger)
|
|                Inputs:	bPhotoReading holds current Smoke Level
|                Outputs:   bALG_Status smoke flags updated
|                           (FLAG_ALG_ALM_DETECT, FLAG_ALG_SMK_DETECT, 
|                            FLAG_SMK_DELTA_ACTIVE)
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void photo_smk_output_test(void)
{
    uchar TestTh;

    // Test Alarm Detect Trigger
    // This is the Programmable Alarm Trigger (state dependant)
    if(PHOTO_Reading >= ALG_SMK_Alarm_Trigger)
    {
        FLAG_ALG_ALM_DETECT = 1;
    }
    else
    {
        FLAG_ALG_ALM_DETECT = 0;
    }
    
    // During PTT, test for a percentage of Calibrated Smoke Alarm Threshold
    if(FLAG_PTT_ACTIVE)
    {
        // For Test, test smoke output has risen above
        // 50% of CAV  to Alarm delta
        TestTh = (PHOTO_Alarm_Thold - PHOTO_Cav_Average.byte.msb);

        TestTh = TestTh >> 1;
        TestTh = (PHOTO_Alarm_Thold - TestTh);

        if(PHOTO_Reading > TestTh)
        {
            FLAG_CAL_ALM_DETECT = 1;
        }
        else
        {
            FLAG_CAL_ALM_DETECT = 0;
        }
    }
    else
    {
        // Test Calibrated Alarm Detect Trigger
        // This is the Standard Normal Alarm Trigger (fixed)
        if(PHOTO_Reading >= PHOTO_Alarm_Thold)
        {
            FLAG_CAL_ALM_DETECT = 1;
        }
        else
        {
            FLAG_CAL_ALM_DETECT = 0;
        }
    }

    // Test Delta Detect Trigger
    // This tracks Rise increase from previous measurement
    if(ALG_SMKRiseDelta > ALG_SMK_Delta_Trigger)
    {
        FLAG_SMK_DELTA_ACTIVE = 1;
    }
    else
    {
         FLAG_SMK_DELTA_ACTIVE = 0;
    }

    // Test Smoke Detect Trigger
    // This is set about 1.5 - 3.0 uamps above CAV (looking for any smoke)
    if(PHOTO_Reading > (PHOTO_Cav_Average.byte.msb + ALG_TRIGGER_SMK) )
    {
        FLAG_ALG_SMK_DETECT = 1;
    }
    else
    {
         FLAG_ALG_SMK_DETECT = 0;
    }
}


/*
+------------------------------------------------------------------------------
| Function:      PHOTO_Init_CAV_Avg
+------------------------------------------------------------------------------
| Purpose:       Initialize Smoke CAV average
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void PHOTO_Init_CAV_Avg(void)
{
    PHOTO_Cav_Average.byte.msb = SYS_RamData.Smk_Cav_At_Cal;
    PHOTO_Cav_Average.byte.lsb = 0;
}


/*
+------------------------------------------------------------------------------
| Function:      photo_smk_chamber_test
+------------------------------------------------------------------------------
| Purpose:       Tests current Smoke Output level against the Fault Threshold
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  TRUE or FALSE  (FALSE if Smoke Chamber Fault)
+------------------------------------------------------------------------------
*/

uchar photo_smk_chamber_test(void)
{
    // Compare last Smoke reading to the fault threshold.  
    // If less than threshold, Log a Smoke Chamber fault and return FALSE
    if(SMK_FAULT_THRESHOLD >= PHOTO_Reading)
    {
        // Log A Chamber fault
        FLT_Fault_Manager(FAULT_PHOTOCHAMBER);
        return FALSE;
    }
    else
    {
        // Log a Chamber Success (this allows fault recovery)
        FLT_Fault_Manager(SUCCESS_PHOTOCHAMBER);
        return TRUE;

    }
}


/*
+------------------------------------------------------------------------------
| Function:      PHOTO_Compute_CAV_Avg
+------------------------------------------------------------------------------
| Purpose:       Computes the running average of the Smoke Chamber CAV
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void PHOTO_Compute_CAV_Avg(void)
{
    uchar blocal;

    // Not allowed during fault or no smoke cal.
    if( !FLAG_SMOKE_ERROR_PENDING && !FLAG_FAULT_FATAL && 
         !FLAG_NO_SMOKE_CAL )
    {
        /*
         * On powerup bPhotoReading will be zero, we don't want to include
         * that in the running average.
         */
        if(PHOTO_Reading != 0)
        {
            if(SMK_STANDBY == photo_state)
            {
                /*
                 * CAV Average is initialized after POR or Test Reset
                 * so no initialization is required here.
                 */
                
                //Test if Accelerated AVG Init Cycle is in Progress
                if(!FLAG_INIT_CAV_AVG)
                {
                    // Test for Accelerated Averaging?
                    photo_dust_shift_detect();
                }

                /*
                 * Verify that current Reading is within
                 * Limits before including in CAV Average.
                 * Reading - CAV Average < MAX Counts
                 */
                blocal = PHOTO_Reading;     // Save in Local Variable

                if(PHOTO_Cav_Average.byte.msb < PHOTO_Reading)
                {
                    // Reading > CAV Average, Test for Maximum Increase Limit
                    // get the amount of increase over current CAV average
                    if((PHOTO_Reading - PHOTO_Cav_Average.byte.msb) > 
                                CAV_INCREASE_LIMIT )
                    {
                        // Limit amount of CAV correction at this time
                        blocal = (PHOTO_Cav_Average.byte.msb + 
                                CAV_INCREASE_LIMIT );

                    }
                }
                else
                {
                    // Reading < CAV Average, Test for Maximum Decrease Limit
                    // get the amount of decrease over current CAV average
                    if((PHOTO_Cav_Average.byte.msb - PHOTO_Reading) > 
                                CAV_DECREASE_LIMIT )
                    {
                        // Limit amount of CAV correction at this time
                        blocal = (PHOTO_Cav_Average.byte.msb - 
                                CAV_DECREASE_LIMIT);

                    }
                }

                // Local variable holds CAV value to use for running average
                // Use the 16 bit averaging routine to maintain the 8 bit 
                //  CAV average
                PHOTO_Cav_Average.word = 
                        ALG_Running_Average(blocal, PHOTO_Cav_Average.word);

                // Test for CAV Initialization Completed
                if(FLAG_INIT_CAV_AVG)
                {
                    #ifdef	CONFIGURE_TEST_SMK_COMP                        
                        SER_Send_String("Drift Comp Countdown - ");
                        SER_Send_Int(' ',(PHOTO_Cav_Timer-1),10);
                        SER_Send_Prompt();
                    #endif                        


                    if(0 == --PHOTO_Cav_Timer)
                    {
                        // Correct Alarm Threshold at end of Init. cycle
                        FLAG_INIT_CAV_AVG = 0;
                        PHOTO_Compensate_Alm_Thold();
                        return;
                    }
                }

                photo_compensate_alm_serial_out();


            }
        }
    }
}


/*
+------------------------------------------------------------------------------
| Function:     photo_dust_shift_detect
+------------------------------------------------------------------------------
| Purpose:      Test for possible Dust Clean Shift 
|                (Chamber's CAV drops significantly)
|
|               Compare CAV running average (long term IRED Degradation/Dust  
|                average) against the PhotoReading running average. (updated       
|                every Standby Measurement)  
|               If (CAV AVG - PhotoReading AVG) > Chamber Shift Threshold
|                then Set FLAG_CAV_INIT to start a CAV Average initialization 
|                cycle
| 
|               NOTE:
|               Increased SMK_CHAMBER_SHIFT_TH value so Temp Cycling tests 
|                would not Trigger A Dust Clean Cycle and move Alarm Threshold 
|                during the Test. This was putting High Sensitity units into 
|                Alarm during test or when test was complete.
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  TRUE or FALSE (TRUE if Dust Shift Detected)
+------------------------------------------------------------------------------
*/

uchar photo_dust_shift_detect(void)
{
    if( PHOTO_Cav_Average.byte.msb >= ALG_PhotoReadingAvg.byte.msb)
    {
        // Test amount of CAV Shift down from CAV Average
        if( (PHOTO_Cav_Average.byte.msb - ALG_PhotoReadingAvg.byte.msb ) > 
                SMK_CHAMBER_SHIFT_TH)
        {
            // Dust clean shift detected
            FLAG_INIT_CAV_AVG = 1;  // Begin CAV Initialization Cycle
            PHOTO_Cav_Timer = INIT_CAV_AVG_TIME;    // Initialization Time

            return TRUE;
        }
    }
    // No Dust Clean Shift detected
    return FALSE;
}


/*
+------------------------------------------------------------------------------
| Function:      PHOTO_Compensate_Alm_Thold
+------------------------------------------------------------------------------
| Purpose:       Compensates the alarm threshold for changes in clean air 
|                 voltage.
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void PHOTO_Compensate_Alm_Thold(void)
{
     #ifdef	CONFIGURE_DRIFT_COMP

        uchar alarm_temp;
    
        if(!FLAG_SMOKE_ERROR_PENDING && !FLAG_FAULT_FATAL)
        {
            #ifndef CONFIGURE_TEST_SMK_COMP
                // Not Allowed if ALG disabled (except drift test versions)
                if(!FLAG_ALG_ENABLED)
                {
                    return;
                }
            #endif

            if(SMK_STANDBY == photo_state)
            {
                // Compare Calibrated CAV against Average CAV
                if(SYS_RamData.Smk_Cav_At_Cal > PHOTO_Cav_Average.byte.msb)
                {
                    // CAL > Average, CAV average drifted lower
                    /*
                     * find the difference between the CAV at calibration and 
                     * the current running average.  Multiply it by 
                     * SMK_ALARM_COMP_SCALE_8_8 and subtract the
                     * result from the alarm threshold at calibration.
                     */

                    // Sensitivity has dropped - this is typically for IRED 
                    //  degradation
                    uchar delta = (SYS_RamData.Smk_Cav_At_Cal - 
                                   PHOTO_Cav_Average.byte.msb);
                    
                    if(PHOTO_Cav_Average.byte.lsb & 0x80)
                    {
                        // Round if Needed
                        delta--;
                    }
                    
                    // 50 % of CAV to Alarm Threshold
                    uchar limit_delta = (SYS_RamData.Smk_Alarm_TH_At_Cal -
                                            SYS_RamData.Smk_Cav_At_Cal) >> 1;
                    
                     // Default to no Previous Correction Used
                    uchar prev_correction_used = 0;
                
                    //Calculate amount of Alarm THold Correction
                    uchar correction = photo_comp_alm_compute_corr(delta);
                    
                    // Determine amount of the total correction needed
                    //  that we may have already already compensated for
                    if(SYS_RamData.Smk_Corrected_Alm_TH < 
                            SYS_RamData.Smk_Alarm_TH_At_Cal)
                    {
                        prev_correction_used = 
                                (SYS_RamData.Smk_Alarm_TH_At_Cal -
                                 SYS_RamData.Smk_Corrected_Alm_TH);
                    }

                    // Test Only
                    #ifdef CONFIGURE_TEST_SMK_COMP
                        SER_Send_String("delta =");
                        SER_Send_Int(' ',delta,10);
                        SER_Send_String(" total corr =");
                        SER_Send_Int(' ',correction,10);
                    #endif
                    
                    // Determine amount of Correction to use this compensation 
                    // pass
                    if(correction >= prev_correction_used )
                    {
                        // Subtract any previously used compensation
                        correction = (correction - prev_correction_used);
                        
                        // Limit compensation if needed
                        correction = photo_compensate_alm_chk_limit(correction);
                        
                    }
                    else if( limit_delta > 
                            (prev_correction_used - correction) )
                    {
                        // For slight drops in Correction, jump straight to 
                        // Corrected alarm Threshold
                        
                        // Zero Out Previous Corrected and Use Full Corrected
                        //  with no limits
                         prev_correction_used = 0;
                    }
                    else
                    {
                        // Re-Calculate total correction from Calibrated CAV
                        //  if correction is significantly less than 
                        //  previously used.
                        prev_correction_used = 0;
                        
                        // Limit compensation if needed
                        correction = photo_compensate_alm_chk_limit(correction);
                    }

                    // Test Only
                    #ifdef CONFIGURE_TEST_SMK_COMP
                        SER_Send_String(" prev =");
                        SER_Send_Int(' ',prev_correction_used,10);
                        SER_Send_String(" corr =");
                        SER_Send_Int(' ',correction,10);
                        SER_Send_Prompt();
                    #endif
                    
                    //Sub Alarm Compensation from Calibrated Alarm threshold
                    alarm_temp = (SYS_RamData.Smk_Alarm_TH_At_Cal - 
                            correction - prev_correction_used);
                    
                    // If Alarm Threshold is below allowed LOW Limit
                    //  set limit, and enter Degrad Compensation Fault
                    if(alarm_temp <= SMK_ALARM_MIN_LIMIT)
                    {
                        alarm_temp = SMK_ALARM_MIN_LIMIT;
                        
                        // We are at maximum allowed compensation
                        //  so log a Compensation Fault
                        // Report Compensation Limit exceeded 
                        FLT_Fault_Manager(FAULT_SMK_DRIFT_COMP);
                        
                    }
                    else
                    {
                        // Report Compensation Limit OK
                        FLT_Fault_Manager(SUCCESS_SMK_DRIFT_COMP);

                    }
                }
                else if(SYS_RamData.Smk_Cav_At_Cal != 
                                    PHOTO_Cav_Average.byte.msb)
                {
                    // CAL < Average, CAV averaged drifted higher

                    /*
                     * find the difference between the CAV at calibration 
                     * and the current running average.  Multiply it by 
                     * SMK_ALARM_COMP_SCALE_8_8 and subtract the
                     * result from the alarm threshold at calibration.
                     *
                     * Sensitivity has increased - this is typically for 
                     * Dust Drift Compensation
                     */
                    uchar delta = (PHOTO_Cav_Average.byte.msb - 
                                    SYS_RamData.Smk_Cav_At_Cal);

                    if(PHOTO_Cav_Average.byte.lsb & 0x80)
                    {
                        // Round if Needed
                        delta++;
                    }
                    
                    // 50 % of CAV to Alarm Threshold
                    uchar limit_delta = (SYS_RamData.Smk_Alarm_TH_At_Cal -
                                            SYS_RamData.Smk_Cav_At_Cal) >> 1;
                    
                    //Calculate amount of Alarm THold Correction
                    uchar correction = photo_comp_alm_compute_corr(delta);

                    // Default to no Correction Used
                    uchar prev_correction_used = 0;
                
                    // Determine amount of the total correction needed
                    //  that we may have already already compensated for
                    if(SYS_RamData.Smk_Corrected_Alm_TH > 
                            SYS_RamData.Smk_Alarm_TH_At_Cal)
                    {
                        prev_correction_used =
                                (SYS_RamData.Smk_Corrected_Alm_TH -
                                 SYS_RamData.Smk_Alarm_TH_At_Cal);
                    }

                    // Test Only
                    #ifdef CONFIGURE_TEST_SMK_COMP
                        SER_Send_String("delta =");
                        SER_Send_Int(' ',delta,10);
                        SER_Send_String(" total corr =");
                        SER_Send_Int(' ',correction,10);
                    #endif

                    // Determine amount of Correction to use this compensation 
                    // pass
                    if(correction >= prev_correction_used )
                    {
                        // Subtract any previously used compensation
                        correction = (correction - prev_correction_used);
                        
                        // Limit compensation if needed
                        correction = photo_compensate_alm_chk_limit(correction);
                        
                    }
                    else if( limit_delta > 
                            (prev_correction_used - correction) )
                    {
                        // For slight drops in Correction, correct directly to 
                        //  proper Corrected Alarm Threshold
                        
                        // Zero Out Previous Corrected and Use Full Corrected
                        //  with no limits
                         prev_correction_used = 0;
                    }
                    else
                    {
                        // Re-Calculate total correction from Calibrated CAV
                        //  if correction is significantly less than 
                        //  previously used.
                        prev_correction_used = 0;
                        
                        // Limit compensation if needed
                        correction = photo_compensate_alm_chk_limit(correction);
                    }
         
                    // Test Only
                    #ifdef CONFIGURE_TEST_SMK_COMP
                        SER_Send_String(" prev =");
                        SER_Send_Int(' ',prev_correction_used,10);
                        SER_Send_String(" corr =");
                        SER_Send_Int(' ',correction,10);
                        SER_Send_Prompt();
                    #endif

                    //Add Alarm Compensation to Calibrated Alarm threshold
                    alarm_temp = (SYS_RamData.Smk_Alarm_TH_At_Cal + 
                                    correction + prev_correction_used);

                    // If New alarm Threshold Wrapped thru 0, then limit it)
                    if(alarm_temp < SYS_RamData.Smk_Alarm_TH_At_Cal)
                    {
                        // Limit Compensated Alarm Threshold to max.
                        alarm_temp = SMK_ALARM_MAX_LIMIT;

                    }

                    // If Alarm Threshold is above allowed HIGH Limit
                    //  set limit, and enter Drift Compensation Fault
                    if(alarm_temp >= SMK_ALARM_MAX_LIMIT)
                    {
                        alarm_temp = SMK_ALARM_MAX_LIMIT;

                        // We are at maximum allowed compensation
                        //  so log a Compensation Fault
                        // Report Compensation Limit exceeded 
                        FLT_Fault_Manager(FAULT_SMK_DRIFT_COMP);

                        #ifdef CONFIGURE_DUST_FAULT_BEEPS_OFF
                            // For Dust Comp Fault, turn off 30 second 
                            //  Fault Beeps
                            FLAG_FAULT_BEEP_INHIBIT = 1;                      
                        #endif

                    }
                    else
                    {
                        // Report Compensation Limit OK
                        FLT_Fault_Manager(SUCCESS_SMK_DRIFT_COMP);

                    }

                }
                else
                {
                   // Clean Air at Cal = CAV Average, simply reset Threshold
                   //  back to original calibrated value 
                   alarm_temp =  SYS_RamData.Smk_Alarm_TH_At_Cal;
                }

                // Set new corrected Alarm Threshold
                PHOTO_Alarm_Thold = alarm_temp;

                // Perform Checksum Verification to insure we're
                //  not Saving corrupted data since last Verification
                if(FALSE == SYS_Validate_Data_Structure())
                {
                    // Reset to restore Data structure and Test for
                    //  Memory Errors
                    SOFT_RESET
                }

                SYS_RamData.Smk_Corrected_Alm_TH = alarm_temp;
                SYS_Save();

                #ifdef  CONFIGURE_DIAG_HIST
                    DIAG_Hist_Record_Avg_CAV(PHOTO_Cav_Average.byte.msb);
                #endif

                // Serial Output
                photo_compensate_alm_serial_out();

            }
        }

     #endif
}

/*
+------------------------------------------------------------------------------
| Function:      photo_compensate_alm_serial_out
+------------------------------------------------------------------------------
| Purpose:       Output Compensation Serial Output
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void photo_compensate_alm_serial_out(void)
{
    #ifdef DBUG_SERIAL_OUTPUT_COMP_CAV
        #ifdef  CONFIGURE_SERIAL_PORT_ENABLED
            if(SERIAL_ENABLE_SMOKE)
            {
                // Send CAV average
                SER_Send_String("Ca");
                SER_Send_Int('v', (uint)PHOTO_Cav_Average.byte.msb, 10);
                if(FLAG_INIT_CAV_AVG)
                {
                    // Send Cycle Initialization
                    SER_Send_Char('*');
                }
                
                // Send CAL Alarm TH
                SER_Send_String(",A");
                SER_Send_Int('c', (uint)SYS_RamData.Smk_Alarm_TH_At_Cal, 10);
                
                // Send corrected Alarm TH
                SER_Send_String(",A");
                SER_Send_Int('t', (uint)SYS_RamData.Smk_Corrected_Alm_TH, 10);

                SER_Send_Prompt();
            }
        #endif
    #endif
}


/*
+------------------------------------------------------------------------------
| Function:      photo_comp_alm_compute_corr
+------------------------------------------------------------------------------
| Purpose:       Compute amount of Drift Correction
|
|                Multiply CAV Delta by SMK_ALARM_COMP_SCALE_8_8 and
|                return the result.
|
|
+------------------------------------------------------------------------------
| Parameters:    CavDelta holds CAV delta
+------------------------------------------------------------------------------
| Return Value:  return computed correction
+------------------------------------------------------------------------------
*/

uchar photo_comp_alm_compute_corr(uchar CavDelta)
{
    if( CavDelta > SMK_CAV_DELTA_MAX )
    {
        // Limit amount of correction this routine can calculate
        // to prevent any math overflow
        CavDelta = SMK_CAV_DELTA_MAX;
    }
    
    smk_set_cal_corr_slope();
    
    uint MathB = (CavDelta<<8);
    // Get the Fractional component
    uint MathA = (uint)photo_get_x();
    
    // Add the Interger component (1.0) 
    // Current Data shows At/CAV = about 1.6
    MathA = MathA | 0x0100;

    unsigned long MathC = (unsigned long)MathA * MathB;
    // Round Up if needed
    MathC += 0x8000;

    MathC = (MathC >> 16);
    MathA = (uint)MathC;

    return (uchar)MathA;
}


/*
+------------------------------------------------------------------------------
| Function:      photo_compensate_alm_chk_limit
+------------------------------------------------------------------------------
| Purpose:       Determine Limit value (This is limited to 50% of the 
|                 calibrated AT-CAV value)
|
|
+------------------------------------------------------------------------------
| Parameters:    alm_delta has delta change to the Alarm Threshold
+------------------------------------------------------------------------------
| Return Value:  return allowed compensated amount
+------------------------------------------------------------------------------
*/

uchar photo_compensate_alm_chk_limit(uchar alm_delta)
{

    uchar delta = (SYS_RamData.Smk_Alarm_TH_At_Cal - 
                   SYS_RamData.Smk_Cav_At_Cal);
    
    // Divide by 2 to get 50% of (CAV to Alarm TH delta)
    delta = delta>>1;

    if(alm_delta > delta)
    {
        // Limit Change for this 24 hour period
        return delta;
    }
    else
    {
        // Change OK
        return alm_delta;
    }
}


/*
+------------------------------------------------------------------------------
| Function:      photo_get_x
+------------------------------------------------------------------------------
| Purpose:       Table lookup of Smoke Correction Multiply Factor
|                These values based upon tested Smoke Chamber calibration data
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  multiply factor from table lookup
+------------------------------------------------------------------------------
*/

uchar photo_get_x(void)
{
#ifdef CONFIGURE_UNDEFINED
    // This SMK_X_TABLE was used by DC Worryfree Models
    //  for UL Testing when we set High/Low Sensitivity units artificially
    //  It should no longer be needed
    
    // CAL Alarm Threshold - CAL CAV value
    uchar temp = (SYS_RamData.Smk_Alarm_TH_At_Cal - SYS_RamData.Smk_Cav_At_Cal);

    // Limit to max of 127
    temp = (temp & 0x7F);

    // divide Delta by Table Index Resol. (=16)
    temp = (temp >> 4);

    return SMK_X_TABLE[temp];
    
#else
    
    #ifdef CONFIGURE_USE_CALCULATED_COMP_SLOPE
        // Use if within range 1.2 - 1.98
        if( (smk_drift_comp_factor > SMK_COMP_SLOPE_1P2 ) && 
            (smk_drift_comp_factor < SMK_COMP_SLOPE_1P98 ) )
        {
            return smk_drift_comp_factor;
        }
        else
        {
            // Mult default factor fron Measured smoke Calibration Data
            return (SMK_MULT_1_6);
        }
    #else
        // Mult factor fron Measured smoke Calibration Data
        return (SMK_MULT_1_6);
    #endif
    
#endif
}


/*
+------------------------------------------------------------------------------
| Function:      smk_set_cal_corr_slope
+------------------------------------------------------------------------------
| Purpose:       Calculate Drift Comp factor to adjust AT 
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void smk_set_cal_corr_slope(void)
{
    uint math1 = (SYS_RamData.Smk_Alarm_TH_At_Cal * 256);
    uint math2 = (math1 / SYS_RamData.Smk_Cav_At_Cal);
    
    smk_drift_comp_factor = (uchar)math2;
    
    #ifndef CONFIGURE_UNDEFINED
        SER_Send_String("Drift Comp Slope =");
        SER_Send_Int(' ',smk_drift_comp_factor,16);
        SER_Send_Prompt();
    #endif
}


/*
+------------------------------------------------------------------------------
| Function:      smk_set_cav_corr_limits
+------------------------------------------------------------------------------
| Purpose:       Calculate min and max correction limits for CAV
|                using smoke calibration values
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void smk_set_cav_corr_limits(void)
{
    uint math1;
    uint math2;
    
    // Determine the slope used for this alarm's smoke chamber
    smk_set_cal_corr_slope();
    
    // Get Slope of this chamber
    math2 = smk_drift_comp_factor;
    math2 = math2 | 0x100;
    
    // Determine Upper CAV Limit
    uint math1 = ((SMK_ALARM_MAX_LIMIT * 256) + 0x80);
    PHOTO_Comp_Max_Cav = (math1 / math2);
    
    // Determine Lower CAV Limit
    math1 = ((SMK_ALARM_MIN_LIMIT * 256) + 0x80);
    PHOTO_Comp_Min_Cav = (math1 / math2);
    
    #ifndef CONFIGURE_UNDEFINED
        SER_Send_String("CAV Max Limit =");
        SER_Send_Int(' ',PHOTO_Comp_Max_Cav,16);
        SER_Send_Prompt();
        SER_Send_String("CAV Min Limit =");
        SER_Send_Int(' ',PHOTO_Comp_Min_Cav,16);
        SER_Send_Prompt();
    #endif
    
}
