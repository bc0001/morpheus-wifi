/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        alarmprio.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  State machine for determining the priority of alarms.
|
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/



/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include    "common.h"
#include    "alarmprio.h"
#include    "comeasure.h"
#include    "diaghist.h"
#include    "app.h"


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

#define ALARM_PRIORITY_INTERVAL_MS      (uint)100
#define ALARM_PRIORITY_INTERVAL_10_MS   (uint)10

#define ALM_RESCHEDULE_TIME_100_MS      (uint)100

// Return Times in ms
#define ALM_6_SEC_RETURN_TIME           (uint)6000
#define ALM_4_SEC_RETURN_TIME           (uint)4000
#define ALM_500_MS_RETURN_TIME          (uint)500

#define ALM_1_SEC_RETURN_TIME           (uint)1000
#define ALM_2_SEC_RETURN_TIME           (uint)2000

#define ALM_10_SEC_RETURN_TIME          (uint)10000
#define ALM_12_SEC_RETURN_TIME          (uint)12000
#define ALM_16_SEC_RETURN_TIME          (uint)16000

#ifdef	CONFIGURE_CO_ALM_BAT_CONSERVE
    // 4 minutes into Alarm
    #define TIMER_BAT_CONSERVE_4_MIN	(uchar)4
    #define TIMER_BAT_CONSERVE_1_MIN    (uchar)1

    #define TIMER_ALM_OFF_90_SECS       (uint)900
    #define TIMER_ALM_OFF_15_SECS       (uint)150
#endif
#ifdef	CONFIGURE_WIRELESS_ALARM_LOCATE
    // 2 minutes Alarm Locate (Local Alarm Only)
    #define TIMER_LOCATE_2_MIN          (uchar)120
    #define TIMER_LOCATE_110_SECS       (uchar)110
    #define TIMER_LOCATE_20_SEC         (uchar)20
#endif

    #define TIMER_CO_OFF_40_SECS            (uchar)40
    #define TIMER_CO_OFF_35_SECS            (uchar)35

#ifdef CONFIGURE_WIRELESS_MODULE
    // Slave/Remote Alarm Second timers
    #define TIMER_CO_SLAVE_ALARM_TIMEOUT    (uchar)12
    #define TIMER_SMK_SLAVE_ALARM_TIMEOUT   (uchar)12
#else
    // Slave/Remote Alarm Second timers
    #define TIMER_CO_SLAVE_ALARM_TIMEOUT    (uchar)10
    #define TIMER_SMK_SLAVE_ALARM_TIMEOUT   (uchar)10
#endif

    #define TIMER_LOC_SLAVE_ALARM_TIMEOUT   (uchar)17

#ifdef CONFIGURE_WIFI
    //
    // For WIFI we should not need these delays
    //
    #define TIMER_MASTER_SLAVE_DELAY_12_SEC (uchar)1
    #define TIMER_SLAVE_TO_REMOTE_DLY_12_SEC (uchar)1
    #define TIMER_SLAVE_TO_REMOTE_DLY_14_SEC (uchar)1

    #define TIMER_SMK_ALM_CLEAR_12_SECS     (uint)120    // 100 ms units
    #define TIMER_SMK_ALM_CLEAR_15_SECS     (uint)50     // 100 ms units
    #define TIMER_SMK_ALM_CLEAR_20_SECS     (uint)10     // 100 ms units
    #define TIMER_SMK_ALM_CLEAR_22_SECS     (uint)10     // 100 ms units
    #define TIMER_SMK_ALM_CLEAR_25_SECS     (uint)10     // 100 ms units
    #define TIMER_SMK_ALM_CLEAR_30_SECS     (uint)10     // 100 ms units
    #define TIMER_SMK_ALM_CLEAR_32_SECS     (uint)10     // 100 ms units
    #define TIMER_SMK_ALM_CLEAR_35_SECS     (uint)10     // 100 ms units
    #define TIMER_SMK_ALM_CLEAR_40_SECS     (uint)10     // 100 ms units
    #define TIMER_SMK_ALM_CLEAR_45_SECS     (uint)10     // 100 ms units

    #define TIMER_REMOTE_INHIBIT_4_SECS     (uchar)1
    #define TIMER_REMOTE_INHIBIT_5_SECS     (uchar)1
    #define TIMER_REMOTE_INHIBIT_15_SECS    (uchar)1
    #define TIMER_REMOTE_INHIBIT_20_SECS    (uchar)1
    #define TIMER_REMOTE_INHIBIT_30_SECS    (uchar)1
    #define TIMER_REMOTE_INHIBIT_40_SECS    (uchar)1

#else
    #define TIMER_MASTER_SLAVE_DELAY_12_SEC (uchar)12
    #define TIMER_SLAVE_TO_REMOTE_DLY_12_SEC (uchar)12
    #define TIMER_SLAVE_TO_REMOTE_DLY_14_SEC (uchar)14

    #define TIMER_SMK_ALM_CLEAR_12_SECS     (uint)120     // 100 ms units
    #define TIMER_SMK_ALM_CLEAR_15_SECS     (uint)150     // 100 ms units
    #define TIMER_SMK_ALM_CLEAR_20_SECS     (uint)200     // 100 ms units
    #define TIMER_SMK_ALM_CLEAR_22_SECS     (uint)220     // 100 ms units
    #define TIMER_SMK_ALM_CLEAR_25_SECS     (uint)250     // 100 ms units
    #define TIMER_SMK_ALM_CLEAR_30_SECS     (uint)300     // 100 ms units
    #define TIMER_SMK_ALM_CLEAR_32_SECS     (uint)320     // 100 ms units
    #define TIMER_SMK_ALM_CLEAR_35_SECS     (uint)350     // 100 ms units
    #define TIMER_SMK_ALM_CLEAR_40_SECS     (uint)400     // 100 ms units
    #define TIMER_SMK_ALM_CLEAR_45_SECS     (uint)450     // 100 ms units

    #define TIMER_REMOTE_INHIBIT_4_SECS     (uchar)4
    #define TIMER_REMOTE_INHIBIT_5_SECS     (uchar)5
    #define TIMER_REMOTE_INHIBIT_15_SECS    (uchar)15
    #define TIMER_REMOTE_INHIBIT_20_SECS    (uchar)20
    #define TIMER_REMOTE_INHIBIT_30_SECS    (uchar)30
    #define TIMER_REMOTE_INHIBIT_40_SECS    (uchar)40

#endif
    
    #define TIMER_SLAVE_TO_REMOTE_DLY_1_SEC  (uchar)1

    #define TIMER_SMK_RMT_ALARM_10_SEC      (uchar)10
    #define TIMER_CO_RMT_ALARM_10_SEC       (uchar)10
    #define TIMER_SMK_RMT_ALARM_12_SEC      (uchar)12
    #define TIMER_CO_RMT_ALARM_12_SEC       (uchar)12
    #define TIMER_SMK_RMT_ALARM_14_SEC      (uchar)14
    #define TIMER_CO_RMT_ALARM_14_SEC       (uchar)14
    #define TIMER_SMK_RMT_ALARM_16_SEC      (uchar)16
    #define TIMER_CO_RMT_ALARM_16_SEC       (uchar)16
    #define TIMER_SMK_RMT_ALARM_32_SEC      (uchar)32
    #define TIMER_CO_RMT_ALARM_32_SEC       (uchar)32

    #define TIMER_SMK_ALM_CLEAR_CANCEL      (uint)1          

    //
    // TIME_SMK_ALM_xx  Used for Slave PTT detect timer
    // This timer is not needed for WIFI models
    //
    #define TIME_SMK_ALM_10_SECS            (uint)100     // 100 ms units
    #define TIME_SMK_ALM_12_SECS            (uint)120     // 100 ms units
    #define TIME_SMK_ALM_15_SECS            (uint)150     // 100 ms units
    #define TIME_SMK_ALM_25_SECS            (uint)250     // 100 ms units
    #define TIME_SMK_ALM_30_SECS            (uint)300     // 100 ms units
    #define TIME_SMK_ALM_45_SECS            (uint)450     // 100 ms units
    #define TIME_SMK_ALM_60_SECS            (uint)600     // 100 ms units


/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
#ifdef	CONFIGURE_CO_ALM_BAT_CONSERVE
    static uchar alm_timer_bat_conserve = 0;
#endif

#ifdef CONFIGURE_INTERCONNECT
    static uchar alm_start_time;
    
#else
    #ifdef CONFIGURE_WIRELESS_MODULE
        static uchar alm_start_time;
    #endif
#endif

#ifdef CONFIGURE_WIRELESS_MODULE
    #ifdef CONFIGURE_INTERCONNECT
        static uchar alm_timer_remote_inhibit;
        static uchar alm_timer_master_slave;
    #endif

    #ifdef	CONFIGURE_CO_ALM_BAT_CONSERVE            
        static uchar alm_timer_wl_bat_conserve;
    #endif
    
        
        
#endif
        
#ifdef  CONFIGURE_WIRELESS_ALARM_LOCATE
    static uchar alm_timer_locate_mode;
 #endif
    
static uchar alm_state = ALM_STATE_ALARM_INIT;
static uint alm_clear_timer = 0; 

#ifdef CONFIGURE_WIRELESS_MODULE
  #ifdef CONFIGURE_INTERCONNECT
    static uint alm_smoke_alarm_time = TIME_SMK_ALM_60_SECS;
    static uint alm_timer_co_off;
  #endif
#endif

#ifdef CONFIGURE_SER_ALM_TIMER
    static uchar alm_sec_count = 1;
#endif


/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/
#ifdef	CONFIGURE_CO_ALM_BAT_CONSERVE
    // Prototype for Bat Conserve Routines
    void alm_init_bat_conserve_timer(void);
    uint alm_alarm_bat_conserve_logic(void);
    
    #ifdef CONFIGURE_WIRELESS_MODULE
        void alm_wl_bat_conserve_timer(void);
        void alm_init_wl_bat_conserv_timer(void);
    #endif
    
    
#endif
    
    
#ifdef CONFIGURE_INTERCONNECT
    void alm_init_slave_timer(void);
#else
    #ifdef CONFIGURE_WIRELESS_MODULE
        void alm_init_slave_timer(void);
    #endif
#endif

    
#ifdef CONFIGURE_WIRELESS_MODULE
  #ifdef CONFIGURE_INTERCONNECT
    void alm_init_remote_inhibit_timer(void);
    void alm_remote_check_inhibit_timer(void);
    
    void alm_CO_alarm_off_timer(void);
    void alm_init_CO_alarm_off_timer(void);
    
    
  #endif
#endif
        
#ifdef	CONFIGURE_WIRELESS_ALARM_LOCATE
    void alm_locate_timer(void);
#endif
    
void alm_init_clear_delay(uint time);


/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/


/*
+------------------------------------------------------------------------------
| Function:      ALM_Priority_Task
+------------------------------------------------------------------------------
| Purpose:       Handle Priority Control and Decisions for Master, Slave and
|                Remote Alarm Modes during Smoke and CO alarms.
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Task Return Time (in number of Task Engine cycles)
+------------------------------------------------------------------------------
*/

/******************************************************************************/
uint ALM_Priority_Task(void)
{

    #ifdef CONFIGURE_DEBUG_ALM_PRIORITY_STATES
        // Test Only
        for(uchar i = (alm_state + 1); i > 0; i--)
        {
            MAIN_TP2_On();
            PIC_delay_us(DBUG_DLY_25_USec);
            MAIN_TP2_Off();
        }
    #endif

    switch (alm_state)
    {
        case ALM_STATE_ALARM_INIT:
        {
            // Pass through this state before Alarm None to Reset 
            //  Alarm Flags to No Present Alarm Condition
            
            FLAG_CO_ALARM_ACTIVE = 0;
            FLAG_SMOKE_ALARM_ACTIVE = 0;
            
            FLAG_SLAVE_ALARM_MODE = 0;
            FLAG_SLAVE_SMOKE_ACTIVE = 0;
            FLAG_SLAVE_CO_ACTIVE = 0;
            
            // Init Interconnect Input Flags
            FLAG_INTCON_SMOKE_RECEIVED = 0;
            
            #ifndef CONFIGURE_UNDEFINED
                if(FLAG_RST_INT_CO_RCVD)
                {
                    // If we clear this flag here, we may be clearing a received
                    //  CO Message after a HW Interconnect Smoke PTT sequence
                    //  from a Combo. Only allow some alarm end conditions to
                    //  reset Received Co messages
                    FLAG_INTCON_CO_RECEIVED = 0;
                    FLAG_RST_INT_CO_RCVD = 0;
                }
            #endif
            
           
            #ifdef CONFIGURE_WIRELESS_MODULE
                #ifdef CONFIGURE_INTERCONNECT  
                    // Enable Master to Slave Out Timer for next master alarm
                    // Currently this applies to wireless Master Smoke Alarm 
                    // with HW Interconnect
                    FLAG_ALM_INT_DRV_ENABLE = 0;
                #endif
            #endif

            // Init Alarm Cancel Flag    
            FLAG_CNCL_SMK_CO_ALARM = 0;
            
            FLAG_BAT_CONSRV_MODE_ACTIVE = 0;
            
            FLAG_REMOTE_ALARM_MODE = 0;
            FLAG_REMOTE_SMOKE_ACTIVE = 0;
            FLAG_REMOTE_HW_SMOKE_ACTIVE = 0;
            FLAG_REMOTE_CO_ACTIVE = 0;
            FLAG_REMOTE_HW_CO_ACTIVE = 0;
            
            #ifdef CONFIGURE_UNDEFINED
                // Do we really want to clear Locate here... I say No
                // Allow 2 minute Locate Timer to clear Locate
                FLAG_ALARM_LOCATE_MODE = 0;
            #endif

            FLAG_GATEWAY_MASTER = 0;
            
            #ifdef CONFIGURE_WIRELESS_MODULE
                FLAG_DO_NOT_ARM = 0;

                #ifdef CONFIGURE_INTERCONNECT
                    // Init Gateway Flags

                    //  Default Enabling Slave to Remote Gateway transmission
                    FLAG_GATEWAY_SLV_TO_REMOTE_OFF = 0;

                    // Enable New Remote to Slave Gateway Timer Cycle
                    FLAG_WL_GATEWAY_ENABLE = 0;
                    
                    // Init Remote to Slave to inactive
                    FLAG_GATEWAY_REMOTE_TO_SLAVE = 0;

                    // Init Gateway INT Activity to no activity
                    FLAG_INT_ACTIVITY_DETECTED = 0;
                    FLAG_INT_CO_ACTIVITY_DETECTED = 0;
                #endif

            #endif

            alm_state = ALM_STATE_ALARM_NONE;
            
            return MAIN_Make_Return_Value(ALM_500_MS_RETURN_TIME);
        }

        case ALM_STATE_ALARM_NONE:

            // Allows Low Power Mode
            FLAG_ALM_PRIO_NOT_IDLE = 0;
            
            // Initialized Off in Alarm None
            FLAG_CNCL_SMK_CO_ALARM = 0;
            
            
            #ifdef	CONFIGURE_WIRELESS_ALARM_LOCATE
                // Update/Monitor Locate Mode Timer if Active
                alm_locate_timer();
            #endif
            
            #ifdef CONFIGURE_CO_ALM_BAT_CONSERVE
                alm_init_bat_conserve_timer();
            #endif
           
                
            //
            // Monitor CO Alarm Off Timer while in Alarm None State
            //
            #ifdef CONFIGURE_WIRELESS_MODULE
                #ifdef CONFIGURE_INTERCONNECT
                    alm_CO_alarm_off_timer();   
                #endif
            #endif
                    
            //
            // Monitor Remote Inhibit Timer
            //
            #ifdef CONFIGURE_WIRELESS_MODULE
              #ifdef CONFIGURE_INTERCONNECT

                alm_remote_check_inhibit_timer();
                
                // Reset Smoke Alarm State Measure Time to Max
                // This is cleared when Slave Smoke is received and Slave
                //  alarm time is measured to detect any HW INT model's
                //  Smoke PTT sequence
                alm_smoke_alarm_time = TIME_SMK_ALM_60_SECS;
                
              #endif
            #endif
            
            if(FLAG_MASTER_SMOKE_ACTIVE)
            {
                // Set the Alarm Mode flags
                FLAG_SLAVE_ALARM_MODE = 0;
                FLAG_REMOTE_ALARM_MODE = 0;
                
                // Enable Smoke alarm T3
                FLAG_SMOKE_ALARM_ACTIVE = 1;

                if(!FLAG_PTT_ACTIVE)
                {
                    // Alarm Memory set if this is a real smoke event
                    FLAG_SMOKE_ALARM_MEMORY = 1;

                    #ifdef CONFIGURE_DIAG_HIST
                        // Record this Smoke Event upon entry
                        DIAG_Hist_Que_Push(HIST_SMK_ALARM_ON_EVENT);
                    #endif
                }

                // The Green LED task is reset so that initiating
                //  alarm can be started immediately
                MAIN_Reset_Task(TASKID_GLED_MANAGER);

                #ifdef CONFIGURE_VOICE
                    // Initialize Voice to Play between temporal patterns
                    FLAG_PLAY_VOICE = 1;
                #endif
                    
                #ifdef CONFIGURE_WIRELESS_MODULE
                    #ifdef CONFIGURE_INTERCONNECT  

                    if( !FLAG_PTT_ACTIVE && !FLAG_REMOTE_PTT_ACTIVE )
                    {
                        
                        // Delay Driving HW Interconnect, 
                        //  inhibit INT Out Drive and start Delay Timer    
                        ALM_Init_Master_Slave_Timer();   
                    }
                    else
                    {
                        // Enable Interconnect Drive Out Immediately
                        //  for PTT alarms
                        FLAG_ALM_INT_DRV_ENABLE = 1;
                    }

                    #endif
                #endif

                FLAG_ALM_PRIO_NOT_IDLE = 1;    
                alm_state = ALM_STATE_MASTER_SMOKE;

            }

            #ifdef CONFIGURE_INTERCONNECT
                // INTCON Smoke Alarm has priority over Remote Smoke Alarm
                else if(FLAG_INTCON_SMOKE_RECEIVED)
                {
                    // If in Remote PTT when Interconnect Signal is Active
                    //  allow Remote PTT to complete and do not enter Slave
                    //  alarm
                    if(!FLAG_REMOTE_PTT_ACTIVE)
                    {
                        // Receiving Interconnect Smoke Signal
                        //  Enter Alarm Smoke Slave State
                        
                        // Set Slave Smoke Alarm Mode Flags
                        FLAG_SLAVE_ALARM_MODE = 1;
                        FLAG_SLAVE_SMOKE_ACTIVE = 1;
                        FLAG_REMOTE_ALARM_MODE = 0;
                        
                        // Start Smoke Alarm T3 unless in Locate Mode
                        if(!FLAG_ALARM_LOCATE_MODE)
                        {
                            FLAG_SMOKE_ALARM_ACTIVE = 1;
                        }

                        // Start Fire message 
                        #ifdef CONFIGURE_VOICE
                            FLAG_PLAY_VOICE = 1;
                        #endif

                        #ifdef CONFIGURE_WIRELESS_MODULE

                            if(FLAG_INT_SMK_INTERRUPTED_CO)
                            {
                                // This can't be a PTT sequence so no need to
                                //  measure Smoke State Time
                                // Max out Timer (Timer Off)
                                alm_smoke_alarm_time = TIME_SMK_ALM_60_SECS;
                                
                                FLAG_INT_SMK_INTERRUPTED_CO = 0;
                                
                            }
                            else
                            {
                                // Reset Smoke Alarm State Timer
                                // This will be used to look for a Slave PTT
                                // sequence instead of a Slave Smoke Alarm event
                                alm_smoke_alarm_time = 0;
                            }
                            
                            /*
                             * Look for more conditions where we do not  
                             *  send network ARM request
                             *  1. Slave to Remote transmissions set to Off
                             *  2. Remote Smoke Alarms being received
                             *  3. Wireless gateway already established
                             *     or
                             *  4. FLAG_DO_NOT_ARM set by another state
                             */
                            if( FLAG_GATEWAY_SLV_TO_REMOTE_OFF ||
                                FLAG_REMOTE_SMOKE_ACTIVE ||
                                FLAG_REMOTE_HW_SMOKE_ACTIVE ||
                                FLAG_WL_GATEWAY_ENABLE )
                            {
                                FLAG_DO_NOT_ARM = 1;
                            }
                            
                            #ifdef CONFIGURE_WIFI
                                //
                                // For WIFI we do not need to Arm here
                                // We can leave it in for PTT
                                //
                                FLAG_DO_NOT_ARM = 1;                            
                            #endif
                            
                            if(!FLAG_DO_NOT_ARM)
                            {
                                // Allow Network to Enter Real Time Mode
                                //  in preparation for alarm or detected PTT
                                APP_PTT_Arm();
                            }
                            else
                            {
                                // ARM message disabled, reset ARM disable
                                FLAG_DO_NOT_ARM = 0;
                            }

                            
                            //This Timer delays start of Remote Alarm
                            // transmissions after entering Slave Alarm
                            APP_Init_Slave_To_Remote_Alarm_Timer
                                    (TIMER_SLAVE_TO_REMOTE_DLY_12_SEC);

                        #endif

                        FLAG_ALM_PRIO_NOT_IDLE = 1;
                        alm_state = ALM_ALARM_SLAVE_SMOKE;
                    }
                    else
                    {
                       /*
                        * During Remote PTT (also receiving INTCON Alarm)
                        * Do not clear FLAG_INTCON_SMOKE_RECEIVED here, will 
                        *  be cleared at PTT/Reset
                        * If set and in Remote PTT, we do not drive HW INT
                        *  since another alarm is already driving it.  
                        */ 
                    }
                }
            #endif

            #ifdef CONFIGURE_WIRELESS_MODULE
                // If in Smoke Hush (only Master is allowed to Hush)
                //  then do not respond to Remote Alarms
                // Remote Alarm is also delayed if Remote Inhibit is Active
                    
            #ifndef CONFIGURE_AUSTRALIA
                
                else if(FLAG_REMOTE_SMOKE_ACTIVE && 
                        !FLAG_SMOKE_HUSH_ACTIVE  && FLAG_ALM_REMOTE_ENABLE)
                    
            #else
                //
                // For Australia respond to Remote alarms while in SMK Hush    
                //
                else if(FLAG_REMOTE_SMOKE_ACTIVE && FLAG_ALM_REMOTE_ENABLE)
                    
            #endif
                {
                    if(!FLAG_REMOTE_PTT_ACTIVE && !FLAG_PTT_ACTIVE)
                    {
                        #ifdef CONFIGURE_INTERCONNECT
                            /* 
                             * Before we enter Remote alarm, wait
                             *  for FLAG_WL_GATEWAY_ENABLE to become active
                             *  before transitioning into Remote Smoke state
                             */
                            if(FLAG_WL_GATEWAY_ENABLE)
                            {
                                // Remote to Slave gateway has been determined
                                
                                // Set Remote Smoke Alarm Mode flags
                                FLAG_SLAVE_ALARM_MODE = 0;
                                FLAG_REMOTE_ALARM_MODE = 1;
                                
                                // Start the Smoke Alarm T3 pattern
                                if(!FLAG_ALARM_LOCATE_MODE)
                                {
                                    FLAG_SMOKE_ALARM_ACTIVE = 1;
                                }
                                
                                // Enable Fire Voice Messages
                                #ifdef CONFIGURE_VOICE
                                    // Init. Voice time to play
                                    FLAG_PLAY_VOICE = 1;
                                #endif

                                FLAG_ALM_PRIO_NOT_IDLE = 1;    
                                alm_state = ALM_ALARM_REMOTE_SMOKE;
                            }
                            
                        #else
                            FLAG_REMOTE_ALARM_MODE = 1;
                            FLAG_SLAVE_ALARM_MODE = 0;
                            
                            if(!FLAG_ALARM_LOCATE_MODE)
                            {
                                FLAG_SMOKE_ALARM_ACTIVE = 1;
                            }

                            #ifdef CONFIGURE_VOICE
                                // Init. Voice time to play
                                FLAG_PLAY_VOICE = 1;
                            #endif

                            FLAG_ALM_PRIO_NOT_IDLE = 1;
                            alm_state = ALM_ALARM_REMOTE_SMOKE;

                        #endif
                    }
                    else
                    {
                        // Clear if PTT active
                        FLAG_REMOTE_SMOKE_ACTIVE = 0;
                    }
                }
                
            #ifndef CONFIGURE_AUSTRALIA
                
                else if(FLAG_REMOTE_HW_SMOKE_ACTIVE && 
                        !FLAG_SMOKE_HUSH_ACTIVE  && FLAG_ALM_REMOTE_ENABLE)
                    
            #else
                //
                // For Australia respond to Remote alarms while in SMK Hush    
                //
                else if(FLAG_REMOTE_HW_SMOKE_ACTIVE && FLAG_ALM_REMOTE_ENABLE)
                    
            #endif
                {
                    if(!FLAG_REMOTE_PTT_ACTIVE && !FLAG_PTT_ACTIVE)
                    {
                        #ifdef CONFIGURE_INTERCONNECT
                            /* 
                             * Before we enter Remote alarm, wait
                             *  for FLAG_WL_GATEWAY_ENABLE to become active
                             *  before transitioning into Remote Smoke state
                             */
                            if(FLAG_WL_GATEWAY_ENABLE)
                            {
                                // Remote to Slave gateway has been determined
                                
                                // Set Remote Smoke Alarm Mode flags
                                FLAG_SLAVE_ALARM_MODE = 0;
                                FLAG_REMOTE_ALARM_MODE = 1;
                                
                                // Start the Smoke Alarm T3 pattern
                                if(!FLAG_ALARM_LOCATE_MODE)
                                {
                                    FLAG_SMOKE_ALARM_ACTIVE = 1;
                                }
                                
                                // Enable Fire Voice Messages
                                #ifdef CONFIGURE_VOICE
                                    // Init. Voice time to play
                                    FLAG_PLAY_VOICE = 1;
                                #endif

                                FLAG_ALM_PRIO_NOT_IDLE = 1;    
                                alm_state = ALM_ALARM_REMOTE_SMOKE_HW;
                            }
                            
                        #else
                            FLAG_REMOTE_ALARM_MODE = 1;
                            FLAG_SLAVE_ALARM_MODE = 0;
                            
                            if(!FLAG_ALARM_LOCATE_MODE)
                            {
                                FLAG_SMOKE_ALARM_ACTIVE = 1;
                            }

                            #ifdef CONFIGURE_VOICE
                                // Init. Voice time to play
                                FLAG_PLAY_VOICE = 1;
                            #endif

                            FLAG_ALM_PRIO_NOT_IDLE = 1;
                            alm_state = ALM_ALARM_REMOTE_SMOKE_HW;

                        #endif
                    }
                    else
                    {
                        // Clear if PTT active
                        FLAG_REMOTE_HW_SMOKE_ACTIVE = 0;
                    }
                }
                
            #endif

                
            #ifdef CONFIGURE_CO
                // Check for Master CO
                else if(FLAG_CO_ALARM_CONDITION)
                {
                    // Set Master CO Alarm Mode flags    
                    FLAG_SLAVE_ALARM_MODE = 0;
                    FLAG_REMOTE_ALARM_MODE = 0;
                    
                    // CO Alarm Memory if a Real CO event
                    if(!FLAG_PTT_ACTIVE)
                    {
                        FLAG_CO_ALARM_MEMORY = 1;
                        
                        #ifdef CONFIGURE_DIAG_HIST
                            // Only Record if entering alarm
                            if(!FLAG_CO_ALARM_ACTIVE)
                            {
                                DIAG_Hist_Que_Push(HIST_ALARM_ON_EVENT);
                            }
                        #endif
                        
                    }
                        
                    // Enable CO alarm T4 pattern
                    FLAG_CO_ALARM_ACTIVE = 1;

                    // Set GLED task to execute immediately for initiating
                    //  alarm blinks
                    MAIN_Reset_Task(TASKID_GLED_MANAGER);

                    // Begin CO alarm voice message
                    #ifdef CONFIGURE_VOICE
                        FLAG_PLAY_VOICE = 1;
                    #endif

                    FLAG_ALM_PRIO_NOT_IDLE = 1;    
                    alm_state = ALM_ALARM_MASTER_CO;
                }
            #endif

            #ifdef CONFIGURE_INTERCONNECT
                else if(FLAG_INTCON_CO_RECEIVED) 
                {
                    // If in Remote PTT when Interconnect Signal is Active
                    //  allow Remote PTT to complete and do not enter Slave
                    //  alarm
                    if(!FLAG_REMOTE_PTT_ACTIVE)
                    {
                        // A CO alarm was received via interconnect
                        // Set alarm mode and go to slave state
                        FLAG_REMOTE_ALARM_MODE = 0;
                        FLAG_SLAVE_ALARM_MODE = 1;
                        FLAG_SLAVE_CO_ACTIVE = 1;

                        // Start CO alarm T4 pattern
                        if(!FLAG_ALARM_LOCATE_MODE)
                        {
                            FLAG_CO_ALARM_ACTIVE = 1;
                        }
                        
                        // Begin CO voice message
                        #ifdef CONFIGURE_VOICE
                            FLAG_PLAY_VOICE = 1;
                        #endif

                        /*
                         * Do I really need this delay before sending a CO
                         * Alarm status to RF Module???
                         */    
                        #ifdef CONFIGURE_WIRELESS_MODULE
                            /*
                             * If CO Slave Alarm has been silent for ~35 secs.
                             * Then allow immediate Alarm Status CCI 
                             *  transmissions
                             */
                            if(FLAG_CO_ALARM_OFF_TIMER)
                            {
                                
                                //This Timer delays start of Remote Alarm
                                // transmissions after entering Slave Alarm
                                
                                // This Timer may not be needed for CO anymore
                                // since changes to RF Modult to handle
                                // Real Time Mode initiator in a multi-gateway 
                                // configuration
                                APP_Init_Slave_To_Remote_Alarm_Timer
                                        (TIMER_SLAVE_TO_REMOTE_DLY_14_SEC);

                            }
                            else
                            {
                                APP_Init_Slave_To_Remote_Alarm_Timer
                                        (TIMER_SLAVE_TO_REMOTE_DLY_1_SEC);
                            }
                            
                        #endif

                        FLAG_ALM_PRIO_NOT_IDLE = 1;
                        alm_state = ALM_ALARM_SLAVE_CO;
                    }
                    else
                    {
                       /*
                        * During Remote PTT (also receiving INTCON Alarm)
                        * Do not clear FLAG_INTCON_SMOKE_RECEIVED here, will 
                        *  be cleared at PTT/Reset
                        * If set and in Remote PTT, we do not drive HW INT
                        *  since another alarm is already driving it.  
                        */ 
                    }
                }
            #endif
                
            #ifdef CONFIGURE_WIRELESS_MODULE
                // Remote Alarm is delayed if Remote Inhibit is Active
                else if(FLAG_REMOTE_CO_ACTIVE && FLAG_ALM_REMOTE_ENABLE)
                {
                    if(!FLAG_REMOTE_PTT_ACTIVE && !FLAG_PTT_ACTIVE)
                    {
                        #ifdef CONFIGURE_INTERCONNECT
                            /* Before we enter Remote alarm, wait
                             * for FLAG_WL_GATEWAY_ENABLE to become active
                             * before transitioning to Remote Smoke state
                             * This delays driving HW Interconnect to allow
                             * lower RFID Gateway to gain control of it.
                             */
                            if(FLAG_WL_GATEWAY_ENABLE)
                            {
                                // Set Remote Alarm Mode flags
                                FLAG_SLAVE_ALARM_MODE = 0;
                                FLAG_REMOTE_ALARM_MODE = 1;
                                
                                // Begin CO alarm T4 pattern
                                if(!FLAG_ALARM_LOCATE_MODE)
                                {
                                    FLAG_CO_ALARM_ACTIVE = 1;
                                }

                                // Begin CO voice message
                                #ifdef CONFIGURE_VOICE
                                    FLAG_PLAY_VOICE = 1;
                                #endif

                                FLAG_ALM_PRIO_NOT_IDLE = 1;
                                alm_state = ALM_ALARM_REMOTE_CO;
                            }

                        #else
                            FLAG_REMOTE_ALARM_MODE = 1;
                            FLAG_SLAVE_ALARM_MODE = 0;
                            
                            if(!FLAG_ALARM_LOCATE_MODE)
                            {
                                FLAG_CO_ALARM_ACTIVE = 1;
                            }

                            #ifdef CONFIGURE_VOICE
                                FLAG_PLAY_VOICE = 1;
                            #endif

                            FLAG_ALM_PRIO_NOT_IDLE = 1;
                            alm_state = ALM_ALARM_REMOTE_CO;

                        #endif
                    }
                    else
                    {
                        FLAG_REMOTE_CO_ACTIVE = 0;
                    }
                }
                
                // Remote Alarm is delayed if Remote Inhibit is Active
                else if(FLAG_REMOTE_HW_CO_ACTIVE && FLAG_ALM_REMOTE_ENABLE)
                {
                    if(!FLAG_REMOTE_PTT_ACTIVE && !FLAG_PTT_ACTIVE)
                    {
                        #ifdef CONFIGURE_INTERCONNECT
                            /* Before we enter Remote alarm, wait
                             * for FLAG_WL_GATEWAY_ENABLE to become active
                             * before transitioning to Remote Smoke state
                             * This delays driving HW Interconnect to allow
                             * lower RFID Gateway to gain control of it.
                             */
                            if(FLAG_WL_GATEWAY_ENABLE)
                            {
                                // Set Remote Alarm Mode flags
                                FLAG_SLAVE_ALARM_MODE = 0;
                                FLAG_REMOTE_ALARM_MODE = 1;
                                
                                // Begin CO alarm T4 pattern
                                if(!FLAG_ALARM_LOCATE_MODE)
                                {
                                    FLAG_CO_ALARM_ACTIVE = 1;
                                }

                                // Begin CO voice message
                                #ifdef CONFIGURE_VOICE
                                    FLAG_PLAY_VOICE = 1;
                                #endif

                                FLAG_ALM_PRIO_NOT_IDLE = 1;
                                alm_state = ALM_ALARM_REMOTE_CO_HW;
                            }

                        #else
                            FLAG_REMOTE_ALARM_MODE = 1;
                            FLAG_SLAVE_ALARM_MODE = 0;
                            
                            if(!FLAG_ALARM_LOCATE_MODE)
                            {
                                FLAG_CO_ALARM_ACTIVE = 1;
                            }

                            #ifdef CONFIGURE_VOICE
                                FLAG_PLAY_VOICE = 1;
                            #endif

                            FLAG_ALM_PRIO_NOT_IDLE = 1;
                            alm_state = ALM_ALARM_REMOTE_CO_HW;

                        #endif
                    }
                    else
                    {
                        FLAG_REMOTE_HW_CO_ACTIVE = 0;
                    }
                }
                
            #endif

            else
            {
                // No Present Alarm Condition
            }
        break;

        
        case ALM_STATE_MASTER_SMOKE:
        {
            
            #ifdef CONFIGURE_UNDEFINED
                // TODO: Undefine Later  BC: Test Only 
                if(FLAG_SMK_ALMPRIO_TRANS)
                {
                    SER_Send_String("SMK_Transition_OFF_MS");
                    SER_Send_Prompt();
                }
            #endif
            // Clear Smoke from CO Transition Indicator
            FLAG_SMK_ALMPRIO_TRANS = 0;
            
            #ifdef	CONFIGURE_WIRELESS_ALARM_LOCATE
                // Update/Monitor Locate Mode
                alm_locate_timer();
            #endif

            #ifdef CONFIGURE_WIRELESS_MODULE
                #ifdef CONFIGURE_INTERCONNECT  
                    /*
                     * Also Check HW INT smoke to see also Monitor HW INT 
                     * smoke to see if perhaps another gateway unit is driving
                     * HW INT during the Master-Slave Time Period
                     * If so, then stop the timer with HW Inte Drv disabled
                     */
                     if(FLAG_TMR_INT_DRV_ACTIVE)
                     {
                         if(FLAG_INTCON_SMOKE_RECEIVED)
                         {
                             // Stop Master to Slave Timer leaving
                             //  Interconnect Drive Out OFF
                             FLAG_INTCON_SMOKE_RECEIVED = 0;
                             FLAG_TMR_INT_DRV_ACTIVE = 0;
                         }
                         
                     }
                
                
                    // Service Master Alarm to INT Out Delay Timer
                    ALM_Check_Master_Slave_Timer();
                
                #endif
            #endif

            //
            // If the Master Smoke flag has been cleared, exit Master Smoke
            //  state.
            //
            if(!FLAG_MASTER_SMOKE_ACTIVE)
            {
                if(!FLAG_PTT_ACTIVE)
                {
                    #ifdef CONFIGURE_DIAG_HIST
                        DIAG_Hist_Que_Push(HIST_ALARM_OFF_EVENT);
                    #endif
                }

                // End Smoke Alarm T3 pattern
                FLAG_SMOKE_ALARM_ACTIVE = 0;
                
                // Allow HW INT CO received to be cleared in Alarm Init state
                FLAG_RST_INT_CO_RCVD = 1;
                
                if(FLAG_GATEWAY_MASTER || FLAG_INTCON_SMOKE_RECEIVED)
                {
                    if(!FLAG_PTT_ACTIVE && !FLAG_REMOTE_PTT_ACTIVE)
                    {
                        
                        if(FLAG_INTCON_SMOKE_RECEIVED)
                        {
                            FLAG_INTCON_SMOKE_RECEIVED = 0;
                            // Init Delay Timer to let HW Int settle
                            alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_40_SECS);
                            
                        }
                        else
                        {
                            #ifdef CONFIGURE_WIRELESS_MODULE
                                // Init Delay Timer 
                                alm_init_clear_delay
                                        (TIMER_SMK_ALM_CLEAR_40_SECS);

                            #else
                                // Init Delay Timer , should not need as 
                                //  much delay since this is not wireless 
                                //  or a gateway 
                                alm_init_clear_delay
                                        (TIMER_SMK_ALM_CLEAR_20_SECS);
                            #endif
                
                        }
                    }
                    else
                    {
                        // Init Delay Timer (shortened for PTT Smoke exits)
                        alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_12_SECS);
                    }
                        
                    // Allows Network Remotes to Clear before looking for
                    //  a new Alarm Condition
                    alm_state = ALM_STATE_SMK_CLR;
                    
                    return MAIN_Make_Return_Value(ALARM_PRIORITY_INTERVAL_MS);
                }
                
                #ifdef CONFIGURE_UNDEFINED
                    // In Some Network / HW INT configurations this 
                    //  allows Alarm Lockup (mult gateways)
                    // If not Gateway Master, return to Alarm None through 
                    //  Alarm Init
                    alm_state = ALM_STATE_ALARM_INIT;    
                #endif
    
                #ifdef CONFIGURE_WIRELESS_MODULE
                    // Init Delay Timer 
                    alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_40_SECS);

                #else
                    // Init Delay Timer , should not need as much delay
                    //  since this is not wireless or a gateway           
                    alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_20_SECS);
                #endif

                // Allows Network Remotes to Clear before looking for
                //  a new Alarm Condition
                alm_state = ALM_STATE_SMK_CLR;
                
            }
            
            // Received a Remote Hush Request, Exit through Smoke Delay
            //  to allow network faster response, if Hush is not possible
            //  i.e Too Much Smoke, Smoke Alarm state will be re-entered    
            if(FLAG_SMK_EXIT_TO_HUSH)
            {
                FLAG_SMK_EXIT_TO_HUSH = 0;
                
                // Allow HW INT CO received to be cleared in Alarm 
                //  Init state
                FLAG_RST_INT_CO_RCVD = 1;
                
                // Stop the Alarm T3 pattern
                FLAG_SMOKE_ALARM_ACTIVE = 0;
                
                 // Init Delay Timer
                alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_22_SECS);

                // Allows Network Remotes time to Clear before looking for
                //  a new Alarm Condition
                alm_state = ALM_STATE_SMK_CLR;

                return MAIN_Make_Return_Value(ALARM_PRIORITY_INTERVAL_MS);
            }
        }
        break;

        case ALM_ALARM_MASTER_CO:
            
          #ifdef CONFIGURE_CO
            
            // Clear SMK Transition Flag for cases where it may have been 
            //  left on
            FLAG_SMK_ALMPRIO_TRANS = 0; 
            
            //
            // Service Remote Inhibit Timer to allow it to complete
            //    if active
            //
            #ifdef CONFIGURE_WIRELESS_MODULE
              #ifdef CONFIGURE_INTERCONNECT
                  alm_remote_check_inhibit_timer();
              #endif
            #endif
            

            #ifdef	CONFIGURE_WIRELESS_ALARM_LOCATE
                // Update/Test Locate Mode if active
                alm_locate_timer();
            #endif

            /*
             * If smoke flag has become active, exit slave CO mode and go back
             * to Alarm None Mode.  The Alarm None logic will then decide what
             * to do. This causes smoke alarm to override CO Alarm.
             */
            if(FLAG_MASTER_SMOKE_ACTIVE ||
               FLAG_INTCON_SMOKE_RECEIVED  ||
               FLAG_REMOTE_SMOKE_ACTIVE || FLAG_REMOTE_HW_SMOKE_ACTIVE)
            {
                //
                // Clear alarm flag to stop the temporal pattern 
                //
                FLAG_CO_ALARM_ACTIVE = 0;
                
                // Reset potential Wireless Bat Conserve logic
                FLAG_WL_ALM_BAT_CONSERVE = 0;
                
                if(FLAG_REMOTE_SMOKE_ACTIVE || FLAG_REMOTE_HW_SMOKE_ACTIVE)
                {
                    // Init Gateway Flags if received Remote Smoke 
                    //  Interrupted this Master CO State
                    FLAG_WL_GATEWAY_ENABLE = 0;
                    FLAG_GATEWAY_REMOTE_TO_SLAVE = 0;
                    
                    FLAG_INT_ACTIVITY_DETECTED = 0;
                    FLAG_INT_CO_ACTIVITY_DETECTED = 0;

                }
                
                #ifdef CONFIGURE_WIRELESS_MODULE
                    if(FLAG_INTCON_SMOKE_RECEIVED)
                    {
                        // Inhibit Arm message if Slave Smoke interrupted CO
                        FLAG_DO_NOT_ARM = 1;
                    }
                
                    /*
                     * This flag is set to prevent Alarm Status Clear from 
                     *  being sent via CCI to the RF Module during transition.
                     * Alarm Status Clear will take network out of Real
                     *  time mode causing problems with remote alarms
                     * 
                     * It is cleared once transition is complete
                     */
                    FLAG_SMK_ALMPRIO_TRANS = 1;
                
                #endif
                
                // Proceed directly to Alarm None on Smoke override
                alm_state = ALM_STATE_ALARM_NONE;
                
                MAIN_Reschedule_Int_Active
                    (TASKID_APP_STAT_MONITOR, ALM_RESCHEDULE_TIME_100_MS);

                // Return on next TIC to speed smoke alarm entry
                return MAIN_Make_Return_Value
                            (ALARM_PRIORITY_INTERVAL_10_MS);
                
            }
            else if(!FLAG_CO_ALARM_CONDITION)
            {
                if(!FLAG_PTT_ACTIVE)
                {
                    // Only Record if exiting alarm
                    #ifdef CONFIGURE_DIAG_HIST
                        DIAG_Hist_Que_Push(HIST_ALARM_OFF_EVENT);
                    #endif
                }
                
                // Allow HW INT CO received to be cleared in Alarm 
                //  Init state
                FLAG_RST_INT_CO_RCVD = 1;

                // Stop CO Alarm T4 pattern
                FLAG_CO_ALARM_ACTIVE = 0;
                
                // Reset potential Wireless Bat Conserve logic
                FLAG_WL_ALM_BAT_CONSERVE = 0;

                if(FLAG_GATEWAY_MASTER && 
                   !FLAG_PTT_ACTIVE  && !FLAG_REMOTE_PTT_ACTIVE)
                {

                    #ifdef CONFIGURE_WIRELESS_MODULE
                        // Init Delay Timer 
                        alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_40_SECS);

                    #else
                        // Init Delay Timer , should not need as much delay
                        //  since this is not wireless or a gateway           
                        alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_20_SECS);
                    #endif

                    
                    // Allow Network Remotes to Clear
                    alm_state = ALM_STATE_CO_CLR;
                    return MAIN_Make_Return_Value(ALARM_PRIORITY_INTERVAL_MS);
                }
                
                if(FLAG_INTCON_CO_RECEIVED)
                {
                    FLAG_INTCON_CO_RECEIVED = 0;
                    // Init Delay Timer 
                    alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_40_SECS);
                    
                    // Allows Network Remotes and Slaves to Clear before 
                    //  looking for a new Alarm Condition
                    alm_state = ALM_STATE_CO_CLR;
                    
                }
                
                #ifdef CONFIGURE_UNDEFINED
                    // In Some Network / HW INT configurations this 
                    //  allows Alarm Lockup (mult gateways)
                    // If not Gateway Master, return to Alarm None through 
                    //  Alarm Init
                    alm_state = ALM_STATE_ALARM_INIT;    
                #endif
                
                #ifdef CONFIGURE_WIRELESS_MODULE
                    // Init Delay Timer 
                    alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_40_SECS);

                #else
                    // Init Delay Timer , should not need as much delay
                    //  since this is not wireless or a gateway           
                    alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_20_SECS);
                #endif
                
                // Allows Network Remotes to Clear before looking for
                //  a new Alarm Condition
                alm_state = ALM_STATE_CO_CLR;
                
            }
            else
            {
                #ifdef  CONFIGURE_CO_ALM_BAT_CONSERVE
                    uint retval;
                    if(retval = alm_alarm_bat_conserve_logic() )
                    {
                        return  MAIN_Make_Return_Value(retval);
                    }
                #endif

            }
          #endif
        break;

        case ALM_ALARM_SLAVE_SMOKE:

            #ifdef CONFIGURE_UNDEFINED            
                // TODO: Undefine Later  BC: Test Only
                if(FLAG_SMK_ALMPRIO_TRANS)
                {
                    SER_Send_String("SMK_Transition_OFF_SS");
                    SER_Send_Prompt();
                }
            #endif


            // Clear Smoke from CO Transition Indicator
            FLAG_SMK_ALMPRIO_TRANS = 0;
                
            //
            // Service Remote Inhibit Timer to allow it to complete
            //    if active
            //
            #ifdef CONFIGURE_WIRELESS_MODULE
              #ifdef CONFIGURE_INTERCONNECT
                alm_remote_check_inhibit_timer();
              #endif
            #endif

            #ifdef CONFIGURE_INTERCONNECT
                
                #ifdef CONFIGURE_WIRELESS_MODULE
                
                    // Clear Gateway INT Activity detected Flag
                    //  no longer required once slave alarm is detected
                    FLAG_INT_ACTIVITY_DETECTED = 0;
                    FLAG_INT_CO_ACTIVITY_DETECTED = 0;
                #endif

                #ifdef CONFIGURE_WIRELESS_MODULE
                    if( FLAG_REMOTE_SMOKE_ACTIVE || FLAG_REMOTE_CO_ACTIVE ||
                        FLAG_REMOTE_HW_SMOKE_ACTIVE || 
                        FLAG_REMOTE_HW_CO_ACTIVE )
                    {
                        //There is already Network Alarm, so disregard PTT 
                        //  monitor
                        alm_smoke_alarm_time = TIME_SMK_ALM_60_SECS;
                        FLAG_SLAVE_PTT_DETECTED = 0;
                    }
                    
                    // For WiFi we can disable the PTT monitor
                    #ifdef CONFIGURE_WIFI
                        alm_smoke_alarm_time = TIME_SMK_ALM_60_SECS;
                        FLAG_SLAVE_PTT_DETECTED = 0;
                    #endif

                    // Measure Time since start of Slave Smoke Alarm Reception
                    //  limit to 1 minute
                    if(++alm_smoke_alarm_time > TIME_SMK_ALM_60_SECS)
                    {
                        alm_smoke_alarm_time = TIME_SMK_ALM_60_SECS;
                    }
                    else if(alm_smoke_alarm_time < TIME_SMK_ALM_25_SECS)
                    {
                        // For PTTs, this typically happens under 10 seconds
                        if(FLAG_INTCON_CO_RECEIVED && !FLAG_SLAVE_PTT_DETECTED)
                        {
                            // Make sure we have waited at least 12 seconds
                            //  after ARM was issued
                            if(alm_smoke_alarm_time > TIME_SMK_ALM_12_SECS)
                            {
                                // If a Slave CO is received immediately
                                //  after short period of Smoke, send a Remote 
                                // PTT instead of Slave to Remote Smoke alarm
                                APP_PTT_Local();

                                FLAG_SLAVE_PTT_DETECTED = 1;

                                // In this case, do not pass PTT Slave Alarms to 
                                //  Wireless Network
                                FLAG_GATEWAY_SLV_TO_REMOTE_OFF = 1;

                                #ifndef CONFIGURE_UNDEFINED
                                    SER_Send_String("Slave PTT Detected...");
                                    SER_Send_Prompt();
                                #endif
                            }
                        }
                    }
                #endif

                // Currently we remain in Slave Alarm state even if 
                //  Master smoke alarm becomes active after entering this state
                //  the exception is if Alarm Locate becomes active     
                #ifdef CONFIGURE_WIRELESS_ALARM_LOCATE
                    if(FLAG_ALARM_LOCATE_MODE)
                    {
                        // Test for Locate Timeout
                        alm_locate_timer();
                        
                        // Alarm Locate Mode Set
                        // Halt Remote/Slave Alarms during Locate
                        FLAG_SMOKE_ALARM_ACTIVE = 0;
                        
                        // Clear any cancels as a result of Locate Entry
                        FLAG_CNCL_SMK_CO_ALARM = 0;
                        
                        // Keep Track of Incoming Slave Alarms for
                        //  when Locate Times Out
                        if(FLAG_INTCON_SMOKE_RECEIVED)
                        {
                            // If still receiving Interconnect Smoke Signal
                            //  Refresh alarm timeout timer.
                            alm_init_slave_timer();

                            FLAG_INTCON_SMOKE_RECEIVED = 0;
                        }
                        
                        /*
                         *  If we quit receiving Slave Smoke Signals, then
                         *  end Slave Smoke RF transmissions 
                         *  If we continue Receiving Slave Smoke alarms
                         *  Then allow them to be passed to Network Bridge
                         */
                        if( (uchar)(MAIN_OneSecTimer - alm_start_time) >
                                 (uchar)TIMER_LOC_SLAVE_ALARM_TIMEOUT)
                        {
                            // Allow Alarm Status Clear during Locate
                            FLAG_GATEWAY_SLV_TO_REMOTE_OFF = 1;
                        }
                        
                        // If Smoke is detected while in slave locate
                        //  Transition to Smoke Master immediately 
                        if(FLAG_MASTER_SMOKE_ACTIVE)
                        {
                            // Exiting Slave Smoke state into Master state
                            FLAG_SLAVE_ALARM_MODE = 0;
                            FLAG_SLAVE_SMOKE_ACTIVE = 0;
                            FLAG_REMOTE_ALARM_MODE = 0;
                            
                            // Begin Smoke Alarm again
                            FLAG_SMOKE_ALARM_ACTIVE = 1;
                            
                            if(!FLAG_PTT_ACTIVE)
                            {
                                // Alarm Memory set if this is a real smoke 
                                //  event
                                FLAG_SMOKE_ALARM_MEMORY = 1;

                                #ifdef CONFIGURE_DIAG_HIST
                                    // Record this Smoke Event upon entry
                                    DIAG_Hist_Que_Push(HIST_SMK_ALARM_ON_EVENT);
                                #endif
                            }

                            // The Green LED task is reset so that initiating
                            //  alarm can be started immediately
                            MAIN_Reset_Task(TASKID_GLED_MANAGER);

                            #ifdef CONFIGURE_VOICE
                                // Initialize Voice to Play between temporal 
                                //  patterns
                                FLAG_PLAY_VOICE = 1;
                            #endif

                            #ifdef CONFIGURE_WIRELESS_MODULE
                                #ifdef CONFIGURE_INTERCONNECT  
                                    // Enable Interconnect Drive Out (no Delay)
                                    FLAG_ALM_INT_DRV_ENABLE = 1;
                                #endif
                            #endif

                            alm_state = ALM_STATE_MASTER_SMOKE;

                        }

                        return MAIN_Make_Return_Value
                                    (ALARM_PRIORITY_INTERVAL_MS);
                    }
                
                #endif

                // Received a Remote Hush Request, Exit through Smoke Delay
                //  to allow network faster response, if Hush is not possible
                //  i.e Too Much Smoke, Smoke Alarm state will be re-entered    
                if(FLAG_SMK_EXIT_TO_HUSH)
                {
                    FLAG_SMK_EXIT_TO_HUSH = 0;
                    FLAG_SMOKE_ALARM_ACTIVE = 0;

                     // Init Delay Timer
                    alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_15_SECS);

                    // Allows Network Remotes to Clear before looking for
                    //  a new Alarm Condition
                    alm_state = ALM_STATE_SMK_CLR;
                    
                    // Prevent HW INT CO received to be cleared in 
                    //  Alarm Init state
                    FLAG_RST_INT_CO_RCVD = 0;
                    

                    return MAIN_Make_Return_Value(ALARM_PRIORITY_INTERVAL_MS);

                }
                        
                //      
                // Set Smoke Alarm Memory if:
                // Smoke Initiating condition is detected in Slave State
                //
                if(FLAG_MASTER_SMOKE_ACTIVE && !FLAG_SMOKE_ALARM_MEMORY)
                {
                    if(!FLAG_PTT_ACTIVE)
                    {
                        // Alarm Memory set if this is a real smoke 
                        //  event
                        FLAG_SMOKE_ALARM_MEMORY = 1;

                        #ifdef CONFIGURE_DIAG_HIST
                            // Record this Smoke Event upon entry
                            DIAG_Hist_Que_Push(HIST_SMK_ALARM_ON_EVENT);
                        #endif
                    }
                }
            
                if(FLAG_INTCON_SMOKE_RECEIVED)
                {
                    // Insure Smoke alarm is Active in case Locate Ended
                    FLAG_SMOKE_ALARM_ACTIVE = 1;
                    
                    // If still receiving Interconnect Smoke Signal
                    //  Refresh alarm timeout timer.
                    alm_init_slave_timer();

                    FLAG_INTCON_SMOKE_RECEIVED = 0;


                }
                    
                #ifdef	CONFIGURE_WIRELESS_MODULE
                    else if(FLAG_CNCL_SMK_CO_ALARM)
                    {
                        // Alarm Cancel received before Timeout

                        // One time event
                        FLAG_CNCL_SMK_CO_ALARM = 0;
                        

                        // Cancel has occurred.  Clear the alarm enable flag
                        // and go to the Alarm None state.
                        FLAG_SMOKE_ALARM_ACTIVE = 0;

                        #ifndef CONFIGURE_UNDEFINED
                            // TODO: Undefine Later  BC: Test Only
                            SER_Send_String("Slave SMK Cancel...");
                            SER_Send_Prompt();
                        #endif

                        #ifdef CONFIGURE_UNDEFINED
                            // This should not be needed for Slave Alarm State
                            if(FLAG_GATEWAY_REMOTE_TO_SLAVE)
                            {
                                // Init Delay Timer since this was also a 
                                //  R-to-S gateway Connection
                                alm_init_clear_delay
                                        (TIMER_SMK_ALM_CLEAR_15_SECS);

                                // Allow Network Alarms to Clear
                                alm_state = ALM_STATE_SMK_CLR;

                                return MAIN_Make_Return_Value
                                        (ALARM_PRIORITY_INTERVAL_MS);
                            }
                        #endif

                        // This should delay Alarm Status Clear when exiting
                        //  Slave Smoke
                        FLAG_SMK_ALMPRIO_TRANS = 1;

                        // Clear this immediately
                        FLAG_SLAVE_SMOKE_ACTIVE = 0;


                        // Start a Remote Alarm Inhibit Timer before
                        //  returning to Alarm None (to allow any remote alarms
                        // to clear from potential Gateway HW Interconnect 
                        // alarms)
                        alm_init_remote_inhibit_timer();
                        
                        // Prevent HW INT CO received to be cleared in 
                        //  Alarm Init state
                        FLAG_RST_INT_CO_RCVD = 0;

                        alm_state = ALM_STATE_ALARM_INIT;

                    }
                #endif

                else if( (uchar)(MAIN_OneSecTimer - alm_start_time) >
                         (uchar)TIMER_SMK_SLAVE_ALARM_TIMEOUT)
                {
                    
                    // Timeout has occurred.  
                    //  Clear the alarm enable flag and goto the 
                    //  Alarm None state.
                    FLAG_SMOKE_ALARM_ACTIVE = 0;
                    

                    #ifdef CONFIGURE_WIRELESS_MODULE
                        // For PTTs, this typically happens under 15 seconds
                        if(FLAG_INTCON_CO_RECEIVED && FLAG_SLAVE_PTT_DETECTED)
                        {
                            #ifdef CONFIGURE_UNDEFINED
                                // Send Remote PTT request
                                APP_PTT_Local();
                            #endif
                    
                            // If Slave PTT detected, go straight into Slave CO
                            //  Alarm state with Gateway turned Off
                            FLAG_SLAVE_PTT_DETECTED = 0;
                            FLAG_SLAVE_ALARM_MODE = 0;
                            
                            // For this condition, Insure Gateway R-to-S OFF
                            FLAG_WL_GATEWAY_ENABLE = 1;
                            FLAG_GATEWAY_REMOTE_TO_SLAVE = 0;
                            
                            FLAG_GATEWAY_SLV_TO_REMOTE_OFF = 1;
                            
                            // A CO alarm was received via interconnect
                            // Set alarm and go to slave state
                            FLAG_SLAVE_ALARM_MODE = 1;
                            FLAG_REMOTE_ALARM_MODE = 0;
                            FLAG_CO_ALARM_ACTIVE = 1;
                            FLAG_SLAVE_CO_ACTIVE = 1;

                            // Terminating Slave Smoke state for PTT detect
                            FLAG_SLAVE_SMOKE_ACTIVE = 0;
                            
                            #ifdef CONFIGURE_VOICE
                                FLAG_PLAY_VOICE = 1;
                            #endif

                            alm_state = ALM_ALARM_SLAVE_CO;
                            
                            return MAIN_Make_Return_Value
                                    (ALARM_PRIORITY_INTERVAL_MS);
                        }
                        
                        
                        // Clear this immediately
                        FLAG_SLAVE_SMOKE_ACTIVE = 0;
                        
                        FLAG_SMK_ALMPRIO_TRANS = 1;
                        
                        // Start a Remote Alarm Inhibit Timer before
                        //  returning to Alarm None (to allow any remote alarms
                        // to clear from potential Gateway HW Interconnect 
                        // alarms)
                        alm_init_remote_inhibit_timer();
                        
                    #endif
                    
                    // Prevent HW INT CO received to be cleared in 
                    //  Alarm Init state
                    FLAG_RST_INT_CO_RCVD = 0;
                        
                    alm_state = ALM_STATE_ALARM_INIT;
                    
                }
                else
                {
                    #ifdef CONFIGURE_DEBUG_ALM_PRIORITY_STATES
                        // Test Only
                        PIC_delay_us(DBUG_DLY_50_USec);
                        for(uchar i=(MAIN_OneSecTimer-alm_start_time);i>0;i--)
                        {
                            // Test Only
                            MAIN_TP2_On();
                            PIC_delay_us(DBUG_DLY_25_USec);
                            MAIN_TP2_Off();
                        }
                    #endif

                    // Here we're in Slave Smoke and we're waiting
                    // for the timeout to occur.

                }

            #endif

        break;

        case ALM_ALARM_SLAVE_CO:
        {
                
            // Clear SMK Transition Flag for cases where it may have been 
            //  left on
            FLAG_SMK_ALMPRIO_TRANS = 0; 
            
            // Stop this timer while in Slave CO
            FLAG_CO_ALARM_OFF_TIMER = 0;
            
            //
            // Service Remote Inhibit Timer to allow it to complete
            //    if active
            //
            #ifdef CONFIGURE_WIRELESS_MODULE
              #ifdef CONFIGURE_INTERCONNECT
                alm_remote_check_inhibit_timer();
              #endif
            #endif

            
            #ifdef CONFIGURE_INTERCONNECT

                #ifdef CONFIGURE_WIRELESS_MODULE
                    // Clear Gateway INT Activity detected Flag
                    //  no longer required once slave alarm is detected
                    FLAG_INT_ACTIVITY_DETECTED = 0;
                    FLAG_INT_CO_ACTIVITY_DETECTED = 0;
                #endif

                #ifdef CONFIGURE_WIRELESS_ALARM_LOCATE
                    if(FLAG_ALARM_LOCATE_MODE)
                    {
                        // Test for Locate Timeout
                        alm_locate_timer();
                        
                        // Alarm Locate Mode Set
                        // Halt Remote/Slave Alarms during Locate
                        FLAG_CO_ALARM_ACTIVE = 0;
                        
                        // Clear any cancels as a result of Locate Entry
                        FLAG_CNCL_SMK_CO_ALARM = 0;
                        
                        // Keep Track of Incoming Slave Alarms for
                        //  when Locate Times Out
                        if(FLAG_INTCON_CO_RECEIVED)
                        {
                            // If still receiving Interconnect Smoke Signal
                            //  Refresh alarm timeout timer.
                            alm_init_slave_timer();

                            FLAG_INTCON_CO_RECEIVED = 0;
                        }
                        
                        /*
                         *  If we quit receiving Slave CO Signals, then
                         *  end Slave CO RF transmissions 
                         *  If we continue Receiving Slave CO alarms
                         *  Then allow them to be passed to Network Bridge
                         */
                        if( (uchar)(MAIN_OneSecTimer - alm_start_time) >
                                 (uchar)TIMER_LOC_SLAVE_ALARM_TIMEOUT)
                        {
                            // Allow Alarm Status Clear during Locate
                            FLAG_GATEWAY_SLV_TO_REMOTE_OFF = 1;
                        }
                        
                        // If Smoke or CO is detected while in locate
                        //  Transition to Alarm Master, but leave Locate Mode
                        //  active to alarm locally only
                        if(FLAG_MASTER_SMOKE_ACTIVE)
                        {
                            FLAG_SLAVE_ALARM_MODE = 0;
                            FLAG_REMOTE_ALARM_MODE = 0;
                            FLAG_SMOKE_ALARM_ACTIVE = 1;
                            
                            if(!FLAG_PTT_ACTIVE)
                            {
                                // Alarm Memory set if this is a real smoke 
                                //  event
                                FLAG_SMOKE_ALARM_MEMORY = 1;

                                #ifdef CONFIGURE_DIAG_HIST
                                    // Record this Smoke Event upon entry
                                    DIAG_Hist_Que_Push(HIST_SMK_ALARM_ON_EVENT);
                                #endif
                            }
                            
                            // Exiting Slave CO state
                            FLAG_SLAVE_CO_ACTIVE = 0;

                            // The Green LED task is reset so that initiating
                            //  alarm can be started immediately
                            MAIN_Reset_Task(TASKID_GLED_MANAGER);

                            #ifdef CONFIGURE_VOICE
                                // Initialize Voice to Play between temporal 
                                //  patterns
                                FLAG_PLAY_VOICE = 1;
                            #endif

                            #ifdef CONFIGURE_WIRELESS_MODULE
                                #ifdef CONFIGURE_INTERCONNECT  
                                    // Enable Interconnect Drive Out (no Delay)
                                    FLAG_ALM_INT_DRV_ENABLE = 1;
                                #endif
                            #endif
                            
                            alm_state = ALM_STATE_MASTER_SMOKE;

                        }
                        else if(FLAG_CO_ALARM_CONDITION)
                        {
                            //
                            // Master CO alarm sounds the CO temporal pattern.
                            //
                            FLAG_SLAVE_ALARM_MODE = 0;
                            FLAG_REMOTE_ALARM_MODE = 0;
                            
                            // Exiting Slave CO state
                            FLAG_SLAVE_CO_ACTIVE = 0;
                            
                            if(!FLAG_PTT_ACTIVE)
                            {
                                FLAG_CO_ALARM_MEMORY = 1;
                                
                                #ifdef CONFIGURE_DIAG_HIST
                                    DIAG_Hist_Que_Push(HIST_ALARM_ON_EVENT);
                                #endif
                            }
                            // Enable CO alarm T4 pattern
                            FLAG_CO_ALARM_ACTIVE = 1;
                            
                            
                            MAIN_Reset_Task(TASKID_GLED_MANAGER);

                            #ifdef CONFIGURE_VOICE
                                FLAG_PLAY_VOICE = 1;
                            #endif

                             alm_state = ALM_ALARM_MASTER_CO;
                        }    
                        
                        #ifdef CONFIGURE_UNDEFINED
                            // Test Only
                            SER_Send_String("CO Slave LOCATE ");
                            SER_Send_Prompt();
                        
                        #endif

                        return MAIN_Make_Return_Value
                                        (ALM_1_SEC_RETURN_TIME);
                        
                    }
                
                #endif
                    
                if(FLAG_MASTER_SMOKE_ACTIVE || FLAG_INTCON_SMOKE_RECEIVED ||
                   FLAG_REMOTE_SMOKE_ACTIVE || FLAG_REMOTE_HW_SMOKE_ACTIVE)
                {
                    
                    // Exit Slave CO if either smoke condition is detected
                    FLAG_INTCON_CO_RECEIVED = 0;
                    FLAG_SLAVE_CO_ACTIVE = 0;
                    FLAG_CO_ALARM_ACTIVE = 0;
                    
                    // Reset potential Wireless Bat Conserve logic
                    FLAG_WL_ALM_BAT_CONSERVE = 0;
                    
                    // Return to Alarm None with Remote CO alarms cleared
                    FLAG_REMOTE_CO_ACTIVE = 0;
                    FLAG_REMOTE_HW_CO_ACTIVE = 0;
                     
                    if(FLAG_REMOTE_SMOKE_ACTIVE)
                    {
                        
                        // Re-Init Gateway Flags if received Remote Smoke 
                        //  Interrupted this CO State
                        FLAG_WL_GATEWAY_ENABLE = 0;
                        FLAG_GATEWAY_REMOTE_TO_SLAVE = 0;
                        
                        FLAG_INT_ACTIVITY_DETECTED = 0;
                        FLAG_INT_CO_ACTIVITY_DETECTED = 0;
                        
                        #ifdef CONFIGURE_UNDEFINED
                            // TODO: Undefine Later  BC: Test Only 
                            SER_Send_String("Init Gateway...");
                            SER_Send_Prompt();
                        #endif

                        // Remote to Slave gateway has been determined

                        // Set Remote Smoke Alarm Mode flags
                        FLAG_SLAVE_ALARM_MODE = 0;
                        FLAG_REMOTE_ALARM_MODE = 1;

                        // Start the Smoke Alarm T3 pattern
                        if(!FLAG_ALARM_LOCATE_MODE)
                        {
                            FLAG_SMOKE_ALARM_ACTIVE = 1;
                        }

                        alm_state = ALM_ALARM_REMOTE_SMOKE;

                    }
                    
                    if(FLAG_REMOTE_HW_SMOKE_ACTIVE)
                    {
                        
                        // Re-Init Gateway Flags if received Remote Smoke 
                        //  Interrupted this CO State
                        FLAG_WL_GATEWAY_ENABLE = 0;
                        FLAG_GATEWAY_REMOTE_TO_SLAVE = 0;
                        
                        FLAG_INT_ACTIVITY_DETECTED = 0;
                        FLAG_INT_CO_ACTIVITY_DETECTED = 0;
                        
                        #ifdef CONFIGURE_UNDEFINED
                            // TODO: Undefine Later  BC: Test Only 
                            SER_Send_String("Init Gateway...");
                            SER_Send_Prompt();
                        #endif

                        // Remote to Slave gateway has been determined

                        // Set Remote Smoke Alarm Mode flags
                        FLAG_SLAVE_ALARM_MODE = 0;
                        FLAG_REMOTE_ALARM_MODE = 1;

                        // Start the Smoke Alarm T3 pattern
                        if(!FLAG_ALARM_LOCATE_MODE)
                        {
                            FLAG_SMOKE_ALARM_ACTIVE = 1;
                        }

                        alm_state = ALM_ALARM_REMOTE_SMOKE_HW;

                    }
                    
                    if(FLAG_INTCON_SMOKE_RECEIVED)
                    {
                        
                      #ifdef CONFIGURE_UNDEFINED  
                        
                        // If Slave CO interrupted by Slave Smoke
                        //  then this is not a Slave PTT sequence
                        //  set FLAG to indicate this 
                        FLAG_INT_SMK_INTERRUPTED_CO = 1;
                        
                        #ifdef CONFIGURE_WIRELESS_MODULE
                            // Inhibit ARM message
                            FLAG_DO_NOT_ARM = 1;
                        #endif

                      #else                        
      
                        // Receiving Interconnect Smoke Signal
                        // Transition straight into Alarm Smoke Slave State
                        FLAG_REMOTE_ALARM_MODE = 0;
                        FLAG_SLAVE_ALARM_MODE = 1;
                        FLAG_SLAVE_SMOKE_ACTIVE = 1;

                        #ifdef CONFIGURE_WIRELESS_MODULE
                            // Hold off the PTT detection timer
                            alm_smoke_alarm_time = TIME_SMK_ALM_60_SECS;
                        #endif
                        
                        // Init Alarm Exit Timeout Timer (i.e slave timer)
                        alm_init_slave_timer();
                        
                        #ifdef CONFIGURE_UNDEFINED
                            #ifdef CONFIGURE_WIRELESS_MODULE
                                #ifdef	CONFIGURE_INTERCONNECT

                                    //This Timer delays start of Remote Alarm
                                    // transmissions after entering Slave Alarm
                                    APP_Init_Slave_To_Remote_Alarm_Timer
                                        (TIMER_SLAVE_TO_REMOTE_DLY_14_SEC);

                                #endif
                            #endif
                        #endif
                        
                        // Proceed directly to Slave Smoke 
                        //  therefore the ARM message is not sent
                        alm_state = ALM_ALARM_SLAVE_SMOKE;

                        break;
                        
                      #endif
                            
                    }
                    else
                    {
                        FLAG_SLAVE_ALARM_MODE = 0;
                    }
                    
                    #ifdef CONFIGURE_WIRELESS_MODULE
                        /*
                         * This flag is set to prevent Alarm Status Clear from 
                         *  being sent via CCI to the RF Module during 
                         *  transition.
                         * Alarm Status Clear will take network out of Real
                         *  time mode causing problems with remote alarms
                         * 
                         * It is cleared once transition is complete
                         */
                        FLAG_SMK_ALMPRIO_TRANS = 1;
                        
                        #ifdef CONFIGURE_UNDEFINED
                            // TODO: Undefine Later  BC: Test Only
                            SER_Send_String(" SMK Transition Active SCO");
                            SER_Send_Prompt();
                        #endif
                    #endif
                    
                    // Proceed directly to Alarm None on Smoke override
                    alm_state = ALM_STATE_ALARM_NONE;
                    
                    MAIN_Reschedule_Int_Active
                        (TASKID_APP_STAT_MONITOR, ALM_RESCHEDULE_TIME_100_MS);

                    // Return on next TIC to speed alarm re-entry
                    return MAIN_Make_Return_Value
                                (ALARM_PRIORITY_INTERVAL_10_MS);
                
                }
                    
                // Check for Master CO Override 
                else if(FLAG_CO_ALARM_CONDITION)
                {
                    //
                    // Master CO alarm sounds the CO temporal pattern.
                    //
                    FLAG_SLAVE_ALARM_MODE = 0;
                    FLAG_REMOTE_ALARM_MODE = 0;
                    
                    // Exiting Slave CO state
                    FLAG_SLAVE_CO_ACTIVE = 0;

                    if(!FLAG_PTT_ACTIVE)
                    {
                        FLAG_CO_ALARM_MEMORY = 1;

                        #ifdef CONFIGURE_DIAG_HIST
                            DIAG_Hist_Que_Push(HIST_ALARM_ON_EVENT);
                        #endif
                    }
                    // Enable CO alarm T4 pattern
                    FLAG_CO_ALARM_ACTIVE = 1;
                    
                    MAIN_Reset_Task(TASKID_GLED_MANAGER);

                    #ifdef CONFIGURE_VOICE
                        FLAG_PLAY_VOICE = 1;
                    #endif

                     alm_state = ALM_ALARM_MASTER_CO;
                }    
                    
                else if(FLAG_INTCON_CO_RECEIVED)
                {
                    // Another Hardwire CO message was received.
                    // Refresh the timeout timer
                    
                    // Insure CO alarm is Active in case Locate Ended
                    FLAG_CO_ALARM_ACTIVE = 1;

                    // timer and check for battery conserve mode.
                    alm_init_slave_timer();

                    FLAG_INTCON_CO_RECEIVED = 0;

                }
                    
                #ifdef CONFIGURE_WIRELESS_MODULE
                    // Look for Alarm Cancel instead of Timeout    
                    else if(FLAG_CNCL_SMK_CO_ALARM)
                    {
                        // One time event
                        FLAG_CNCL_SMK_CO_ALARM = 0;

                        // If the alarm timeout timer has expired, exit CO
                        //  remote state.
                        FLAG_CO_ALARM_ACTIVE = 0;

                        #ifndef CONFIGURE_UNDEFINED
                            // TODO: Undefine Later  BC: Test Only
                            SER_Send_String("Slave CO Cancel...");
                            SER_Send_Prompt();
                        #endif


                        #ifdef CONFIGURE_UNDEFINED
                            // Should not be needed for Slave CO State 

                            if(FLAG_GATEWAY_REMOTE_TO_SLAVE)
                            {
                                // Init Delay Timer since this was also a 
                                //  R-to-S gateway Connection   
                                
                                // Init Delay Timer
                                alm_init_clear_delay
                                        (TIMER_SMK_ALM_CLEAR_15_SECS);
                                
                                // Allow Network Alarms to Clear
                                alm_state = ALM_STATE_CO_CLR;

                                return MAIN_Make_Return_Value
                                        (ALARM_PRIORITY_INTERVAL_MS);
                            }
                        #endif


                        // Reset potential Wireless Bat Conserve logic
                        FLAG_WL_ALM_BAT_CONSERVE = 0;
                        
                        // Clear this immediately
                        FLAG_SLAVE_CO_ACTIVE = 0;

                        #ifdef CONFIGURE_WIRELESS_MODULE
                            // This should delay Alarm Status Clear when 
                            // exiting
                            FLAG_SMK_ALMPRIO_TRANS = 1;

                            // Start a Remote Alarm Inhibit Timer before
                            //  returning to Alarm None (to allow any remote 
                            // alarms to clear from potential Gateway 
                            // HW Interconnect alarms)
                            alm_init_remote_inhibit_timer();
                        #endif

                        // return to Alarm None immediately 
                        alm_state = ALM_STATE_ALARM_INIT;

                    }
                #endif

                else if( (uchar)(MAIN_OneSecTimer - alm_start_time) >
                         (uchar)TIMER_CO_SLAVE_ALARM_TIMEOUT )
                {
                    
                    FLAG_CO_ALARM_ACTIVE = 0;
                    
                    // Reset potential Wireless Bat Conserve logic
                    FLAG_WL_ALM_BAT_CONSERVE = 0;
                    
                    
                    #ifdef CONFIGURE_WIRELESS_MODULE

                        // Begin Timing CO OFF with timer
                        alm_init_CO_alarm_off_timer();
                        
                        FLAG_SMK_ALMPRIO_TRANS = 1;
                    
                        // Start a Remote Alarm Inhibit Timer before
                        //  returning to Alarm None (to allow any remote alarms
                        // to clear from potential Gateway HW Interconnect 
                        // alarms)
                        alm_init_remote_inhibit_timer();
                    #endif
                        
                    // If the alarm timeout timer has expired,
                    //   exit CO slave mode.
                    alm_state = ALM_STATE_ALARM_INIT;
                    
                }
                else
                {
                    #ifdef CONFIGURE_DEBUG_ALM_PRIORITY_STATES
                    // Test Only
                    PIC_delay_us(DBUG_DLY_50_USec);
                        for(uchar i=(MAIN_OneSecTimer-alm_start_time);i>0;i--)
                        {
                            // Test Only
                            MAIN_TP2_On();
                            PIC_delay_us(DBUG_DLY_25_USec);
                            MAIN_TP2_Off();
                        }
                    #endif

                 }
            #endif
        }
        break;

        case ALM_ALARM_REMOTE_SMOKE:
                    
            #ifdef CONFIGURE_WIRELESS_MODULE

                #ifdef CONFIGURE_UNDEFINED
                    // TODO: Undefine Later  BC: Test Only
                    if(FLAG_SMK_ALMPRIO_TRANS)
                    {
                        SER_Send_String("SMK_Transition_OFF_RS");
                        SER_Send_Prompt();
                    }
                #endif
                // Remote Smoke from CO Transition Completed
                FLAG_SMK_ALMPRIO_TRANS = 0;
            
                #ifdef CONFIGURE_WIRELESS_ALARM_LOCATE
                    if(FLAG_ALARM_LOCATE_MODE)
                    {
                        // Test for Locate Timeout
                        alm_locate_timer();
                        
                        // Alarm Locate Mode Set
                        // Halt Remote/Slave Alarms during Locate
                        FLAG_SMOKE_ALARM_ACTIVE = 0;
                        
                        // Clear any cancels as a result of Locate Entry
                        FLAG_CNCL_SMK_CO_ALARM = 0;
                        
                        // Keep Track of Incoming Slave Alarms for
                        //  when Locate Times Out
                        if(FLAG_REMOTE_SMOKE_ACTIVE || 
                                FLAG_REFRESH_REMOTE_STATE)
                        {
                            // If still receiving Smoke Signal
                            //  Refresh alarm timeout timer.
                            alm_init_slave_timer();

                            FLAG_REMOTE_SMOKE_ACTIVE = 0;
                            FLAG_REFRESH_REMOTE_STATE = 0;
                        }
                        
                        // If Smoke is detected while in locate
                        //  Transition to Smoke Master, but leave Locate Mode
                        //  active to alarm locally only
                        if(FLAG_MASTER_SMOKE_ACTIVE)
                        {
                            FLAG_SLAVE_ALARM_MODE = 0;
                            FLAG_REMOTE_ALARM_MODE = 0;
                            FLAG_SMOKE_ALARM_ACTIVE = 1;
                            
                            if(!FLAG_PTT_ACTIVE)
                            {
                                // Alarm Memory set if this is a real smoke 
                                //  event
                                FLAG_SMOKE_ALARM_MEMORY = 1;

                                #ifdef CONFIGURE_DIAG_HIST
                                    // Record this Smoke Event upon entry
                                    DIAG_Hist_Que_Push(HIST_SMK_ALARM_ON_EVENT);
                                #endif
                            }

                            // The Green LED task is reset so that initiating
                            //  alarm can be started immediately
                            MAIN_Reset_Task(TASKID_GLED_MANAGER);

                            #ifdef CONFIGURE_VOICE
                                // Initialize Voice to Play between temporal 
                                //  patterns
                                FLAG_PLAY_VOICE = 1;
                            #endif

                            #ifdef CONFIGURE_WIRELESS_MODULE
                                #ifdef CONFIGURE_INTERCONNECT  
                                    // Enable Interconnect Drive Out (no Delay)
                                    FLAG_ALM_INT_DRV_ENABLE = 1;
                                #endif
                            #endif

                            alm_state = ALM_STATE_MASTER_SMOKE;
                            
                            return MAIN_Make_Return_Value
                                        (ALARM_PRIORITY_INTERVAL_MS);

                        }
                        
                        return MAIN_Make_Return_Value
                                        (ALM_500_MS_RETURN_TIME);
                    }
                
                #endif


                // Received a Remote Hush Request, Exit through Smoke Delay
                //  to allow network faster response, if Hush is not possible
                //  i.e Too Much Smoke, Smoke Alarm state will be re-entered    
                if(FLAG_SMK_EXIT_TO_HUSH)
                {
                    FLAG_SMK_EXIT_TO_HUSH = 0;
                    FLAG_SMOKE_ALARM_ACTIVE = 0;

                     // Init Delay Timer
                    alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_15_SECS);

                    // Allows Network Remotes to Clear before looking for
                    //  a new Alarm Condition
                    alm_state = ALM_STATE_SMK_CLR;
                    
                    // Allow HW INT CO received to be cleared in 
                    // Alarm Init state
                    FLAG_RST_INT_CO_RCVD = 1;

                    return MAIN_Make_Return_Value(ALARM_PRIORITY_INTERVAL_MS);

                }
                
                #ifndef CONFIGURE_INTERCONNECT
                    // If Smoke is detected while in Remote and not a Gateway
                    //  Model (DC wireless) Transition to Smoke Master 
                    if(FLAG_MASTER_SMOKE_ACTIVE)
                    {
                        FLAG_SLAVE_ALARM_MODE = 0;
                        FLAG_REMOTE_ALARM_MODE = 0;
                        FLAG_SMOKE_ALARM_ACTIVE = 1;
                        
                        if(!FLAG_PTT_ACTIVE)
                        {
                            // Alarm Memory set if this is a real smoke 
                            //  event
                            FLAG_SMOKE_ALARM_MEMORY = 1;

                            #ifdef CONFIGURE_DIAG_HIST
                                // Record this Smoke Event upon entry
                                DIAG_Hist_Que_Push(HIST_SMK_ALARM_ON_EVENT);
                            #endif
                        }

                        // The Green LED task is reset so that initiating
                        //  alarm can be started immediately
                        MAIN_Reset_Task(TASKID_GLED_MANAGER);

                        #ifdef CONFIGURE_VOICE
                            // Initialize Voice to Play between temporal 
                            //  patterns
                            FLAG_PLAY_VOICE = 1;
                        #endif

                        alm_state = ALM_STATE_MASTER_SMOKE;

                        return MAIN_Make_Return_Value
                                    (ALARM_PRIORITY_INTERVAL_MS);

                    }
                #endif
            
                #ifdef CONFIGURE_INTERCONNECT
                    // Since Slave Smoke has priority over Remote Smoke
                    //  Test for received HW Interconnect Smoke Alarm
                    if(FLAG_INTCON_SMOKE_RECEIVED)
                    {
                        // Receiving Interconnect Smoke Signal
                        // Transition straight into Alarm Smoke Slave State
                        FLAG_REMOTE_ALARM_MODE = 0;
                        FLAG_SLAVE_ALARM_MODE = 1;
                        FLAG_SLAVE_SMOKE_ACTIVE = 1;

                        // Hold off the PTT detection timer
                        alm_smoke_alarm_time = TIME_SMK_ALM_60_SECS;
                        
                        // Init Alarm Exit Timeout Timer (i.e slave timer)
                        alm_init_slave_timer();
                        
                        #ifdef CONFIGURE_UNDEFINED
                            #ifdef CONFIGURE_WIRELESS_MODULE
                                #ifdef	CONFIGURE_INTERCONNECT

                                    //This Timer delays start of Remote Alarm
                                    // transmissions after entering Slave Alarm
                                    APP_Init_Slave_To_Remote_Alarm_Timer
                                        (TIMER_SLAVE_TO_REMOTE_DLY_14_SEC);

                                #endif
                            #endif
                        #endif
                        
                        // Proceed directly to Slave Smoke 
                        //  therefore the ARM message is not sent
                        alm_state = ALM_ALARM_SLAVE_SMOKE;

                        break;
                    }
                #endif
                
                if(FLAG_REMOTE_SMOKE_ACTIVE || FLAG_REFRESH_REMOTE_STATE)
                {
                    // Insure Smoke alarm is Active in case Locate Ended
                    FLAG_SMOKE_ALARM_ACTIVE = 1;
                    
                    //
                    // Refresh alarm timeout timer.
                    //
                    alm_init_slave_timer();
                    FLAG_REMOTE_SMOKE_ACTIVE = 0;
                    FLAG_REFRESH_REMOTE_STATE = 0;
                    
                    #ifdef CONFIGURE_SER_ALM_TIMER
                        alm_sec_count = 1;
                    #endif
                    
                }

                // Look For Alarm Cancel instead of Timeout
                else if(FLAG_CNCL_SMK_CO_ALARM)
                {
                    // One time event
                    FLAG_CNCL_SMK_CO_ALARM = 0;

                    // Cancel has occurred.  Clear the alarm enable flag
                    // and go to the Alarm None state.
                    FLAG_SMOKE_ALARM_ACTIVE = 0;
                    
                    // Allow HW INT CO received to be cleared in 
                    // Alarm Init state
                    FLAG_RST_INT_CO_RCVD = 1;

                    #ifndef CONFIGURE_UNDEFINED
                        // TODO: Undefine Later  BC: Test Only
                        SER_Send_String("Remote SMK Cancel...");
                        SER_Send_Prompt();
                    #endif

                    #ifdef CONFIGURE_INTERCONNECT   
                        // This code is redundant - can be removed....
                        if(FLAG_GATEWAY_REMOTE_TO_SLAVE)
                        {
                            // Init Delay Timer since this was also a R-to-S
                            //  gateway Connection
                            alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_35_SECS);

                            // Allow Network Alarms to Clear
                            alm_state = ALM_STATE_SMK_CLR;

                            return MAIN_Make_Return_Value
                                    (ALARM_PRIORITY_INTERVAL_MS);
                        }
                    #endif

                    // Return to Alarm Delay (even on Cancel)
                    alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_35_SECS);
                    alm_state = ALM_STATE_SMK_CLR;

                }
                
                else if( (uchar)(MAIN_OneSecTimer - alm_start_time) >=
                         (uchar)TIMER_SMK_RMT_ALARM_14_SEC)
                {
                    // Timeout has occurred.  Clear the alarm enable flag
                    // and go to the Alarm None state.
                    FLAG_SMOKE_ALARM_ACTIVE = 0;
                    
                    // Allow HW INT CO received to be cleared in 
                    // Alarm Init state
                    FLAG_RST_INT_CO_RCVD = 1;
                    
                    #ifndef CONFIGURE_UNDEFINED
                        // TODO: Undefine Later  BC: Test Only
                        SER_Send_String("Remote SMK Timeout...");
                        SER_Send_Prompt();
                    #endif
                    
                    #ifdef CONFIGURE_INTERCONNECT     
                        if(FLAG_GATEWAY_REMOTE_TO_SLAVE)
                        {
                            // Init Delay Timer since this was also a R-to-S
                            //  gateway Connection
                            alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_35_SECS);

                            // Allow Network Alarms to Clear
                            alm_state = ALM_STATE_SMK_CLR;

                            return MAIN_Make_Return_Value
                                    (ALARM_PRIORITY_INTERVAL_MS);
                        }
                    #endif 
                        
                    // Return to Alarm Delay (
                    alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_35_SECS);
                    alm_state = ALM_STATE_SMK_CLR;
                    
                }
                else
                {
                    #ifdef CONFIGURE_DEBUG_ALM_PRIORITY_STATES
                        // Test Only
                        PIC_delay_us(DBUG_DLY_50_USec);
                        for(uchar i=(MAIN_OneSecTimer-alm_start_time);i>0;i--)
                        {
                            // Test Only
                            MAIN_TP2_On();
                            PIC_delay_us(DBUG_DLY_25_USec);
                            MAIN_TP2_Off();
                        }
                    #endif

                    #ifdef CONFIGURE_SER_ALM_TIMER
                        // Here we're in remote smoke and we're waiting
                        //  for the timeout to occur.
                        if(0 == --alm_sec_count)
                        {
                            alm_sec_count = 10;

                            SER_Send_String("Time -");
                            SER_Send_Int(' ',(uchar)(MAIN_OneSecTimer -
                                                    alm_start_time),10);
                            SER_Send_Prompt();
                        }
                    #endif

                }

            #endif

        break;


        case ALM_ALARM_REMOTE_CO:

            // Clear SMK Transition Flag for cases where it may have been 
            //  left on
            FLAG_SMK_ALMPRIO_TRANS = 0; 
            
            #ifdef CONFIGURE_WIRELESS_MODULE

                #ifdef CONFIGURE_WIRELESS_ALARM_LOCATE
                    if(FLAG_ALARM_LOCATE_MODE)
                    {
                        // Test for Locate Timeout
                        alm_locate_timer();

                        // Alarm Locate Mode Set
                        // Halt Remote/Slave Alarms during Locate
                        FLAG_CO_ALARM_ACTIVE = 0;
                        
                        // Clear any cancels as a result of Locate Entry
                        FLAG_CNCL_SMK_CO_ALARM = 0;

                        // Keep Track of Incoming Slave Alarms for
                        //  when Locate Times Out
                        if(FLAG_REMOTE_CO_ACTIVE || FLAG_REFRESH_REMOTE_STATE)
                        {
                            // If still receiving Interconnect Smoke Signal
                            //  Refresh alarm timeout timer.
                            alm_init_slave_timer();

                            FLAG_REMOTE_CO_ACTIVE = 0;
                            FLAG_REFRESH_REMOTE_STATE = 0;
                        }

                        // If Smoke or CO is detected while in locate
                        //  Transition to Alarm Master, but leave Locate Mode
                        //  active to alarm locally only
                        if(FLAG_MASTER_SMOKE_ACTIVE)
                        {
                            FLAG_SLAVE_ALARM_MODE = 0;
                            FLAG_REMOTE_ALARM_MODE = 0;
                            FLAG_SMOKE_ALARM_ACTIVE = 1;
                            
                            if(!FLAG_PTT_ACTIVE)
                            {
                                // Alarm Memory set if this is a real smoke 
                                //  event
                                FLAG_SMOKE_ALARM_MEMORY = 1;

                                #ifdef CONFIGURE_DIAG_HIST
                                    // Record this Smoke Event upon entry
                                    DIAG_Hist_Que_Push(HIST_SMK_ALARM_ON_EVENT);
                                #endif
                            }

                            // The Green LED task is reset so that initiating
                            //  alarm can be started immediately
                            MAIN_Reset_Task(TASKID_GLED_MANAGER);

                            #ifdef CONFIGURE_VOICE
                                // Initialize Voice to Play between temporal 
                                //  patterns
                                FLAG_PLAY_VOICE = 1;
                            #endif

                            #ifdef CONFIGURE_WIRELESS_MODULE
                                #ifdef CONFIGURE_INTERCONNECT  
                                    // Enable Interconnect Drive Out (no Delay)
                                    FLAG_ALM_INT_DRV_ENABLE = 1;
                                #endif
                            #endif
                            
                            alm_state = ALM_STATE_MASTER_SMOKE;
                            
                             return MAIN_Make_Return_Value
                                        (ALARM_PRIORITY_INTERVAL_MS);

                        }
                        else if(FLAG_CO_ALARM_CONDITION)
                        {
                            //
                            // Master CO alarm sounds the CO temporal pattern.
                            //
                            FLAG_REMOTE_ALARM_MODE = 0;
                            
                            if(!FLAG_PTT_ACTIVE)
                            {
                                FLAG_CO_ALARM_MEMORY = 1;

                                #ifdef CONFIGURE_DIAG_HIST
                                    DIAG_Hist_Que_Push(HIST_ALARM_ON_EVENT);
                                #endif
                            }
                            // Enable CO alarm T4 pattern
                            FLAG_CO_ALARM_ACTIVE = 1;

                            MAIN_Reset_Task(TASKID_GLED_MANAGER);

                            #ifdef CONFIGURE_VOICE
                                FLAG_PLAY_VOICE = 1;
                            #endif

                             alm_state = ALM_ALARM_MASTER_CO;
                             
                             return MAIN_Make_Return_Value
                                        (ALARM_PRIORITY_INTERVAL_MS);

                        }    
                        
                        return MAIN_Make_Return_Value
                                        (ALM_500_MS_RETURN_TIME);
                    }
                    
                #endif

                /* Test for Higher Priority Alarms */
            
                // Test for Smoke since it has priority over CO
                if(FLAG_MASTER_SMOKE_ACTIVE ||
                    FLAG_INTCON_SMOKE_RECEIVED  ||
                    FLAG_REMOTE_SMOKE_ACTIVE || FLAG_REMOTE_HW_SMOKE_ACTIVE )
                {
                    // Exit Remote CO if any smoke condition is entered,
                    FLAG_CO_ALARM_ACTIVE = 0;
                    FLAG_REMOTE_CO_ACTIVE = 0;
                    
                    // No need to re-init Remote to Slave
                    //  gateway when transitioning from Remote CO 
                    FLAG_WL_GATEWAY_ENABLE = 1; 
                    
                    #ifndef CONFIGURE_UNDEFINE
                        // Actually we DO need to Reset Slave to Remote gateway
                        //  enable for the condition when Slave Smoke
                        //  overides Remote CO
                    
                        // If Smoke override is from HW INT signal
                        //  we should enable transmit to RF Module
                        if(FLAG_INTCON_SMOKE_RECEIVED)
                        {
                            // May need to act as a gateway to Network
                            FLAG_GATEWAY_SLV_TO_REMOTE_OFF = 0;
                        }
                    #endif
                    
                    FLAG_INTCON_CO_RECEIVED = 0;
                    FLAG_BAT_CONSRV_MODE_ACTIVE = 0;

                    FLAG_REMOTE_ALARM_MODE = 0;
                   
                    #ifdef CONFIGURE_WIRELESS_MODULE
                        // Do not Re-ARM from Remote CO condition
                        FLAG_DO_NOT_ARM = 1;
                    #endif
                    
                    #ifdef CONFIGURE_WIRELESS_MODULE
                        /*
                         * This flag is set to prevent Alarm Status Clear from 
                         *  being sent via CCI to the RF Module during 
                         *  transitions. Alarm Status Clear will take network 
                         *  out of Real time mode causing problems with remote 
                         *  alarms
                         */
                        FLAG_SMK_ALMPRIO_TRANS = 1;
                        
                        #ifdef CONFIGURE_UNDEFINED
                            // TODO: Undefine Later  BC: Test Only
                            SER_Send_String(" SMK Transition Active RCO");
                            SER_Send_Prompt();
                        #endif
                        
                    #endif

                    // Proceed directly to Alarm None on Smoke override
                    alm_state = ALM_STATE_ALARM_NONE;
              
                    MAIN_Reschedule_Int_Active
                        (TASKID_APP_STAT_MONITOR, ALM_RESCHEDULE_TIME_100_MS);

                    // Return on next TIC to speed alarm re-entry
                    return MAIN_Make_Return_Value
                                (ALARM_PRIORITY_INTERVAL_10_MS);
                    
                }

                if(FLAG_CO_ALARM_CONDITION)
                {
                    //
                    // Master CO alarm sounds the CO temporal pattern.
                    //
                    FLAG_REMOTE_ALARM_MODE = 0;

                    MAIN_Reset_Task(TASKID_GLED_MANAGER);

                    if(!FLAG_PTT_ACTIVE)
                    {
                        FLAG_CO_ALARM_MEMORY = 1;

                        #ifdef CONFIGURE_DIAG_HIST
                            DIAG_Hist_Que_Push(HIST_ALARM_ON_EVENT);
                        #endif
                    }
                    // Enable CO alarm T4 pattern
                    FLAG_CO_ALARM_ACTIVE = 1;

                     alm_state = ALM_ALARM_MASTER_CO;

                     return MAIN_Make_Return_Value
                                (ALARM_PRIORITY_INTERVAL_MS);
                }    
            
                #ifdef CONFIGURE_INTERCONNECT
                    // Since Slave CO has priority over Remote CO
                    //  Test for received HW Interconnect CO Alarm
                    else if(FLAG_INTCON_CO_RECEIVED)
                    {
                        // A CO alarm was received via interconnect
                        // Set alarm and go straight into slave state
                        FLAG_SLAVE_ALARM_MODE = 1;
                        FLAG_SLAVE_CO_ACTIVE = 1;
                        
                        FLAG_REMOTE_ALARM_MODE = 0;
                        FLAG_REMOTE_CO_ACTIVE = 0;
                        
                        FLAG_BAT_CONSRV_MODE_ACTIVE = 0;
                        
                        // Init Alarm Exit Timeout Timer (i.e slave timer)
                        alm_init_slave_timer();

                        
                        #ifdef CONFIGURE_UNDEFINE
                            // Don't think we want to reset Gateway Flag here
                            // May need to act as a gateway to Network
                            FLAG_GATEWAY_SLV_TO_REMOTE_OFF = 0;
                        #endif
                        
                        #ifdef CONFIGURE_UNDEFINED
                            #ifdef CONFIGURE_WIRELESS_MODULE
                                #ifdef	CONFIGURE_INTERCONNECT
                                    //This Timer delays start of Remote Alarm
                                    // transmissions after entering Slave Alarm
                                    APP_Init_Slave_To_Remote_Alarm_Timer
                                        (TIMER_SLAVE_TO_REMOTE_DLY_14_SEC);
                                #endif
                            #endif
                        #endif
                        
                        alm_state = ALM_ALARM_SLAVE_CO;
                    }
                #endif    

                else if(FLAG_REMOTE_CO_ACTIVE || FLAG_REFRESH_REMOTE_STATE)
                {
                    // Insure CO alarm is Active in case Locate Ended
                    FLAG_CO_ALARM_ACTIVE = 1;
                    
                    // Another Hardwire CO message was received.  Refresh
                    //  the timeout timer
                    
                    #ifdef CONFIGURE_UNDEFINED
                        // TODO: Undefine Later  BC: Test Only
                        SER_Send_String("RMT CO_Set...");
                        SER_Send_Prompt();
                    #endif

                    // and check for battery conserve mode.
                    alm_init_slave_timer();
                    FLAG_REMOTE_CO_ACTIVE = 0;
                    FLAG_REFRESH_REMOTE_STATE = 0;

                }

                // Look for Alarm Cancel instead of Timeout    
                else if(FLAG_CNCL_SMK_CO_ALARM)
                {
                    // One time event
                    FLAG_CNCL_SMK_CO_ALARM = 0;

                    // Allow HW INT CO received to be cleared in 
                    // Alarm Init state
                    FLAG_RST_INT_CO_RCVD = 1;
                    
                    // If the alarm timeout timer has expired, exit CO
                    //  remote state.
                    FLAG_CO_ALARM_ACTIVE = 0;

                    #ifndef CONFIGURE_UNDEFINED
                        // TODO: Undefine Later  BC: Test Only
                        SER_Send_String("Remote CO Cancel...");
                        SER_Send_Prompt();
                    #endif

                    #ifdef CONFIGURE_INTERCONNECT    
                        if(FLAG_GATEWAY_REMOTE_TO_SLAVE)
                        {
                            // Init Delay Timer since this was also a R-to-S
                            //  gateway Connection   

                            // Init Delay Timer
                            alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_35_SECS);

                            // Allow Network Alarms to Clear
                            alm_state = ALM_STATE_CO_CLR;

                            return MAIN_Make_Return_Value
                                    (ALARM_PRIORITY_INTERVAL_MS);
                        }
                    #endif

                    // Init Delay Timer, Allow Network Alarms to Clear
                    alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_35_SECS);
                    alm_state = ALM_STATE_CO_CLR;

                }
                    
                else if( (uchar)(MAIN_OneSecTimer - alm_start_time) >=
                         (uchar)TIMER_CO_RMT_ALARM_14_SEC)
                {
                    // If the alarm timeout timer has expired, exit CO
                    //  remote state.
                    FLAG_CO_ALARM_ACTIVE = 0;
                    
                    // Allow HW INT CO received to be cleared in 
                    // Alarm Init state
                    FLAG_RST_INT_CO_RCVD = 1;
                    
                    #ifndef CONFIGURE_UNDEFINED
                        // TODO: Undefine Later  BC: Test Only
                        SER_Send_String("Remote CO Timeout...");
                        SER_Send_Prompt();
                    #endif
                    
                    #ifdef CONFIGURE_INTERCONNECT    
                        if(FLAG_GATEWAY_REMOTE_TO_SLAVE)
                        {
                            // Init Delay Timer since this was also a R-to-S
                            //  gateway Connection   

                            // Init Delay Timer
                            alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_35_SECS);

                            // Allow Network Alarms to Clear
                            alm_state = ALM_STATE_CO_CLR;

                            return MAIN_Make_Return_Value
                                    (ALARM_PRIORITY_INTERVAL_MS);
                        }
                    #endif
                        
                    // Init Delay Timer, Allow Network Alarms to Clear
                    alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_35_SECS);
                    alm_state = ALM_STATE_CO_CLR;
                    
                }
                else
                {
                    #ifdef CONFIGURE_DEBUG_ALM_PRIORITY_STATES
                        // Test Code Only
                        PIC_delay_us(DBUG_DLY_50_USec);
                        for(uchar i=(MAIN_OneSecTimer-alm_start_time); i>0; i--)
                        {
                            // Test Only
                            MAIN_TP2_On();
                            PIC_delay_us(DBUG_DLY_25_USec);
                            MAIN_TP2_Off();
                        }
                    #endif

                }

            #endif

        break;
        
        

        case ALM_ALARM_REMOTE_SMOKE_HW:
                    
            #ifdef CONFIGURE_WIRELESS_MODULE

                // Clear SMK Transition Flag for cases where it may have been 
                //  left on
                FLAG_SMK_ALMPRIO_TRANS = 0; 
            
                #ifdef CONFIGURE_UNDEFINED
                    // TODO: Undefine Later  BC: Test Only
                    if(FLAG_SMK_ALMPRIO_TRANS)
                    {
                        SER_Send_String("SMK_Transition_OFF_RS");
                        SER_Send_Prompt();
                    }
                #endif
                // Remote Smoke from CO Transition Completed
                FLAG_SMK_ALMPRIO_TRANS = 0;
            
                #ifdef CONFIGURE_WIRELESS_ALARM_LOCATE
                    if(FLAG_ALARM_LOCATE_MODE)
                    {
                        // Test for Locate Timeout
                        alm_locate_timer();
                        
                        // Alarm Locate Mode Set
                        // Halt Remote/Slave Alarms during Locate
                        FLAG_SMOKE_ALARM_ACTIVE = 0;
                        
                        // Clear any cancels as a result of Locate Entry
                        FLAG_CNCL_SMK_CO_ALARM = 0;
                        
                        // Keep Track of Incoming Slave Alarms for
                        //  when Locate Times Out
                        if(FLAG_REMOTE_HW_SMOKE_ACTIVE || 
                                FLAG_REFRESH_REMOTE_STATE)
                        {
                            // If still receiving Smoke Signal
                            //  Refresh alarm timeout timer.
                            alm_init_slave_timer();

                            FLAG_REMOTE_HW_SMOKE_ACTIVE = 0;
                            FLAG_REFRESH_REMOTE_STATE = 0;
                        }
                        
                        // If Smoke is detected while in locate
                        //  Transition to Smoke Master, but leave Locate Mode
                        //  active to alarm locally only
                        if(FLAG_MASTER_SMOKE_ACTIVE)
                        {
                            FLAG_SLAVE_ALARM_MODE = 0;
                            FLAG_REMOTE_ALARM_MODE = 0;
                            FLAG_SMOKE_ALARM_ACTIVE = 1;
                            
                            if(!FLAG_PTT_ACTIVE)
                            {
                                // Alarm Memory set if this is a real smoke 
                                //  event
                                FLAG_SMOKE_ALARM_MEMORY = 1;

                                #ifdef CONFIGURE_DIAG_HIST
                                    // Record this Smoke Event upon entry
                                    DIAG_Hist_Que_Push(HIST_SMK_ALARM_ON_EVENT);
                                #endif
                            }

                            // The Green LED task is reset so that initiating
                            //  alarm can be started immediately
                            MAIN_Reset_Task(TASKID_GLED_MANAGER);

                            #ifdef CONFIGURE_VOICE
                                // Initialize Voice to Play between temporal 
                                //  patterns
                                FLAG_PLAY_VOICE = 1;
                            #endif

                            #ifdef CONFIGURE_WIRELESS_MODULE
                                #ifdef CONFIGURE_INTERCONNECT  
                                    // Enable Interconenct Drive Out (no Delay)
                                    FLAG_ALM_INT_DRV_ENABLE = 1;
                                #endif
                            #endif
                            
                            alm_state = ALM_STATE_MASTER_SMOKE;
                            
                            return MAIN_Make_Return_Value
                                        (ALARM_PRIORITY_INTERVAL_MS);

                        }
                        
                        return MAIN_Make_Return_Value
                                        (ALM_500_MS_RETURN_TIME);
                    }
                
                #endif


                // Received a Remote Hush Request, Exit through Smoke Delay
                //  to allow network faster response, if Hush is not possible
                //  i.e Too Much Smoke, Smoke Alarm state will be re-entered    
                if(FLAG_SMK_EXIT_TO_HUSH)
                {
                    FLAG_SMK_EXIT_TO_HUSH = 0;
                    FLAG_SMOKE_ALARM_ACTIVE = 0;

                     // Init Delay Timer
                    alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_15_SECS);

                    // Allows Network Remotes to Clear before looking for
                    //  a new Alarm Condition
                    alm_state = ALM_STATE_SMK_CLR;

                    // Allow HW INT CO received to be cleared in 
                    // Alarm Init state
                    FLAG_RST_INT_CO_RCVD = 1;
                    
                    return MAIN_Make_Return_Value(ALARM_PRIORITY_INTERVAL_MS);

                }

                /* Test for Higher Priority Smoke Alarms */
                
                // If Smoke is detected 
                //  Transition to Smoke Master
                if(FLAG_MASTER_SMOKE_ACTIVE)
                {
                    FLAG_SLAVE_ALARM_MODE = 0;
                    FLAG_REMOTE_ALARM_MODE = 0;
                    FLAG_REMOTE_HW_SMOKE_ACTIVE = 0;
                    FLAG_SMOKE_ALARM_ACTIVE = 1;
                    
                    if(!FLAG_PTT_ACTIVE)
                    {
                        // Alarm Memory set if this is a real smoke 
                        //  event
                        FLAG_SMOKE_ALARM_MEMORY = 1;

                        #ifdef CONFIGURE_DIAG_HIST
                            // Record this Smoke Event upon entry
                            DIAG_Hist_Que_Push(HIST_SMK_ALARM_ON_EVENT);
                        #endif
                    }

                    // The Green LED task is reset so that initiating
                    //  alarm can be started immediately
                    MAIN_Reset_Task(TASKID_GLED_MANAGER);

                    #ifdef CONFIGURE_VOICE
                        // Initialize Voice to Play between temporal 
                        //  patterns
                        FLAG_PLAY_VOICE = 1;
                    #endif

                    #ifdef CONFIGURE_WIRELESS_MODULE
                        #ifdef CONFIGURE_INTERCONNECT  
                            // Enable Interconnect Drive Out (no Delay)
                            FLAG_ALM_INT_DRV_ENABLE = 1;
                        #endif
                    #endif

                    alm_state = ALM_STATE_MASTER_SMOKE;

                    return MAIN_Make_Return_Value
                                (ALARM_PRIORITY_INTERVAL_MS);

                }
            
                #ifdef CONFIGURE_INTERCONNECT
                    // Since Slave Smoke has priority over Remote Smoke
                    //  Test for received HW Interconnect Smoke Alarm
                    if(FLAG_INTCON_SMOKE_RECEIVED)
                    {
                        // Receiving Interconnect Smoke Signal
                        // Transition straight into Alarm Smoke Slave State
                        FLAG_REMOTE_ALARM_MODE = 0;
                        FLAG_SLAVE_ALARM_MODE = 1;
                        FLAG_SLAVE_SMOKE_ACTIVE = 1;

                        // Hold off the PTT detection timer
                        alm_smoke_alarm_time = TIME_SMK_ALM_60_SECS;
                        
                        // Init Alarm Exit Timeout Timer (i.e slave timer)
                        alm_init_slave_timer();
                        
                        #ifdef CONFIGURE_UNDEFINED
                            #ifdef CONFIGURE_WIRELESS_MODULE
                                #ifdef	CONFIGURE_INTERCONNECT
                                    //This Timer delays start of Remote Alarm
                                    // transmissions after entering Slave Alarm
                                    APP_Init_Slave_To_Remote_Alarm_Timer
                                        (TIMER_SLAVE_TO_REMOTE_DLY_14_SEC);
                                #endif
                            #endif
                        #endif
                        
                        // Proceed directly to Slave Smoke 
                        //  therefore the ARM message is not sent
                        alm_state = ALM_ALARM_SLAVE_SMOKE;

                        break;
                    }
                #endif

                // If Non HW Remote Smoke is received 
                //  Transition to Remote Smoke State directly from here
                if(FLAG_REMOTE_SMOKE_ACTIVE)
                {
                    FLAG_REMOTE_HW_SMOKE_ACTIVE = 0;
                    
                     // Set Remote Smoke Alarm Mode flags
                    FLAG_SMOKE_ALARM_ACTIVE = 1;

                    // Refresh Alarm Timer
                    alm_init_slave_timer();
                    
                    alm_state = ALM_ALARM_REMOTE_SMOKE;

                    return MAIN_Make_Return_Value
                                (ALARM_PRIORITY_INTERVAL_MS);
                }
                
                
                if(FLAG_REMOTE_HW_SMOKE_ACTIVE || FLAG_REFRESH_REMOTE_STATE)
                {
                    // Insure Smoke alarm is Active in case Locate Ended
                    FLAG_SMOKE_ALARM_ACTIVE = 1;
                    
                    //
                    // Refresh alarm timeout timer.
                    //
                    alm_init_slave_timer();
                    FLAG_REMOTE_HW_SMOKE_ACTIVE = 0;
                    FLAG_REFRESH_REMOTE_STATE = 0;
                    
                    #ifdef CONFIGURE_SER_ALM_TIMER
                        alm_sec_count = 1;
                    #endif
                    
                }

                // Look For Alarm Cancel instead of Timeout
                else if(FLAG_CNCL_SMK_CO_ALARM)
                {
                    // One time event
                    FLAG_CNCL_SMK_CO_ALARM = 0;

                    // Allow HW INT CO received to be cleared in 
                    // Alarm Init state
                    FLAG_RST_INT_CO_RCVD = 1;
                    
                    // Cancel has occurred.  Clear the alarm enable flag
                    // and go to the Alarm None state.
                    FLAG_SMOKE_ALARM_ACTIVE = 0;
                    

                    #ifndef CONFIGURE_UNDEFINED
                        // TODO: Undefine Later  BC: Test Only
                        SER_Send_String("Remote HW SMK Cancel...");
                        SER_Send_Prompt();
                    #endif

                    #ifdef CONFIGURE_INTERCONNECT    
                        if(FLAG_GATEWAY_REMOTE_TO_SLAVE)
                        {
                            // Init Delay Timer since this was also a R-to-S
                            //  gateway Connection
                            alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_35_SECS);

                            // Allow Network Alarms to Clear
                            alm_state = ALM_STATE_SMK_CLR;

                            return MAIN_Make_Return_Value
                                    (ALARM_PRIORITY_INTERVAL_MS);
                        }
                    #endif

                    // Return to Alarm Delay (even on Cancel)
                    alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_35_SECS);
                    alm_state = ALM_STATE_SMK_CLR;

                }
                
                else if( (uchar)(MAIN_OneSecTimer - alm_start_time) >=
                         (uchar)TIMER_SMK_RMT_ALARM_14_SEC)
                {
                    // Timeout has occurred.  Clear the alarm enable flag
                    // and go to the Alarm None state.
                    FLAG_SMOKE_ALARM_ACTIVE = 0;
                    
                    // Allow HW INT CO received to be cleared in 
                    // Alarm Init state
                    FLAG_RST_INT_CO_RCVD = 1;
                    
                    #ifndef CONFIGURE_UNDEFINED
                        // TODO: Undefine Later  BC: Test Only
                        SER_Send_String("Remote HW SMK Timeout...");
                        SER_Send_Prompt();
                    #endif
                    
                    #ifdef CONFIGURE_INTERCONNECT     
                        if(FLAG_GATEWAY_REMOTE_TO_SLAVE)
                        {
                            // Init Delay Timer since this was also a R-to-S
                            //  gateway Connection
                            alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_35_SECS);

                            // Allow Network Alarms to Clear
                            alm_state = ALM_STATE_SMK_CLR;

                            return MAIN_Make_Return_Value
                                    (ALARM_PRIORITY_INTERVAL_MS);
                        }
                    #endif 
                        
                    // Return to Alarm Delay (
                    alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_35_SECS);
                    alm_state = ALM_STATE_SMK_CLR;
                    
                }
                else
                {
                    #ifdef CONFIGURE_DEBUG_ALM_PRIORITY_STATES
                        // Test Only
                        PIC_delay_us(DBUG_DLY_50_USec);
                        for(uchar i=(MAIN_OneSecTimer-alm_start_time);i>0;i--)
                        {
                            // Test Only
                            MAIN_TP2_On();
                            PIC_delay_us(DBUG_DLY_25_USec);
                            MAIN_TP2_Off();
                        }
                    #endif

                    #ifdef CONFIGURE_SER_ALM_TIMER
                        // Here we're in remote smoke and we're waiting
                        //  for the timeout to occur.
                        if(0 == --alm_sec_count)
                        {
                            alm_sec_count = 10;

                            SER_Send_String("Time -");
                            SER_Send_Int(' ',(uchar)(MAIN_OneSecTimer -
                                                    alm_start_time),10);
                            SER_Send_Prompt();
                        }
                    #endif

                }

            #endif

        break;


        case ALM_ALARM_REMOTE_CO_HW:

            #ifdef CONFIGURE_WIRELESS_MODULE

                #ifdef CONFIGURE_WIRELESS_ALARM_LOCATE
                    if(FLAG_ALARM_LOCATE_MODE)
                    {
                        // Test for Locate Timeout
                        alm_locate_timer();

                        // Alarm Locate Mode Set
                        // Halt Remote/Slave Alarms during Locate
                        FLAG_CO_ALARM_ACTIVE = 0;
                        
                        // Clear any cancels as a result of Locate Entry
                        FLAG_CNCL_SMK_CO_ALARM = 0;

                        // Keep Track of Incoming Slave Alarms for
                        //  when Locate Times Out
                        if(FLAG_REMOTE_HW_CO_ACTIVE || 
                                FLAG_REFRESH_REMOTE_STATE)
                        {
                            // If still receiving Remote Smoke Signal
                            //  Refresh alarm timeout timer.
                            alm_init_slave_timer();

                            FLAG_REMOTE_HW_CO_ACTIVE = 0;
                            FLAG_REFRESH_REMOTE_STATE = 0;
                        }

                        // If Smoke or CO is detected while in locate
                        //  Transition to Alarm Master, but leave Locate Mode
                        //  active to alarm locally only
                        if(FLAG_MASTER_SMOKE_ACTIVE)
                        {
                            FLAG_SLAVE_ALARM_MODE = 0;
                            FLAG_REMOTE_ALARM_MODE = 0;
                            FLAG_SMOKE_ALARM_ACTIVE = 1;
                            
                            if(!FLAG_PTT_ACTIVE)
                            {
                                // Alarm Memory set if this is a real smoke 
                                //  event
                                FLAG_SMOKE_ALARM_MEMORY = 1;

                                #ifdef CONFIGURE_DIAG_HIST
                                    // Record this Smoke Event upon entry
                                    DIAG_Hist_Que_Push(HIST_SMK_ALARM_ON_EVENT);
                                #endif
                            }

                            // The Green LED task is reset so that initiating
                            //  alarm can be started immediately
                            MAIN_Reset_Task(TASKID_GLED_MANAGER);

                            #ifdef CONFIGURE_VOICE
                                // Initialize Voice to Play between temporal 
                                //  patterns
                                FLAG_PLAY_VOICE = 1;
                            #endif

                            #ifdef CONFIGURE_WIRELESS_MODULE
                                #ifdef CONFIGURE_INTERCONNECT  
                                    // Enable Interconnect Drive Out (no Delay)
                                    FLAG_ALM_INT_DRV_ENABLE = 1;
                                #endif
                            #endif
                            
                            alm_state = ALM_STATE_MASTER_SMOKE;
                            
                             return MAIN_Make_Return_Value
                                        (ALARM_PRIORITY_INTERVAL_MS);

                        }
                        else if(FLAG_CO_ALARM_CONDITION)
                        {
                            //
                            // Master CO alarm sounds the CO temporal pattern.
                            //
                            FLAG_REMOTE_ALARM_MODE = 0;

                            MAIN_Reset_Task(TASKID_GLED_MANAGER);

                            if(!FLAG_PTT_ACTIVE)
                            {
                                FLAG_CO_ALARM_MEMORY = 1;

                                #ifdef CONFIGURE_DIAG_HIST
                                    DIAG_Hist_Que_Push(HIST_ALARM_ON_EVENT);
                                #endif
                            }
                            // Enable CO alarm T4 pattern
                            FLAG_CO_ALARM_ACTIVE = 1;
                            
                             alm_state = ALM_ALARM_MASTER_CO;
                             
                             return MAIN_Make_Return_Value
                                        (ALARM_PRIORITY_INTERVAL_MS);

                        }    
                        
                        return MAIN_Make_Return_Value
                                        (ALM_500_MS_RETURN_TIME);
                    }
                    
                #endif

                // Test for Smoke since it has priority over CO
                if(FLAG_MASTER_SMOKE_ACTIVE ||
                    FLAG_INTCON_SMOKE_RECEIVED  ||
                    FLAG_REMOTE_SMOKE_ACTIVE || FLAG_REMOTE_HW_SMOKE_ACTIVE)
                {
                    // Exit Remote CO if any smoke condition is entered,
                    FLAG_CO_ALARM_ACTIVE = 0;
                    FLAG_REMOTE_HW_CO_ACTIVE = 0;
                    
                    // No need to re-init Remote to Slave
                    //  gateway when transitioning from Remote CO 
                    FLAG_WL_GATEWAY_ENABLE = 1; 
                    
                    #ifndef CONFIGURE_UNDEFINE
                        // Actually we DO need to Reset Slave to Remote gateway
                        //  enable for the condition when Slave Smoke
                        //  overides Remote CO
                    
                        // If Smoke override is from HW INT signal
                        //  we should enable transmit to RF Module
                        if(FLAG_INTCON_SMOKE_RECEIVED)
                        {
                            // May need to act as a gateway to Network
                            FLAG_GATEWAY_SLV_TO_REMOTE_OFF = 0;
                        }
                    #endif
                    
                    FLAG_INTCON_CO_RECEIVED = 0;
                    FLAG_BAT_CONSRV_MODE_ACTIVE = 0;

                    FLAG_REMOTE_ALARM_MODE = 0;
                   
                    #ifdef CONFIGURE_WIRELESS_MODULE
                        // Do not Re-ARM from Remote CO condition
                        FLAG_DO_NOT_ARM = 1;
                    #endif
                    
                    #ifdef CONFIGURE_WIRELESS_MODULE
                        /*
                         * This flag is set to prevent Alarm Status Clear from 
                         *  being sent via CCI to the RF Module during 
                         *  transitions. Alarm Status Clear will take network 
                         *  out of Real time mode causing problems with remote 
                         *  alarms
                         */
                        FLAG_SMK_ALMPRIO_TRANS = 1;
                        
                        #ifdef CONFIGURE_UNDEFINED
                            // TODO: Undefine Later  BC: Test Only
                            SER_Send_String(" SMK Transition Active RCO");
                            SER_Send_Prompt();
                        #endif
                        
                    #endif

                    // Proceed directly to Alarm None on Smoke override
                    alm_state = ALM_STATE_ALARM_NONE;
              
                    MAIN_Reschedule_Int_Active
                        (TASKID_APP_STAT_MONITOR, ALM_RESCHEDULE_TIME_100_MS);

                    // Return on next TIC to speed alarm re-entry
                    return MAIN_Make_Return_Value
                                (ALARM_PRIORITY_INTERVAL_10_MS);
                    
                }
                    
                /* Test For Higher Priority CO Alarms */
                    
                if(FLAG_CO_ALARM_CONDITION)
                {
                    //
                    // Master CO alarm sounds the CO temporal pattern.
                    //
                    FLAG_REMOTE_ALARM_MODE = 0;
                    FLAG_CO_ALARM_ACTIVE = 1;
                    FLAG_REMOTE_HW_CO_ACTIVE = 0;

                    MAIN_Reset_Task(TASKID_GLED_MANAGER);

                    #ifdef CONFIGURE_VOICE
                        FLAG_PLAY_VOICE = 1;
                    #endif

                    if(!FLAG_PTT_ACTIVE)
                    {
                        FLAG_CO_ALARM_MEMORY = 1;

                        #ifdef CONFIGURE_DIAG_HIST
                            DIAG_Hist_Que_Push(HIST_ALARM_ON_EVENT);
                        #endif
                    }

                     alm_state = ALM_ALARM_MASTER_CO;

                     return MAIN_Make_Return_Value
                                (ALARM_PRIORITY_INTERVAL_MS);
                }    

                // If Non HW Remote CO is received 
                //  Transition to Remote CO State directly from here
                if(FLAG_REMOTE_CO_ACTIVE)
                {
                    FLAG_REMOTE_HW_CO_ACTIVE = 0;
                    
                    FLAG_CO_ALARM_ACTIVE = 1;

                    // Refresh Alarm Timer
                    alm_init_slave_timer();
                    
                    alm_state = ALM_ALARM_REMOTE_CO;

                    return MAIN_Make_Return_Value
                                (ALARM_PRIORITY_INTERVAL_MS);
                }

                #ifdef CONFIGURE_INTERCONNECT
                    // Since Slave CO has priority over Remote CO
                    //  Test for received HW Interconnect CO Alarm
                    else if(FLAG_INTCON_CO_RECEIVED)
                    {
                        // A CO alarm was received via interconnect
                        // Set alarm and go straight into slave state
                        FLAG_SLAVE_ALARM_MODE = 1;
                        FLAG_SLAVE_CO_ACTIVE = 1;
                        
                        FLAG_REMOTE_ALARM_MODE = 0;
                        FLAG_REMOTE_HW_CO_ACTIVE = 0;
                        
                        FLAG_BAT_CONSRV_MODE_ACTIVE = 0;
                        
                        // Init Alarm Exit Timeout Timer (i.e slave timer)
                        alm_init_slave_timer();

                        
                        #ifdef CONFIGURE_UNDEFINE
                            // Don't think we want to reset Gateway Flag here
                            // May need to act as a gateway to Network
                            FLAG_GATEWAY_SLV_TO_REMOTE_OFF = 0;
                        #endif
                        
                        #ifdef CONFIGURE_UNDEFINED
                            #ifdef CONFIGURE_WIRELESS_MODULE
                                #ifdef	CONFIGURE_INTERCONNECT
                                    //This Timer delays start of Remote Alarm
                                    // transmissions after entering Slave Alarm
                                    APP_Init_Slave_To_Remote_Alarm_Timer
                                        (TIMER_SLAVE_TO_REMOTE_DLY_14_SEC);
                                #endif
                            #endif
                        #endif
                        
                        alm_state = ALM_ALARM_SLAVE_CO;
                    }
                #endif    

                else if(FLAG_REMOTE_HW_CO_ACTIVE || FLAG_REFRESH_REMOTE_STATE)
                {
                    // Insure CO alarm is Active in case Locate Ended
                    FLAG_CO_ALARM_ACTIVE = 1;
                    
                    // Another Hardwire CO message was received.  Refresh
                    //  the timeout timer
                    
                    #ifdef CONFIGURE_UNDEFINED
                        // TODO: Undefine Later  BC: Test Only
                        SER_Send_String("RMT HW CO_Set...");
                        SER_Send_Prompt();
                    #endif

                    // and check for battery conserve mode.
                    alm_init_slave_timer();
                    FLAG_REMOTE_HW_CO_ACTIVE = 0;
                    FLAG_REFRESH_REMOTE_STATE = 0;

                }

                // Look for Alarm Cancel instead of Timeout    
                else if(FLAG_CNCL_SMK_CO_ALARM)
                {
                    // One time event
                    FLAG_CNCL_SMK_CO_ALARM = 0;

                    // Allow HW INT CO received to be cleared in 
                    // Alarm Init state
                    FLAG_RST_INT_CO_RCVD = 1;
                    
                    // If the alarm timeout timer has expired, exit CO
                    //  remote state.
                    FLAG_CO_ALARM_ACTIVE = 0;

                    #ifndef CONFIGURE_UNDEFINED
                        // TODO: Undefine Later  BC: Test Only
                        SER_Send_String("Remote HW CO Cancel...");
                        SER_Send_Prompt();
                    #endif

                    #ifdef CONFIGURE_INTERCONNECT    
                        if(FLAG_GATEWAY_REMOTE_TO_SLAVE)
                        {
                            // Init Delay Timer since this was also a R-to-S
                            //  gateway Connection   

                            // Init Delay Timer
                            alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_35_SECS);

                            // Allow Network Alarms to Clear
                            alm_state = ALM_STATE_CO_CLR;

                            return MAIN_Make_Return_Value
                                    (ALARM_PRIORITY_INTERVAL_MS);
                        }
                    #endif

                    // Init Delay Timer, Allow Network Alarms to Clear
                    alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_35_SECS);
                    alm_state = ALM_STATE_CO_CLR;

                }
                    
                else if( (uchar)(MAIN_OneSecTimer - alm_start_time) >=
                         (uchar)TIMER_CO_RMT_ALARM_14_SEC)
                {
                    // If the alarm timeout timer has expired, exit CO
                    //  remote state.
                    FLAG_CO_ALARM_ACTIVE = 0;
                    
                    // Allow HW INT CO received to be cleared in 
                    // Alarm Init state
                    FLAG_RST_INT_CO_RCVD = 1;
                    
                    #ifndef CONFIGURE_UNDEFINED
                        // TODO: Undefine Later  BC: Test Only
                        SER_Send_String("Remote HW CO Timeout...");
                        SER_Send_Prompt();
                    #endif
                    
                    #ifdef CONFIGURE_INTERCONNECT    
                        if(FLAG_GATEWAY_REMOTE_TO_SLAVE)
                        {
                            // Init Delay Timer since this was also a R-to-S
                            //  gateway Connection   

                            // Init Delay Timer
                            alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_35_SECS);

                            // Allow Network Alarms to Clear
                            alm_state = ALM_STATE_CO_CLR;

                            return MAIN_Make_Return_Value
                                    (ALARM_PRIORITY_INTERVAL_MS);
                        }
                    #endif
                        
                    // Init Delay Timer, Allow Network Alarms to Clear
                    alm_init_clear_delay(TIMER_SMK_ALM_CLEAR_35_SECS);
                    alm_state = ALM_STATE_CO_CLR;
                    
                }
                else
                {
                    #ifdef CONFIGURE_DEBUG_ALM_PRIORITY_STATES
                        // Test Code Only
                        PIC_delay_us(DBUG_DLY_50_USec);
                        for(uchar i=(MAIN_OneSecTimer-alm_start_time); i>0; i--)
                        {
                            // Test Only
                            MAIN_TP2_On();
                            PIC_delay_us(DBUG_DLY_25_USec);
                            MAIN_TP2_Off();
                        }
                    #endif

                    // Here we're in remote CO and we're waiting for the
                    // timeout to occur.
                        
                }

            #endif

        break;

        case ALM_STATE_CO_CLR:

            //
            // If we are in Network Error, we should not need
            //   full alarm delay Time since network communication
            //   isn't operational anyway.
            //
            if(FLAG_NETWORK_ERROR_MODE) 
            {
                alm_clear_timer = TIMER_SMK_ALM_CLEAR_CANCEL;
            }
            
            if(0 == --alm_clear_timer)
            {
                alm_state = ALM_STATE_ALARM_INIT;
                
                #ifndef CONFIGURE_UNDEFINED
                    // TODO: Undefine Later  BC: Test Only
                    SER_Send_String("CO Clr Dly End...");
                    SER_Send_Prompt();
                #endif
                
            }
        break;
        
        case ALM_STATE_SMK_CLR:
            
            //
            // If we are in Network Error, we should not need
            //   full alarm delay Time since network communication
            //   isn't operational anyway.
            //
            if(FLAG_NETWORK_ERROR_MODE) 
            {
                alm_clear_timer = TIMER_SMK_ALM_CLEAR_CANCEL;
            }
            
            if(0 == --alm_clear_timer)
            {
                alm_state = ALM_STATE_ALARM_INIT;
                
                #ifndef CONFIGURE_UNDEFINED
                    // TODO: Undefine Later  BC: Test Only
                    SER_Send_String("SMK Clr Dly End...");
                    SER_Send_Prompt();
                #endif
                
            }
        break;
            
        default:
            alm_state = ALM_STATE_ALARM_INIT;
        break;

    }

    return MAIN_Make_Return_Value(ALARM_PRIORITY_INTERVAL_MS);
}


/*
+------------------------------------------------------------------------------
| Function:      alm_init_clear_delay
+------------------------------------------------------------------------------
| Purpose:       Initialize Alarm Clear delay Time for 
|                Co or Smoke Alarm Clear States
|
+------------------------------------------------------------------------------
| Parameters:    Delay Time (in 100 ms units)
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void alm_init_clear_delay(uint time)
{
    alm_clear_timer = time;
    
    #ifndef CONFIGURE_UNDEFINED
        // TODO: Undefine Later  BC: Test Only
        SER_Send_String("Start Alarm Clear Dly - ");
        SER_Send_Int(' ',(alm_clear_timer/10),10);
        SER_Send_Prompt();
    #endif
    
}


#ifdef CONFIGURE_SERIAL_ALM_SOURCE_OUT
/*
+------------------------------------------------------------------------------
| Function:      ALM_Get_State
+------------------------------------------------------------------------------
| Purpose:       Return Current Alarm Priority State
|                Used to output current alarm mode via Serial Port
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  ALM_State
+------------------------------------------------------------------------------
*/

uchar ALM_Get_State(void)
{
    return alm_state;
}

#endif

#ifdef CONFIGURE_WIRELESS_MODULE
  #ifdef CONFIGURE_INTERCONNECT

/*
+------------------------------------------------------------------------------
| Function:      alm_init_CO_alarm_off_timer
+------------------------------------------------------------------------------
| Purpose:       Start CO Alarm Off Timer
|   
|               When a Slave CO Alarm Signal times out from the HW Interconnect
|               Start this Timer. If it expires before another CO
|               Slave alarm is received, then allow CCI CO Alarm status to 
|               be sent immediately.
|
|               This should allow CO Battery Conserve transmissions from
|               a Slave CO Signal
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
    
void alm_init_CO_alarm_off_timer(void)
{
   alm_timer_co_off = MAIN_OneSecTimer;
   FLAG_CO_ALARM_OFF_TIMER = 1;
   
    #ifndef CONFIGURE_UNDEFINED
        SER_Send_String("Start CO Off timer");
        SER_Send_Prompt();
    #endif
}

/*
+------------------------------------------------------------------------------
| Function:      alm_CO_alarm_off_timer
+------------------------------------------------------------------------------
| Purpose:       Test Co Alarm Off Timer
|                FLAG_CO_ALARM_OFF_TIMER reset if Timer Expires
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void alm_CO_alarm_off_timer(void)
{
    if(FLAG_CO_ALARM_OFF_TIMER)
    {
        if( (uchar)(MAIN_OneSecTimer - alm_timer_co_off) >=
             (uchar)(TIMER_CO_OFF_35_SECS) )
        {
            FLAG_CO_ALARM_OFF_TIMER = 0;    
            
            #ifndef CONFIGURE_UNDEFINED
                SER_Send_String("End CO Off timer");
                SER_Send_Prompt();
            #endif
            
        }
    }
}
  #endif
#endif
    
    
#ifdef CONFIGURE_INTERCONNECT    
//****************************************************************************************************************************    


/*
+------------------------------------------------------------------------------
| Function:      ALM_Init_Slave_Timer
+------------------------------------------------------------------------------
| Purpose:       Re-initialize the Slave Alarm Timer
|                If no new Alarms are received before the Timer Expires
|                 Slave or Remote Alarm is terminated (10-12 seconds)
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void alm_init_slave_timer(void)
{
    alm_start_time = MAIN_OneSecTimer;
}

#else
    #ifdef CONFIGURE_WIRELESS_MODULE

        // Wireless Option uses the same Timer
        void alm_init_slave_timer(void)
        {
            alm_start_time = MAIN_OneSecTimer;
        }

    #endif
#endif

#ifdef  CONFIGURE_WIRELESS_ALARM_LOCATE
/*
+------------------------------------------------------------------------------
| Function:      ALM_Init_Locate_Mode_Timer
+------------------------------------------------------------------------------
| Purpose:       Initialize the Alarm Locate Timer
|
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void ALM_Init_Locate_Timer(void)
{
    alm_timer_locate_mode = MAIN_OneSecTimer;
    
    #ifndef CONFIGURE_UNDEFINED

        #ifdef CONFIGURE_AUSTRALIA    
            // TODO: Undefine Later  BC: Test Only
            SER_Send_String("Start 20 Sec Locate ...");
            SER_Send_Prompt();
        #else
            // TODO: Undefine Later  BC: Test Only
            SER_Send_String("Start 2 Min Locate ...");
            SER_Send_Prompt();
        #endif
        
    #endif
    
}



/*
+------------------------------------------------------------------------------
| Function:      alm_locate_timer
+------------------------------------------------------------------------------
| Purpose:       Test for Alarm Locate Mode and update timer as needed
|                Alarm Locate has a 2 minute timeout
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void alm_locate_timer(void)
{
    if(FLAG_ALARM_LOCATE_MODE)
    {
        #ifdef CONFIGURE_AUSTRALIA        
            if( (uchar)(MAIN_OneSecTimer - alm_timer_locate_mode) >=
                       TIMER_LOCATE_20_SEC)
            {
                // Local Alarm Mode Timed Out
                FLAG_ALARM_LOCATE_MODE = 0;

                #ifndef CONFIGURE_UNDEFINED
                    // TODO: Undefine Later  BC: Test Only
                    SER_Send_String("Locate End...");
                    SER_Send_Prompt();
                #endif

            }
        #else
                if( (uchar)(MAIN_OneSecTimer - alm_timer_locate_mode) >=
                           TIMER_LOCATE_2_MIN)
                {
                    // Local Alarm Mode Timed Out
                    FLAG_ALARM_LOCATE_MODE = 0;

                    #ifndef CONFIGURE_UNDEFINED
                        // TODO: Undefine Later  BC: Test Only
                        SER_Send_String("2 Min Locate End...");
                        SER_Send_Prompt();
                    #endif

                }

        #endif
    }  
}



#endif

#ifdef	CONFIGURE_WIRELESS_MODULE
  #ifdef CONFIGURE_INTERCONNECT
/*
+------------------------------------------------------------------------------
| Function:      alm_init_remote_inhibit_timer
+------------------------------------------------------------------------------
| Purpose:       Inhibit Remote Alarm for 30 seconds to
|                allow potential slave to remote gateway to end
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void alm_init_remote_inhibit_timer(void)
{
    // Set the Timer
    alm_timer_remote_inhibit = MAIN_OneSecTimer;
    FLAG_ALM_REMOTE_ENABLE = 0;
    
    #ifndef CONFIGURE_UNDEFINED

        // TODO: Undefine Later  BC: Test Only
        SER_Send_String("Start Remote Inhibit Timer...");
        SER_Send_Prompt();
        
        #ifdef CONFIGURE_DEFINATELY_UNDEFINED
            SER_Send_String("T-");
            SER_Send_Int(' ',(uint)(alm_timer_remote_inhibit),10);
            SER_Send_Prompt();
        #endif

    #endif
    

}

/*
+------------------------------------------------------------------------------
| Function:      alm_remote_check_inhibit_timer
+------------------------------------------------------------------------------
| Purpose:       Monitor Remote Alarm Inhibit Timer
|                Enable Remote Alarm at expiration
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void alm_remote_check_inhibit_timer(void)
{
    if(!FLAG_ALM_REMOTE_ENABLE)
    {
        uchar alm_ri_time = 
                (uchar)(MAIN_OneSecTimer - alm_timer_remote_inhibit);
        
        // During Remote Inhibit Timer Active 
        //  Keep Remote Alarms Cleared
        FLAG_REMOTE_SMOKE_ACTIVE = 0;
        FLAG_REMOTE_CO_ACTIVE = 0;
        FLAG_REMOTE_HW_SMOKE_ACTIVE = 0;
        FLAG_REMOTE_HW_CO_ACTIVE = 0;
                
        if(alm_ri_time >= TIMER_REMOTE_INHIBIT_30_SECS)
        {
            FLAG_ALM_REMOTE_ENABLE = 1;
            
            #ifndef CONFIGURE_UNDEFINED
                // TODO: Undefine Later  BC: Test Only
                SER_Send_String("End Remote Inhibit Timer...");
                SER_Send_Prompt();
            #endif
            
        }
        
        if(FLAG_SMK_ALMPRIO_TRANS)
        {
            /*
             * There is a multi Gateway Issue during Slave Smoke End to
             *  Remote CO alarm condition where sending Alarm Status Clear
             *  disrupts the network while another gateway was sending CO
             *  Alarm status as soon as Slave Smoke Alarm ended
             */
            if(alm_ri_time >= TIMER_REMOTE_INHIBIT_4_SECS)
            {
                // Allow Alarm Status to Clear during Remote Inhibit
                FLAG_SMK_ALMPRIO_TRANS = 0;

                #ifndef CONFIGURE_UNDEFINED
                    // TODO: Undefine Later  BC: Test Only
                    SER_Send_String("Allow Alarm Status Clear...");
                    SER_Send_Prompt();
                #endif

            }
        }
        
        
    }
    
}


/*
+------------------------------------------------------------------------------
| Function:      ALM_Init_Master_Slave_Timer
+------------------------------------------------------------------------------
| Purpose:       Delay Master Alarm to Interconnect Signal Drive Output
|                allowing Network Alarms time to detect a Remote Alarm 1st
|                if necessary. This helps prevent multiple gateways driving
|                the HW Interconnect line
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void ALM_Init_Master_Slave_Timer(void)
{
    // Set the Timer
    alm_timer_master_slave = MAIN_OneSecTimer;
    FLAG_ALM_INT_DRV_ENABLE = 0;
    FLAG_TMR_INT_DRV_ACTIVE = 1;
    
    #ifndef CONFIGURE_UNDEFINED
        // TODO: Undefine Later  BC: Test Only
        SER_Send_String("Start Master-Slave Dly Timer...");
        SER_Send_Prompt();
    #endif

}

/*
+------------------------------------------------------------------------------
| Function:      ALM_Check_Master_Slave_Timer
+------------------------------------------------------------------------------
| Purpose:       Monitor master Alarm to Interconnect Output Delay Timer
|                Enable HW Drive Out at Expiration
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void ALM_Check_Master_Slave_Timer(void)
{
    if(FLAG_TMR_INT_DRV_ACTIVE)    
    {
        uchar alm_dly_time = 
                (uchar)(MAIN_OneSecTimer - alm_timer_master_slave);
        
        if(alm_dly_time >= TIMER_MASTER_SLAVE_DELAY_12_SEC)
        {
            FLAG_ALM_INT_DRV_ENABLE = 1;
            FLAG_TMR_INT_DRV_ACTIVE = 0;
            
            #ifndef CONFIGURE_UNDEFINED
                // TODO: Undefine Later  BC: Test Only
                SER_Send_String("End Master-Slave Dly Timer...");
                SER_Send_Prompt();
            #endif
            
        }
    }
}

  #endif
#endif


#ifdef	CONFIGURE_CO_ALM_BAT_CONSERVE
/*
+------------------------------------------------------------------------------
| Function:      ALM_Init_Bat_Conserve_Timer
+------------------------------------------------------------------------------
| Purpose:       Re-initialize the battery Conserve Timer
|
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void alm_init_bat_conserve_timer(void)
{
    alm_timer_bat_conserve = MAIN_OneMinuteTic;
}

/*
+------------------------------------------------------------------------------
| Function:      ALM_Alarm_Bat_Conserve_Logic
+------------------------------------------------------------------------------
| Purpose:       Controls Alarm On/Off Timing
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Returns the number of milliseconds to be returned to the 
|                scheduling engine or zero to use the default time.
+------------------------------------------------------------------------------
*/

uint alm_alarm_bat_conserve_logic(void)
{
    // If we're in battery conserve mode already, see if it's time to sound
    // the sounder.
    if(FLAG_BAT_CONSRV_MODE_ACTIVE )
    {
        #ifdef	CONFIGURE_WIRELESS_MODULE
            // Update Wireless Bat Conserve Timer
            alm_wl_bat_conserve_timer();
        #endif
        
        // We are currently in battery conserve mode.
        // If AC is back on, clear the battery conserve mode flag as
        // battery conserve is not active on AC.
        //
        if(FLAG_AC_DETECT)
        {
            FLAG_BAT_CONSRV_MODE_ACTIVE = 0;
            FLAG_CO_ALARM_ACTIVE = 1;
            alm_init_bat_conserve_timer();
            
            #ifdef	CONFIGURE_WIRELESS_MODULE
                // Allow CCI CO Alarm Status to be sent under AC Power
                FLAG_WL_ALM_BAT_CONSERVE = 0;
            #endif
        }
        //
        // If the alarm is already on, turn it off.
        //
        else if(FLAG_CO_ALARM_ACTIVE)
        {
            FLAG_CO_ALARM_ACTIVE = 0;
        }
        else
        {
            
            // If a minute has passed, re-init the timer and set the CO active
            // flag for the sound module.
            if(alm_timer_bat_conserve != MAIN_OneMinuteTic)
            {
                #ifndef CONFIGURE_UNDEFINED
                    // TODO: Undefine Later  BC: Test Only
                    SER_Send_String("CO ALM BAT Conserve...");
                    SER_Send_Prompt();
                #endif
            
                FLAG_CO_ALARM_ACTIVE = 1;
                FLAG_PLAY_VOICE = 1;
                alm_init_bat_conserve_timer();
                
                #ifdef	CONFIGURE_WIRELESS_MODULE                
                    // Start Wireless Battery Conserve Timer
                    //  This should send a CO Alarm status ON CCI message and  
                    //  delay CO alarm OFF CCI message for 15 seconds to support 
                    //  Wireless Real Time Alarm Timing
                    alm_init_wl_bat_conserv_timer();
                #endif
            }
        }
        return ALM_4_SEC_RETURN_TIME;
    }
    else
    {
        /*
         * If the unit is on AC, don't check for battery conserve.  Re-init the
         * battery conserve timer so that if the AC power goes off the unit will
         * start from the beginning of the battery conserve timeout.
         */
        if(FLAG_AC_DETECT)
        {
            alm_init_bat_conserve_timer();
            
            #ifdef	CONFIGURE_WIRELESS_MODULE
                // Allow CCI CO Alarm Status to be sent under Ac Power 
                FLAG_WL_ALM_BAT_CONSERVE = 0;
            #endif
            
        }
        //
        // Check the battery conserve timer to see if we need to go into
        // battery conserve mode. (min of 4 minutes)
        //
        else if( (uchar)(MAIN_OneMinuteTic - alm_timer_bat_conserve) >
                  (uchar)TIMER_BAT_CONSERVE_4_MIN )
        {
            FLAG_CO_ALARM_ACTIVE = 0;
            FLAG_BAT_CONSRV_MODE_ACTIVE = 1;
            alm_init_bat_conserve_timer();
            
            #ifdef	CONFIGURE_WIRELESS_MODULE
                // Allow CCI CO Alarm Status to be cleared for 
                //  Alarm Battery Conserve operation
                FLAG_WL_ALM_BAT_CONSERVE = 1;
            #endif
            
        }
            
    }
    return 0;
}


#ifdef	CONFIGURE_WIRELESS_MODULE

/*
+------------------------------------------------------------------------------
| Function:      alm_init_wl_bat_conserv_timer
+------------------------------------------------------------------------------
| Purpose:       Turn OFF CCI Alarm Status CO Alarm for 15 seconds to
|                allow RF Module to exit Real Time Mode during CO Alarm
|                Battery conserve mode (to lower current draw)
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void alm_init_wl_bat_conserv_timer(void)
{
    // Set the Timer
    alm_timer_wl_bat_conserve = MAIN_OneSecTimer;
    
    // Enable CCI CO Alarm Status to be sent
    FLAG_WL_ALM_BAT_CONSERVE = 0;
    
    #ifndef CONFIGURE_UNDEFINED

        // TODO: Undefine Later  BC: Test Only
        SER_Send_String("Start Bat Csv Timer...");
        SER_Send_Prompt();
        
        #ifdef CONFIGURE_DEFINATELY_UNDEFINED
            SER_Send_String("T-");
            SER_Send_Int(' ',(uint)(alm_timer_wl_bat_conserve),10);
            SER_Send_Prompt();
        #endif

    #endif
    

}

/*
+------------------------------------------------------------------------------
| Function:      alm_wl_bat_conserve_timer
+------------------------------------------------------------------------------
| Purpose:       Monitor Wireless CO Alarm Battery Conserve Timer
|                Enable Re-Sending of CCI CO Alarm Status at expiration
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void alm_wl_bat_conserve_timer(void)
{
    if(!FLAG_WL_ALM_BAT_CONSERVE)
    {
        uchar alm_bc_time = 
                (uchar)(MAIN_OneSecTimer - alm_timer_wl_bat_conserve);
                
        if(alm_bc_time >= TIMER_REMOTE_INHIBIT_15_SECS)
        {
            // Enable CCI CO Alarm Status to be Cleared
            FLAG_WL_ALM_BAT_CONSERVE = 1;
            
            #ifndef CONFIGURE_UNDEFINED
                // TODO: Undefine Later  BC: Test Only
                SER_Send_String("End WL Bat Csv Timer...");
                SER_Send_Prompt();
            #endif
            
        }
    }
}


#endif  //CONFIGURE_WIRELESS_MODULE

#endif  //CONFIGURE_CO_ALM_BAT_CONSERVE
