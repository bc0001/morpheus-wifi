/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        ptt.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  To start and control Push to Test (PTT) sequence  
|               initiated by Button Press or, for wireless models, 
|               Remote PTT request.
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/



/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include	"common.h"
#include	"ptt.h"
#include	"systemdata.h"
#include	"sound.h"
#include	"fault.h"
#include	"comeasure.h"
#include	"life.h"
#include    "memory_18877.h"
#include    "diaghist.h"
#include    "photosmoke.h"
#include    "interconnect.h"
#include    "VBoost.h"
#include    "led.h"
#include    "voice.h"

#ifdef CONFIGURE_WIRELESS_MODULE
    #include    "alarmmp.h"
    #include    "app.h"
#endif


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

// Defined PTT task states
#define		PTT_STATE_IDLE                  0
#define     PTT_STATE_COUNTDOWN             1
#define     PTT_STATE_WAIT_SMK_ALARM        2
#define     PTT_STATE_INT_IN_SUPER          3
#define     PTT_STATE_WAIT_SMK_CLEAR        4
#define		PTT_STATE_WAIT_CO_ALARM         5
#define		PTT_STATE_WAIT_ALARM_CLEAR      6
#define     PTT_STATE_ABORT_TO_IDLE         7
#define     PTT_STATE_TEST_COMPLETED        8
#define     PTT_STATE_RESUME                9
#define     PTT_STATE_RESET                 10

// Defined LED during Countdown States
#define     PTT_STATE_LED_GRN               0
#define     PTT_STATE_LED_RED               1
#define     PTT_STATE_LED_AMB               2

#define		PTT_CO_ALARM_THRESHOLD          (uint)70    // PPM

// Defines Task State Return Times
#define		PTT_DEFAULT_INT_500_MS          (uint)500   // 1/2 second
#define     PTT_TIME_1_SEC                  (uint)1000  // 1 Second
#define     PTT_TIME_1_5_SEC                (uint)1500  // 1.5 Seconds
#define     PTT_TIME_2_SEC                  (uint)2000  // 2 Seconds
#define     PTT_TIME_3_SEC                  (uint)3000  // 3 Seconds
#define		PTT_WAIT_CO_ALARM_TIME_8_SEC    (uint)8000  // 8 seconds
#define		PTT_DEFAULT_INT_3000_MS         (uint)3000  // 3 seconds
#define     PTT_WAIT_SMK_ALARM_TIME_MS      (uint)3500	// 3.5 seconds
#define     PTT_WAIT_TIME_10_SEC            (uint)10000 // 10 seconds
#define     PTT_WAIT_TIME_12_SEC            (uint)12000 // 12 seconds

#define     PTT_TIMEOUT_10_SEC              (uchar)20   

#define     PTT_COUNTDOWN_1_SEC             (uchar)1    
#define     PTT_COUNTDOWN_2_SEC             (uchar)2    
#define     PTT_COUNTDOWN_3_SEC             (uchar)3    
#define     PTT_COUNTDOWN_10_SEC            (uchar)10 
#define     PTT_COUNTDOWN_11_SEC            (uchar)11 
#define     PTT_COUNTDOWN_12_SEC            (uchar)12
#define     PTT_COUNTDOWN_13_SEC            (uchar)13
#define     PTT_COUNTDOWN_14_SEC            (uchar)14

#define     PTT_COUNT_XMIT_PTT              (uchar)1    // in seconds 
#define     PTT_COUNT_MSG_BEGIN             (uchar)12   // in seconds
#define     PTT_COUNT_MSG_COUNTDOWN         (uchar)5    // in seconds 

#ifdef CONFIGURE_NO_REGULATOR
    #define		VMEAS_AD_TEST_THRESHOLD		(uint)500	// mVolts
#else
    #define		VMEAS_AD_TEST_THRESHOLD		(uint)180	// AD Counts
#endif

#define PTT_FAULT_RETRY_3_TIMES (uchar)3


/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
static uchar ptt_state = 0;
static uchar ptt_count;
static uchar ptt_led_state = 0;

/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/

/*
+------------------------------------------------------------------------------
| Function:      PTT_Process_Task
+------------------------------------------------------------------------------
| Purpose:       Controls Push to Test (PTT) sequence
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  PTT next state Execution Time (in Task TICs)
|
+------------------------------------------------------------------------------
*/
unsigned int PTT_Process_Task(void)
{
    #ifdef CONFIGURE_DEBUG_PTT_STATES
        // Test Only
        for(uchar i = (ptt_state + 1); i > 0; i--)
        {
            // Test Only
            MAIN_TP2_On();
            PIC_delay_us(DBUG_DLY_25_USec);
            MAIN_TP2_Off();
        }
    #endif

    #ifdef CONFIGURE_UNDEFINED
         if(FLAG_PTT_ACTIVE) 
         {
            SER_Send_String("\rPTT State- ");
            SER_Send_Int(' ',ptt_state,10);
            SER_Send_String("\r");
         }
    #endif

    switch (ptt_state)
    {
        case PTT_STATE_IDLE:
        {
            // Wait here until the PTT Active flag gets set, probably from the
            // button module. Wait for a Button Voice to Send before
            // Starting PTT
            //
            // Also allow any active LED profiles to complete
            //  since LED cycling will be needed during PTT countdown
            //
            if(FLAG_PTT_ACTIVE && !FLAG_VOICE_SEND && 
                !FLAG_REMOTE_PTT_ACTIVE && !FLAG_LED_PRO_N_PROGRESS )
            {
                
                FLAG_PTT_COUNDOWN_ACTIVE = 1;
                
                #ifdef CONFIGURE_WIRELESS_MODULE
                    // Notify RF Module ARM for PTT  
                    APP_PTT_Arm();
                #endif

                //Init/Start second Timer here
                ptt_count = PTT_COUNTDOWN_14_SEC;     // Init Countdown Timer
                
                // Begin with Red LED ON
                ptt_led_state = PTT_STATE_LED_RED;

                // Next state.
                ptt_state = PTT_STATE_COUNTDOWN;

                return	MAIN_Make_Return_Value(PTT_TIME_1_SEC);
                
            }
            
            #ifdef CONFIGURE_WIRELESS_MODULE            
                if(FLAG_REMOTE_PTT_ACTIVE && !FLAG_VOICE_SEND && 
                   !FLAG_LED_PRO_N_PROGRESS )                    
                {
                    // Skip full countdown if Remote
                    
                    // Set Countdown Timer to end right away
                    ptt_count = PTT_COUNTDOWN_1_SEC;     

                    // Next state.
                    ptt_state = PTT_STATE_COUNTDOWN;

                    // Set PTT active
                    FLAG_PTT_ACTIVE = 1;

                    return	MAIN_Make_Return_Value(PTT_TIME_1_SEC);
                }
            #endif

            // Insure PTT cancel was not left On with no PTT active
            if(!FLAG_PTT_ACTIVE)
            {
                FLAG_PTT_CANCEL = 0;
            }
        }
        break;

        case PTT_STATE_COUNTDOWN:
        {
            // PTT can be canceled if button pressed during countdown
            if(FLAG_PTT_CANCEL)
            {
                // Cancel Test
                RLED_Off();
                GLED_Off();
                ALED_Off();

                #ifdef CONFIGURE_VOICE
                    VCE_Play(VCE_TEST_CANCELLED);
                #endif

                // Wait for Voice Msg to complete
                FLAG_PTT_COUNDOWN_ACTIVE = 0;
                ptt_state = PTT_STATE_ABORT_TO_IDLE;

                return	MAIN_Make_Return_Value(PTT_TIME_2_SEC);
                    
            }
            
            #ifdef CONFIGURE_WIRELESS_MODULE
                if(PTT_COUNT_XMIT_PTT == ptt_count)
                {
                    if(!FLAG_REMOTE_PTT_ACTIVE)
                    {
                        // Notify RF Module of a Local PTT (Set PTT)
                        // We should be in Real Time Mode by now
                        APP_PTT_Local();
                    }
                }
            #endif
            
            if(0 == --ptt_count)
            {
                RLED_Off();
                GLED_Off();
                ALED_Off();
                
                if(!FLAG_VOICE_SEND)
                {
                    // Start the PTT sequence.
                    #ifdef CONFIGURE_DIAG_HIST
                        DIAG_Hist_Que_Push(HIST_PTT_EVENT);
                        
                        // If Life = Day 0, Bump Day to Day 1
                        // This is to distinguish production events up to
                        //  the 1st logged PTT
                        if(LFE_DAY_ZERO == LIFE_Get_Current_Day() )
                        {
                            // Set Life Counter to 1
                            SYS_RamData.Life_LSB++;
                            SYS_RamData.Life_Checksum = SYS_RamData.Life_LSB + 
                                                        SYS_RamData.Life_MSB;
                            SYS_Save();
                            
                        }
                        
                    #endif


                    ptt_count = PTT_TIMEOUT_10_SEC;     // Set Alarm Timeout

                    // Increase IRed drive Level to simulate Alarm
                    PHOTO_Set_Drive_Pulse_Width
                            (SYS_RamData.Smk_IRled_PTT_Drive);

                    if(SYS_RamData.Smk_Corrected_Alm_TH >
                            SYS_RamData.Smk_Alarm_TH_At_Cal )
                    {
                       // If Compensation has increased the Alarm Threshold
                       // Use the calibrated alarm Threshold
                       PHOTO_Alarm_Thold = SYS_RamData.Smk_Alarm_TH_At_Cal;

                    }

                    #ifdef CONFIGURE_WIRELESS_MODULE
                       #ifdef CONFIGURE_INTERCONNECT    
                        // If Remote PTT is active, start Interconnect Drive Out
                        //  delay so lowest RFID takes control of Interconnect 
                        //  output drive. This keeps multiple Remote PTT 
                        //  gateways from driving HWINT at same time.
                        if(FLAG_REMOTE_PTT_ACTIVE)
                        {
                            // Initialize / Start Remote PTT delay Timer
                            APP_Init_Rmt_PTT_Dly_Timer();
                        }
                    
                       #endif
                    #endif

                    // Reset the PhotoSmoke to Standby state
                    PHOTO_Reset();
                    // Reset Photo Smoke Task to execute immediately
                    MAIN_Reset_Task(TASKID_PHOTO_SMOKE);


                    // Next state.
                    ptt_state = PTT_STATE_WAIT_SMK_ALARM;
                    
                }
                else
                {
                    // Hold in this state until Voice Send is clear
                    ptt_count++;
                }
            }
            else
            {
                
                // Wait during countdown
                // Cycle thru Led States
                switch(ptt_led_state)
                {
                    // Red/Amber/Green LED cycles
                    case PTT_STATE_LED_RED:
                        ALED_Off();
                        GLED_Off();
                        RLED_On();
                        ptt_led_state = PTT_STATE_LED_AMB;
                    break;
                    
                    case PTT_STATE_LED_AMB:
                        RLED_Off();
                        ALED_On();
                        ptt_led_state = PTT_STATE_LED_GRN;
                    break;

                    case PTT_STATE_LED_GRN:
                    default:
                        ALED_Off();
                        GLED_On();
                        ptt_led_state = PTT_STATE_LED_RED;
                    break;

                }
                #ifndef CONFIGURE_UNDEFINED
                    #ifdef CONFIGURE_VOICE
                        if(PTT_COUNT_MSG_BEGIN == ptt_count)
                        {
                            // Begin Voice message at this point in time 
                            VCE_Play(VCE_TESTING_AND_LOUD);
                            VCE_Play(VCE_PRESS_TO_CANCEL_TEST);
                        }
                
                        if(PTT_COUNT_MSG_COUNTDOWN >= ptt_count)
                        {
                            // Begin Voice coundown 
                            VCE_Announce_Number(ptt_count);
                        }
                    #endif
                #endif

                return	MAIN_Make_Return_Value(PTT_TIME_1_SEC);
            }
        }
        break;

        case PTT_STATE_WAIT_SMK_ALARM:
        {
            
            FLAG_PTT_COUNDOWN_ACTIVE = 0;

            if(FLAG_MASTER_SMOKE_ACTIVE)
            {
                
                #ifdef CONFIGURE_ESCAPE_LIGHT
                    // Test Escape Light during smoke alarm
                    FLAG_ESC_LIGHT_ON = 1;
                #endif


                // Next state. (Test VBoost and INT_IN)
                ptt_state = PTT_STATE_INT_IN_SUPER;

                // Set a Fault Retry Count
                ptt_count = PTT_FAULT_RETRY_3_TIMES;

                return  MAIN_Make_Return_Value(PTT_DEFAULT_INT_3000_MS);

            }
            else
            {
                #ifdef CONFIGURE_INTERCONNECT
                    // This was added for Strobe Model, to handle when 2 units 
                    //  were both trying to start a PTT
                    // For Remote PTTs, slave should not be entered once
                    //  PTT begins, so this only applies to Master PTT
                    if( FLAG_SLAVE_ALARM_MODE && !FLAG_REMOTE_PTT_ACTIVE )
                    {
                        // If slave Alarm is received during PTT sequence
                        //  abort the PTT
                        ptt_state = PTT_STATE_ABORT_TO_IDLE;
                    }
                #endif

                if(0 == --ptt_count)
                {
                    //  Unit has not gone into alarm. That's a PTT fault.
                    FLT_Fault_Manager(FAULT_PTT);
                    ptt_state = PTT_STATE_ABORT_TO_IDLE;
                }

            }
        }
        break;

        case PTT_STATE_INT_IN_SUPER:
        {
           
            // Test VBoost voltage and INT_IN signals before
            //  ending Smoke Alarm Test
            #ifdef CONFIGURE_INTERCONNECT
                #ifdef CONFIGURE_TEST_VBOOST
                    // Test VBoost/VINT voltage
                    // Check the VINT voltage, it should be greater than 
                    //  7 volts.
                    if(FALSE == VBOOST_Test())
                    {
                        // Test Only
                        #ifndef CONFIGURE_PRODUCTION_UNIT
                            SER_Send_String("VBst Failed");
                            SER_Send_Prompt();
                        #endif

                        //  Either VBoost is Low (DC operation) or AC Power 
                        //   supply 9-volts is Low.
                        FLT_Fault_Manager(FAULT_INT_SUPERVISION);

                        ptt_state = PTT_STATE_ABORT_TO_IDLE;

                        return  MAIN_Make_Return_Value(PTT_DEFAULT_INT_500_MS);
                    }
                    else
                    {
                        #ifndef CONFIGURE_PRODUCTION_UNIT
                            // Test Only
                            SER_Send_String("VBst Passed");
                            SER_Send_Prompt();
                        #endif
                    }
                #endif

                #ifdef CONFIGURE_DEBUG_INT_IN_TEST
                    // Track Interconnect Signal
                    MAIN_TP2_On();
                #endif

                // Locate disables INT Out transmissions, so do not test
                //  INT_IN this PTT   
                // Also if in Remote PTT don't supervise the Interconnect
                //  Line    
                if(!FLAG_ALARM_LOCATE_MODE && !FLAG_REMOTE_PTT_ACTIVE)
                {
                    // Test INT_IN signal
                    // If INT_IN is not active, the Interconnect Circuit
                    //   has a problem.

                    if( FALSE == INT_Is_Int_In_Active() )
                    {

                        #ifdef CONFIGURE_DEBUG_INT_IN_TEST
                            PIC_delay_ms(40);

                            // Track Interconnect Signal
                            MAIN_TP2_Off();
                        #endif

                        #ifndef CONFIGURE_PRODUCTION_UNIT
                            // Test Only
                            SER_Send_String("INT_IN Failed");
                            SER_Send_Prompt();
                         #endif

                            //  Either unit has not Driven the Smoke 
                            //  Interconnect signal, or INT_IN is not 
                            //  functioning correctly.
                            //  That's logged as an INT fault.
                            FLT_Fault_Manager(FAULT_INT_SUPERVISION);

                            ptt_state = PTT_STATE_ABORT_TO_IDLE;
                            return  MAIN_Make_Return_Value
                                    (PTT_DEFAULT_INT_500_MS);
                    }
                    else
                    {
                        #ifdef CONFIGURE_DEBUG_INT_IN_TEST
                            // Track Interconnect Signal
                            PIC_delay_ms(40);
                            MAIN_TP2_Off();
                         #endif

                         #ifndef CONFIGURE_PRODUCTION_UNIT
                            // Test Only
                            SER_Send_String("INT_IN Passed");
                            SER_Send_Prompt();
                          #endif
                    }
                }
            #endif

            ptt_state = PTT_STATE_WAIT_SMK_CLEAR;
            
            return MAIN_Make_Return_Value(PTT_WAIT_SMK_ALARM_TIME_MS);
        }

        case PTT_STATE_WAIT_SMK_CLEAR:
        {
            // Reset IRed drive Level to Normal operation
            PHOTO_Set_Drive_Pulse_Width(SYS_RamData.Smk_IRled_Drive_Level);

            FLAG_MASTER_SMOKE_ACTIVE = 0;   // Smoke Alarm Off

            #ifdef CONFIGURE_INTERCONNECT
                // This was added for Strobe Model, to handle when 2 units 
                //  were both trying to start a PTT
                // For Remote PTTs, slave should not be entered once
                //  PTT begins, so this only applies to Master PTT
                if( FLAG_SLAVE_ALARM_MODE && !FLAG_REMOTE_PTT_ACTIVE )
                {
                    // If slave Alarm is received during PTT sequence
                    //  abort the PTT
                    ptt_state = PTT_STATE_ABORT_TO_IDLE;
                    return  MAIN_Make_Return_Value(PTT_DEFAULT_INT_500_MS);
                }
            #endif

            #ifdef CONFIGURE_ESCAPE_LIGHT
                FLAG_ESC_LIGHT_ON = 0;	// Escape Light off
            #endif

            #ifdef CONFIGURE_CO
                // If unit is actively in CO Fatal Fault, then CO Test 
                //  should indicate CO Test failed and not Pass CO test
                if( FLAG_FAULT_FATAL && 
                    FAULT_SENSOR_SHORT == FLT_Fatal_Code ||
                    FAULT_CO_HEALTH == FLT_Fatal_Code )
                {
                    // This should only apply to Remote PTTs
                    //  as Standard Button press blinks err code and resets 
                    //  during a Fault and does not execute a PTT.
                    FLAG_PTT_ACTIVE = 0;
                    FLAG_REMOTE_PTT_ACTIVE = 0;

                    // No need to log a PTT fault, unit is already in 
                    //  Fatal CO fault
                    ptt_state = PTT_STATE_IDLE;
                    
                    return  MAIN_Make_Return_Value(PTT_DEFAULT_INT_500_MS);
                }
                
                // Next state.
                ptt_state = PTT_STATE_WAIT_CO_ALARM;

                ptt_count = PTT_TIMEOUT_10_SEC;     // Set Alarm Timeout

                #ifdef CONFIGURE_UNDEFINED
                    //Test Only Undefine later
                    SER_Send_Int('z',(CO_vout_baseline + 
                                      VMEAS_AD_TEST_THRESHOLD),10);
                    SER_Send_Prompt();
                #endif

                // Reset the CO State Machine, and send charge pulses to
                //  test CO Sensor output
                CO_Meas_Reset();
                CO_Meas_Charge(CO_vout_baseline + VMEAS_AD_TEST_THRESHOLD);

                // Reset Co Measure Task to execute immediately
                MAIN_Reset_Task(TASKID_COMEASURE);

            #else
                
                // Wait for Sounder to Stop, then Reset
                ptt_state = PTT_STATE_WAIT_ALARM_CLEAR;
                
            #endif
        }
        break;

        #ifdef CONFIGURE_CO
            case PTT_STATE_WAIT_CO_ALARM:
            {

                if(CO_wPPM > PTT_CO_ALARM_THRESHOLD)
                {
                    // CO detected, Set Alarm Condition
                    FLAG_CO_ALARM_CONDITION = 1;
                    FLAG_CO_ALARM_ACTIVE = 1;

                    // Proceed to Clear CO Alarm state
                    ptt_state = PTT_STATE_WAIT_ALARM_CLEAR;

                    return	MAIN_Make_Return_Value
                                (PTT_WAIT_CO_ALARM_TIME_8_SEC);
                }
                else
                {
                    if(0 == --ptt_count)
                    {
                        //  Unit has not gone into alarm. That's a PTT fault.
                        FLT_Fault_Manager(FAULT_PTT);
                        FLAG_PTT_ACTIVE = 0;
                        FLAG_REMOTE_PTT_ACTIVE = 0;

                        ptt_state = PTT_STATE_IDLE;
                    }
                    else
                    {
                        // Hold Test charge level during sampling
                        CO_Meas_Charge(CO_vout_baseline + 
                                       VMEAS_AD_TEST_THRESHOLD);
                    }
                }
            }
            break;
        #endif

        case PTT_STATE_WAIT_ALARM_CLEAR:
        {
            FLAG_CO_ALARM_CONDITION = 0;
            FLAG_CO_ALARM_ACTIVE = 0;

            #ifdef CONFIGURE_VOICE
                // Announce Test Complete
                if( !FLAG_VOICE_BUSY && !FLAG_REMOTE_PTT_ACTIVE )
                {
                    VCE_Play(VCE_TEST_COMPLETE);
                    
                    #ifdef CONFIGURE_UNDEFINED
                        VCE_Play(VCE_SND_PUSH_TO_TEST);
                    #endif
                }
            #endif
           
            ptt_state = PTT_STATE_TEST_COMPLETED;  
        }
        break;

        case PTT_STATE_ABORT_TO_IDLE:
        {
            // Reset IRed drive Level to Normal operation
            PHOTO_Set_Drive_Pulse_Width(SYS_RamData.Smk_IRled_Drive_Level);

            FLAG_MASTER_SMOKE_ACTIVE = 0;   // End Smoke Alarm
            FLAG_CO_ALARM_CONDITION = 0;    // End CO Alarm Off
            FLAG_CO_ALARM_ACTIVE = 0;
            FLAG_PTT_ACTIVE = 0;
            FLAG_REMOTE_PTT_ACTIVE = 0;
            
            if(!FLAG_VOICE_ACTIVE)
            {
                FLAG_PTT_CANCEL = 0;
                ptt_state = PTT_STATE_IDLE;
            }
            else
            {
                // 1st allow all voice messages to complete before 
                //  returning to Idle
                
            }
        }
        break;
        
        case PTT_STATE_TEST_COMPLETED:
        {
            // When sounder state is Idle the alarm pattern is complete
            // wait for Voice to complete if active
            if( (SND_STATE_IDLE == SND_Get_State() ) &&
                 !FLAG_VOICE_ACTIVE )
            {
                // Insure Alarm Flags after test are Cleared
                FLAG_INTCON_SMOKE_RECEIVED = 0;
                FLAG_INTCON_CO_RECEIVED = 0;
                FLAG_REMOTE_SMOKE_ACTIVE = 0;
                FLAG_REMOTE_HW_SMOKE_ACTIVE = 0;
                FLAG_REMOTE_CO_ACTIVE = 0;
                FLAG_REMOTE_HW_CO_ACTIVE = 0;
                
                if(FLAG_PTT_RESUME)
                {
                    // Instead of resetting the unit, resume
                    //  existing operation
                    ptt_state = PTT_STATE_RESUME;
                }
                else
                {
                    ptt_state = PTT_STATE_RESET;
                }
            }
        }
        break;
        
        case PTT_STATE_RESUME:
        {
            // Add any resume code that may be needed here
            FLAG_PTT_ACTIVE = 0;
            FLAG_REMOTE_PTT_ACTIVE = 0;
            FLAG_PTT_CANCEL = 0;
            
            // Insure INT Drive is re-enabled if turned off during Remote PTT
            FLAG_INT_DRV_OFF_DURING_PTT = 0;
            FLAG_REMOTE_PTT_TIMER_ON = 0;
            
            #ifndef CONFIGURE_UNDEFINED
                VCE_Play(VCE_SND_PUSH_TO_TEST);
            #endif
            
            
            if(FLAG_NETWORK_ERROR_HUSH_ACTIVE)
            {
                #ifdef CONFIGURE_VOICE
                    // Announce Return to Network Error from Connection Lost
                    if(!FLAG_VOICE_BUSY)
                    {
                        VCE_Play(VCE_NETWORK_CONN_LOST);
                    }
                #endif
            }
            
            FLAG_PTT_RESUME = 0;
            ptt_state = PTT_STATE_IDLE;
        }
        break;
        
        case PTT_STATE_RESET:
        {
            FLAG_PTT_ACTIVE = 0;
            SOFT_RESET
        }
        break;
        
        
        default:
        {
            ptt_state = PTT_STATE_IDLE;
        }
        break;
    }

    return  MAIN_Make_Return_Value(PTT_DEFAULT_INT_500_MS);
}

