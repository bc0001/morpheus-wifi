/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        coalarm.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Contains functionality for CO alarm time COHB calculations.
|
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include    "common.h"
#include    "coalarm.h"
#include    "systemdata.h"
#include    "main.h"
#include    "comeasure.h"
#include    "cocalibrate.h"


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

// ****************************************************************************
// Debug Test Only Defines (un-comment as needed)

//#define CONFIGURE_DBUG_SERIAL_OUPUT_ACCUM

// ****************************************************************************



// These constants are used with the Alarm Curve calculations.
// A value of 1.0 is equal to 0x10000, so these hex values are fractional
// constants.
#define AL_ALARM_SUM_CONSTANT		(uint)0xb80b		// == 0.7189
#define AL_PREALARM_SUM_CONSTANT	(uint)0x49fb		// == 0.289

// Alarm Accumulation Task Intervals
#define CO_ALARM_INTERVAL_62_SEC    (uint)62000
#define CO_ALARM_INTERVAL_30_SEC    (uint)30000
#define CO_ALARM_INTERVAL_2_SEC     (uint)2000
#define CO_ALARM_INTERVAL_1_SEC     (uint)1000


#ifdef	CONFIGURE_CO
/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
// Located in Non-Reset Common memory
 persistent static volatile unsigned int  alarm_past_sum;
 persistent static volatile unsigned int  alarm_interim_sum;
 persistent static volatile unsigned int  alarm_sum;

 #ifdef CONFIGURE_CENELEC_ALARM
    // Preserved average, declared here
    uint alarm_slope_average;
#endif


/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/


/*
+------------------------------------------------------------------------------
| Function:      COAlarm_Init
+------------------------------------------------------------------------------
| Purpose:       CO Alarm Initialization
|                Resets CO Alarm Accumulators
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void CO_Alarm_Init(void)
{
    alarm_past_sum = 0;
    alarm_interim_sum = 0;
    alarm_sum = 0;
	
    #ifdef CONFIGURE_CENELEC_ALARM
        alarm_slope_average	= 0;
    #endif

}



/*
+------------------------------------------------------------------------------
| Function:      CO_Alarm_Task
+------------------------------------------------------------------------------
| Purpose:       Update CO Alarm Accumulators based upon measured PPM
|                Must be serviced every 30 seconds for correct accumulation
|
|  This portion of the alarm algorithm uses the second order accumulation 
|  equation given as follows:
|
|    Accum = INT(PreviousAccum * A1 + PrePreviousAccum * A2 + PPM - Constant)
|
|    Equation Definitions:
|
|      Accum            = new accumulation value
|      PreviousAccum    = previous accumulation value
|      PrePreviousAccum = Accumulation value before previous accumulation
|      A1               = Constant fraction
|      A2               = Constant fraction
|      PPM              = current CO reading in PPM with restrictions
|
|  The equation was derived to alarm at 7.5 percent COHb to allow for the
|  maximum amount of error on either side.  To alarm at 7.5 percent COHb
|  the unit must alarm at the following constant CO concentrations:
|
|  The UL standard states the unit must alarm according to the following
|  table:
|
|    CO        Minimum     Maximum     Geometric
|    Level     Alarm       Alarm       Center
|    (PPM)     Time        Time        Time
|              (minutes)   (minutes)   (minutes)
|  -----------------------------------------------
|      70        60.0       189.0       106.5
|     150        10.0        60.0        24.5
|     400         4.0        15.0         7.8
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  CO Alarm Process Next Execution Time (in Task TICs)
+------------------------------------------------------------------------------
*/

uint CO_Alarm_Task(void)
{
    uint local;
    unsigned long alarm_temp1;

    #ifdef CONFIGURE_CENELEC_ALARM
        uint w_ppm_buffer	;
    #endif
	
    #ifdef CONFIGURE_CO_FUNC_TEST
        if(FLAG_FUNC_CO_TEST)
        {
            // Functional CO Test Code here
            FLAG_CO_ALARM_CONDITION = 0;
            if(CO_wPPM > FUNC_CO_ALARM_THRESHOLD)
            {
                FLAG_CO_ALARM_CONDITION = 1;
            }

            return MAIN_Make_Return_Value(CO_ALARM_INTERVAL_2_SEC);

        }
    #endif

    // Don't do calculation if we're in PTT mode.
    if(FLAG_PTT_ACTIVE)
    {
        return MAIN_Make_Return_Value(CO_ALARM_INTERVAL_30_SEC);
    }


    #ifdef CONFIGURE_SERIAL_CO_ALARM
        //***********************************************************
        // Test Code Only
        // This simulates CO alarm with "F" command
        if(FLAG_CO_TEST_ALARM)
        {
            FLAG_CO_ALARM_CONDITION = 1;
            return MAIN_Make_Return_Value(CO_ALARM_INTERVAL_1_SEC);
        }
        //***********************************************************
    #endif
    
  
    // Don't calculate if we're not calibrated.
    uchar calstate = CO_Cal_Get_State();
    if( !((CAL_STATE_VERIFY == calstate) ||
             (CAL_STATE_CALIBRATED == calstate)) )
    {
        return MAIN_Make_Return_Value(CO_ALARM_INTERVAL_30_SEC);   
    }

    // Store the last accumulation value
    alarm_past_sum = alarm_interim_sum;

    //
    //  Multiply previous accumulation value by fraction.
    //
    alarm_interim_sum = alarm_sum;

    alarm_temp1 = ( ((unsigned long)alarm_interim_sum) * AL_ALARM_SUM_CONSTANT);
    local = (alarm_temp1 >> 16);

    #ifdef CONFIGURE_CENELEC_ALARM
        // Because Alarm Curve is so flat at 50 PPM, Set PPM value to a common
        // value when within a MIN/MAX Window for Alarm Time stablilty.
        // in Cenelec configuration only
        if(FLAG_CALIBRATION_COMPLETE)
        {
            if(CO_wPPM < AL_CENELEC_PPM_AVG_MAX)
            {
                alarm_slope_average = ((3 * alarm_slope_average) + CO_wPPM) / 4;
                CO_wPPM = alarm_slope_average;
            }
        }

        // Set w_ppm_buffer with either current PPM or Cenelec centered PPM
        if( (CO_wPPM < AL_CENELEC_50_PPM_MAX) && 
             (CO_wPPM > AL_CENELEC_50_PPM_MIN) )
        {
            w_ppm_buffer = AL_CENELEC_50_PPM;
        }
        else
        {
            w_ppm_buffer = CO_wPPM;
        }

    #endif

    //
    //  If the PPM level is not greater than the accumulator decay constant,
    //  do not perform the curve calculation as the accumulator will not decay
    //  after it has reached a certain value.
    //
    if( CO_wPPM >= AL_ACCUMULATOR_MINIMUM_THRESHOLD)
    {
        alarm_temp1 = ( ((unsigned long)alarm_past_sum) *
                                        AL_PREALARM_SUM_CONSTANT);

        alarm_temp1 = (alarm_temp1 >> 16);
        alarm_sum = local + alarm_temp1;
    }

    #ifdef CONFIGURE_CENELEC_ALARM
        if(AL_ACCUMULATOR_DECAY_THRESHOLD < w_ppm_buffer)
        {
            wAlarmSum = (wAlarmSum + w_ppm_buffer +
                                     AL_ACCUMULATOR_DECAY_CONSTANT);
        }
        else
        {
            wAlarmSum += w_ppm_buffer;
        }
    #else

        if(AL_ACCUMULATOR_DECAY_THRESHOLD < CO_wPPM)
        {
            alarm_sum = (alarm_sum + CO_wPPM + AL_ACCUMULATOR_DECAY_CONSTANT);
        }
        else
        {
            alarm_sum += CO_wPPM;
        }
    #endif

        // Subtract AL_ACCUMULATOR_COMP_THRESHOLD
        if(alarm_sum > AL_ACCUMULATOR_COMP_THRESHOLD)
        {
            alarm_sum -= AL_ACCUMULATOR_COMP_THRESHOLD;

            // check for alarm condition
            if(alarm_sum > AL_ALARM_THRESHOLD)
            {
                alarm_sum = AL_ALARM_THRESHOLD_LIMIT;
                FLAG_CO_ALARM_CONDITION = 1;
            }
            else
            {
                #ifdef CONFIGURE_CENELEC_ALARM
                    // The Cenelec configuration is the same except that
                    // w_ppm_buffer is used the accumulator is below the alarm
                    // threshold
                    if(w_ppm_buffer > AL_THRESHOLD_SLOPE_THRESHOLD)
                    {
                        if( (AL_ALARM_THRESHOLD - wAlarmSum) < 
                             (w_ppm_buffer * AL_THRESHOLD_SLOPE_HIGH_MULTIPLE) )
                        {
                            wAlarmSum = AL_ALARM_THRESHOLD_LIMIT;
                            FLAG_CO_ALARM_CONDITION = 1;
                        }
                        else
                        {
                            FLAG_CO_ALARM_CONDITION = 0;
                        }
                    }
                    else
                    {
                        if( (AL_ALARM_THRESHOLD - wAlarmSum) < (w_ppm_buffer *
                              AL_THRESHOLD_SLOPE_MULTIPLE) )
                        {
                            wAlarmSum = AL_ALARM_THRESHOLD_LIMIT;
                            FLAG_CO_ALARM_CONDITION = 1;
                        }
                        else
                        {
                            FLAG_CO_ALARM_CONDITION = 0;
                        }
                    }
                #else
                    // The UL configuration uses CO_wPPM
                    if(CO_wPPM > AL_THRESHOLD_SLOPE_THRESHOLD)
                    {

                        if( (AL_ALARM_THRESHOLD - alarm_sum) < (CO_wPPM *
                              AL_THRESHOLD_SLOPE_HIGH_MULTIPLE) )
                        {
                            alarm_sum = AL_ALARM_THRESHOLD_LIMIT;
                            FLAG_CO_ALARM_CONDITION = 1;
                         }
                        else
                        {
                            FLAG_CO_ALARM_CONDITION = 0;
                        }

                    }
                    else
                    {
                        if( (AL_ALARM_THRESHOLD - alarm_sum) < (CO_wPPM *
                              AL_THRESHOLD_SLOPE_MULTIPLE) )
                        {
                            alarm_sum = AL_ALARM_THRESHOLD_LIMIT;
                            FLAG_CO_ALARM_CONDITION = 1;
                        }
                        else
                        {
                            FLAG_CO_ALARM_CONDITION = 0;
                        }
                    }

                #endif

            }
        }
        else
        {
            alarm_sum = 0;
            alarm_interim_sum = 0;
            FLAG_CO_ALARM_CONDITION = 0;
        }

    #ifdef CONFIGURE_DBUG_SERIAL_OUPUT_ACCUM
        // Test Only
        // Send accumulator value.
        SER_Send_Int('A', alarm_sum, SERIAL_CONV_BASE_DECIMAL);
        SER_Send_Prompt();
    #endif

    // This value cannot change as the calculations are based on this
    // 30 second interval.
    return MAIN_Make_Return_Value(CO_ALARM_INTERVAL_30_SEC);   // 30 Seconds
}


#else   /* No CO Sensor */


// Placeholder functions if no CO Sensor
void CO_Alarm_Init(void)
{

}

uint CO_Alarm_Task(void)
{
    return MAIN_Make_Return_Value(CO_ALARM_INTERVAL_62_SEC);   
}

#endif  /* CONFIGURE_CO */