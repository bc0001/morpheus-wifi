/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        button.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Button Detect, D'Bounce and Button Press Processing Code
|               
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include	"common.h"
#include	"button.h"
#include	"sound.h"
#include	"comeasure.h"
#include	"cocompute.h"
#include	"battery.h"
#include	"life.h"
#include    "fault.h"
#include    "photosmoke.h"
#include    "led.h"
#include    "alarmprio.h"

#ifdef CONFIGURE_WIRELESS_MODULE
    #include    "alarmmp.h"
    #include    "app.h"
#endif


#ifdef CONFIGURE_VOICE
    #include    "voice.h"
#endif


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

#define	BTN_CHECK_INTERVAL_100MS        (uint)100   // Button Sample rate in ms
#define BTN_INTERVAL_60_SEC             (uint)60000 // in ms
#define BTN_INTERVAL_2_SEC              (uint)2000  // in ms
#define	BTN_FUNCT_TEST_INTERVAL_1_SEC   (uint)1000  // 1 second
#define	BTN_4_SEC_DWELL                 (uint)4000  // 4 seconds
#define BTN_3_SEC_DWELL                 (uint)3000  // 3 seconds
#define BTN_1_SEC_TIME                  (uint)1000  // 1 second

#define BTN_DLY_50_MS                   (uint)50      // in ms

#define BTN_4_SEC_COUNT			(uchar)40	// 4 seconds  (for Join)
#define	BTN_8_SEC_COUNT         (uchar)80 	// 8 seconds  (for OOB)

#define	BTN_10_SEC_COUNT		(uchar)100  // 10 seconds CO Func Test

#define BTN_TIME_1_5_SEC        (uchar)15   // 1.5 Seconds
#define BTN_TIME_2_SEC          (uchar)20   // 2 Seconds
#define BTN_TIME_4_SEC          (uchar)40   // 4 seconds
#define BTN_TIME_5_SEC          (uchar)50   // 5 seconds

#define BTN_MAX_COUNT           (uchar)150	// 15 secs

#define BTN_COUNT_1_SEC         (uchar)1
#define BTN_COUNT_2_SEC         (uchar)2
#define BTN_COUNT_4_SEC         (uchar)4
#define BTN_COUNT_5_SEC         (uchar)5
#define BTN_COUNT_10_SEC        (uchar)10

#define BTN_STUCK_COUNT         (uchar)4   // Every 4 seconds, chirp

#define BTN_DETECT_FOR_2_COUNTS (uchar)2   // Used for PTT cancel


#define	FUNC_CO_TIMEOUT_TIME            (uchar)120  // Seconds
#define WAIT_RELEASE_TIMEOUT_TIME       (uchar)5    // Seconds
#define WAIT_PTT_TIMEOUT_TIME_20_SEC    (uchar)20   // Seconds

#define WAIT_PTT_TIMEOUT_TIME_40_SEC    (uint)400  // 100 ms units


#define ALG_OFF_TIME        (uchar)240  // in Minutes


#ifdef CONFIGURE_WIFI
    // No network size is applicable
    #define SND_NET_SIZE_OF_2   (uchar)0
#else
    #define SND_NET_SIZE_OF_2   (uchar)2
#endif

/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/

/* Alternate User Button Press Functions */
enum
{
    BTN_COUNT_1_PRESS = 1,
    BTN_COUNT_2_PRESSES, 
    BTN_COUNT_3_PRESSES,     
    BTN_COUNT_4_PRESSES,
    BTN_COUNT_5_PRESSES
};

enum
{
    BTN_PRESS_1_BEEP = 1,
    BTN_PRESS_2_BEEPS, 
    BTN_PRESS_3_BEEPS     
};

/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
static unsigned char btn_state = BTN_STATE_IDLE;
static unsigned char btn_counter;
static unsigned int btn_timeout_count;
static unsigned char btn_press_count;
static unsigned char btn_alg_disable_timer;
static unsigned char btn_ptt_cancel_count;


/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/


/*
+------------------------------------------------------------------------------
| Function:      BTN_Process_Task
+------------------------------------------------------------------------------
| Purpose:       Button Press Detect and Processing
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Button Process Next Execution Time (in Task TICs)
+------------------------------------------------------------------------------
*/

unsigned int BTN_Process_Task(void)
{
  #ifdef CONFIGURE_DEBUG_BTN_STATES
    // Test Only
    for(uchar i = (btn_state + 1); i > 0; i--)
    {
        // Test Only
        MAIN_TP2_On();
        PIC_delay_us(DBUG_DLY_25_USec);
        MAIN_TP2_Off();
    }
  #endif

    switch (btn_state)
    {
            case BTN_STATE_IDLE:
            {
                FLAG_BUTTON_NOT_IDLE = 0;

                if(FLAG_NO_SMOKE_CAL)
                {
                    // Smoke Cal must be completed
                    return  MAIN_Make_Return_Value(BTN_INTERVAL_60_SEC);
                }

            #ifdef CONFIGURE_CO
                // Button is disabled if unit not CO calibrated
                if( !FLAG_CALIBRATION_COMPLETE || FLAG_CALIBRATION_UNTESTED)
                {
                    // CO Cal must be completed
                    return  MAIN_Make_Return_Value(BTN_INTERVAL_60_SEC);
                }
            #endif

                if(PORT_TEST_BTN_PIN)
                {
                    FLAG_BUTTON_NOT_IDLE = 1;           // Leaving Idle State
                    btn_state = BTN_STATE_DEBOUNCE;
                    
                    #ifndef CONFIGURE_BTN_CHIRP
                        // For faster Voice Chip Sound Response, turn on Voice
                        //   Power Now (Shaves off 100ms of Setup Time delay)
                        PORT_VOICE_EN_PIN = 1;
                        FLAG_VCE_BUTTON_PRESS = 1;
                    #endif
                }
                FLAG_BUTTON_EDGE_DETECT = 0;
            }
            break;

            case    BTN_STATE_DEBOUNCE:
            {
                // See if button is still depressed
                if(PORT_TEST_BTN_PIN)
                {
                    #ifdef CONFIGURE_BTN_CHIRP
                       /* Forcing a BTN chirp here works better during Alarm
                        *  to provide Button Press Feedback
                        * Turn on the sounder, delay and turn off the sounder
                        */
                        HORN_OFF
                        PIC_delay_ms(BTN_DLY_50_MS);
                        HORN_ON
                        PIC_delay_ms(BTN_DLY_50_MS);
                        HORN_OFF
                    #else
                        #ifdef CONFIGURE_VOICE
                            if(!FLAG_VOICE_BUSY)
                            {
                                VCE_Play(VCE_SND_BTN_PRESS);
                            }
                        #endif
                    #endif
                    
                    // This is the 1st Button Press
                    btn_press_count = BTN_COUNT_1_PRESS;

                    // Button is debounced.  Start timer and transition to wait
                    // for button to be released.
                    btn_counter = 0;
                    
                    // Init PTT Cancel here in case a cancel is needed later
                    btn_ptt_cancel_count = BTN_DETECT_FOR_2_COUNTS;
                    
                    btn_state = BTN_STATE_ACTIVE1;

                }
                else
                {
                    // Button is up, go back and wait for another button press.
                    btn_state = BTN_STATE_IDLE;
                    FLAG_BUTTON_EDGE_DETECT = 0;
                    
                    FLAG_VCE_BUTTON_PRESS = 0;
                    if(!FLAG_VOICE_ACTIVE)
                    {
                        PORT_VOICE_EN_PIN = 0;
                    }

                }
            }
            break;

            case BTN_STATE_ACTIVE1:
            {
                /* This is the 1st Button Press Active
                 *
                 *  If Press and Hold Detected
                 *  Proceed to timer_2 after 2 seconds of button pressed
                 *
                 *  Otherwise Proceed to Button Release 1 State
                 *
                 */
                if(btn_counter++ >= BTN_TIME_2_SEC)
                {
                    // Monitor for longer than 2 second Sec Press/Hold
                    btn_state = BTN_STATE_TIMER_2;

                }
                else
                {
                    if(!PORT_TEST_BTN_PIN)
                    {
                        // Button released before 2 seconds expired
                        btn_counter = 0;
                        btn_state = BTN_STATE_RELEASED1;

                    }
                }
            }
            break;


            case BTN_STATE_RELEASED1:
            {
                /*
                 * Here we want to do a couple of things
                 * 1. If Button remains released > 1.5 seconds,
                 *      proceed to Function state
                 * 2. If Button is pressed a second time, re-start button timer
                 *    and proceed to Active 2 State
                 */
                if(BTN_TIME_1_5_SEC == btn_counter++)
                {
                    // proceed to Button Function state after ~1.5 seconds
                    btn_state = BTN_STATE_FUNCTION;
                }
                else if(PORT_TEST_BTN_PIN)
                {
                    #ifdef CONFIGURE_BTN_CHIRP
                       /* Forcing a BTN chirp here works better during Alarm
                        *  to provide Button Press Feedback
                        * Turn on the sounder, delay and turn off the sounder
                        */
                        HORN_OFF
                        PIC_delay_ms(BTN_DLY_50_MS);
                        HORN_ON
                        PIC_delay_ms(BTN_DLY_50_MS);
                        HORN_OFF
                    #else
                        #ifdef CONFIGURE_VOICE
                            if(!FLAG_VOICE_BUSY)
                            {
                                VCE_Play(VCE_SND_BTN_PRESS);
                            }
                        #endif
                    #endif
                    
                    // Button Pressed 2nd Time, count it
                    btn_press_count++;

                    // Proceed to Active 2 State
                    btn_counter = 0;
                    btn_state = BTN_STATE_ACTIVE2;

                }
                else
                {
                    // Button still released remain in this state
                }
            }
            break;

            case BTN_STATE_ACTIVE2:
            {
                // This is the 2nd Button Press Active
                if(BTN_TIME_1_5_SEC == btn_counter++)
                {
                    if(BTN_COUNT_2_PRESSES == btn_press_count)
                    {
                        // Proceed to Button Timer state after 1.5 seconds
                        //  if this is Second Press
                        // Press/Release & Press/Hold action detected
                        btn_state = BTN_STATE_TIMER;
                    }
                    else
                    {
                        // Proceed to Button Function state after 1.5 seconds
                        // Stop Counting Press & Releases
                        // Multiple 2 to 5 Button Press action detected
                        btn_state = BTN_STATE_FUNCTION_2;
                    }
                }
                else if(!PORT_TEST_BTN_PIN)
                {
                    if(btn_press_count > BTN_COUNT_4_PRESSES)
                    {
                        // Limit to a Maximum of 5 Button Presses
                        btn_state = BTN_STATE_FUNCTION_2;
                    }
                    else
                    {
                        // Button released before 1.5 seconds expired
                        btn_counter = 0;
                        btn_state = BTN_STATE_RELEASED2;
                    }
                }
                else
                {
                    // Button still pressed remain in this state
                }
            }
            break;

            case BTN_STATE_RELEASED2:
            {
                // In this state we simply keep Track of Button Presses
                // and after time with no action proceed to Function_2
                if(BTN_TIME_1_5_SEC == btn_counter++)
                {
                    // proceed to Button Function_2 state after max 1.5 secs
                    btn_state = BTN_STATE_FUNCTION_2;
                }
                else if(PORT_TEST_BTN_PIN)
                {
                    #ifdef CONFIGURE_BTN_CHIRP
                       /* Forcing a BTN chirp here works better during Alarm
                        *  to provide Button Press Feedback
                        * Turn on the sounder, delay and turn off the sounder
                        */
                        HORN_OFF
                        PIC_delay_ms(BTN_DLY_50_MS);
                        HORN_ON
                        PIC_delay_ms(BTN_DLY_50_MS);
                        HORN_OFF
                    #else
                        #ifdef CONFIGURE_VOICE
                            if(!FLAG_VOICE_BUSY)
                            {
                                VCE_Play(VCE_SND_BTN_PRESS);
                            }
                        #endif
                    #endif

                    // Button Pressed,  Count it
                    btn_press_count++;

                    // Back to Active 2 State
                    btn_counter = 0;
                    btn_state = BTN_STATE_ACTIVE2;

                }
                else
                {
                    // Remain in this state
                }
            }
            break;

            case BTN_STATE_TIMER:
            {
                // Timing the Press/Release, Press/Hold Functions
                if(PORT_TEST_BTN_PIN)
                {
                    // Button Still Pressed longer than 2 seconds
                    btn_counter++;

                        if(btn_counter >= BTN_MAX_COUNT)
                        {
                            btn_state = BTN_STATE_BTN_STUCK;
                            
                            // Use BTN counter for 4 second Chirp Interval
                            btn_counter = BTN_STUCK_COUNT;
                        }
                        else if(BTN_4_SEC_COUNT == btn_counter)
                        {
                            SND_Set_Chirp_Cnt(BTN_PRESS_2_BEEPS);
                        }
                        else if(BTN_10_SEC_COUNT == btn_counter)
                        {
                            SND_Set_Chirp_Cnt(BTN_PRESS_3_BEEPS);
                        }
                        else
                        {
                            // Remain in this State
                        }
                }
                else
                {
                    // Button Released, proceed to 
                    //   Press/Release followed by Press/Hold Functions
                    //   Function 4
                     btn_state = BTN_STATE_FUNCTION_4;
                }
            }
            break;


            case BTN_STATE_FUNCTION:
            {
                // This is Single Press/Release State
                #ifdef CONFIGURE_SERIAL_BTN_TEST
                    // TODO: Undefine Later  BC: Test Only
                    SER_Send_String("BTN Function -");
                    SER_Send_Prompt();
                #endif

                #ifndef CONFIGURE_PRODUCTION_UNIT
                    #ifdef CONFIGURE_UNDEFINED
                        // TODO: Undefine Later  BC: Test Only
                        SER_Send_String("COMM - ");
                        SER_Send_Int(' ',APP_Remote_COMM_Status.ALL,16);
                        SER_Send_Prompt();
                    #endif
                #endif

                #ifdef CONFIGURE_SERIAL_CONNECT_BY_BUTTON    
                    // If serial Port is inactive, check it to see if
                    //   this button press should activate serial port   
                    if(!FLAG_SERIAL_PORT_ACTIVE)  
                    {
                        SER_Init();    // Test for Serial Port Initialization

                        if(FLAG_SERIAL_PORT_ACTIVE)
                        {
                            // A Serial Port was detected and activated, take  
                            //  no other actions
                            btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                            btn_state = BTN_STATE_WAIT_RELEASE;
                            break;
                        }
                   }
                #endif

                #ifdef CONFIGURE_SPECIAL_LIGHT_TEST_FW
                    //
                    // Test FW will re-enable Power LED fading
                    //  on 1st Button Press after Reset
                    //
                    if (FLAG_LED_RING_OFF)
                    {
                        //Re-enable LED Ring Power Indication
                        FLAG_LED_RING_OFF = 0;
                        
                        // Allow Power Indicator begin immediately
                        MAIN_Reset_Task(TASKID_GLED_MANAGER);
                        
                        btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                        btn_state = BTN_STATE_WAIT_RELEASE;
                        break;
                    }
                    else
                    {
                        //Disable LED Ring Power Indication
                        FLAG_LED_RING_OFF = 1;
                        
                        // Allow Power Indicator begin immediately
                        MAIN_Reset_Task(TASKID_GLED_MANAGER);
                        
                        btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                        btn_state = BTN_STATE_WAIT_RELEASE;
                        break;
                    }
                #endif
                        

                if(btn_counter < BTN_4_SEC_COUNT)
                {
                    // These are Short Press/release Functions 
                    //  (under 4 seconds)
                    #ifdef CONFIGURE_SERIAL_BTN_TEST
                        // TODO: Undefine Later  BC: Test Only
                        SER_Send_String("Short Press");
                        SER_Send_Prompt();
                    #endif

                    #ifdef CONFIGURE_WIRELESS_MODULE    
                        // Announce Message if in this Transitional State   
                        if(STATE_LED_NETWORK_NOT_FOUND == LED_Get_Red_State())
                        {
                             #ifdef CONFIGURE_VOICE
                                if(!FLAG_VOICE_BUSY)
                                {
                                    #ifndef CONFIGURE_WIFI
                                        // Devices not found, not connected
                                        VCE_Play(VCE_NO_DEVICES_FOUND);
                                    #endif
                                    
                                    VCE_Play(VCE_NOT_CONNECTED);
                                }
                             #endif

                            btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                            btn_state = BTN_STATE_WAIT_RELEASE;
                            break;
                        }
                    #endif

                    // EOL FATAL we Request Fault Error Code Blinks
                    //  and announce Replace Alarm but no other actions
                    if(FLAG_EOL_FATAL)
                    {
                        // Announce Replace Alarm Message 
                        #ifdef CONFIGURE_VOICE
                            if(!FLAG_VOICE_BUSY)
                            {
                                VCE_Play(VCE_REPLACE_ALARM);
                            }
                        #endif
                        
                        // LED Module should respond to this.
                        FLAG_REQ_ERROR_CODE_BLINK = 1;
                        FLAG_REQ_RESET_AFTER_BLINKS = 0;
                        
                        // Wait for Button release and then Idle
                        btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                        btn_state = BTN_STATE_WAIT_RELEASE;
                        break;
                    }
                        
                    // In Alarm Locate, ID Mode or Alerts    
                    if( (FLAG_ALARM_LOCATE_MODE && !FLAG_MASTER_SMOKE_ACTIVE) || 
                        (FLAG_ALARM_LOCATE_MODE && !FLAG_CO_ALARM_CONDITION) ||
                        FLAG_ALERT_PANIC || 
                        FLAG_ALERT_WEATHER || FLAG_ALERT_DETECT || 
                        FLAG_ALARM_IDENTIFY )
                    {
                        // No Action if in 2 minute Locate Mode or Bridge
                        //  modes
                        // Wait for Button release and then Idle
                        btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                        btn_state = BTN_STATE_WAIT_RELEASE;
                        break;
                        
                    }
                        
                    // Remote PTT is in progress?    
                    if(FLAG_REMOTE_PTT_ACTIVE) 
                    {
                        // Enter Button PTT wait state to allow PTT to complete
                        btn_timeout_count = WAIT_PTT_TIMEOUT_TIME_40_SEC;
                        btn_state = BTN_STATE_WAIT_PTT;
                        break;
                    }

                    //  CO Alarm Reset subject to re-alarm UL requirements
                    //   Master CO Only
                    // This also begins a string of "else if" statements    
                    if(FLAG_CO_ALARM_CONDITION && !FLAG_SMOKE_ALARM_ACTIVE)
                    {
                        // Perform Soft Reset unless Smoke Alarm is Active
                        SOFT_RESET
                    }
                        
                    #ifdef CONFIGURE_SMK_HUSH_ACTIVATE
                        //
                        // Are we in Smoke Hush or Smoke Alarm?
                        // If Alarm, issue Hush Request, The Smoke State
                        //  will process either Hush or Hush Cancel
                        //
                        if(FLAG_SMOKE_HUSH_ACTIVE)
                        {

                            #ifdef CONFIGURE_HUSH_CANCEL
                                // Currently by design, Button does not
                                //  Cancel Hush

                                FLAG_SMOKE_HUSH_REQUEST = 1;
                                MAIN_Reset_Task(TASKID_PHOTO_SMOKE);

                                //If not in Smoke Alarm anymore
                                //  send Hush Canceled Voice
                                #ifdef CONFIGURE_VOICE
                                    if(!FLAG_CAL_ALM_DETECT)
                                    {
                                        // Announce Hush Canceled when 
                                        //   returning to Standby
                                        VCE_Play(VCE_HUSH_CANCELLED);
                                    }
                                #endif

                                // Wait for Button release and then Idle
                                btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                                btn_state = BTN_STATE_WAIT_RELEASE;
                                break;
                                
                            #else

                                if( (ALM_STATE_CO_CLR == ALM_Get_State() ) ||
                                    (ALM_STATE_SMK_CLR == ALM_Get_State() ) ) 
                                {
                                    // No PTT action during an Alarm Clear State

                                    #ifndef CONFIGURE_UNDEFINED
                                        SER_Send_String("PTT not allowed, ");
                                        SER_Send_String
                                                ("Alarm Clear State Active");
                                        SER_Send_Prompt();
                                    #endif

                                    // Signal PTT not Allowed
                                    #ifdef CONFIGURE_VOICE
                                        VCE_Play(VCE_SND_NETWORK_ERROR);
                                    #endif   

                                    btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                                    btn_state = BTN_STATE_WAIT_RELEASE;
                                    break;
                                }

                                // Do a PTT / Reset from Smoke Hush
                                FLAG_PTT_ACTIVE = 1;
                                btn_timeout_count = 
                                        WAIT_PTT_TIMEOUT_TIME_40_SEC;
                                btn_state = BTN_STATE_WAIT_PTT;
                                break;

                            #endif

                        }
                        
                        if(FLAG_MASTER_SMOKE_ACTIVE)
                        {
                            #ifdef CONFIGURE_AUSTRALIA

                                // Australia Has No Too Much Smoke Limit
                                FLAG_SMOKE_HUSH_REQUEST = 1;
                                MAIN_Reset_Task(TASKID_PHOTO_SMOKE);

                                 // Send Hush Activated Voice
                                #ifdef CONFIGURE_VOICE
                                    VCE_Play(VCE_HUSH_ACTIVATED);
                                #endif

                                #ifdef CONFIGURE_WIRELESS_ALARM_LOCATE
                                    #ifdef CONFIGURE_UNDEFINED    
                                        // Instead of a Smoke Hush status 
                                        //  message send an Alarm Locate 
                                        //  Request    
                                        FLAG_ALARM_LOCATE_REQUEST = 1;
                                    #else

                                        // Send Locate Message from
                                        // Button module so it transmits 
                                        // before Smoke Alarm Clear
                                        APP_Send_Alarm_Locate();

                                    #endif
                                #endif

                            #else
                                // Check last smoke measured level
                                if(PHOTO_Hush_Too_High())
                                {
                                     // Send Hush Activated Voice
                                    #ifdef CONFIGURE_VOICE
                                        VCE_Play(VCE_TOO_MUCH_SMOKE);
                                    #endif

                                }
                                else
                                {
                                    FLAG_SMOKE_HUSH_REQUEST = 1;
                                    MAIN_Reset_Task(TASKID_PHOTO_SMOKE);

                                     // Send Hush Activated Voice
                                    #ifdef CONFIGURE_VOICE
                                        VCE_Play(VCE_HUSH_ACTIVATED);
                                    #endif

                                    #ifdef CONFIGURE_WIRELESS_ALARM_LOCATE

                                        #ifdef CONFIGURE_WIFI
                                            //
                                            // WiFi Models do not TX Locate
                                            //
                                        #else
                                            //
                                            // Don't Send Locate if already 
                                            // in Locate when Hush was requested
                                            //
                                            if(!FLAG_ALARM_LOCATE_MODE)
                                            {
                                                //
                                                // Send Locate Message from here 
                                                // so it transmits before  
                                                // Smoke Alarm Clear. This is  
                                                // important for Smoke Hush 
                                                // feature to work correctly
                                                //
                                                APP_Send_Alarm_Locate();
                                            }
                                        #endif
                                    #endif
                                }
                            #endif

                            // Wait for Button release and then Idle
                            btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                            btn_state = BTN_STATE_WAIT_RELEASE;
                            break;
                        }

                    #endif

                    if(FLAG_FAULT_FATAL)
                    {
                        #ifdef CONFIGURE_DRIFT_FAULT_RST_OFF
                            // No fault Reset if this is configured
                            //  for Drift Comp faults
                            if( FAULT_SMK_DRIFT_COMP == FLT_Fatal_Code)
                            {
                                // After Blinks complete, no reset 
                                // since this is a  Drift Fatal Fault
                                FLAG_REQ_ERROR_CODE_BLINK = 1;
                                FLAG_REQ_RESET_AFTER_BLINKS = 0;
                                
                                // Wait for Button release and then Idle
                                btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                                btn_state = BTN_STATE_WAIT_RELEASE;
                                break;
                            }
                        #endif
                        
                        // This is an Alarm Fatal Fault Condition
                        // If in Alarm Fatal Fault Mode - Request Error Code 
                        //  Blinks
                        
                        // After Blinks complete, soft reset will occur
                        // since this is a Fatal Fault
                        FLAG_REQ_ERROR_CODE_BLINK = 1;
                        FLAG_REQ_RESET_AFTER_BLINKS = 1;

                        // If CO Fault Save Condition for Reset
                        if(FLAG_CO_FAULT_PENDING)
                        {
                           FLAG_SENSOR_FAULT_RST = 1;
                           
                        }

                        // Wait for Button release and then Idle
                        btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                        btn_state = BTN_STATE_WAIT_RELEASE;
                        break;
                    }


                    #ifdef CONFIGURE_WIRELESS_MODULE
                        if(FLAG_APP_SEARCH_MODE)
                        {
                            #ifdef CONFIGURE_VOICE
                                if(!FLAG_PREV_JOINED)
                                {
                                    // All we do here is announce Voice Message
                                    //  Ready to Connect Message
                                    FLAG_REQ_VOICE_SEARCH_1 = 1;
                                }
                                else
                                {
                                    // Unit is part of a network but in
                                    //  search Mode, Announce searching 
                                    //  message at Button Press
                                    if(!FLAG_VOICE_BUSY)
                                    {
                                        #ifdef CONFIGURE_WIFI
                                            VCE_Play(VCE_NOT_CONNECTED);
                                        #else
                                            VCE_Play(VCE_NETWORK_SEARCH);
                                        #endif
                                    }
                                }
                            #endif

                            // Wait for Button release and then Idle
                            btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                            btn_state = BTN_STATE_WAIT_RELEASE;
                            break;
                            
                        }
                        
                        #ifndef CONFIGURE_WIFI 
                            // Request Number of Devices
                            if(FLAG_APP_JOIN_MODE)
                            {
                                FLAG_REQ_NUM_DEVICES = 1;

                                // Wait for Button release and then Idle
                                btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                                btn_state = BTN_STATE_WAIT_RELEASE;
                                break;

                            }
                        #else
                            if(FLAG_APP_JOIN_MODE)
                            {
                                // No Action for WiFi
                                
                                // Wait for Button release and then Idle
                                btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                                btn_state = BTN_STATE_WAIT_RELEASE;
                                break;

                            }
                        #endif

                    #endif

                    #ifdef CONFIGURE_EOL_HUSH
                        if(FLAG_EOL_HUSH_ACTIVE)
                        {
                            // This no longer executes a PTT/Reset
                            #ifdef CONFIGURE_UNDEFINED
                                // Do a PTT/ Reset from EOL Hush
                                FLAG_PTT_ACTIVE = 1;
                                btn_timeout_count = 
                                        WAIT_PTT_TIMEOUT_TIME_40_SEC;
                                btn_state = BTN_STATE_WAIT_PTT;
                                break;
                            #endif

                            // Instead, simply announce Replace Alarm Voice 
                            //  message and resume Low battery Hush condition    
                            #ifdef CONFIGURE_VOICE
                                if(!FLAG_VOICE_BUSY)
                                {
                                    VCE_Play(VCE_REPLACE_ALARM);
                                }
                            #endif

                            // Wait for Button release and then Idle
                            btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                            btn_state = BTN_STATE_WAIT_RELEASE;
                            break;
                        }
                        
                        if(FLAG_EOL_MODE)
                        {
                            // A Sensor Fault condition 
                            //  during EOL Mode disables unit entering EOL Hush
                            if(FLAG_SMOKE_ERROR_PENDING ||
                                    FLAG_CO_FAULT_PENDING)
                            {
                                // Do a PTT/ Reset from EOL mode
                                FLAG_PTT_ACTIVE = 1;
                                btn_timeout_count = 
                                        WAIT_PTT_TIMEOUT_TIME_40_SEC;
                                btn_state = BTN_STATE_WAIT_PTT;
                                break;
                            }
                            else
                            {
                                #ifdef CONFIGURE_VOICE
                                    if(!FLAG_VOICE_BUSY)
                                    {
                                        VCE_Play(VCE_TEMP_SILENCED);
                                    }
                                #endif

                                // If unit is in EOL, enter EOL hush if not 
                                //  already in EOL hush.
                                LIFE_Init_EOL_Hush();

                                // Wait for Button release and then Idle
                                btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                                btn_state = BTN_STATE_WAIT_RELEASE;
                                break;
                            }
                        }
                    #endif

                    #ifdef CONFIGURE_WIRELESS_ALARM_LOCATE
                        // Only wireless models will feature Smoke Locate
                        // ACDC Wireless models may be in Slave Alarm
                        
                        if( FLAG_SMOKE_ALARM_ACTIVE &&
                           (FLAG_SLAVE_ALARM_MODE || FLAG_REMOTE_ALARM_MODE) )
                        {
                            #ifdef CONFIGURE_WIFI
                                // WiFi power?
                                if(FLAG_AC_DETECT)
                                {
                                //    
                                // No Locate out from Alarm in WiFI
                                // Locate from APP only
                                //    
                                #ifdef CONFIGURE_UNDEFINED
                                    // Send Locate Message directly from Button  
                                    //  module so it transmits immediately 
                                    APP_Send_Alarm_Locate();

                                    // Set this unit into Locate mode too since
                                    //  it is not in Master Alarm
                                    FLAG_ALARM_LOCATE_MODE = 1;
                                    // Initialize the Locate Mode Timer
                                    ALM_Init_Locate_Timer();
                                #endif
                                }
                            
                            #else

                                // Send Locate Message directly from Button  
                                //  module so it transmits immediately 
                                APP_Send_Alarm_Locate();

                                // Set this unit into Locate mode too since
                                //  it is not in Master Alarm
                                FLAG_ALARM_LOCATE_MODE = 1;
                                // Initialize the Locate Mode Timer
                                ALM_Init_Locate_Timer();
                            
                            #endif


                            // Wait for Button release and then Idle
                            btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                            btn_state = BTN_STATE_WAIT_RELEASE;
                            break;
                            
                        }
                        
                        // Only wireless models will feature CO Locate
                        // ACDC Wireless models may be in Slave Alarm
                        if( FLAG_CO_ALARM_ACTIVE &&
                            (FLAG_SLAVE_ALARM_MODE || FLAG_REMOTE_ALARM_MODE) )
                        {
                            
                            #ifdef CONFIGURE_WIFI
                                // WiFi power?
                                if(FLAG_AC_DETECT)
                                {
                                    // Issue a CO Locate request message via CCI
                                    FLAG_ALARM_LOCATE_REQUEST = 1;
                                
                                }
                            
                            #else
                                // Issue a CO Locate request message via CCI
                                FLAG_ALARM_LOCATE_REQUEST = 1;
                            #endif
                            
                            // Wait for Button release and then Idle
                            btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                            btn_state = BTN_STATE_WAIT_RELEASE;
                            break;
                        }

                    #else
                        // Non Wireless Models perform a Soft Reset
                        //  then re-enter Alarm if alarm still active
                        if( FLAG_SLAVE_ALARM_MODE || FLAG_REMOTE_ALARM_MODE )
                        {
                            // Perform Soft Reset
                            SOFT_RESET
                        }
                        
                    #endif

                    #ifdef CONFIGURE_CO
                        #ifdef CONFIGURE_CO_PEAK_MEMORY
                            //
                            // If unit recorded CO peak memory, play CO 
                            // previously detected voice.
                            //
                            if(FLAG_PEAK_MEMORY)
                            {
                                //
                                // Play CO previously detected here.
                                //
                                FLAG_PEAK_MEMORY = 0;

                                #ifdef	CONFIGURE_VOICE
                                    // Announce Co Previously Detected
                                    VCE_Play(VCE_CO_PREVIOUSLY);
                                #endif

                                #ifdef CONFIGURE_DIAG_HIST
                                    #ifdef CONFIGURE_DIAG_HIST
                                        DIAG_Hist_Que_Push(HIST_PEAK_EVENT);
                                    #endif
                                #endif

                                // Wait for Button release and then Idle
                                btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                                btn_state = BTN_STATE_WAIT_RELEASE;
                                break;
                            }
                        #endif
                    #endif
                        
                    #ifdef CONFIGURE_LB_HUSH
                        if(FLAG_LB_HUSH_ACTIVE)
                        {
                            // This no longer executes a PTT/Reset
                            #ifdef CONFIGURE_UNDEFINED
                                // Proceed to Test/Reset
                                btn_state = BTN_STATE_ISSUE_A_TEST;
                                return MAIN_Make_Return_Value(BTN_1_SEC_TIME);
                            #endif

                            // Instead, simply announce Replace Alarm Voice 
                            //  message and resume Low Battery Hush condition    
                            #ifdef CONFIGURE_VOICE
                                if(!FLAG_VOICE_BUSY)
                                {
                                    VCE_Play(VCE_REPLACE_ALARM);
                                }
                            #endif

                            // Wait for Button release and then Idle
                            btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                            btn_state = BTN_STATE_WAIT_RELEASE;
                            break;
                        }
                    #endif
                        
                    #ifdef CONFIGURE_WIRELESS_MODULE

                        #ifdef CONFIGURE_LB_HUSH
                            if( (FLAG_LOW_BATTERY ||  
                                 FLAG_STATUS_RMT_LB_ACTIVE) && 
                                !FLAG_LB_HUSH_INHIBIT     && 
                                !FLAG_LB_HUSH_DISABLED )
                            {

                                #ifdef CONFIGURE_VOICE
                                    if(!FLAG_VOICE_BUSY)
                                    {
                                        VCE_Play(VCE_TEMP_SILENCED);
                                    }
                                #endif

                                // If in low battery, initiate low battery hush
                                BAT_Init_LB_Hush();

                                // Wait for Button release and then Idle
                                btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                                btn_state = BTN_STATE_WAIT_RELEASE;
                                break;
                            }
                        #endif

                            if( FLAG_LOW_BATTERY || FLAG_STATUS_RMT_LB_ACTIVE )
                            {
                                // Morpheus no longer executes a PTT/Reset
                                #ifdef CONFIGURE_UNDEFINED
                                    // Proceed to Test/Reset
                                    btn_state = BTN_STATE_ISSUE_A_TEST;
                                    
                                    return MAIN_Make_Return_Value
                                            (BTN_1_SEC_TIME);
                                #endif

                                #ifdef CONFIGURE_VOICE        
                                    #ifdef CONFIGURE_VCE_OPEN_SWITCH 
                                        // Test for Open Switch
                                        if(FLAG_BAT_ABNORMALLY_LOW)
                                        {
                                            VCE_Play(VCE_ACTIVATE_BATTERY);
                                        }
                                        else
                                        {
                                            // Replace Alarm for 10 year sealed
                                            VCE_Play(VCE_REPLACE_ALARM);
                                        }
                                    #else
                                        //
                                        // Instead, simply announce Replace   
                                        //  Alarm Voice message and resume 
                                        //  Low battery
                                        //
                                        if(!FLAG_VOICE_BUSY)
                                        {
                                            VCE_Play(VCE_REPLACE_ALARM);
                                        }
                                    #endif
                                #endif
                                
                                // Wait for Button release and then Idle
                                btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                                btn_state = BTN_STATE_WAIT_RELEASE;
                                break;
                            }
                    
                    #else

                        #ifdef CONFIGURE_LB_HUSH
                            if( FLAG_LOW_BATTERY && !FLAG_LB_HUSH_INHIBIT && 
                                !FLAG_LB_HUSH_DISABLED )
                            {

                                #ifdef CONFIGURE_VOICE
                                    if(!FLAG_VOICE_BUSY)
                                    {
                                        VCE_Play(VCE_TEMP_SILENCED);
                                    }
                                #endif

                                // If in low battery, initiate low battery hush
                                BAT_Init_LB_Hush();

                                // Wait for Button release and then Idle
                                btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                                btn_state = BTN_STATE_WAIT_RELEASE;
                                break;
                            } 
                        #endif

                            if( FLAG_LOW_BATTERY && FLAG_LB_HUSH_DISABLED )
                            {
                                // This no longer executes a PTT/Reset
                                #ifdef CONFIGURE_UNDEFINED
                                    // Proceed to Test/Reset
                                    btn_state = BTN_STATE_ISSUE_A_TEST;
                                    
                                    return MAIN_Make_Return_Value
                                            (BTN_1_SEC_TIME);
                                #endif

                                #ifdef CONFIGURE_VOICE        
                                    #ifdef CONFIGURE_VCE_OPEN_SWITCH 
                                        // Test for Open Switch
                                        if(FLAG_BAT_ABNORMALLY_LOW)
                                        {
                                            VCE_Play(VCE_ACTIVATE_BATTERY);
                                        }
                                        else
                                        {
                                            // Replace Alarm for 10 year sealed
                                            VCE_Play(VCE_REPLACE_ALARM);
                                        }
                                    #else
                                        //
                                        // Instead, simply announce Replace   
                                        //  Alarm Voice message and resume 
                                        //  Low battery
                                        //
                                        if(!FLAG_VOICE_BUSY)
                                        {
                                            VCE_Play(VCE_REPLACE_ALARM);
                                        }
                                    #endif
                                #endif
                                
                                // Wait for Button release and then Idle
                                btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                                btn_state = BTN_STATE_WAIT_RELEASE;
                                break;
                            }
                    #endif

                    #ifdef CONFIGURE_WIRELESS_MODULE

                        if( FLAG_NETWORK_ERROR_MODE &&
                           !FLAG_NETWORK_ERROR_HUSH_ACTIVE )
                        {
                            // Unit is in a Network Error Condition
                            if( (APP_Rf_Network_Size > SND_NET_SIZE_OF_2) ||
                                (AMP_FAULT_CCI == FLT_NetworkFaultCode ) )
                            {
                                
                                //
                                // For WiFi - This is the path always taken
                                //  since CCI is the only network error fault
                                //
                            
                                
                                // Error Code should be blinked when Hush
                                // is entered.
                                FLAG_REQ_ERROR_CODE_BLINK = 1;
                                FLAG_REQ_RESET_AFTER_BLINKS = 0;

                                #ifdef CONFIGURE_VOICE
                                    if(!FLAG_VOICE_BUSY)
                                    {
                                        VCE_Play(VCE_TEMP_SILENCED);
                                    }
                                #endif

                                // If in Network Error Mode - Request Error Hush
                                FLT_Init_Error_Hush();
                            }
                            else
                            {
                                // Error Code should be blinked 
                                FLAG_REQ_ERROR_CODE_BLINK = 1;
                                FLAG_REQ_RESET_AFTER_BLINKS = 0;
                                
                                // No need to Hush, since there are no 
                                //  voices or error chirps
                                #ifdef CONFIGURE_VOICE
                                    if(!FLAG_VOICE_BUSY)
                                    {
                                        if(FLAG_WIRELESS_DISABLED)
                                        {
                                             VCE_Play(VCE_REPLACE_ALARM);
                                        }
                                        else
                                        {
                                            VCE_Play(VCE_NETWORK_CONN_LOST);
                                        }
                                    }
                                #endif
                                    
                            }
                            
                            // Wait for Button release and then Idle
                            btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                            btn_state = BTN_STATE_WAIT_RELEASE;
                            break;
                        }
                        
                        if(FLAG_NETWORK_ERROR_HUSH_ACTIVE)
                        {
                            // Do a PTT/Resume if Net Error Hush
                            FLAG_PTT_ACTIVE = 1;
                            FLAG_PTT_RESUME = 1;
                            //
                            // We used to reset error here, but not now.
                            //
                            
                            //BC: TODO - For WiFi should we reset Network Error?
                            
                            
                            btn_timeout_count = WAIT_PTT_TIMEOUT_TIME_40_SEC;
                            btn_state = BTN_STATE_WAIT_PTT;
                            break;
                        }

                    #endif
                        
                    #ifdef CONFIGURE_CO
                        #ifdef CONFIGURE_CO_ALARM_MEMORY
                            //
                            // If unit recorded CO Alarm, play CO
                            //  previously detected voice.
                            //
                            if( FLAG_CO_ALARM_MEMORY ||
                                FLAG_CO_ALARM_MEMORY_ANNOUNCE)
                            {
                                //
                                // Play CO previously detected here.
                                //
                                FLAG_CO_ALARM_MEMORY = 0;
                                FLAG_CO_ALARM_MEMORY_ANNOUNCE = 0;

                                #ifdef	CONFIGURE_VOICE
                                    // Announce CO Previously Detected
                                    VCE_Play(VCE_CO_PREVIOUSLY);
                                #endif

                            }
                        #endif
                    #endif

                    #ifdef CONFIGURE_SMK_ALARM_MEMORY
                        //
                        // If unit recorded Smoke Alarm, play Smoke
                        // previously detected voice.
                        //
                        if( FLAG_SMOKE_ALARM_MEMORY ||
                            FLAG_SMK_ALARM_MEMORY_ANNOUNCE )
                        {
                            //
                            // Play SMK previously detected here.
                            //
                            FLAG_SMOKE_ALARM_MEMORY = 0;
                            FLAG_SMK_ALARM_MEMORY_ANNOUNCE = 0;

                            // This was removed from the specification
                            #ifdef	CONFIGURE_VOICE
                                // Announce Co Previously Detected
                                VCE_Play(VCE_SMK_PREVIOUSLY);
                            #endif

                        }
                    #endif

                    if( (ALM_STATE_CO_CLR == ALM_Get_State() ) ||
                        (ALM_STATE_SMK_CLR == ALM_Get_State() ) ) 
                    {
                        // No PTT action during an Alarm Clear State
                        
                        #ifndef CONFIGURE_UNDEFINED
                            SER_Send_String("PTT not allowed, ");
                            SER_Send_String("Alarm Clear State Active");
                            SER_Send_Prompt();
                        #endif

                        // Signal PTT not Allowed
                        #ifdef CONFIGURE_VOICE
                            VCE_Play(VCE_SND_NETWORK_ERROR);
                        #endif   
                        
                        btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                        btn_state = BTN_STATE_WAIT_RELEASE;
                        break;
                    }
                            
                    // After ALL that if we reach this point
                    //  we finally come to PTT button press

                    // Do a PTT.
                    FLAG_PTT_ACTIVE = 1;
                    btn_timeout_count = WAIT_PTT_TIMEOUT_TIME_40_SEC;
                    btn_state = BTN_STATE_WAIT_PTT;
                    break;

 
                }   // End of Single Press/Release Actions
                    
                // Check/Wait for button release before returning to Idle
                btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                btn_state = BTN_STATE_WAIT_RELEASE;

            }
            break;

            case    BTN_STATE_WAIT_PTT:
            {
                // Wait in this state until the PTT module clears the
                // Active flag and PTT cancel has been completed
                if(!FLAG_PTT_ACTIVE  && !FLAG_PTT_CANCEL)
                {
                    btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                    btn_state = BTN_STATE_WAIT_RELEASE;
                    
                }
                else
                {
                   // PTT in Progress , monitor Button for PTT Cancel
                   if(PORT_TEST_BTN_PIN && FLAG_PTT_ACTIVE)
                   {
                       if( (0 == --btn_ptt_cancel_count)  &&
                            !FLAG_PTT_CANCEL )
                       {
                            #ifdef CONFIGURE_VOICE
                                if(!FLAG_VOICE_BUSY)
                                {
                                    VCE_Play(VCE_SND_BTN_PRESS);
                                }
                            #endif

                            FLAG_PTT_CANCEL = 1;
                            
                            return MAIN_Make_Return_Value
                                    (BTN_CHECK_INTERVAL_100MS);
                       }
                   }
                   
                    // Update PTT Wait Timeout Timer
                    if(0 == --btn_timeout_count)
                    {
                        // PTT Wait Timeout - Exit This state
                        FLAG_PTT_CANCEL = 0;
                        FLAG_PTT_ACTIVE = 0;
                        
                        btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                        btn_state = BTN_STATE_WAIT_RELEASE;
                    }
                    else
                    {
                        return MAIN_Make_Return_Value
                                (BTN_CHECK_INTERVAL_100MS);
                    }
                }
            }
            break;

            case    BTN_STATE_WAIT_RELEASE:
            {
                FLAG_ACTIVE_TIMER = 1;
                // Minimum time to remain in Active Mode
                MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_2_SEC;
                
                // Wait in this state until the PTT Button is inactive
                if(PORT_TEST_BTN_PIN)
                {
                    // Remain here until Button Released
                    //Timeout Timer in case button gets stuck here
                    if(0 == --btn_counter)
                    {
                        // Proceed to Btn Stuck on Timeout
                        btn_state = BTN_STATE_BTN_STUCK;
                        btn_counter = BTN_STUCK_COUNT;
                    }
                    else
                    {
                        return MAIN_Make_Return_Value(BTN_1_SEC_TIME);
                    }
                }
                else
                {
                    btn_state = BTN_STATE_IDLE;
                }
            }
            break;


            case BTN_STATE_ISSUE_A_TEST:
            {
                // Issue a PTT.
                FLAG_PTT_ACTIVE = 1;
                btn_timeout_count = WAIT_PTT_TIMEOUT_TIME_40_SEC;
                btn_state = BTN_STATE_WAIT_PTT;
            }
            break;

      #ifdef CONFIGURE_CO_FUNC_TEST
            case    BTN_STATE_FUNC_TEST	:
                // Functional Test in Progress
                // Monitor Button or Timeout Timer for Test Complete,
                // then Reset
                if(0 == --btn_counter)
                {
                    // Reset after Button is released
                    btn_state = BTN_STATE_WAIT_FT_RELEASE;
                }
                else if(PORT_TEST_BTN_PIN)
                {
                    // Button Press Feedback Chirp
                    SND_Set_Chirp_Cnt(BTN_PRESS_1_BEEP);

                    // Reset after Button is released
                    btn_state = BTN_STATE_WAIT_FT_RELEASE;
                }

                return MAIN_Make_Return_Value(BTN_FUNCT_TEST_INTERVAL_1_SEC);

            case    BTN_STATE_WAIT_FT_RELEASE:
                // Wait in this state until button release
                if(PORT_TEST_BTN_PIN)
                {
                    // Remain here until Button Released

                }
                else
                {
                    FLAG_FUNC_CO_TEST = 0;

                    // Reset the CPU
                    SOFT_RESET
                }

            break;
      #endif


            case BTN_STATE_FUNCTION_2:
            {
                // This is Multi-Press/Release State
                switch (btn_press_count)
                {

                    #ifdef CONFIGURE_VOICE
                      #ifdef CONFIGURE_CANADA
                        // Can Add 2-5 Press states Here (if needed)
                        case BTN_COUNT_2_PRESSES:
                        {
                            //Language select toggles
                            if(FLAG_VOICE_FRENCH)
                            {
                                FLAG_VOICE_FRENCH = 0;

                                if(!FLAG_VOICE_BUSY)
                                {
                                    VCE_Play(VCE_ENGLISH_SELECTED);
                                    VCE_Play(VCE_FR_FOR_FRENCH_PUSH_BTN);

                                }
                            }
                            else
                            {
                                FLAG_VOICE_FRENCH = 1;

                                if(!FLAG_VOICE_BUSY)
                                {
                                    // Will be translated to French
                                    VCE_Play(VCE_ENGLISH_SELECTED);
                                    VCE_Play(VCE_FR_FOR_FRENCH_PUSH_BTN);
                                }
                            }
                        }
                        break;
                      #endif
                    #endif

                    default:
                    {
                        #ifdef CONFIGURE_SERIAL_BTN_TEST
                            SER_Send_String("Btn Count -");
                            SER_Send_Int( ' ', btn_press_count, 10 );
                            SER_Send_Prompt();
                        #endif

                        // Do nothing
                    }
                    break;
                }
                
                btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                btn_state = BTN_STATE_WAIT_RELEASE;
            }
            break;

            
            case BTN_STATE_FUNCTION_3:
            {
                // These are Press/Hold Functions longer than 4 seconds
                
                // Ignore Button Press Function if in NET Not Found
                //  transitional state    
                if(STATE_LED_NETWORK_NOT_FOUND == LED_Get_Red_State() )
                {

                    // Ignore Button Press, Do Nothing
                    SER_Send_String("\rButton Ignored\r");
                    SER_Send_Prompt();
                    
                    btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                    btn_state = BTN_STATE_WAIT_RELEASE;
                    break;
                }
                
                if(btn_counter < BTN_8_SEC_COUNT)
                {
                  #ifdef CONFIGURE_WIRELESS_MODULE
                  // This is Press/Hold for 4 Seconds 

                    // Determine Actions Required based upon Mode of Operation
                    if(FLAG_APP_SEARCH_MODE && !FLAG_PREV_JOINED)
                    {
                        // No Coordinator for WiFi models
                        #ifndef CONFIGURE_WIFI
                            #ifdef CONFIGURE_SERIAL_BTN_TEST
                                SER_Send_String("Est. Coord");
                                SER_Send_Prompt();
                            #else
                                // During Search Mode a Press/Hold
                                // Sends Establish a Coordinator COMM Message  
                                // via CCI
                                FLAG_REQ_ESTABLISH_COORD = 1;
                                FLAG_REQ_VOICE_JOIN_COORD = 1;
                            #endif

                        #else
                            //    
                            // For WiFi send end Search Status
                            // We do this by setting Coordinator bit of 
                            // COMM Status
                            // 
                                
                            // Cancel Search Mode   
                            #ifndef CONFIGURE_UNDEFINED
                                FLAG_REQ_ESTABLISH_COORD = 1; 
                                
                                //
                                // WiFi Module should end Search Mode
                                // send 0x80 COMM status
                                //FLAG_COMM_JOIN_IN_PROGRESS = 0;
                                
                            #else
                                 SER_Send_String("\r4Sec Btn Ignored\r");
                            #endif

                        #endif
                    }
                    else if(FLAG_APP_SEARCH_MODE && FLAG_PREV_JOINED)
                    {
                        // Add actions if User BTN Presses and Holds 4 seconds
                        //  while searching to RE-Join network
                        //  (maybe a voice annunciation)
                        
                        #ifndef CONFIGURE_WIFI
                            // For now repeat the Network Search message
                            #ifdef CONFIGURE_VOICE
                                VCE_Play(VCE_NETWORK_SEARCH);
                            #endif
                        #else
                            //
                            // For WiFi send end Search Mode Status
                            // We do this by setting Coordinator bit of 
                            // COMM Status
                            //    
                                
                            // For WiFi cancel Stealth Search Mode    
                            #ifndef CONFIGURE_UNDEFINED
                                FLAG_REQ_ESTABLISH_COORD = 1;
                                
                                //
                                // WiFi Module should end Search Mode
                                // send 0x80 COMM status
                                //FLAG_COMM_JOIN_IN_PROGRESS = 0;
                            #else
                                 SER_Send_String("\r4Sec Btn Ignored\r");
                            #endif
                        #endif

                    }
                    else if(FLAG_APP_JOIN_MODE)
                    {
                        #ifdef CONFIGURE_SERIAL_BTN_TEST
                            SER_Send_String("Network Close");
                            SER_Send_Prompt();
                        #else

                            // No Join Close Button action for WiFi models
                            #ifndef CONFIGURE_WIFI
                                // If in Join (Network Open Mode)
                                // Request to Close Network
                                FLAG_REQ_NETWORK_CLOSE = 1;
                                FLAG_JOIN_CLOSE_INITIATING = 1;

                                // Halt Join Mode Pings for minimum of 12 sec
                                LED_Set_Join_Ping(LED_JOIN_PING_12_SEC);
                            #else
                                //
                                // For WiFi send end Cloud Connect Status
                                // We do this by setting Coordinator bit of 
                                // COMM Status
                                //
                                
                                // For WiFi End Cloud Join Mode    
                                #ifndef CONFIGURE_UNDEFINED
                                    FLAG_REQ_ESTABLISH_COORD = 1; 
                                    
                                    //
                                    // WiFi Module should end Search Mode
                                    // send 0x80 COMM status
                                    //FLAG_COMM_JOIN_IN_PROGRESS = 0;
                                    
                                #else
                                     SER_Send_String("\r4Sec Btn Ignored\r");
                                #endif
                            #endif
                        #endif

                    }
                    else if(FLAG_NETWORK_ERROR_MODE)
                    {
                        if( AMP_FAULT_CCI == FLT_NetworkFaultCode)
                        {
                            
                            #ifdef CONFIGURE_WIFI   
                                // WiFi power?
                                if(FLAG_AC_DETECT)
                                {
                                    //
                                    // Attempt to Clear Network Error
                                    // If CCI is bad should re-enter Error
                                    //
                                    APP_Reset_Fault();
                                }
                            #else
                                // Do nothing as CCI Fault has automatic Fault
                                //  recovery
                            #endif
                        }
                        else
                        {
                            if(!FLAG_WIRELESS_DISABLED)
                            {
                                
                                #ifdef CONFIGURE_WIFI
                                // Reset Fault will clear fault and attempt to 
                                // Re-join Send Reset Fault Message to RF Module

                                    // WiFi power?
                                    if(FLAG_AC_DETECT)
                                    {
                                        // Send Fault Reset message via CCI
                                        APP_Reset_Fault();
                                    }
                                
                                #else
                                    APP_Reset_Fault();
                                #endif

                                // Reset task for Faster Response to Error Reset
                                MAIN_Reset_Task(TASKID_TROUBLE_MANAGER);

                            }
                        }
                    }
                    else
                    {
                        //Inhibit Join Request under certain conditions
                        if( FLAG_EOL_FATAL ||FLAG_FAULT_FATAL ||
                            FLAG_CO_ALARM_ACTIVE || FLAG_SMOKE_ALARM_ACTIVE ||
                            FLAG_ALERT_PANIC || FLAG_ALERT_WEATHER ||
                            FLAG_ALERT_DETECT || FLAG_ALARM_IDENTIFY ||
                            FLAG_SMOKE_HUSH_ACTIVE || FLAG_ALARM_LOCATE_MODE ||
                            FLAG_PTT_ACTIVE)
                        {
                            
                            // Ignore Button Press, Do Nothing
                            SER_Send_String("\r4Sec Btn Ignored\r");
                        }
                        else
                        {
                            #ifdef CONFIGURE_SERIAL_BTN_TEST
                                SER_Send_String("Network Open");
                                SER_Send_Prompt();
                            #else
                                //
                                // Keep this join request by button
                                // for WiFi (for now)
                                //
                                #ifdef CONFIGURE_WIFI
                                    //
                                    // Ignore 4 Second Button Press
                                    //  for WiFi if not in commissioning state
                                    //
                                    SER_Send_String("\r4Sec Btn Ignored\r"); 
                                        
                                    #ifdef CONFIGURE_UNDEFINED
                                    //    
                                    // Only Re-Commission if
                                    // Not Connected Status and AC Power is ON
                                    //
                                    if(FLAG_COMM_RFD &&
                                       FLAG_COMM_JOIN_COMPLETE &&
                                       FLAG_COMM_ONLINE)
                                    {
                                        // Currently Connected
                                        SER_Send_String("\r4Sec Btn Ignored\r"); 
                                    }
                                    else if(FLAG_AC_DETECT)
                                    {
                                       // Network Open/Join Request
                                        // APP Module transmits any COM Status 
                                        // Change
                                        FLAG_REQ_ENABLE_JOIN = 1;

                                        // Clear any Network Err conditions with
                                        //  this Join Request
                                        APP_Reset_Fault();

                                        // This can only be an RFD Join 
                                        FLAG_REQ_VOICE_JOIN_RFD = 1;
                                    }
                                    #endif
                                #else
                                    //
                                    // Network Open/Join Request
                                    // APP Module transmits any Comm Status 
                                    // Change
                                    //
                                    FLAG_REQ_ENABLE_JOIN = 1;

                                    // Clear any Network Err conditions with
                                    //  this Join Request
                                    APP_Reset_Fault();

                                    // This can be either an RFD Join or 
                                    // COORD Join 
                                    if(FLAG_COMM_COORDINATOR)
                                    {
                                        FLAG_REQ_VOICE_JOIN_COORD = 1;
                                    }
                                    else
                                    {
                                        FLAG_REQ_VOICE_JOIN_RFD = 1;
                                    }
                                
                                #endif
                            #endif
                        }
                    }
                  #endif

                }
                else
                {
                    //This is an 8 Second Press and Hold Function
                            
                    //Inhibit OOB Request under certain conditions
                    // Allowed during EOL Mode or EOL Hush but not Fatal
                    if( FLAG_EOL_FATAL ||FLAG_FAULT_FATAL || 
                        FLAG_LOW_BATTERY || FLAG_PTT_ACTIVE ||
                        FLAG_ALERT_PANIC || FLAG_ALERT_WEATHER ||
                        FLAG_ALERT_DETECT || FLAG_ALARM_IDENTIFY ||
                        FLAG_SMOKE_HUSH_ACTIVE || FLAG_ALARM_LOCATE_MODE ||
                        FLAG_SMOKE_ALARM_ACTIVE || FLAG_CO_ALARM_ACTIVE ||
                        FLAG_CO_ALARM_CONDITION)
                    {
                        // Ignore Button Press, Do Nothing
                        SER_Send_String("8 Sec Press Ignored");
                        SER_Send_Prompt();
                    }
                    
                    #ifdef CONFIGURE_WIFI
                        else if(!FLAG_AC_DETECT)
                        {
                            // Ignore Reset to OOB
                            SER_Send_String("8 Sec Press Ignored");
                            SER_Send_Prompt();
                        }
                    #endif
                    
                    else
                    {
                        if(!FLAG_WIRELESS_DISABLED)
                        {
                            // This is Button Press longer than 8 seconds
                            // Reset to OOB state 
                            #ifdef CONFIGURE_SERIAL_BTN_TEST
                                SER_Send_String("Reset OOB");
                                SER_Send_Prompt();
                            #else
                                #ifdef CONFIGURE_WIRELESS_MODULE
                                    // Set the COMM Status Out Bit
                                    // APP Module should transmit any Comm  
                                    // Status Change
                                    FLAG_REQ_RESET_OOB = 1;

                                    // A Reset Fault will clear fault and 
                                    // attempt to Reset to OOB. Send Reset 
                                    // Fault Message to RF Module
                                    APP_Reset_Fault();

                                    #ifdef CONFIGURE_VOICE
                                        // Announce Resetting Voice Message
                                        VCE_Play(VCE_RESET_WIRELESS_SETTINGS);
                                    #endif
                                #endif
                            #endif
                        }
                        else
                        {
                            // Wireless Module is not operating
                            #ifdef CONFIGURE_VOICE
                                // Announce Replace Alarm Voice Message
                                VCE_Play(VCE_REPLACE_ALARM);
                            #endif
                        }
                    }
                }

                btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                btn_state = BTN_STATE_WAIT_RELEASE;

            }
            break;
            
            
            case    BTN_STATE_FUNCTION_4:
            {
                // This is Press/Release then Press and Hold Functions
                if(btn_counter < BTN_4_SEC_COUNT)
                {
                    // Press/Release and Press/Hold under 4 seconds 
                    // Currently No function for this condition
                    #ifdef CONFIGURE_SERIAL_BTN_TEST
                        SER_Send_String("PR/PH under 4 secs");
                        SER_Send_Prompt();
                   #endif
                }
                else if(btn_counter < BTN_10_SEC_COUNT)
                {
                  // Press/Release and Press/Hold 4-9 seconds
                    #ifdef CONFIGURE_SERIAL_BTN_TEST
                        SER_Send_String("PR/PH 4-9 secs");
                        SER_Send_Prompt();
                    #endif

                  #ifdef CONFIGURE_BUTTON_ALG_ENABLE
                        // Disable Smoke Algorithm and init. the ALG disable 
                        //  timer
                        FLAG_ALG_ENABLED = 0;
                        FLAG_BUTTON_ALG_ENABLED = 0;
                        btn_alg_disable_timer = ALG_OFF_TIME;
                        
                        SER_Send_String("ALG Off ");
                        SER_Send_Prompt();
                        
                        #ifdef CONFIGURE_VOICE
                            // Use as ALG Off feedback
                            VCE_Play(VCE_SND_NETWORK_ERROR);
                        #endif   
                        
                        
                  #endif

                }
                else
                {
                    // Press/Release and Press/Hold 10 seconds
                    #ifdef CONFIGURE_SERIAL_BTN_TEST
                        SER_Send_String("PR/PH 10 secs");
                        SER_Send_Prompt();
                    #endif

                  #ifdef CONFIGURE_CO_FUNC_TEST

                    // Functional Test Mode Selected
                    FLAG_FUNC_CO_TEST = 1;
                    MAIN_Reset_Task(TASKID_COALARM);

                    // Init Timer for Functional Testing
                    btn_counter = FUNC_CO_TIMEOUT_TIME;

                    // Reset CO Measure state
                    CO_Meas_Reset();

                    btn_state = BTN_STATE_FUNC_TEST;

                    return MAIN_Make_Return_Value(BTN_INTERVAL_2_SEC);

                  #else

                    // No action required
                    
                  #endif

                }
                
                btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                btn_state = BTN_STATE_WAIT_RELEASE;
            }
            break;

            case BTN_STATE_TIMER_2:
            {
                // Timing the Press/Release, Press/Hold Functions
                if(PORT_TEST_BTN_PIN)
                {
                    // Button Still Pressed longer than 2 seconds
                    btn_counter++;

                        if(btn_counter >= BTN_MAX_COUNT)
                        {
                            // We have no functions past 15 seconds
                            // Maximum Button Press proceed to button stuck
                            //  with no button function action
                            
                            btn_state = BTN_STATE_BTN_STUCK;
                            btn_counter = BTN_STUCK_COUNT;
                            
                        }
                        else if(BTN_4_SEC_COUNT == btn_counter)
                        {
                            #ifdef CONFIGURE_WIRELESS_MODULE
                                #ifdef CONFIGURE_BTN_CHIRP
                                    SND_Set_Chirp_Cnt(BTN_PRESS_2_BEEPS);                            
                                #else
                                    #ifdef CONFIGURE_VOICE
                                        if(!FLAG_VOICE_BUSY)
                                        {
                                            VCE_Play(VCE_SND_BTN_PRESS);
                                            VCE_Play(VCE_SND_BTN_PRESS);
                                            VCE_Play(VCE_PAUSE_500_MS);
                                        }
                                    #endif
                                #endif
                            #endif
                        }
                        else if(BTN_8_SEC_COUNT == btn_counter)
                        {
                            #ifdef CONFIGURE_WIRELESS_MODULE
                                #ifdef CONFIGURE_BTN_CHIRP
                                    SND_Set_Chirp_Cnt(BTN_PRESS_3_BEEPS);                            
                                #else
                                    #ifdef CONFIGURE_VOICE
                                        if(!FLAG_VOICE_BUSY)
                                        {
                                            VCE_Play(VCE_SND_BTN_PRESS);
                                            VCE_Play(VCE_SND_BTN_PRESS);
                                            VCE_Play(VCE_SND_BTN_PRESS);
                                            VCE_Play(VCE_PAUSE_500_MS);
                                        }
                                    #endif
                                #endif
                            #endif
                        }
                        else
                        {
                            // Remain in this State
                        }
                }
                else
                {
                    // Button Released, proceed
                    if(btn_counter < BTN_4_SEC_COUNT)    
                    {
                        // Proceed to Standard Button Press and Release 
                        // Functions if under 4 seconds
                        btn_state = BTN_STATE_FUNCTION;

                    }
                    else
                    {
                        #ifdef CONFIGURE_WIRELESS_MODULE
                            // Proceed to > 4 seconds Press/Hold functions
                            btn_state = BTN_STATE_FUNCTION_3;
                        #else
                            btn_counter = WAIT_RELEASE_TIMEOUT_TIME;
                            btn_state = BTN_STATE_WAIT_RELEASE;
                        #endif
                    }
                }
            }
            break;

            case BTN_STATE_BTN_STUCK:
            {
                FLAG_ACTIVE_TIMER = 1;
                // Minimum time to remain in Active Mode
                MAIN_ActiveTimer = TIMER_ACTIVE_MINIMUM_2_SEC;
                
                // Wait in this state until the PTT Button is inactive
                if(PORT_TEST_BTN_PIN)
                {
                    if(0 == --btn_counter)
                    {
                        btn_counter = BTN_STUCK_COUNT;
                    
                       /* 
                        * Force a BTN chirp every 4 seconds
                        */
                        HORN_OFF
                        PIC_delay_ms(BTN_DLY_50_MS);
                        HORN_ON
                        PIC_delay_ms(BTN_DLY_50_MS);
                        HORN_OFF
                        
                        #ifndef CONFIGURE_UNDEFINED
                            SER_Send_String("Button Stuck");
                            SER_Send_Prompt();
                        #endif
                        
                    }
                    
                    // Remain in this state until Button is inactive
                    return MAIN_Make_Return_Value(BTN_1_SEC_TIME);

                }
                else
                {
                    btn_state = BTN_STATE_IDLE;
                }
            }
            break;



            default:
                btn_state = BTN_STATE_IDLE;
            break;

    }

    return MAIN_Make_Return_Value(BTN_CHECK_INTERVAL_100MS);
}

/*
+------------------------------------------------------------------------------
| Function:      BTN_Get_State
+------------------------------------------------------------------------------
| Purpose:       Return Current Button State
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  btn_state  (see BTN_STATE Constants in header file)
+------------------------------------------------------------------------------
*/

unsigned char BTN_Get_State(void)
{
    return  btn_state;
}


/*
+------------------------------------------------------------------------------
| Function:      BTN_Chk_ALG_Off_Timer
+------------------------------------------------------------------------------
| Purpose:       Check Algorithm Off timer and enable algorithm if it times out
|                This should be called every minute
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void BTN_Chk_ALG_Off_Timer(void)
{
   if(!FLAG_BUTTON_ALG_ENABLED)
   {
       #ifndef CONFIGURE_UL_TESTING
           // No Timer for UL testing
           if(0 == --btn_alg_disable_timer)
           {
               FLAG_BUTTON_ALG_ENABLED = 1;
               FLAG_ALG_ENABLED = 1;
               
               SER_Send_String("ALG On ");
               SER_Send_Prompt();
               
           }
       #endif

   }
}