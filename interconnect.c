/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        interconnect.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Functions for hardwire interconnect.
|               
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include    "common.h"
#include    "interconnect.h"
#include    "a2d.h"
#include    "vboost.h"
#include    "alarmprio.h"
#include    "app.h"

#ifdef CONFIGURE_INTERCONNECT

/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

#define INTCON_MINIMUM_HIGH_COUNTS (uchar)3
#define INTCON_MINIMUM_DETECT_COUNTS (uchar)3

#define INTCON_TX_STATE_COMPLETE	19	// 1 low + 16 data + 2 low bits

// (INTCON_RX_BIT_TICS/MAIN_TIC_INTERVAL)
#define INTCON_RX_BIT_TICS          (uchar)10	// 10 10mS passes for 100mS
#define INTCON_RX_BIT_TIC_8         (uchar)8    // 8 Bit Tics
 
#define INTCON_DEVICE_ADDRESS		0xA0    // 10100000
#define INTCON_CO_ALARM_ID          0x05    // 00000101
#define INTCON_SIGNAL_CO_MESSAGE	0xA5
#define INTCON_SIGNAL_TX_CO_MESSAGE	0x8822  // 10 00 10 00 00 10 00 10
#define INTCON_SIGNAL_FIRE_MESSAGE	0xFF    // 11111111

#define INT_DEFAULT_INTERVAL_10_MS  (uint)10
#define INT_INTERVAL_1_SECOND       (uint)1000
#define INT_INTERVAL_2_SECONDS      (uint)2000
#define INT_INTERVAL_4_SECONDS      (uint)4000
#define INT_INTERVAL_5_SECONDS      (uint)5000
#define INT_INTERVAL_15_SECONDS     (uint)15000
#define INT_BIT_TIME_50_MS          (uint)50
#define INT_INTERVAL_200_MS         (uint)200
#define INT_INTERVAL_100_MS         (uint)100

#define INT_IN_VREF 2500    //mVolts

// This is INT_IN signal Threshold Level Voltage
#define INT_IN_HIGH_VOLTAGE 1000000        //uVolts
#define INT_IN_HIGH_COUNTS  (((INT_IN_HIGH_VOLTAGE/INT_IN_VREF) * 1024)/1000)

#define INT_IN_BIT_COUNT_MAX 0xff

/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
static uchar bIntcon_State = 0;
static uchar bIntconRxBitTics;
static uchar bIntconBitCounter;
static uchar bIntconRxHighCount;
static uint bIntDataReg;


/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/
void int_intcon_rx_reset(void);
void int_inc_high_count(void);
uchar int_process_rx_bit(void);


/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/

/*
+------------------------------------------------------------------------------
| Function:      INT_Intercon_Init
+------------------------------------------------------------------------------
| Purpose:       Initialize hardware Interconnect Hardware and State
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void INT_Intercon_Init(void)
{
    int_intcon_rx_reset();

    #ifdef CONFIGURE_INT_ANALOG_INPUT
        // Set Port B0 to analog input
        ANSELBbits.ANSB0 = 1;
    #endif
    
    bIntcon_State = INTCON_STATE_INIT;
}


/*
+------------------------------------------------------------------------------
| Function:      uchar INT_get_state(void)
+------------------------------------------------------------------------------
| Purpose:       Return current Interconnect state
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
uchar INT_get_state(void)
{
    return bIntcon_State;
}


/*
+------------------------------------------------------------------------------
| Function:      INT_Process_Task
+------------------------------------------------------------------------------
| Purpose:       Hardware Interconnec Task Management
|                Handles Smoke and CO Alarm output messages
|                and reception of Smoke and CO Alarm input messages
|                Smoke alarm takes priority over CO alarm
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Interconnect Process Next Execution Time (in Task TICs)
+------------------------------------------------------------------------------
*/
uint INT_Process_Task(void)
{
    #ifdef CONFIGURE_DEBUG_INT_STATES
        // Test Only
        for(uchar i = (bIntcon_State + 1); i > 0; i--)
        {
            // Test Only
            MAIN_TP_On();
            PIC_delay_us(DBUG_DLY_25_USec);
            MAIN_TP_Off();
        }
    #endif

    // VBoost Off, in Active mode if AC Power is ON
    if(FLAG_AC_DETECT)
    {
        // VINT supplied by AC Power Supply
        VBOOST_OFF
    }
    else
    {
        // VINT must be supplied by VBoost

        // Leave it off if we are not transmitting anything
        //  so check Master/Remote Smoke or Master CO Alarm flags
        if(FLAG_MASTER_SMOKE_ACTIVE  || FLAG_REMOTE_ALARM_MODE ||
           (FLAG_CO_ALARM_CONDITION && FLAG_CO_ALARM_ACTIVE))
        {
            VBOOST_ON

            #ifdef CONFIGURE_TEST_VBOOST
                // We Test the VBoost Voltage here, if it ever drops
                // as a result of a short, then Reset the Interconnect Circuit
                // (Disables Interconnect and Enters Stand Alone Mode)

                // Currently, VINT voltage is supplied by either the AC Power
                // Supply (10 volts) or by VBoost, if AC Power is OFF.

                if(FALSE == VBOOST_Test())
                {
                    // If VINT Power is Low, we reset Interconnect and operate
                    // in Stand alone mode.
                    int_intcon_rx_reset();

                    // Monitor VBoost every so often since it reads low.
                    return MAIN_Make_Return_Value(INT_INTERVAL_4_SECONDS);
                }
            #endif

        }
        else
        {
            // We may get a final CO transmit after exiting alarm
            //   So leave VBoost ON
            if(!FLAG_INTCON_TX_CO_SIGNAL)
            {
                VBOOST_OFF
            }
        }
    }

    switch (bIntcon_State)
    {
        case INTCON_STATE_INIT:

            bIntcon_State++;
            
            #ifndef CONFIGURE_CO
                #ifdef CONFIGURE_WIRELESS_MODULE
                    // Longer HW Interconnect Init Period for Smoke Only
                    //  wireless models
                    return MAIN_Make_Return_Value(INT_INTERVAL_15_SECONDS);
                    
                #else
                    // Start with some delay before monitoring INT_IN
                    return MAIN_Make_Return_Value(INT_INTERVAL_5_SECONDS);
                #endif

            #else
                // Start with some delay before monitoring INT_IN
                return MAIN_Make_Return_Value(INT_INTERVAL_5_SECONDS);
            #endif


        //*********************************************************************
        case INTCON_STATE_IDLE:
            
            #ifndef CONFIGURE_WIRELESS_MODULE
                // Always Enable Interconnect Drive Out for non-Wireless
                //  interconnect models
                FLAG_ALM_INT_DRV_ENABLE = 1;
                
                // Never Should be set in this model
                FLAG_ALARM_LOCATE_MODE = 0;
            #endif

            #ifdef CONFIGURE_WIRELESS_MODULE
               #ifdef CONFIGURE_INTERCONNECT 
                    // Test for Remote PTT delay active
                    APP_Rmt_PTT_Timer();
               #endif
            #endif
        
            // Monitor for HW Interconnect Gateway Disabled
            // If in Remote Smoke and gateway is OFF disable INT TX
            // If in Remote CO and gateway is OFF disable INT TX
            // If Receiving Slave Smoke or CO during Remote PTT disable INT TX
            if( FLAG_SLAVE_ALARM_MODE ||
                    
                ( (ALM_ALARM_REMOTE_SMOKE == ALM_Get_State() ) &&
                  !FLAG_GATEWAY_REMOTE_TO_SLAVE )  ||
                    
                ( (ALM_ALARM_REMOTE_SMOKE_HW == ALM_Get_State() ) &&
                  !FLAG_GATEWAY_REMOTE_TO_SLAVE )  ||                    
                    
                ( (ALM_ALARM_REMOTE_CO == ALM_Get_State() ) &&
                  !FLAG_GATEWAY_REMOTE_TO_SLAVE )   ||
                    
                ( (ALM_ALARM_REMOTE_CO_HW == ALM_Get_State() ) &&
                  !FLAG_GATEWAY_REMOTE_TO_SLAVE )   ||
                    
                ( FLAG_REMOTE_PTT_ACTIVE  && 
                  (FLAG_INTCON_SMOKE_RECEIVED || FLAG_INTCON_CO_RECEIVED ||
                   FLAG_INT_DRV_OFF_DURING_PTT || FLAG_REMOTE_PTT_TIMER_ON) ) )
            {
                
                // Test Only, Remove later
                #ifdef CONFIGURE_UNDEFINED
                    #ifdef CONFIGURE_INT_ALM_SERIAL
                        SER_Send_String("INT TP1 ");
                        SER_Send_Prompt();
                    #endif    
                #endif
                
                // Since a HW INT signal was received during a Remote PTT
                //   set a Flag to keep HW INT transmissions off for the
                //   full duration of Remote PTT. Clear it after PTT
                // However, Don't Latch Drive Off during Remote PTT delay
                //  time
                if( FLAG_REMOTE_PTT_ACTIVE && !FLAG_REMOTE_PTT_TIMER_ON )
                {
                    FLAG_INT_DRV_OFF_DURING_PTT = 1;
                }
                else
                {
                    FLAG_INT_DRV_OFF_DURING_PTT = 0;
                }
                
                // Clear any pending CO Transmissions
                FLAG_INTCON_TX_CO_SIGNAL = 0;

                // Do not check incoming HW Alarms during PTT
                if(!FLAG_PTT_ACTIVE  && !FLAG_REMOTE_PTT_ACTIVE)
                {
                    // Here we are not allowing transmissions, but Continue to 
                    //  Check for incoming slave alarm signals
                    if(INT_Is_Int_In_Active() )
                    {

                        // There is activity on the Interconnect input.
                        // Initialize for receive state.
                        int_inc_high_count();
                        bIntcon_State = INCON_STATE_RECEIVE_1ST;
                        FLAG_INTCON_STATE_NOT_IDLE = 1;

                    }
                    else
                    {
                        // No incoming slave signals
                        FLAG_INTCON_EDGE_DETECT = 0;
                    }
                }
            }
            else
            {
                // Clear Flag to indicate Interconnect Idle State
                FLAG_INTCON_STATE_NOT_IDLE = 0;
                
                /*
                 * If Master Smoke is active, start transmitting the smoke
                 * interconnect signal, unless another alarm is already
                 * driving Interconnect Line.
                 * 
                 * Wireless Remote Smoke Alarm may also drive Interconnect
                 *  once the RF Gateway Timer hs expired.
                 * 
                 * Locate Mode is a temporary local Alarm Only, so if Master
                 *  Smoke Alarm, stop driving the Interconnect Line
                 * 
                 * Also monitor FLAG_ALM_INT_DRV_ENABLE before Transmitting
                 *  Smoke alarm signal. This becomes enabled about
                 *  10-12 seconds after Master Smoke Alarm condition begins
                 *  but only applies to Wireless Master Smoke alarms
                 */
                
                if( FLAG_MASTER_SMOKE_ACTIVE && 
                    FLAG_ALM_INT_DRV_ENABLE && !FLAG_ALARM_LOCATE_MODE )
                {
                    /*
                     * Here Master Smoke Alarm is driving Interconnect
                     * Turn the interconnect into an output & set low.
                     * Inverted logic will cause the actual interconnect
                     * line to go high.
                     */
                    INT_set_output();

                    // Interconnect Output Active (Drive I/O Line High)
                    INT_OUT_HIGH

                    bIntcon_State = INTCON_STATE_MSTR_SMOKE;
                    FLAG_INTCON_STATE_NOT_IDLE = 1;

                    if( !FLAG_REMOTE_PTT_ACTIVE && !FLAG_PTT_ACTIVE )
                    {
                        // Master Smoke Alarm acting as a Gateway
                        // PTTs should not set Master Gateway
                        FLAG_GATEWAY_MASTER = 1;
                    }

                    #ifdef CONFIGURE_INT_ALM_SERIAL
                        SER_Send_String("TX SMOKE HWINT ");
                        SER_Send_Prompt();
                    #endif     
                     
                }
                #ifdef CONFIGURE_WIRELESS_MODULE
                    else if( FLAG_ALARM_LOCATE_MODE &&
                           ( (ALM_ALARM_REMOTE_SMOKE == ALM_Get_State() ) ||
                             (ALM_ALARM_REMOTE_SMOKE_HW == ALM_Get_State() )) )
                    {
                        
                    // Test Only, Remove later
                    #ifdef CONFIGURE_UNDEFINED
                        #ifdef CONFIGURE_INT_ALM_SERIAL
                            SER_Send_String("INT TP2 ");
                            SER_Send_Prompt();
                        #endif    
                    #endif
                            
                        // In Remote Smoke Locate State
                        // Slow Down to allow time for Locate Exit
                        //  before next Interconnect Transmission
                        return MAIN_Make_Return_Value(INT_INTERVAL_1_SECOND);
                        
                    }
                    else if( !FLAG_ALARM_LOCATE_MODE  &&
                           ( (ALM_ALARM_REMOTE_SMOKE == ALM_Get_State() ) ||
                             (ALM_ALARM_REMOTE_SMOKE_HW == ALM_Get_State() )) )
                    {

                        // Remote Gateway Smoke Alarm is driving Interconnect
                        // Turn the interconnect into an output & set active.

                        INT_set_output();

                        // Interconnect Output Active (Drive I/O Line High)
                        INT_OUT_HIGH

                        bIntcon_State = INTCON_STATE_REMOTE_SMOKE;
                        FLAG_INTCON_STATE_NOT_IDLE = 1;

                        #ifdef CONFIGURE_INT_ALM_SERIAL
                            SER_Send_String("TX SMOKE HWINT ");
                            SER_Send_Prompt();
                        #endif            

                    }
                #endif
                else
                {
                    // No Master Smoke or Slave Alarms are Active
                    
                    // Send CO Signal?
                    if(FLAG_INTCON_TX_CO_SIGNAL)
                    {
                        if( FLAG_CO_ALARM_CONDITION &&
                            !FLAG_REMOTE_PTT_ACTIVE &&
                            !FLAG_PTT_ACTIVE )
                        {
                            // Master CO Alarm acting as a Gateway
                            //  PTTs should not set Master gateway
                            FLAG_GATEWAY_MASTER = 1;
                        }

                        // If in Alarm Locate halt transmitting CO Alarm
                        if(!FLAG_ALARM_LOCATE_MODE)
                        {
                            /*
                             * Turn output pin to output. Inverted logic in
                             * interconnect drive will cause interconnect  
                             * line to go low to start the first bit.
                            */

                            INT_OUT_LOW
                            INT_set_output();

                            // Prepare the byte to be transmitted.
                            bIntDataReg = INTCON_SIGNAL_TX_CO_MESSAGE;

                            // Initialize the bit counter and transition to 
                            //  transmit state.
                            bIntconBitCounter = INTCON_TX_STATE_COMPLETE;
                            bIntcon_State = INTCON_STATE_CO_TRANSMIT;
                            FLAG_INTCON_STATE_NOT_IDLE = 1;

                            return MAIN_Make_Return_Value(INT_BIT_TIME_50_MS);
                        }
                        else
                        {
                            // No CO transmissions during Locate
                            
                            
                            #ifdef CONFIGURE_UNDEFINED
                                // Test only
                                SER_Send_String("Locate, Halt Slave Alarms- ");
                                SER_Send_Prompt();
                            #endif

                            FLAG_INTCON_TX_CO_SIGNAL = 0;
                            return MAIN_Make_Return_Value
                                        (INT_INTERVAL_2_SECONDS);
                        }
                    }
                    else
                    {
                        // Do not check incoming HW Alarms during PTT
                        if(!FLAG_PTT_ACTIVE  && !FLAG_REMOTE_PTT_ACTIVE)
                        {
                            // No Alarms  Check for a new incoming signal.
                            if(INT_Is_Int_In_Active() )
                            {

                                #ifdef CONFIGURE_DEBUG_INT_IN_DETECT
                                    // Track Interconnect Signal
                                    MAIN_TP2_On();
                                #endif

                                // There is activity on the Interconnect input.
                                // Initialize for receive state.
                                int_inc_high_count();
                                bIntcon_State = INCON_STATE_RECEIVE_1ST;
                                FLAG_INTCON_STATE_NOT_IDLE = 1;

                            }
                            else
                            {
                                #ifdef CONFIGURE_DEBUG_INT_IN_DETECT
                                    // Track Interconnect Signal
                                    MAIN_TP2_Off();
                                #endif

                                #ifdef CONFIGURE_DEBUG_INT_BIT_DETECTED
                                    MAIN_TP_Off();
                                #endif

                                // No incoming slave signals
                                FLAG_INTCON_EDGE_DETECT = 0;
                            }
                        }
                    }
                }
            }

        break;

        //*********************************************************************
        case    INTCON_STATE_CO_TRANSMIT:

            // Shift out the next bit.
            if(bIntDataReg & 0x0001)
            {
                // Bit is High
                INT_OUT_HIGH
            }
            else
            {
                // Bit is Low
                INT_OUT_LOW
            }

            bIntDataReg = (bIntDataReg >> 1);

            if(0 == --bIntconBitCounter)
            {
                // The last bit has been sent.  Reset state machine.
                FLAG_INTCON_TX_CO_SIGNAL = 0;
                int_intcon_rx_reset();
                
                
                #ifdef CONFIGURE_INT_ALM_SERIAL
                    SER_Send_String("TX CO HWINT ");
                    SER_Send_Prompt();
                #endif
            }

            // 50 ms Bit Times
            return MAIN_Make_Return_Value(INT_BIT_TIME_50_MS);
 

        //*********************************************************************
        case    INTCON_STATE_MSTR_SMOKE:
            
            #ifdef CONFIGURE_WIRELESS_MODULE
                // Do not Drive HWINT during 2 minute Locate
                if(FLAG_ALARM_LOCATE_MODE)
                {
                    INT_OUT_LOW
                    bIntcon_State = INCON_STATE_WAIT;
                    
                    #ifdef CONFIGURE_INT_ALM_SERIAL
                        SER_Send_String("TX SMOKE HWINT LOCATE ACTIVE");
                        SER_Send_Prompt();
                    #endif     

                    // Discharge the Line before returning to input
                    return MAIN_Make_Return_Value(INT_INTERVAL_200_MS);
                }
            #endif
            
            // 	If Master Smoke is inactive, clear smoke interconnect signal.
            if( !FLAG_MASTER_SMOKE_ACTIVE && !FLAG_SMOKE_ALARM_ACTIVE )
            {
                INT_OUT_LOW
                bIntcon_State = INCON_STATE_WAIT;

                // Discharge the Line before returning to input
                return MAIN_Make_Return_Value(INT_INTERVAL_200_MS);
            }

        break;

        //*********************************************************************
        case    INTCON_STATE_REMOTE_SMOKE:
            //
            // 	If Remote Smoke is inactive, clear smoke interconnect signal.
            //  Also during locate FLAG_SMOKE_ALARM_ACTIVE held inactive
            //
            if( !FLAG_ALARM_LOCATE_MODE  &&
                ( (ALM_ALARM_REMOTE_SMOKE == ALM_Get_State() ) ||
                  (ALM_ALARM_REMOTE_SMOKE_HW == ALM_Get_State() )) )
            {
                INT_set_output();

                // Interconnect Output Active (Drive I/O Line High)
                if(!FLAG_SMK_SYNC_ACTIVE)
                {
                    INT_OUT_HIGH
                }
            }
            else
            {
                INT_OUT_LOW
                bIntcon_State = INCON_STATE_WAIT;

                // Discharge the Line before returning to input
                return MAIN_Make_Return_Value(INT_INTERVAL_200_MS);
            }
        break;
        
        

        //*********************************************************************
        case    INCON_STATE_WAIT:
            /*
             * Turn output back into input. This puts the interconnect 
             * circuitry in a high impedance state.
            */
            INT_set_input();
            bIntcon_State = INTCON_STATE_IDLE;

        break;

        //*********************************************************************
        case    INCON_STATE_RECEIVE_1ST:
            // signal is active high for rx
            if(INT_Is_Int_In_Active() )
            {
                #ifdef CONFIGURE_DEBUG_INT_IN_DETECT
                    // Track Interconnect Signal
                    MAIN_TP2_Off();
                    PIC_delay_us(DBUG_DLY_20_USec);
                    MAIN_TP2_On();
                #endif

                 // Interconnect pin is high
                int_inc_high_count();
            }
            else
            {
                #ifdef CONFIGURE_DEBUG_INT_IN_DETECT
                    // Track Interconnect Signal
                    MAIN_TP2_On();
                    PIC_delay_us(DBUG_DLY_20_USec);
                    MAIN_TP2_Off();
                #endif

                // Interconnect pin is low
                    
                /*
                 *  To help eliminate noise, a minimum number of highs must 
                 *  be received before the start of the transmission is 
                 *  recognized.  If a minimum number of high counts wasn't 
                 *  received, and now the line is low, the
                 *  recently received highs are disgarded as noise.
                 */
                if(INTCON_MINIMUM_DETECT_COUNTS > bIntconRxHighCount)
                {
                    // Not enough highs for a valid bit, reset
                    int_intcon_rx_reset();
                    return MAIN_Make_Return_Value(INT_DEFAULT_INTERVAL_10_MS);
                }

            }

            // Process High Bit
            if( TRUE == int_process_rx_bit() )
            {
                // Bit complete
                // First bit has been received. Go to Receive Next state.
                bIntcon_State = INTCON_STATE_RECEIVE_NEXT;
            }

        break;

        //*********************************************************************
        case    INTCON_STATE_RECEIVE_NEXT:

            if(INT_Is_Int_In_Active() )
            {
                #ifdef CONFIGURE_DEBUG_INT_IN_DETECT
                    // Track Interconnect Signal
                    MAIN_TP2_Off();
                    PIC_delay_us(DBUG_DLY_20_USec);
                    MAIN_TP2_On();
                #endif

                // Interconnect pin is high
                    
                // Increment the high count before processing the sample.
                int_inc_high_count();
            }
            else
            {
                #ifdef CONFIGURE_DEBUG_INT_IN_DETECT
                    // Track Interconnect Signal
                    MAIN_TP2_On();
                    PIC_delay_us(DBUG_DLY_20_USec);
                    MAIN_TP2_Off();
                #endif
            }

            // Process the Sample
            if(int_process_rx_bit() )
            {
               #ifdef CONFIGURE_WIRELESS_MODULE
                    if(!FLAG_WL_GATEWAY_ENABLE)
                    {
                        if(!FLAG_SLAVE_ALARM_MODE)
                        {
                            // Flag added to allow Gateway HW INT Detect to 
                            //  respond faster if another alarm is trying to  
                            //  grab the HW Int signal but only if not already
                            //  in Slave Alarm
                            FLAG_INT_ACTIVITY_DETECTED = 1;

                            //If the LSB of bIntDataReg reads zero, then this  
                            //  must be incoming CO Alarm signal 
                            //  (cannot be smoke)
                            if(!FLAG_INT_CO_ACTIVITY_DETECTED)
                            {
                                // Monitor bit 0 throughout this serial 
                                // reception
                                if(0 == (bIntDataReg & 0x01))
                                {
                                    FLAG_INT_CO_ACTIVITY_DETECTED = 1;
                                    
                                    // TODO: Undefine Later  BC: Test Only
                                    #ifdef CONFIGURE_UNDEFINED
                                        SER_Send_String("INT CO Detected...");
                                        SER_Send_Prompt();
                                    #endif
                                }
                            }
                        }
                    }
                    
                    
                #endif
                
                
                
                
                // Bit complete
                // See if the last bit has been received.
                if(bIntconBitCounter >= 8)
                {
                    // Check for smoke message.
                    if(INTCON_SIGNAL_FIRE_MESSAGE == bIntDataReg)
                    {
                        
                        // A fire message was receieved.
                        FLAG_INTCON_SMOKE_RECEIVED = 1;

                        #ifdef CONFIGURE_INT_ALM_SERIAL
                            if(!FLAG_SLAVE_SMOKE_ACTIVE)
                            {
                                SER_Send_String("RX SMOKE HWINT ");
                                SER_Send_Prompt();
                            }
                        #endif
                      
                    }
                    else
                    {
                        // Check for CO message.
                        if( (INTCON_SIGNAL_CO_MESSAGE == bIntDataReg) &&
                             !FLAG_INTCON_CO_RECEIVED )
                        {
                            // A CO message was receieved.
                            FLAG_INTCON_CO_RECEIVED = 1;

                            #ifdef CONFIGURE_INT_ALM_SERIAL
                                SER_Send_String("RX CO HWINT ");
                                SER_Send_Prompt();
                            #endif
                        }
                        
                        // It wasn't CO or Smoke, just Reset the receiver.
                    }

                    int_intcon_rx_reset();
                }
            }
        break;

        default:
            bIntcon_State = INTCON_STATE_IDLE;
            break;

    }
    return MAIN_Make_Return_Value(INT_DEFAULT_INTERVAL_10_MS);
}


/*
+------------------------------------------------------------------------------
| Function:      INT_Is_Int_In_Active
+------------------------------------------------------------------------------
| Purpose:       Tests the hardware interconnect line for its current state
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Returns TRUE if the interconnect input signal is active. 
|                FALSE otherwise.
+------------------------------------------------------------------------------
*/
uchar INT_Is_Int_In_Active(void)
{
    #ifdef CONFIGURE_INT_ANALOG_INPUT
        // Analog input for Interconnect
    
        // Read INIT_IN as analog voltage and compare against
        // High/Low Threshold

        uint adval = A2D_Convert_10Bit(A2D_INT_IN_CHAN);

        if( (uint)INT_IN_HIGH_COUNTS < adval )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }

    #else
        // Digital input for Interconnect
        
        if(PORT_INT_IN_PIN)
        {
            #ifdef CONFIGURE_DEBUG_INTERCONNECT_IN
                MAIN_TP_On();
            #endif

            return TRUE;
        }
        else
        {
            #ifdef CONFIGURE_DEBUG_INTERCONNECT_IN
                MAIN_TP_Off();
            #endif

            return FALSE;
        }

    #endif
}


/*
+------------------------------------------------------------------------------
| Function:      INT_set_output
+------------------------------------------------------------------------------
| Purpose:       Sets hardware I/O in preparation for driving the 
|                hardware interconnect line
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void INT_set_output(void)
{
    //	Set interconnect circuit pins to outputs.
    PORT_INT_LO_DRV_ENA
    PORT_INT_HI_DRV_ENA
    PORT_INT_OUT_ENA

    // Set interconnect out pin to digital.
    PORT_INT_OUT_DIGITAL

    LAT_INT_HI_DRV_PIN = 1;
    LAT_INT_LO_DRV_PIN = 0;

    /* When Driving Out on the Interconnect, we should disable
     *  the Interrupt on Change Bit to prevent un-needed INT_IN INTS.
     *  Re-enable IOC when we return to  Interconnect Input Mode
     */
    INT_IN_DISABLE_IOC



}

/*
+------------------------------------------------------------------------------
| Function:      INT_set_input
+------------------------------------------------------------------------------
| Purpose:       Sets hardware I/O in preparation for reception on the 
|                hardware interconnect line
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void INT_set_input(void)
{

    //	Set interconnect circuit pins for Input Mode

    // Floating Digital I/O not recommended (for current)
    #ifdef CONFIGURE_UNDEFINED
        PORT_INT_LO_DRV_DIS
        PORT_INT_HI_DRV_DIS
    #endif

    LAT_INT_HI_DRV_PIN = 0;
    LAT_INT_LO_DRV_PIN = 0;

    // Set interconnect output pin to analog input to prevent leakage currents.
    PORT_INT_OUT_DIS
    PORT_INT_OUT_ANALOG

    // Reset INT_IN IOC enable and Flags
    if(IOCBFbits.IOCBF0)
    {
        IOCBFbits.IOCBF0 = 0;
        IOC_INTERRUPT_FLAG = 0;
    }

    // Re-enable Interrupt on Change
    INT_IN_ENABLE_IOC

}


/*
+------------------------------------------------------------------------------
| Function:      int_process_rx_bit
+------------------------------------------------------------------------------
| Purpose:       Process received bits
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  TRUE if bit is complete. FALSE if in the middle of a bit.
|                
+------------------------------------------------------------------------------
*/
uchar int_process_rx_bit(void)
{
    // 	Increment our tic counter each time through. If its 10,
    //    we've seen enough to accept the bit,
    if(++bIntconRxBitTics  >= INTCON_RX_BIT_TICS)
    {
        // We've now seen enough tics, accept the bit and clear the timers, etc

        bIntconRxBitTics = 0;   // Restart the bit timer.
        bIntconBitCounter++;    // Inc the number of Bits we've seen

        /*
         *  Determine whether a one bit or zero bit was received.  If the high
         *  count is greater than a minimum threshold, a one bit was received.
         *  Otherwise, a zero bit was received.
         */

        if(INTCON_MINIMUM_HIGH_COUNTS <= bIntconRxHighCount)
        {
            // New Bit is a Logic One
            bIntDataReg = (bIntDataReg << 1);
            bIntDataReg = (bIntDataReg | 1);

            #ifdef CONFIGURE_DEBUG_INT_BIT_DETECTED
                MAIN_TP_On();
            #endif
        }
        else
        {
            // bit received was a zero, just left shift
            bIntDataReg = bIntDataReg << 1;

            #ifdef CONFIGURE_DEBUG_INT_BIT_DETECTED
                MAIN_TP_Off();
            #endif
        }
        
        bIntconRxHighCount = 0;
        return TRUE;

    }
    else
    {
        // We're in the middle of a bit.
        return FALSE;
    }
}

/*
+------------------------------------------------------------------------------
| Function:      int_inc_high_count
+------------------------------------------------------------------------------
| Purpose:       Count number of 10ms periods interconnect signal is active
|                
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|                
+------------------------------------------------------------------------------
*/
void int_inc_high_count(void)
{
    if( !(INT_IN_BIT_COUNT_MAX == bIntconRxHighCount) )
    {
        // Also: We don't count bits that are at the end of the pulse.
        // Only Counts pulses during the 1st 70 ms of the bit time
        if(bIntconRxBitTics < INTCON_RX_BIT_TIC_8)
        {
            bIntconRxHighCount++;
        }
    }

}

/*
+------------------------------------------------------------------------------
| Function:      int_intcon_rx_reset
+------------------------------------------------------------------------------
| Purpose:       Reset Interconnect State to Idle State
|                Interconnect receive registers are cleared in preparation for 
|                any new messages
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
|                
+------------------------------------------------------------------------------
*/
void int_intcon_rx_reset(void)
{
    INT_set_input();
    
    bIntconBitCounter = 0;
    bIntconRxHighCount = 0;
    bIntconRxBitTics = 0;
    bIntDataReg = 0;

    bIntcon_State = INTCON_STATE_IDLE;

}



#else   // No HW Interconnect

// Dummy placeholder functions if Interconnect not Configured

uint INT_Process_Task(void)
{

    return MAIN_Make_Return_Value(62000);
}

void INT_Intercon_Init(void)
{

}


#endif