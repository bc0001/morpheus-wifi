/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        cocalibrate.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for cocalibrate.c module
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

#ifndef CALIBRATE_H
#define CALIBRATE_H


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/


    //
    //  Cal Status bit Definitions
    //
    #define STATUS_PARTIAL_BITMAP	(uchar)0
    #define STATUS_COMPLETE_BITMAP	(uchar)1
    #define STATUS_UNTESTED_BITMAP	(uchar)2

    //
    // Cal Status byte definitions
    //
    #define STATUS_CAL_ZERO             (uchar)0
    #define STATUS_CAL_150              (uchar)1
    #define STATUS_CAL_COMPLETE         (uchar)3
    #define STATUS_CAL_FINAL_TEST       (uchar)7

    // Status Checksum Definitions (complement of the status byte)
    #define STATUS_CAL_ZERO_CSUM        (uchar)0xFF
    #define STATUS_CAL_150_CSUM         (uchar)0xFE
    #define STATUS_CAL_COMPLETE_CSUM    (uchar)0xFC
    #define STATUS_CAL_FINAL_TEST_CSUM  (uchar)0xF8

    #define STATUS_CSUM_VALID           (uchar)0xFF

    // CO is Calibrated at 150 PPM
    #define CAL_PPM_LEVEL_150           150


    // The new CAV range is now 225.01mV to 339.17mV.  
    // (low - 225 = 92 A/D counts)
    // (center - 282.09 = 115 A/D counts)
    // (high - 339 = 139 A/D counts)

    //      0 PPM Cal
    #define CALIBRATION_OFFSET_MAXIMUM		(uint)170
    #define CALIBRATION_OFFSET_MINIMUM		(uint)60

    #define CALIBRATION_OFFSET_TYPICAL      (uint)116

    // NOTE: Offset is subtracted from 150 measurement in the firmware

    // Typically about 150 counts for 150 PPM +/- tolerance
    // LAB 150 Cal data shows range about 120 - 180

    //		150 PPM Cal
    #define CALIBRATION_SLOPE_MAXIMUM		(uint)250
    #define CALIBRATION_SLOPE_MINIMUM		(uint)50




/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/

/* Cal States */
enum
{
    CAL_STATE_ZERO = 0,
    CAL_STATE_150,              
    CAL_STATE_VERIFY,          
    CAL_STATE_WAITING,          
    CAL_STATE_CALIBRATED,      
    CAL_STATE_WAIT_DELAY,      
    CAL_STATE_FINAL_COMPLETE    

};


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
    void CO_Cal_Init(void);
    uint CO_Cal_Manager_Task(void);
    uchar CO_Cal_Get_State(void);









#endif  // CALIBRATE_H
		
		
