/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        comeasure.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for comeasure.c module
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

#ifndef COMEASURE_H
#define COMEASURE_H


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

// Constants for state machine
#define CO_MEAS_INIT            0
#define CO_MEAS_STABILIZE       1
#define CO_MEAS_SAMPLE          2
#define CO_MEAS_TEST_INIT       3
#define CO_MEAS_TEST_MONITOR    4


// CO Test I/O
#define CO_TEST_ON          LAT_CO_TEST_PIN = 1;
#define CO_TEST_OFF         LAT_CO_TEST_PIN = 0;



/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/

    extern uint CO_wPPM;
    extern uint CO_raw_counts;
    extern uint CO_raw_avg_counts;
    extern uint CO_vout_baseline;

    #ifdef CONFIGURE_NO_REGULATOR
        extern uint CO_vdd_calc;
    #endif

    #ifdef CONFIGURE_MANUAL_PPM_COMMAND
        extern uint CO_manual_PPM_value;
    #endif



/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
    uint CO_Measure_Task(void);
    void CO_Meas_Reset(void);
    void CO_Meas_Charge(uint);
    uchar CO_Meas_Get_State(void);


		

#endif  // COMEASURE_H
		
		
