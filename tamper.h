/* 
 * File:   tamper.h
 * Author: bill.chandler
 *
 * Created on July 29, 2013, 11:04 AM
 */

#ifndef TAMPER_H
#define	TAMPER_H

#ifdef	__cplusplus
extern "C" {
#endif


    #ifdef _CONFIGURE_TAMPER_SWITCH
        // Prototypes
        UINT Tamper_Process(void);
        void Tamper_Init(void);

    #endif

#ifdef	__cplusplus
}
#endif

#endif	/* TAMPER_H */

