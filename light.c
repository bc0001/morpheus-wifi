/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        light.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Functions for light measurement
|               
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/



/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include    "common.h"
#include    "light.h"
#include    "main.h"
#include    "led.h"
#include    "a2d.h"
#include    "led_display.h"
#include    "command_proc.h"
#include    "battery.h"


#ifdef CONFIGURE_LIGHT_SENSOR

/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

#define     LIGHT_INTERVAL_100_MS   (uint)100
#define     LIGHT_INTERVAL_500_MS   (uint)500
#define     LIGHT_INTERVAL_1000_MS  (uint)1000
#define     LIGHT_INTERVAL_1500_MS  (uint)1500
#define     LIGHT_INTERVAL_2000_MS  (uint)2000
#define     LIGHT_INTERVAL_3000_MS  (uint)3000
#define     LIGHT_INTERVAL_4000_MS  (uint)4000
#define     LIGHT_INTERVAL_5000_MS  (uint)5000
#define     LIGHT_INTERVAL_6000_MS  (uint)6000
#define     LIGHT_INTERVAL_7000_MS  (uint)7000
#define     LIGHT_INTERVAL_8000_MS  (uint)8000

#define     LIGHT_INTERVAL_7_SEC    (uint)7000
#define     LIGHT_INTERVAL_10_SEC   (uint)10000
#define     LIGHT_INTERVAL_30_SEC   (uint)30000
#define     LIGHT_INTERVAL_59_SEC   (uint)59000


#define     LIGHT_TIMER_2_ACTIVE_SECONDS   (uint)2
#define     LIGHT_TIMER_5_ACTIVE_SECONDS   (uint)5
#define     LIGHT_TIMER_10_ACTIVE_SECONDS  (uint)10
#define     LIGHT_TIMER_53_ACTIVE_SECONDS  (uint)53
#define     LIGHT_TIMER_60_ACTIVE_SECONDS  (uint)60

#define     LIGHT_TIMER_90_STBY_SECONDS    (uint)9
#define     LIGHT_TIMER_60_STBY_SECONDS    (uint)6
#define     LIGHT_TIMER_30_STBY_SECONDS    (uint)3

#define     TIME_SECS_10            (uint)10
#define     TIME_SECS_30            (uint)30

#define     TIME_HOURS_24           (uint)24
#define     TIME_HOURS_12           (uchar)12
#define     TIME_HOURS_14           (uchar)14
#define     TIME_HOURS_6            (uchar)6
#define     TIME_HOURS_4            (uchar)4

// 24 Hour Data Structure
#define     LIGHT_STRUCTURE_LENGTH  (uchar)24
#define     LIGHT_STRUCTURE_START   (uchar)0
#define     LIGHT_STRUCTURE_END     (uchar)LIGHT_STRUCTURE_LENGTH -1
#define     HOUR_CNT_UNDERFLOWED    (uchar)0xFF


#define     LIGHT_NUM_SAMPLES       (uchar)4
#define     LIGHT_DIM_COUNT_THRESHOLD (uchar)1     // 1 sample

#define     SOUND_INHIBIT_MAX_HR    (uint)14
#define     SOUND_INHIBIT_MAX       (uint)(SOUND_INHIBIT_MAX_HR * 3600)

#define     NIGHT_DAY_TH_HYST       (uint)30

// Light/Day Threshold = % of Min/Max Range
#define LGT_TH_PERCENT  (uint)30

// 100% Duty Cycle Time
#define LGT_LED_BLINK_TIME_LONG      (uint)25000
#define LGT_LED_BLINK_TIME_SHORT     (uint)5000

#define LGT_LED_DIM_BLINK_COUNT     (uint)1000
// Dim Duty Cycle
#define LGT_LED_PULSE_ON_TIME       (uint)1
#define LGT_LED_PULSE_OFF_TIME      (uint)20



// A/D Reference for PIC = VDD
// FVR to calculate VDD = 1024 mv
#define FVR_MVOLTS			(uint)1024
// FVR_MVOLTS * 1024
#define FVR_FACTOR          (unsigned long)1048576

#define STATE_LGT_INIT      0
#define STATE_LGT_IDLE      1
#define STATE_LGT_TEST      2
#define STATE_LGT_DISCHARGE 3
#define STATE_RECOVERY_TIME 4
#define STATE_LGT_MEAS      5
#define STATE_LGT_BLINK     6


// define minimum signal limit (delta in mv)
#define LGT_DETECT_MIN  (uint)110
#define MIN_DARK_PERIOD (uchar)6

// Make Light Led Light Sensor Pin analog input
#define LIGHT_LED_INPUT        TRISBbits.TRISB4 = 1;
#define LIGHT_LED_ANALOG_IN    ANSELBbits.ANSB4 = 1;
// Make Light Led Pin Output
#define LIGHT_LED_OUTPUT        TRISBbits.TRISB4 = 0;
#define LIGHT_LED_DIGITAL_OUT   ANSELBbits.ANSB4 = 0;


// Light Sensor LED
#define LIGHT_LED_ON    LAT_LIGHT_SENSE_PIN = 1;
#define LIGHT_LED_OFF   LAT_LIGHT_SENSE_PIN = 0;

// Light Structure Attribute Bit Masks
#define ATTRIB_LIGHT_BIT_SET   0x80
#define ATTRIB_NIGHT_BIT_SET   0x04


// Set Dim Light Threshold (based upon testing with current light sense LED)
//#define GLED_DIM_TH   (uint)410
//#define GLED_DIM_TH   (uint)310
//#define GLED_DIM_TH   (uint)275

// Lower Dim Threshold Lower based upon Light Testing against a purchased
//  Nightlight in Light Test Room 
// (Average of 8 units mounted on ceiling and walls)  
// Ceiling - about 80  
// Walls - about 115   (max 167)
// Hyst. average - ~20
//#define GLED_DIM_TH     (uint)160

#define GLED_DIM_TH     (uint)390


// ****************************************************************************
// Debug  Section, Test Only Defines (un-comment as needed)


// ****************************************************************************



/*
|-----------------------------------------------------------------------------
| Typedef and Enums and Constants
|-----------------------------------------------------------------------------
*/

#ifdef CONFIGURE_LIGHT_TEST_TABLE

// These defined tables are used for testing only for the 1st 24 hours
//  (accelerated in minutes)
    
#ifdef CONFIGURE_LGT_TABLE_DAY_FIRST 
    // start in Day time
    uint LGT_TEST_TABLE[24]=
    {
        1100,       //0
        1101,
        1102,
        1103,
        1104,
        1105,
        1106,
        1107,
        108,
        109,
        110,
        111,
        112,
        113,
        114,
        115,
        116,
        117,
        118,
        1119,
        1120,
        1121,
        1122,
        1123        //23

    };
 
#else
 
    // start at Night time
    uint LGT_TEST_TABLE[24]=
    {
        100,       //0
        101,
        102,
        103,
        104,
        105,
        106,
        1107,
        1108,
        1109,
        1110,
        1111,
        1112,
        1113,
        1114,
        1115,
        1116,
        1117,
        1118,
        1119,
        1120,
        121,
        122,
        123        //23

    };
 
#endif
#endif


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/
persistent uint LIGHT_Night_Day_TH;
persistent uchar LIGHT_Current_Hour;

// 24 Hour Light Data Structure {Hours 0 - 23)
persistent struct light_data_t LIGHT_Hour[LIGHT_STRUCTURE_LENGTH];


/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
static uchar light_state = STATE_LGT_IDLE;

#ifdef CONFIGURE_LGT_FADE_OFF_TO_MEASURE
    // Init First measurement
    static uint light_timer = LIGHT_TIMER_60_ACTIVE_SECONDS;
#else
    static uint  light_timer = LIGHT_TIMER_2_ACTIVE_SECONDS;
#endif

static uint  sound_inhibit_timer = 0;
static uchar light_dim_count = 0;

static uint light_meas_avg = 0;
uint light_min = 0;
uint light_max = 0;

uchar light_longest_drk_count;
uchar dark_count;
uchar light_count;
uchar dark_hour;




/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/
void light_analysis(void);

#ifdef CONFIGURE_LIGHT_TEST_TABLE
    // Test Only
    void light_struct_init(void);
#endif

    
/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/

/*
+------------------------------------------------------------------------------
| Function:      LIGHT_Data_Init
+------------------------------------------------------------------------------
| Purpose:       Clear Light Data Structure
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void LIGHT_Data_Init(void)
{
    for(uchar i = 0; i < LIGHT_STRUCTURE_LENGTH; i++)
    {
        LIGHT_Hour[i].Light_Current = 0;
        LIGHT_Hour[i].Light_Previous = 0;
        LIGHT_Hour[i].Attrib = 0;

        FLAG_LIGHT_ALG_ON = 0;
    }
    
    LIGHT_Current_Hour = 0;
    LIGHT_Night_Day_TH = 0;

    #ifdef CONFIGURE_LIGHT_TEST_TABLE
        // Firmware Test Code Only to pre-load the Light Structure
        light_struct_init();
        light_analysis();
        LIGHT_Current_Hour = 0;
    #endif
}


/*
+------------------------------------------------------------------------------
| Function:     LIGHT_Manager
+------------------------------------------------------------------------------
| Purpose:      Light Manager handles timing for Light Measurement Sampling
|               In AC Power Mode, we sample faster for nightlight response                          
|               In DC Power Mode, we sample at a slower rate
|
|               Ambient Light is measured and recorded in a 24 hour data 
|               structure. Every 24 hours it is analyzed and a Day/Night
|               cycle is determined. In one cannot be determined, then light
|               algoeithm is turned Off.
|
|               LED light dim is set or reset based upon most recent light
|               measurements. Dim is set if light falls below a preset
|               Dim Threshold.
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  Task Next Execution Return Time (in Task Engine TICs)
+------------------------------------------------------------------------------
*/
unsigned int LIGHT_Manager(void)
{
    #ifdef CONFIGURED_UNDEFINED
        // Test Only
        SER_Send_String("LGT Stat");
        SER_Send_Int('e', light_state, 10);
    #endif

    switch (light_state)
    {
        case STATE_LGT_IDLE:
        {
            #ifdef CONFIGURE_CO
                // If not calibrated do not perform any Light sampling
                if(!FLAG_CALIBRATION_COMPLETE || FLAG_CALIBRATION_UNTESTED ||
                    FLAG_NO_SMOKE_CAL)
                {
                    return  MAIN_Make_Return_Value(LIGHT_INTERVAL_30_SEC);
                }
            #else
                // If no Smoke Cal do not perform any Light sampling
                if(FLAG_NO_SMOKE_CAL)
                {
                    return  MAIN_Make_Return_Value(LIGHT_INTERVAL_30_SEC);
                }

            #endif

            #ifdef  CONFIGURE_MAN_NIGHT_DAY_TOGGLE
                // Don't run Light manager Code if Test Code enabled
                return  MAIN_Make_Return_Value(LIGHT_INTERVAL_30_SEC);
            #endif

            if(FLAG_SMOKE_ALARM_ACTIVE || FLAG_CO_ALARM_ACTIVE)
            {
                // Postpone Light Sampling if in Alarm
                return  MAIN_Make_Return_Value(LIGHT_INTERVAL_1000_MS);
                
            }
                
            // Update Light sampling Timer    
            if(FLAG_MODE_ACTIVE)  
            {
                // Active Mode uses 1 second task times
                light_timer--;
            }
            else
            {
                // In Low Power Mode we run on 10 Second Task Times 
                //  so monitor / adjust light timer accordingly
                if(light_timer <= TIME_SECS_10)
                {
                    light_timer = 0;
                }
                else
                {
                    light_timer = (light_timer - TIME_SECS_10);
                }
            }
                
            // Test light timer expiration to make a measurement    
            if( (0 == light_timer) || FLAG_BLINK_LIGHT_SENS_LED )
            {
                // Time to initiate a Light measurement
                
                // 1st Reset the sampling Timer
                if(FLAG_MODE_ACTIVE)
                {
                    //
                    // In Active Mode, measure every 60 seconds
                    //  from end of previous measurement, this will be 
                    //  approx. every 70 seconds
                    //  
                    light_timer = LIGHT_TIMER_60_ACTIVE_SECONDS;
                    
                }
                else
                {
                    light_timer = LIGHT_TIMER_60_ACTIVE_SECONDS;
                }
                
                #ifdef CONFIGURE_LGT_FADE_OFF_TO_MEASURE
                    // Delay this Light measurement if ESC Light is ON
                    //  or Green LED is not in IDLE
                    if( !FLAG_ESC_LIGHT_ON && !FLAG_GLED_NOT_IDLE )
                #else
                    // Delay this Light measurement if ESC Light is ON
                    if(!FLAG_ESC_LIGHT_ON)
                #endif
                {
                    FLAG_LIGHT_MEAS_ACTIVE = 1;
                    
                    // Begin a light Sample next task time
                    light_state++;                    
                }
                else
                {
                    // Back up Timer to repeat this state
                    light_timer++;
                }
            }
                
            // When not sampling light, process current Light Structure    

            // ******* Low Battery Inhibit, Night Flag Control *******
            // If Night Data is valid and LOW Battery Inhibit is enabled and
            // If the current hour has night attribute set, Delay LB chirp
            if(FLAG_LIGHT_ALG_ON && !FLAG_SOUND_INHIBIT_DISABLE)
            {
                // Does current hour match a night time hour?
                if(LIGHT_Hour[LIGHT_Current_Hour].Attrib & 
                        ATTRIB_NIGHT_BIT_SET)
                {
                    if(!FLAG_SOUND_INHIBIT)
                    {
                        // Delay the LB Chirp
                        FLAG_SOUND_INHIBIT = 1;

                        // Init the maximun allowable time for LB Inhibit
                        sound_inhibit_timer = SOUND_INHIBIT_MAX;
                    }
                    else
                    {
                        if(FLAG_MODE_ACTIVE)
                        {
                            // Faster Task Time (1 second)
                            if(0 == --sound_inhibit_timer)
                            {
                                // Allow LB Chirps
                                FLAG_SOUND_INHIBIT = 0;

                                // Disable LB Inhibit feature after Timer 
                                //  expires.
                                // Flag only cleared by Soft or PWR Reset
                                FLAG_SOUND_INHIBIT_DISABLE = 1;
                            }
                        }
                        else
                        {
                            // Slower Task Time in LP Mode ( 30 seconds)
                            sound_inhibit_timer =
                                      (sound_inhibit_timer - TIME_SECS_30);

                            if(sound_inhibit_timer < 100)
                            {
                                // Allow LB Chirps
                                FLAG_SOUND_INHIBIT = 0;

                                // Disable LB Inhibit feature after Timer 
                                //  expires.
                                // Flag only cleared by Soft or PWR Reset
                                FLAG_SOUND_INHIBIT_DISABLE = 1;

                            }

                        }
                    }
                }
                else
                {
                    FLAG_SOUND_INHIBIT = 0;
                }
            }
            else
            {
                FLAG_SOUND_INHIBIT = 0;
            }
                
            // Detect Night to Day and Day to Night transitions 
            if(FLAG_LIGHT_ALG_ON)  
            {
                // Monitor for Current Night Hour
                if(LIGHT_Hour[LIGHT_Current_Hour].Attrib & 
                        ATTRIB_NIGHT_BIT_SET)
                {
                    // Current Hour is a Night Hour
                    
                    // If Previously Hour was Day
                    //  we must be transitioning from Day to Night
                    //  so set Day/Night Change Detect Flag
                    if(!FLAG_NIGHT_DETECT)
                    {
                        // Cleared after Processed by Trouble Manager
                        FLAG_DAY_NIGHT_SWITCHED = 1;
                        FLAG_NIGHT_DETECT = 1;

                        #ifdef CONFIGURE_LIGHT_CHANGE_SERIAL
                            SER_Send_String("Day/Night Change Detected- ");
                            SER_Send_Prompt();  
                        #endif
                    }
                }
                else
                {
                    // Current Hour is a Day Hour
                    
                    // If Previous Hour was Night
                    //  we must be transitioning from Night to Day
                    //  so set Day/Night Change Detect Flag
                    if(FLAG_NIGHT_DETECT)
                    {
                        // Cleared after Processed by Trouble Manager
                        FLAG_DAY_NIGHT_SWITCHED = 1;
                        FLAG_NIGHT_DETECT = 0;

                        #ifdef CONFIGURE_LIGHT_CHANGE_SERIAL
                            SER_Send_String("Night/Day Change Detected- ");
                            SER_Send_Prompt();  
                        #endif
                    }
                }
            }
        }
        break;

        case STATE_LGT_TEST:
        {
            // Prepare for measurement
            #ifdef CONFIGURE_LGT_FADE_OFF_TO_MEASURE 
                if(FLAG_AC_DETECT)
                {
                    if(!FLAG_LGT_MEAS_CYCLE)
                    {
                        // If AC_Detect is active (Green LED Ring ON)
                        //  request Green LED Fade OFF and wait for Fade OFF
                        FLAG_LGT_MEAS_CYCLE = 1;
                        
                        // Allow Fade OFF to begin immediately
                        MAIN_Reset_Task(TASKID_GLED_MANAGER);
                        
                        // Remain in this state Return time to allow Fade OFF
                        return  MAIN_Make_Return_Value(LIGHT_INTERVAL_1500_MS);
                    }
                    else
                    {
                        // Proceed to Light Measure state after 
                        //  completing FADE OFF (~ 2.5 sec delay))
                        light_state++;
                        return  MAIN_Make_Return_Value(LIGHT_INTERVAL_1000_MS);
                    }

                }
                else
                {                  
                    // Proceed to Light Discharge state under DC Power
                    light_state++;

                    if(FLAG_MODE_ACTIVE)
                    {
                        return  MAIN_Make_Return_Value(LIGHT_INTERVAL_1000_MS);
                    }
                    else
                    {
                        return  MAIN_Make_Return_Value(LIGHT_INTERVAL_1000_MS);
                    } 

                }
            
            #else

                // Simply Proceed to Light Measure state
                light_state++;

                if(FLAG_MODE_ACTIVE)
                {
                    return  MAIN_Make_Return_Value(LIGHT_INTERVAL_100_MS);
                }
                else
                {
                    return  MAIN_Make_Return_Value(LIGHT_INTERVAL_1000_MS);
                } 
                
            #endif
        }
       
        case STATE_LGT_DISCHARGE:
        {
            // proceed to next state
            light_state++;
                
            if(FLAG_MODE_ACTIVE)
            {
                // Supply a discharge pulse before beginning 
                //  Light sample cycle 
                LIGHT_LED_OUTPUT
                LIGHT_LED_DIGITAL_OUT         
                LIGHT_LED_OFF

                return  MAIN_Make_Return_Value(LIGHT_INTERVAL_1000_MS);
            }
            else
            {
                // Supply a discharge Pulse before measurement
                LIGHT_LED_OUTPUT
                LIGHT_LED_DIGITAL_OUT         
                LIGHT_LED_OFF

                return  MAIN_Make_Return_Value(LIGHT_INTERVAL_1000_MS);
            }     
         }  
        
        case STATE_RECOVERY_TIME:
        { 
            // Discharge Complete set to analog Input
            LIGHT_LED_ANALOG_IN
            LIGHT_LED_INPUT
                    
            // Allow sampling input to stabilize before measurement 
            light_state++;
            
            #ifdef CONFIGURE_SPECIAL_LIGHT_TEST_FW
                if (FLAG_LED_RING_OFF)
                {
                    // Compare long stabilize time against shorter to optimize
                    //  measurement time after a discharge pulse
                    return  MAIN_Make_Return_Value(LIGHT_INTERVAL_6000_MS);
                }
                else
                {
                    // This is the normal operating time
                    return  MAIN_Make_Return_Value(LIGHT_INTERVAL_6000_MS);
                }
            #else
                return  MAIN_Make_Return_Value(LIGHT_INTERVAL_6000_MS);
            #endif
          
        }
        
        case STATE_LGT_MEAS:
        {
            // Read 1.024 FVR for VDD calculation
            uint GLED_FVR_Measured = A2D_Convert_10Bit(A2D_FVR_CHAN);

            // Take a Light Sample
            uint result = A2D_Convert_10Bit(A2D_LIGHT_CHAN);

            for(uchar i = 0; i < LIGHT_NUM_SAMPLES; i++)
            {
                PIC_delay_ms(1);
                // Take a Light Sample
                result = ((result + A2D_Convert_10Bit(A2D_LIGHT_CHAN)) / 2);
            }


            // Calculate current VDD voltage
            unsigned long GLED_Vdd = (FVR_FACTOR / GLED_FVR_Measured);

            // From VDD calculation, calculate the Green Led measured Voltage
            unsigned long GLED_Measured = ((unsigned long)(GLED_Vdd * result)
                                                                       / 1024);

            if(0 == light_meas_avg)
            {
                // Set Initial Average Value
                FLAG_LIGHT_FIRST_MEAS = 1;
                light_meas_avg = (uint)GLED_Measured;
            }
            else
            {
                FLAG_LIGHT_FIRST_MEAS = 0;
                light_meas_avg = ((light_meas_avg + (uint)GLED_Measured) / 2);
            }
            
            // Check to see if it is dark enough to apply Green LED Dim
            if( ((uint)GLED_Measured) < GLED_DIM_TH )
            {
                // Dark Detected
                if(LIGHT_DIM_COUNT_THRESHOLD <= ++light_dim_count)
                {
                    // Set/Continue Dim Light Detect condition
                    light_dim_count--;
                    
                    if(!FLAG_LED_DIM)
                    {
                        // Light Not Detected
                        FLAG_LED_DIM = 1;
                        
                        // Service Green LED task immediately
                        MAIN_Reset_Task(TASKID_GLED_MANAGER);
                    }
                    
                    #ifndef CONFIGURE_UNDEFINED
                        // Test Only
                        SER_Send_String("Dim Set...");
                        SER_Send_Prompt();
                    #endif

                }
            }
            else
            {
                // Add in some Hysteresis before clearing DIM
                if( ((uint)GLED_Measured) > 
                        (GLED_DIM_TH + NIGHT_DAY_TH_HYST) ) 
                {
                    if(FLAG_LED_DIM)
                    {
                        // Light Detected
                        FLAG_LED_DIM = 0;
                        
                        // Service Green LED task immediately
                        MAIN_Reset_Task(TASKID_GLED_MANAGER);
                    }
                
                    // Reset Dim Measured Counter
                    light_dim_count = 0;
                    
                }
            }

           if(SERIAL_ENABLE_LIGHT)
           {
                #ifdef CONFIGURE_OUTPUT_RAW_LIGHT
                    SER_Send_String("Lra");
                    SER_Send_Int('w', (uint)GLED_Measured, 10);
                    SER_Send_String(", Lav");
                    SER_Send_Int('g', (uint)light_meas_avg, 10);
                    SER_Send_Prompt();
                #else
                    SER_Send_Int('L', (uint)light_meas_avg, 10);
                    SER_Send_Prompt();
                #endif

           }

            FLAG_LIGHT_MEAS_ACTIVE = 0;         // Light Sample Completed
            light_state = STATE_LGT_IDLE;       // return to Idle state
          
            // If in Alarm do not Blink the Light Sensor LED
            if(!FLAG_SMOKE_ALARM_ACTIVE && !FLAG_CO_ALARM_ACTIVE)
            {
                if(FLAG_BLINK_LIGHT_SENS_LED)
                {
                    // Only Blink 1 time
                    FLAG_BLINK_LIGHT_SENS_LED = 0;

                    // Blink Light Sensor LED
                    // 1. Program Pin as Output
                    // 3. Blink Led (pulse))
                    // 4. Discharge it while output low
                    // 5. Return to Input (Light sensor))
                    LIGHT_LED_OUTPUT
                    LIGHT_LED_DIGITAL_OUT         

                    if(FLAG_LED_DIM) 
                    {
                        // LED Dim Mode

                        //Blink the Short Blink for Dim condition
                        // AC or DC Power
                        LIGHT_LED_ON
                        for(uint i = 0; i < LGT_LED_BLINK_TIME_SHORT;i++)
                        {
                            // LED Blink Pulse
                        }
                        LIGHT_LED_OFF
                                
                    }
                    else
                    {
                        // Not LED Dim Mode
                        
                        if(FLAG_MODE_ACTIVE)
                        {
                            // AC or DC Active Mode
                            LIGHT_LED_ON
                            for(uint i = 0; i < LGT_LED_BLINK_TIME_LONG;i++)
                            {
                                // LED Blink Pulse
                            }
                            LIGHT_LED_OFF
                        }
                        else
                        {
                            // DC Low Power Mode
                            LIGHT_LED_ON
                            for(uint i = 0; i < LGT_LED_BLINK_TIME_SHORT;i++)
                            {
                                // LED Blink Pulse
                            }
                            LIGHT_LED_OFF
                        }
                    }
                    
                    for(uint i = 0; i < LGT_LED_BLINK_TIME_LONG;i++)
                    {
                        // Add some LED Off Time before returning to 
                        //  analog input and Light sampling
                    }
                    
                    // Set back to analog input sampling
                    LIGHT_LED_ANALOG_IN
                    LIGHT_LED_INPUT
                }
            }

            if(FLAG_MODE_ACTIVE)
            {
                #ifdef CONFIGURE_LGT_FADE_OFF_TO_MEASURE
                    // Allow Green LED to Fade back On
                    FLAG_LGT_MEAS_CYCLE = 0;
                    
                    if(FLAG_AC_DETECT && !FLAG_LED_DIM)
                    {
                        // Allow FADE ON to begin immediately
                        MAIN_Reset_Task(TASKID_GLED_MANAGER);
                    }
                    
                    // 1 Second Task Timing for active mode
                    return  MAIN_Make_Return_Value(LIGHT_INTERVAL_1000_MS);
                    
                #else

                    // Set minimum stabilization time before next measurement 
                    //  5 seconds + 2 second mearuremnt timer
                    return  MAIN_Make_Return_Value(LIGHT_INTERVAL_5000_MS);
                    
                #endif
                
            }
            else
            {
                // 10 Second Task return 
                return  MAIN_Make_Return_Value(LIGHT_INTERVAL_10_SEC);
            }  
         }
        
        default:
            light_state = STATE_LGT_IDLE;
        break;

    }

    if(FLAG_MODE_ACTIVE)
    {
        return  MAIN_Make_Return_Value(LIGHT_INTERVAL_1000_MS);
    }
    else
    {
        return  MAIN_Make_Return_Value(LIGHT_INTERVAL_10_SEC);
    }
}


/*
+------------------------------------------------------------------------------
| Function:      light_analysis
+------------------------------------------------------------------------------
| Purpose:       Analysis of Light Data Structure to determine Dark/Light 
|                Threshold and determine a night day cycle for Low Battery 
|                Chirp control.
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void light_analysis(void)
{

    // Collect signal levels from most recent 24 hours
    light_max = LIGHT_Hour[0].Light_Current;
    light_min = LIGHT_Hour[0].Light_Current;

    // Record min and max light samples for previous 24 hours
    for(uint h = 0; h < TIME_HOURS_24; h++)
    {
        if(LIGHT_Hour[h].Light_Current > light_max)
        {
            light_max = LIGHT_Hour[h].Light_Current;
        }
        if(LIGHT_Hour[h].Light_Current < light_min)
        {
            light_min = LIGHT_Hour[h].Light_Current;
        }
    }

    // If light signal detected less than LGT_DETECT_MIN then disable
    // Light Algorithm Flag and start next 24 Hours
    // (either all light or all dark measured)
    if((light_max - light_min) < LGT_DETECT_MIN)
    {
        FLAG_LIGHT_SIGNAL_GOOD = 0;

        // Disable Light ALG
        // This will use default settings for any Night Light Control
        // and Low Battery Chirp control.
        FLAG_LIGHT_ALG_ON = 0;

        for(uchar i = 0; i < TIME_HOURS_24; i++)
        {
            // Save to previous 24 hours data and prepare
            // for next 24 hour measurement period
            LIGHT_Hour[i].Light_Previous = LIGHT_Hour[i].Light_Current;
            LIGHT_Hour[i].Light_Current = 0;
            // Clear Attributes
            LIGHT_Hour[i].Attrib = 0;
        }
    }
    else
    {
        FLAG_LIGHT_SIGNAL_GOOD = 1;

        // Set Dark/Light Threshold for this analysis
        unsigned long Matha = (((unsigned long)light_max - light_min) << 16);
        Matha = (((Matha * LGT_TH_PERCENT)/100)  >> 16);
        Matha += light_min;
        LIGHT_Night_Day_TH = (uint)Matha;

        //Locate 1st Light Transition and set starting hour
        // for data light analysis.
        if(LIGHT_Hour[0].Light_Current > LIGHT_Night_Day_TH)
        {
            // Look for Light to Dark transition
            for(uint h = 1; h < TIME_HOURS_24; h++)
            {
                if(LIGHT_Hour[h].Light_Current < LIGHT_Night_Day_TH)
                {
                    FLAG_LIGHT = 0;       //Init the Light Flag
                    LIGHT_Current_Hour = h;
                    // Set to 24 to immediately terminate loop
                    h = TIME_HOURS_24;
                }
            }
        }
        else
        {
            // Look for Dark to Light transition
            for(uint h = 1; h < TIME_HOURS_24; h++)
            {
                if(LIGHT_Hour[h].Light_Current > LIGHT_Night_Day_TH)
                {
                    FLAG_LIGHT = 1;       //Init the Light Flag
                    LIGHT_Current_Hour = h;
                    // Set to 24 to immediately terminate loop
                    h = TIME_HOURS_24;
                }
            }
        }

        // Init counters
        light_longest_drk_count = 0;
        dark_count = 0;
        light_count = 0;

        // Sweep 24 hour data and determine dark hour counts.
        // Current hour is pointing to 1st detected light transition hour
        for(uint h = 0; h < TIME_HOURS_24; h++)
        {
            if(LIGHT_Current_Hour > LIGHT_STRUCTURE_END)
            {
                LIGHT_Current_Hour = 0;
            }

            if(LIGHT_Hour[LIGHT_Current_Hour].Light_Current > 
                    LIGHT_Night_Day_TH)
            {

                // Set Light Detected attribute for this hour
                LIGHT_Hour[LIGHT_Current_Hour].Attrib = 0x80;

                // Light Hour
                if(!FLAG_LIGHT)
                {
                    //Dark to Light Transition detected, Init Light counter
                    light_count = 1;
                }
                else
                {
                    // Next Light Hour
                    light_count++;
                }

               FLAG_LIGHT = 1;  // Indicate we are in Light Counting sequence
            }
            else
            {
                // Clr Light Detected attribute for this hour
                LIGHT_Hour[LIGHT_Current_Hour].Attrib = 0x00;

                // Dark Hour
                if(FLAG_LIGHT)
                {
                    //Light to Dark transition
                    if(light_count < TIME_HOURS_4)
                    {
                        // Filter out (disregard) Light Hours if under 4 hours 
                        // and Count towards dark hours
                        dark_count += light_count;
                    }
                    else
                    {
                        //Light to dark transition, init dark counter
                        dark_count = 1;
                    }

                }
                else
                {
                    //Next dark hour
                    dark_count++;
                }

                // Record the longest dark Period detected
                if(dark_count > light_longest_drk_count)
                {
                    // Save longest detected Dark Hour and Count
                    light_longest_drk_count = dark_count;
                    dark_hour = LIGHT_Current_Hour;

                }

                //Light_Tot_Drk_Count++;
                FLAG_LIGHT = 0;   // Indicate we are in Dark Counting sequence

            }

            LIGHT_Current_Hour++;
        }

        // OK
        // Dark Hours have been tested and hour counts set.
        // Attempt to define night period from dark hours
        //

        // 1st move data to previous storage area of structure
        for(uchar h = 0; h < TIME_HOURS_24; h++)
        {
            // Save to previous 24 hours data and prepare
            // for next 24 hour measurement period
            LIGHT_Hour[h].Light_Previous = LIGHT_Hour[h].Light_Current;
            LIGHT_Hour[h].Light_Current = 0;
        }

        // Dark count must be a large enough period (MIN_DARK_PERIOD)
        //  to be accepted as a recognized night period.
        if(light_longest_drk_count > (MIN_DARK_PERIOD - 1) )
        {
            // Find the center hour of longest dark period
            //  then set night attribute +/- 6 hours from center
            LIGHT_Current_Hour = dark_hour;
            for(uchar i = (dark_count >> 1); i > 0; i--)
            {
                //Backup into center of dark period
                if(HOUR_CNT_UNDERFLOWED == --LIGHT_Current_Hour)
                {
                    // wrap to last hour
                    LIGHT_Current_Hour = LIGHT_STRUCTURE_END;    
                }

            }
            // Back up another 6 hours
            for(uchar i = TIME_HOURS_6; i > 0; i--)
            {
                if(HOUR_CNT_UNDERFLOWED == --LIGHT_Current_Hour)
                {
                    // wrap to last hour
                    LIGHT_Current_Hour = LIGHT_STRUCTURE_END;    
                }

            }

            // Now we are pointing to the start of night period
            // For a 12 Hour period, set the Night Detect Attribute
            // Added 1 extra Hour (14 Hours) to postpone any chirps a 
            //  little longer)
            for(uchar i = 0; i < TIME_HOURS_14; i++)
            {
                LIGHT_Hour[LIGHT_Current_Hour].Attrib = 
                   LIGHT_Hour[LIGHT_Current_Hour].Attrib | ATTRIB_NIGHT_BIT_SET;

                if(++LIGHT_Current_Hour > LIGHT_STRUCTURE_END)
                {
                    LIGHT_Current_Hour = 0;
                }

            }

            /* 
             * Also fill in any additional Zero Attribute hours with 
             *   ATTRIB_NIGHT_BIT_SET to expand Night Hours greater than 14 
             *   hours.
             * These should be hours that were below the Light/Day threshold
             *   but just outside the 14 hour night period.
             */               
            for(uchar h = 0; h < TIME_HOURS_24; h++)
            {
                if(0 == LIGHT_Hour[h].Attrib)
                {
                    LIGHT_Hour[h].Attrib = 
                       LIGHT_Hour[h].Attrib | ATTRIB_NIGHT_BIT_SET;
                }

            }

            // Indicate that we have valid Light Algorithm data
            FLAG_LIGHT_ALG_ON = 1;

            // Reset FLAG that disables LB Chirp Inhibit
            FLAG_SOUND_INHIBIT_DISABLE = 0;
        }
        else
        {
            // Unable to determine Night, Set Light Alg off
            FLAG_LIGHT_ALG_ON = 0;

        }

    }

    /* Init for the next 24 Hours of data collection
     * We dynamically adjust Night detected for multiple reasons
     * 1. Our 24 hour timer is based upon a 1% clock so timing drifts
     *     requiring contunuous light sampling.
     * 2. Alarm may be moved to a different location.
     * 3. Light levels may change at the current location over time
     *     so we may need to redefine light/dark thresholds
     */
    LIGHT_Current_Hour = 0;

}


/*
+------------------------------------------------------------------------------
| Function:      LIGHT_Record
+------------------------------------------------------------------------------
| Purpose:       Light record is called every Hour to fill the Light Data  
|                Structure with Hourly Avg. Light measurements and call Light 
|                Analysis once a full 24 hour cycle has been recorded.
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void LIGHT_Record(void)
{
    LIGHT_Hour[LIGHT_Current_Hour].Light_Current = (uint)light_meas_avg;
    
    // 24 hour Hour cycle
    if(TIME_HOURS_24 == ++LIGHT_Current_Hour)
    {
        // Analyze the Data for Light Algorithm and set attributes
        light_analysis();

        #ifdef CONFIGURE_OUTPUT_LIGHT_STRUCT
            if(SERIAL_ENABLE_LIGHT)
            {
                // For Test purposes and Data Collection transmit Data 
                //  Structure thru the serial port
                CMD_Processor("L");
            }
        #endif

    }
}


#ifdef CONFIGURE_LIGHT_TEST_TABLE

/*
+------------------------------------------------------------------------------
| Function:      light_struct_init
+------------------------------------------------------------------------------
| Purpose:       Firmware Test Code Only to pre-load the Light Structure  
|                
|                
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/
void light_struct_init(void)
{
    uint x;

    for(uchar i = 0; i < TIME_HOURS_24; i++)
    {
        x = LGT_TEST_TABLE[i];
        LIGHT_Hour[i].Light_Current = x;
    }
}

#endif




#else   // No light sensor configured

    // Placeholders for global routines if no Light Sensor
    unsigned int LIGHT_Manager(void)
    {
        return  MAIN_Make_Return_Value(LIGHT_INTERVAL_59_SEC);
    }
    
    void LIGHT_Record(void)
    {

    }
    
    void LIGHT_Data_Init(void)
    {
        
    }

#endif

 


