/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        light.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for light.c module
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


#ifndef LIGHT_H
#define	LIGHT_H

#ifdef	__cplusplus
extern "C" {
#endif



/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/
    
    // In mvolts
    #define NIGHT_DAY_TH_DEFAULT   (uint)425

/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/
    struct light_data_t
    {
        uint Light_Current;
        uint Light_Previous;
        uchar Attrib;
    };


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/
    #ifdef CONFIGURE_LIGHT_SENSOR
        extern persistent uchar LIGHT_Current_Hour;
        extern persistent struct light_data_t LIGHT_Hour[];
        extern persistent uint LIGHT_Night_Day_TH;
    #endif


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
    // Light Sensing Routines
    uint LIGHT_Manager(void);
    void LIGHT_Data_Init(void);
    void LIGHT_Record(void);



        
        
#ifdef	__cplusplus
}
#endif

#endif	/* LIGHT_H */

