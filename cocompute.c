/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        cocompute.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  To compute CO PPM using A/D measurement and calibration values
|               Contains functionality for CO ppm calculations and
|               filtering if required.
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include	"common.h"
#include	"cocompute.h"
#include	"systemdata.h"
#include	"coalarm.h"
#include	"comeasure.h"
#include	"cocalibrate.h"
#include	"life.h"
#include    "diaghist.h"
#include    "algorithm.h"

#ifdef CONFIGURE_VOICE
    #include "voice.h"
#endif


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

#ifdef	CONFIGURE_CO

// Annual age correction percent
#define ANNUAL_PERCENT_INCREASE	2.5
#define PERCENT_PER_QTR     ((ANNUAL_PERCENT_INCREASE*1000) / 4)

// Used on Digital Display Models
#define	PEAK_THRESHOLD_PPM      (uint)9
#define	DISPLAY_PPM_THRESHOLD   (uint)30
#define CO_PEAK_TEST_15_SAMPLES (uchar)15


#define DAYS_IN_YEAR            (uint)365
#define QTRS_IN_YEAR            (uint)4

#ifdef	CONFIGURE_CO_PEAK_MEMORY
    // Peak Record Threshold
    #define PEAK_PPM_THRESHOLD      (uint)10

    // Peak Indicator Flag Threshold
    #define PEAK_FLAG_THRESHOLD_PPM	(uint)100
#endif

// *****************************************************************************
// Debug  Section, Test Only Defines (un-comment as needed)

//#define CONFIGURE_DBUG_SERIAL_PPM_OUTPUT_CORRECTED

// *****************************************************************************


/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
#ifdef CONFIGURE_DIAG_HIST
    static uchar COMP_CO_Peak_Count = CO_PEAK_TEST_15_SAMPLES;
#endif

/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/
uint comp_age_correct_PPM(uint);

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/













/*
+------------------------------------------------------------------------------
| Function:      CO_Compute_Calc_PPM
+------------------------------------------------------------------------------
| Purpose:       convert Raw A/D counts to Co PPM using CO Sensor calibration
|                constants.
|
+------------------------------------------------------------------------------
| Parameters:    raw_counts  (A/D value from Co Sensor circuit measurment)
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void COMP_Co_Calc_PPM(uint raw_counts)
{
    uint offset = ((SYS_RamData.CO_Cal.offset_LSB) + 
                   (256 * SYS_RamData.CO_Cal.offset_MSB));

    // Save previous PPM value for ALG calculations
    ALG_Prev_PPM = CO_wPPM;

    // check for underflow first
    if( raw_counts < offset)
    {
        CO_wPPM = 0;
    }
    else
    {
        // subtract the offset and multipy by the scale.
        CO_wPPM = (raw_counts - offset);

        // Calculate PPM using scale (slope)
        CO_wPPM = (uint)(((uint32_t)CO_wPPM * (SYS_RamData.CO_Cal.scale_LSB + 
                          (256 * SYS_RamData.CO_Cal.scale_MSB))) >> 8);

        // Correct the PPM value by the age correction.
        CO_wPPM = comp_age_correct_PPM(CO_wPPM);

        #ifdef CONFIGURE_DIAG_HIST
            if(LFE_DAY_ZERO != LIFE_Get_Current_Day() )
            {
                //
                // Do not begin recording Peak CO Until Day 1
                //
                if(CO_wPPM > PEAK_THRESHOLD_PPM)
                {
                    // Don't spend processor time doing this if under 10 PPM
                    // Saves a little power in LP Mode
                    if(0 == --COMP_CO_Peak_Count)
                    {
                        // Test Peak CO every 15 samples
                        COMP_CO_Peak_Count = CO_PEAK_TEST_15_SAMPLES;

                        // This test will overrun the 10 ms tic when reading
                        //  Flash Memory
                        DIAG_Hist_Test_Peak(CO_wPPM);
                    }
                }
            }
        #endif

    }

    if(SERIAL_ENABLE_CO)
    {
        SER_Send_Int('C', CO_wPPM, 10);
        if(FLAG_CO_ALARM_CONDITION)
        {
            SER_Send_String("*");
        }
        SER_Send_Prompt();
    }
}


/*
+------------------------------------------------------------------------------
| Function:      comp_age_correct_PPM
+------------------------------------------------------------------------------
| Purpose:       Based upon the life of the Sensor, adjust PPM value by
|                increased sensitivity. (2.5 % per year)
|
|
+------------------------------------------------------------------------------
| Parameters:    ppm
+------------------------------------------------------------------------------
| Return Value:  ppm_corrected (adjusted ppm value)
+------------------------------------------------------------------------------
*/

uint comp_age_correct_PPM(uint ppm)
{
    unsigned long num_qtrs;
    uint days = LIFE_Get_Current_Day();

    // Calc. number of Qtrs of Life
    num_qtrs = ( ((uint32_t)days*(uint32_t)100) /
                 (((uint32_t)DAYS_IN_YEAR * 100)/QTRS_IN_YEAR) );

    uint ppm_corrected = ( ppm + ( ((uint32_t)ppm *
            (num_qtrs * (uint32_t)PERCENT_PER_QTR) ) / 100000 ) );

    #ifdef CONFIGURE_60_HOUR_TEST
        #ifdef CONFIGURE_DBUG_SERIAL_PPM_OUTPUT_CORRECTED
            SER_Send_Int('-', ppm, 10);
            SER_Send_Prompt();
            SER_Send_Int('+', ppm_corrected, 10);
            SER_Send_Prompt();
        #endif
    #endif

    return (ppm_corrected);
}


#else   // #ifdef CONFIGURE_CO (If No CO Sensor)

void COMP_Co_Calc_PPM(uint)
{

}

uint comp_age_correct_PPM(uint)
{
    return 0;
}

#endif  // #ifdef CONFIGURE_CO

