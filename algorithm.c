/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        algorithm.c
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Functions for smoke algorithm support.
|
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include "common.h"
#include "algorithm.h"
#include "comeasure.h"
#include "photosmoke.h"


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/
// Output Alg. Status via serial port
#ifndef CONFIGURE_UNDEFINED
    #define CONFIGURE_OUTPUT_ALG
#endif

#define ALG_CO_CHANGE_LIMIT (uchar)250
#define ALG_TEMP_CHANGE_LIMIT (uchar)250

#define ALG_CO_AMBIENT_TIME_24_HR (uchar)24
#define ALG_CO_AMBIENT_TIME_1_HR (uchar)1

/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| External Variables declared and owned by this module but used by others
|-----------------------------------------------------------------------------
*/

// Global variable definitions
#ifdef CONFIGURE_CO
    // CO
    uint ALG_COAverageROR = 0;
    uint ALG_COAveragePPM = 0;
    uchar ALG_CO_Offset = 0;
    uint ALG_Prev_PPM = 0;
    uint ALG_CO_PPM_Trigger = 0;
    uint ALG_CO_ROR_Trigger = 0;
#endif

// Smoke
avg_t ALG_PhotoReadingAvg = 0;

uchar ALG_PrevPhotoReading = 0;

uint ALG_SMKAverageROR = 0;
uchar ALG_SMKRiseDelta = 0;
uchar ALG_SMK_Alarm_Trigger = 0;
uint ALG_SMK_ROR_Trigger = 0;
uchar ALG_SMK_Delta_Trigger = 0;

// Temp
uint ALG_ADTemp = 0;
uint ALG_TMPAverageROR = 0;
uint ALG_Prev_Temp = 0;
uint ALG_TMP_ROR_Trigger = 0;

// General
uchar ALG_Fast_Sample_Timer = 0;


/*
|-----------------------------------------------------------------------------
| Variables used in this module
|-----------------------------------------------------------------------------
*/
#ifdef CONFIGURE_CO
    // Local variable definitions
    static uchar alg_hour_timer = 0;    // Used as CO Ambient Measurement Timer
#endif

/*
|-----------------------------------------------------------------------------
| Local Prototypes
|-----------------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------------------
| External Functions
|-----------------------------------------------------------------------------
*/


/*
+------------------------------------------------------------------------------
| Function:      ALG_Photo_Reading_Avg
+------------------------------------------------------------------------------
| Purpose:       Update the Smoke Photo Reading Average
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void ALG_Photo_Reading_Avg(void)
{

    // Initialize the average for the 1st sample
    if(0 == ALG_PhotoReadingAvg.word)
    {
        // Init reading (Into MSH of Average)
        ALG_PhotoReadingAvg.byte.msb = PHOTO_Reading;
    }
    else
    {
        ALG_PhotoReadingAvg.word = ALG_Running_Average(PHOTO_Reading,
                                                   ALG_PhotoReadingAvg.word);
    }
}


#ifdef CONFIGURE_CO

/*
+------------------------------------------------------------------------------
| Function:      ALG_ROR_Monitor_CO
+------------------------------------------------------------------------------
| Purpose:       Update the CO rate of Rise parameter
|                 CO Rate of Rise Monitor Entry
|                 Input - CO_wPPM holds current PPM value
|                         ALG_Prev_PPM holds Previous PPM value
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void ALG_ROR_Monitor_CO(void)
{
    uchar delta;

    if(CO_wPPM > ALG_Prev_PPM)
    {
        delta = (CO_wPPM - ALG_Prev_PPM);
        // Limit amount of CO change
        if(delta > ALG_CO_CHANGE_LIMIT)
        {
            delta = ALG_CO_CHANGE_LIMIT;
        }
    }
    else
    {
        delta = 0;
    }

    // Update new CO ROR
    ALG_COAverageROR = ALG_ROR_Monitor(delta,ALG_COAverageROR);

    // Round out any small fractional part below 0x00 10
    if(ALG_COAverageROR < 0x10)
    {
        ALG_COAverageROR = 0;
    }
 }
#endif // #ifdef CONFIGURE_CO


/*
+------------------------------------------------------------------------------
| Function:      ALG_ROR_Monitor_TMP
+------------------------------------------------------------------------------
| Purpose:       Update the Temperature rate of Rise parameter
|                 Temp Rate of Rise Monitor Entry
|                 Input - ALG_ADTemp holds current temp A/D value
|                         ALG_Prev_Temp holds Previous temp A/D value
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void ALG_ROR_Monitor_TMP(void)
{
    uchar delta;

    // Temperature is actually backwards
    // Value lowers as Temp Rises
    if(ALG_ADTemp < ALG_Prev_Temp)
    {
        delta = (ALG_Prev_Temp - ALG_ADTemp);
        // Limit amount of change
        if(delta > ALG_TEMP_CHANGE_LIMIT)
        {
            delta = ALG_TEMP_CHANGE_LIMIT;
        }

        if(!FLAG_SMOKE_FAST_SAMPLE)
        {
            // Divide by 10 for 10 sec sampling
            delta = delta/10;
        }
    }
    else
    {
        // Temp is dropping don't record a rise
        delta = 0;
    }

    // Update new Temp ROR
    ALG_TMPAverageROR = ALG_ROR_Monitor(delta,ALG_TMPAverageROR);

    // Round out any small fractional part below 0x00 10
    if(ALG_TMPAverageROR < 0x10)
    {
        ALG_TMPAverageROR = 0;
    }


 }

/*
+------------------------------------------------------------------------------
| Function:      ALG_ROR_Monitor_SMK
+------------------------------------------------------------------------------
| Purpose:       Update the Smoke rate of Rise parameter
|                 SMK Rate of Rise Monitor Entry
|                 Input - bPhotoReading holds current smoke A/D value
|                         bPrevPhotoReading holds Previous smoke A/D value
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void ALG_ROR_Monitor_SMK(void)
{
    uchar delta;

    if(PHOTO_Reading > ALG_PrevPhotoReading)
    {
        delta = (PHOTO_Reading - ALG_PrevPhotoReading);

        if(!FLAG_SMOKE_FAST_SAMPLE)
        {
            // Divide by 10 for 10 sec sampling
            delta = delta/10;
        }
        ALG_SMKRiseDelta = delta;
    }
    else
    {
        ALG_SMKRiseDelta = 0;
    }

    // Update new SMK ROR
    ALG_SMKAverageROR = ALG_ROR_Monitor(ALG_SMKRiseDelta,ALG_SMKAverageROR);

    // Round out any small fractional part below 0x00 10
    if(ALG_SMKAverageROR < 0x10)
    {
        ALG_SMKAverageROR = 0;
    }

    // Here we output the Algorithm Serial Data if enabled

    #ifdef CONFIGURE_SERIAL_PORT_ENABLED
      #ifdef CONFIGURE_OUTPUT_ALG
        if(SERIAL_ENABLE_ALG)
        {
            #ifdef	CONFIGURE_CO
                SER_Send_Char('C');
                SER_Send_Int('r', ALG_COAverageROR, 16);
                SER_Send_Char(',');
            #endif

            SER_Send_Char('S');
            SER_Send_Int('r', ALG_SMKAverageROR, 16);
            SER_Send_Char(',');

            SER_Send_Int('D', ALG_SMKRiseDelta, 16);

            #ifdef CONFIGURE_TEMPERATURE
                SER_Send_Char(',');
                SER_Send_Char('T');
                SER_Send_Int('r', ALG_TMPAverageROR, 16);
                //SER_Send_Char(',');
            #endif

            SER_Send_Prompt();
        }
      #endif
    #endif

 }

/*
+------------------------------------------------------------------------------
| Function:      ALG_ROR_Monitor
+------------------------------------------------------------------------------
| Purpose:       Rate of Rise Monitor
|                 
|
+------------------------------------------------------------------------------
| Parameters:    avg = current ROR average
|                value = 8 bit Delta to average in
+------------------------------------------------------------------------------
| Return Value:  updated average value returned
+------------------------------------------------------------------------------
*/

uint ALG_ROR_Monitor(uchar value, uint ror_avg)
{
    return (ALG_Running_Average(value,ror_avg));
}


/*
+------------------------------------------------------------------------------
| Function:      ALG_Running_Average
+------------------------------------------------------------------------------
| Purpose:       This computes running average of 8-Bit values.
|
|
+------------------------------------------------------------------------------
| Parameters:    avg = current running average
|                value = 8 bit value to average in running average
+------------------------------------------------------------------------------
| Return Value:  new running average
+------------------------------------------------------------------------------
*/

uint ALG_Running_Average(uchar value, uint average)
{
    uint wIntValue = (value << 8);     // Place value in the MSB (integer part)

    unsigned long x = (((unsigned long)average * 3) + wIntValue);

    // Divide by 4 and round up if needed
    
    x = x + 0x0002;   // Performs any required rounding before divide by 4
    x = x >> 2;

    return (uint)x;
}

#ifdef CONFIGURE_CO

/*
+------------------------------------------------------------------------------
| Function:     ALG_CO_Ambient_Compensate
+------------------------------------------------------------------------------
| Purpose:      CO ambient CO compensation routine
|               Tracks a running average of ambient background CO in PPM and
|               sets an offset for use in PPM Trigger detection.
|
|               Inputs - CO_wPPM contains most recent CO measurement
|
|               Outputs - ALG_COAveragePPM (updated measured CO running average)
| 		          ALG_CO_Offset, PPM to subtract in CO Trigger detection
|
|               Every Hour the CO running average is updated with ambient
|               CO measured
|               If CO_wPPM > maximum ambient limit (10 PPM) then that value is
|               not used in the average
|               Every 24 hours a new offset is calculated based upon the
|               running average. A test configuration that runs every minute
|               is available for testing purposes
|
|
|
+------------------------------------------------------------------------------
| Parameters:    none
+------------------------------------------------------------------------------
| Return Value:  none
+------------------------------------------------------------------------------
*/

void ALG_CO_Ambient_Compensate(void)
{
    if( !FLAG_FAULT_FATAL && !FLAG_CO_ALARM_CONDITION )
    {
        if( CO_MEAS_SAMPLE == CO_Meas_Get_State() )
        {
            // Test last PPM sample and verify it is within acceptable limits
            // Must be < CO Ambient Threshold
            if(CO_AMBIENT_LIMIT >= CO_wPPM )
            {
                // Compute running average
                ALG_COAveragePPM = (( (ALG_COAveragePPM * 3)+ CO_wPPM )/4);
            }

            // Update the Co Ambient Timer
            if(0 == alg_hour_timer)
            {
                // At Zero (1st Pass after reset)
                #ifdef	CONFIGURE_TEST_CO_AMBIENT
                        alg_hour_timer = ALG_CO_AMBIENT_TIME_1_HR;
                #else
                        alg_hour_timer = ALG_CO_AMBIENT_TIME_24_HR;
                #endif
            }
            else
            {
                if(0 == --alg_hour_timer)
                {
                    // At Zero (1st Pass after reset)
                    #ifdef CONFIGURE_TEST_CO_AMBIENT
                            alg_hour_timer = ALG_CO_AMBIENT_TIME_1_HR;
                    #else
                            alg_hour_timer = ALG_CO_AMBIENT_TIME_24_HR;
                    #endif

                    // Update latest PPM offset value
                    ALG_CO_Offset = (uchar)ALG_COAveragePPM;

                #ifdef	CONFIGURE_TEST_CO_AMBIENT
                    #ifdef	CONFIGURE_SERIAL_PORT_ENABLED
                        SER_Send_Char('C');
                        SER_Send_Int('a', ALG_COAveragePPM, 16);
                        SER_Send_Prompt();
                    #endif
                #endif

                }
            }
        }
    }
}

#endif