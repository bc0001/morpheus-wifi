/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        sound.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for sound.c module
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/


#ifndef	SOUNDER_H
#define SOUNDER_H


/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/
    #define HORN_ON     LAT_HORN_DISABLE_PIN = 0;
    #define HORN_OFF    LAT_HORN_DISABLE_PIN = 1;


	#define SND_STATE_IDLE      0
	#define	SND_STATE_OFF       1
	#define SND_STATE_ON        2
	#define SND_STATE_CHIRP     3
	#define SND_STATE_VERIFY    4


    #define	SND_TROUBLE_IDLE                    0
    #define	SND_TROUBLE_FATAL_FAULT             1
    #define SND_TROUBLE_EOL_FATAL               2
    #define SND_TROUBLE_EOL_MODE                3
    #define SND_TROUBLE_NETWORK_ERROR_MODE      4
    #define	SND_TROUBLE_LB                      5
    #define SND_TROUBLE_REMOTE_LB               6



/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/

    unsigned int SND_Process_Task(void);
    unsigned char SND_Get_State(void);
    unsigned char SND_Get_Trouble_State(void);
    void SND_Do_Chirp(void);
    void SND_Reset_Task(void);
    void SND_Set_Chirp_Cnt(uchar count);
    unsigned char SND_Get_Chirp_Cnt(void);
    unsigned int SND_Trouble_Manager_Task(void);

    #ifdef CONFIGURE_WIRELESS_MODULE
        void SND_Init_Fault_Timer_Off(void);
    #endif

    #ifdef CONFIGURE_LB_HUSH    
        void SND_Init_LB_Timer_Off(void);
    #endif

    void SND_Init_Voice_Timer_Off(void);


#endif  //SOUNDER_H

