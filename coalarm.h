/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        coalarm.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for coalarm.c module
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

#ifndef COALARM_H
#define COALARM_H

/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

// For Functional Test use 40 PPM Alarm Threshold
#define FUNC_CO_ALARM_THRESHOLD     (uint)40
#define CO_PRESENT_THRESHOLD		(uint)100


#ifdef	CONFIGURE_UL_ALARM_CURVE

    #define	AL_ALARM_THRESHOLD      (uint)8260

    //
    //  The threshold limit constant sets the accumulator limit.  The 
    //  accumulator limit is not set to the alarm threshold because algorithm 
    //  isn't allowed to decay quickly enough.  The limit is set by subtracting 
    //  the PPM value or a multiple thereof where the decay should begin, 
    //  from the threshold.
    //
    #define	AL_ALARM_THRESHOLD_LIMIT            (uint)8108
    #define	AL_REALARM_THRESHOLD                (uint)7489

    #define AL_ACCUMULATOR_MINIMUM_THRESHOLD	(uint)45
    #define AL_ACCUMULATOR_COMP_THRESHOLD       (uint)38
    #define AL_ACCUMULATOR_DECAY_CONSTANT       (uint)13
    #define AL_ACCUMULATOR_DECAY_THRESHOLD      (uint)97

    #define AL_THRESHOLD_SLOPE_MULTIPLE         (uint)4
    #define AL_THRESHOLD_SLOPE_HIGH_MULTIPLE	(uint)8
    #define AL_THRESHOLD_SLOPE_THRESHOLD        (uint)228

#endif


//
//  BSI Alarm Definitions
//
#ifdef	CONFIGURE_BSI_ALARM

    #define AL_RESET_SINGLE_THRESHOLD       (uint)82
    #define AL_RESET_INHIBIT_THRESHOLD      (uint)229

    #define	AL_ALARM_THRESHOLD              (uint)3590

    //
    //  The threshold limit constant sets the accumulator limit.  The accumulator
    //  limit is not set to the alarm threshold because algorithm isn't allowed to
    //  to decay quickly enough.  The limit is set by subtracting the PPM value or
    //  a multiple thereof where the decay should begin, from the threshold.
    //
    #define	AL_ALARM_THRESHOLD_LIMIT        (uint)3514

    //
    //  The realarm threshold must be high enough to ensure the test button logic
    //  activates the alarm immediately.
    //
    #define	AL_REALARM_THRESHOLD            (uint)3388

    #define AL_ACCUMULATOR_MINIMUM_THRESHOLD	(uint)45
    #define AL_ACCUMULATOR_COMP_THRESHOLD       (uint)38
    #define AL_ACCUMULATOR_DECAY_CONSTANT       (uint)18
    #define AL_ACCUMULATOR_DECAY_THRESHOLD      (uint)65

    #define AL_THRESHOLD_SLOPE_MULTIPLE         (uint)2
    #define AL_THRESHOLD_SLOPE_HIGH_MULTIPLE	(uint)8
    #define AL_THRESHOLD_SLOPE_THRESHOLD        (uint)206

#endif


//
//  CENELEC Alarm Definitions
//
#ifdef CONFIGURE_CENELEC_ALARM

    #define AL_RESET_SINGLE_THRESHOLD           (uint)82
    #define AL_RESET_INHIBIT_THRESHOLD          (uint)229

    // Set Forced Alarm Condition Threshold 
    //  at 200 PPM
    #define	CENELEC_300_PPM_THRESHOLD           (uint)200

    #define	AL_ALARM_THRESHOLD                  (uint)4200

    //
    //  The threshold limit constant sets the accumulator limit.  The 
    //  accumulator limit is not set to the alarm threshold because algorithm 
    //  isn't allowed to to decay quickly enough.  The limit is set by 
    //  subtracting the PPM value or a multiple thereof where the decay should 
    //  begin, from the threshold.
    //
    #define	AL_ALARM_THRESHOLD_LIMIT	(uint)4124

    //
    //  The realarm threshold must be high enough to ensure the test button 
    //  logic activates the alarm immediately.
    //
    #define	AL_REALARM_THRESHOLD		(uint)4074

    //#define AL_ACCUMULATOR_MINIMUM_THRESHOLD	(uint)45
    #define AL_ACCUMULATOR_MINIMUM_THRESHOLD	(uint)41
    #define AL_ACCUMULATOR_COMP_THRESHOLD       (uint)32
    #define AL_ACCUMULATOR_DECAY_CONSTANT       (uint)18
    #define AL_ACCUMULATOR_DECAY_THRESHOLD      (uint)63

    #define AL_THRESHOLD_SLOPE_MULTIPLE         (uint)3
    #define AL_THRESHOLD_SLOPE_HIGH_MULTIPLE	(uint)14
    #define AL_THRESHOLD_SLOPE_THRESHOLD        (uint)192

    //Performing Running Average if < 90 PPM
    #define AL_CENELEC_PPM_AVG_MAX              (uint)90	

    #define AL_CENELEC_50_PPM_MAX               (uint)70
    #define AL_CENELEC_50_PPM                   (uint)53
    #define AL_CENELEC_50_PPM_MIN               (uint)41

#endif



/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
uint CO_Alarm_Task(void);
void CO_Alarm_Init(void);


#endif  //COALARM_H
		
		
