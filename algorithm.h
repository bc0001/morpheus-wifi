/*
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
|
|  File:        algorithm.h
|  Author:      Bill Chandler
|
|------------------------------------------------------------------------------
|------------------------------------------------------------------------------
| Description:  Header file for algorithm.c module
|
|
|------------------------------------------------------------------------------
| Copyright:    This software is the property of
|
|               UTC Fire & Security
|               Colorado Springs, CO  80919
|               (719) 598-4262
|
|               �2015 UTC Fire & Security. All rights reserved.
|
|   The contents of this file are Kidde proprietary  and confidential.
|   The use, duplication, or disclosure of this material in any form without
|   permission from UTC Fire & Security is strictly forbidden.
|------------------------------------------------------------------------------
|
| Revision History:
|     Revisions maintained by SVN revision control software.
|
*/

#ifndef ALGORITHM_H
#define	ALGORITHM_H

/*
|-----------------------------------------------------------------------------
| Includes
|-----------------------------------------------------------------------------
*/
#include "photosmoke.h"


/*
|-----------------------------------------------------------------------------
| Defines
|-----------------------------------------------------------------------------
*/

//*****************************************************************************/
/* Algorithm constants
 * Note: estimated counts/uAmp = 6 counts
 *
 * SMK Standby State Constants
 * For ROR Constants - 2 bytes - High byte is Integer, Low Byte is fractional
 * (i.e. - 0x0280 == 2.5)  0xFF00 = ROR OFF
 */

#define	ALG_SMK_STBY_ALARM_OFFSET   0       // Alarm Threshold Offset
                                            //  (Added to Base Alarm Point)

#define	ALG_SMK_STBY_SROR_TRIGGER   (uint)0xFF00    // Smoke Rate of Rise 
                                                    //  Action Trigger

#define	ALG_SMK_STBY_DELTA_TRIGGER  (uchar)10   // Smoke Measurement Delta 
                                                //  Trigger (A/D counts)

#define	ALG_SMK_STBY_CO_TRIGGER     (uchar)30   // CO PPM Trigger Level (PPM)

#define	ALG_SMK_STBY_CROR_TRIGGER   (uint)0x0200    // CO Rate of Rise Action 
                                                    //  Trigger (PPM/Sec)

#define	ALG_SMK_STBY_TROR_TRIGGER   (uint)0x0200    // Temperature Rate of Rise 
                                                    //  Action Trigger 

// Hush Values
#define ALG_SMK_HUSH_PERCENT    (uint)40

// I believe hush is always referenced from Calibrated Alarm Threshold
//  so this should be closer to 6 uAmps of Hush 
// (3 uAmp Slump is not factored in)
//#define ALG_SMK_HUSH_FIXED      (uchar)24          // ~ 3.0 uAmps
#define ALG_SMK_HUSH_FIXED      (uchar)36          // ~ 6.0 uAmps

/*
 * SMK Slump1 State Constants (Sensitivity Reduction slump)
 * For ROR Constants - 2 bytes - High byte is Integer, Low Byte is fractional
 */

// Slump1 Percentage of CAV to Alarm Threshold (AT-CAV) * (percent/100)
#define ALG_SMK_SLUMP1_PERCENT  (uint)40
#define ALG_SMK_SLUMP1_LIMIT    (uchar)20 // ~ 3.0 uAmps Max

#define ALG_SMK_SLUMP1_FIXED    (uchar)18 // ~ 3.0 uAmps

#define	ALG_SMK_SLUMP1_SROR_TRIGGER (uint)0xFF00 // Smoke Rate of Rise 
                                                 //  Action Trigger

#define	ALG_SMK_SLUMP1_DELTA_TRIGGER (uchar)10 // Smoke Measurement Delta 
                                               //  Trigger (A/D counts)

#define	ALG_SMK_SLUMP1_CO_TRIGGER (uchar)30   // CO PPM Trigger Level

#define	ALG_SMK_SLUMP1_CROR_TRIGGER (uint)0x0200    // CO Rate of Rise Action 
                                                    //  Trigger

#define	ALG_SMK_SLUMP1_TROR_TRIGGER (uint)0x0200    // Temperature Rate of 
                                                    //  Rise Action Trigger

/*
 * SMK Slump2 State Constants (Delayed Alarm slump)
 * For ROR Constants - 2 bytes - High byte is Integer, Low Byte is fractional
 */

// Slump2 Percentage of CAV to Alarm Threshold (AT-CAV) * (percent/100)
#define ALG_SMK_SLUMP2_PERCENT  (uint)40
#define ALG_SMK_SLUMP2_LIMIT    (uchar)20 // ~ 3.0 uAmps Max

#define ALG_SMK_SLUMP2_FIXED    (uchar)18 // ~ 3.0 uAmps

#define	ALG_SMK_SLUMP2_SROR_TRIGGER  (uint)0x0300 // Smoke Rate of Rise Action
                                                  //  Trigger

#define	ALG_SMK_SLUMP2_DELTA_TRIGGER (uchar)10 // Smoke Measurement Delta
                                               //  Trigger (A/D counts)

#define	ALG_SMK_SLUMP2_CO_TRIGGER    (uchar)30 // CO PPM Trigger Level

#define	ALG_SMK_SLUMP2_CROR_TRIGGER  (uint)0x0200 // CO Rate of Rise Action
                                                  //  Trigger

#define	ALG_SMK_SLUMP2_TROR_TRIGGER  (uint)0x0900 // Temperature Rate of Rise
                                                  //  Action Trigger

/*
 * SMK Jump State Constants
 * For ROR Constants - 2 bytes - High byte is Integer, Low Byte is fractional
 */

// Jump Percentage of CAV to Alarm Threshold (AT-CAV) * (percent/100)
#define ALG_SMK_JUMP_PERCENT    (uint)40
#define ALG_SMK_JUMP_LIMIT      (uchar)20 // ~ 3.0 uAmps Max

#define	ALG_SMK_JUMP_SROR_TRIGGER (uint)0xFF00 // Smoke Rate of Rise Action
                                               //  Trigger

#define	ALG_SMK_JUMP_DELTA_TRIGGER (uchar)10 // Smoke Measurement Delta 
                                             //  Trigger(A/D counts)

#define	ALG_SMK_JUMP_CO_TRIGGER	   (uchar)30 // CO PPM Trigger Level

#define	ALG_SMK_JUMP_CROR_TRIGGER  (uint)0x0200 // CO Rate of Rise Action
                                                //  Trigger

#define	ALG_SMK_JUMP_TROR_TRIGGER  (uint)0x0200 // Temperature Rate of Rise 
                                                //  Action Trigger

//
// General ALG Constants
//
#define ALG_TRIGGER_SMK     (uchar)20 // in A/D counts (Triggers Smoke
                                      //  Detected and Fast Sampling)

#define	CO_AMBIENT_LIMIT	(uchar)11 // Maximum Ambient CO used for
                                      //  Compensation (in PPM)

#define ALG_SLUMP1_TIME		(uchar)8	// 8 minutes
#define ALG_JUMP_TIME		(uchar)4	// 4 minutes
#define	ALG_ALM_DELAY		(uchar)10	// 10 Seconds




/*
|-----------------------------------------------------------------------------
| Typedef and Enums
|-----------------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------------------
| Global Variables declared and owned by this module
|-----------------------------------------------------------------------------
*/
// Global Variables
// CO
extern uint ALG_COAverageROR;
extern uint ALG_COAveragePPM;
extern uchar ALG_CO_Offset;
extern uint ALG_Prev_PPM;
extern uint ALG_CO_PPM_Trigger;
extern uint ALG_CO_ROR_Trigger;

// Smoke
extern avg_t ALG_PhotoReadingAvg;
extern uchar ALG_PrevPhotoReading;
extern uint ALG_SMKAverageROR;
extern uchar ALG_SMKRiseDelta;
extern uchar ALG_SMK_Alarm_Trigger;
extern uint ALG_SMK_ROR_Trigger;
extern uchar ALG_SMK_Delta_Trigger;

// Temperature
extern uint ALG_TMPAverageROR;
extern uint ALG_Prev_Temp;
extern uint ALG_TMP_ROR_Trigger;
extern uint ALG_ADTemp;


// General
extern uchar ALG_Fast_Sample_Timer;



/*
|-----------------------------------------------------------------------------
| Global Functions owned and exposed by this module
|-----------------------------------------------------------------------------
*/
// Define Global Routines
void ALG_ROR_Monitor_TMP(void);
uint ALG_Running_Average(uchar value, uint average);
uint ALG_ROR_Monitor(uchar value, uint ror_avg);
void ALG_Photo_Reading_Avg(void);
void ALG_ROR_Monitor_SMK(void);


#ifdef CONFIGURE_CO
    void ALG_CO_Ambient_Compensate(void);
    void ALG_ROR_Monitor_CO(void);
#endif


#endif	/* ALGORITHM_H */

